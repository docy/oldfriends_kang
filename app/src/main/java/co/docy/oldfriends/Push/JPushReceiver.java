package co.docy.oldfriends.Push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import cn.jpush.android.api.JPushInterface;
import co.docy.oldfriends.Activitis.ActivityMainGroup;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventPushOnBind;
import de.greenrobot.event.EventBus;

/**
 * 自定义接收器
 * 
 * 如果不定义这个 Receiver，则：
 * 1) 默认用户会打开主界面
 * 2) 接收不到自定义消息
 */
public class JPushReceiver extends BroadcastReceiver {
	private static final String TAG = "JPush";

	private static int groupId;
	private static String groupName;

	@Override
	public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
//		Log.d(TAG, "[JPushReceiver] onReceive - " + intent.getAction() + ", extras: " + parseBundle(bundle));
		
        if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
            String regId = bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID);
            Log.d(TAG, "[JPushReceiver] 接收Registration Id : " + regId);
            //send the Registration Id to your server...
			MyConfig.jpush_regist_id = regId;
			MyLog.d("","API_USER_PUSH_TOKEN: httpURLConnPostJson() -1: " + MyConfig.jpush_regist_id);
			MyConfig.saveStringToPreference(context, MyConfig.PREF_JPUSH_REGIST_ID, MyConfig.jpush_regist_id);
			EventBus.getDefault().post(new EventPushOnBind());
                        
        } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
        	Log.d(TAG, "[JPushReceiver] 接收到推送下来的自定义消息: " + bundle.getString(JPushInterface.EXTRA_MESSAGE));
        	processCustomMessage(context, bundle);
        
        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
            Log.d(TAG, "[JPushReceiver] 接收到推送下来的通知");
            int notifactionId = bundle.getInt(JPushInterface.EXTRA_NOTIFICATION_ID);
            Log.d(TAG, "[JPushReceiver] 接收到推送下来的通知的ID: " + notifactionId);
        	
        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
            Log.d(TAG, "[JPushReceiver] 用户点击打开了通知");

			parseBundle(context, bundle);
            
//        	//打开自定义的Activity
//        	Intent i = new Intent(context, ActivityAdvanceSetting.class);
//        	i.putExtras(bundle);
//        	//i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        	i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP );
//        	context.startActivity(i);
        	
        } else if (JPushInterface.ACTION_RICHPUSH_CALLBACK.equals(intent.getAction())) {
            Log.d(TAG, "[JPushReceiver] 用户收到到RICH PUSH CALLBACK: " + bundle.getString(JPushInterface.EXTRA_EXTRA));
            //在这里根据 JPushInterface.EXTRA_EXTRA 的内容处理代码，比如打开新的Activity， 打开一个网页等..
        	
        } else if(JPushInterface.ACTION_CONNECTION_CHANGE.equals(intent.getAction())) {
        	boolean connected = intent.getBooleanExtra(JPushInterface.EXTRA_CONNECTION_CHANGE, false);
        	Log.w(TAG, "[JPushReceiver]" + intent.getAction() +" connected state change to "+connected);
        } else {
        	Log.d(TAG, "[JPushReceiver] Unhandled intent - " + intent.getAction());
        }
	}

	// 打印所有的 intent extra 数据
	private static String parseBundle(Context context, Bundle bundle) {
		StringBuilder sb = new StringBuilder();
		for (String key : bundle.keySet()) {
			if (key.equals(JPushInterface.EXTRA_NOTIFICATION_ID)) {
				sb.append("\nkey:" + key + ", value:" + bundle.getInt(key));
			}else if(key.equals(JPushInterface.EXTRA_CONNECTION_CHANGE)){
				sb.append("\nkey:" + key + ", value:" + bundle.getBoolean(key));
			} else if (key.equals(JPushInterface.EXTRA_EXTRA)) {
				if (bundle.getString(JPushInterface.EXTRA_EXTRA).isEmpty()) {
					Log.i(TAG, "This message has no Extra data");
					continue;
				}

				try {
					JSONObject json = new JSONObject(bundle.getString(JPushInterface.EXTRA_EXTRA));
					Iterator<String> it =  json.keys();

					groupId = -1;
					groupName = null;

					while (it.hasNext()) {
						String myKey = it.next().toString();
						sb.append("\nkey:" + key + ", value: [" +
								myKey + " - " +json.optString(myKey) + "]");

						/**
                         * 我们的key是groupId和groupName
						 */
						if(myKey.equals("groupId")){
							groupId = Integer.valueOf(json.optString(myKey));
						}else if(myKey.equals("groupName")){
							groupName = json.optString(myKey);
						}

					}
					if((groupId != -1)&&(groupName != null)){
						sendNotificationToApp(context, groupId, groupName);
					}
				} catch (JSONException e) {
					Log.e(TAG, "Get message extra JSON error!");
				}

			} else {
				sb.append("\nkey:" + key + ", value:" + bundle.getString(key));
			}
		}
		return sb.toString();
	}
	
	//send msg to MainActivity
	private void processCustomMessage(Context context, Bundle bundle) {
		
		Log.d("","[JPushReceiver]: processCustomMessage");
		
//		if (MainActivity.isForeground) {
//			String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
//			String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
//			Intent msgIntent = new Intent(MainActivity.MESSAGE_RECEIVED_ACTION);
//			msgIntent.putExtra(MainActivity.KEY_MESSAGE, message);
//			if (!ExampleUtil.isEmpty(extras)) {
//				try {
//					JSONObject extraJson = new JSONObject(extras);
//					if (null != extraJson && extraJson.length() > 0) {
//						msgIntent.putExtra(MainActivity.KEY_EXTRAS, extras);
//					}
//				} catch (JSONException e) {
//
//				}
//
//			}
//			context.sendBroadcast(msgIntent);
//		}
	}


	private static void sendNotificationToApp(Context context, int groupId, String groupName){


		MyConfig.cancelAllNotification();//清除其他推送通知

		//todo:这里要先判断主聊天室的当前状态

		PushNotificationClick pushNotificationClick = new PushNotificationClick();
		pushNotificationClick.groupId = groupId;
		pushNotificationClick.groupName = groupName;

		Intent intent = new Intent();
		if(!TextUtils.isEmpty(groupName) && groupName.length()>0 && !"null".equals(groupName)){
			intent.setClass(context.getApplicationContext(), ActivityMainGroup.class);
		}else{

            /**
             * 20160712 私聊也使用小组框架
             */

//			intent.setClass(context.getApplicationContext(), ActivityDirectChatGroup.class);
			intent.setClass(context.getApplicationContext(), ActivityMainGroup.class);
		}

		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, pushNotificationClick.groupId);
		intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, pushNotificationClick.groupName);

		MyLog.d("", "notification push: groupId:" + pushNotificationClick.groupId + "  name:" + pushNotificationClick.groupName);

		context.getApplicationContext().startActivity(intent);

	}
}
