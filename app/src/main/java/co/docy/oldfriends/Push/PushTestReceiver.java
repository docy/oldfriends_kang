package co.docy.oldfriends.Push;

import android.content.Context;
import android.content.Intent;

import com.baidu.android.pushservice.PushMessageReceiver;
import com.google.gson.Gson;

import java.util.List;

import co.docy.oldfriends.Activitis.ActivityMainGroup;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventPushMsg;
import co.docy.oldfriends.EventBus.EventPushOnBind;
import de.greenrobot.event.EventBus;

/**
 * Created by youhy on 8/6/15.
 */
public class PushTestReceiver extends PushMessageReceiver {

    @Override
    public void onBind(Context context, int errorCode, String appid,
                       String userId, String channelId, String requestId) {
//        //EventBus.getDefault().register(this);

        MyConfig.baidupush_appid = appid;
        MyConfig.baidupush_userId = userId;
        MyConfig.baidupush_channelId = channelId;
        MyConfig.baidupush_requestId = requestId;
        String responseString = "onBind errorCode=" + errorCode + " appid="
                + appid + " userId=" + userId + " channelId=" + channelId
                + " requestId=" + requestId;
        MyLog.d("", "baidu push PushTestReceiver onBind " + responseString);

        EventBus.getDefault().post(new EventPushOnBind());

    }

    @Override
    public void onUnbind(Context context, int i, String s) {
        MyLog.d("","baidu push PushTestReceiver onUnbind");
//        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onSetTags(Context context, int i, List<String> list, List<String> list1, String s) {
        MyLog.d("","baidu push PushTestReceiver onSetTags");

    }

    @Override
    public void onDelTags(Context context, int i, List<String> list, List<String> list1, String s) {
        MyLog.d("","baidu push PushTestReceiver onDelTags");

    }

    @Override
    public void onListTags(Context context, int i, List<String> list, String s) {
        MyLog.d("", "baidu push PushTestReceiver onListTags");

    }

    @Override
    public void onMessage(Context context, String s, String s1) {
        /**
         * 目前没有使用百度推送的message功能，而是使用下面的notification功能
         */
        MyLog.d("","baidu push PushTestReceiver onMessage: " + s + " " + s1);

        EventBus.getDefault().post(new EventPushMsg(new PushMsg(MyConfig.PUSH_onMessage, s, s1, null)));
    }

    @Override
    public void onNotificationClicked(Context context, String s, String s1, String s2) {

        MyConfig.cancelAllNotification();//清除其他推送通知

        MyLog.d("", "notification push: onNotificationClicked: s:" + s + "  s1:" + s1 + " s2:" + s2);

        //todo:这里要先判断主聊天室的当前状态


        Intent intent = new Intent();
        intent.setClass(context.getApplicationContext(), ActivityMainGroup.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Gson gson = new Gson();
        PushNotificationClick pushNotificationClick = gson.fromJson(s2, PushNotificationClick.class);

        intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, pushNotificationClick.groupId);
        intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, pushNotificationClick.groupName);

        MyLog.d("", "notification push: groupId:" + pushNotificationClick.groupId + "  name:" + pushNotificationClick.groupName);

        context.getApplicationContext().startActivity(intent);

//        EventBus.getDefault().post(new EventPushMsg(new PushMsg(MyConfig.PUSH_onNotificationClicked, s, s1, s2)));
    }

    @Override
    public void onNotificationArrived(Context context, String s, String s1, String s2) {
        MyLog.d("", "notification push: onNotificationArrived: s:" + s + "  s1:" + s1 + " s2:"+s2);

        EventBus.getDefault().post(new EventPushMsg(new PushMsg(MyConfig.PUSH_onNotificationArrived, s, s1, s2)));
    }
}
