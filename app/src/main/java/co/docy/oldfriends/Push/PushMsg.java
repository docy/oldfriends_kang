package co.docy.oldfriends.Push;

/**
 * Created by youhy on 6/1/15.
 */
public class PushMsg {

    public int type;
    public String s;
    public String s1;
    public String s2;

    public PushMsg(int type, String s, String s1, String s2){
        this.type = type;
        this.s = s;
        this.s1 = s1;
        this.s2 = s2;
    }

}
