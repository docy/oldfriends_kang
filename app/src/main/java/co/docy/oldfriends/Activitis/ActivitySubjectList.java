package co.docy.oldfriends.Activitis;


import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.R;


public class ActivitySubjectList extends ActivityBase {

//    public int groupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_list);

        Intent intent = getIntent();
        if (intent != null) {
            MyConfig.subjectListGroupId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, -1);
            String format = getString(R.string.title_activity_subject_list);
            String result = String.format(format, intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME));
            setTitle(result);
        } else {
            finish();
        }

        init();

        /**
         * todo:
         * 在用FragmentTransaction动态添加fragment时，
         * fragment其中的PullToRefreshListView控件没有被分配空间，
         * 可能是该控件在特定情况下计算height有误，或其他未知原因
         * 故目前采用布局文件添加fragment的方法
         * 此方法的缺陷在于，没有预先传递groupId给fragment
         */

//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        FragmentSubjectList fragmentSubjectList = FragmentSubjectList.newInstance(groupId);
//        fragmentTransaction.add(R.id.activity_subject_list_linearLayout, fragmentSubjectList);
//        fragmentTransaction.commit();

    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    private void init() {


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
