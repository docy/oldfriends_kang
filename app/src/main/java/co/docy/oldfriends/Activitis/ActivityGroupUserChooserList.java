package co.docy.oldfriends.Activitis;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.Fragments.FragmentContactMultiChoose;
import co.docy.oldfriends.R;

/**
 * Created by wuxue on 15/11/19.
 */
public class ActivityGroupUserChooserList extends ActivityBase {
    private FragmentContactMultiChoose fragmentContactMultiChoose;
    private LinkedList<ContactUniversal> list = new LinkedList<>();

    //  JsonGetGroupUserRet.GetGroupUserRet
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_user_list);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentContactMultiChoose = FragmentContactMultiChoose.newInstance(MyConfig.MODE_CHOOSE_SINGLE_WITHOUT_SUBMIT);
        fragmentTransaction.add(R.id.activity_contact_multi_choose_inearLayout, fragmentContactMultiChoose);
        fragmentTransaction.commit();
        MyLog.d("", "ListAdapterContactOfCompanyMultiChoose ActivityContactOfCompanyMultiChoose ");
        fragmentContactMultiChoose.submitListener = new FragmentContactMultiChoose.OnSubmitClicked() {
            @Override
            public void OnClicked(final LinkedList<ContactUniversal> list_universal_selected) {
                MyConfig.gotoUserInfo(ActivityGroupUserChooserList.this, list_universal_selected.get(list_universal_selected.size()-1).id);
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
       // list.clear();
        //list = ContactUniversal.convertListFromGetGroupUserRet(MyConfig.jsonGetGroupUserRet.data);
      //  list = MyConfig.activityGroupUserChooserListList;

        fragmentContactMultiChoose.setList(MyConfig.activityGroupUserChooserListList);
        fragmentContactMultiChoose.setTitle(MyConfig.activityGroupUserChooserListTitle);
    }
}
