package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.math.BigDecimal;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.R;

public class ActivityActivityInputPage extends ActivityBase {

    private String type;
    private EditText activity_input_page_number;
    private EditText activity_input_page_string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_input_page);
        activity_input_page_number = (EditText) findViewById(R.id.activity_input_page_number);
        activity_input_page_string = (EditText) findViewById(R.id.activity_input_page_string);
        activity_input_page_number.setVisibility(View.GONE);
        activity_input_page_string.setVisibility(View.GONE);

        type = getIntent().getStringExtra("type");
        switch (type){
            case "people_max":
                activity_input_page_number.setVisibility(View.VISIBLE);
                activity_input_page_number.setHint("请输入报名人数上限");
                activity_input_page_number.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case "address":
                activity_input_page_string.setVisibility(View.VISIBLE);
                activity_input_page_string.setHint("请输入地址");
                break;
            case "cost":
                activity_input_page_number.setVisibility(View.VISIBLE);
                activity_input_page_number.setHint("请输入活动费用(元)");

                break;
            case "people_num":
                activity_input_page_number.setVisibility(View.VISIBLE);
                activity_input_page_number.setHint("请输入报名人数");
                break;

        }

        /**页面显示时自动弹出软键盘*/
        MyConfig.show_keyboard_must_call_from_activity(this);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_input_page, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.activity_input_page_sure:

                backWithResult();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void backWithResult() {
        String result = "";
        switch (type){
            case "people_max":
                result = activity_input_page_number.getText().toString().trim();
                if(TextUtils.isEmpty(result)){
                    Toast.makeText(this,"人数上限不能为空",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(new BigDecimal(result).compareTo(new BigDecimal(Integer.MAX_VALUE)) == 1){/**当输入的数字超过int最大值时会在parseInt时出错，故做这个限制*/
                Toast.makeText(this,"您输入的数字过大，请重新输入",Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
            case "address":
                result = activity_input_page_string.getText().toString().trim();

                if(TextUtils.isEmpty(result)){
                    Toast.makeText(this,"地址不能为空",Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
            case "cost":
                result = activity_input_page_number.getText().toString().trim();

                if(TextUtils.isEmpty(result)){
                    Toast.makeText(this,"活动费用不能为空",Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
            case "people_num":
                result = activity_input_page_number.getText().toString().trim();

                if(TextUtils.isEmpty(result)){
                    Toast.makeText(this,"报名人数不能为空",Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
        }
        Intent data = new Intent();
        data.putExtra("result",result);
        setResult(RESULT_OK,data);
        finish();
    }
}
