package co.docy.oldfriends.Activitis;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventRegistSubmitted;
import co.docy.oldfriends.Messages.JsonUpdatePersonInfo;
import co.docy.oldfriends.Messages.JsonUpdatePersonInfoRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import de.greenrobot.event.EventBus;


public class _ActivityRegistPersonInfo extends ActivityBase {

    Button bSend;
    RelativeLayout regist_person_info_avatar_tag;
    ImageView regist_person_info_avatar_image;
    RadioGroup regist_person_info_sex_radiogroup;
    RadioButton regist_person_info_sex_man, regist_person_info_sex_woman;

    File tempAttach;
    File randomAttach;

    boolean waitting_server = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist_person_info);


        Intent intent = getIntent();
        if (intent == null) {
            finish();
        }

        bSend = (Button) findViewById(R.id.regist_person_info_send);
        bSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!waitting_server) {
                    waitting_server = true;

                    updateUserAvatarAndSex();

                }
            }
        });

        regist_person_info_avatar_tag = (RelativeLayout) findViewById(R.id.regist_person_info_avatar_tag);
        regist_person_info_avatar_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                intent.setType(MyConfig.MIME_IMAGE_STAR);

                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, MyConfig.REQUEST_GALLERY_AVATAR);
                }

            }
        });
        regist_person_info_avatar_image = (ImageView) findViewById(R.id.regist_person_info_avatar_image);

        regist_person_info_sex_radiogroup = (RadioGroup) findViewById(R.id.regist_person_info_sex_radiogroup);
        regist_person_info_sex_man = (RadioButton) findViewById(R.id.regist_person_info_sex_man);
        regist_person_info_sex_woman = (RadioButton) findViewById(R.id.regist_person_info_sex_woman);


        if (MyConfig.usr_sex == MyConfig.USER_SEX_MAN) {//true = 男
            regist_person_info_sex_man.setChecked(true);
        } else {
            regist_person_info_sex_woman.setChecked(true);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                randomAttach = HttpTools.okhttpGetFile(MyConfig.getApiDomain_NoSlash_GetResources() + "/directrandomlogo?type=user",
                        new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg"));

                if (randomAttach != null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Picasso.with(_ActivityRegistPersonInfo.this).load(randomAttach)
                                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                                    .into(regist_person_info_avatar_image);
                        }
                    });
                }
            }
        }).start();
    }

    public void updateUserAvatarAndSex(){

        if(tempAttach != null) {
            MyConfig.uploadUserAvatar(tempAttach);
        }else{
            MyConfig.uploadUserAvatar(randomAttach);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                final JsonUpdatePersonInfo jsonUpdatePersonInfo = new JsonUpdatePersonInfo();
                jsonUpdatePersonInfo.accessToken = MyConfig.usr_token;
                if(regist_person_info_sex_man.isChecked()) {
                    jsonUpdatePersonInfo.sex = true;
                }else{
                    jsonUpdatePersonInfo.sex = false;
                }

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonUpdatePersonInfo, JsonUpdatePersonInfo.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_UPDATE, jsonStr);

                final JsonUpdatePersonInfoRet jsonUpdatePersonInfoRet = gson.fromJson(s, JsonUpdatePersonInfoRet.class);
                MyLog.d("", "HttpTools: " + s);
                if (jsonUpdatePersonInfoRet != null && jsonUpdatePersonInfoRet.code==MyConfig.retSuccess()) {

                    if (jsonUpdatePersonInfo.sex) {
                        MyConfig.usr_sex = MyConfig.USER_SEX_MAN;//男
                    } else {
                        MyConfig.usr_sex = MyConfig.USER_SEX_WOMAN;//女
                    }
                    MyConfig.prefEditor.putInt(MyConfig.PREF_USER_SEX, MyConfig.usr_sex);
                    MyConfig.prefEditor.commit();

                } else {

                }

            }
        }).start();

        finish();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyLog.d("", "onActivityResult hit ActivityRegistPersonInfo! request code " + requestCode + " result code " + resultCode);
        if (requestCode == MyConfig.REQUEST_GALLERY_AVATAR && resultCode == Activity.RESULT_OK) {

            Intent innerIntent = new Intent("com.android.camera.action.CROP");
            innerIntent.setDataAndType(data.getData(), MyConfig.MIME_IMAGE_STAR);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true");// 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
            if (innerIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);
            }

        }

        if (requestCode == MyConfig.REQUEST_IMAGE_CROP && resultCode == Activity.RESULT_OK) {
            MyLog.d("", "onActivityResult hit ActivityRegistPersonInfo! REQUEST_IMAGE_CROP" + resultCode);

            Bitmap bitmap = data.getParcelableExtra("data");

            tempAttach = new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg");

            // 图像保存到文件中
            FileOutputStream foutput = null;
            try {
                foutput = new FileOutputStream(tempAttach);
                if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, foutput)) {
//                    Toast.makeText(MyConfig.app,
//                            "截图已生成缓存文件，等待上传：" + tempAttach.getAbsolutePath(),
//                            Toast.LENGTH_LONG).show();
                    MyConfig.MyToast(-1, MyConfig.app,
                            getResources().getString(R.string.image_feil_exsit) + tempAttach.getAbsolutePath());
                    Picasso.with(this).load(tempAttach)
                            .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                            .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                            .into(regist_person_info_avatar_image);
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//            MyLog.d("", "onActivityResult hit fragment! uploadUserAvatar() file length " + tempAttach.length());
//            uploadUserAvatar(tempAttach);

        }
//
//        if (requestCode == MyConfig.REQUEST_LOGIN) {
//
//            if (resultCode == Activity.RESULT_OK) {
//                finishWithSuccess();
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//                finishWithFailure();
//            }
//        }

    }


    private void finishWithSuccess() {
        EventBus.getDefault().post(new EventRegistSubmitted());

        setResult(RESULT_OK, new Intent());

        MyConfig.flag_open_invite_code_activity = 1;
        MyLog.d("","regist about: flag_open_invite_code_activity true");

        finish();
    }

    private void finishWithFailure() {
        setResult(RESULT_CANCELED, new Intent());
        finish();
    }


    private void notifyFailure(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Toast.makeText(
//                        ActivityRegistPersonInfo.this,
//                        message,
//                        Toast.LENGTH_LONG).show();
                MyConfig.MyToast(0, _ActivityRegistPersonInfo.this,
                        message);
            }
        });

    }


    private void notifySuccess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Toast.makeText(
//                        ActivityRegistPersonInfo.this,
//                        getString(R.string.prompt_success),
//                        Toast.LENGTH_LONG).show();
                MyConfig.MyToast(0, _ActivityRegistPersonInfo.this,
                        getString(R.string.prompt_success));
            }
        });

    }

}
