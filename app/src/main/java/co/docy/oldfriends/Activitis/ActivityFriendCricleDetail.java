package co.docy.oldfriends.Activitis;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.FriendCricleMsg;
import co.docy.oldfriends.EventBus.EventFriendCricleDelDetail;
import co.docy.oldfriends.Messages.JsonFriendCricleChangeLike;
import co.docy.oldfriends.Messages.JsonFriendCricleDetailRet;
import co.docy.oldfriends.Messages.JsonFriendCricleCommonRet;
import co.docy.oldfriends.Messages.JsonFriendCricleSendComment;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import co.docy.oldfriends.emoji.EmojiFragment;
import co.docy.oldfriends.emoji.EmojiUtils;
import de.greenrobot.event.EventBus;

/**
 * Created by khp on 2016/7/29.
 */
public class ActivityFriendCricleDetail extends ActivityBase {
    ImageView peopel_photo,photo,image_expression,image_play;
    TextView people_name,title,message_time,message_del,comment_num,fav_name,comment_send;
    LinearLayout comment_linear,fav_linear,comment_name,comment_input_linear;
    RelativeLayout image_relative;
    CheckBox fav_num;
    EditText et;
    ScrollView scrollView;
    private FrameLayout fl_emoji_fragment;
    private EmojiFragment _fragment;
    private boolean emojiIsVisible = false;
    int id;
    ArrayList<FriendCricleMsg> likeList = new ArrayList<>();
    ArrayList<FriendCricleMsg> commentList = new ArrayList<>();

    FriendCricleMsg friendsMsg = new FriendCricleMsg();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_cricle_detail);
        id = getIntent().getIntExtra("id",-1);
        initView();
        getDatafromServer();

    }

    public void initView(){
        image_play =(ImageView) findViewById(R.id.activity_friend_cricle_detail_photo_start);
        image_relative =(RelativeLayout) findViewById(R.id.activity_friend_cricle_detail_photo_relative);
        scrollView =(ScrollView) findViewById(R.id.activity_friend_cricle_detail_scrollview);
        comment_linear = (LinearLayout)findViewById(R.id.activity_friend_cricle_detail_comment_linear);
        fav_linear = (LinearLayout)findViewById(R.id.activity_friend_cricle_detail_fav_linear);
        message_time = (TextView)findViewById(R.id.activity_friend_cricle_detail_time);
        message_del = (TextView)findViewById(R.id.activity_friend_cricle_detail_del);
        fav_num = (CheckBox) findViewById(R.id.activity_friend_cricle_detail_fav);
        fav_name =(TextView) findViewById(R.id.activity_friend_cricle_detail_fav_name);
        comment_num = (TextView) findViewById(R.id.activity_friend_cricle_detail_comment);
        comment_name =(LinearLayout) findViewById(R.id.activity_friend_cricle_detail_comment_name);
        peopel_photo = (ImageView) findViewById(R.id.activity_friend_cricle_detail_people_photo);
        people_name = (TextView)findViewById(R.id.activity_friend_cricle_detail_people_name);
        photo = (ImageView) findViewById(R.id.activity_friend_cricle_detail_photo);
        title = (TextView) findViewById(R.id.activity_friend_cricle_detail_title);

        comment_send =(TextView) findViewById(R.id.activity_friend_cricle_detail_comment_send);
        comment_input_linear =(LinearLayout) findViewById(R.id.activity_friend_cricle_detail_comment_layout);
        et =(EditText) findViewById(R.id.activity_friend_cricle_detail_comment_edit);
        image_expression =(ImageView) findViewById(R.id.activity_friend_cricle_detail_comment_image);
        fl_emoji_fragment = (FrameLayout) findViewById(R.id.activity_friend_cricle_detail_fl_emoji_fragment);
        _fragment = (EmojiFragment) getSupportFragmentManager().findFragmentById(R.id.emoji_fragment_main);
        _fragment.setEditTextHolder(et);
        image_expression.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emojiIsVisible){
                    hideEmojiDialog();
                }else {
                    showEmojiDialog();
                }

            }
        });

    }

    public void getDatafromServer(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                HashMap<String,String> map = new HashMap<>();
                map.put("id",id+"");
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CHANGE_FRIEND_LIKE+id ,MyConfig.usr_token);
                MyLog.d("kanghongpu","朋友圈详情"+s);
                final JsonFriendCricleDetailRet jsonFriendCricleDetailRet = gson.fromJson(s,JsonFriendCricleDetailRet.class);
                if(jsonFriendCricleDetailRet!=null&&jsonFriendCricleDetailRet.code==MyConfig.retSuccess()&&jsonFriendCricleDetailRet.data!=null){
                   runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           friendsMsg = FriendCricleMsg.choosedataDetail(jsonFriendCricleDetailRet.data);
                           likeList.addAll(likeList.size(),friendsMsg.likeMsg);
                           commentList.addAll(commentList.size(),friendsMsg.commentMsg);
                           setDataintoView();
                           if(friendsMsg.videoPath!=null){
                               DownloadVideo(friendsMsg);
                           }
                       }
                   });

                }
            }
        }).start();
    }
    public void DownloadVideo(FriendCricleMsg msg){
        //视频下载
        if(msg.videoPath!=null) {
            new MyConfig.DownloadFileFromUrl(
                    new MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo("好友圈", MyConfig.ATTACHMENT_ACTION_DO_NOTHING))
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                            MyConfig.getApiDomain_NoSlash_GetResources() + msg.videoPath,//在服务器的地址
                            msg.videoPath_local);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        comment_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCommentToServer();
            }
        });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();
                fl_emoji_fragment.setVisibility(View.GONE);
                image_expression.setImageResource(R.drawable.emoji_button_3x);
                return false;
            }
        });
        et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fl_emoji_fragment.setVisibility(View.GONE);
                image_expression.setImageResource(R.drawable.emoji_button_3x);
            }
        });

    }

    private void hideEmojiDialog() {
        fl_emoji_fragment.setVisibility(View.GONE);
        image_expression.setImageResource(R.drawable.emoji_button_3x);
        EmojiUtils.show_keyboard_must_call_from_activity(this);
        emojiIsVisible = false;
    }

    /**
     * 显示表情选择框
     */
    private void showEmojiDialog() {
        /**EditText没有获取焦点时点击事件不执行*/
        if(!et.hasFocus()){
            et.setFocusable(true);
            et.requestFocus();
        }
        EmojiUtils.hide_keyboard_must_call_from_activity(this);
        SystemClock.sleep(300);
        fl_emoji_fragment.setVisibility(View.VISIBLE);
        image_expression.setImageResource(R.drawable.button_keyboard);
        emojiIsVisible = true;
    }


    public void setDataintoView(){
        title.setVisibility(View.GONE);
        photo.setVisibility(View.GONE);
        message_del.setVisibility(View.GONE);
        fav_linear.setVisibility(View.GONE);
        comment_linear.setVisibility(View.GONE);
        image_play.setVisibility(View.GONE);
        image_relative.setVisibility(View.GONE);

        people_name.setText(friendsMsg.user_nickName);
        Picasso.with(this).load(MyConfig.getApiDomain_NoSlash_GetResources() + friendsMsg.user_avatar)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(peopel_photo);

        message_time.setText(MyConfig.getTime(friendsMsg.createdAt));
        if(friendsMsg.userId == MyConfig.usr_id){
            message_del.setVisibility(View.VISIBLE);
        }
        if(friendsMsg.title!=null&&friendsMsg.title.length()>0) {
            title.setVisibility(View.VISIBLE);
            title.setText(friendsMsg.title);
        }
        //根据数据的格式来判断显示  图片/视频/文字
        if(friendsMsg.image!=null) {
            image_relative.setVisibility(View.VISIBLE);
            if(friendsMsg.videoPath!=null) {
                image_play.setVisibility(View.VISIBLE);
            }
            photo.setVisibility(View.VISIBLE);
            Glide.with(this).load(MyConfig.getApiDomain_NoSlash_GetResources() + friendsMsg.image)
                    .centerCrop()
                    .into(photo);
        }
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(friendsMsg.videoPath!=null){
                    friendsMsg.videoProgress = MyConfig.getFileDownloadProgress(friendsMsg.videoPath_local, friendsMsg.videosize);

                    if(friendsMsg.videoProgress == 100){
                        MyLog.d("kanghongpu",friendsMsg.videoPath_local+111);
                        Intent intent = new Intent(ActivityFriendCricleDetail.this, ActivityVideoPlayer.class);
                        intent.putExtra("mode", 1);
                        intent.putExtra("path", friendsMsg.videoPath_local);
                        intent.putExtra("recorder_os",friendsMsg.os);
                        startActivity(intent);
                    }else {
                        MyLog.d("kanghongpu",friendsMsg.videoPath_local+222);
                        Intent intent = new Intent(ActivityFriendCricleDetail.this, ActivityVideoPlayer.class);
                        intent.putExtra("mode",0);
                        intent.putExtra("path", MyConfig.getApiDomain_NoSlash_GetResources() + friendsMsg.videoPath);
                        intent.putExtra("recorder_os",friendsMsg.os);
                        startActivity(intent);
                    }
                }else {
                    Intent intent = new Intent(ActivityFriendCricleDetail.this, ActivityGallery.class);
                    intent.putExtra("type", 10);
                    if(friendsMsg.fullPath!=null) {
                        intent.putExtra("url", MyConfig.getApiDomain_NoSlash_GetResources() + friendsMsg.fullPath);
                    }else{
                        intent.putExtra("url", MyConfig.getApiDomain_NoSlash_GetResources() + friendsMsg.image);
                    }
                    startActivity(intent);
                }
            }
        });
        //显示点赞的信息
        if(likeList == null||likeList.size()==0){
            fav_num.setText("");
            fav_num.setChecked(false);
        }
        if(likeList!=null&&likeList.size()>0){
            fav_num.setChecked(friendsMsg.liked);
            fav_num.setText(likeList.size()+"");
            fav_linear.setVisibility(View.VISIBLE);
            StringBuilder builder = new StringBuilder();
            for(int i=0;i<likeList.size();i++){
                builder.append(likeList.get(i).like_nickName+"\r");
            }
            fav_name.setText(builder.toString());
        }
        //显示评论的信息
        comment_name.removeAllViews();
        if(commentList == null||commentList.size()==0){
            comment_num.setText("");
        }
        if (commentList!=null&&commentList.size()>0){
            comment_num.setText(commentList.size()+"");
            comment_linear.setVisibility(View.VISIBLE);
//            Collections.reverse(commentList);
            for(int i=commentList.size()-1;i>=0;i--) {
                TextView text = new TextView(this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(0, 10, 0, 0);
                MyConfig.addForeColorSpan(commentList.get(i).comment_nickName+" : "+commentList.get(i).comment,
                        this.getResources().getColor(R.color.xjt_subject),
                        (commentList.get(i).comment_nickName+" : "+commentList.get(i).comment).indexOf(commentList.get(i).comment_nickName),
                        (commentList.get(i).comment_nickName+" : "+commentList.get(i).comment).indexOf(commentList.get(i).comment_nickName) + (commentList.get(i).comment_nickName).length(),
                        text);
                text.setLayoutParams(params);
                comment_name.addView(text);
            }
        }
        //点击点赞按钮
        fav_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(friendsMsg.liked){
                    changeLike(true);
                }else{
                    changeLike(false);
                }

            }
        });
        //点击评论的按钮
        comment_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendComment();
            }
        });

        //删除自己发的朋友圈
        message_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delFriendOfCricleMsg();
            }
        });


    }


    public void sendComment(){
//        弹出键盘
//        comment_input_linear.setVisibility(View.VISIBLE);
//        et.setFocusable(true);
//        et.requestFocus();
        MyConfig.show_keyboard_must_call_from_activity(this);

    }
    public void sendCommentToServer(){
        final String str = et.getText().toString();
        if(str!=null&&str.length()>0) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Gson gson = new Gson();
                    final JsonFriendCricleSendComment jsonSendFriendsComment = new JsonFriendCricleSendComment();
                    jsonSendFriendsComment.accessToken = MyConfig.usr_token;
                    jsonSendFriendsComment.comment = EmojiUtils.replaceNameByField(ActivityFriendCricleDetail.this,str);;
                    String strJsonChangeFriendLike = gson.toJson(jsonSendFriendsComment, JsonFriendCricleSendComment.class);
                    String s = new HttpTools().httpURLConnPostJson
                            (MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CHANGE_FRIEND_LIKE + friendsMsg.id + MyConfig.API_COMMENT_FRIEND, strJsonChangeFriendLike);
                    MyLog.d("kanghongpu", s);
                    JsonFriendCricleCommonRet jsonSendFriendOfCricleRet = gson.fromJson(s, JsonFriendCricleCommonRet.class);
                    if (jsonSendFriendOfCricleRet != null && jsonSendFriendOfCricleRet.code == MyConfig.retSuccess()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                FriendCricleMsg msg = new FriendCricleMsg();
                                msg.comment_nickName = MyConfig.usr_nickname;
                                msg.comment = str;
                                commentList.add(msg);
                                hideKeyboard();
                                fl_emoji_fragment.setVisibility(View.GONE);
                                setDataintoView();

                            }
                        });
                    }
                }
            }).start();
        }
    }
    public void hideKeyboard(){
        //隐藏键盘
        MyConfig.hide_keyboard_must_call_from_activity(this);
        et.setText("");

    }


    public void delFriendOfCricleMsg(){
        //删除自己发的朋友圈
        new  AlertDialog.Builder(this)
                .setMessage("确定要删除吗？" )
                .setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Gson gson = new Gson();
                                final JsonFriendCricleChangeLike jsonChangeFriendLike = new JsonFriendCricleChangeLike();
                                jsonChangeFriendLike.accessToken = MyConfig.usr_token;
                                String strJsonChangeFriendLike = gson.toJson(jsonChangeFriendLike,JsonFriendCricleChangeLike.class);
                                String s = new HttpTools().httpURLConnPostJson
                                        (MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CHANGE_FRIEND_LIKE +friendsMsg.id+ MyConfig.API_DEL_FRIEND,strJsonChangeFriendLike);
                                MyLog.d("kanghongpu",s);
                                JsonFriendCricleCommonRet jsonSendFriendOfCricleRet = gson.fromJson(s,JsonFriendCricleCommonRet.class);
                                if(jsonSendFriendOfCricleRet!=null&&jsonSendFriendOfCricleRet.code==MyConfig.retSuccess()){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            EventBus.getDefault().post(new EventFriendCricleDelDetail(id));
                                            finish();
                                        }
                                    });
                                }

                            }
                        }).start();
                    }
                })
                .setNegativeButton("否" , null)
                .show();

    }
    public void changeLike(final boolean tag){
        //实现点赞
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                final JsonFriendCricleChangeLike jsonChangeFriendLike = new JsonFriendCricleChangeLike();
                jsonChangeFriendLike.accessToken = MyConfig.usr_token;
                if(tag){
                    jsonChangeFriendLike.unlike = tag;
                }

                String strJsonChangeFriendLike = gson.toJson(jsonChangeFriendLike,JsonFriendCricleChangeLike.class);

                String s = new HttpTools().httpURLConnPostJson
                        (MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CHANGE_FRIEND_LIKE +friendsMsg.id+ MyConfig.API_CHANGE_FRIEND_LIKE2,strJsonChangeFriendLike);
                MyLog.d("kanghongpu",s);
                JsonFriendCricleCommonRet jsonSendFriendOfCricleRet = gson.fromJson(s,JsonFriendCricleCommonRet.class);
                if(jsonSendFriendOfCricleRet!=null&&jsonSendFriendOfCricleRet.code==MyConfig.retSuccess()){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            FriendCricleMsg msg = new FriendCricleMsg();
                            friendsMsg.liked = !friendsMsg.liked;
                            if(tag){
                                for(int i=0;i<likeList.size();i++){
                                    if(likeList.get(i).like_nickName.equals(MyConfig.usr_nickname)){
                                        likeList.remove(i);
                                    }
                                }
                            }else{
                                msg.like_nickName = MyConfig.usr_nickname;
                                likeList.add(msg);
                            }
                            setDataintoView();

                        }
                    });
                }

            }
        }).start();
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
