package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;

import java.io.File;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.R;


public class ActivityCreateSubject extends ActivityBase {

    private TextInputLayout create_subject_title_et_layout;
    EditText create_subject_title_et, create_subject_body_et;
    ImageView create_subject_attached_view;

    File attached;//注意文件模式时attached为空
    String title;
    String body;
    int attachedType;//可能：文本、图片、文件等
    int share_in_category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_subject);

        Intent intent = getIntent();
        String filePath = intent.getStringExtra(MyConfig.CONST_STRING_PARAM_ATTACHED);
        MyLog.d("","fixbug---get file crash: filePath " + filePath);
        if (filePath != null) {
            attached = new File(filePath);
        }
        title = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_TITLE);
        body = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_BODY);
        attachedType = intent.getIntExtra(MyConfig.SUBJECT_ATTACHED_TYPE, -1);
        share_in_category = intent.getIntExtra(MyConfig.CONST_PARAM_STR_SHARE_IN_CATEGORY,  MyConfig.SHARE_IN_CATEGORY_NOT_SHARE_IN);
        setTitle(MyConfig.CREATE_SUBJECT_TITLE);//设置窗口的title。这里最好通过intent传递过来，而不是用全局变量来传递
        init();
    }

    private void init() {

        create_subject_title_et_layout = (TextInputLayout) findViewById(R.id.create_subject_title_et_layout);

        create_subject_title_et = (EditText) findViewById(R.id.create_subject_title_et);
        create_subject_title_et.setText(title);

        create_subject_title_et.addTextChangedListener(new MyTextWatcher(create_subject_title_et));

        create_subject_body_et = (EditText) findViewById(R.id.create_subject_body_et);
        create_subject_body_et.setText(body);
        create_subject_attached_view = (ImageView) findViewById(R.id.create_subject_attached_view);

        MyLog.d("","httptools video attachedType " + attachedType);

        if (attached != null) {
            if (attachedType == MyConfig.SUBJECT_ATTACHED_FILE) {
                create_subject_attached_view.setImageResource(R.drawable.ext_file);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                create_subject_attached_view.setLayoutParams(params);
            }else if(attachedType == MyConfig.SUBJECT_ATTACHED_VIDEO){

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(MyConfig.getWidthByScreenPercent(50), MyConfig.getHeightByScreenPercent(50));
                create_subject_attached_view.setLayoutParams(params);

                Glide.with(this).load(Uri.fromFile(attached)).signature(new StringSignature(""+attached.hashCode()+attached.length())).into(create_subject_attached_view);
            }else {
                MyConfig.imageViewShowWithLimit(create_subject_attached_view, attached, MyConfig.getWidthByScreenPercent(70));
            }
        }

        /**
         * 20160315
         * 最新的修改是，如果带有附件，则取消正文
         * 20160505
         *  最新的修改是，都没有正文
         */
//        if(attached != null){

            findViewById(R.id.create_subject_body).setVisibility(View.GONE);
            findViewById(R.id.create_subject_seperate).setVisibility(View.GONE);
            findViewById(R.id.create_subject_title_tag).setVisibility(View.GONE);

//        }

    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.create_subject_title_et:
                    validateTitle();
                    break;

            }
        }
    }

    final static int TITLE_LENGTH = 255;//这是服务器确定的长度，超过则服务器返回出错
    private boolean validateTitle() {

        StringBuilder sb = new StringBuilder(create_subject_title_et.getText());

        if (sb.length() > TITLE_LENGTH) {
            create_subject_title_et.setText(sb.substring(0,TITLE_LENGTH));
            create_subject_title_et_layout.setError("最长"+TITLE_LENGTH+"字");
//            requestFocus(create_subject_title_et);
            create_subject_title_et.setSelection(TITLE_LENGTH);
            return false;
        } else {
            create_subject_title_et_layout.setError("您还可以输入"+(TITLE_LENGTH-sb.length())+"字");
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void backWithoutResult() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);

//        MyConfig.cleanAndFinishActivity(this);
        finish();
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        backWithoutResult();
    }


    private void backWithResult() {
        /**
         * 这里通过intent将用户生成的标题、正文、附件传回
         */

        checkSubjectTitle();

        Intent intent = new Intent();
        if(attached != null) {
            intent.putExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_ATTACHED, attached.getAbsolutePath());
        }
        intent.putExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_TITLE, title);
        intent.putExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_BODY, create_subject_body_et.getText().toString().trim());
        intent.putExtra(MyConfig.CONST_PARAM_STR_SHARE_IN_CATEGORY, share_in_category);

        setResult(RESULT_OK, intent);

//        MyConfig.cleanAndFinishActivity(this);
        finish();
    }

    private void checkSubjectTitle() {
        /**
         * 去掉头尾的空白
         * 如果长度为0，添加缺省的标题
         */
        title = create_subject_title_et.getText().toString().trim();
        if (title.length() == 0) {

            /**
             * 如果是文件、地图、图片可以设置缺省标题
             */

            title = generateTitleByType(attachedType);

        }
        create_subject_title_et.setText(title);
    }

    private String generateTitleByType(int attachedType) {
        switch (attachedType){
            case MyConfig.SUBJECT_ATTACHED_FILE:
//                return attached.getName();
                return getString(R.string.menu_plus_file);
            case MyConfig.SUBJECT_ATTACHED_IMAGE:
//                return attached.getName();
                return getString(R.string.menu_plus_gallery);
            case MyConfig.SUBJECT_ATTACHED_MAP:
                return getString(R.string.menu_plus_map);
        }
        return getString(R.string.menu_plus_subject);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_subject_create, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                Toast.makeText(this, "home key is pressed, input is discarded", Toast.LENGTH_LONG).show();

                backWithoutResult();
                return true;
            case R.id.create_subject_submit:

                backWithResult();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
