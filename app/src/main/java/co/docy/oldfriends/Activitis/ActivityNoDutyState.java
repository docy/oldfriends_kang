package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import co.docy.oldfriends.EventBus.EventRegistSuccess;
import co.docy.oldfriends.R;

public class ActivityNoDutyState extends ActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_duty_state);
        Button no_duty_state_agree = (Button) findViewById(R.id.no_duty_state_agree);
        no_duty_state_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_regist = new Intent(ActivityNoDutyState.this, ActivityRegistTongChuang.class);
                startActivity(intent_regist);
            }
        });
    }

    /**
     * 注册成功之后关闭免责页面
     * @param eventRegistSuccess
     */
    public void onEventMainThread(EventRegistSuccess eventRegistSuccess) {
        finish();
    }
}
