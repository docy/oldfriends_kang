package co.docy.oldfriends.Activitis;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

import co.docy.oldfriends.Adapters.ListAdapterCreateContactMultiChoose;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.R;

public class ActivityCreateGroupPeople extends ActivityBase {

    private ListView mListView;
    private ListAdapterCreateContactMultiChoose listAdapterContactMultiChoose;
    private String category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group_people);
        /**这是我的通讯录中的数据，就显示这个*/
        ArrayList<ContactUniversal> contactUniversals = MyConfig.contactUniversals;

        category = getIntent().getStringExtra(MyConfig.CREATE_GROUP_PEOPLE_CATEGORY);
        ArrayList<ContactUniversal> showContactUniversals = new ArrayList<>();
        switch (category){
            case MyConfig.CREATE_GROUP_PEOPLE_CATEGORY_INVITE:
                for (ContactUniversal contactUniversal:contactUniversals){
                    if(!contactUniversal.selected) {
                        showContactUniversals.add(contactUniversal);
                    }
                }
                break;
            case MyConfig.CREATE_GROUP_PEOPLE_CATEGORY_KICK:
                for (ContactUniversal contactUniversal:contactUniversals){
                    if(contactUniversal.selected) {
                        showContactUniversals.add(contactUniversal);

                    }
                }
                setTitle("删除成员");
                break;

            case MyConfig.CREATE_GROUP_PEOPLE_CATEGORY_MORE:
                for (ContactUniversal contactUniversal:contactUniversals){
                    if(contactUniversal.selected) {
                        showContactUniversals.add(contactUniversal);

                    }
                }
                setTitle("小组成员");
                break;
        }


        mListView = (ListView) findViewById(R.id.list_view);

        listAdapterContactMultiChoose = new ListAdapterCreateContactMultiChoose(this, showContactUniversals,category);
        mListView.setAdapter(listAdapterContactMultiChoose);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        switch (category){
            case MyConfig.CREATE_GROUP_PEOPLE_CATEGORY_INVITE:
            case MyConfig.CREATE_GROUP_PEOPLE_CATEGORY_KICK:
                getMenuInflater().inflate(R.menu.menu_group_create, menu);
                break;
        }

       return super.onCreateOptionsMenu(menu);

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.create_group_submit:
                    setResult(Activity.RESULT_OK);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
