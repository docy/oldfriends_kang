package co.docy.oldfriends.Activitis;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.LinkedList;

import co.docy.oldfriends.Adapters.ListAdapterCoursewareDownload;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.EventBus.EventFileDownloadProgress;
import co.docy.oldfriends.Messages.JsonGetClassDetailRet;
import co.docy.oldfriends.R;

public class ActivityCoursewareDownLoad extends ActivityBase {

    private ListView lv_courseware_download;
    private LinkedList<JsonGetClassDetailRet.ClassDetailFilesInfo> files;
    private JsonGetClassDetailRet.ClassDetailFilesInfo filesInfo;
    private ProgressDialog progressDialog;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courseware_download);
        lv_courseware_download = (ListView) findViewById(R.id.lv_courseware_download);

        files = MyConfig.classDetailRetRet.data.files;

        if(files != null && files.size() > 0){
            lv_courseware_download.setAdapter(new ListAdapterCoursewareDownload(this, files));

            lv_courseware_download.setOnItemClickListener(itemClickListener);
        }else {
            showHintDialog();
        }

    }
    /**
     * 若没有课件则弹出一个对话框，给予提示
     */
    private void showHintDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCoursewareDownLoad.this);
        // builder.setTitle("是否呼叫下方联系人:");
       View view = View.inflate(ActivityCoursewareDownLoad.this, R.layout.dialog_hint_phone, null);
        ((TextView)view.findViewById(R.id.tv_dialog_phone)).setText("温馨提示");
        ((TextView)view.findViewById(R.id.tv_dialog_name)).setText("暂时没有课件");

        builder.setView(view);
       // builder.setMessage("暂无课件");

        builder.setPositiveButton("确定", null);

        builder.show();

    }




    /**条目点击事件**/
    private AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            filesInfo = files.get(position);
            filesInfo.filePath_inServer = filesInfo.path;

            /**得到文件保存到本地的路径*/
            if (filesInfo.hashCode != null) {
                filesInfo.filePath_inLocal = MyConfig.getLocalFileByHashcodeAndFileName(filesInfo.hashCode, filesInfo.name).getAbsolutePath();
            } else {
                filesInfo.filePath_inLocal = MyConfig.getLocalFileByUrlAndFileName(filesInfo.filePath_inServer, filesInfo.name).getAbsolutePath();
            }

            /**把文件保存到本地*/
            new MyConfig.DownloadFileFromUrl(new MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo(ActivityCoursewareDownLoad.class.getName(), MyConfig.ATTACHMENT_ACTION_DO_NOTHING)).execute(
                    MyConfig.getApiDomain_NoSlash_GetResources() + filesInfo.filePath_inServer,//在服务器的地址
                    filesInfo.filePath_inLocal);//本地地址
            showProgressDialog();

        }
    };

    /**
     * 显示进度对话框
     */
    private void showProgressDialog() {
        pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pd.setTitle("正在下载......");
        pd.setMax(100);

        /**设置禁用返回键*/
        pd.setCancelable(false);
        /**当触摸对话框以外的区域pd不消失*/
        pd.setCanceledOnTouchOutside(false);

    }


    /**
     * 显示下载进度
     * @param eventFileDownloadProgress
     */
    public void onEventMainThread(EventFileDownloadProgress eventFileDownloadProgress) {

        if (eventFileDownloadProgress.downloadFileFromUrlSenderInfo.sender.equals(ActivityCoursewareDownLoad.class.getName()) && eventFileDownloadProgress.progress!=0 ) {


            pd.setProgress(eventFileDownloadProgress.progress);
            if(eventFileDownloadProgress.progress == 100){
                if(pd!=null) {
                    pd.dismiss();
                    pd = null;
                    openFileChooser(MyConfig.MIME_FILE_STAR);
                }
            }

            /**这个代码一定要写在这个位置，因为若是下载完毕就不显示pd了*/
            if(pd!=null) {
                pd.show();
            }
        }

    }


    /**
     * 打开文件
     *
     * ？？注意打开文件时传递的参数是什么？
     * @param mime
     */
    private void openFileChooser(String mime) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setType(mime);
        if (mime.equals(MyConfig.MIME_IMAGE_STAR)) {
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, MyConfig.REQUEST_GALLERY);
            }
        } else if (mime.equals(MyConfig.MIME_FILE_STAR)) {
            if (intent.resolveActivity(getPackageManager()) != null) {
                // MyLog.d("", "fixbug---get file crash: before get file");
                //startActivityForResult(intent, MyConfig.REQUEST_FILE);

                intent = new Intent("android.intent.action.VIEW");
                intent.addCategory("android.intent.category.DEFAULT");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Uri uri = Uri.fromFile(new File(filesInfo.filePath_inLocal));
                intent.setDataAndType(uri, filesInfo.type);
                startActivityForResult(intent, MyConfig.REQUEST_FILE);
            }
        }
    }
}
