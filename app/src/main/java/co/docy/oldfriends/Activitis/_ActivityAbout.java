package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.widget.TextView;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.R;

/**
 * Created by wuxue on 15/12/1.
 */
public class _ActivityAbout extends ActivityBase {

    TextView about_version_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_layout);
        setTitle(getString(R.string.about));
        about_version_name = (TextView)findViewById(R.id.about_version_name);
        about_version_name.setText(getResources().getString(R.string.version) + MyConfig.appVersion_from_local.versionName);
    }

}
