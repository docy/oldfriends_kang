package co.docy.oldfriends.Activitis;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.DevicePhoneContact;
import co.docy.oldfriends.EventBus.EventHaveSendInviteCode;
import co.docy.oldfriends.Fragments._FragmentPhoneContact;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;


public class _ActivityInviteFriendFromPhoneContact extends ActivityBase {

    String invite_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friend);

        //EventBus.getDefault().register(this);

        Intent intent = getIntent();
        invite_code = intent.getStringExtra("invite_code");


        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        _FragmentPhoneContact fragmentPhoneContact = _FragmentPhoneContact.newInstance(new _FragmentPhoneContact.OnClickedListener() {
            @Override
            public void OnClicked(DevicePhoneContact deviceContactNamePhone) {
                MyLog.d("", "invite friend: device contact list clicked + " + deviceContactNamePhone.name);
                sendInviteCode(deviceContactNamePhone);
            }
        });
        fragmentTransaction.add(R.id.activity_invite_friend_linearLayout, fragmentPhoneContact);
        fragmentTransaction.commit();

        getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.xjt_background));

    }

    private void sendInviteCode(DevicePhoneContact devicePhoneContact){

        MyConfig.MyToast(-1, this, getResources().getString(R.string.send_invite_code_to) + devicePhoneContact.name + " " + devicePhoneContact.phone);

        /**
         * 调用系统短信接口
         */

        Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("sms:"+ devicePhoneContact.phone));
//        Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("sms:13552860811"));
        smsIntent.putExtra("sms_body", getResources().getString(R.string.this_your_invite_code_to)+invite_code);
//        smsIntent.putExtra("sms_body", "小集体测试短信发送");

        startActivity(smsIntent);

        EventBus.getDefault().post(new EventHaveSendInviteCode());

        finish();


    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void onEventMainThread(EventHaveSendInviteCode eventHaveSendInviteCode) {
        finish();
    }
}
