package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;

/**
 * Created by khp on 2016/4/23.
 */
public class ActivityNotificationMessage extends ActivityBase{
    private TextView notification_author,notification_time,notification_reach,notification_body;
    ImageView notification_photo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(MyConfig.scheduleNotification.title);
        setContentView(R.layout.activity_notification_messagen);
        notification_photo =(ImageView) findViewById(R.id.activity_notification_message_photo);
        notification_author =(TextView) findViewById(R.id.activity_notification_message_author);
        notification_time =(TextView) findViewById(R.id.activity_notification_message_time);
        notification_reach =(TextView) findViewById(R.id.activity_notification_message_reach);
        notification_body =(TextView) findViewById(R.id.activity_notification_message_body);


        notification_author.setText(MyConfig.scheduleNotification.author);
        notification_time.setText(MyConfig.Date2StringDateAndTime(MyConfig.UTCString2Date(MyConfig.scheduleNotification.time)));
        notification_body.setText(MyConfig.scheduleNotification.body);
        notification_reach.setText(MyConfig.scheduleNotification.receiver);
        Picasso.with(this).load(MyConfig.getApiDomain_NoSlash_GetResources() + MyConfig.scheduleNotification.senderAvatar)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
//                .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
//                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(notification_photo);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_info,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
