package co.docy.oldfriends.Activitis;


import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventAppQuit;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;


public class _ActivityCompanyView extends ActivityBase {

    Dialog dialog_newbie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_view);
        MyLog.d("", "app start steps: ActivityCompanyView onCreate");
    }

    @Override
    protected void onResume() {
        super.onResume();

        MyLog.d("", "app start steps: ActivityCompanyView onResume");

        newbie_1();

    }

    private void newbie_1() {

        MyLog.d("", "newbie: 1");

//        MyConfig.loadAppOpenCountFromPreference_newbie_1(ActivityCompanyViews.this);
        MyConfig.app_open_count_newbie_guide_1 = MyConfig.loadIntFromPreference(_ActivityCompanyView.this, "app_open_count_newbie_guide_1");

        if (MyConfig.app_open_count_newbie_guide_1 == -1) {
            MyLog.d("", "newbie: 2, count=" + MyConfig.app_open_count_newbie_guide_1);

            MyConfig.app_open_count_newbie_guide_1 = 1;
            MyConfig.saveIntToPreference(_ActivityCompanyView.this, "app_open_count_newbie_guide_1", MyConfig.app_open_count_newbie_guide_1);

            dialog_newbie = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
            ImageView view = new ImageView(this);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog_newbie.dismiss();
                }
            });
            Picasso.with(this).load(R.drawable.newbie_guide_1)
                    .fit()
                    .into(view);
            dialog_newbie.setContentView(view);
            dialog_newbie.show();

            return;
        }
    }

    @Override
    public void onBackPressed() {

      //  if (MyConfig.compnayListSize < 1||MyConfig.jsonGetCurrentCompanyRet == null){
        if (MyConfig.usr_status >= MyConfig.USR_STATUS_HAVE_CURRENT_COMPANY && MyConfig.jsonGetCurrentCompanyRet != null && MyConfig.jsonGetCurrentCompanyRet.code==MyConfig.retSuccess()) {

            super.onBackPressed();

        }else{

            if (backPressed == true) {

//                finish();
                EventBus.getDefault().post(new EventAppQuit());

            } else {

                backPressed = true;
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        backPressed = false;
                    }
                }, 2000);

            }

        }

    }

    private boolean backPressed = false;
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
////            Toast.makeText(this, getResources().getString(R.string.sys_quit_notice), Toast.LENGTH_SHORT).show();
//            MyConfig.MyToast(-1, this, getResources().getString(R.string.sys_quit_notice));
//
//            if (backPressed == true) {
////                finish();
//                EventBus.getDefault().post(new EventAppQuit());
//            } else {
//                backPressed = true;
//                new Handler().postDelayed(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        backPressed = false;
//                    }
//                }, 2000);
//
//            }
//            return false; // 让系统不再处理这次回退键，因为系统收到回退键后会立即把app关了
//        }
//        return super.onKeyDown(keyCode, event);
//    }

}
