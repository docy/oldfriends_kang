package co.docy.oldfriends.Activitis;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.gson.Gson;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.Fragments.FragmentContactMultiChoose;
import co.docy.oldfriends.Messages.JsonGroupDeletePeople;
import co.docy.oldfriends.Messages.JsonGroupDeletePeopleRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by wuxue on 15/11/19.
 */
public class ActivityDeleteUserList extends ActivityBase {
    private FragmentContactMultiChoose fragmentContactMultiChoose;
    private LinkedList<ContactUniversal> list = new LinkedList<>();
    int groupId;

    //  JsonGetGroupUserRet.GetGroupUserRet
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_user_list);
        Intent intent = getIntent();
        groupId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, -1);
        int mode = intent.getIntExtra("mode", -1);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentContactMultiChoose = FragmentContactMultiChoose.newInstance(mode);

        fragmentTransaction.add(R.id.activity_contact_multi_choose_inearLayout, fragmentContactMultiChoose);
        fragmentTransaction.commit();

        MyLog.d("", "ListAdapterContactOfCompanyMultiChoose ActivityContactOfCompanyMultiChoose ");

        fragmentContactMultiChoose.submitListener = new FragmentContactMultiChoose.OnSubmitClicked() {
            @Override
            public void OnClicked(final LinkedList<ContactUniversal> list_universal_selected) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        final JsonGroupDeletePeople jsonGroupDeletePeople = new JsonGroupDeletePeople();
                        jsonGroupDeletePeople.accessToken = MyConfig.usr_token;
                        for (ContactUniversal contactUniversal : list_universal_selected) {
                            jsonGroupDeletePeople.userIds.add(contactUniversal.id);
                        }
                        Gson gson = new Gson();
                        String jsonStr = gson.toJson(jsonGroupDeletePeople, JsonGroupDeletePeople.class);

                        final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_USERS_KICK_GROUP + groupId + MyConfig.API2_GROUP_USERS_KICK_2, jsonStr);
                        MyLog.d("", "JsonGroupDeletePeopleRet address: " + MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_USERS_KICK_GROUP + groupId + MyConfig.API2_GROUP_USERS_KICK_2);
                        final JsonGroupDeletePeopleRet jsonGroupDeletePeopleRet = gson.fromJson(s, JsonGroupDeletePeopleRet.class);
                        MyLog.d("", "JsonGroupDeletePeopleRet : " + s);
                        if (jsonGroupDeletePeopleRet != null && jsonGroupDeletePeopleRet.code==MyConfig.retSuccess()) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            });
                        }

                    }
                }).start();

            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        list.clear();
        if(MyConfig.jsonGetGroupUserRet!=null&&MyConfig.jsonGetGroupUserRet.data!=null) {
            list = ContactUniversal.convertListFromGetGroupUserRet(MyConfig.jsonGetGroupUserRet.data);
            list.remove(0);
            fragmentContactMultiChoose.setList(list);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
