package co.docy.oldfriends.Activitis;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventCloseActivity;
import co.docy.oldfriends.EventBus.EventViewpagerSetCurrentItemPosition;
import co.docy.oldfriends.Messages.JsonCreateCompany;
import co.docy.oldfriends.Messages.JsonCreateCompanyRet;
import co.docy.oldfriends.Messages.JsonSetCurrentCompany;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;


public class _ActivityCreateCompany extends ActivityBase {
    File tempAttach;
    File randomAttach;

    private TextView create_company_name_tag, create_company_tel_tag,
            create_company_position_tag, create_company_location_text, create_company_type_text;
    private EditText create_company_name_text, create_company_tel_text, create_company_position_text, create_company_desc_text;
    private ImageView create_company_tag, create_company_info_avatar_image;
    private CheckBox create_company_inner_switch;
    private Button create_company_send;
    private RelativeLayout create_company_info_avatar_tag, create_company_location, create_company_type;
    public static String mLocation = "";


    int company_category = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_company);

        MyConfig.activityCreateCompany = this;

        create_company_location_text = (TextView) findViewById(R.id.create_company_location_text);
        create_company_location = (RelativeLayout) findViewById(R.id.create_company_location);
        create_company_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(ActivityCreateCompany.this, ActivitySetLoacation.class));

                new DialogFragment() {

                    @Override
                    public void onCreate(Bundle savedInstanceState) {
                        super.onCreate(savedInstanceState);
                    }

                    @Override
                    public void onDestroy() {
                        super.onDestroy();
                    }

                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                getActivity());
                        builder.setTitle("请选择城市")
                                .setItems(R.array.city_list, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // The 'which' argument contains the index position
                                        // of the selected item
                                        create_company_location_text.setText(MyConfig.citylist[which]);
                                    }
                                })
                                .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                        return builder.create();
                    }
                }.show(getFragmentManager(), "");


            }
        });
        create_company_tag = (ImageView) findViewById(R.id.create_company_tag);
        create_company_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                intent.setType(MyConfig.MIME_IMAGE_STAR);
                if (intent.resolveActivity(_ActivityCreateCompany.this.getPackageManager()) != null) {
                    startActivityForResult(intent, MyConfig.REQUEST_GALLERY_AVATAR);
                }
            }
        });
        create_company_desc_text = (EditText) findViewById(R.id.create_company_desc_text);

//        create_company_info_avatar_tag = (RelativeLayout) findViewById(R.id.create_company_info_avatar_tag);
//        create_company_info_avatar_tag.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                intent.addCategory(Intent.CATEGORY_OPENABLE);
//                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
//                intent.setType(MyConfig.MIME_IMAGE_STAR);
//
//                if (intent.resolveActivity(getPackageManager()) != null) {
//                    startActivityForResult(intent, MyConfig.REQUEST_GALLERY_AVATAR);
//                }
//
//            }
//        });
//        create_company_info_avatar_image = (ImageView) findViewById(R.id.create_company_info_avatar_image);

        create_company_name_tag = (TextView) findViewById(R.id.create_company_name_tag);

        create_company_inner_switch = (CheckBox) findViewById(R.id.create_company_inner_switch);

        //  create_company_name_tag.getLayoutParams().width = MyConfig.getWidthByScreenPercent(30);
        create_company_name_text = (EditText) findViewById(R.id.create_company_name_text);
        create_company_tel_tag = (TextView) findViewById(R.id.create_company_tel_tag);
        create_company_tel_tag.getLayoutParams().width = MyConfig.getWidthByScreenPercent(30);
        create_company_tel_text = (EditText) findViewById(R.id.create_company_tel_text);
        create_company_position_tag = (TextView) findViewById(R.id.create_company_position_tag);
        create_company_position_tag.getLayoutParams().width = MyConfig.getWidthByScreenPercent(30);
        create_company_position_text = (EditText) findViewById(R.id.create_company_position_text);

        create_company_send = (Button) findViewById(R.id.create_company_send);
        //  create_company_send.getLayoutParams().width = MyConfig.getWidthByScreenPercent(66);

        create_company_type_text = (TextView)findViewById(R.id.create_company_type_text);
        create_company_type = (RelativeLayout)findViewById(R.id.create_company_type);
        create_company_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DialogFragment() {

                    @Override
                    public void onCreate(Bundle savedInstanceState) {
                        super.onCreate(savedInstanceState);
                    }

                    @Override
                    public void onDestroy() {
                        super.onDestroy();
                    }

                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                getActivity());
                        builder.setTitle(getString(R.string.please_select))
                                .setItems(R.array.category_list, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // The 'which' argument contains the index position
                                        // of the selected item
                                        create_company_type_text.setText(MyConfig.categorylist[which]);
                                        company_category = which;
                                    }
                                })
                                .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                        return builder.create();
                    }
                }.show(getFragmentManager(), "");


            }
        });


        create_company_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                threadToCreateCompany();

            }
        });
        new Thread(new Runnable() {
            @Override
            public void run() {
                randomAttach = HttpTools.okhttpGetFile(MyConfig.getApiDomain_NoSlash_GetResources() + "/directrandomlogo?type=company",
                        new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg"));
                if (randomAttach != null) {
                    MyLog.d("", "create company avatar: randomAttach " + randomAttach.getAbsolutePath());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Picasso.with(_ActivityCreateCompany.this).load(randomAttach)
                                    //.fit()
                                    .resize(MyConfig.getWidthByScreenPercent(100), MyConfig.getPxFromDimen(_ActivityCreateCompany.this, R.dimen.companyviews_height))
                                    .centerCrop()
                                    .into(create_company_tag);
                        }
                    });
                }
            }
        }).start();

//        setLocation();


    }



   /* AMapLocationListener aMapLocationListener = new AMapLocationListener() {
        *//**
         * 此方法已经废弃
         *//*
        @Override
        public void onLocationChanged(Location location) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        *//**
         * 定位成功后回调函数
         *//*
        @Override
        public void onLocationChanged(AMapLocation amapLocation) {
            MyLog.d("", "MyLocation: onLocationChanged");
            if (amapLocation != null
                    && amapLocation.getAMapException().getErrorCode() == 0) {
                final String city = amapLocation.getCity();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        create_company_location_text.setText(city);
                    }
                });

            } else {
                MyLog.d("", "MyLocation ERR:" + amapLocation.getAMapException().getErrorCode());
            }
        }
    };

    *//**
     * AMap和LocationSource通信，LocationSource提供onLocationChangedListener回调界面
     * mAMapLocationManager和地图服务器通信，并向服务器提供aMapLocationListener回调界面
     * 在aMapLocationListener回调里面，从服务器获得的数据传送给onLocationChangedListener回调
     * 由此，服务器的数据传递给练AMap
     *//*
    LocationSource locationSource = new LocationSource() {

        *//**
         * 激活定位
         *//*
        @Override
        public void activate(OnLocationChangedListener onLocationChangedListener) {
            MyLog.d("", "MyLocation: activate");
            if (mAMapLocationManager == null) {
                mAMapLocationManager = LocationManagerProxy.getInstance(_ActivityCreateCompany.this);
            }
            if (mAMapLocationManager != null) {
                // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
                // 注意设置合适的定位时间的间隔（最小间隔支持为2000ms），并且在合适时间调用removeUpdates()方法来取消定位请求
                // 在定位结束后，在合适的生命周期调用destroy()方法
                // 其中如果间隔时间为-1，则定位只定一次
                // 在单次定位情况下，定位无论成功与否，都无需调用removeUpdates()方法移除请求，定位sdk内部会移除
                mAMapLocationManager.requestLocationData(
                        *//**
                         * 参数:
                         provider - 注册监听的 provider 名称。 用户需根据定位需求来设定provider。
                         有三种定位Provider供用户选择，分别是:
                         LocationManagerProxy.GPS_PROVIDER，代表使用手机GPS定位；
                         LocationManagerProxy.NETWORK_PROVIDER，代表使用手机网络定位；
                         LocationProviderProxy.AMapNetwork，代表高德网络定位服务。
                         minTime - 位置变化的通知时间，单位为毫秒，实际时间有可能长于或短于设定值 。
                         参数设置为-1，为单次定位；反之为每隔设定的时间，都会触发定位请求。
                         minDistance - 位置变化通知距离，单位为米。
                         rtm_listener - 监听listener。
                         *//*
                        LocationProviderProxy.AMapNetwork, -1, 1000, aMapLocationListener);
            }
        }

        *//**
         * 停止定位
         *//*
        @Override
        public void deactivate() {
            if (mAMapLocationManager != null) {
                mAMapLocationManager.removeUpdates(aMapLocationListener);
                mAMapLocationManager.destroy();
            }
            mAMapLocationManager = null;
        }

    };

    private AMap aMap;
//    private MapView mapView;
    private LocationManagerProxy mAMapLocationManager;

    public void setLocation(){

        aMap = new MapView(this).getMap();
        aMap.setLocationSource(locationSource);// 设置定位监听
//        aMap.getUiSettings().setMyLocationButtonEnabled(true);// 设置默认定位按钮是否显示
        aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
        // 设置定位的类型为定位模式 ，可以由定位、跟随或地图根据面向方向旋转几种
        aMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);

//        aMap.setOnMapLoadedListener(mOnMapLoadedListener);

        MyLog.d("","MyLocation, in setLocation() ");
    }
*/

    @Override
    protected void onResume() {
        super.onResume();
        create_company_location_text.setText(mLocation);
        mLocation = "";
    }

    private void finishWithSuccess() {
        setResult(RESULT_OK, new Intent());
        finish();
    }

//    private void finishWithFailure(String error) {
//        MyConfig.MyToast(0, this, error);
//        finish();
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_room_info, menu);
        getMenuInflater().inflate(R.menu.menu_create_company, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.create_company:
                threadToCreateCompany();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean waitting_server = false;

    public void threadToCreateCompany() {

        if(waitting_server){
            return;
        }

        waitting_server = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                String name = create_company_name_text.getText().toString();
                String phone = create_company_tel_text.getText().toString();
                String description = create_company_desc_text.getText().toString();
                JsonCreateCompany jsonCreateCompany = new JsonCreateCompany();
                jsonCreateCompany.accessToken = MyConfig.usr_token;
                jsonCreateCompany.name = name;
                jsonCreateCompany.phone = phone;
                jsonCreateCompany.desc = description;
                jsonCreateCompany.category = company_category;
                jsonCreateCompany.isPrivate = create_company_inner_switch.isChecked();
                jsonCreateCompany.city = create_company_location_text.getText().toString();
                if (tempAttach != null) {
                    jsonCreateCompany.logoUrl = tempAttach;
                    MyLog.d("", "create company avatar: 1 tempAttach " + tempAttach.getAbsolutePath());
                } else {
                    jsonCreateCompany.logoUrl = randomAttach;
                    MyLog.d("", "create company avatar: 2 randomAttach " + randomAttach.getAbsolutePath());

                }
                if (company_category < 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MyConfig.MyToast(0, _ActivityCreateCompany.this, getResources().getString(R.string.null_sort_remind));
                            waitting_server = false;
                        }
                    });
                } else {
                    final JsonCreateCompanyRet jsonCreateCompanyRet = new HttpTools().okhttpCreateCompanyWithPhoto(jsonCreateCompany);
                    MyLog.d("", "jsonCreateCompany jsonCreateCompanyRet " + jsonCreateCompany + jsonCreateCompanyRet);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (jsonCreateCompanyRet != null) {
                                if (jsonCreateCompanyRet.code==MyConfig.retSuccess()) {
                                    //设置为当前公司
                                    JsonSetCurrentCompany jsonSetCurrentCompany = new JsonSetCurrentCompany(jsonCreateCompanyRet.data.id);

                                    MyConfig.setCurrentCompany(jsonSetCurrentCompany);

                                    EventBus.getDefault().post(new EventViewpagerSetCurrentItemPosition(0));

                                    MyConfig.eventbusDelayPost(new EventCloseActivity(_ActivityCreateCompany.this.getClass().getName()), 500);


                                } else {
//                                finishWithFailure(jsonCreateCompanyRet.message);
                                    MyConfig.MyToast(0, _ActivityCreateCompany.this, jsonCreateCompanyRet.message);
                                    waitting_server = false;
                                }
                            } else {
                                waitting_server = false;
                            }
                        }
                    });
                }

            }
        }).start();
    }

    public void onEventMainThread(EventCloseActivity eventCloseActivity) {

        MyLog.d("", "EventCloseActivity " + eventCloseActivity.activityName);

        if (eventCloseActivity.activityName.equals(_ActivityCreateCompany.this.getClass().getName())) {
            finishWithSuccess();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyLog.d("", "onActivityResult hit ActivityRegistPersonInfo! request code " + requestCode + " result code " + resultCode);
        if (requestCode == MyConfig.REQUEST_GALLERY_AVATAR && resultCode == Activity.RESULT_OK) {

            Intent innerIntent = new Intent("com.android.camera.action.CROP");
            innerIntent.setDataAndType(data.getData(), MyConfig.MIME_IMAGE_STAR);
            //innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true"); 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
            if (innerIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);
            }

        }

        if (requestCode == MyConfig.REQUEST_IMAGE_CROP && resultCode == Activity.RESULT_OK) {
            MyLog.d("", "onActivityResult hit ActivityRegistPersonInfo! REQUEST_IMAGE_CROP" + resultCode);

            Bitmap bitmap = data.getParcelableExtra("data");

            tempAttach = new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg");

            // 图像保存到文件中
            FileOutputStream foutput = null;
            try {
                foutput = new FileOutputStream(tempAttach);
                if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, foutput)) {
//                    Toast.makeText(MyConfig.app,
//                            "截图已生成缓存文件，等待上传：" + tempAttach.getAbsolutePath(),
//                            Toast.LENGTH_LONG).show();
                    MyConfig.MyToast(-1, MyConfig.app,
                            getResources().getString(R.string.image_feil_exsit) + tempAttach.getAbsolutePath());
                    Picasso.with(this).load(tempAttach)
//                            .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
//                            .transform(new RoundedTransformation(MyConfig.AVATAR_RADIUS, 0))
                            //.fit()
                            .resize(MyConfig.getWidthByScreenPercent(100), MyConfig.getPxFromDimen(_ActivityCreateCompany.this, R.dimen.companyviews_height))
                            .centerCrop()
                            .into(create_company_tag);
                    // .into(create_company_info_avatar_image);
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            MyLog.d("", "onActivityResult hit fragment! uploadAvatar() file length " + tempAttach.length());
//            uploadAvatar(tempAttach);

        }

    }
}
