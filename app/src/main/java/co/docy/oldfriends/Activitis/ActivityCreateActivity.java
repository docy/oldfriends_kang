package co.docy.oldfriends.Activitis;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.DataClass.ActivityTime;
import co.docy.oldfriends.DataClass.MActivity;
import co.docy.oldfriends.R;

public class ActivityCreateActivity extends ActivityBase implements View.OnClickListener {

    private EditText create_activity_title;
    private LinearLayout ll_create_activity_time_limit;
    private EditText create_activity_desc;
    private TextView create_activity_time_limit;
    private int mYear;
    private int mMonth;
    private int mDayOfMonth;
    private int mHour;
    private int mMinute;
    private Dialog mDialog;
    private TextView activity_dialog_time_limit;
    private TextView activity_dialog_cancel;
    private TextView activity_dialog_confirm;
    private DatePicker mDatePicker;
    private TimePicker mTimePicker;
    private CheckBox create_activity_name_open;
    private LinearLayout ll_create_activity_people_max;
    private TextView create_activity_people_max;
    private LinearLayout ll_create_activity_time_start;
    private LinearLayout ll_create_activity_address;
    private TextView create_activity_time_start;
    private TextView create_activity_address;
    private String timeType;
    private int TYPE_PEOPLE_MAX = 1;
    private int TYPE_ADDRESS = 2;
    private int TYPE_COST = 3;
    private TextView choose_time_type;
    private LinearLayout ll_create_activity_time_end;
    private LinearLayout ll_create_activity_cost;
    private TextView create_activity_time_end;
    private TextView create_activity_cost;
    /**由于报名的时间在本地显示的样式与上传到服务器上的样式不同，所以定义一个类，有两个字段，一个表示本地显示，一个表示上传到服务器*/
    private ActivityTime activityStartTime;
    private ActivityTime activityEndTime;
    private ActivityTime activityTimeDeadline;
    double longitude;//经度
    double latitude;//纬度

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_activity);
        activityStartTime = new ActivityTime();
        activityEndTime = new ActivityTime();
        activityTimeDeadline = new ActivityTime();

        initView();
        initListener();
        initCurrentDate();

    }

    /**
     * 初始化控件
     */
    private void initView() {
        /**下面是容器控件*/
        ll_create_activity_time_start = (LinearLayout) findViewById(R.id.ll_create_activity_time_start);
        ll_create_activity_time_end = (LinearLayout) findViewById(R.id.ll_create_activity_time_end);
        ll_create_activity_people_max = (LinearLayout) findViewById(R.id.ll_create_activity_people_max);
        ll_create_activity_address = (LinearLayout) findViewById(R.id.ll_create_activity_address);
        ll_create_activity_cost = (LinearLayout) findViewById(R.id.ll_create_activity_cost);
        ll_create_activity_time_limit = (LinearLayout) findViewById(R.id.ll_create_activity_time_limit);

        /**下面是子控件*/
        create_activity_desc = (EditText) findViewById(R.id.create_activity_desc);
        create_activity_time_start = (TextView) findViewById(R.id.create_activity_time_start);
        create_activity_time_end = (TextView) findViewById(R.id.create_activity_time_end);
        create_activity_people_max = (TextView) findViewById(R.id.create_activity_people_max);
        create_activity_address = (TextView) findViewById(R.id.create_activity_address);
        create_activity_cost = (TextView) findViewById(R.id.create_activity_cost);
        create_activity_time_limit = (TextView) findViewById(R.id.create_activity_time_limit);
    }

    /**
     * 初始化监听器
     */
    private void initListener() {
        ll_create_activity_time_start.setOnClickListener(this);
        ll_create_activity_time_end.setOnClickListener(this);
        ll_create_activity_people_max.setOnClickListener(this);
        ll_create_activity_address.setOnClickListener(this);
        ll_create_activity_cost.setOnClickListener(this);
        ll_create_activity_time_limit.setOnClickListener(this);
    }

    /**
     * 初始化当前时间，即页面刚进来时报名截止时间显示当前时间
     */
    private void initCurrentDate() {
        // 获取当前的年、月、日、小时、分钟
        Calendar c = Calendar.getInstance();

        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDayOfMonth = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        activityStartTime.setTime(mYear, mMonth, mDayOfMonth,mHour, mMinute);
        activityEndTime.setTime(mYear, mMonth, mDayOfMonth,mHour, mMinute);
        activityTimeDeadline.setTime(mYear, mMonth, mDayOfMonth, mHour, mMinute);
        create_activity_time_limit.setText(activityStartTime.getLocalTime());
        create_activity_time_start.setText(activityEndTime.getLocalTime());
        create_activity_time_end.setText(activityTimeDeadline.getLocalTime());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_create_activity_time_start:
                timeType = "timeStart";
                chooseLimitTime();
                break;
            case R.id.ll_create_activity_time_end:
                timeType = "timeEnd";
                chooseLimitTime();
                break;
            case R.id.ll_create_activity_people_max:
                Intent peopleMaxIntent = new Intent(this, ActivityActivityInputPage.class);
                peopleMaxIntent.putExtra("type", "people_max");

                startActivityForResult(peopleMaxIntent, TYPE_PEOPLE_MAX);
                break;
            case R.id.ll_create_activity_address:
                /*Intent addressIntent = new Intent(this, ActivityActivityInputPage.class);
                addressIntent.putExtra("type", "address");*/
                Intent addressIntent = new Intent(this, ActivityMyLocation.class);/**更改为从地图上获取点*/

                startActivityForResult(addressIntent, TYPE_ADDRESS);
                break;
            case R.id.ll_create_activity_cost:
                Intent costIntent = new Intent(this, ActivityActivityInputPage.class);
                costIntent.putExtra("type", "cost");
                startActivityForResult(costIntent, TYPE_COST);
                break;
            case R.id.ll_create_activity_time_limit:
                timeType = "timeLimit";
                chooseLimitTime();
                break;
            case R.id.activity_dialog_cancel:
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                break;
            case R.id.activity_dialog_confirm:
                setStartAndLImitTime();

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TYPE_PEOPLE_MAX && resultCode == RESULT_OK) {
            create_activity_people_max.setText(data.getStringExtra("result"));
        } else if (requestCode == TYPE_ADDRESS && resultCode == RESULT_OK) {
            create_activity_address.setText(MyConfig.tempMapLocation.address);
            longitude = MyConfig.tempMapLocation.longitude;
            latitude = MyConfig.tempMapLocation.latitude;
        }else if (requestCode == TYPE_COST && resultCode == RESULT_OK) {
            create_activity_cost.setText(data.getStringExtra("result"));
        }
    }

    /**
     * 点击dialog的确定按钮后设定开始时间或者截止时间
     */
    private void setStartAndLImitTime() {
        int mYear = mDatePicker.getYear();
        int mMonth = mDatePicker.getMonth();
        int mDayOfMonth = mDatePicker.getDayOfMonth();

        int mMinute = mTimePicker.getCurrentMinute();
        int mHour = mTimePicker.getCurrentHour();

        String serverDateString = MyConfig.getServerDateString(mYear, mMonth, mDayOfMonth, mHour, mMinute);
        Date date = MyConfig.string_yyyy_MM_dd_HH_mm2Date(serverDateString);
        if(date.getTime()<System.currentTimeMillis()-1000*60){
            Toast.makeText(this,"您选择的时间早于当前时间,请重新选择!",Toast.LENGTH_SHORT).show();
            return;
        }
        switch (timeType) {
            case "timeStart":
                activityStartTime.setTime(mYear, mMonth, mDayOfMonth,mHour, mMinute);
                create_activity_time_start.setText(activityStartTime.getLocalTime());
                break;
            case "timeEnd":
                activityEndTime.setTime(mYear, mMonth, mDayOfMonth,mHour, mMinute);
                create_activity_time_end.setText(activityEndTime.getLocalTime());
                break;
            case "timeLimit":
                activityTimeDeadline.setTime(mYear, mMonth, mDayOfMonth,mHour, mMinute);
                create_activity_time_limit.setText(activityTimeDeadline.getLocalTime());
                break;
        }
        if (mDialog != null) {
            mDialog.dismiss();
        }

    }

    /**
     * 弹出对话框，选择截止时间
     */
    private void chooseLimitTime() {
        mDialog = new Dialog(this) {
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                requestWindowFeature(Window.FEATURE_NO_TITLE);

                setContentView(getChooseTimeView());
                Window window = this.getWindow();
                WindowManager.LayoutParams params = window.getAttributes();
                params.gravity = Gravity.BOTTOM;
                params.width = window.getWindowManager().getDefaultDisplay().getWidth();
                params.verticalMargin = 0;
                params.horizontalMargin = 0;

                window.setAttributes(params);
            }
        };

        mDialog.show();
    }

    /**
     * 得到dialog的view对象，并实现dialog上控件的初始化
     *
     * @return
     */
    private View getChooseTimeView() {
        View view = View.inflate(this, R.layout.dialog_activity_choice_time, null);
        mDatePicker = (DatePicker) view.findViewById(R.id.choose_time_datePicker);
        mTimePicker = (TimePicker) view.findViewById(R.id.choose_time_TimePicker);

        choose_time_type = (TextView) view.findViewById(R.id.choose_time_type);
        switch (timeType) {
            case "timeStart":
                choose_time_type.setText("活动开始时间");
                break;
            case "timeEnd":
                choose_time_type.setText("活动结束时间");
                break;
            case "timeLimit":
                choose_time_type.setText("报名截止时间");
                break;
        }

        activity_dialog_time_limit = (TextView) view.findViewById(R.id.activity_dialog_time_limit);
        activity_dialog_time_limit.setText(MyConfig.getLocalDateStringWithWeek(mYear, mMonth, mDayOfMonth,mHour, mMinute));

        activity_dialog_cancel = (TextView) view.findViewById(R.id.activity_dialog_cancel);
        activity_dialog_confirm = (TextView) view.findViewById(R.id.activity_dialog_confirm);
        /**设置为24小时制*/
        mTimePicker.setIs24HourView(true);
        activity_dialog_cancel.setOnClickListener(this);
        activity_dialog_confirm.setOnClickListener(this);

        /**隐藏年份*/
        ((ViewGroup) (((ViewGroup) mDatePicker.getChildAt(0)).getChildAt(0)))
                .getChildAt(0).setVisibility(View.GONE);

        /**初始化DatePicker组件，初始化时指定监听器*/
        mDatePicker.init(mYear, mMonth, mDayOfMonth, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker arg0, int year, int month,
                                      int day) {

                /**让日期动态显示*/
                activity_dialog_time_limit.setText(MyConfig.getLocalDateStringWithWeek(year, month, day,mTimePicker.getCurrentHour(), mTimePicker.getCurrentMinute()));
            }
        });

        /**初始化TimePicker组件*/
        mTimePicker.setCurrentHour(mHour);
        mTimePicker.setCurrentMinute(mMinute);

        /**为TimePicker指定监听器*/
        mTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker arg0, int hour, int minute) {
                /**让时间动态显示*/
                activity_dialog_time_limit.setText(MyConfig.getLocalDateStringWithWeek(mDatePicker.getYear(), mDatePicker.getMonth(), mDatePicker.getDayOfMonth(),
                        hour, minute));
            }
        });
        return view;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.create_activity_submit:
                //Toast.makeText(this,"发布",Toast.LENGTH_SHORT).show();
                backWithResult();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void backWithResult() {
        String desc = create_activity_desc.getText().toString().trim();
        String time_start = activityStartTime.getServerTime().trim();
        String time_end = activityEndTime.getServerTime().trim();
        String people_max = create_activity_people_max.getText().toString().trim();
        String address = create_activity_address.getText().toString().trim();
        String cost = create_activity_cost.getText().toString().trim();
        String time_limit = activityTimeDeadline.getServerTime().trim();

        if (TextUtils.isEmpty(desc)) {
            MyConfig.MyToast(0, this, "描述不能为空");
            return;
        }
        if (TextUtils.isEmpty(people_max)) {
            MyConfig.MyToast(0, this, "请设置人数上限");
            return;
        }
        if (TextUtils.isEmpty(address)) {
            MyConfig.MyToast(0, this, "请指定活动地点");
            return;
        }
        if (TextUtils.isEmpty(cost)) {
                MyConfig.MyToast(0, this, "请输入活动经费");
                return;
        }

        Date date_start = MyConfig.string_yyyy_MM_dd_HH_mm2Date(time_start);
        if(date_start.getTime()<System.currentTimeMillis()-1000*60){
            Toast.makeText(this,"您选择的开始时间早于当前时间,请重新选择!",Toast.LENGTH_SHORT).show();
            return;
        }

        Date date_end = MyConfig.string_yyyy_MM_dd_HH_mm2Date(time_end);
        if(date_end.getTime()<System.currentTimeMillis()-1000*60){
            Toast.makeText(this,"您选择的开始时间早于当前时间,请重新选择!",Toast.LENGTH_SHORT).show();
            return;
        }

        Date date_limit = MyConfig.string_yyyy_MM_dd_HH_mm2Date(time_limit);
        if(date_limit.getTime()<System.currentTimeMillis()-1000*60){
            Toast.makeText(this,"您选择的报名截止时间早于当前时间,请重新选择!",Toast.LENGTH_SHORT).show();
            return;
        }

        MyConfig.tempActivity = new MActivity();
        MyConfig.tempActivity.desc = desc;
        MyConfig.tempActivity.title = desc;
        MyConfig.tempActivity.startTime = time_start;
        MyConfig.tempActivity.endTime = time_end;

        MyConfig.tempActivity.limit = Integer.parseInt(people_max);
        MyConfig.tempActivity.location = address;
        MyConfig.tempActivity.cost = Float.parseFloat(cost);
        MyConfig.tempActivity.closeTime = time_limit;

        MyConfig.tempActivity.longitude = longitude;
        MyConfig.tempActivity.latitude = latitude;

        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();

    }

    /**
     * 得到当前时间，时间格式为：“2016-03-06 10:30”
     *
     * @return
     */
    private String getCurrentTime() {
        Date today = Calendar.getInstance().getTime();
        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return formatter.format(today);
    }
}
