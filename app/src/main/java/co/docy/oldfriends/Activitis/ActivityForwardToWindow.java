package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.LinkedList;

import co.docy.oldfriends.Adapters.ListAdapterGroups;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.Group;
import co.docy.oldfriends.Messages.JsonForwardTopic;
import co.docy.oldfriends.Messages.JsonHttpRet_Universal;
import co.docy.oldfriends.Messages.JsonUserGroupsRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by wuxue on 15/10/29.
 */
public class ActivityForwardToWindow extends ActivityBase {
    // public LinkedList<JsonUserGroupsRet.UserGroupsRet> list_group = new LinkedList<>();
    public LinkedList<Group> list_group = new LinkedList<>();
    ListAdapterGroups adapter;
    ListView activity_company_group_list;

    int topicId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_group_list);

        activity_company_group_list = (ListView) findViewById(R.id.activity_company_group_list);

        Intent intent = getIntent();
        if(intent == null){
            finish();
        }else{
            topicId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_TOPICID, -1);
            if(topicId == -1){
                finish();
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        useGroupList();
    }

    private void useGroupList() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();
                /**
                 * 获取已经加入的rooms
                 */
                String user_rooms = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_ROOMS, MyConfig.usr_token);
                MyLog.d("", "ActivityForwardToWindow: user rooms " + user_rooms);
                MyConfig.jsonUserGroupsRet = gson.fromJson(user_rooms, JsonUserGroupsRet.class);

                if (MyConfig.jsonUserGroupsRet != null && MyConfig.jsonUserGroupsRet.code==MyConfig.retSuccess()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            list_group.clear();
                            //    list_group = MyConfig.jsonUserGroupsRet.data;
                            for (JsonUserGroupsRet.UserGroupsRet urr : MyConfig.jsonUserGroupsRet.data) {
                                if (
                                        urr.category == MyConfig.SERVER_ROOM_CATEGORY_NORMAL
                                        || urr.category == MyConfig.SERVER_ROOM_CATEGORY_PRIVATE
                                        || urr.category == MyConfig.SERVER_ROOM_CATEGORY_DIRECT
                                        || urr.category == MyConfig.SERVER_ROOM_CATEGORY_CLASS
                                        ) {
                                    list_group.add(Group.fromUserGroupsRet(urr));
                                }
                            }
                            adapter = new ListAdapterGroups(ActivityForwardToWindow.this, list_group, MyConfig.GROUP_LIST_TYPE_GROUP_SIMPLE_INFO, null, null);
                            activity_company_group_list.setAdapter(adapter);
                            activity_company_group_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                    forwardTo(list_group.get(position).createGroupRet.id);

                                }
                            });
                        }
                    });

                }

            }
        }).start();
    }

    private void forwardTo(final int groupId){

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonForwardTopic jsonForwardTopic = new JsonForwardTopic();
                jsonForwardTopic.accessToken = MyConfig.usr_token;
                jsonForwardTopic.groupId = groupId;
                jsonForwardTopic.topicId = topicId;
                jsonForwardTopic.clientId = MyConfig.formClientCheckId(groupId);

                String jsonStr = gson.toJson(jsonForwardTopic, JsonForwardTopic.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_FORWARD_TOPIC + topicId + MyConfig.API2_FORWARD_TOPIC_2, jsonStr);
                MyLog.d("", "ActivityForwardToWindow ret: " + s);

                final JsonHttpRet_Universal jsonHttpRet_universal = gson.fromJson(s, JsonHttpRet_Universal.class);
                if(jsonHttpRet_universal != null && jsonHttpRet_universal.code == MyConfig.retSuccess()){

                    MyLog.d("", "ActivityForwardToWindow success! " + s);

                }else{

                }

                finish();

            }
        }).start();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
        // return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
