package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonResetMessageNotice;
import co.docy.oldfriends.Messages.JsonResetMessageNoticeRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/4/21.
 * 我的设置中新消息接受的Activity
 */
public class ActivitySettingMessage extends ActivityBase implements View.OnClickListener{
    private ImageView image_messgae;
    boolean tag =MyConfig.usr_enableNoti;
    final JsonResetMessageNotice jsonResetMessageNotice = new JsonResetMessageNotice();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_message);
        image_messgae =(ImageView) findViewById(R.id.activity_setting_message_receive);
        image_messgae.setOnClickListener(this);
        if(tag) {
            image_messgae.setImageResource(R.drawable.switch_icon_on);
        }else {
            image_messgae.setImageResource(R.drawable.switch_icon_off);
        }

    }
    public void onClick(View v) {

        if(tag) {
            image_messgae.setImageResource(R.drawable.switch_icon_off);
           initdata_off();
          tag = false;
            
        }else{
            image_messgae.setImageResource(R.drawable.switch_icon_on);
            initdata_on();
          tag = true;
        }
    }
   public void initdata_off(){
       jsonResetMessageNotice.notification=false;
       new MyThread().start();

}
    public void initdata_on(){
        jsonResetMessageNotice.notification=true;
        new MyThread().start();
    }

    class MyThread extends Thread{
        public void run(){
            Gson gson = new Gson();
            jsonResetMessageNotice.accessToken = MyConfig.usr_token;
            String strJsonResetMessageNotice = gson.toJson(jsonResetMessageNotice,JsonResetMessageNotice.class);
            final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_SETNOTIFICATION , strJsonResetMessageNotice);

            final JsonResetMessageNoticeRet jsonResetMessageNoticeRet = gson.fromJson(s, JsonResetMessageNoticeRet.class);
            if (jsonResetMessageNoticeRet != null&&jsonResetMessageNoticeRet.code==MyConfig.retSuccess()) {

                MyConfig.usr_enableNoti = jsonResetMessageNotice.notification;

                MyConfig.prefEditor.putBoolean(MyConfig.PREF_USR_WEIXIN_ENABLE_NOTI, MyConfig.usr_enableNoti);
                MyConfig.prefEditor.commit();

            }
        }
    }
}