package co.docy.oldfriends.Activitis;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.Fragments.FragmentContactMultiChoose;
import co.docy.oldfriends.Messages.JsonGetCompanyUserListRet;
import co.docy.oldfriends.Messages.JsonGroupInvitePeople;
import co.docy.oldfriends.Messages.JsonGroupInvitePeopleRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;


public class ActivityGroupInvitePeople extends ActivityBase {

    int groupId;
    String name;
    String desc;
    int mode;

    FragmentContactMultiChoose fragmentContactMultiChoose;
    LinkedList<ContactUniversal> list_universal_company_all = new LinkedList<>();
    LinkedList<ContactUniversal> list_universal_choose_from = new LinkedList<>();
    private MyConfig.IF_AfterHttpRequest IFAfterHttpRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_invite_people);

        Intent intent = getIntent();
        if (intent != null) {
            groupId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, -1);
            name = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME);
            desc = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC);
            mode = intent.getIntExtra("mode", MyConfig.MODE_CHOOSE_SINGLE);
            MyLog.d("", "groupinfo choose 1: " + mode);
        } else {
            finish();
        }

        init();

//        //EventBus.getDefault().register(this);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentContactMultiChoose = FragmentContactMultiChoose.newInstance(mode);
        fragmentTransaction.add(R.id.activity_contact_multi_choose_inearLayout, fragmentContactMultiChoose);
        fragmentTransaction.commit();

        MyLog.d("", "ListAdapterContactOfCompanyMultiChoose ActivityContactOfCompanyMultiChoose ");

        fragmentContactMultiChoose.submitListener = new FragmentContactMultiChoose.OnSubmitClicked() {
            @Override
            public void OnClicked(final LinkedList<ContactUniversal> list_universal_selected) {


                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        final JsonGroupInvitePeople jsonGroupInvitePeople = new JsonGroupInvitePeople();
                        jsonGroupInvitePeople.accessToken = MyConfig.usr_token;
//                        jsonGroupInvitePeople.groupId = groupId;
                        for (ContactUniversal contactUniversal : list_universal_selected) {
                            jsonGroupInvitePeople.toAdd.add(contactUniversal.id);
                        }
                        Gson gson = new Gson();
                        String jsonStr = gson.toJson(jsonGroupInvitePeople, JsonGroupInvitePeople.class);

                        final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_USERS_INVITE + groupId + MyConfig.API2_GROUP_USERS_INVITE_2, jsonStr);

                        final JsonGroupInvitePeopleRet jsonGroupInvitePeopleRet = gson.fromJson(s, JsonGroupInvitePeopleRet.class);
                        MyLog.d("", "HttpTools: " + s);
                        if (jsonGroupInvitePeopleRet != null && jsonGroupInvitePeopleRet.code==MyConfig.retSuccess()) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            });
                        }

                    }
                }).start();


            }
        };

    }

    @Override
    public void onResume() {
        super.onResume();
        if (MyConfig.usr_status < MyConfig.USR_STATUS_HAVE_CURRENT_COMPANY) {//要先设置当前公司，然后才有公司通讯录的显示
            return;
        }

        if (MyConfig.jsonGetCurrentCompanyRet == null || MyConfig.jsonGetCurrentCompanyRet.data == null) {
            return;
        }
        IFAfterHttpRequest = new MyConfig.IF_AfterHttpRequest() {
            @Override
            public void doAfterHttpRequest() {
                // 先转换为universal格式
                list_universal_company_all.clear();
                for (JsonGetCompanyUserListRet.GetCompanyUserListRet getCompanyUserListRet : MyConfig.jsonGetCompanyUserListRet.data) {
                    list_universal_company_all.add(ContactUniversal.fromJsonGetCompanyUserListRet(getCompanyUserListRet));
                }
                // 去掉bypass列表中的元素
                MyConfig.byPassContactUniversalList_temp = ContactUniversal.convertListFromGetGroupUserRet(MyConfig.jsonGetGroupUserRet.data);
                list_universal_choose_from.clear();
                for (ContactUniversal contactUniversal_company : list_universal_company_all) {
                    boolean found = false;
                    for (ContactUniversal contactUniversal_bypass : MyConfig.byPassContactUniversalList_temp) {
                        if (contactUniversal_company.id == contactUniversal_bypass.id) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        list_universal_choose_from.add(contactUniversal_company);
                    }
                }
                fragmentContactMultiChoose.setList(list_universal_choose_from);
            }
        };
        MyConfig.getCurrentYearUserAddressBooks(MyConfig.jsonGetCurrentCompanyRet.data.id, IFAfterHttpRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
//        //EventBus.getDefault().unregister(this);
    }

    private void init() {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


//    public void onEventMainThread(EventQuitGroup eventQuitGroup) {
//        finish();
//    }
}
