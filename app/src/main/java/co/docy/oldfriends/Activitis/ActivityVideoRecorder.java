package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventHeart200ms;
import co.docy.oldfriends.EventBus.EventRepeatRecordVideo;
import co.docy.oldfriends.EventBus.EventSetSoundRemind;
import co.docy.oldfriends.Media.VideoRecorder;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;

public class ActivityVideoRecorder extends ActivityBase implements View.OnTouchListener, View.OnClickListener {

    private ImageView record_video;
    private SurfaceView mSurfaceView;
    /**自定义视频录制类*/
    private VideoRecorder mVideoRecorder;
    private TextView video_repeat_record;
    private TextView video_comfirm_used;
    private ProgressBar record_video_progressbar;
    private VideoView play_video_view;

    /**是否录制完成，false表示还未录制视频，处于准备录制状态*/
    private boolean isRecorded = false;

    /**是否正在播放，false表示还未播放，处于准备录制状态*/
    private boolean isPlay = false;

    /**打开的是否是后置摄像头，默认是false，第一次打开前置摄像头*/
    private boolean isBackCamera = false;

    private int time = -1;

    /**视频最长录制长度为5秒，因为使用的是200毫秒心跳，所以最大时间为5*5*/
    private int maxTime = 5 * 5;
    private Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_recorder);
        initView();
        initData();
        initListener();
        EventBus.getDefault().post(new EventSetSoundRemind(true));/**打开200ms心跳*/
    }


    private void initView() {
        mSurfaceView = (SurfaceView) findViewById(R.id.my_surface_view);
        record_video = (ImageView) findViewById(R.id.record_video);
        play_video_view = (VideoView) findViewById(R.id.play_video_view);/**支持预览播放的功能，当播放时SurfaceView隐藏，VideoView显示出来*/
        video_repeat_record = (TextView) findViewById(R.id.video_repeat_record);
        video_comfirm_used = (TextView) findViewById(R.id.video_comfirm_used);
        record_video_progressbar = (ProgressBar) findViewById(R.id.record_video_progressbar);
    }

    private void initData() {
        mVideoRecorder = new VideoRecorder(this, mSurfaceView);

        record_video_progressbar.setMax(maxTime);
        record_video_progressbar.setProgress(maxTime);

    }

    private void initListener() {
        record_video.setOnTouchListener(this);

        video_repeat_record.setOnClickListener(this);
        video_comfirm_used.setOnClickListener(this);
        record_video.setOnClickListener(this);

        /**播放完成监听器*/
        play_video_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                record_video.setImageResource(R.drawable.record_video_play_3x);
                isPlay = false;
            }
        });
    }


    /**
     * 开始录制视频
     */
    private void startRecordVideo() {
        record_video.setImageResource(R.drawable.record_video_press_3x);
        time = 0;
        record_video_progressbar.setVisibility(View.VISIBLE);
        mVideoRecorder.startRecordVideo();
        menu.findItem(R.id.switch_camera).setVisible(false);
    }

    /**
     * 完成一次录制
     */
    private void finishRecordVedio() {
        isRecorded = true;
        time = -1;
        record_video.setImageResource(R.drawable.record_video_play_3x);
        record_video_progressbar.setVisibility(View.GONE);
        video_repeat_record.setVisibility(View.VISIBLE);
        video_comfirm_used.setVisibility(View.VISIBLE);


        mVideoRecorder.stopRecordVideo();
    }

    /**
     * 播放视频,每次播放都是从头播放，由于视频较短，暂不支持从暂停位置播放
     */
    public void playVideo() {
        if (play_video_view != null) {
            play_video_view.setVisibility(View.VISIBLE);
            mSurfaceView.setVisibility(View.GONE);
            record_video.setImageResource(R.drawable.record_video_pause_3x);
            isPlay = true;

            play_video_view.setVideoPath(mVideoRecorder.getCurrentVideoPath());
            play_video_view.start();

        }
    }

    /**
     * 暂停视频
     */
    public void pauseVideo() {
        if (play_video_view != null) {
            record_video.setImageResource(R.drawable.record_video_play_3x);
            isPlay = false;

            play_video_view.pause();
        }
    }

    /**
     * 重拍
     */
    public void repeatRecordVideo() {
        mVideoRecorder.startPreview();
        record_video.setImageResource(R.drawable.record_video_nomal_3x);
        isRecorded = false;
        video_repeat_record.setVisibility(View.INVISIBLE);
        video_comfirm_used.setVisibility(View.INVISIBLE);
        menu.findItem(R.id.switch_camera).setVisible(true);
        play_video_view.setVisibility(View.GONE);
        mSurfaceView.setVisibility(View.VISIBLE);
    }

    /**
     * 使用视频
     */
    private void backWithResult() {
        MyConfig.current_video_path = mVideoRecorder.getCurrentVideoPath();

        setResult(RESULT_OK, new Intent());
        finish();
    }


    public void onEventMainThread(EventHeart200ms eventHeart200ms) {
        if (time >= 0) {
            time++;
            if (time >= maxTime) {
                if (!isRecorded) finishRecordVedio();
            }
            record_video_progressbar.setProgress(maxTime - time);
            MyLog.d("kwwl", "录制视频的进度===================" + time);
        }

    }

    /**
     * 当视频录制时间小于1秒时会出现crash，当出现crash时发送event事件，自动调用“重拍”方法
     *
     * @param eventRepeatRecordVideo
     */
    public void onEventMainThread(EventRepeatRecordVideo eventRepeatRecordVideo) {
        Log.d("kwwl", "mMediaRecorder.stop()时crash了");
        repeatRecordVideo();
        Toast.makeText(this, "请长按重拍", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (isRecorded) return false;/**若录制完成则不执行onTouch事件，返回值必须是false，执行onClick事件*/
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startRecordVideo();
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (isRecorded) return false;
                finishRecordVedio();
                break;
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.video_repeat_record:/**重拍*/
                repeatRecordVideo();
                break;
            case R.id.video_comfirm_used:
                backWithResult();
                break;
            case R.id.record_video:
                if(isRecorded){
                    if(isPlay){
                        pauseVideo();
                    }else{
                        playVideo();
                    }
                }
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_record_viedo, menu);
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.switch_camera:
                mVideoRecorder.switchCamera(isBackCamera);
                isBackCamera = !isBackCamera;
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mVideoRecorder != null) {
            mVideoRecorder.releaseMediaRecorder();
            mVideoRecorder.releaseCamera();
        }
        EventBus.getDefault().post(new EventSetSoundRemind(false));/**关闭200ms心跳*/

    }
}
