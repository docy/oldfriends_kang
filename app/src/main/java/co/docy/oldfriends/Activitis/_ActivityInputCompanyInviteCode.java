package co.docy.oldfriends.Activitis;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventHaveGetCurrentCompany;
import co.docy.oldfriends.EventBus.EventHaveSetCurrentCompany;
import co.docy.oldfriends.Fragments._FragmentInputCompanyInviteCode;
import co.docy.oldfriends.R;


public class _ActivityInputCompanyInviteCode extends ActivityBase {

    int mode;
    LinearLayout activity_company_invite_linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_invite);

        MyConfig.activityInputCompanyInviteCode = this;

        activity_company_invite_linearLayout = (LinearLayout)findViewById(R.id.activity_company_invite_linearLayout);

//        Intent intent = getIntent();
//        if (intent != null) {
//            groupId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, -1);
//            name = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME);
//            desc = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC);
//        } else {
//            finish();
//        }

        MyLog.d("", "app start steps 3.2, ActivityInputCompanyInviteCode onCreate");
        init();

        //EventBus.getDefault().register(this);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        _FragmentInputCompanyInviteCode fragmentInputCompanyInviteCode = _FragmentInputCompanyInviteCode.newInstance(mode);
        fragmentTransaction.add(R.id.activity_company_invite_linearLayout, fragmentInputCompanyInviteCode);
        fragmentTransaction.commit();

    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    private void init() {

        Intent intent = getIntent();
        mode = intent.getIntExtra("mode", 1);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                skipInputInviteCode();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void skipInputInviteCode(){

        finishWithFailure();

    }

    public void onEventMainThread(EventHaveGetCurrentCompany eventHaveGetCurrentCompany) {
        MyLog.d("","create or choose company: eventCompanyGetCurrent.code " + eventHaveGetCurrentCompany.code);
        if(eventHaveGetCurrentCompany.code == MyConfig.retSuccess()) {
            finishWithSuccess();
        }else{

        }
    }

    public void onEventMainThread(EventHaveSetCurrentCompany eventHaveSetCurrentCompany) {
        MyLog.d("","create or choose company: eventCompanySetCurrent.success " + eventHaveSetCurrentCompany.success);

        if(eventHaveSetCurrentCompany.success == true) {
            finishWithSuccess();
        }else{
          //  MyConfig.snackbarTop(activity_company_invite_linearLayout, "Join company failed!", R.color.Red, Snackbar.LENGTH_SHORT);
         MyConfig.MyToast(0,this,"Join company failed!");
        }
    }

    public void finishWithSuccess() {
        setResult(RESULT_OK, new Intent());
        finish();
    }

    public void finishWithFailure() {
        setResult(RESULT_CANCELED, new Intent());
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            skipInputInviteCode();
            return true; // 让系统不再处理这次回退键，因为系统收到回退键后会立即把app关了
        }
        return super.onKeyDown(keyCode, event);
    }

}
