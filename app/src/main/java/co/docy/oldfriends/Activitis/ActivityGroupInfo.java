package co.docy.oldfriends.Activitis;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Fragments.FragmentGroupInfo;
import co.docy.oldfriends.R;


public class ActivityGroupInfo extends ActivityBase {

    int groupId;
    String name;
    String desc;
    public static boolean joined;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_info);

        Intent intent = getIntent();
        if (intent != null) {
            groupId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, -1);
            name = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME);
            desc = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC);
            joined = intent.getBooleanExtra(MyConfig.CONSTANT_PARAMETER_STRING_JOINED,true);
        } else {
            finish();
        }

        init();

//        //EventBus.getDefault().register(this);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragmentGroupInfo fragmentGroupInfo = FragmentGroupInfo.newInstance( groupId,  name);
        fragmentTransaction.add(R.id.activity_groupinfo_linearLayout, fragmentGroupInfo);
        fragmentTransaction.commit();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
//        //EventBus.getDefault().unregister(this);
    }

    private void init() {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



//    public void onEventMainThread(EventQuitGroup eventQuitGroup) {
//        finish();
//    }
}
