package co.docy.oldfriends.Activitis;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonDeleteCompany;
import co.docy.oldfriends.R;

/**
 * Created by wuxue on 15/10/23.
 */
public class _ActivityTransferCompany extends ActivityBase implements View.OnClickListener {
    private Button mAttornButton, mDissolutionButton;
    private TextView mMemberCounttext, mRemindText;
    private Context mContext = this;
    public Intent newIntent;
    private MyConfig.IF_AfterHttpRequest IFAfterHttpRequest = new MyConfig.IF_AfterHttpRequest() {
        @Override
        public void doAfterHttpRequest() {
            mMemberCounttext.setText(String.valueOf(MyConfig.jsonGetCompanyUserListRet.data.size()));
//            MyConfig.jsonGetCompanyUserListRet_temp = MyConfig.jsonGetCompanyUserListRet;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exitgroup_layout);
        mAttornButton = (Button) findViewById(R.id.exit_attorn_button);
        mDissolutionButton = (Button) findViewById(R.id.exit_dissolution_button);
        mMemberCounttext = (TextView) findViewById(R.id.exit_member_count_text);
        mRemindText = (TextView) findViewById(R.id.exit_remind_text);
        mRemindText.setText(getResources().getString(R.string.exitcompany_remind_exit));
        newIntent = getIntent();

        /**
         * todo
         *  下面这个应该放在ActivityChooseCompanyOwner里面
         */
        // getUserList();
        MyConfig.getCurrentYearUserAddressBooks(newIntent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, -1), IFAfterHttpRequest);

        mAttornButton.setOnClickListener(this);
        mDissolutionButton.setOnClickListener(this);

    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.exit_attorn_button:
                Intent intent = getIntent();
                Intent newIntent = new Intent(this, _ActivityChooseCompanyOwner.class);
                newIntent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, -1));
                startActivity(newIntent);
                break;
            case R.id.exit_dissolution_button:
                Intent dissolutionIntent = getIntent();
                showRemindDialog(getResources().getString(R.string.exit_delete_company), getResources().getString(R.string.exit_deletecompany_remind), mContext, dissolutionIntent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, -1));
                break;
        }
    }

    /**
     * 弹出解散集体和转让对话框
     */
    public static void showRemindDialog(String title, final String remindstr, final Context mContext, final int companyId) {
        View dialogView = LayoutInflater.from(mContext).inflate(
                R.layout.dialog_exit_remind_layout, null);
        final Dialog setDialog = new Dialog(mContext, R.style.DialogStyle);
        setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialog.setCancelable(false);
        setDialog.getWindow().setContentView(dialogView);
        WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
        lp.width = MyConfig.getWidthByScreenPercent(80);
        setDialog.getWindow().setAttributes(lp);
        final Button cancleButton = (Button) dialogView.findViewById(R.id.cancle);
        Button loginButton = (Button) dialogView.findViewById(R.id.login);
        TextView remindText = (TextView) dialogView.findViewById(R.id.remind_text);
        remindText.setText(remindstr);
        TextView titletext = (TextView) dialogView.findViewById(R.id.remind_title);
        titletext.setText(title);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                JsonDeleteCompany jsonDeleteCompany = new JsonDeleteCompany();
                jsonDeleteCompany.accessToken = MyConfig.usr_token;
                jsonDeleteCompany.companyId = companyId;
                // MyConfig.DeleteCompany(jsonDeleteCompany);
                Intent intent = new Intent(mContext, _ActivityCompanyView.class);
                mContext.startActivity(intent);
                setDialog.cancel();
            }

        });
        cancleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setDialog.cancel();
            }
        });
        setDialog.setCanceledOnTouchOutside(false);
        setDialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
