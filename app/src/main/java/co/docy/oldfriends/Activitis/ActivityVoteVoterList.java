package co.docy.oldfriends.Activitis;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Fragments.FragmentVoteVoterList;
import co.docy.oldfriends.R;


public class ActivityVoteVoterList extends ActivityBase {

    String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_list);

        Intent intent = getIntent();
        if (intent != null) {
            title = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_TITLE);

        } else {
            finish();
        }

        init();

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        FragmentVoteVoterList fragmentVoteVoterList = FragmentVoteVoterList.newInstance();

        fragmentTransaction.add(R.id.activity_person_list_linearLayout, fragmentVoteVoterList);
        fragmentTransaction.commit();

    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private void init() {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
