package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.widget.ListView;

import co.docy.oldfriends.Adapters.ListAdapterActivityPersonList;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.R;

public class ActivityActivityMoreMemberList extends ActivityBase {

    private ListView activity_more_member_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_more_member_list);
        activity_more_member_list = (ListView) findViewById(R.id.activity_more_member_list);
        activity_more_member_list.setAdapter(new ListAdapterActivityPersonList(this, MyConfig.joinedUsers));
    }
}
