package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonCreatNoticeMessage;
import co.docy.oldfriends.Messages.JsonCreatNoticeMessageRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/6/6.
 */
public class ActivitySendNotice extends ActivityBase {
    EditText sendnotice_contact,sendnotice_title,sendnotice_txt;
    ImageView sendnotice_img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_notice);
        sendnotice_contact =(EditText) findViewById(R.id.activity_send_notice_contact);
        sendnotice_title =(EditText) findViewById(R.id.activity_send_notice_title);
        sendnotice_txt =(EditText) findViewById(R.id.activity_send_notice_txt);
        sendnotice_img =(ImageView) findViewById(R.id.activity_send_notice_img);
        sendnotice_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivitySendNotice.this,ActivityChooseGroupToSendNotice.class);
                startActivityForResult(intent,1);

            }
        });
    }
    ArrayList<String> name = new ArrayList<>();
    ArrayList<Integer> id = new ArrayList<>();
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //intent参数回传
        if(resultCode == RESULT_OK){
            name = data.getExtras().getStringArrayList("name");
            id = data.getExtras().getIntegerArrayList("id");
            String str = name.toString();
            sendnotice_contact.setText(str.substring(1,str.length()-1));
        }

    }

    public void getContextFromEditText(){
        //连接后台发送通知
        String name = sendnotice_contact.getText().toString();
        final String title = sendnotice_title.getText().toString();
        final String txt = sendnotice_txt.getText().toString();
        final JsonCreatNoticeMessage jsonCreatNoticeMessage = new JsonCreatNoticeMessage();
        jsonCreatNoticeMessage.accessToken = MyConfig.usr_token;

        if(name!=null&&name.length()>0){
            for(int i=0;i<id.size();i++){
                jsonCreatNoticeMessage.groupIds[i] =id.get(i);
            }
        }else {
            Toast.makeText(ActivitySendNotice.this,"请选择小组",Toast.LENGTH_SHORT).show();
            return;
        }
        if(title!=null&&title.length()>0){
            jsonCreatNoticeMessage.title = title;
        }else{
            Toast.makeText(ActivitySendNotice.this,"请输入标题",Toast.LENGTH_SHORT).show();
            return;
        }
        if(txt!=null&&txt.length()>0){
            jsonCreatNoticeMessage.content = txt;
        }else{
            Toast.makeText(ActivitySendNotice.this,"请输入内容",Toast.LENGTH_SHORT).show();
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String strJsonCreatNoticeMessage = gson.toJson(jsonCreatNoticeMessage,JsonCreatNoticeMessage.class);
                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2()+MyConfig.API_CREATE_MESSAGE ,strJsonCreatNoticeMessage);
                MyLog.d("发通知返回的信息",s);
                JsonCreatNoticeMessageRet jsonCreatNoticeMessageRet = gson.fromJson(s,JsonCreatNoticeMessageRet.class);
                if(jsonCreatNoticeMessageRet!=null&&jsonCreatNoticeMessageRet.code==MyConfig.retSuccess()){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ActivitySendNotice.this,"发送成功",Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            menu.findItem(R.id.create_notice).setEnabled(true);
                            Toast.makeText(ActivitySendNotice.this,"发送失败，请重新发送",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }

    Menu menu;
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_notice, menu);
        this.menu = menu;
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.create_notice:
                getContextFromEditText();
                menu.findItem(R.id.create_notice).setEnabled(false);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
