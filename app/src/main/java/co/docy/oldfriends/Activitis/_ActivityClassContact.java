package co.docy.oldfriends.Activitis;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import co.docy.oldfriends.Fragments._FragmentClassContact;
import co.docy.oldfriends.R;


public class _ActivityClassContact extends ActivityBase implements _FragmentClassContact.getPersonNum {

    int groupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_contact);
        Intent intent = getIntent();
        setTitle(intent.getStringExtra("class_name")+"("+intent.getIntExtra("personNum",0)+"人)");
        init();


        int class_id = intent.getIntExtra("class_id", -1);

        if(class_id != -1) {

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            _FragmentClassContact fragmentCompanyContact = _FragmentClassContact.newInstance(class_id);
            fragmentTransaction.add(R.id.activity_company_contact_linearLayout, fragmentCompanyContact);
            fragmentTransaction.commit();
        }

    }
    @Override
    public void getPersonNumBysize(int num) {
//        setTitle(getIntent().getStringExtra("class_name")+"("+num+"人)");
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onPause() {
        super.onPause();

    }

    private void init() {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
