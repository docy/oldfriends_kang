package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventHeartOneSecond;
import co.docy.oldfriends.Messages.JsonSmsValidCodeUpdatePhone;
import co.docy.oldfriends.Messages.JsonSmsValidCodeUpdatePhoneRet;
import co.docy.oldfriends.Messages.JsonUpdatePersonInfo;
import co.docy.oldfriends.Messages.JsonUpdatePersonInfoRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class ActivityUpdatePersonInfo extends ActivityBase {
    private EditText update_et_phone, update_et_code;// update_et_email,
    private ImageView create_subject_attached_view;
    private String old_nickname, old_email, old_phone, itemName;
    private int old_sex;
    private TextView remindText;
    private String tempStr = new String();
    private RelativeLayout update_sex;
    private RadioButton man_radio, woman_radio;
    private boolean is_change_username;
    private LinearLayout update_et_valid_container;
    private Button update_et_code_button;

    int count = 0;
    private String old_class;
    private String old_location;
    private String old_company;
    private String old_title;
    private EditText update_userinfo;
    private String old_company_introduce;
    private String old_duty;
    private EditText update_company_introduce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_update);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        old_nickname = bundle.getString("nick_name");
        old_phone = bundle.getString("phone");
        old_email = bundle.getString("email");
        old_class = bundle.getString("class");
        old_location = bundle.getString("location");
        old_company = bundle.getString("company");
        old_title = bundle.getString("title");
        old_company_introduce = bundle.getString("company_introduce");
        old_duty = bundle.getString("duty");

        if (old_phone == null) {
            old_phone = "";
        }
        itemName = intent.getStringExtra("itemName");
        // old_email = bundle.getString("email");

        MyLog.d("", "1 " + old_nickname + " 2 " + old_email + " 3 " + old_phone + " itemName " + itemName);
        init();
    }

    private void init() {
        update_userinfo = (EditText) findViewById(R.id.update_userinfo);
        update_company_introduce = (EditText) findViewById(R.id.update_company_introduce);


        update_et_phone = (EditText) findViewById(R.id.update_et_phone);
        update_et_phone.setText(old_phone);


        /**验证码部分*/
        update_et_valid_container = (LinearLayout) findViewById(R.id.update_et_valid_container);
        update_et_code_button = (Button) findViewById(R.id.update_et_code_button);
        update_et_code_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count == 0) {
                    /**
                     * 要求验证码
                     */
                    count = 60;
                    smsValidCodeUpdatePhone();

                }

            }
        });
        update_et_code = (EditText) findViewById(R.id.update_et_code);

        /**暂时不使用这个提示了，改为各自TextView的hint属性提示*/
        remindText = (TextView) findViewById(R.id.user_update_remind_text);
        remindText.setVisibility(View.GONE);

//        update_username.setSelection(old_username.length());


        /**
         * 根据目前UI设计，只能同时修改一个参数
         */
        //  update_et_email.setVisibility(View.GONE);

        update_userinfo.setVisibility(View.GONE);

        update_company_introduce.setVisibility(View.GONE);

        update_et_phone.setVisibility(View.GONE);

        update_et_valid_container.setVisibility(View.GONE);

        String sFormat = getResources().getString(R.string.user_info_null_remind);


        switch (itemName) {

            case "phone":
                update_et_phone.setVisibility(View.VISIBLE);
                update_et_valid_container.setVisibility(View.VISIBLE);
                setTitle(getResources().getString(R.string.user_phone));
                tempStr = String.format(sFormat, getResources().getString(R.string.user_phone));
                break;
            case "nick_name":
                update_userinfo.setText(old_nickname);
                update_userinfo.setVisibility(View.VISIBLE);
                setTitle(getString(R.string.update_user_name));
                tempStr = String.format(sFormat, getString(R.string.update_user_name));
                break;
            case "email":
                update_userinfo.setVisibility(View.VISIBLE);
                update_userinfo.setText(old_email);
                setTitle("邮箱");
                tempStr = String.format(sFormat, "邮箱");
                break;

            case "class":
                update_userinfo.setVisibility(View.VISIBLE);
                update_userinfo.setText(old_class);
                setTitle("班级");
                tempStr = String.format(sFormat, "班级");
                break;
            case "location":
                update_userinfo.setVisibility(View.VISIBLE);
                update_userinfo.setText(old_location);
                setTitle("位置");
                tempStr = String.format(sFormat, "位置");
                break;
            case "company":
                update_userinfo.setVisibility(View.VISIBLE);
                update_userinfo.setText(old_company);
                setTitle("所在单位");
                tempStr = String.format(sFormat, "所在单位");
                break;
            case "title":
                update_userinfo.setVisibility(View.VISIBLE);
                update_userinfo.setText(old_title);
                setTitle("职务");
                tempStr = String.format(sFormat, "职务");
                break;
            case "company_introduce":
                update_company_introduce.setVisibility(View.VISIBLE);
                update_company_introduce.setText(old_company_introduce);
                setTitle("企业简介");
                tempStr = String.format(sFormat, "企业简介");
                break;
            case "duty":
                update_company_introduce.setVisibility(View.VISIBLE);
                update_company_introduce.setText(old_duty);
                setTitle("分管领域");
                tempStr = String.format(sFormat, "分管领域");
                break;


        }
//        if (itemName.equals("nickName")) {
//
//        } else if (itemName.equals("email")) {
//         //   update_et_email.setVisibility(View.VISIBLE);
//            setTitle(getResources().getString(R.string.user_email));
//            tempStr = String.format(sFormat, getResources().getString(R.string.user_email));
//        } else if (itemName.equals("phone")) {
//
//        }

        update_userinfo.setHint(tempStr);
        update_company_introduce.setHint(tempStr);
        update_et_phone.setHint(tempStr);
    }


    /**
     * 修改手机时获取验证码
     */
    private void smsValidCodeUpdatePhone() {

        MyLog.d("", "rxandroid: smsValidCodeUpdatePhone()");

        JsonSmsValidCodeUpdatePhone jsonSmsValidCodeUpdatePhone = new JsonSmsValidCodeUpdatePhone();
        jsonSmsValidCodeUpdatePhone.phone = update_et_phone.getText().toString();
        jsonSmsValidCodeUpdatePhone.countryCode = "+86";
        subscription_list.add( //subscription_list在ActivityBase中，便于在onDestroy全部释放
                HttpTools.http_api_service().smsValidCodeUpdatePhone(jsonSmsValidCodeUpdatePhone)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<JsonSmsValidCodeUpdatePhoneRet>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                                MyConfig.MyToast(1, ActivityUpdatePersonInfo.this, "验证码发送失败，请检查电话号码是否有误");
                                count = 0;
                            }

                            @Override
                            public void onNext(JsonSmsValidCodeUpdatePhoneRet jsonSmsValidCodeUpdatePhoneRet) {
                                String format = getResources().getString(R.string.please_wait);
                                String result = String.format(format, count);
                                update_et_code_button.setText(result);


                                if (jsonSmsValidCodeUpdatePhoneRet.code == MyConfig.retSuccess()) {

                                    if (MyConfig.debug_autofill_validcode) {
                                        update_et_code.setText(jsonSmsValidCodeUpdatePhoneRet.data.code);
                                    }
                                }
                            }
                        })
        );


    }


    public void onEventMainThread(EventHeartOneSecond eventHeartOneSecond) {

        if (count > 0) {
            count--;
        }
        if (count > 0) {
            String format = getResources().getString(R.string.please_wait);
            String result = String.format(format, count);
            update_et_code_button.setText(result);
        } else {
            update_et_code_button.setText(getString(R.string.prompt_valid_button));
        }

    }


    public void backWithoutResult() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
//        Toast.makeText(this, "input is discarded", Toast.LENGTH_LONG).show();
        backWithoutResult();
    }

    private void backWithResult() {
        Intent intent = new Intent();
//        intent.putExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_TITLE, create_subject_title_et.getText().toString());
//      intent.putExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_BODY, create_subject_body_et.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * 更新个人信息到服务器
     */
    private void updatePersonInfoToServer() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

        //  final String email_new = update_et_email.getText().toString().trim();
        final String phone_new = update_et_phone.getText().toString().trim();

        final String update_userinfo_new = update_userinfo.getText().toString().trim();
        final String update_company_introduce_new = update_company_introduce.getText().toString().trim();


        new Thread(new Runnable() {
            @Override
            public void run() {
                final JsonUpdatePersonInfo jsonUpdatePersonInfo = new JsonUpdatePersonInfo();
                jsonUpdatePersonInfo.accessToken = MyConfig.usr_token;
                /**需要修改的条目不同，向服务器提交的数据不同*/
                switch (itemName) {
                    case "phone":
                        jsonUpdatePersonInfo.phone = phone_new;
                        break;
                    case "nick_name":
                        jsonUpdatePersonInfo.nickName = update_userinfo_new;
                        break;
                    case "email":
                        jsonUpdatePersonInfo.email = update_userinfo_new;
                        break;
                    case "class":
                        jsonUpdatePersonInfo.user_class = update_userinfo_new;
                        break;
                    case "location":
                        jsonUpdatePersonInfo.city = update_userinfo_new;
                        break;
                    case "company":
                        jsonUpdatePersonInfo.company = update_userinfo_new;
                        break;
                    case "title":
                        jsonUpdatePersonInfo.title = update_userinfo_new;
                        break;
                    case "company_introduce":
                        jsonUpdatePersonInfo.companyIntro = update_company_introduce_new;
                        break;
                    case "duty":
                        jsonUpdatePersonInfo.duty = update_company_introduce_new;
                        break;
                }

                if (update_et_phone.getVisibility() == View.VISIBLE) {
                    /**
                     * 如果是修改电话号码，那么需要验证码 20160329
                     */
                    jsonUpdatePersonInfo.code = update_et_code.getText().toString();
                }

                //  jsonUpdatePersonInfo.email = email_new;

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonUpdatePersonInfo, JsonUpdatePersonInfo.class);
                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_UPDATE, jsonStr);
                final JsonUpdatePersonInfoRet jsonUpdatePersonInfoRet = gson.fromJson(s, JsonUpdatePersonInfoRet.class);
                MyLog.d("", "HttpTools: " + s);
                if (jsonUpdatePersonInfoRet != null && jsonUpdatePersonInfoRet.code == MyConfig.retSuccess()) {

                    switch (itemName) {
                        case "phone":
                            MyConfig.usr_phone = jsonUpdatePersonInfo.phone;
                            MyConfig.prefEditor.putString(MyConfig.PREF_USER_PHONE, MyConfig.usr_phone);
                            break;
                        case "nick_name":
                            MyConfig.usr_nickname = jsonUpdatePersonInfo.nickName;
                            MyConfig.prefEditor.putString(MyConfig.PREF_USER_NICKNAME, MyConfig.usr_nickname);
                            break;
                        case "email":
                            MyConfig.usr_email = jsonUpdatePersonInfo.email;
                            MyConfig.prefEditor.putString(MyConfig.PREF_USER_EMAIL, MyConfig.usr_email);
                            break;
                        case "class":
                            MyConfig.usr_current_class_name = jsonUpdatePersonInfo.user_class;
                            MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_CURRENT_CLASS_NAME, MyConfig.usr_current_class_name);
                            break;
                        case "location":
                            MyConfig.usr_city = jsonUpdatePersonInfo.city;
                            MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_CITY, MyConfig.usr_city);
                            break;
                        case "company":
                            MyConfig.usr_company = jsonUpdatePersonInfo.company;
                            MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_COMPANY, MyConfig.usr_company);
                            break;
                        case "title":
                            MyConfig.usr_title = jsonUpdatePersonInfo.title;
                            MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_TITLE, MyConfig.usr_title);
                            break;
                        case "company_introduce":
                            MyConfig.usr_company_intro = jsonUpdatePersonInfo.companyIntro;
                            MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_COMPANY_INTRO, MyConfig.usr_company_intro);
                            break;
                        case "duty":
                            MyConfig.usr_duty = jsonUpdatePersonInfo.duty;
                            MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_DUTY, MyConfig.usr_duty);
                            break;
                    }


                    MyConfig.prefEditor.commit();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            backWithResult();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MyConfig.MyToast(0, ActivityUpdatePersonInfo.this, MyConfig.getErrorMsg(""+jsonUpdatePersonInfoRet.code));
                        }
                    });
                }
            }
        }).start();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_update_submit, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                Toast.makeText(this, "home key is pressed, input is discarded", Toast.LENGTH_LONG).show();

                backWithoutResult();
                return true;
            case R.id.update_submit:
                updatePersonInfoToServer();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
