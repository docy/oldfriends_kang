package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import co.docy.oldfriends.Adapters.ListAdapterRegistLable;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.RegistLable;
import co.docy.oldfriends.Messages.JsonRegistLabel;
import co.docy.oldfriends.Messages.JsonRegistLabelGetRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

public class ActivityRegistLabel extends ActivityBase {

    private RecyclerView regist_label_recycler_view;
    private Button regist_label_confirm;

    private ArrayList<RegistLable> labelList;
    private ArrayList<String> selectedLabelList;
    private ArrayList<String> unselectedLabelList;

    private GridLayoutManager mLayoutManager;

    private ListAdapterRegistLable adapter;
    /**
     * 添加标签监听器
     */
    private ListAdapterRegistLable.MyClickListener clickListener;
    /**
     * 删除标签的监听器
     */
    private ListAdapterRegistLable.DeleteClickListener deleteClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist_label);
        regist_label_recycler_view = (RecyclerView) findViewById(R.id.regist_label_recycler_view);

        regist_label_confirm = (Button) findViewById(R.id.regist_label_confirm);
        labelList = new ArrayList<>();
        selectedLabelList = new ArrayList<>();
        unselectedLabelList = new ArrayList<>();
        initRecyclerView();
        getDataFormServer();
        regist_label_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendLabel2Server();
            }
        });

        MyConfig.flag_open_label_activity = 0;
    }

    /**
     * 填充RecyclerViewe数据
     */
    private void initRecyclerView() {
        initAdapterListnener();
        mLayoutManager = new GridLayoutManager(this, 4);
        regist_label_recycler_view.setLayoutManager(mLayoutManager);
        adapter = new ListAdapterRegistLable(this, selectedLabelList, unselectedLabelList, clickListener, deleteClickListener);
        regist_label_recycler_view.setAdapter(adapter);
    }

    /**
     * 重写Adapter的回调接口，一个是添加标签的回调接口，另一个是删除标签的回调接口
     */
    private void initAdapterListnener() {
        clickListener = new ListAdapterRegistLable.MyClickListener() {
            @Override
            public void onClick(int position, int selectedLabelListLength) {
                if (position > selectedLabelListLength + 1) {
                    String label = unselectedLabelList.get(position - selectedLabelListLength - 2);
                    unselectedLabelList.remove(position - selectedLabelListLength - 2);
                    selectedLabelList.add(label);
                }
                adapter.notifyDataSetChanged();
                adapter.updateSelectedLabelListLength();
            }
        };
        deleteClickListener = new ListAdapterRegistLable.DeleteClickListener() {
            @Override
            public void onClick(int position) {
                String label = selectedLabelList.get(position - 1);
                selectedLabelList.remove(position - 1);
                unselectedLabelList.add(label);
                adapter.notifyDataSetChanged();
                adapter.updateSelectedLabelListLength();
            }
        };
    }

    /**
     * 将标签提交到服务器
     */
    private void sendLabel2Server() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                JsonRegistLabel jsonRegistLabel = new JsonRegistLabel();
                jsonRegistLabel.tags = selectedLabelList;
                jsonRegistLabel.accessToken = MyConfig.usr_token;
                Gson gson = new Gson();
                String json_label = gson.toJson(jsonRegistLabel);
                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_REGISTER_LABEL, json_label);
                MyLog.d("kwwl", "提交标签返回的结果是：=============" + s);
                try {
                    JSONObject jo = new JSONObject(s);
                    int code = jo.getInt("code");
                    if (code == 200) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }


    /**
     * 从服务器中获取标签集
     */
    public void getDataFormServer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GET_REGISTER_LABEL, MyConfig.usr_token);
                MyLog.d("kwwl", "得到标签返回的结果是：=============" + s);
                JsonRegistLabelGetRet jsonGetRegistLabelRet = new Gson().fromJson(s, JsonRegistLabelGetRet.class);
                if (jsonGetRegistLabelRet != null && jsonGetRegistLabelRet.code == 200) {
                    for (JsonRegistLabelGetRet.RegistLabel registLabel : jsonGetRegistLabelRet.data.all) {
                        unselectedLabelList.add(registLabel.name);
                    }
                    for (JsonRegistLabelGetRet.RegistLabel registLabel : jsonGetRegistLabelRet.data.mine) {
                        selectedLabelList.add(registLabel.name);
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            adapter.updateSelectedLabelListLength();
                        }
                    });
                }
            }
        }).start();
    }

}
