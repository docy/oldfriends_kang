package co.docy.oldfriends.Activitis;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.VideoView;

import java.io.File;

import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.R;


public class ActivityVideoPlayer extends ActivityBase {

    /**
     * 视频可能的来源
     *  网上，应该http开头，mode为0
     *  本地目录，应该/开头，mode为1
     *  asset，应该file开头，mode为2
     *  ------为安全起见，暂时使用mode来指明来源
     */
    int mode = -1;
    String recorder_os;

    VideoView videoView;
    File videoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        Intent intent = getIntent();
        mode = intent.getIntExtra("mode", -1);
        recorder_os = intent.getStringExtra("recorder_os");
        MyLog.d("","video play xiaomi: os:" + recorder_os);

        if(recorder_os == null){

        }else if(recorder_os.equals("ios")){

            MyLog.d("","video play xiaomi: os is ios");

            if(Build.BRAND.equals("Huawei") && (Build.MODEL.startsWith("PE") || Build.MODEL.startsWith("H60"))) {
                /**
                 * 华为荣耀播放ios录制的视频时方向不对，这里用landscape模式来处理
                 */
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

            }else if(Build.BRAND.equals("Xiaomi") && Build.MODEL.equals("MI 4LTE")) {

                MyLog.d("","video play xiaomi: ");

                /**
                 * 小米4不能播放ios视频
                 */
                String videoPath = intent.getStringExtra("path");
                switch (mode){
                    case 0:
//                        videoView.setVideoURI(Uri.parse(videoPath));

                        MyLog.d("","video play xiaomi: mode 0");

                        Intent intent_xiaomi_uri = new Intent();
                        intent_xiaomi_uri.setAction(Intent.ACTION_VIEW);
                        intent_xiaomi_uri.setDataAndType(Uri.parse(videoPath), "video/mp4");
                        startActivity(intent_xiaomi_uri);

                        break;
                    case 1:
//                        videoFile = new File(videoPath);
//                        videoView.setVideoURI(Uri.fromFile(videoFile));
                        MyLog.d("", "video play xiaomi: mode 1 ");

                        Intent intent_xiaomi_file = new Intent();
                        intent_xiaomi_file.setAction(Intent.ACTION_VIEW);
                        intent_xiaomi_file.setDataAndType(Uri.fromFile(new File(videoPath)), "video/mp4");
                        startActivity(intent_xiaomi_file);


                        break;
                    case 2:
                        videoView.setVideoURI(Uri.fromFile(new File(videoPath)));
                        break;

                }
                finish();
                return;
            }

        }else if(recorder_os.equals("android")){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        videoView = (VideoView)findViewById(R.id.fragment_videoplayer_videoview);

        videoView.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                MyLog.d("", "VideoView touched " + event.getAction());

                switch (event.getAction() & MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_UP:
                        finish();
                        break;
                }
                return true;
            }
        });



//        if (videoView.isPlaying()){
//            videoView.stopPlayback();
//        }


        if(mode == -1) {
//            videoView.setVideoURI(Uri.parse(MyConfig.getApiDomain_NoSlash_GetResources() + "/uploads/file-1468984454592-17.mp4"));
//            videoView.start();
        }else{

            String videoPath = intent.getStringExtra("path");
            switch (mode){
                case 0:
                    MyLog.d("", "VideoView mode 0" );
                    videoView.setVideoURI(Uri.parse(videoPath));
                    break;
                case 1:
                    videoFile = new File(videoPath);
                    MyLog.d("", "VideoView mode 1" );

                    MyLog.d("", "VideoView path " + videoFile.getAbsolutePath() + " size " + videoFile.length());

                    videoView.setVideoURI(Uri.fromFile(videoFile));
                    break;
                case 2:
                    videoView.setVideoURI(Uri.fromFile(new File(videoPath)));
                    break;

            }

            videoView.start();

        }

    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void init() {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
