package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Fragments.FragmentIntroducePage;
import co.docy.oldfriends.R;


public class ActivityIntroducePages extends AppCompatActivity {

    fragmentPagerAdapter mFragmentPagerAdapter;
    ViewPager mViewPager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();

        if( ! MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {

            setContentView(R.layout.activity_introduce_pages);
            mFragmentPagerAdapter =
                    new fragmentPagerAdapter(
                            getSupportFragmentManager());
            mViewPager = (ViewPager) findViewById(R.id.introduce_pages);
            mViewPager.setAdapter(mFragmentPagerAdapter);
        }else{

            setContentView(R.layout.activity_imageview);

            ImageView iv = (ImageView)findViewById(R.id.intro_single_imageview);
            Picasso.with(this).load(R.drawable.guide_page_2x).fit().into(iv);
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    MyConfig.app_open_count = 1;
//                MyConfig.saveAppOpenCountToPreference(getActivity());
                    MyConfig.saveIntToPreference(ActivityIntroducePages.this, "app_open_count", MyConfig.app_open_count);

                    Intent newIntent = new Intent(ActivityIntroducePages.this, ActivityViewPager.class);
                    startActivity(newIntent);

                    finish();
                }
            });
        }

    }

    public class fragmentPagerAdapter extends FragmentPagerAdapter {
        public fragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = FragmentIntroducePage.newInstance(i);
            return fragment;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "" + position;
        }
    }

}
