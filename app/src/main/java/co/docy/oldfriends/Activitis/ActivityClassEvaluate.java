package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.R;

public class ActivityClassEvaluate extends ActivityBase {

    private WebView mWebView;
    private String urlCategory,name,url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_evaluate);
        urlCategory = getIntent().getStringExtra(MyConfig.WEB_VIEW_URL_CATEGORY);
        url = getIntent().getStringExtra("url");
        name = getIntent().getStringExtra("name");
        initView();
        initData();
    }


    private void initView() {
        mWebView = (WebView) findViewById(R.id.webview_calss_evaluate);
        WebSettings webSettings = mWebView.getSettings();
        //设置WebView属性，能够执行Javascript脚本
        webSettings.setJavaScriptEnabled(true);
        //设置可以访问文件
        webSettings.setAllowFileAccess(true);
        //设置支持缩放
        webSettings.setBuiltInZoomControls(true);


    }



    private void initData() {
        switch (urlCategory){
            /**课程评价*/
            case MyConfig.WEB_VIEW_URL_CATEGORY_CLASS_EVALUATE:
                setTitle("课程评价");
                mWebView.loadUrl(MyConfig.WEB_VIEW_URL_REVIEW);
            break;
            /**挣学分*/
            case MyConfig.WEB_VIEW_URL_CATEGORY_CREDIT:
                setTitle(name);
                //加载需要显示的网页
                mWebView.loadUrl(url);
                //设置web视图
                mWebView.setWebViewClient(new WebViewClient());
                mWebView.setWebChromeClient(new WebChromeClient() {
                    @Override
                    public void onProgressChanged(WebView view, int newProgress) {
                        if (newProgress == 100) {
                            // 网页加载完成
                            mWebView.setVisibility(View.VISIBLE);
                        } else {
                            // 加载中
                            mWebView.setVisibility(View.GONE);

                        }

                    }
                });
                break;
            case "H5page":
                setTitle(name);
                mWebView.loadUrl(url);
                mWebView.setWebViewClient(new WebViewClient());
                break;
        }

    }
    private class WebViewClient extends android.webkit.WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return false;
        }
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            if(url.contains("html")){
                finish();
            }else {
                mWebView.goBack(); //goBack()表示返回WebView的上一页面
                return true;
            }
        }
        finish();//结束退出程序
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_info,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
