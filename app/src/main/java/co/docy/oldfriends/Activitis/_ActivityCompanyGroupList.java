package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.LinkedList;

import co.docy.oldfriends.Adapters.ListAdapterGroups;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.Group;
import co.docy.oldfriends.Messages.JsonCompanyGroupListRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;


public class _ActivityCompanyGroupList extends ActivityBase {

    int company_id;
    JsonCompanyGroupListRet jsonCompanyGroupListRet;
    public LinkedList<Group> list_group = new LinkedList<>();
    ListAdapterGroups adapter;
    ListView activity_company_group_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_group_list);
        Intent intent = getIntent();
        company_id = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, -1);
        activity_company_group_list = (ListView) findViewById(R.id.activity_company_group_list);

        getCompanyGroupList();
    }


    private void getCompanyGroupList() {


        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();

                /**
                 * 获取可以加入，但还没有加入的rooms
                 */
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_GROUP_LIST + company_id + MyConfig.API2_COMPANY_GROUP_LIST_2, MyConfig.usr_token);
                MyLog.d("", "getCompanyGroupList ret " + s);
                jsonCompanyGroupListRet = gson.fromJson(s, JsonCompanyGroupListRet.class);
                if (jsonCompanyGroupListRet != null && jsonCompanyGroupListRet.code==MyConfig.retSuccess()) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for (JsonCompanyGroupListRet.CompanyGroupListRet data : jsonCompanyGroupListRet.data) {
                                if (data.category == MyConfig.MSG_CATEGORY_NORMAL) {
                                    list_group.add(Group.fromCompanyGroupListRet(data));
                                }
                            }
                            adapter = new ListAdapterGroups(_ActivityCompanyGroupList.this, list_group, MyConfig.GROUP_LIST_TYPE_GROUP_SIMPLE_INFO,null,null);
                            activity_company_group_list.setAdapter(adapter);
                        }
                    });
                }
            }
        }).start();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
