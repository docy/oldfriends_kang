package co.docy.oldfriends.Activitis;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.astuetz.PagerSlidingTabStrip;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.joda.time.LocalDate;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.Group;
import co.docy.oldfriends.EventBus.EventAppVersionCheckInfo;
import co.docy.oldfriends.EventBus.EventCompanyCreated;
import co.docy.oldfriends.EventBus.EventCompanyInfoUpdated;
import co.docy.oldfriends.EventBus.EventCompanyJoined;
import co.docy.oldfriends.EventBus.EventContactDownloadList;
import co.docy.oldfriends.EventBus.EventDisconnectSocket;
import co.docy.oldfriends.EventBus.EventFileDownloadProgress;
import co.docy.oldfriends.EventBus.EventGotoChooseCompany;
import co.docy.oldfriends.EventBus.EventGroupListUpdateFromServer;
import co.docy.oldfriends.EventBus.EventGroupListUpdateUi;
import co.docy.oldfriends.EventBus.EventHaveGetCurrentCompany;
import co.docy.oldfriends.EventBus.EventHaveSetCurrentCompany;
import co.docy.oldfriends.EventBus.EventKick;
import co.docy.oldfriends.EventBus.EventLightNotificationRedDot;
import co.docy.oldfriends.EventBus.EventLogin;
import co.docy.oldfriends.EventBus.EventLogout;
import co.docy.oldfriends.EventBus.EventNetConnectStatusChanged;
import co.docy.oldfriends.EventBus.EventContactDownloadPlease;
import co.docy.oldfriends.EventBus.EventPushOnBind;
import co.docy.oldfriends.EventBus.EventReConnectSocket;
import co.docy.oldfriends.EventBus.EventRestartService;
import co.docy.oldfriends.EventBus.EventViewpagerSetCurrentItemPosition;
import co.docy.oldfriends.Fragments.FragmentV4AppVerticalViewpager;
import co.docy.oldfriends.Fragments._FragmentV4Applicaitons;
import co.docy.oldfriends.Fragments.FragmentV4CompanyContactTongChuang;
import co.docy.oldfriends.Fragments._FragmentV4VerticalViewpagerTest;
import co.docy.oldfriends.Fragments._FragmentV4CompanyContact;
import co.docy.oldfriends.Fragments._FragmentV4DirectTalkList;
import co.docy.oldfriends.Fragments._FragmentV4FavoriteList;
import co.docy.oldfriends.Fragments.FragmentV4Me;
import co.docy.oldfriends.Fragments._FragmentV4Option;
import co.docy.oldfriends.Fragments.FragmentV4TalkList;
import co.docy.oldfriends.Messages.JsonGetMsgRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicCommentRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicRet;
import co.docy.oldfriends.Messages.JsonGroupCanJoinRet;
import co.docy.oldfriends.Messages.JsonLastMsgUniversal;
import co.docy.oldfriends.Messages.JsonLogout;
import co.docy.oldfriends.Messages.JsonLogoutRet;
import co.docy.oldfriends.Messages.JsonUserGroupsRet;
import co.docy.oldfriends.Messages.Json_DirectRoom_GetTopicImageRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Service.MyService;
import de.greenrobot.event.EventBus;

/**
 * Created by youhy on 5/11/15.
 */
public class ActivityViewPager extends ActivityBase implements FragmentV4CompanyContactTongChuang.getContactNum {

    private Toolbar toolbar;
    ViewPager mViewPager;

    int currentPagerTab = 0;

    Menu menu;

    SectionsPagerAdapter mSectionsPagerAdapter;
    //    SampleListFragment sampleListFragment[] = new SampleListFragment[4];

    _FragmentV4FavoriteList fragmentV4FavoriteList;
    _FragmentV4Option fragmentV4Option;
    _FragmentV4CompanyContact fragmentV4CompanyContact;
    FragmentV4CompanyContactTongChuang fragmentV4CompanyContactTongChuang;
    FragmentV4Me fragmentV4Me;
    FragmentV4TalkList fragmentV4TalkList;
    _FragmentV4DirectTalkList fragmentV4DirectTalkList;
    _FragmentV4Applicaitons fragmentV4ApplicaitonsNotused;
    FragmentV4AppVerticalViewpager fragmentV4AppVerticalViewpager;
    _FragmentV4VerticalViewpagerTest fragmentV4VerticalViewpagerTest;

    PagerSlidingTabStrip tabs;
    RadioGroup radioGroup;
    RadioButton radio_b0, radio_b1, radio_b2, radio_b3;


    MyService mService;
    boolean mBound = false;
    public void onCreate(Bundle savedInstanceState) {

        MyConfig.activityViewPager = this;
        super.child_activity_name = this.getClass().getName();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);

        MyLog.d("","login failed: 3");
//        forceLoginWhenViewpagerOnCreate();
        initToolbar();
        sysInit();
        initFragments();
        initViewPager();
        initService();

        initShare();

        //EventBus.getDefault().register(this);

    }

    private void initShare() {

//        com.umeng.socialize.utils.Log.LOG = true;


    }

    private void initToolbar() {

        // Set a toolbar to replace the action bar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");

        if(MyConfig.have_company_view) {

            //toolbar.setNavigationIcon(R.drawable.goto_company_list);
            toolbar.setNavigationIcon(R.drawable.home_3x);
            /**
             * 设置icon大小
             */
            String s = "navigation";
            toolbar.setNavigationContentDescription(s);
            ArrayList<View> potentialViews = new ArrayList<View>();
            toolbar.findViewsWithText(potentialViews, s, View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);

            if (potentialViews.size() > 0) {
                ImageButton navigationIcon = (ImageButton) potentialViews.get(0); //navigation icon is ImageButton
                if (navigationIcon != null) {
                    navigationIcon.getLayoutParams().width = MyConfig.getPxFromDimen(this, R.dimen.house_icon_size);
                    navigationIcon.getLayoutParams().height = MyConfig.getPxFromDimen(this, R.dimen.house_icon_size);
                    Picasso.with(this).load(R.drawable.home_3x).into(navigationIcon);
                }
            }

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent_advance_setting = new Intent(ActivityViewPager.this, _ActivityCompanyView.class);
                    startActivity(intent_advance_setting);
//                MyConfig.MyToast(0, ActivityViewPager.this, "setNavigationOnClickListener");
                }
            });
        }
    }

//    private void forceLoginWhenViewpagerOnCreate() {
//        /**
//         * todo
//         *
//         * 这里比较迷惑，涉及app意外重启、开发调试时app的覆盖安装
//         *
//         */
//
//        MyConfig.flag_viewpager_start_______seems_no_use = 1;//from onCreate()
//
//    }

    @Override
    protected void onResume() {
        super.onResume();
        MyLog.d("","app start steps 0, usr_status:" +MyConfig.usr_status);
        /**
         * load user data from disk
         */
        MyLog.d("","app start steps 1, usr_status:" +MyConfig.usr_status);
        MyConfig.loadUserDataFromPreference(this);
        MyConfig.getOraderDataToSharedPreferences();
        MyLog.d("","app start steps 2, usr_status:" +MyConfig.usr_status);
        if (MyConfig.usr_status < MyConfig.USR_STATUS_LOGIN) {
            MyLog.d("", "app start steps 2.1");
            gotoLogin();
            return;
        }

        MyLog.d("", "app start steps 3");
        if (MyConfig.flag_open_invite_code_activity > 0) {
            MyLog.d("", "app start steps 3.1");
            gotoCompanyInvite();
            return;
        }

        if (MyConfig.flag_open_label_activity > 0) {
            MyLog.d("", "app start steps 3.1");
            Intent intent = new Intent(this, ActivityRegistLabel.class);
            startActivity(intent);
            return;
        }


        if(MyConfig.FALSE_TAG) {
            /**
             * 在老朋友这个app里面，第一次微信登录之后不提示用户输入手机号用户名等信息
             */
            if (MyConfig.flag_open_regist_from_weixin_activity > 0) {

//            MyConfig.MyToast(1, this, "flag_open_regist_from_weixin_activity");

                startActivity(new Intent(this, ActivityRegistFromWeixin.class));
                return;
            }
        }

        /**
         * 新手引导
         */
        newbie_2();

        /**
         * 登录之后即可更新公司列表
         */
        MyLog.d("", "app start steps 4");
        MyConfig.companyListUpdateFromServer();
        MyLog.d("","API_USER_PUSH_TOKEN: in viewpager()" );
        initPush();


        MyLog.d("", "app start steps 5, usr_status:" + MyConfig.usr_status);
        if (MyConfig.usr_status < MyConfig.USR_STATUS_HAVE_CURRENT_COMPANY) {
            MyLog.d("", "app start steps 5.1");
            checkCurrentCompany();
            return;
        }

        if(MyConfig.jsonGetCurrentCompanyRet != null && MyConfig.jsonGetCurrentCompanyRet.code == MyConfig.retSuccess()){

            getUserDataAfterCurrentCompany();

        }else{
            checkCurrentCompany();
            return;
        }

        updatePagerMenu(mViewPager.getCurrentItem());
    }

    private void getUserDataAfterCurrentCompany() {
        /**
         * 有当前公司后可以获取用户数据
         */

//        toolbar.setTitle(MyConfig.jsonGetCurrentCompanyRet.data.name);
//        toolbar.setTitleTextColor(getResources().getColor(R.color.White));

        MyLog.d("", "app start steps 5.2");
        groupListUpdateFromServer();

        MyLog.d("", "app start steps 5.3");
        checkShareIn();


        /**
         */
        MyLog.d("", "app start steps 5.4");
        MyLog.d("", "API_COURSES_BY_SLOT get courses from viewpager");
        if(MyConfig.jsonGetCoursesBySlotRet == null) {

            /**
             * 这里下载学员课程信息
             * 目前是简单处理，即下载登录时的前一年及后面三年的课程信息
             * 以后有空再优化，比如根据日历翻页来动态下载
             */

            LocalDate days_begin = new LocalDate().minusYears(1);
            MyLog.d("", "app start steps 5.5: " + days_begin.toString());
            MyConfig.getMoreCoursesBySlot(days_begin.toString(), 52*4);
        }

        MyConfig.getMyPrivilege();

    }

    private void checkShareIn() {
        /**
         * 查看有没有分享进来的资料
         */
        MyLog.d("", "share in process: start viewpager.... " + MyConfig.flag_have_share_in);
        if (MyConfig.flag_have_share_in) {
            Intent intent_main = new Intent(ActivityViewPager.this, ActivityShareInWindow.class);
            startActivity(intent_main);
            MyLog.d("", "share in process: viewpager start share in window.... ");
        }
    }

    private void gotoLogin() {
        MyLog.d("", "app start steps gotoLogin(): start ActivityLogin, app_open_count:" + MyConfig.app_open_count);

//        if((MyConfig.activityLogin != null) && (MyConfig.getTopAppPackageName().endsWith(ActivityLogin.class.getName()))){
//        if(MyConfig.activityAccount != null){
//        if(MyConfig.activityLoginTongChuang != null){
//        if(MyConfig.getTopAppPackageName().endsWith(ActivityLoginTongChuang.class.getName())){

        if(MyConfig.FALSE_TAG){

        }else {
            MyLog.d("", "app start steps gotoLogin(): start ActivityLogin by intent");
            Intent intent_login = new Intent(ActivityViewPager.this, ActivityLoginTongChuang.class);
//            Intent intent_login = new Intent(ActivityViewPager.this, ActivityLogin.class);
//            Intent intent_login = new Intent(ActivityViewPager.this, ActivityAccount.class);
//            intent_login.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivityForResult(intent_login, MyConfig.REQUEST_LOGIN);
            this.overridePendingTransition(0, 0);
        }
    }

    private void initPush() {

        MyLog.d("","API_USER_PUSH_TOKEN: initPush()" + MyConfig.jpush_regist_id );
        MyConfig.setPushToken();


//        if(MyConfig.FALSE_TAG) {
//            //0:not regist, 1:registed but havn't notify server, 2:notified server
//            if (MyConfig.usr_push_status < 1) {
//                MyConfig.baidupush_startWork();
//            } else if (MyConfig.usr_push_status == 1) {
//                MyConfig.setPushToken();
//            } else {
//
//            }
//        }
    }

    @Override
    public void onPause() {
        super.onPause();
//        EventBus.getDefault().post(new EventDisconnectSocket());
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Unregister
        //EventBus.getDefault().unregister(this);

        // Unbind from the service
        quitService();

    }

    public void quitService() {
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        stopService(new Intent(this, MyService.class));
    }


    public void onEventMainThread(EventRestartService eventRestartService) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                quitService();

                try {
                    Thread.sleep(500);
                } catch (Exception e) {

                }

                initService();

            }
        }).start();

    }

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MyService.LocalBinder binder = (MyService.LocalBinder) service;
            mService = binder.getService();

            MyLog.d("", "playlist: mService = binder.getService();");
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }


    };

    private void initService() {
        // Bind to LocalService
        bindService(new Intent(this, MyService.class), mConnection, Context.BIND_AUTO_CREATE);
        startService(new Intent(this, MyService.class));

//        if (MyConfig.jsonGetCurrentCompanyRet == null) { //todo: 这是什么？为什么有这个判断？
//            MyLog.d("", "initService drive socketReConnect()");
//
//            EventBus.getDefault().post(new EventReConnectSocket());
//        }
    }

    View.OnClickListener radioOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            MyLog.d("","radioGroup.getCheckedRadioButtonId()"+radioGroup.getCheckedRadioButtonId());
            switch (radioGroup.getCheckedRadioButtonId()){
                case R.id.radio_b0:
                    mViewPager.setCurrentItem(0);
                    break;
                case R.id.radio_b2:
                    mViewPager.setCurrentItem(1);
                    break;
                case R.id.radio_b3:
                    mViewPager.setCurrentItem(2);
                    break;
                case R.id.radio_b1:
                    mViewPager.setCurrentItem(3);
                    break;
            }
        }
    };
    private void initViewPager() {

        radioGroup = (RadioGroup)findViewById(R.id.radioGroup);
        radio_b0 = (RadioButton)findViewById(R.id.radio_b0);
        radio_b0.setOnClickListener(radioOnClickListener);
        radio_b1 = (RadioButton)findViewById(R.id.radio_b1);
        radio_b1.setOnClickListener(radioOnClickListener);
        radio_b2 = (RadioButton)findViewById(R.id.radio_b2);
        radio_b2.setOnClickListener(radioOnClickListener);
        radio_b3 = (RadioButton)findViewById(R.id.radio_b3);
        radio_b3.setOnClickListener(radioOnClickListener);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(
                getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);

        // Bind the tabs to the ViewPager
        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setShouldExpand(true);
        tabs.setViewPager(mViewPager);
        tabs.setTextSize((int) getResources().getDimension(R.dimen.viewpager_option_textSize));
        tabs.setIndicatorColorResource(R.color.Yellow);
        tabs.setMinimumHeight(MyConfig.getPxFromDimen(this, R.dimen.viewpager_tabs_height));
        tabs.getLayoutParams().height = MyConfig.getPxFromDimen(this, R.dimen.viewpager_tabs_height);
//        tabs.setUnderlineHeight(5);
        tabs.setIndicatorHeight(6);
        tabs.setBackgroundResource(R.color.xjt_toolbar_blue);
        tabs.setTextColorResource(R.color.White);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updatePagerMenu(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    private void initFragments() {

        fragmentV4TalkList = FragmentV4TalkList.newInstance("group");
        fragmentV4DirectTalkList = _FragmentV4DirectTalkList.newInstance("direct");
        MyLog.d("", "read contact viewpager call newInstance ");
        fragmentV4FavoriteList = _FragmentV4FavoriteList.newInstance();
        fragmentV4Option = _FragmentV4Option.newInstance("setting");
        //fragmentV4CompanyContact = FragmentV4CompanyContact.newInstance("contact");
        fragmentV4CompanyContactTongChuang = FragmentV4CompanyContactTongChuang.newInstance("contact");
        fragmentV4Me = FragmentV4Me.newInstance("myInfo");
        fragmentV4ApplicaitonsNotused = _FragmentV4Applicaitons.newInstance();
        fragmentV4AppVerticalViewpager = FragmentV4AppVerticalViewpager.newInstance();
        fragmentV4VerticalViewpagerTest = _FragmentV4VerticalViewpagerTest.newInstance();
    }

    private void sysInit() {
//        MyConfig.app = getApplicationContext();

        try {
            PackageManager pm = getPackageManager();
            PackageInfo packageInfo = pm.getPackageInfo(this.getPackageName(), 0);
            MyConfig.packageName = packageInfo.packageName;
            MyLog.d("", "app in background: MyConfig.packageName " + MyConfig.packageName);
        } catch (PackageManager.NameNotFoundException e) {
        }


        getScreenSize();
        MyConfig.MSG_PHOTO_PREVIEW_WIDTH = MyConfig.screenWidth / 2;
//            MyConfig.MSG_PHOTO_PREVIEW_HEIGHT = MyConfig.screenHeight / 2;
        MyConfig.MSG_PHOTO_PREVIEW_HEIGHT = MyConfig.screenWidth / 2;


        MyConfig.mkAppDir();
        MyLog.d("", "appCacheDirPath: " + MyConfig.appCacheDirPath);

        MyConfig.loadDebugSettingFromPreference(this);

//        MyConfig.app_lang = MyConfig.getAppLanguage(this);
    }



    private void getScreenSize() {
        MyConfig.wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = MyConfig.wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        MyConfig.screenWidth = size.x;
        MyConfig.screenHeight = size.y;
        MyLog.d("", "screenWidth " + MyConfig.screenWidth + " screenHeight " + MyConfig.screenHeight);

        MyConfig.scale = getResources().getDisplayMetrics().density;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            MyLog.d("", "viewpager get fragment " + position);
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.

            if (position == 0) {
                return fragmentV4TalkList;
            } else if (position == 1) {
                MyLog.d("", "read contact viewpager getItem");
//                return fragmentV4DirectTalkList;
                //return fragmentV4CompanyContact;
                //return  fragmentV4CompanyContactTongChuang;
                return fragmentV4AppVerticalViewpager;
            } else if (position == 2) {
//                return fragmentV4Applicaitons;
//                return fragmentV4CalendarVerticalViewpager;
                return fragmentV4Me;
            }  else {
                return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    //  toolbar.setVisibility(View.VISIBLE);
                    return getString(R.string.pager_groups).toUpperCase(l);
                case 1:
                    // toolbar.setVisibility(View.GONE);
                    return getString(R.string.pager_direct).toUpperCase(l);
                case 2:
                    //  toolbar.setVisibility(View.GONE);
                    return getString(R.string.pager_notification).toUpperCase(l);
                case 3:
                    //  toolbar.setVisibility(View.GONE);
                    return getString(R.string.pager_setting).toUpperCase(l);
//                case 4:
//                    return getString(R.string.pager_4_companies).toUpperCase(l);
            }
            return null;
        }
    }


    public void onEventMainThread(EventViewpagerSetCurrentItemPosition eventViewpagerSetCurrentItemPosition) {

        mViewPager.setCurrentItem(eventViewpagerSetCurrentItemPosition.current_item_position);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_xiaojiti, menu);
        this.menu = menu;

        updatePagerMenu(mViewPager.getCurrentItem());

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_invite:
                Intent intent_invite_friend = new Intent(this, _ActivityInviteFriendFromPhoneContact.class);
                startActivity(intent_invite_friend);
                return true;
            case R.id.action_test:
//                testApi();
//                testGetSubjectList();
//                gotoCompanyInvite();
//                Snackbar.make(mViewPager, "set current company sucess!", Snackbar.LENGTH_LONG).show();
//                testGetUserList();

                int net = MyConfig.netConnectCheck();

                switch (net) {
                    case -1:
                        MyConfig.MyToast(0, this,getResources().getString(R.string.net_unknow));
                        break;
                    case 0:
                        MyConfig.MyToast(0, this, getResources().getString(R.string.net_unconnect));
                        break;
//                    case 1:
//                        MyConfig.MyToast(0, this, "已联网");
//                        break;
                    default:
                        MyConfig.MyToast(0, this, getResources().getString(R.string.net_connect));
                        //   MyConfig.MyToast(0, this, "网络情况不明");
                }
                return true;
            case R.id.action_reconnect_socket:
                EventBus.getDefault().post(new EventReConnectSocket());
                return true;
            case R.id.action_search:
                Intent choose_room = new Intent(ActivityViewPager.this, ActivityGroupList.class);
                startActivityForResult(choose_room, MyConfig.REQUEST_CHOOSE_GROUP);
                return true;
            case R.id.action_create_company:
                Intent create_company = new Intent(ActivityViewPager.this, _ActivityCreateCompany.class);
                startActivityForResult(create_company, MyConfig.REQUEST_CREATE_COMPANY);
                return true;
            case R.id.action_choose_company:
                gotoChooseCompany();
                return true;
            case R.id.action_create_room:
                Intent create_room = new Intent(ActivityViewPager.this, ActivityCreateGroup.class);
                startActivityForResult(create_room, MyConfig.REQUEST_CREATE_ROOM);
                return true;
            case R.id.action_regist:
                Intent intent_regist = new Intent(ActivityViewPager.this, _ActivityRegist.class);
                startActivity(intent_regist);
                return true;
            case R.id.action_login:
                MyLog.d("", "app start  steps 2.1 --- option button login");
                gotoLogin();
                return true;
            case R.id.action_logout:
                MyLog.d("", "app start steps 2.1 --- option button logout");
                userLogout();
                return true;
            case R.id.action_debug:
                Intent intent_advance_setting = new Intent(this, ActivityAdvanceSetting.class);
                startActivity(intent_advance_setting);
                return true;
            case R.id.action_contact:
                Intent intent_contact = new Intent(this, _ActivityCompanyContact.class);
                startActivity(intent_contact);
                return true;
            case R.id.action_search_contact:
                Intent intent_search_contact = new Intent(this, ActivitySearchContactPerson.class);
                startActivity(intent_search_contact);
                return true;
            case R.id.action_contact_download:
                downloadContactConfirm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void downloadContactConfirm(){

        MyConfig.MyDialog.newInstance(getString(R.string.prompt_contact_download), new MyConfig.MyDialog.IF_MyDialogClicked() {
            @Override
            public void dialogButtonClicked() {

                EventBus.getDefault().post(new EventContactDownloadPlease());

            }
        }, new MyConfig.MyDialog.IF_MyDialogClicked() {
            @Override
            public void dialogButtonClicked() {

            }
        }).show(getSupportFragmentManager(), "");


    }


    public void updatePagerMenu(int position) {

        if(menu == null){
            return;
        }
        menu.findItem(R.id.action_invite).setVisible(false);
        menu.findItem(R.id.action_test).setVisible(MyConfig.debug_quick_menu);
        menu.findItem(R.id.action_reconnect_socket).setVisible(MyConfig.debug_quick_menu);
        if (position == 0) {
            menu.findItem(R.id.action_create_room).setVisible(true).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            menu.findItem(R.id.action_search).setVisible(true).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        } else {
            menu.findItem(R.id.action_create_room).setVisible(false);
            menu.findItem(R.id.action_search).setVisible(false);
        }
        menu.findItem(R.id.action_create_company).setVisible(MyConfig.debug_quick_menu);
        menu.findItem(R.id.action_choose_company).setVisible(MyConfig.debug_quick_menu);
        //    menu.findItem(R.id.action_create_room).setVisible(true);
        menu.findItem(R.id.action_regist).setVisible(MyConfig.debug_quick_menu);
        menu.findItem(R.id.action_login).setVisible(MyConfig.debug_quick_menu);
        menu.findItem(R.id.action_logout).setVisible(MyConfig.debug_quick_menu);
        menu.findItem(R.id.action_debug).setVisible(MyConfig.debug_quick_menu);
        if(position == 100) {//不用这个按钮了
            menu.findItem(R.id.action_contact).setVisible(true).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }else{
            menu.findItem(R.id.action_contact).setVisible(false);
        }
        if(position == 1) {

//            menu.findItem(R.id.action_search_contact).setVisible(true).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);;

            if(contact_is_downloading) {
                menu.findItem(R.id.action_contact_download).setVisible(false);
            }else{
                if(MyConfig.usr_type == MyConfig.USER_TYPE_IS_TEACHER) {//只有老师才可能下载整个通讯录
                    menu.findItem(R.id.action_contact_download).setVisible(true).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                }
            }
        }else{
            menu.findItem(R.id.action_contact_download).setVisible(false);
            menu.findItem(R.id.action_search_contact).setVisible(false);
        }

        switch (position) {
            case 0://group
                radio_b0.setChecked(true);
                toolbar.setTitle(getString(R.string.pager_groups_team));
                toolbar.setTitleTextColor(getResources().getColor(R.color.White));
                break;
            case 1://contact
                radio_b2.setChecked(true);
                toolbar.setTitle(getString(R.string.pager_notification));
                toolbar.setTitleTextColor(getResources().getColor(R.color.White));
                break;
            case 2://notification
                radio_b3.setChecked(true);
                toolbar.setTitle(getString(R.string.pager_setting));
                toolbar.setTitleTextColor(getResources().getColor(R.color.White));
                break;
            case 3://setting
                radio_b1.setChecked(true);
                toolbar.setTitle(getString(R.string.pager_groups_talk)+"("+contact_count+"人)");
                toolbar.setTitleTextColor(getResources().getColor(R.color.White));
                break;
        }
    }
    int contact_count;
    @Override
    public void getContactNumBysize(int num) {
        contact_count=num;
    }
    public void userLogout() {

        MyLog.d("", "app start steps 2.11: logout()");

        userLogoutNotifyServer();
        MyLog.d("", "app start steps 2.12: logout()");
        EventBus.getDefault().post(new EventDisconnectSocket());
        MyLog.d("", "app start steps 2.13: logout()");
        MyConfig.logoutClear();
        MyLog.d("", "app start steps 2.14: logout()");
        MyConfig.saveUserDataToPreference(ActivityViewPager.this);
        MyLog.d("", "app start steps 2.15: logout()");
        MyConfig.cancelAllNotification();
        MyLog.d("", "app start steps 2.16 --- userLogout");
        gotoLogin();
        setViewpagerCurrentPageWithDelayToAvoidFlash();

        MyConfig.cleanAppDir();
        /**退出登录后杀掉ActivityViewPager页面*/
       ActivityViewPager.this.finish();
    }


    public void setViewpagerCurrentPageWithDelayToAvoidFlash() {

        /**
         * 使用延迟这种笨办法，使得login页面完全启动并覆盖屏幕之后再让viewpager切换fragment
         * 以此防止屏幕闪动
         */

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (Exception e) {

                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MyLog.d("", "logout and setCurrentItem(0)");
                        mViewPager.setCurrentItem(0);//下次登入时直接是群组讨论页面
                    }
                });
            }
        }).start();
    }

    public void userLogoutNotifyServer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                MyLog.d("", "backprocess at userLogout() 0");
                Gson gson = new Gson();

                JsonLogout jsonLogout = new JsonLogout();
                jsonLogout.accessToken = MyConfig.usr_token;
                jsonLogout.os = MyConfig.os_string;
                jsonLogout.uuid = MyConfig.uuid;

                String jsonStr = gson.toJson(jsonLogout, JsonLogout.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LOGOUT, jsonStr);
                MyLog.d("", "HttpTools return string: " + s);

                final JsonLogoutRet jsonLogoutRet = gson.fromJson(s, JsonLogoutRet.class);
                if (jsonLogoutRet != null) {

//                    if(jsonLogoutRet.code==MyConfig.retSuccess() && jsonLogoutRet.message.equals("Success")){
                    if (jsonLogoutRet.code==MyConfig.retSuccess()) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });

                    }

                }
                MyLog.d("", "apitest: send msg ret " + s);

            }
        }).start();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        MyLog.d("", "onActivityResult hit activity! request code " + requestCode + " result code " + resultCode);

        super.onActivityResult(requestCode, resultCode, data);

        /**
         * 登入
         */
        if (requestCode == MyConfig.REQUEST_LOGIN && resultCode == RESULT_OK) {

            MyLog.d("", "socketio: login sucess");
//            socketConnect();


            /**
             * 有多个地方需要更新room list
             *  启动后直接处于已经login状态
             *  加入或退出room
             *  登入
             *  创建room
             *  如果有刷新按钮
             */
//            EventBus.getDefault().post(new EventLogin());
//不在这里发送了，改在ActivityLogin里面发送

        }

        /**
         * 创建company
         */
        if (requestCode == MyConfig.REQUEST_CREATE_COMPANY && resultCode == RESULT_OK) {
            MyLog.d("", "socketio: create company and back to viewpager");
            EventBus.getDefault().post(new EventCompanyCreated());

        }

        /**
         * 创建room
         */
        if (requestCode == MyConfig.REQUEST_CREATE_ROOM && resultCode == RESULT_OK) {
            MyLog.d("", "socketio: create room and back to viewpager");
            EventBus.getDefault().post(new EventGroupListUpdateFromServer());

        }

        /**
         * 设置当前公司
         */
        if (requestCode == MyConfig.REQUEST_COMPANY_INVITE) {
            MyLog.d("", "app start steps 3.2, clear flag_open_invite_code_activity");
            MyConfig.flag_open_invite_code_activity = 0;

            if (resultCode == RESULT_CANCELED) {
                //用户没有设定当前公司，则登出用户
                //userLogout();
                //或者什么也不做，在viewpager的resume中进入company views页面
            } else {
                //do nothing
            }
        }

    }


    public void onEventMainThread(EventGotoChooseCompany eventGotoChooseCompany) {
        MyLog.d("", "eventbus: onEventMainThread in ViewPager.java: EventGotoChooseCompany");

        gotoChooseCompany();
    }

    public void onEventMainThread(EventGroupListUpdateFromServer eventGroupListUpdateFromServer) {
        MyLog.d("", "eventbus: onEventMainThread in ViewPager.java: EventCompanyListUpdate");

        groupListUpdateFromServer();
    }


    public void onEventMainThread(EventLogout eventLogout) {
        MyLog.d("", "eventbus: onEventMainThread in ViewPager.java: EventLogout");
        MyLog.d("", "app start steps 2.1 --- viewpager --- EventLogout eventLogout");
        if (MyConfig.getErrorMsg("" + eventLogout.event_type).length() >0 && MyConfig.usr_status != MyConfig.USR_STATUS_UNKNOWN){
            //暂时注释掉后台发来的err msg: 请先登录
//            MyConfig.MyToast(1, this, MyConfig.getErrorMsg("" + eventLogout.event_type));
        }
//        if (eventLogout.event_type == MyConfig.ERROR_INVALID_TOKEN) {
//            MyConfig.MyToast(0, this, MyConfig.getErrorMsg("" + MyConfig.ERROR_INVALID_TOKEN));
//        }
        userLogout();
    }

    public void onEventMainThread(EventLogin eventLogin) {
        MyLog.d("", "eventbus: onEventMainThread in ViewPager.java: EventLogin");

        //这里应该是重走onResume()流程?

    }


    public void onEventMainThread(EventNetConnectStatusChanged eventNetConnectStatusChanged) {

        if (MyConfig.net_status >= 1) {
            MyLog.d("", "app start steps: EventNetConnectStatusChanged");

            if(MyConfig.usr_status >= MyConfig.USR_STATUS_LOGIN) {
                MyLog.d("", "app start steps: EventNetConnectStatusChanged cause get current company " + MyConfig.usr_status);
                //重新联网后，如果已经登录，则获取当前公司
                MyConfig.getCurrentCompany();
            }
        }
    }

    public void onEventMainThread(EventHaveSetCurrentCompany eventHaveSetCurrentCompany) {

        MyLog.d("choosecompany", "app start steps: viewpager get EventHaveSetCurrentCompany ");

        if (eventHaveSetCurrentCompany.success) {
            MyConfig.getCurrentCompany();
        }

    }

    public void onEventMainThread(EventHaveGetCurrentCompany eventHaveGetCurrentCompany) {

        MyLog.d("choosecompany", "company change flow: viewpager get EventHaveGetCurrentCompany, code:"+eventHaveGetCurrentCompany.code+" companyId=" + eventHaveGetCurrentCompany.companyId);

        /**
         * code
         *  0:  得到当前公司
         *  1:  失败
         *  3:  未授权
         */
        if (eventHaveGetCurrentCompany.code == MyConfig.retSuccess()) {

            MyConfig.getCurrentCompany_failures = 0;

            MyLog.d("", "app start steps: ActivityViewpager eventHaveGetCurrentCompany.code success");

//            Toast.makeText(this, getString(R.string.prompt_get_current_company_success), Toast.LENGTH_SHORT).show();
            MyConfig.MyToast(-1, this, getString(R.string.prompt_get_current_company_success));

            getUserDataAfterCurrentCompany();
//            mViewPager.setCurrentItem(0);

        } else {

            MyConfig.getCurrentCompany_failures++;
            MyLog.d("", "app start steps: ActivityViewpager eventHaveGetCurrentCompany.code failed");

            if(MyConfig.getCurrentCompany_failures < 10){
                checkCurrentCompany();
            }else {
                if(MyConfig.have_company_view) {
//                gotoCompanyViews();
                }
            }

        }
//        else if (eventHaveGetCurrentCompany.code == -1) {
//            /**
//             * 可能网络不通
//             */
////            Toast.makeText(this, getString(R.string.net_msg_not_connected), Toast.LENGTH_SHORT).show();
//            MyConfig.MyToast(0, this, getString(R.string.net_msg_not_connected));
//        } else {
//
////            Toast.makeText(this, getString(R.string.prompt_get_current_company_failed), Toast.LENGTH_SHORT).show();
//            MyConfig.MyToast(-1, this, getString(R.string.prompt_get_current_company_failed));
//            gotoLogin();
//
//        }
    }

    public void onEventMainThread(EventCompanyJoined eventCompanyJoined) {

        if (eventCompanyJoined.companyId > 0) {
            MyConfig.companyListUpdateFromServer();
        }

    }

    public void onEventMainThread(EventCompanyInfoUpdated eventCompanyInfoUpdated) {
        MyLog.d("", "updateCompanyInfoRefresh 2: EventCompanyListUpdate");

        MyConfig.companyListUpdateFromServer();

    }

    public void onEventMainThread(EventCompanyCreated eventCompanyCreated) {
        MyLog.d("", "eventbus: onEventMainThread in ViewPager.java: EventCompanyListUpdate");

        MyConfig.companyListUpdateFromServer();

    }

    public void onEventMainThread(EventKick e) {
        String groupName = new String("");
        for (int i = 0; i < MyConfig.jsonUserGroupsRet.data.size(); i++) {
            if (e.getMsgRet().groupId == MyConfig.jsonUserGroupsRet.data.get(i).id) {
                groupName = MyConfig.jsonUserGroupsRet.data.get(i).name;
                break;
            }
        }
        MyConfig.MyToast(1, this, getString(R.string.maingroup_kick_remind) + groupName);
        if (null != MyConfig.activityMainGroup && MyConfig.activityMainGroup.groupId == e.getMsgRet().groupId) {
            Intent intent = new Intent(this, ActivityViewPager.class);
            startActivity(intent);
        }
    }

    public void checkCurrentCompany() {

        MyLog.d("choosecompany", "app start steps: checkCurrentCompany() ");

        MyConfig.getCurrentCompany();

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                /**
//                 * 获取当前company
//                 */
//                String current_company = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GET_CURRENT_COMPANIES, MyConfig.usr_token);
//                MyLog.d("", "eventbus: checkCurrentCompany " + current_company);
//
//                Gson gson = new Gson();
//                JsonGetCurrentCompanyRet jsonGetCurrentCompanyRet = gson.fromJson(current_company, JsonGetCurrentCompanyRet.class);
//
//                if (jsonGetCurrentCompanyRet != null && jsonGetCurrentCompanyRet.code==MyConfig.retSuccess()) {//收到新的当前公司
//
//                    if (MyConfig.jsonGetCurrentCompanyRet != null && //已经有当前公司
//                            MyConfig.jsonGetCurrentCompanyRet.data.id != jsonGetCurrentCompanyRet.data.id) { //当前公司有改变
//                        /**
//                         * 根据后台的需求，当前公司变化要重连socket
//                         */
//                        MyLog.d("", "current company change drive socketReConnect()");
//                        EventBus.getDefault().post(new EventReConnectSocket());
//
//                    }
//
//                    MyConfig.jsonGetCurrentCompanyRet = jsonGetCurrentCompanyRet;
//
//                    MyLog.d("", "USR_STATUS ACTIVITYVIEWPAGER checkCurrentCompany" + MyConfig.usr_status);
//                    MyConfig.updateUserStatus(2);
//                    MyConfig.flag_viewpager_start_______seems_no_use = 2;
//
//                    updateCompanyAndGroupAfterSetCurrentCompany();
//
//                } else {
//                    gotoCompanyInvite();
//                }
//
//            }
//        }).start();

    }


    public void gotoCompanyViews() {
        MyLog.d("", "app start steps: ActivityViewpager gotoCompanyViews");
        Intent intent = new Intent(ActivityViewPager.this, _ActivityCompanyView.class);
        startActivityForResult(intent, MyConfig.REQUEST_COMPANY_VIEWS);
    }


    public void gotoCompanyInvite() {
        MyLog.d("", "regist and invite, 3: gotoCompanyInvite " + MyConfig.flag_open_invite_code_activity);
        Intent intent = new Intent(ActivityViewPager.this, _ActivityInputCompanyInviteCode.class);
        intent.putExtra("mode", 3);
        startActivityForResult(intent, MyConfig.REQUEST_COMPANY_INVITE);
    }

    public void gotoChooseCompany() {

        Intent intent = new Intent(ActivityViewPager.this, _ActivityChooseCompany.class);
        startActivityForResult(intent, MyConfig.REQUEST_CHOOSE_COMPANY);
    }

    private void groupListUpdateFromServer() {

        MyLog.d("choosecompany", "company change flow: viewpager grouplist refresh");

        /**
         * 更新本地的group list列表
         */

        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();

                /**
                 * 获取可以加入，但还没有加入的rooms
                 */
                String rooms_can_join = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_CAN_JOIN, MyConfig.usr_token);
                MyLog.d("", "apitest: get rooms can join ret " + rooms_can_join);
                MyConfig.jsonGroupCanJoinRet = gson.fromJson(rooms_can_join, JsonGroupCanJoinRet.class);
                if (MyConfig.jsonGroupCanJoinRet != null) {
                    MyLog.d("", "apitest: get rooms can join ret, code " + MyConfig.jsonGroupCanJoinRet.code);
                }
//                    EventBus.getDefault().post(new EventRoomCanJoin());
//                    MyLog.d("", "eventbus: EventBus.getDefault().post in ActivityViewPager.java");

                /**
                 * 获取已经加入的rooms
                 */
                String user_rooms = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_ROOMS, MyConfig.usr_token);
                MyLog.d("", "apitest: user rooms " + user_rooms);
                MyConfig.jsonUserGroupsRet = gson.fromJson(user_rooms, JsonUserGroupsRet.class);

                if (MyConfig.jsonUserGroupsRet != null && MyConfig.jsonUserGroupsRet.code==MyConfig.retSuccess()) {
                    EventBus.getDefault().post(new EventGroupListUpdateUi());
                }

                MyLog.d("", "grouplist refresh ActivityViewPager groupListUpdateFromServer 2");

            }
        }).start();

    }


    /**
     * todo
     * 1，下面一部分比较啰嗦，考虑是否可以不区分类型，直接把socket msg添加到jsonUserGroupsRet
     * 2，尚未接受部分类型消息：
     * 1，群组创建，有人加入和退出
     * 2，私信中的图片和地图信息
     */
    public void onEventMainThread(JsonGetMsgRet.GetMsgRet json_socket_msg) {
        JsonLastMsgUniversal jsonLastMsgUniversal = JsonLastMsgUniversal.fromJsonSocketMsg(json_socket_msg);//构建最后一条信息
        groupListUpdateFromSocket(json_socket_msg.groupId, jsonLastMsgUniversal);
    }

    public void onEventMainThread(JsonGetMsgTopicRet.GetMsgTopicRet json_socket_topic) {
        JsonLastMsgUniversal jsonLastMsgUniversal = JsonLastMsgUniversal.fromJsonSocketTopic(json_socket_topic);
        groupListUpdateFromSocket(json_socket_topic.groupId, jsonLastMsgUniversal);
    }

    public void onEventMainThread(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet json_socket_topicComment) {
        JsonLastMsgUniversal jsonLastMsgUniversal = JsonLastMsgUniversal.fromJsonSocketTopicComment(json_socket_topicComment);
        groupListUpdateFromSocket(json_socket_topicComment.groupId, jsonLastMsgUniversal);
    }

    public void onEventMainThread(Json_DirectRoom_GetTopicImageRet.DirectRoom_GetTopicImageRet json_socket_directroom_image) {//私信里面的图片及地图
        JsonLastMsgUniversal jsonLastMsgUniversal = JsonLastMsgUniversal.fromJsonSocketDirectroomImage(json_socket_directroom_image);
        groupListUpdateFromSocket(json_socket_directroom_image.groupId, jsonLastMsgUniversal);
    }

    /**
     * 最初从http获取，保存在MyConfig.jsonUserGroupsRet，最后一条信息在messages中，格式是JsonMsgUniversal
     * 格式流程：jsonUserGroupsRet.JsonMsgUniversal --> Group.fromUserGroupsRet --> adapter按类别解释
     * 增加新类别后，在『Group.fromUserGroupsRet』这一步做一条虚假的信息用于显示
     * <p/>
     * 其后从socket获得新信息，更新未读数目，以及更换最后一条信息
     * 格式流程：socket --> JsonMsgUniversal.fromJsonSocketTopicComment --> jsonUserGroupsRet.JsonMsgUniversal --> Group --> adapter按类别解释
     * 增加新类别后，仅仅在『JsonMsgUniversal.fromJsonSocketTopicComment』这一步做一个虚假的消息用于显示，并且放在userGroupsRet.messages.getLast().info.message中
     */
    public void groupListUpdateFromSocket(int groupId, JsonLastMsgUniversal jsonLastMsgUniversal) {
        if (MyConfig.jsonUserGroupsRet != null) {
            for (JsonUserGroupsRet.UserGroupsRet urr : MyConfig.jsonUserGroupsRet.data) {
                if (urr.id == groupId) {
                    MyLog.d("", "update grouplist from socket " + urr.id);
                    /**
                     * 更新未读数
                     */
                    urr.unreadCount++;
                    /**
                     * 更新最后一条信息相关数据
                     */
                    urr.messages.clear();
                    urr.messages.add(jsonLastMsgUniversal);
                }
            }
            EventBus.getDefault().post(new EventGroupListUpdateUi());
        } else {
            MyLog.d("", "grouplist refresh groupListUpdateFromSocket failed");
        }
    }

    public void onEventMainThread(EventPushOnBind eventPushOnBind) {

        MyLog.d("","API_USER_PUSH_TOKEN: httpURLConnPostJson() -2: " + MyConfig.jpush_regist_id);

        MyConfig.setPushToken();

    }



    private boolean backPressed = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            Toast.makeText(this, getResources().getString(R.string.sys_quit_notice), Toast.LENGTH_SHORT).show();
            MyConfig.MyToast(-1, this, getResources().getString(R.string.sys_quit_notice));

            if (backPressed == true) {
                finish();
            } else {
                backPressed = true;
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        backPressed = false;
                    }
                }, 2000);

            }
            return true; // 让系统不再处理这次回退键，因为系统收到回退键后会立即把app关了
        }
        return super.onKeyDown(keyCode, event);
    }


    public void onEventMainThread(EventAppVersionCheckInfo eventAppVersionCheckInfo) {

        /**
         * app版本检测及更新提醒策略
         *
         *  每隔10分钟检测一次服务器提供的版本
         *      today_server是今天服务器上的最新版本，其他含义类似
         *
         *  -------------------------------------------
         *            本地            服务器
         *  今天      current_local   current_server
         *  昨天      last_local      last_server
         *  -------------------------------------------
         *
         *  if (current_server > last_server) //如果last_server有效的话。初始状态为-1，这是无效的
         *      立即提醒
         *  else if(current_server > current_local)
         *      按day提醒
         *
         *  记录更新
         *      last_local = current_local
         *      last_server = current_server
         *
         *  效果
         *      发布新版本时立即提醒
         *      其后按天提醒
         *      直到用户更新
         */

        if ((eventAppVersionCheckInfo.server_last_versionCode >= 0)//如果last_server有效
            &&(eventAppVersionCheckInfo.server_current_versionCode > eventAppVersionCheckInfo.server_last_versionCode)) {

            MyConfig.appVersionUpdateReminder();
            MyConfig.appVersion_reminded_day = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
            MyConfig.saveAppVersionRemindedDay(this);

        } else if (eventAppVersionCheckInfo.server_current_versionCode > eventAppVersionCheckInfo.local_versionCode) {

            MyConfig.loadAppVersionRemindedDay(this);
            String today_remind = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
            if (today_remind.equals(MyConfig.appVersion_reminded_day)) {

            } else {
                MyConfig.appVersionUpdateReminder();

                MyConfig.appVersion_reminded_day = today_remind;
                MyConfig.saveAppVersionRemindedDay(this);
            }

        }

    }


    Dialog dialog_newbie;

    private void newbie_2() {

        MyConfig.app_open_count_newbie_guide_2 = MyConfig.loadIntFromPreference(ActivityViewPager.this, "app_open_count_newbie_guide_2");

        if (MyConfig.app_open_count_newbie_guide_2 == -1) {

            MyConfig.app_open_count_newbie_guide_2 = 1;
            MyConfig.saveIntToPreference(ActivityViewPager.this, "app_open_count_newbie_guide_2", MyConfig.app_open_count_newbie_guide_2);

            dialog_newbie = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
            ImageView view = new ImageView(this);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog_newbie.dismiss();
                }
            });
            Picasso.with(this).load(R.drawable.newbie_guide_frist)
                    .fit()
                    .into(view);
            dialog_newbie.setContentView(view);
            dialog_newbie.show();

            return;
        }
    }

    boolean contact_is_downloading;
    public void onEventMainThread(final EventContactDownloadList eventContactDownloadList) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java");

        if(eventContactDownloadList.flag == EventContactDownloadList.DOWNLOAD_END) {
            contact_is_downloading = false;

            String format = getString(R.string.prompt_contact_download_end);
            String result = String.format(format, eventContactDownloadList.successed, eventContactDownloadList.failed);

            MyConfig.MyToast(1, this, result);
        }else if(eventContactDownloadList.flag == EventContactDownloadList.DOWNLOAD_BEGIN){
            contact_is_downloading = true;
            MyConfig.MyToast(1, this, getString(R.string.prompt_contact_download_begin));
        }
        updatePagerMenu(mViewPager.getCurrentItem());

    }

    public void onEventMainThread(EventLightNotificationRedDot eventLightNotificationRedDot) {

        if(eventLightNotificationRedDot.lightRedDot){
            radio_b2.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.radio_application_selector_red_dot), null, null);
        }else{
            radio_b2.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.radio_application_selector), null, null);
        }

    }



    public void onEventMainThread(EventGroupListUpdateUi eventGroupListUpdateUi) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java");

        lightUnReadRedDot();
    }


    private void lightUnReadRedDot() {

        if (MyConfig.usr_status < MyConfig.USR_STATUS_HAVE_CURRENT_COMPANY) {
            return;
        }

        boolean haveUnRead = false;
        if (MyConfig.jsonUserGroupsRet != null
                && MyConfig.jsonUserGroupsRet.code == MyConfig.retSuccess()
                && MyConfig.jsonUserGroupsRet.data != null) {
            for (JsonUserGroupsRet.UserGroupsRet urr : MyConfig.jsonUserGroupsRet.data) {
                if (urr.category == MyConfig.SERVER_ROOM_CATEGORY_NORMAL
                        || urr.category == MyConfig.SERVER_ROOM_CATEGORY_PRIVATE
                        || urr.category == MyConfig.SERVER_ROOM_CATEGORY_DIRECT
                        || urr.category == MyConfig.SERVER_ROOM_CATEGORY_CLASS
                        ) {
                    Group group = Group.fromUserGroupsRet(urr);
                    if(group.unreadCount > 0){
                        haveUnRead = true;
                        break;
                    }
                }
            }
        }

        if(haveUnRead){

            radio_b0.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.radio_group_selector_red_dot), null, null);

        }else{

            radio_b0.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.radio_group_selector), null, null);

        }

    }


    public void onEventMainThread(EventFileDownloadProgress eventFileDownloadProgress) {

        if (eventFileDownloadProgress.downloadFileFromUrlSenderInfo.sender.equals(ActivityGallery.class.getName())) {

            /**
             * 这里似乎不用了？ActivityGallery直接获取bitmap并且写入本地文件，应该不走这里了
             */

            if(eventFileDownloadProgress.progress == 100) {

                MyLog.d("", "share out, onEventMainThread gallery  " + eventFileDownloadProgress.downloadFileFromUrlSenderInfo.sender);

                File f = new File(eventFileDownloadProgress.des);

                //让系统扫描
                MediaScannerConnection.scanFile(this, new String[] {MyConfig.appDirPath}, null, new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri){
                            }
                        });

                MyConfig.shareImageOnly(this, MyConfig.ATTACHED_TYPE_UNKNOWN, f, eventFileDownloadProgress.downloadFileFromUrlSenderInfo.code);

                /**
                 * 通知主聊天室及子聊天室更新
                 */
                MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo downloadFileFromUrlSenderInfo_to_mainroom = new MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo(ActivityMainGroup.class.getName(), MyConfig.ATTACHMENT_ACTION_DO_NOTHING);
                MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo downloadFileFromUrlSenderInfo_to_subroom = new MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo(ActivitySubGroup.class.getName(), MyConfig.ATTACHMENT_ACTION_DO_NOTHING);
                EventBus.getDefault().post(new EventFileDownloadProgress(eventFileDownloadProgress.src, eventFileDownloadProgress.des, 100, MyConfig.FILE_DOWNLOAD_STOPED, downloadFileFromUrlSenderInfo_to_mainroom));
                EventBus.getDefault().post(new EventFileDownloadProgress(eventFileDownloadProgress.src, eventFileDownloadProgress.des, 100, MyConfig.FILE_DOWNLOAD_STOPED, downloadFileFromUrlSenderInfo_to_subroom));

            }

        }

    }


}
