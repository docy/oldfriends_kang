package co.docy.oldfriends.Activitis;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

import co.docy.oldfriends.Adapters.ListAdapterFriendCircle;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.FriendCricleMsg;
import co.docy.oldfriends.EventBus.EventFriendCricleDelDetail;
import co.docy.oldfriends.Messages.JsonFriendCricleChangeLike;
import co.docy.oldfriends.Messages.JsonFriendCricleRet;
import co.docy.oldfriends.Messages.JsonFriendCricleSocketRet;
import co.docy.oldfriends.Messages.JsonFriendCricleWithHeadRet;
import co.docy.oldfriends.Messages.JsonFriendCricleCommonRet;
import co.docy.oldfriends.Messages.JsonGetNewCommentRet;
import co.docy.oldfriends.Messages.JsonNoticeIntoCricle;
import co.docy.oldfriends.Messages.JsonFriendCricleSendNewContentRet;
import co.docy.oldfriends.Messages.JsonFriendCricleSendComment;
import co.docy.oldfriends.Messages.JsonSignFriendCricleMoveTime;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import co.docy.oldfriends.emoji.EmojiFragment;
import co.docy.oldfriends.emoji.EmojiUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by khp on 2016/7/21.
 * 朋友圈主页
 */
public class ActivityFriendCircle extends ActivityBase implements View.OnClickListener{
    PopupWindow popupWindow;
    ListAdapterFriendCircle listAdapterCircleOfFriends;
    LinkedList<FriendCricleMsg> list = new LinkedList();
    JsonFriendCricleRet jsonCricleOfFriendRet;
    SwipyRefreshLayout swipyrefreshlayout;
    ImageView bg_friend,myphoto_friend,unRead_imgae_friend,image_expression,myfinish;
    TextView myname_friend,send_comment,unRead_msg,mychoose;
    LinearLayout unRead_linear,title_linear;
    ListView listview;
    EditText et;
    int page =1;
    int comment_postion;
    int dialogSelectPosition = -1;
    private FrameLayout fl_emoji_fragment;
    private EmojiFragment _fragment;
    private boolean emojiIsVisible = false;

    File tempAttach;
    Dialog  mDialog;
    TextView state_friend,video_friend,image_friend,camera_friend,cancel_friend;

    LinearLayout comment_linear;
    View header;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circle_of_friends);
        getSupportActionBar().hide();
       /*刷新代码*/
        swipyrefreshlayout =(SwipyRefreshLayout) findViewById(R.id.activtiy_cricle_of_friend_refresh);
        swipyrefreshlayout.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
        swipyrefreshlayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if(direction==SwipyRefreshLayoutDirection.TOP){
                    swipyrefreshlayout.setRefreshing(false);

                }else if(direction==SwipyRefreshLayoutDirection.BOTTOM){
                    getDataOfFriend(page);

                }
            }
        });
        myfinish =(ImageView) findViewById(R.id.activtiy_cricle_of_friend_finish);
        mychoose =(TextView)findViewById(R.id.activtiy_cricle_of_friend_choose);
        title_linear =(LinearLayout) findViewById(R.id.activtiy_cricle_of_friend_title);

        fl_emoji_fragment = (FrameLayout) findViewById(R.id.activtiy_cricle_of_friend_fl_emoji_fragment);
        listview =(ListView) findViewById(R.id.activtiy_cricle_of_friend_recycler);
        listAdapterCircleOfFriends = new ListAdapterFriendCircle(this,list,changeLike);
        header = LayoutInflater.from(this).inflate(R.layout.circle_of_friends_headview,null);
        listview.addHeaderView(header);
        listview.setAdapter(listAdapterCircleOfFriends);
        comment_linear =(LinearLayout) findViewById(R.id.activtiy_cricle_of_friend_comment_layout);
        et =(EditText) findViewById(R.id.activtiy_cricle_of_friend_comment_edit);
        _fragment = (EmojiFragment) getSupportFragmentManager().findFragmentById(R.id.emoji_fragment_main);
        _fragment.setEditTextHolder(et);
        send_comment =(TextView) findViewById(R.id.activtiy_cricle_of_friend_comment_send);
        image_expression =(ImageView) findViewById(R.id.activtiy_cricle_of_friend_comment_image);
        image_expression.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emojiIsVisible){
                    hideEmojiDialog();
                }else {
                    showEmojiDialog();
                }

            }
        });
        initdata(header);
        getDataOfFriend(page);
        initDataWithHead();
        NoticeIntoCricle(true);
        getNewDataFromServer();
    }

    @Override
    public void onResume() {
        super.onResume();
        send_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCommentToServer();
            }
        });
        et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fl_emoji_fragment.setVisibility(View.GONE);
                image_expression.setImageResource(R.drawable.emoji_button_3x);
            }
        });


        listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            //滑动时隐藏键盘
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:// 滚动时
                        hideKeyboard();
                        fl_emoji_fragment.setVisibility(View.GONE);
                        image_expression.setImageResource(R.drawable.emoji_button_3x);
                        break;
                }
            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(-header.getTop() > MyConfig.dp2Px(ActivityFriendCircle.this,180)){
                    title_linear.setBackgroundResource(R.drawable.c_bg_calendar_clip_3x);
                }else{
                    title_linear.setBackgroundResource(R.color.transparent);
                }
            }
        });
        myfinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mychoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosefriendaction();
            }
        });

    }

    public void DownloadVideo(FriendCricleMsg msg){
        //视频下载
        if(msg.videoPath!=null) {
            new MyConfig.DownloadFileFromUrl(
                    new MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo("好友圈", MyConfig.ATTACHMENT_ACTION_DO_NOTHING))
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                            MyConfig.getApiDomain_NoSlash_GetResources() + msg.videoPath,//在服务器的地址
                            msg.videoPath_local);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NoticeIntoCricle(false);
        //标记离开朋友圈的时间
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                JsonSignFriendCricleMoveTime jsonSignFriendCricleMoveTime = new JsonSignFriendCricleMoveTime();
                jsonSignFriendCricleMoveTime.accessToken = MyConfig.usr_token;
                String strMovetime = gson.toJson(jsonSignFriendCricleMoveTime,JsonSignFriendCricleMoveTime.class);
                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2()+MyConfig.API_SIGN_FRIEND_CRICLE_MOVE_TIME ,strMovetime);
                MyLog.d("查看朋友圈离开的时间",s);
            }
        }).start();
    }

    public void initdata(View view){
        //加载listview上面的数据
        bg_friend =(ImageView) view.findViewById(R.id.activtiy_cricle_of_friend_bg);
        myphoto_friend =(ImageView) view.findViewById(R.id.activtiy_cricle_of_friend_myphoto);
        myname_friend =(TextView) view.findViewById(R.id.activtiy_cricle_of_friend_myname);
        unRead_imgae_friend =(ImageView) view.findViewById(R.id.activtiy_cricle_of_friend_unread_image);
        unRead_msg =(TextView) view.findViewById(R.id.activtiy_cricle_of_friend_unread_msg);
        unRead_linear =(LinearLayout) view.findViewById(R.id.activtiy_cricle_of_friend_unread_linear);
        myname_friend.setText(MyConfig.usr_nickname);
        Picasso.with(this).load(MyConfig.getApiDomain_NoSlash_GetResources() + MyConfig.usr_avatar)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(myphoto_friend);
        bg_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDataWithHead();
            }
        });

        myphoto_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.gotoUserInfo(ActivityFriendCircle.this,MyConfig.usr_id);
            }
        });

        unRead_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unRead_linear.setVisibility(View.GONE);
                Intent intent = new Intent(ActivityFriendCircle.this,ActivityFriendCricleNewComment.class);
                startActivity(intent);
                count = 0;

            }
        });
    }


    private void hideEmojiDialog() {
        fl_emoji_fragment.setVisibility(View.GONE);
        image_expression.setImageResource(R.drawable.emoji_button_3x);
        EmojiUtils.show_keyboard_must_call_from_activity(this);
        emojiIsVisible = false;
    }

    /**
     * 显示表情选择框
     */
    private void showEmojiDialog() {
        /**EditText没有获取焦点时点击事件不执行*/
        if(!et.hasFocus()){
            et.setFocusable(true);
            et.requestFocus();
        }
        EmojiUtils.hide_keyboard_must_call_from_activity(this);
        SystemClock.sleep(500);
        fl_emoji_fragment.setVisibility(View.VISIBLE);
        image_expression.setImageResource(R.drawable.button_keyboard);
        emojiIsVisible = true;
    }




    public void NoticeIntoCricle(final boolean tag){
        //通知进入或者退出朋友圈
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                JsonNoticeIntoCricle intoCricle = new JsonNoticeIntoCricle();
                intoCricle.accessToken = MyConfig.usr_token;
                intoCricle.reading = tag ;
                String strInto = gson.toJson(intoCricle,JsonNoticeIntoCricle.class);
                String s  = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2()+MyConfig.API_IN_OR_OUT_CRICLE , strInto);
                MyLog.d("kanghongpu","进入朋友圈"+s);
            }
        }).start();
    }

    public void getNewDataFromServer(){
        //获取别人对自己的评论
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2()+MyConfig.API_GET_NEW_DATA_SIGN ,MyConfig.usr_token);
                MyLog.d("kanghongpu","查看和自己有关的数据"+s);
                final JsonGetNewCommentRet jsonGetNewCommentRet = gson.fromJson(s,JsonGetNewCommentRet.class);
                if(jsonGetNewCommentRet!=null&&jsonGetNewCommentRet.code==MyConfig.retSuccess()&&jsonGetNewCommentRet.data!=null){
                    if(jsonGetNewCommentRet.data.count>0){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                unRead_linear.setVisibility(View.VISIBLE);
                                unRead_msg.setText(jsonGetNewCommentRet.data.count+"条新评论");
                                Picasso.with(ActivityFriendCircle.this).load(MyConfig.getApiDomain_NoSlash_GetResources() + jsonGetNewCommentRet.data.avatar)
                                        .fit()
                                        .centerCrop()
                                        .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                                        .into(unRead_imgae_friend);
                            }
                        });
                    }
                }

            }
        }).start();
    }

    public void setDataWithHead(){
        //更换listview上面的图片
        AlertDialog.Builder builder = new AlertDialog.Builder(this,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
//        builder.setTitle("请选择");
        dialogSelectPosition = 0;

        String[] dialogArray =  new String[]{"从手机相册中选择", "拍照"};

        builder.setSingleChoiceItems(dialogArray, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogSelectPosition = which;
            }
        });
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setDialogPositiveButtonClick();
            }
        });
        builder.setNegativeButton("取消", null);
        AlertDialog dialog = builder.create();// 只创建，不显示，显示之前可以做其他操作。
        dialog.show();
    }
    private void setDialogPositiveButtonClick() {
        switch (dialogSelectPosition) {
            case -1:
                break;
            case 0:
                openFileChooserImage(MyConfig.MIME_IMAGE_STAR);
                break;
            case 1:
                openCamera();
                break;
        }
    }
    public void initDataWithHead(){
        //获取listview上面的图片
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                HashMap<String,String> map = new HashMap<>();
                map.put("userId",MyConfig.usr_id+"");
                String s = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2() +
                        MyConfig.API_CHANGE_FRIEND_LIKE + MyConfig.API_GET_DATA_OF_FRIEND_WITH_HEAD ,map,MyConfig.usr_token);
                MyLog.d("获取头部图片",s);
                final JsonFriendCricleWithHeadRet jsonFriendCricleWithHead = gson.fromJson(s,JsonFriendCricleWithHeadRet.class);
                if(jsonFriendCricleWithHead!=null&&jsonFriendCricleWithHead.code == MyConfig.retSuccess()
                        &&jsonFriendCricleWithHead.data!=null){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Glide.with(ActivityFriendCircle.this).load(MyConfig.getApiDomain_NoSlash_GetResources() + jsonFriendCricleWithHead.data.profilePicture)
                                    .centerCrop()
//                                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                                    .into(bg_friend);
                        }
                    });
                }
            }
        }).start();
    }


    ListAdapterFriendCircle.ChangeFriendLikes changeLike = new ListAdapterFriendCircle.ChangeFriendLikes() {
        @Override
        public void changeFriendLikesListen(int postion,boolean tag,int diff) {
            if(diff==0){
                changeLike(list.get(postion).id,tag,postion);
            }else if(diff==1){
                delFriendOfCricleMsg(list.get(postion).id,postion);
            }else if(diff==2){
                sendComment(postion);
            }else if(diff==3){
                Intent intent  = new Intent(ActivityFriendCircle.this,ActivityFriendCricleDetail.class);
                intent.putExtra("id",list.get(postion).id);
                startActivity(intent);
            }
        }
    };
    public void sendComment(final int postion){
//        弹出键盘
        comment_linear.setVisibility(View.VISIBLE);

        et.setFocusable(true);
        et.requestFocus();
        MyConfig.show_keyboard_must_call_from_activity(this);
        comment_postion = postion;
    }
    public void sendCommentToServer(){
        final String str = et.getText().toString();
        if(str!=null&&str.length()>0) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Gson gson = new Gson();
                    final JsonFriendCricleSendComment jsonSendFriendsComment = new JsonFriendCricleSendComment();
                    jsonSendFriendsComment.accessToken = MyConfig.usr_token;
                    jsonSendFriendsComment.comment = EmojiUtils.replaceNameByField(ActivityFriendCircle.this,str);
                    String strJsonChangeFriendLike = gson.toJson(jsonSendFriendsComment, JsonFriendCricleSendComment.class);
                    String s = new HttpTools().httpURLConnPostJson
                            (MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CHANGE_FRIEND_LIKE + list.get(comment_postion).id + MyConfig.API_COMMENT_FRIEND, strJsonChangeFriendLike);
                    MyLog.d("查看评论的返回值", s);
                    JsonFriendCricleCommonRet jsonSendFriendOfCricleRet = gson.fromJson(s, JsonFriendCricleCommonRet.class);
                    if (jsonSendFriendOfCricleRet != null && jsonSendFriendOfCricleRet.code == MyConfig.retSuccess()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                FriendCricleMsg msg = new FriendCricleMsg();

                                msg.comment_nickName = MyConfig.usr_nickname;
                                msg.comment = str;
                                list.get(comment_postion).commentMsg.add(msg);

                                listAdapterCircleOfFriends.notifyDataSetChanged();
                                hideKeyboard();
                                fl_emoji_fragment.setVisibility(View.GONE);
                            }
                        });
                    }
                }
            }).start();
        }
    }
    public void hideKeyboard(){
        //隐藏键盘
        MyConfig.hide_keyboard_must_call_from_activity(this);
        et.setText("");
        comment_linear.setVisibility(View.GONE);
    }


    public void delFriendOfCricleMsg(final int id,final int postion){
    //删除自己发的朋友圈
        new  AlertDialog.Builder(this)
                .setMessage("确定要删除吗？" )
                .setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Gson gson = new Gson();
                                final JsonFriendCricleChangeLike jsonChangeFriendLike = new JsonFriendCricleChangeLike();
                                jsonChangeFriendLike.accessToken = MyConfig.usr_token;
                                String strJsonChangeFriendLike = gson.toJson(jsonChangeFriendLike,JsonFriendCricleChangeLike.class);
                                String s = new HttpTools().httpURLConnPostJson
                                        (MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CHANGE_FRIEND_LIKE +id+ MyConfig.API_DEL_FRIEND,strJsonChangeFriendLike);
                                MyLog.d("查看删除朋友圈的返回值",s);
                                JsonFriendCricleCommonRet jsonSendFriendOfCricleRet = gson.fromJson(s,JsonFriendCricleCommonRet.class);
                                if(jsonSendFriendOfCricleRet!=null&&jsonSendFriendOfCricleRet.code==MyConfig.retSuccess()){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            list.remove(postion);
                                            listAdapterCircleOfFriends.notifyDataSetChanged();
                                        }
                                    });
                                }

                            }
                        }).start();
                    }
                })
                .setNegativeButton("否" , null)
                .show();

    }
    public void changeLike(final int id,final boolean tag,final int postion){
        //实现点赞
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                final JsonFriendCricleChangeLike jsonChangeFriendLike = new JsonFriendCricleChangeLike();
                jsonChangeFriendLike.accessToken = MyConfig.usr_token;
                if(tag){
                    jsonChangeFriendLike.unlike = tag;
                }
                MyLog.d("kanghongpu000",id+"aaaa"+tag+"bbbb"+jsonChangeFriendLike.unlike);

                String strJsonChangeFriendLike = gson.toJson(jsonChangeFriendLike,JsonFriendCricleChangeLike.class);
                MyLog.d("kanghongpu000",strJsonChangeFriendLike);
                String s = new HttpTools().httpURLConnPostJson
                        (MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CHANGE_FRIEND_LIKE +id+ MyConfig.API_CHANGE_FRIEND_LIKE2,strJsonChangeFriendLike);
                MyLog.d("查看点赞的返回值",s);
                JsonFriendCricleCommonRet jsonSendFriendOfCricleRet = gson.fromJson(s,JsonFriendCricleCommonRet.class);
                if(jsonSendFriendOfCricleRet!=null&&jsonSendFriendOfCricleRet.code==MyConfig.retSuccess()){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            FriendCricleMsg msg = new FriendCricleMsg();
                            list.get(postion).liked = !tag;
                            if(tag){
                                for(int i=0;i<list.get(postion).likeMsg.size();i++){
                                    if(list.get(postion).likeMsg.get(i).like_nickName.equals(MyConfig.usr_nickname)){
                                        list.get(postion).likeMsg.remove(i);
                                    }
                                }
                            }else{
                                msg.like_nickName = MyConfig.usr_nickname;
                                list.get(postion).likeMsg.add(msg);
                            }
                            listAdapterCircleOfFriends.notifyDataSetChanged();
                        }
                    });
                }

            }
        }).start();
    }

    public void getDataOfFriend(int get_more_limit){
    //获取朋友圈数据
        Call<JsonFriendCricleRet> repos = HttpTools.http_api_service().getFriengMsg(get_more_limit);
        repos.enqueue(new Callback<JsonFriendCricleRet>() {
            @Override
            public void onResponse(Call<JsonFriendCricleRet> call, Response<JsonFriendCricleRet> response) {
                swipyrefreshlayout.setRefreshing(false);

                if (response.code() == 200) {
                    jsonCricleOfFriendRet  = response.body();

                    if (jsonCricleOfFriendRet.data != null&& jsonCricleOfFriendRet.data.rows!=null) {
                        page++;
                        list.addAll(list.size(), FriendCricleMsg.chooseData(jsonCricleOfFriendRet.data.rows));
                        listAdapterCircleOfFriends.notifyDataSetChanged();
                        for(int i = 0;i<list.size();i++){
                            if(list.get(i).videoPath!=null){
                                DownloadVideo(list.get(i));
                            }
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonFriendCricleRet> call, Throwable t) {
                MyLog.d("", "retrofit: onFailure " + t.getMessage());
                swipyrefreshlayout.setRefreshing(false);
            }
        });

    }


    public void onEventMainThread(JsonFriendCricleSendNewContentRet jsonCricleOfFriendRet) {
        MyLog.d("查看有没有执行这里",jsonCricleOfFriendRet.data.title);
        if(jsonCricleOfFriendRet!=null&&jsonCricleOfFriendRet.data!=null){
            FriendCricleMsg jc = new FriendCricleMsg();
            jc.user_nickName = MyConfig.usr_nickname;
            jc.user_avatar = MyConfig.usr_avatar;

            if(jsonCricleOfFriendRet.data.info!=null) {
                jc.fullPath = jsonCricleOfFriendRet.data.info.fullPath;
                jc.videoPath = jsonCricleOfFriendRet.data.info.videoPath;
                jc.videosize = jsonCricleOfFriendRet.data.info.size;
                jc.os = jsonCricleOfFriendRet.data.info.os;

            }
            jc.liked = jsonCricleOfFriendRet.data.liked;
            jc.userId = jsonCricleOfFriendRet.data.userId;
            jc.image = jsonCricleOfFriendRet.data.image;
            jc.createdAt =jsonCricleOfFriendRet.data.createdAt;
            jc.id = jsonCricleOfFriendRet.data.id;
            jc.title = jsonCricleOfFriendRet.data.title;
            jc.updatedAt =jsonCricleOfFriendRet.data.updatedAt;
            jc.type = jsonCricleOfFriendRet.data.type;
            list.addFirst(jc);
            listAdapterCircleOfFriends.notifyDataSetChanged();
            MyConfig.setLocalFileAnd(jc);
            DownloadVideo(jc);

        }
    }
    public void onEventMainThread(EventFriendCricleDelDetail delDetail) {
        for(FriendCricleMsg fmsg:list){
            if(fmsg.id == delDetail.fid){
                list.remove();
                listAdapterCircleOfFriends.notifyDataSetChanged();
                break;
            }
        }
    }

    int count=0;
    public void onEventMainThread(JsonFriendCricleSocketRet jsonFriendCricleSocketRet) {
        if(jsonFriendCricleSocketRet.userId==MyConfig.usr_id){

        }else {
            count++;
            unRead_linear.setVisibility(View.VISIBLE);
            unRead_msg.setText(count + "条新评论");
            Picasso.with(ActivityFriendCircle.this).load(MyConfig.getApiDomain_NoSlash_GetResources() + jsonFriendCricleSocketRet.avatar)
                    .fit()
                    .centerCrop()
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(unRead_imgae_friend);
        }

    }

    public void choosefriendaction(){
        mDialog = new Dialog(this) {
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                requestWindowFeature(Window.FEATURE_NO_TITLE);
                setContentView(getContentView());
                Window window = this.getWindow();
                WindowManager.LayoutParams params = window.getAttributes();
                params.gravity = Gravity.BOTTOM;
                params.width = window.getWindowManager().getDefaultDisplay().getWidth();
                params.verticalMargin = 0;
                params.horizontalMargin = 0;

                window.setAttributes(params);
            }
        };

        mDialog.show();
    }

    public View getContentView(){
        View view = this.getLayoutInflater().inflate(R.layout.choose_send_friend_cricle,null);
        state_friend =(TextView) view.findViewById(R.id.choose_send_friend_cricle_state);
        video_friend =(TextView) view.findViewById(R.id.choose_send_friend_cricle_video);
        image_friend =(TextView) view.findViewById(R.id.choose_send_friend_cricle_image);
        camera_friend =(TextView) view.findViewById(R.id.choose_send_friend_cricle_camera);
        cancel_friend =(TextView) view.findViewById(R.id.choose_send_friend_cricle_cancel);
        state_friend.setOnClickListener(this);
        video_friend.setOnClickListener(this);
        image_friend.setOnClickListener(this);
        camera_friend.setOnClickListener(this);
        cancel_friend.setOnClickListener(this);
        return view;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.choose_send_friend_cricle_state:
                startNewActivity("动态",null);
                mDialog.dismiss();
                break;
            case R.id.choose_send_friend_cricle_video:
                openVideoRecorder();
                mDialog.dismiss();
                break;
            case R.id.choose_send_friend_cricle_image:
                openFileChooserImage(MyConfig.MIME_IMAGE_STAR);
                mDialog.dismiss();
                break;
            case R.id.choose_send_friend_cricle_camera:
                openCamera();
                mDialog.dismiss();
                break;
            case R.id.choose_send_friend_cricle_cancel:
                if(mDialog!=null)
                    mDialog.dismiss();
                break;
            case R.id.activtiy_cricle_of_friend_comment_send:
                break;
        }
    }
    public void startNewActivity(String title,String path){
        Intent intent = new Intent(ActivityFriendCircle.this,ActivityFriendCircleSendNewContent.class);
        intent.putExtra("title",title);
        if(path!=null){
            intent.putExtra("file",path);
        }
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==MyConfig.REQUEST_CAMERA&&resultCode==RESULT_OK){
            if(dialogSelectPosition==-1) {
                startNewActivity("相机", tempAttach.getPath());
            }else{
                setHeadPictureToServer(tempAttach);
            }

        }else if (requestCode==MyConfig.REQUEST_GALLERY&&resultCode==RESULT_OK){

                Uri fileUri = data.getData();
                String realPath = MyConfig.getUriPath(this, fileUri);
                tempAttach = new File(realPath);
            if(dialogSelectPosition==-1) {
                startNewActivity("图片", tempAttach.getPath());
            }else{
                setHeadPictureToServer(tempAttach);
            }

        }else if (requestCode==MyConfig.REQUEST_VIDEO&&resultCode==RESULT_OK){
            startNewActivity("小视屏", MyConfig.current_video_path);
        }
    }

    private void openVideoRecorder() {
        Intent intent = new Intent(this, ActivityVideoRecorder.class);
        startActivityForResult(intent, MyConfig.REQUEST_VIDEO);
    }


    public void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        tempAttach = MyConfig.getCameraFileBigPicture();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempAttach));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, MyConfig.REQUEST_CAMERA);
        }
    }

    private void openFileChooserImage(String mime) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setType(mime);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, MyConfig.REQUEST_GALLERY);
        }

    }

    public void setHeadPictureToServer(final File file){
        //上传头部图片到服务器
        new Thread(new Runnable() {
            @Override
            public void run() {
                final JsonFriendCricleCommonRet jsonSendCricleOfFriendRet =HttpTools.okhttpUploadFriendHeadImage(file);
                if (jsonSendCricleOfFriendRet!= null && jsonSendCricleOfFriendRet.code == MyConfig.retSuccess()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Glide.with(ActivityFriendCircle.this).load(file)
                                    .centerCrop()
                                    .into(bg_friend);
                        }
                    });

                }
            }
        }).start();
    }

    private void initPopUp(int postion) {
        LinearLayout pop_mainroom_info_layout, pop_mainroom_subjects_layout;

        View popView = getLayoutInflater().inflate(R.layout.friend_cricle_function, null);

        popupWindow = new PopupWindow(popView, LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());


    }



}
