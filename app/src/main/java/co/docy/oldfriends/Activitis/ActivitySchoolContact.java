package co.docy.oldfriends.Activitis;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import co.docy.oldfriends.Fragments.FragmentSchoolContact;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/5/3.
 * 校友录的Activity
 */
public class ActivitySchoolContact extends ActivityBase{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_contact);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragmentSchoolContact fragmentSchoolContact = FragmentSchoolContact.newInstance();
        fragmentTransaction.add(R.id.activity_school_contact, fragmentSchoolContact);
        fragmentTransaction.commit();

    }

}
