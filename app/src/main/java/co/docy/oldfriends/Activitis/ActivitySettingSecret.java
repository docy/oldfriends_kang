package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/4/22.
 * 我的设置中隐私的Activity
 */
public class ActivitySettingSecret extends ActivityBase implements View.OnClickListener{
    private ImageView image_secret;
    private LinearLayout linearLayout_secret;
    boolean tag = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_secret);
        linearLayout_secret =(LinearLayout) findViewById(R.id.activity_setting_secret_blackcontact);
        image_secret =(ImageView) findViewById(R.id.activity_setting_secret_receive);
        image_secret.setOnClickListener(this);

        if(tag) {
            image_secret.setImageResource(R.drawable.switch_icon_on);
        }else {
            image_secret.setImageResource(R.drawable.switch_icon_off);
        }
    }
    public void onClick(View v){
        if(tag) {
            image_secret.setImageResource(R.drawable.switch_icon_off);
            tag = false;

        }else {
            image_secret.setImageResource(R.drawable.switch_icon_on);
            tag = true;
        }
    }
}
