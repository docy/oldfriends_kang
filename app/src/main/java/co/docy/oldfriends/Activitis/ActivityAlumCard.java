package co.docy.oldfriends.Activitis;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Fragments.FragmentAlumCard;
import co.docy.oldfriends.R;


public class ActivityAlumCard extends ActivityBase {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alum_fragment);
        Intent intent = getIntent();
        if (intent != null) {

            Bundle bundle = intent.getBundleExtra(MyConfig.CONSTANT_PARAMETER_STRING_BUNDLE);

            init();

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            FragmentAlumCard fragmentUserInfo = FragmentAlumCard.newInstance();
            fragmentTransaction.add(R.id.activity_alum_framelayout, fragmentUserInfo);
            fragmentTransaction.commit();

        } else {
            finish();
        }

        getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.xjt_background));


    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void init() {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
