package co.docy.oldfriends.Activitis;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.Fragments.FragmentContactMultiChoose;
import co.docy.oldfriends.Messages.JsonLeaveGroup;
import co.docy.oldfriends.Messages.JsonTransferGroupOwner;
import co.docy.oldfriends.R;

/**
 * Created by wuxue on 15/10/16.
 */
public class ActivityChooseGroupOwner extends ActivityBase {
    private int groupId;
    private String name, mDesc;
    private Context context = this;
    public static final int EXIT_ATTORN = 0;
    public static final int EXIT_SURE = 2;
    private FragmentContactMultiChoose fragmentContactMultiChoose;
    private LinkedList<ContactUniversal> list = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_invite_people);
        Intent intent = getIntent();
        groupId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, -1);
        name = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME);
        mDesc = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentContactMultiChoose = FragmentContactMultiChoose.newInstance(MyConfig.MODE_CHOOSE_SINGLE_WITHOUT_SUBMIT);

        fragmentTransaction.add(R.id.activity_contact_multi_choose_inearLayout, fragmentContactMultiChoose);
        fragmentTransaction.commit();

        MyLog.d("", "ListAdapterContactOfCompanyMultiChoose ActivityContactOfCompanyMultiChoose ");

        fragmentContactMultiChoose.submitListener = new FragmentContactMultiChoose.OnSubmitClicked() {
            @Override
            public void OnClicked(final LinkedList<ContactUniversal> list_universal_selected) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showRemindDialog(getResources().getString(R.string.exit_attorn_group),
                                getResources().getString(R.string.exit_attorn_remind),
                                context, EXIT_ATTORN, 0, list_universal_selected);
                    }

                });
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        list.clear();
        list = ContactUniversal.convertListFromGetGroupUserRet(MyConfig.jsonGetGroupUserRet.data);
        list.remove(0);
        fragmentContactMultiChoose.setList(list);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showRemindDialog(String title, final String remindstr, final Context mContext, final int type, final int position,
                                 final LinkedList<ContactUniversal> tempList) {
        View dialogView = LayoutInflater.from(mContext).inflate(
                R.layout.dialog_exit_remind_layout, null);
        final Dialog setDialog = new Dialog(mContext, R.style.DialogStyle);
        setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialog.setCancelable(false);
        setDialog.getWindow().setContentView(dialogView);
        WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
        lp.width = MyConfig.getWidthByScreenPercent(80);
        setDialog.getWindow().setAttributes(lp);
        Button cancleButton = (Button) dialogView.findViewById(R.id.cancle);
        Button loginButton = (Button) dialogView.findViewById(R.id.login);
        TextView remindText = (TextView) dialogView.findViewById(R.id.remind_text);
        if (type == EXIT_ATTORN) {
            remindText.setText(tempList.get(position).nickName + remindstr);
        } else {
            remindText.setText(remindstr);
        }
        TextView titletext = (TextView) dialogView.findViewById(R.id.remind_title);
        titletext.setText(title);
        if (type == EXIT_SURE) {
            titletext.setVisibility(View.GONE);
        } else {
            titletext.setVisibility(View.VISIBLE);

        }
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                switch (type) {
                    case EXIT_ATTORN:
                        JsonTransferGroupOwner jsonTransferGroupOwner = new JsonTransferGroupOwner();
                        jsonTransferGroupOwner.accessToken = MyConfig.usr_token;
                        jsonTransferGroupOwner.groupId = groupId;
                        jsonTransferGroupOwner.userId = tempList.get(position).id;
                        MyConfig.GroupTransferOwner(jsonTransferGroupOwner);
                        showRemindDialog("", mContext.getResources().getString(R.string.exit_sureexit), mContext, EXIT_SURE, 0, tempList);

                        break;
                    case EXIT_SURE:
                        JsonLeaveGroup jsonLeaveGroup = new JsonLeaveGroup();
                        jsonLeaveGroup.accessToken = MyConfig.usr_token;
                        jsonLeaveGroup.groupId = groupId;
                        MyConfig.leaveGroup(jsonLeaveGroup);
                        Intent intent = new Intent(mContext, ActivityViewPager.class);
                        mContext.startActivity(intent);
                        finish();
                        break;
                }
                setDialog.cancel();
            }

        });
        cancleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (type) {
                    case EXIT_ATTORN:
                        break;
                    case EXIT_SURE:
                        finish();
                        Intent intent = new Intent(mContext, ActivityGroupInfo.class);
                        intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
                        intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, name);
                        intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, mDesc);
                        mContext.startActivity(intent);
                        break;
                    default:
                        break;
                }

                setDialog.cancel();
            }
        });
        setDialog.setCanceledOnTouchOutside(false);
        setDialog.show();
    }


}
