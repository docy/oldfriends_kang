package co.docy.oldfriends.Activitis;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventCloseActivity;
import co.docy.oldfriends.EventBus.EventRegistSuccess;
import co.docy.oldfriends.Messages.JsonLogin;
import co.docy.oldfriends.Messages.JsonRegistRet;
import co.docy.oldfriends.Messages.JsonRegistWithPhotoTongChuang;
import co.docy.oldfriends.Messages.JsonSmsSignup;
import co.docy.oldfriends.Messages.JsonSmsSignupRet;
import co.docy.oldfriends.Messages.JsonWeixinLoginRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;


public class ActivityRegistTongChuang extends ActivityBase {
    ImageView regist_frant_img;
    Button regist_send, regist_get_code;
    EditText regist_pwd;
    EditText regist_phone;
    EditText regist_code;

    AVLoadingIndicatorView regist_avloadingIndicatorView;

    JsonRegistWithPhotoTongChuang jsonRegistWithPhoto;
    int count = 0;
    boolean waitting_server;

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist_tongchuang);

        regist_frant_img = (ImageView)findViewById(R.id.regist_front_img);
        regist_frant_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConfig.hide_keyboard_must_call_from_activity(ActivityRegistTongChuang.this);
            }
        });

        regist_pwd = (EditText) findViewById(R.id.regist_pwd);
        regist_phone = (EditText) findViewById(R.id.regist_phone);

        regist_get_code = (Button) findViewById(R.id.regist_get_code);
        regist_get_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 0) {
                    smsSignup();
                }
            }
        });
        regist_code = (EditText) findViewById(R.id.regist_code);

        regist_send = (Button) findViewById(R.id.regist_send);
        regist_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotoRegist();

            }
        });



        waitting_server = false;
        regist_avloadingIndicatorView = (AVLoadingIndicatorView)findViewById(R.id.regist_avloadingIndicatorView);
//        regist_avloadingIndicatorView.setVisibility(View.GONE);

        handler = new Handler();
    }

    private void showWaitAnimator(final boolean b){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(b){
                    regist_avloadingIndicatorView.setVisibility(View.VISIBLE);
                }else {
                    regist_avloadingIndicatorView.setVisibility(View.GONE);
                }
            }
        });
    }

    private void gotoRegist() {

        jsonRegistWithPhoto = new JsonRegistWithPhotoTongChuang();
        jsonRegistWithPhoto.phone = regist_phone.getText().toString().trim();
        regist_phone.setText(jsonRegistWithPhoto.phone);
        jsonRegistWithPhoto.code = regist_code.getText().toString().trim();
        jsonRegistWithPhoto.password = regist_pwd.getText().toString().trim();
        regist_pwd.setText(jsonRegistWithPhoto.password);
        jsonRegistWithPhoto.countryCode = MyConfig.app_country.phone_prefix;

        jsonRegistWithPhoto.sex = true;//先假定为男，下一步更改

        /**
         * 这里客户端先自己做一个合法性检测
         */
//        if (jsonRegistWithPhoto.name.length() == 0) {
//            notifyRegistResualt(getResources().getString(R.string.null_name));
//            return;
//        }
        if (jsonRegistWithPhoto.phone.length() == 0) {
            notifyRegistResualt(getResources().getString(R.string.null_phone));
            return;
        }
//        if (jsonRegistWithPhoto.email.length() == 0) {
//            notifyRegistResualt(getResources().getString(R.string.null_email));
//            return;
//        }
        //客户端取消密码限制 20151216
//        if (jsonRegistWithPhoto.password.length() < getResources().getInteger(R.integer.mini_length_user_pwd)) {
//            notifyRegistResualt("密码不能少于6位");
//            return;
//        }
        if (jsonRegistWithPhoto.code.length() == 0) {
            notifyRegistResualt(getResources().getString(R.string.null_code));
            return;
        }

        if (jsonRegistWithPhoto.password.length() == 0) {
            notifyRegistResualt(getResources().getString(R.string.null_password));
            return;
        }
        /**
         * 新注册接口，包含可选的photo信息
         */
        new Thread(new Runnable() {
            @Override
            public void run() {

                showWaitAnimator(true);

                JsonRegistRet jsonRegistRet = HttpTools.okhttpUploadRegistWithPhotoTongChuang(jsonRegistWithPhoto);


                if (jsonRegistRet != null) {


                    if (jsonRegistRet.code!=MyConfig.retSuccess()) {
                        notifyRegistResualt(jsonRegistRet.message);

                    } else {

                        notifyRegistResualt(getResources().getString(R.string.regist_success));
                        EventBus.getDefault().post(new EventRegistSuccess());/**注册成功之后发送事件，关闭登录页面和免责页面*/
//                        selectRegistlabel();
                        loginRequest();

                    }
                } else {
                    notifyRegistResualt(getResources().getString(R.string.server_not_return));
                }

                showWaitAnimator(false);

            }
        }).start();


    }

    /**
     * 进入ActivityRegistLabel页面，选择标签，
     *
     * 这是一个额外的操作，标签也是一个可选的操作，无论是否选择标签都不影响注册的过程，当取消这个功能时不调用这个方法即可
     */
    private void selectRegistlabel() {
        startActivity(new Intent(this,ActivityRegistLabel.class));
    }


    public void loginRequest() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonLogin jsonLogin = new JsonLogin();
                jsonLogin.account = jsonRegistWithPhoto.phone;
                jsonLogin.password = jsonRegistWithPhoto.password;
                jsonLogin.os = MyConfig.os_string;
                jsonLogin.uuid = MyConfig.uuid;

                Gson gson = new Gson();
                String json_login = gson.toJson(jsonLogin);

                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LOGIN, json_login);

                JsonWeixinLoginRet jsonLoginRet = gson.fromJson(s, JsonWeixinLoginRet.class);

                if (jsonLoginRet != null) {

                    if (jsonLoginRet != null){
                        if(jsonLoginRet.code==MyConfig.retSuccess()) {
                            MyConfig.getDataByOrder();
                            MyConfig.updateUserInforFromLogin(ActivityRegistTongChuang.this, jsonLogin.password, s, jsonLoginRet);

                            /**
                             * 由于用户的具体信息由后台输入，所以这里没必要让用户设置了
                             */
                            if(MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {
                                Intent intent_person_info = new Intent(ActivityRegistTongChuang.this, _ActivityRegistPersonInfo.class);
                                startActivity(intent_person_info);
                            }

                            MyConfig.flag_open_invite_code_activity = 0;//0, no invite, 1,please start invite, 2, have started invite
                            MyConfig.flag_open_label_activity = 1;//0, no invite, 1,please start invite, 2, have started invite
                            MyLog.d("", "app start steps 0, regist set flag_open_invite_code_activity");
                            startActivity(new Intent(ActivityRegistTongChuang.this,ActivityViewPager.class));
                            finish();

                        }else{
                            finishWithLoginFailed();

                        }
                    }

                } else {

                    finishWithLoginFailed();

//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
////                            Toast.makeText(ActivityLogin.this, getString(R.string.net_msg_not_connected), Toast.LENGTH_SHORT).show();
//                            MyConfig.MyToast(0, ActivityRegist.this, getString(R.string.net_msg_not_connected));
//                        }
//                    });

                }

            }
        }).start();
    }


    public void finishWithLoginFailed() {
        /**
         * 登录失败则跳转到登录页面
         */
        Intent intent_login = new Intent(ActivityRegistTongChuang.this, ActivityLoginTongChuang.class);
        startActivity(intent_login);

        finish();
    }

    /**
     * 电话号码已经被注册后，弹出对话框
     * *
     */
    private void showErrorDialog(String remindstr) {
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(
                R.layout.dialog_login_remind_layout, null);
        final Dialog setDialog = new Dialog(ActivityRegistTongChuang.this, R.style.DialogStyle);
        setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialog.setCancelable(false);
        setDialog.getWindow().setContentView(dialogView);
        WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
        lp.width = MyConfig.getWidthByScreenPercent(80);
        setDialog.getWindow().setAttributes(lp);
        Button cancleButton = (Button) dialogView.findViewById(R.id.cancle);
        Button loginButton = (Button) dialogView.findViewById(R.id.login);
        TextView remindText = (TextView) dialogView.findViewById(R.id.remind_text);
        remindText.setText(remindstr);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                startActivity(new Intent(ActivityRegistTongChuang.this, _ActivityLogin.class));
                setDialog.cancel();
            }

        });
        cancleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialog.cancel();
            }
        });
        setDialog.setCanceledOnTouchOutside(false);
        setDialog.show();
    }

    private void notifyRegistResualt(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MyConfig.MyToast(0, ActivityRegistTongChuang.this,
                        message);
            }
        });

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(updateThread);
    }

    Runnable updateThread = new Runnable() {
        public void run() {
            // do some thing
            if (count > 0) {
                count--;
            }
            if (count > 0) {
                String format = getResources().getString(R.string.please_wait);
                String result = String.format(format,count);
                regist_get_code.setText(result);

                if(handler != null) {
                    handler.postDelayed(updateThread, 1000);
                }

            } else {

                regist_get_code.setText(getString(R.string.prompt_valid_button));

                if(handler != null) {
                    handler.removeCallbacks(updateThread);
                }
            }

        }
    };

//    public void onEventMainThread(EventHeartOneSecond eventHeartOneSecond) {
//
//        if (count > 0) {
//            count--;
//        }
//        if (count > 0) {
//            String format = getResources().getString(R.string.please_wait);
//            String result = String.format(format,count);
//            regist_get_code.setText(result);
//        } else {
//            regist_get_code.setText(getString(R.string.prompt_valid_button));
//        }
//
//    }


    private void smsSignup() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSmsSignup jsonSmsSignup = new JsonSmsSignup();
                jsonSmsSignup.phone = regist_phone.getText().toString();


                String jsonStr = gson.toJson(jsonSmsSignup, JsonSmsSignup.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SMS_SIGNUP, jsonStr);
                MyLog.d("", "apitest: send msg ret " + s);

                final JsonSmsSignupRet jsonSmsSignupRet = gson.fromJson(s, JsonSmsSignupRet.class);
                if (jsonSmsSignupRet != null) {

                    if (jsonSmsSignupRet.code==MyConfig.retSuccess()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                count = 60;
                                String format = getResources().getString(R.string.please_wait);
                                String result = String.format(format,count);
                                regist_get_code.setText(result);
                                if (MyConfig.debug_autofill_validcode) {
                                    regist_code.setText(jsonSmsSignupRet.data.code);
                                }

                                handler.postDelayed(updateThread, 1000);

                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MyConfig.MyToast(0, ActivityRegistTongChuang.this, jsonSmsSignupRet.message);
                            }
                        });

                    }
                }else{
                    MyConfig.MyToast(0, ActivityRegistTongChuang.this, getResources().getString(R.string.server_connect_fail));
                }

            }
        }).start();
    }



}
