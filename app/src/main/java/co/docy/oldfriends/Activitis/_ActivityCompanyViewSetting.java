package co.docy.oldfriends.Activitis;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import co.docy.oldfriends.Fragments._FragmentCompanyViewSetting;
import co.docy.oldfriends.R;

/**
 * Created by wuxue on 15/11/26.
 */
public class _ActivityCompanyViewSetting extends ActivityBase {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_view_setting);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        _FragmentCompanyViewSetting fragmentCompanyViewSetting = _FragmentCompanyViewSetting.newInstance("companysetting");
        fragmentTransaction.add(R.id.activity_company_set_layout, fragmentCompanyViewSetting);
        fragmentTransaction.commit();
        setTitle(getResources().getString(R.string.set));
    }

}
