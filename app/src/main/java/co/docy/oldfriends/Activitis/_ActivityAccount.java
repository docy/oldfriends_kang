package co.docy.oldfriends.Activitis;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.astuetz.PagerSlidingTabStrip;

import java.util.Locale;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventAppQuit;
import co.docy.oldfriends.EventBus.EventLogin;
import co.docy.oldfriends.Fragments._FragmentV4AccountLogin;
import co.docy.oldfriends.Fragments._FragmentV4AccountRegist1;
import co.docy.oldfriends.Fragments._FragmentV4AccountRegist2;
import co.docy.oldfriends.Fragments._FragmentV4AccountReset;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;


public class _ActivityAccount extends ActivityBase {

    ViewPager viewPager;
    PagerSlidingTabStrip pagerSlidingTabStrip;
    SectionsPagerAdapter sectionsPagerAdapter;

    _FragmentV4AccountLogin fragmentV4AccountLogin;
    _FragmentV4AccountReset fragmentV4AccountReset;
    _FragmentV4AccountRegist1 fragmentV4AccountRegist1;
    _FragmentV4AccountRegist2 fragmentV4AccountRegist2;

    boolean fragmentPageAccountRegist2_is_showing;
    boolean fragmentPageAccountReset_is_showing;

    ImageView iv_logo;


    /**
     * if 0, no auto login
     * if 1, auto login
     * if -1, according to usr_status
     */
    int login_cmd;
    String auto_login_name;
    String auto_login_pwd;

    ImageButton login_weixin;
    Button logout_weixin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.child_activity_name = this.getClass().getName();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        MyLog.d("", "app start steps: ActivityAccount onCreate");

        MyConfig.activityAccount = this;
        login_cmd = getIntent().getIntExtra(MyConfig.TAG_LOGIN_CMD, -1);

        initViews();

    }

    private void initViews() {
        initWidget();
        initFragments();
        initViewpager();
        initWeixin();
    }

    private void initWidget() {

        iv_logo = (ImageView) findViewById(R.id.activity_account_logo);
//        iv_logo.getLayoutParams().width = MyConfig.screenWidth / 4;
//        iv_logo.getLayoutParams().height = MyConfig.screenWidth / 4;
        iv_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConfig.hide_keyboard_must_call_from_activity(_ActivityAccount.this);
                return;
            }
        });
        iv_logo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                showFragmentRegist2();

                if (fragmentV4AccountLogin.fragment_page_account_login_phone_name.getText().toString().trim().endsWith("set")) {
                    Intent intent_advance_setting = new Intent(_ActivityAccount.this, ActivityAdvanceSetting.class);
                    startActivity(intent_advance_setting);
                }
                return true;
            }
        });
        iv_logo.setSoundEffectsEnabled(false);
    }

    private void showViewPager() {

        if (fragmentPageAccountRegist2_is_showing) {
            hideFragmentRegist2();
            login_weixin.setVisibility(View.VISIBLE);
            pagerSlidingTabStrip.setVisibility(View.VISIBLE);
        }
        if (fragmentPageAccountReset_is_showing) {
            hideFragmentReset();
            login_weixin.setVisibility(View.VISIBLE);
        }


//        fragmentPageAccountReset.setVisibile(View.GONE);
//        fragmentPageAccountRegist2.setVisibile(View.GONE);
//        viewPager.setAlpha(1);

//        hideFragmentReset();
    }

    public void showFragmentReset() {

        fragmentPageAccountReset_is_showing = true;
        fragmentV4AccountReset.setVisibile(View.VISIBLE);
        login_weixin.setVisibility(View.INVISIBLE);
        ObjectAnimator objectAnimator =
                ObjectAnimator.ofFloat(fragmentV4AccountReset,
                        "y", MyConfig.screenHeight, viewPager.getY()).setDuration(300);
        objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.setRepeatCount(0);
//        objectAnimator.start();

        ObjectAnimator objectAnimator2 =
                ObjectAnimator.ofFloat(viewPager,
                        "alpha", 1, 0).setDuration(300);
        objectAnimator2.setInterpolator(new LinearInterpolator());
        objectAnimator2.setRepeatCount(0);


        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2);
        animatorSet.start();

    }

    public void hideFragmentReset() {

        fragmentPageAccountReset_is_showing = false;

        ObjectAnimator objectAnimator =
                ObjectAnimator.ofFloat(fragmentV4AccountReset,
                        "y", viewPager.getY(), MyConfig.screenHeight).setDuration(300);
        objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.setRepeatCount(0);
//        objectAnimator.start();

        ObjectAnimator objectAnimator2 =
                ObjectAnimator.ofFloat(viewPager,
                        "alpha", 0, 1).setDuration(300);
        objectAnimator2.setInterpolator(new LinearInterpolator());
        objectAnimator2.setRepeatCount(0);


        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2);
        animatorSet.start();

    }

    public void showFragmentRegist2() {

        pagerSlidingTabStrip.setVisibility(View.INVISIBLE);
        login_weixin.setVisibility(View.INVISIBLE);
        fragmentPageAccountRegist2_is_showing = true;
        fragmentV4AccountRegist2.setVisibile(View.VISIBLE);
        fragmentV4AccountRegist2.setCount(120);
        fragmentV4AccountRegist2.phone = fragmentV4AccountRegist1.fragment_page_account_regist_1_phone.getText().toString().trim();
        fragmentV4AccountRegist2.fragment_page_account_regist_2_phone
                .setText("("+ fragmentV4AccountRegist1.fragment_page_account_regist_1_country_prefix.getText().toString().trim()+") "
                        + fragmentV4AccountRegist1.fragment_page_account_regist_1_phone.getText().toString().trim());

        ObjectAnimator objectAnimator =
                ObjectAnimator.ofFloat(fragmentV4AccountRegist2,
                        "y", MyConfig.screenHeight, viewPager.getY()).setDuration(300);
        objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.setRepeatCount(0);
//        objectAnimator.start();

        ObjectAnimator objectAnimator2 =
                ObjectAnimator.ofFloat(viewPager,
                        "alpha", 1, 0).setDuration(300);
        objectAnimator2.setInterpolator(new LinearInterpolator());
        objectAnimator2.setRepeatCount(0);


        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2);
        animatorSet.start();

    }

    public void hideFragmentRegist2() {
        fragmentPageAccountRegist2_is_showing = false;
        fragmentV4AccountRegist2.setCount(0);
        ObjectAnimator objectAnimator =
                ObjectAnimator.ofFloat(fragmentV4AccountRegist2,
                        "y", viewPager.getY(), MyConfig.screenHeight).setDuration(300);
        objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.setRepeatCount(0);
//        objectAnimator.start();

        ObjectAnimator objectAnimator2 =
                ObjectAnimator.ofFloat(viewPager,
                        "alpha", 0, 1).setDuration(300);
        objectAnimator2.setInterpolator(new LinearInterpolator());
        objectAnimator2.setRepeatCount(0);


        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator, objectAnimator2);
        animatorSet.start();

    }

//    public void hideFragmentReset() {
//        initViewpager();
//        initFragments();
//    }

    private void initFragments() {

        fragmentV4AccountLogin = _FragmentV4AccountLogin.newInstance();
        fragmentV4AccountRegist1 = _FragmentV4AccountRegist1.newInstance();

        fragmentV4AccountReset = (_FragmentV4AccountReset) getSupportFragmentManager().findFragmentById(R.id.activity_account_fragment_reset);
        fragmentV4AccountReset.setVisibile(View.GONE);
        fragmentV4AccountRegist2 = (_FragmentV4AccountRegist2) getSupportFragmentManager().findFragmentById(R.id.activity_account_fragment_regist_2);
        fragmentV4AccountRegist2.setVisibile(View.GONE);
    }

    private void initViewpager() {

//        activity_account_layout_viewpager = (LinearLayout)findViewById(R.id.activity_account_layout_viewpager);

        sectionsPagerAdapter = new SectionsPagerAdapter(
                getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.activity_account_viewpage);
        viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.setCurrentItem(1);

        pagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.activity_account_viewpager_tabs);

        // Bind the tabs to the ViewPager
        pagerSlidingTabStrip.setShouldExpand(true);//TabPaddingLeftRight
        pagerSlidingTabStrip.setTabPaddingLeftRight(30);
        pagerSlidingTabStrip.setViewPager(viewPager);
//        pagerSlidingTabStrip.setIndicatorColorResource(R.color.Yellow);
        pagerSlidingTabStrip.setMinimumHeight(MyConfig.getPxFromDimen(this, R.dimen.viewpager_tabs_height));
        pagerSlidingTabStrip.getLayoutParams().height = MyConfig.getPxFromDimen(this, R.dimen.viewpager_tabs_height);
//        tabs.setUnderlineHeight(5);
        pagerSlidingTabStrip.setIndicatorHeight(3);
        pagerSlidingTabStrip.setIndicatorColor(getResources().getColor(R.color.White));
//        pagerSlidingTabStrip.setBackgroundResource(R.color.xjt_toolbar_green);
        pagerSlidingTabStrip.setTextColorResource(R.color.White);
        pagerSlidingTabStrip.setAllCaps(false);
        pagerSlidingTabStrip.setTextSize((int) (getResources().getDimension(R.dimen.setting_text_size_small)));

        pagerSlidingTabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                showViewPager();
                if (position == 1) {
                    pagerSlidingTabStrip.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initWeixin() {
        logout_weixin = (Button) findViewById(R.id.activity_account_weixin_logout);
//        logout_weixin.setVisibility(View.VISIBLE);
        logout_weixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConfig.logoutWeixin(_ActivityAccount.this);

            }
        });
        login_weixin = (ImageButton) findViewById(R.id.activity_account_weixin_login);
//        login_weixin.setVisibility(View.VISIBLE);
        login_weixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConfig.loginWeixin(_ActivityAccount.this);
            }
        });
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            MyLog.d("", "viewpager get fragment " + position);
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.

            if (position == 0) {
                return fragmentV4AccountRegist1;
            } else if (position == 1) {
                MyLog.d("", "read contact viewpager getItem");
                return fragmentV4AccountLogin;
            } else {
                return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    //  toolbar.setVisibility(View.VISIBLE);
                    return getString(R.string.regist_regist);
                case 1:
                    // toolbar.setVisibility(View.GONE);
                    return getString(R.string.login_login);
            }
            return null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        MyLog.d("", "ActivityLogin onResume");

        /**
         * load user data
         */
//        if(MyConfig.prefShared != null) {
//            auto_login_name = MyConfig.prefShared.getString(MyConfig.PREF_USER_NAME, "");
//            et_name.setText(auto_login_name);
//            et_name.setSelection(auto_login_name.length());
//            auto_login_pwd = MyConfig.prefShared.getString(MyConfig.PREF_USER_PWD, "");
//            et_pwd.setText(auto_login_pwd);
//            et_pwd.setSelection(auto_login_pwd.length());
//        }
//
//        switch (login_cmd) {
//            case -1:
//                autoLoginAccordingToUsrStatus();
//                break;
//            case 0:
//                break;
//            case 1:
//                loginIfHavePwd();
//                break;
//        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        MyConfig.activityAccount = null;

    }

    private void autoLoginAccordingToUsrStatus() {
        /**
         * app可能意外重启等等，MyConfig.usr_status是不可靠的，只有preference里面保存的才是可靠的
         */
//        MyConfig.usr_status = MyConfig.prefShared.getInt(MyConfig.PREF_USR_STATUS, -1);
        MyConfig.loadUserDataFromPreference(this);

        MyLog.d("", "autologin in resume " + MyConfig.usr_status);
        if (MyConfig.usr_status > MyConfig.USR_STATUS_LOGOUT) {

            MyLog.d("", "autologin: login()");

            loginIfHavePwd();
        }
    }

    private void loginIfHavePwd() {
        if (auto_login_name != null && auto_login_pwd != null) {
            loginRequest();
        }
    }

    public void loginRequest() {
        new Thread(new Runnable() {
            @Override
            public void run() {
//
//                String login_name_or_phone = et_name.getText().toString();
//                String pwd = et_pwd.getText().toString();
//
//                JsonLogin jsonLogin = new JsonLogin();
//                jsonLogin.account = login_name_or_phone;
//                jsonLogin.password = pwd;
//                if(MyConfig.debug_using_inner_server) {
//                    jsonLogin.os = MyConfig.os_string;
//                    jsonLogin.uuid = MyConfig.uuid;
//                }else{
//
//                }
//
//                Gson gson = new Gson();
//                String j = gson.toJson(jsonLogin);
//
//                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LOGIN, j);
//
//                JsonLoginRet jsonLoginRet = gson.fromJson(s, JsonLoginRet.class);
//
//                if (jsonLoginRet != null) {
//
//                    if (jsonLoginRet.code!=MyConfig.retSuccess()) {
//
//                        /**
//                         * 密码错了就清除保存的密码，并保持在这个位置
//                         */
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                              //  et_pwd.setText("");
//                                MyConfig.MyToast(0,ActivityLoginNew.this,getString(R.string.error_login));
//                            }
//                        });
//
//                        MyConfig.usr_pwd = "";
//                        MyConfig.saveUserDataToPreference(ActivityLoginNew.this);
//
//                    }else if (jsonLoginRet.data != null) {
//
//                        MyConfig.logoutClear();
//
//                        MyConfig.usr_login_ret_json = s;
//                        MyConfig.jsonLoginRet = jsonLoginRet;
//                        MyConfig.usr_id = jsonLoginRet.data.id;
//                        MyConfig.usr_name = jsonLoginRet.data.name;//注意name要从这里取，因为login_name_or_phone也可能是phone
//                        MyConfig.usr_nickname = jsonLoginRet.data.nickName;
//                        MyConfig.usr_phone = jsonLoginRet.data.phone;
//                        MyConfig.usr_pwd = pwd;
//                        MyConfig.usr_token = jsonLoginRet.data.authToken;//常用变量就提取出来
//                        MyConfig.usr_status = MyConfig.USR_STATUS_LOGIN;
//                        MyConfig.usr_email = jsonLoginRet.data.email;
//                        MyConfig.usr_phone = jsonLoginRet.data.phone;
//                        if (jsonLoginRet.data.sex) {
//                            MyConfig.usr_sex = 1;//男
//                        } else {
//                            MyConfig.usr_sex = 0;//女
//                        }
//                        MyConfig.usr_avatar = jsonLoginRet.data.avatar;
//
//                        MyConfig.saveUserDataToPreference(ActivityLoginNew.this);
//
//                        finishWithSuccess();
//                    }
//
//                } else {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
////                            Toast.makeText(ActivityLogin.this, getString(R.string.net_msg_not_connected), Toast.LENGTH_SHORT).show();
//                            MyConfig.MyToast(0, ActivityLoginNew.this, getString(R.string.net_msg_not_connected));
//                        }
//                    });
//                }

            }
        }).start();
    }

    private void finishWithSuccess() {

        EventBus.getDefault().post(new EventLogin());

        setResult(RESULT_OK, new Intent());
        finish();
    }

    private void finishWithFailure() {
        setResult(RESULT_CANCELED, new Intent());
        finish();
    }

    private boolean backPressed = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            Toast.makeText(this, getResources().getString(R.string.sys_quit_notice), Toast.LENGTH_SHORT).show();
            MyConfig.MyToast(-1, this, getResources().getString(R.string.sys_quit_notice));

            if (backPressed == true) {
//                MyConfig.activityViewPager.finish();
//                finish();
                EventBus.getDefault().post(new EventAppQuit());
            } else {
                backPressed = true;
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        backPressed = false;
                    }
                }, 2000);

                showViewPager();

            }
            return true; // 让系统不再处理这次回退键，因为系统收到回退键后会立即把app关了
        }
        return super.onKeyDown(keyCode, event);
    }
}
