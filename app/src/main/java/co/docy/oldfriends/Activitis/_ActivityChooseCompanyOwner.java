package co.docy.oldfriends.Activitis;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.Fragments.FragmentContactMultiChoose;
import co.docy.oldfriends.Messages.JsonTransferCompanyOwner;
import co.docy.oldfriends.Messages.JsonTransferCompanyOwnerRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by wuxue on 15/10/21.
 */
public class _ActivityChooseCompanyOwner extends ActivityBase {

    //    private Context context = (Context)this;
    public Intent newIntent;
    private FragmentContactMultiChoose fragmentContactMultiChoose;

    private LinkedList<ContactUniversal> list = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_invite_people);
        newIntent = getIntent();
        final int mCompanyID = newIntent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, -1);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentContactMultiChoose = FragmentContactMultiChoose.newInstance(MyConfig.MODE_CHOOSE_SINGLE_WITHOUT_SUBMIT);

        fragmentTransaction.add(R.id.activity_contact_multi_choose_inearLayout, fragmentContactMultiChoose);
        fragmentTransaction.commit();

        MyLog.d("", "ListAdapterContactOfCompanyMultiChoose ActivityContactOfCompanyMultiChoose ");

        fragmentContactMultiChoose.submitListener = new FragmentContactMultiChoose.OnSubmitClicked() {
            @Override
            public void OnClicked(final LinkedList<ContactUniversal> list_universal_selected) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showRemindDialog(getResources().getString(R.string.exit_attorn_company), getResources().getString(R.string.exit_transfer_company_owner),
                                list_universal_selected.get(0).id, _ActivityChooseCompanyOwner.this, list_universal_selected.get(0).nickName, mCompanyID);
                    }

                });
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        list = ContactUniversal.convertListFromGetCompanyUserListRet(MyConfig.jsonGetCompanyUserListRet.data);
        for (int i = 0; i < list.size(); i++) {
            if (MyConfig.usr_id == list.get(i).id) {
                list.remove(i);
                break;
            }
        }
        fragmentContactMultiChoose.setList(list);

    }

    public void showRemindDialog(String title, final String remindstr, final int adminId, final Context mContext, final String owner, final int mCompanyId) {
        View dialogView = LayoutInflater.from(mContext).inflate(
                R.layout.dialog_exit_remind_layout, null);
        final Dialog setDialog = new Dialog(mContext, R.style.DialogStyle);
        setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialog.setCancelable(false);
        setDialog.getWindow().setContentView(dialogView);
        WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
        lp.width = MyConfig.getWidthByScreenPercent(80);
        setDialog.getWindow().setAttributes(lp);
        Button cancleButton = (Button) dialogView.findViewById(R.id.cancle);
        Button okButton = (Button) dialogView.findViewById(R.id.login);
        TextView remindText = (TextView) dialogView.findViewById(R.id.remind_text);
        TextView titletext = (TextView) dialogView.findViewById(R.id.remind_title);
        titletext.setText(title);

        remindText.setText(owner + remindstr);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        JsonTransferCompanyOwner jsonTransferCompanyOwner = new JsonTransferCompanyOwner();
                        jsonTransferCompanyOwner.accessToken = MyConfig.usr_token;
                        jsonTransferCompanyOwner.newAdminId = adminId;
                        Gson gson = new Gson();
                        String jsonStr = gson.toJson(jsonTransferCompanyOwner, JsonTransferCompanyOwner.class);
                        MyLog.d("", "HttpTools: jsonStr   :" + mCompanyId + "   " + jsonStr);
                        final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_TRANCEFER + mCompanyId + "/transfer", jsonStr);
                        MyLog.d("", "HttpTools: sssssss   :" + s);
                        final JsonTransferCompanyOwnerRet jsonTransferCompanyOwnerRet = gson.fromJson(s, JsonTransferCompanyOwnerRet.class);
                        if (jsonTransferCompanyOwnerRet != null && jsonTransferCompanyOwnerRet.code==MyConfig.retSuccess()) {
                            // Intent intent = new Intent(mContext, ActivityCompanySetting.class);
//                            Intent intent = new Intent(mContext, ActivityViewPager.class);
//                            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, mCompanyId);
//                            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORID, adminId);
//                            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORNICK, owner);
//                            mContext.startActivity(intent);
                            MyConfig.getCurrentCompany();

                            finish();
                        }

                    }
                }).start();
                setDialog.cancel();
            }
        });
        cancleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setDialog.cancel();
            }
        });
        setDialog.setCanceledOnTouchOutside(false);
        setDialog.show();
    }


}
