package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.LinkedList;

import co.docy.oldfriends.Adapters.ListAdapterFriendCricleUserInfo;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.FriendCricleMsg;
import co.docy.oldfriends.Messages.JsonFriendCricleRet;
import co.docy.oldfriends.Messages.JsonFriendCricleWithHeadRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;

/**
 * Created by khp on 2016/7/29.
 */
public class ActivityFriendCricleUserInfo extends ActivityBase {
    ListAdapterFriendCricleUserInfo listAdapterCricleFriendUserInfo;
    SwipyRefreshLayout swipyrefreshlayout;
    LinkedList<FriendCricleMsg> list = new LinkedList<>();
    ImageView myfinish,mychoose;
    LinearLayout title_linear;
    ListView listView;
    int userId;
    String userNickname,userAvart;
    int page=1;
    View header;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cricle_friend_user_info);
        getSupportActionBar().hide();
        Intent intent =getIntent();
        userId = intent.getIntExtra("userId",-1);
        userNickname = intent.getStringExtra("userName");
        userAvart =intent.getStringExtra("userAvart");
        swipyrefreshlayout =(SwipyRefreshLayout) findViewById(R.id.activity_cricle_friend_userinfo_refresh);
        listView =(ListView) findViewById(R.id.activity_cricle_friend_userinfo_listview);
        swipyrefreshlayout.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
        swipyrefreshlayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                initdata();

            }
        });
        listAdapterCricleFriendUserInfo = new ListAdapterFriendCricleUserInfo(this,list,onclick);
        header = LayoutInflater.from(this).inflate(R.layout.circle_of_friends_headview,null);
        listView.addHeaderView(header);
        listView.setAdapter(listAdapterCricleFriendUserInfo);
        initDataWithHead(header);
        initdata();
        initDataWithHead();

    }

    @Override
    protected void onResume() {
        super.onResume();
        myfinish =(ImageView) findViewById(R.id.activtiy_cricle_of_friend_finish);
        mychoose =(ImageView) findViewById(R.id.activtiy_cricle_of_friend_choose);
        title_linear =(LinearLayout) findViewById(R.id.activtiy_cricle_of_friend_title);
        mychoose.setVisibility(View.GONE);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            //滑动时隐藏键盘
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(-header.getTop() > MyConfig.dp2Px(ActivityFriendCricleUserInfo.this,180)){
                    title_linear.setBackgroundResource(R.drawable.c_bg_calendar_clip_3x);
                }else{
                    title_linear.setBackgroundResource(R.color.transparent);
                }
            }
        });
        myfinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    ListAdapterFriendCricleUserInfo.OnClickitem onclick = new ListAdapterFriendCricleUserInfo.OnClickitem() {
        @Override
        public void onclickitem(int postion) {
            Intent intent  = new Intent(ActivityFriendCricleUserInfo.this,ActivityFriendCricleDetail.class);
            intent.putExtra("id",list.get(postion).id);
            startActivity(intent);
        }
    };
    ImageView bg_friend;
    public void initDataWithHead(View v){
        //listview上面的布局
        bg_friend =(ImageView) v.findViewById(R.id.activtiy_cricle_of_friend_bg);
        TextView myname =(TextView) v.findViewById(R.id.activtiy_cricle_of_friend_myname);
        ImageView myphoto =(ImageView) v.findViewById(R.id.activtiy_cricle_of_friend_myphoto);

        myname.setText(userNickname);
        Picasso.with(this).load(MyConfig.getApiDomain_NoSlash_GetResources() + userAvart)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(myphoto);

    }

    public void initDataWithHead(){
        //获取listview上面的图片
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                HashMap<String,String> map = new HashMap<>();
                map.put("userId",userId+"");
                String s = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2() +
                        MyConfig.API_CHANGE_FRIEND_LIKE + MyConfig.API_GET_DATA_OF_FRIEND_WITH_HEAD ,map,MyConfig.usr_token);
                MyLog.d("获取头部图片",s);
                final JsonFriendCricleWithHeadRet jsonFriendCricleWithHead = gson.fromJson(s,JsonFriendCricleWithHeadRet.class);
                if(jsonFriendCricleWithHead!=null&&jsonFriendCricleWithHead.code == MyConfig.retSuccess()
                        &&jsonFriendCricleWithHead.data!=null){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Glide.with(ActivityFriendCricleUserInfo.this).load(MyConfig.getApiDomain_NoSlash_GetResources() + jsonFriendCricleWithHead.data.profilePicture)
                                    .centerCrop()
                                    .into(bg_friend);
                        }
                    });
                }
            }
        }).start();
    }


    public void initdata(){
        //获取数据
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                HashMap<String,String> map = new HashMap<>();
                map.put("userId",userId+"");
                map.put("page",page+"");
                String s = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GET_DATA_OF_FRIEND ,map,MyConfig.usr_token);
                MyLog.d("kanghongpu","获取自己的列表"+s);
                final JsonFriendCricleRet jsonCricleOfFriendRet = gson.fromJson(s,JsonFriendCricleRet.class);
                if(jsonCricleOfFriendRet!=null&&jsonCricleOfFriendRet.code==MyConfig.retSuccess()
                        &&jsonCricleOfFriendRet.data!=null&&jsonCricleOfFriendRet.data.rows!=null){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            page++;
                            list.addAll(list.size(), FriendCricleMsg.chooseData(jsonCricleOfFriendRet.data.rows));
                            listAdapterCricleFriendUserInfo.notifyDataSetChanged();
                            swipyrefreshlayout.setRefreshing(false);
                        }
                    });
                }
            }
        }).start();
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
