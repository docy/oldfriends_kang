package co.docy.oldfriends.Activitis;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import co.docy.oldfriends.Adapters.ListAdapterChatRoom;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonGetChatRoomListRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

public class ActivityChatRoomList extends ActivityBase implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    private ListView chat_room_list;
    private MyConfig.MySearchView mSearchView;
    private ListAdapterChatRoom adapter;
    private ArrayList<JsonGetChatRoomListRet.ChatRoomListData> chatRoomListData;
    private String[] nicknames;
    private JsonGetChatRoomListRet jsonGetChatRoomList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        chat_room_list = (ListView) findViewById(R.id.chat_room_list);
        nicknames = getNicknamesFromAssets();
        initListener();
        getDataFromServer();
    }


    private void initListener() {
        chat_room_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDialogForNickname(position);
            }
        });
    }

    /**
     * 弹出一个对话框，输入你的昵称
     */
    private void showDialogForNickname(final int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ActivityChatRoomList.this);
        View view = View.inflate(ActivityChatRoomList.this, R.layout.dialog_input_nickname, null);
        final EditText chat_room_input_nick = (EditText) view.findViewById(R.id.chat_room_input_nick);
        chat_room_input_nick.setText(nicknames[new Random().nextInt(nicknames.length)]);/**得到随机的昵称*/

        final TextView chat_room_dialog_cancel = (TextView) view.findViewById(R.id.chat_room_dialog_cancel);
        final TextView chat_room_dialog_confirm = (TextView) view.findViewById(R.id.chat_room_dialog_confirm);
        builder.setView(view);

        final AlertDialog inputNicknameDialog = builder.create();
        chat_room_dialog_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String chatRoomNickname = chat_room_input_nick.getText().toString().trim();
                if (TextUtils.isEmpty(chatRoomNickname)) {
                    Toast.makeText(ActivityChatRoomList.this, "昵称不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(ActivityChatRoomList.this, ActivityDirectChatGroup.class);
                intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, chatRoomListData.get(position).id);
                intent.putExtra(MyConfig.CHAT_ROOM_NAME, chatRoomListData.get(position).name);
                intent.putExtra(MyConfig.CHAT_ROOM_ONLINE_NUM, chatRoomListData.get(position).userCount);
                intent.putExtra(MyConfig.CHAT_ROOM_NICK_NAME, chatRoomNickname);
                startActivity(intent);
                inputNicknameDialog.dismiss();
            }
        });

        chat_room_dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputNicknameDialog.dismiss();
            }
        });

        inputNicknameDialog.show();
        chat_room_input_nick.setSelection(chat_room_input_nick.getText().length());
        MyConfig.show_keyboard_must_call_from_editText(ActivityChatRoomList.this, chat_room_input_nick);


    }

    /**
     * 从服务器中获取聊天室列表
     */
    public void getDataFromServer() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String chatRooms = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUPS_CHATROOMS, MyConfig.usr_token);
                MyLog.d("kwwl", "获取的聊天室列表数据是=========================" + chatRooms);
                jsonGetChatRoomList = gson.fromJson(chatRooms, JsonGetChatRoomListRet.class);
                chatRoomListData = new ArrayList<>();
                if (jsonGetChatRoomList != null && jsonGetChatRoomList.data != null && jsonGetChatRoomList.data.size() > 0) {
                    chatRoomListData.addAll(jsonGetChatRoomList.data);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter = new ListAdapterChatRoom(ActivityChatRoomList.this, chatRoomListData);
                            chat_room_list.setAdapter(adapter);
                        }
                    });
                }
            }
        }).start();


    }

    /**
     * 读取nick_name.txt文件，获取昵称字符串数组
     *
     * @return
     */
    private String[] getNicknamesFromAssets() {
        try {
            InputStream is = getResources().getAssets().open("nick_name.txt");
            BufferedReader tBufferedReader = new BufferedReader(new InputStreamReader(is));
            StringBuffer sb = new StringBuffer();
            String strTemp = "";
            while ((strTemp = tBufferedReader.readLine()) != null) {
                sb.append(strTemp);
            }
            return sb.toString().split(",");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add("Search");
        item.setIcon(R.drawable.company_views_search);
        //   item.setIcon(getActivity().getResources().getDrawable(R.drawable.company_views_search))
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM
                | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        mSearchView = new MyConfig.MySearchView(ActivityChatRoomList.this);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        item.setActionView(mSearchView);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        chatRoomListData.clear();
        if (TextUtils.isEmpty(newText)) {
            chatRoomListData.addAll(jsonGetChatRoomList.data);
        } else{
            for (JsonGetChatRoomListRet.ChatRoomListData data : jsonGetChatRoomList.data) {
                if (data.name.contains(newText)) {
                    chatRoomListData.add(data);
                }
            }
        }
        adapter.mTextFilter = newText;
        adapter.notifyDataSetChanged();
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        return true;
    }

    @Override
    public boolean onClose() {
        if (!TextUtils.isEmpty(mSearchView.getQuery())) {
            mSearchView.setQuery(null, true);
        }
        return true;
    }


}
