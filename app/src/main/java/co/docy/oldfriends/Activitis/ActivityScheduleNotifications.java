package co.docy.oldfriends.Activitis;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Fragments.FragmentScheduleNotifications;
import co.docy.oldfriends.R;


public class ActivityScheduleNotifications extends ActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_port_fragment_linearlayout);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getBundleExtra(MyConfig.CONSTANT_PARAMETER_STRING_BUNDLE);

            init();

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            FragmentScheduleNotifications fragmentScheduleNotifications = FragmentScheduleNotifications.newInstance();
            fragmentTransaction.add(R.id.activity_port_fragment_linearLayout, fragmentScheduleNotifications);
            fragmentTransaction.commit();

        } else {
            finish();
        }

        getWindow().getDecorView().setBackgroundColor(getResources().getColor(R.color.xjt_background));


    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    private void init() {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



//    public void onEventMainThread(EventQuitGroup eventQuitGroup) {
//        finish();
//    }
}
