package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventActivityRecreate;
import co.docy.oldfriends.EventBus.EventAppQuit;
import co.docy.oldfriends.EventBus.EventLogout;
import co.docy.oldfriends.EventBus.EventNetConnectFail;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;
import rx.Subscription;

public class ActivityBase extends AppCompatActivity {

    protected LinkedList<Subscription> subscription_list = new LinkedList<>();

    boolean eventbut_registed = false;

    public String child_activity_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        MyLog.d("", "share in process: this activity is " + child_activity_name);

        if(MyConfig.activityViewPager == null){

            Intent intent = new Intent(this, ActivityViewPager.class);
            startActivity(intent);

            finish();
        }


    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
    @Override
    protected void onResume() {
        super.onResume();

    }

    /**
     * Dispatch onStart() to all fragments.  Ensure any created loaders are
     * now started.
     */
    @Override
    protected void onStart() {
        super.onStart();

        /**
         * eventbus消息的处理可能引用view控件，所以eventbus的注册要在view控件初始化之后，所以挪动到这里
         * view控件的初始化在onCreate中
         */
        if(!eventbut_registed) {
            EventBus.getDefault().register(this);
            eventbut_registed = true;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(eventbut_registed) {
            EventBus.getDefault().unregister(this);
            eventbut_registed = false;
        }

        Subscription sub;
        while ( (sub = subscription_list.poll()) != null) {

            if (sub != null && !sub.isUnsubscribed()) {
                sub.unsubscribe();
            }

        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        MyConfig.hide_keyboard_must_call_from_activity(this);

    }

    public void onEventMainThread(EventAppQuit eventAppQuit) {
        finish();
    }

    public void onEventMainThread(EventActivityRecreate eventActivityRecreate) {
        recreate();
    }

    public void onEventMainThread(EventLogout eventLogout) {
        if (this.getClass().getName().equals(MyConfig.activityViewPager.getClass().getName())) {
            MyLog.d("","app start steps: event logout keep viewpager -- "+this.getClass().getName());
        } else if (MyConfig.activityAccount!=null && this.getClass().getName().equals(MyConfig.activityAccount.getClass().getName())) {
            MyLog.d("","app start steps: event logout keep -- "+this.getClass().getName());
        } else {
            MyLog.d("","app start steps: event logout close -- "+this.getClass().getName());
            finish();
        }
    }

    public void onEventMainThread(EventNetConnectFail event) {
        //Snackbar.make(getView(), jsonSetAvatarRet.message, Snackbar.LENGTH_LONG).show();
        if (null != getCurrentFocus() ){

            MyConfig.ISnackBarListener iSnackBarListener = new MyConfig.ISnackBarListener() {
                @Override
                public void doListener() {
                    if(android.os.Build.VERSION.SDK_INT > 10 ){
                        //3.0以上打开设置界面，也可以直接用ACTION_WIRELESS_SETTINGS打开到wifi界面
                        startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                    } else {
                        startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
                    }
                }
            };
            MyConfig.snackbarTop(getCurrentFocus(), getResources().getString(R.string.net_connect_error), R.color.xjt_mainroom_unread_color, Snackbar.LENGTH_LONG ,iSnackBarListener);
        }
    }
}
