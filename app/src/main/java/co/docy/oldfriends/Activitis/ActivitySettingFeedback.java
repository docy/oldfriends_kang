package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonGetFeedbackRet;
import co.docy.oldfriends.Messages.JsonSubmitFeedback;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

public class ActivitySettingFeedback extends ActivityBase {

    private EditText etFeedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_feedback);
        initView();
    }

    /**
     * 初始化View对象
     */
    private void initView() {
        etFeedback = (EditText) findViewById(R.id.et_feedback);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting_feedback, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.setting_feedback:
                    submitFeedback();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * 提交数据
     */
    private void submitFeedback() {
        final String content = etFeedback.getText().toString();
        if(TextUtils.isEmpty(content)){
            Toast.makeText(this ,"反馈内容不能为空",Toast.LENGTH_SHORT).show();
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonSubmitFeedback jsonSubmitFeedback = new JsonSubmitFeedback();
                jsonSubmitFeedback.accessToken = MyConfig.usr_token;

                /**暂时让输入的内容为title，内容为“”*/
                jsonSubmitFeedback.title = content;
                jsonSubmitFeedback.content = "";

               

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonSubmitFeedback, JsonSubmitFeedback.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SETTING_FEEDBACK , jsonStr);

               final JsonGetFeedbackRet jsonGetFeedbackRet = gson.fromJson(s, JsonGetFeedbackRet.class);
                MyLog.d("", "HttpTools: " + s);
                if (jsonGetFeedbackRet != null && jsonGetFeedbackRet.code==MyConfig.retSuccess()) {

                    ActivitySettingFeedback.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ActivitySettingFeedback.this ,"提交成功",Toast.LENGTH_SHORT).show();
                            ActivitySettingFeedback.this.finish();
                        }
                    });
                }

            }
        }).start();
    }
}
