package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonGetGroupUserRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;

/**
 * 选择被@的人的页面
 */
public class ActivityPegSomeBody extends ActivityBase {

    private ListView mListView;
    private ProgressBar mProgressBar;
    private JsonGetGroupUserRet jsonGetGroupUserRet;
    private LinkedList<JsonGetGroupUserRet.GetGroupUserRet> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_peg_some_body);
        mListView = (ListView)findViewById(R.id.lv_peg_some_body);
        mProgressBar = (ProgressBar)findViewById(R.id.pb_peg_some_body);
        getGroupUsers();
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MyConfig.PEG_SOME_BODY_USER_ID = data.get(position).id;
                MyConfig.PEG_SOME_BODY_USER_NICK_NAME = data.get(position).nickName;

                setResult(RESULT_OK);
                finish();
            }
        });
    }



    /**
     * 获取小组成员信息
     */
    private void getGroupUsers() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String, String> hm = new HashMap<>();
                hm.put("order", "name");
                String s = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_USERS + MyConfig.PEG_SOME_BODY_GROUP_ID + MyConfig.API2_GROUP_USERS_2 , hm, MyConfig.usr_token);

                Gson gson = new Gson();
               // String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_USERS + MyConfig.PEG_SOME_BODY_GROUP_ID + MyConfig.API2_GROUP_USERS_2, MyConfig.usr_token);
                jsonGetGroupUserRet = gson.fromJson(s, JsonGetGroupUserRet.class);
                data = jsonGetGroupUserRet.data;

                ActivityPegSomeBody.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (jsonGetGroupUserRet!= null && jsonGetGroupUserRet.code == MyConfig.retSuccess()) {

                            mProgressBar.setVisibility(View.GONE);

                            mListView.setAdapter(new MyAdapter());
                        }

                    }
                });


            }
        }).start();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class MyAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public JsonGetGroupUserRet.GetGroupUserRet getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MyLog.d("","ListAdapterContactOfCompanyMultiChoose position "+position);
            JsonGetGroupUserRet.GetGroupUserRet userRet = data.get(position);
            final ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = View.inflate(ActivityPegSomeBody.this,R.layout.row_contact_multi_choose, null);
                viewHolder.contact_layout = (LinearLayout)convertView.findViewById(R.id.contact_layout_person);
                viewHolder.contact_avatar = (ImageView) convertView.findViewById(R.id.contact_avatar);
                viewHolder.contact_name = (TextView) convertView.findViewById(R.id.contact_desc);
                viewHolder.contact_phone = (TextView) convertView.findViewById(R.id.contact_phone);
                viewHolder.contact_choose = (ToggleButton) convertView.findViewById(R.id.contact_choose);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            Picasso.with(ActivityPegSomeBody.this).load(MyConfig.getApiDomain_NoSlash_GetResources() + userRet.avatar)
//                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .fit().centerCrop()
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(viewHolder.contact_avatar);

            viewHolder.contact_name.setText(userRet.nickName);
            viewHolder.contact_phone.setVisibility(View.GONE);
            viewHolder.contact_choose.setVisibility(View.GONE);
            return convertView;
        }
    }

    private static class ViewHolder {
        LinearLayout contact_layout;
        ImageView contact_avatar;
        TextView contact_name;
        TextView contact_phone;
        ToggleButton contact_choose;
    }
}
