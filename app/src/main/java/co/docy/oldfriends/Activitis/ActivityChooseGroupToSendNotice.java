package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;

import co.docy.oldfriends.Adapters.ListAdapterChooseGroupToSendNotice;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonChooseGroupToSendNotice;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/6/7.
 */
public class ActivityChooseGroupToSendNotice extends ActivityBase {
    ListView choose_group_listview;
    ListAdapterChooseGroupToSendNotice listAdapterChooseGroupToSendNotice;
    JsonChooseGroupToSendNotice jsonChooseGroupToSendNotice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_group_tosend_notice);
        choose_group_listview =(ListView) findViewById(R.id.activity_choose_group_listview);
        new Thread(new Runnable() {
            @Override
            public void run() {
                String str = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CHOOSE_GROUP ,MyConfig.usr_token);
                MyLog.d("查看发通知小组",str);
               jsonChooseGroupToSendNotice = new Gson().fromJson(str,JsonChooseGroupToSendNotice.class);
                if(jsonChooseGroupToSendNotice.data!= null&&jsonChooseGroupToSendNotice.code==MyConfig.retSuccess()){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listAdapterChooseGroupToSendNotice = new ListAdapterChooseGroupToSendNotice(ActivityChooseGroupToSendNotice.this,
                                     jsonChooseGroupToSendNotice.data ,onclicklisten,onchooseclicklisten);
                            choose_group_listview.setAdapter(listAdapterChooseGroupToSendNotice);
                        }
                    });
                }
            }
        }).start();
    }
    ListAdapterChooseGroupToSendNotice.OnClickedListener onclicklisten = new ListAdapterChooseGroupToSendNotice.OnClickedListener() {
        @Override
        public void OnClicked(int position) {

        }
    };

    ArrayList<String> name = new ArrayList<>();
    ArrayList<Integer> id = new ArrayList<>();
    ListAdapterChooseGroupToSendNotice.OnChooseChangedListener onchooseclicklisten = new ListAdapterChooseGroupToSendNotice.OnChooseChangedListener() {
        @Override
        public void OnClicked(int position, boolean isChecked) {
                    if(isChecked){
                        id.add(jsonChooseGroupToSendNotice.data.get(position).id);
                        name.add(jsonChooseGroupToSendNotice.data.get(position).name);
                     }else{
                        for(int i=0;i<name.size();i++){
                            if(name.get(i).equals(jsonChooseGroupToSendNotice.data.get(position).name)){
                                name.remove(i);
                                id.remove(i);
                            }
                        }

                    }
        }
    };


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_company, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.create_company:
                Intent intent = new Intent();
                intent.putStringArrayListExtra("name",name);
                intent.putIntegerArrayListExtra("id",id);
                setResult(RESULT_OK,intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
