package co.docy.oldfriends.Activitis;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.io.File;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonFriendCricleSendNewContentRet;
import co.docy.oldfriends.Messages.JsonFriendCricleSendNewContent;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;

/**
 * Created by khp on 2016/7/22.
 */
public class ActivityFriendCircleSendNewContent extends ActivityBase {
    EditText title_input;
    ImageView body_image;
    File attached;
    String strTitle;
    View dialogView;
    Dialog setDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_to_friendcircle);
        title_input =(EditText) findViewById(R.id.activity_send_friendcircle_edittext);
        body_image =(ImageView) findViewById(R.id.activity_send_friendcircle_image);

        Intent intent = getIntent();
        String filePath = intent.getStringExtra("file");
        if (filePath != null) {
            attached = new File(filePath);
            MyLog.d("kanghongpu",attached.getAbsolutePath());
        }
        strTitle = intent.getStringExtra("title");
        setTitle(strTitle);
        if(attached!=null) {
            Glide.with(this).load(Uri.fromFile(attached)).into(body_image);
        }

        dialogView = LayoutInflater.from(this).inflate(
                R.layout.dialog_user_picture, null);
        setDialog = new Dialog(this, R.style.DialogStyle);
    }
    Menu menu;
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_notice, menu);
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.create_notice:
                if(strTitle.equals("动态")){
                    indata();
                }else if(strTitle.equals("相机")||strTitle.equals("图片")){
                    sendImageToServer(null);
                    toastSendState();
                }else if(strTitle.equals("小视屏")){
                    sendImageToServer(MyConfig.os_string);
                    toastSendState();

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void indata(){
        final String title = title_input.getText().toString();
        if(title==null||title.length()==0){
            Toast.makeText(ActivityFriendCircleSendNewContent.this,"请输入内容",Toast.LENGTH_SHORT).show();
        }else {
            menu.findItem(R.id.create_notice).setEnabled(false);
            toastSendState();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Gson gson = new Gson();
                    JsonFriendCricleSendNewContent jsonSendFriendOfCricle = new JsonFriendCricleSendNewContent();
                    jsonSendFriendOfCricle.accessToken = MyConfig.usr_token;
                    jsonSendFriendOfCricle.title = title;
                    String strjsonSendFriendOfCricle = gson.toJson(jsonSendFriendOfCricle, JsonFriendCricleSendNewContent.class);
                    String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GET_DATA_OF_FRIEND, strjsonSendFriendOfCricle);
                    MyLog.d("查看发送朋友圈", s);
                    final JsonFriendCricleSendNewContentRet jsonCricleOfFriendRet = gson.fromJson(s, JsonFriendCricleSendNewContentRet.class);
                    if (jsonCricleOfFriendRet != null && jsonCricleOfFriendRet.code == MyConfig.retSuccess()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                EventBus.getDefault().post(jsonCricleOfFriendRet);
                                finish();
                            }
                        });

                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                menu.findItem(R.id.create_notice).setEnabled(true);
                                setDialog.dismiss();
                                Toast.makeText(ActivityFriendCircleSendNewContent.this, "发送失败，请重新发送", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }).start();
        }
    }
    public void sendImageToServer(final String os){
        menu.findItem(R.id.create_notice).setEnabled(false);
        final String title = title_input.getText().toString();
        new Thread(new Runnable() {
            @Override
            public void run() {
                final JsonFriendCricleSendNewContentRet jsonSendCricleOfFriendRet =HttpTools.okhttpUploadFriendImage(os,title,attached);
                if (jsonSendCricleOfFriendRet!= null && jsonSendCricleOfFriendRet.code == MyConfig.retSuccess()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            EventBus.getDefault().post(jsonSendCricleOfFriendRet);
                            finish();
                        }
                    });

                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            menu.findItem(R.id.create_notice).setEnabled(true);
                            setDialog.dismiss();
                            Toast.makeText(ActivityFriendCircleSendNewContent.this,"发送失败，请重新发送",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }


    public void toastSendState(){
        setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialog.setCancelable(true);
        setDialog.getWindow().setContentView(dialogView);
        setDialog.show();
    }

}
