package co.docy.oldfriends.Activitis;


import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import co.docy.oldfriends.Adapters.ListAdapterMsg;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventFileDownloadProgress;
import co.docy.oldfriends.EventBus.EventGroupListUpdateFromServer;
import co.docy.oldfriends.EventBus.EventHeart200ms;
import co.docy.oldfriends.EventBus.EventHeartOneSecond;
import co.docy.oldfriends.EventBus.EventListviewScorll;
import co.docy.oldfriends.EventBus.EventMainroomGotoUnread;
import co.docy.oldfriends.EventBus.EventIMsgSoundPlayEnd;
import co.docy.oldfriends.EventBus.EventScreenSwitch;
import co.docy.oldfriends.EventBus.EventSetSoundRemind;
import co.docy.oldfriends.EventBus.EventSingleMsgDelete;
import co.docy.oldfriends.EventBus.EventTopicDelete;
import co.docy.oldfriends.EventBus.EventUpdateActivityNumInMain;
import co.docy.oldfriends.Media.Player;
import co.docy.oldfriends.Media.Recorder;
import co.docy.oldfriends.DataClass.IMsg;
import co.docy.oldfriends.Messages.JsonGetEventRet;
import co.docy.oldfriends.Messages.JsonGetGroupInfoRet;
import co.docy.oldfriends.Messages.JsonGetMsgRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicCommentRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicRet;
import co.docy.oldfriends.Messages.JsonGroupResetUnread;
import co.docy.oldfriends.Messages.JsonSendFileRet;
import co.docy.oldfriends.Messages.JsonSendImageRet;
import co.docy.oldfriends.Messages.JsonSendMapRet;
import co.docy.oldfriends.Messages.JsonSendMsg;
import co.docy.oldfriends.Messages.JsonSendMsgRet;
import co.docy.oldfriends.Messages.JsonSendSoundMsgRet;
import co.docy.oldfriends.Messages.JsonSendTopic;
import co.docy.oldfriends.Messages.JsonSendTopicActivityCreate;
import co.docy.oldfriends.Messages.JsonSendTopicActivityCreateRet;
import co.docy.oldfriends.Messages.JsonSendTopicRet;
import co.docy.oldfriends.Messages.JsonSendTopicVoteCreate;
import co.docy.oldfriends.Messages.JsonSendTopicVoteCreateRet;
import co.docy.oldfriends.Messages.JsonSendVideoRet;
import co.docy.oldfriends.Messages.JsonSoundMsg;
import co.docy.oldfriends.Messages.JsonUploadImageRet;
import co.docy.oldfriends.Messages.JsonUserGroupsRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.emoji.EmojiFragment;
import co.docy.oldfriends.emoji.EmojiUtils;
import de.greenrobot.event.EventBus;
import info.hoang8f.android.segmented.SegmentedGroup;


public class ActivityMainGroup extends ActivityBase implements OnClickListener {

    PopupWindow popupWindow;

    LinearLayout subject_button_universal_layout;
    TextView subject_button_universal_cancel;
    View subject_button_universal_gray;
    TextView   universal_subject, universal_file, universal_map, universal_vote,universal_activity;

    RadioButton mainroom_talk_view, mainroom_subject_list_view;
    SegmentedGroup room_list_segment;
    float view_y_origin;
    private ArrayList<Integer> targets = new ArrayList<>();
    private EmojiFragment _fragment;
    private ImageView iv_add_emoji;
    private FrameLayout fl_emoji_fragment;
    private boolean emojiIsVisible = false;
    private TextView universal_camera;
    private TextView universal_video;
    private TextView universal_gallery;
    private SensorManager mSensorManager;
    private AudioManager audioManager;
    private Sensor mAudioSensor;
    private MySensorEventListener mySensorEventListener;

    private void initSegmentButton() {
        room_list_segment = (SegmentedGroup) findViewById(R.id.mainroom_segment);
        room_list_segment.setTintColor(getResources().getColor(R.color.xjt_toolbar_blue));
        room_list_segment.getLayoutParams().width = MyConfig.screenWidth * 2 / 3;
        view_y_origin = room_list_segment.getY();
//        room_list_segment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if (checkedId == R.id.mainroom_talk_view) {
//                    if(menu != null) {
//                        menu.findItem(R.id.main_room_info).setVisible(true);
//                        MyLog.d("", "sequence of optionmenu create: setVisible(true)");
//                    }
//                    fragmentSubjectList.setVisibile(View.GONE);
//                    fragmentSubjectList.setHasOptionsMenu(false);
//                } else {
//                    if(menu != null) {
//                        menu.findItem(R.id.main_room_info).setVisible(false);
//                        MyLog.d("", "sequence of optionmenu create: setVisible(false)");
//                    }
//                    fragmentSubjectList.setVisibile(View.VISIBLE);
//                    fragmentSubjectList.setHasOptionsMenu(true);
//                }
//
//            }
//        });
        room_list_segment.getLayoutParams().width = MyConfig.screenWidth * 2 / 3;

        mainroom_talk_view = (RadioButton) findViewById(R.id.mainroom_talk_view);
        mainroom_subject_list_view = (RadioButton) findViewById(R.id.mainroom_subject_list_view);

        mainroom_talk_view.setChecked(true);
    }

    //    FragmentSubjectList fragmentSubjectList;
    private void initSubjectListFragment() {
//        fragmentSubjectList = (FragmentSubjectList)getFragmentManager().findFragmentById(R.id.main_fragment_subject_list);
//        fragmentSubjectList.setVisibile(View.GONE);//20160429又不用了
////        fragmentSubjectList.setHasOptionsMenu(false);
    }


    private JsonGetGroupInfoRet jsonGetGroupInfoRet;
    private LinearLayout mainroom_input_panel;
    private TextView main_goto_unread, bSend, ButtonRecord, remind_text;
    private ImageView ivPlus;
    private EditText et;
    private PullToRefreshListView lv_msg;
    private FloatingActionMenu fam;
    private FloatingActionButton fab_plus_subject, fab_plus_camera, fab_plus_gallery, fab_plus_file, fab_plus_map, fab_plus_vote;
    private ListAdapterMsg la_msg;
    private JsonSoundMsg jsonSoundMsg;
    private JsonGetEventRet.GetEventRet getEventRet;
    private JsonGetMsgRet.GetMsgRet getMsgRet;
    private JsonGetMsgTopicRet.GetMsgTopicRet getMsgTopicRet;
    private JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet getMsgTopicCommentRet;
    public IMsg imsMainRoomRoot;
    private String name, desc, tempAttachMime, sharedText;
    int mUnread = 0, groupId = -1, msgPageCount = 0, time_in_room, mainroom_started_by_share_in = -1, room_type = MyConfig.LOCAL_ROOM_CATEGORY_NORMAL;
    private File tempAttach;
    private ActionBar ab;
    private ImageView fam_image;
    private Button ButtonSwitchInput;
    private boolean input_mode_record = false, isGiveUp = true;
    private Recorder mRecord;
    private ImageView soundImageView, cancle_view, microphone;
    private RelativeLayout recording_layout;
    private Player player = null;
    private float locationY;
    private Recorder.IUIApdate mIUiupdate = new Recorder.IUIApdate() {
        @Override
        public void UiApdate(double MaxAmplitude) {
            double amp = MyConfig.Amplitudescale(MaxAmplitude);
            MyLog.d("", "MaxAmplitude====+++=======" + amp);
            if (amp < MyConfig.SOUND_LEVEL_1) {
                soundImageView.setImageResource(R.drawable.sound1);
            } else if (amp < MyConfig.SOUND_LEVEL_2) {
                soundImageView.setImageResource(R.drawable.sound2);
            } else if (amp < MyConfig.SOUND_LEVEL_3) {
                soundImageView.setImageResource(R.drawable.sound3);
            } else if (amp < MyConfig.SOUND_LEVEL_4) {
                soundImageView.setImageResource(R.drawable.sound4);
            } else if (amp < MyConfig.SOUND_LEVEL_5) {
                soundImageView.setImageResource(R.drawable.sound5);
            } else {
                soundImageView.setImageResource(R.drawable.sound6);
            }
        }
    };

    MyConfig.IOnOkClickListener listener = new MyConfig.IOnOkClickListener() {
        @Override
        public void OnOkClickListener() {
            finish();
        }
    };

    private boolean touched_to_clear_unread = false;
    private boolean touched_to_record = false;
    boolean last_touch_state = false;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);

        parseIntent(intent);

        initGroup();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_room);
        MyConfig.activityMainGroup = this;
//        socketTools = new SocketTools();

        ab = getSupportActionBar();

//        slidingMenuSetting(savedInstanceState);
//        slidingMenuAttach();
        initView();


        Intent intent = getIntent();
        if (intent != null) {
            parseIntent(intent);

        } else {
            finish();
        }


        initGroup();
        EventBus.getDefault().post(new EventSetSoundRemind(true));

        initSubjectListFragment();
        initSegmentButton();
        MyLog.d("", "sequence of optionmenu create: 1");

        initPopUp();

        initAudioMode();

    }

    public void parseIntent(Intent intent) {
        groupId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, -1);
        MyConfig.subjectListGroupId = groupId;//pass to subject list fragment
        name = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME);
        desc = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC);
        mainroom_started_by_share_in = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_MAINROOM_START_MODE, -1);

        MyLog.d("", "share in process: groupId " + groupId + " categery " + MyConfig.share_in_category + ", mainroom_started_by_share_in=" + mainroom_started_by_share_in);

        if (mainroom_started_by_share_in == MyConfig.MAINROOM_STARTED_FOR_SHARE_IN) {


            switch (MyConfig.share_in_category) {
                case MyConfig.SHARE_IN_CATEGORY_FILE:
                    //发表文件主题
                    String realPathFile = MyConfig.getUriPath(this, MyConfig.extra_stream);
                    if (realPathFile != null) {
                        tempAttach = new File(realPathFile);
                        MyLog.d("", "share in process: mainroom get name " + tempAttach.getName());
                    }
                    openSubjectInputWithAttach(MyConfig.REQUEST_SUBJECT_WITH_FILE, MyConfig.extra_subject, MyConfig.extra_text, realPathFile, MyConfig.SHARE_IN_CATEGORY_FILE);
                    break;
                case MyConfig.SHARE_IN_CATEGORY_TEXT:
                    if (MyConfig.extra_subject != null) {
                        //如果有subject，发表文本主题
                        openSubjectInput(MyConfig.REQUEST_SUBJECT_ONLY, MyConfig.extra_subject, MyConfig.extra_text);
                    } else {
                        //如果没有，发表普通文本msg
                        preload_shared_msg_text();
                    }
                    break;
                case MyConfig.SHARE_IN_CATEGORY_IMAGE:

                    String realPathImage = MyConfig.getUriPath(this, MyConfig.extra_stream);
                    tempAttach = new File(realPathImage);
                    MyLog.d("", "share in process: mainroom get name " + tempAttach.getName());


                    /**
                     * 根据目前和ios的约定，如果分享进来的图片主题中标题或正文含有link，则视为新闻，此刻需要加上『news』标记，并且link放在标题里面
                     */
                    if (MyConfig.extra_subject != null && MyConfig.extra_subject.contains("http")) {
                        openSubjectInputWithAttach(MyConfig.REQUEST_SUBJECT_WITH_IMAGE, MyConfig.extra_subject, MyConfig.extra_text, realPathImage, MyConfig.SHARE_IN_CATEGORY_NEWS);
                    } else if (MyConfig.extra_text != null && MyConfig.extra_text.contains("http")) {
                        MyConfig.extra_subject = MyConfig.extra_text;
                        MyConfig.extra_text = null;
                        openSubjectInputWithAttach(MyConfig.REQUEST_SUBJECT_WITH_IMAGE, MyConfig.extra_subject, MyConfig.extra_text, realPathImage, MyConfig.SHARE_IN_CATEGORY_NEWS);
                    } else {
                        //发表图片主题
                        openSubjectInputWithAttach(MyConfig.REQUEST_SUBJECT_WITH_IMAGE, MyConfig.extra_subject, MyConfig.extra_text, realPathImage, MyConfig.SHARE_IN_CATEGORY_IMAGE);
                    }

                    break;
                case MyConfig.SHARE_IN_CATEGORY_IMAGE_LIST:

                    //发表多张图片主题

                    break;

            }

        }
    }

    public void preload_shared_msg_text() {
        sharedText = MyConfig.extra_text;
        MyLog.d("", "share in process: mainroom get " + sharedText);

        if (sharedText != null) {
            MyLog.d("", "share in process: et set " + sharedText);
            et.setText(getResources().getString(R.string.from_share) + sharedText);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        newbie_3();
        time_in_room = 0;
        MyLog.d("", "sequence of optionmenu create: 2");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (player != null) {
//            player.pause();
            player.release();
            player = null;
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**取消距离监听器*/
        if(mySensorEventListener != null) {
            mSensorManager.unregisterListener(mySensorEventListener);
        }
        EventBus.getDefault().post(new EventSetSoundRemind(false));
        // Unregister
        //EventBus.getDefault().unregister(this);
        if(mRecord!=null){
            mRecord.stopRecord();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public int count = -1;

    public void onEventMainThread(EventHeart200ms eventHeart200ms) {
        if (count >= 0) {
            count++;
        }
        if (count >= (MyConfig.MAX_RECORD_TIME * 1000 / 200)) {
            int duration = 0;
            //stop record
            if (mRecord != null) {
                count = -1;
                duration = mRecord.stopRecord();
                mRecord = null;
                ButtonRecord.setBackgroundResource(R.drawable.button_record_up);
                ButtonRecord.setText(getResources().getString(R.string.touch_to_speak));
                ButtonRecord.setTextColor(getResources().getColor(R.color.xjt_time_link));
                touched_to_record = false;
                recording_layout.setVisibility(View.GONE);
                MyLog.d("", "count==========" + count);
                getSoundMsg2Send(duration);
            }
        } else if (count >= ((MyConfig.MAX_RECORD_TIME - 10) * 1000 / 200)) {
            if (locationY < -500) {
                cancle_view.setVisibility(View.VISIBLE);
                microphone.setVisibility(View.GONE);
                soundImageView.setVisibility(View.GONE);
                remind_text.setText(getResources().getString(R.string.record_leave_to_giveup_remind));
            } else {
                cancle_view.setVisibility(View.GONE);
                microphone.setVisibility(View.VISIBLE);
                soundImageView.setVisibility(View.VISIBLE);
                String format = getResources().getString(R.string.record_last_time);
                String result = String.format(format, MyConfig.MAX_RECORD_TIME - (count * 200 / 1000));
                remind_text.setText(result);
            }

        } else {
            if (locationY < -500) {
                cancle_view.setVisibility(View.VISIBLE);
                microphone.setVisibility(View.GONE);
                soundImageView.setVisibility(View.GONE);
                remind_text.setText(getResources().getString(R.string.record_leave_to_giveup_remind));
            } else {
                cancle_view.setVisibility(View.GONE);
                microphone.setVisibility(View.VISIBLE);
                soundImageView.setVisibility(View.VISIBLE);
                remind_text.setText(getResources().getString(R.string.record_up_to_giveup_remind));
            }
        }

        if ((last_touch_state) && (touched_to_record)) {
            //start record
            if (mRecord == null) {
                if (player != null) {
                    player.release();
                    player = null;
                }
                mRecord = new Recorder(MyConfig.tempSoundFile);
                mRecord.startRecord();
                count = 0;
            } else {
                mIUiupdate.UiApdate(mRecord.mediaRecorder.getMaxAmplitude());
            }

        } else if ((!last_touch_state) && (!touched_to_record)) {
            int duration = 0;
            //stop record
            if (mRecord != null) {
                count = -1;
                duration = mRecord.stopRecord();
                mRecord = null;
                if (!isGiveUp && duration <= 60) {  //send Message
                    getSoundMsg2Send(duration);
                }
            }
        }

        last_touch_state = touched_to_record;

    }

    private void initView() {

        ButtonRecord = (TextView) findViewById(R.id.mainroom_record_button);
        ButtonSwitchInput = (Button) findViewById(R.id.switch_input);

        et = (EditText) findViewById(R.id.mainroom_input_edit);

        et.setOnClickListener(this);


        /**这个Fragment用来显示表情，并将et和Fragment绑定在一起*/
        _fragment = (EmojiFragment) getSupportFragmentManager().findFragmentById(R.id.emoji_fragment_main);
        _fragment.setEditTextHolder(et);

        iv_add_emoji = (ImageView) findViewById(R.id.iv_add_emoji);
        iv_add_emoji.setOnClickListener(this);

        fl_emoji_fragment = (FrameLayout) findViewById(R.id.fl_emoji_fragment);

        main_goto_unread = (TextView) findViewById(R.id.main_goto_unread);
        lv_msg = (PullToRefreshListView) findViewById(R.id.main_lv_msg);
//        lv_msg.getRefreshableView().setOnScrollListener(onScrollListener);
        bSend = (TextView) findViewById(R.id.mainroom_send_msg);
        ivPlus = (ImageView) findViewById(R.id.mainroom_plus);
        ivPlus.setOnClickListener(this);
        soundImageView = (ImageView) findViewById(R.id.sound);
        cancle_view = (ImageView) findViewById(R.id.cancel_view);
        microphone = (ImageView) findViewById(R.id.microphone);
        recording_layout = (RelativeLayout) findViewById(R.id.recording_layout);
        remind_text = (TextView) findViewById(R.id.remind_text);


        ButtonSwitchInput.setOnClickListener(this);

        bSend.setOnClickListener(this);

        ButtonRecord.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                MyLog.d("", "recording in mainroom: ButtonRecord any" + event.getAction());
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        locationY = event.getY();
                        MyLog.d("", "recording in mainroom: ButtonRecord ACTION_DOWN");
                        touched_to_record = true;
                        ButtonRecord.setBackgroundResource(R.drawable.button_recorder);
                        ButtonRecord.setText(getResources().getString(R.string.leave_to_send));
                        ButtonRecord.setTextColor(getResources().getColor(R.color.White));
                        recording_layout.setVisibility(View.VISIBLE);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        locationY = event.getY();
                        break;
                    case MotionEvent.ACTION_UP:
//                        MyLog.d("", "recording in mainroom: ButtonRecord ACTION_UP");
                        ButtonRecord.setBackgroundResource(R.drawable.button_record_up);
                        ButtonRecord.setText(getResources().getString(R.string.touch_to_speak));
                        ButtonRecord.setTextColor(getResources().getColor(R.color.xjt_time_link));
                        touched_to_record = false;
                        recording_layout.setVisibility(View.GONE);
                        MyLog.d("", "recording in mainroom: ButtonRecord ACTION_UP === " + event.getRawY() + "  " + event.getY() + "  " + event.getYPrecision());
                        if (event.getY() < -500) {
                            //give up
                            isGiveUp = true;
                        } else {
                            //send anyway
                            isGiveUp = false;
                        }

                        break;
                    default:
                        break;
                }
                return true;
            }
        });


        lv_msg.getRefreshableView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (MotionEvent.ACTION_DOWN == event.getAction()) {
                    MyLog.d("", "unread view : touch down");
                    touched_to_clear_unread = true;
                } else if (MotionEvent.ACTION_UP == event.getAction()) {
                    MyLog.d("", "unread view : touch up");
                    touched_to_clear_unread = false;
                    MyConfig.hide_keyboard_must_call_from_activity(ActivityMainGroup.this);
                    hideEmojiDialog();
                }
                return false;
            }
        });

        lv_msg.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem = -1;
            private boolean hide = false;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

//                MyLog.d("", "listview scroll: onScrollStateChanged scrollState" + scrollState);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                MyLog.d("", "listview scroll: onScroll" + firstVisibleItem + " " + visibleItemCount + " " + totalItemCount);
                if (firstVisibleItem + visibleItemCount < totalItemCount - MyConfig.room_listview_scroll_threshold) {
                    MyLog.d("", "listview scroll: onScroll TRANSCRIPT_MODE_DISABLED " + firstVisibleItem + " " + visibleItemCount + " " + totalItemCount);
                    view.setTranscriptMode(ListView.TRANSCRIPT_MODE_DISABLED);

                } else {
                    view.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
                    MyLog.d("", "listview scroll: onScroll TRANSCRIPT_MODE_ALWAYS_SCROLL " + firstVisibleItem + " " + visibleItemCount + " " + totalItemCount);
                }

                //无论向上还是向下滑动时，未读信息消失
//                    MyLog.d("", "unread view 0: " + mLastFirstVisibleItem + " " + firstVisibleItem);
                if (mLastFirstVisibleItem >= 0) {
                    if (firstVisibleItem < mLastFirstVisibleItem) {
//                            MyLog.d("", "unread view 1: " + mLastFirstVisibleItem + " " + firstVisibleItem);
                        if (touched_to_clear_unread) {
                            main_goto_unread.setVisibility(View.INVISIBLE);
                        }
                        if (hide) {
                            segmentButtonMoveDown();
                            hide = false;
                        }

                    }
                    if (firstVisibleItem > mLastFirstVisibleItem) {
//                            MyLog.d("", "unread view 2: " + mLastFirstVisibleItem + " " + firstVisibleItem);
                        if (touched_to_clear_unread) {
                            main_goto_unread.setVisibility(View.INVISIBLE);
                        }
                        if (!hide) {
                            segmentButtonMoveUp();
                            hide = true;
                        }

                    }
                }
                mLastFirstVisibleItem = firstVisibleItem;


            }
        });

        lv_msg.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                // ...
//                new AsyncTask<Void, Void, Void>() {
//                    @Override
//                    protected Void doInBackground(Void... params) {
//                        // 处理刷新任务
//                        try {
//                            Thread.sleep(2000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                        return null;
//                    }
//
//                    @Override
//                    protected void onPostExecute(Void reslst) {
//                        // 更行内容，通知 PullToRefresh 刷新结束
//                        lv_msg.onRefreshComplete();
//                    }
//                }.execute();

//                getGroupMsg();

                new AsyncTaskGetMoreMsg(new AfterGetMoreMsg() {
                    @Override
                    public void afterGetMoreMsg(int newItemCount) {
//                        if (MyConfig.netConnectCheck() <= 0){
//                            MyConfig.MyToast(0,ActivityMainGroup.this,getString(R.string.net_connect_error));
//                            return;
//                        }
                        lv_msg.onRefreshComplete();

                        if (newItemCount > 0) {
                            //更新msg列表
//                            MyConfig.buildTimeShowString(imsMainRoomRoot.ll_sub_msg);

                            la_msg.notifyDataSetChanged();
                            lv_msg.getRefreshableView().setSelectionFromTop(newItemCount + 1, 0);

                        }
                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            }
        });


        ButtonRecord.setVisibility(View.GONE);
        et.setVisibility(View.VISIBLE);


        et.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN)) {
                    getMsg2Send();
                    return true;
                }
                return false;
            }
        });
        et.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 1) {
                    bSend.setVisibility(View.VISIBLE);
                    ivPlus.setVisibility(View.GONE);
//                    fam.setVisibility(View.GONE);

                } else {
                    bSend.setVisibility(View.GONE);
                    ivPlus.setVisibility(View.VISIBLE);
//                    fam.setVisibility(View.VISIBLE);
                }

                MyConfig.pegSomeBody(ActivityMainGroup.this, groupId, s);
            }
        });




        fam = (FloatingActionMenu) findViewById(R.id.fab_menu);
        fam_image = fam.getMenuIconView();
        fam_image.setImageResource(R.drawable.fab_plus);
        fam.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean b) {
                if (b) {
                    fam_image.setImageResource(R.drawable.fab_back);
                    MyLog.d("", "hide keyboard: mainroom fam call hide()");
//                    MyConfig.hideSoftKeyboard(ActivityMainGroup.this, null);
                    MyConfig.hide_keyboard_must_call_from_activity(ActivityMainGroup.this);
                    hideEmojiDialog();
                } else {
                    fam_image.setImageResource(R.drawable.fab_plus);
                }
            }
        });
        fam.setClosedOnTouchOutside(true);

        fab_plus_subject = (FloatingActionButton) findViewById(R.id.fab_plus_subject);
        fab_plus_camera = (FloatingActionButton) findViewById(R.id.fab_plus_camera);
        fab_plus_gallery = (FloatingActionButton) findViewById(R.id.fab_plus_gallery);
        fab_plus_file = (FloatingActionButton) findViewById(R.id.fab_plus_file);
        fab_plus_map = (FloatingActionButton) findViewById(R.id.fab_plus_map);
        fab_plus_vote = (FloatingActionButton) findViewById(R.id.fab_plus_vote);
        fab_plus_subject.setOnClickListener(fabClickListener);
        fab_plus_camera.setOnClickListener(fabClickListener);
        fab_plus_gallery.setOnClickListener(fabClickListener);
        fab_plus_file.setOnClickListener(fabClickListener);
        fab_plus_map.setOnClickListener(fabClickListener);
        fab_plus_vote.setOnClickListener(fabClickListener);


        mainroom_input_panel = (LinearLayout) findViewById(R.id.mainroom_input_panel);

        if (MyConfig.debug_float_button_mainroom) {
            ivPlus.setVisibility(View.GONE);
            fam.setVisibility(View.VISIBLE);
        } else {
            ivPlus.setVisibility(View.VISIBLE);
            fam.setVisibility(View.GONE);
        }

        subject_button_universal_layout = (LinearLayout) findViewById(R.id.universal_subject_main);
        subject_button_universal_cancel = (TextView) findViewById(R.id.subject_button_universal_cancel);
        subject_button_universal_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                subject_button_universal_layout.setVisibility(View.GONE);
            }
        });
        subject_button_universal_gray = findViewById(R.id.subject_button_universal_gray);
        subject_button_universal_gray.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                subject_button_universal_layout.setVisibility(View.GONE);
            }
        });

        universal_camera = (TextView) findViewById(R.id.universal_camera);
        universal_video = (TextView) findViewById(R.id.universal_video);

        universal_gallery = (TextView) findViewById(R.id.universal_gallery);
        universal_subject = (TextView) findViewById(R.id.universal_subject);
        universal_file = (TextView) findViewById(R.id.universal_file);
        universal_map = (TextView) findViewById(R.id.universal_map);
        universal_vote = (TextView) findViewById(R.id.universal_vote);
        universal_activity = (TextView) findViewById(R.id.universal_activity);
        universal_subject.setOnClickListener(universalButtonClickListener);
        universal_camera.setOnClickListener(universalButtonClickListener);
        universal_video.setOnClickListener(universalButtonClickListener);
        universal_gallery.setOnClickListener(universalButtonClickListener);
        universal_file.setOnClickListener(universalButtonClickListener);
        universal_map.setOnClickListener(universalButtonClickListener);
        universal_vote.setOnClickListener(universalButtonClickListener);
        universal_activity.setOnClickListener(universalButtonClickListener);
    }


    public void segmentButtonMoveUp() {
        ObjectAnimator objectAnimator =
                ObjectAnimator.ofFloat(room_list_segment,
                        "y", view_y_origin, view_y_origin - room_list_segment.getHeight()).setDuration(300);
        objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.setRepeatCount(0);
        objectAnimator.start();
    }

    public void segmentButtonMoveDown() {
        ObjectAnimator objectAnimator =
                ObjectAnimator.ofFloat(room_list_segment,
                        "y", view_y_origin - room_list_segment.getHeight(), view_y_origin).setDuration(300);
        objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.setRepeatCount(0);
        objectAnimator.start();
    }


    public void onEventMainThread(EventListviewScorll eventListviewScorll) {
        if (eventListviewScorll.move == 1) {
            segmentButtonMoveUp();
        } else if (eventListviewScorll.move == 2) {
            segmentButtonMoveDown();
        }

    }

    public void initGroup() {

        if (ab != null) {
            ab.setTitle(name);
//            ab.setSubtitle(desc);

            MyLog.d("", "actionbar set title");
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_INFO + groupId, MyConfig.usr_token);
                jsonGetGroupInfoRet = new Gson().fromJson(s, JsonGetGroupInfoRet.class);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (jsonGetGroupInfoRet != null) {
                            if (jsonGetGroupInfoRet.code == MyConfig.retSuccess()) {

                                /**
                                 * 根据群组信息进行配置
                                 *
                                 * 可能：
                                 *      我是站长
                                 *      我是群主
                                 *      我什么也不是
                                 */
                                if (null != MyConfig.jsonGetCurrentCompanyRet && MyConfig.usr_id == MyConfig.jsonGetCurrentCompanyRet.data.creator) {
                                    /**
                                     * 如果本人是集体创建者
                                     */

                                } else {
                                    if (jsonGetGroupInfoRet.data.broadcast) {
                                        ivPlus.setVisibility(View.GONE);
                                        fam.setVisibility(View.GONE);
                                        mainroom_input_panel.setVisibility(View.GONE);
                                    }
                                }

                                if (MyConfig.usr_id == jsonGetGroupInfoRet.data.creator) {
                                    /**
                                     * 如果本人是群主
                                     */

                                } else {

                                }

                                if(jsonGetGroupInfoRet.data.category == MyConfig.SERVER_ROOM_CATEGORY_DIRECT){
                                    room_type = MyConfig.LOCAL_ROOM_CATEGORY_DIRECT;

                                    updatePagerMenu(room_type);

                                }
                            }
                        }
                    }
                });
            }
        }).start();

        imsMainRoomRoot = new IMsg(
                MyConfig.USER_INDEX_ADMIN,
                "admin",
                null,

                Calendar.getInstance().getTime(),

                MyConfig.LEVEL_MSG_0,
                null,
                MyConfig.MSG_CATEGORY_SUBJECT,

                groupId,
                -1,
                MyConfig.formClientCheckId(groupId),

                "room id:" + groupId,
                "this is root msg"
        );

        MyConfig.mainRoomRootNode = imsMainRoomRoot;
        la_msg = new ListAdapterMsg(ActivityMainGroup.this, MyConfig.LEVEL_ROOM_MAIN, imsMainRoomRoot.ll_sub_msg, onLinkClickedListener, onImageClickedListener);
//        la_msg.downloadClickedListener = MyConfig.onDownloadClickedListener;
//        la_msg.fileClickedListener = MyConfig.onFileClickedListener;
        la_msg.soundClickedListener = onSoundClickedListener;
        la_msg.downloadClickedListener = new ListAdapterMsg.OnDownloadClickedListener() {
            @Override
            public void onDownloadClicked(IMsg imsg) {
                new MyConfig.DownloadFileFromUrl(new MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo(ActivityMainGroup.class.getName(), MyConfig.ATTACHMENT_ACTION_DO_NOTHING))
                        .execute(
                                MyConfig.getApiDomain_NoSlash_GetResources() + imsg.filePath_inServer,//在服务器的地址
                                imsg.filePath_inLocal);//本地地址
            }
        };

        la_msg.fileClickedListener = new ListAdapterMsg.OnFileClickedListener() {
            @Override
            public void onFileClicked(final Context context, final IMsg imsg) {

                new MyConfig.DownloadFileFromUrl(new MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo(ActivityMainGroup.class.getName(), MyConfig.ATTACHMENT_ACTION_DL_OPEN)).execute(
                        MyConfig.getApiDomain_NoSlash_GetResources() + imsg.filePath_inServer,//在服务器的地址
                        imsg.filePath_inLocal);//本地地址
            }
        };

        la_msg.captureLongClickedListener = new MyConfig.ClassCaptureLongClick(
                ActivityMainGroup.this,
                imsMainRoomRoot.ll_sub_msg,
                imsMainRoomRoot.ll_sub_msg_with_image,
                la_msg,
                new MyConfig.ClassCaptureLongClick.AvatarLongPressed() {
                    @Override
                    public void avatarLongPressed(IMsg iMsg) {

                        targets.add(iMsg.user_id);
                        StringBuilder sb = new StringBuilder(et.getText());
                        sb.append("@"+iMsg.user_name);
                        et.setText(sb.toString());
                        et.setSelection(sb.length());

                    }
                });

//        la_msg.captureLongClickedListener = new ListAdapterMsg.OnCaptureLongClickedListener() {
//
//            @Override
//            public boolean onLongClicked(final int position, final IMsg iMsg, MotionEvent event) {
//
//                boolean isLongPressed = MyConfig.captureLongClick(event);
//
//                if(isLongPressed){
//
//                    new DialogFragment() {
//
//                        @Override
//                        public Dialog onCreateDialog(Bundle savedInstanceState) {
//                            AlertDialog.Builder builder = new AlertDialog.Builder(
//                                    getActivity());
//
//                            final String s = MyConfig.getTextFromIMsg(iMsg);
//
//                            if(s != null) {
//                                builder.setTitle(getString(R.string.please_select))
//                                        .setItems(R.array.single_msg_operation, new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int which) {
//                                                // The 'which' argument contains the index position
//                                                // of the selected item
//                                                switch (which) {
//                                                    case 0:
//                                                        MyConfig.copyTextToClipBoard(ActivityMainGroup.this, s);
//                                                        return;
//                                                    case 1:
//                                                        MyConfig.deleteIMsg(position, iMsg, imsMainRoomRoot.ll_sub_msg, imsMainRoomRoot.ll_sub_msg_with_image, la_msg);
//                                                        return;
//                                                    default:
//                                                        return;
//                                                }
//                                            }
//                                        })
//                                        .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//
//                                            }
//                                        });
//                            }else{
//                                builder.setTitle(getString(R.string.prompt_delete_msg))
//                                        .setPositiveButton(getString(R.string.prompt_confirm), new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialogInterface, int i) {
//                                                MyConfig.deleteIMsg(position, iMsg, imsMainRoomRoot.ll_sub_msg, imsMainRoomRoot.ll_sub_msg_with_image, la_msg);
//                                            }
//                                        })
//                                        .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//
//                                            }
//                                        });
//                            }
//                            return builder.create();
//                        }
//                    }.show(getFragmentManager(), "");
//
//
//                }
//
//                return isLongPressed;
//
//            }
//        };

        lv_msg.setAdapter(la_msg);
        imsMainRoomRoot.la_msg = la_msg;
        setUnRead();
        initRoomMsgList();


////        getGroupMsg();
//        new AsyncTaskGetMoreMsg(new AfterGetMoreMsg() {
//            @Override
//            public void afterGetMoreMsg(int newItemCount) {
////                lv_msg.onRefreshComplete();
//
//                if (newItemCount > 0) {
//
//                    //更新msg列表
////                    MyConfig.buildTimeShowString(imsMainRoomRoot.ll_sub_msg);
//
//                    la_msg.notifyDataSetChanged();
//
////                    lv_msg.getRefreshableView().setSelectionFromTop(newItemCount + 1, 0);
//                }
//            }
//        }).execute();

    }


    private void setUnRead() {
        if (MyConfig.jsonUserGroupsRet != null && MyConfig.jsonUserGroupsRet.code == MyConfig.retSuccess()) {
            for (JsonUserGroupsRet.UserGroupsRet urr : MyConfig.jsonUserGroupsRet.data) {
                /**
                 * todo
                 *  这里崩过，空指针异常。
                 *  如果jsonUserGroupsRet不为空，但是data还可能为空？
                 */
                if (urr.id == groupId) {
                    if (urr.unreadCount > 0) {
                        mUnread = urr.unreadCount;
//                        main_goto_unread.setText("您有未读信息" + urr.unreadCount + "条");
                        if (mUnread > MyConfig.UNREAD_MAX) {
                            main_goto_unread.setText(getResources().getString(R.string.unread_count) + MyConfig.UNREAD_MAX + "+");
                            mUnread = MyConfig.UNREAD_MAX;
                        } else {
                            main_goto_unread.setText(getResources().getString(R.string.unread_count) + urr.unreadCount);
                        }
                        main_goto_unread.setVisibility(View.VISIBLE);
                        main_goto_unread.setOnClickListener(this);
                    }
                }
            }
        }
    }


    public View getViewByPosition(int pos, ListView listView) {
        /**
         * 来自http://stackoverflow.com/questions/24811536/android-listview-get-item-view-by-position
         * 未仔细考察
         */
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            MyLog.d("", "getViewByPosition 1: " + pos + " " + firstListItemPosition + " " + lastListItemPosition);
            /**
             * 说明此position不在listview的当前view里面
             * 此刻返回的view不管用
             */
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            MyLog.d("", "getViewByPosition 2: " + pos + " " + firstListItemPosition + " " + lastListItemPosition + " " + childIndex);
            /**
             * 此position在listview的当前view里面
             * 此刻返回的view可以被操作
             */
            return listView.getChildAt(childIndex);
        }
    }


    public void onEventMainThread(EventMainroomGotoUnread eventMainroomGotoUnread) {

        if (imsMainRoomRoot.ll_sub_msg.size() > mUnread) {
            MyLog.d("", "goto unread: imsMainRoomRoot.ll_sub_msg.size() > mUnread");
            lv_msg.getRefreshableView().setSelectionFromTop(imsMainRoomRoot.ll_sub_msg.size() - mUnread + 1, 0);

            new Thread(new Runnable() {

                /**
                 * todo
                 *  前面对listview做了setSelectionFromTop操作
                 *  后续获取某个子view并企图对其进行闪动
                 *  怀疑这个setSelectionFromTop操作是异步的，且不知如何追踪操作的结束
                 *  所以后续获取子view时该子view可能还没来得及换入进listview，也即该子view获取不到
                 *  这导致闪动操作的对象错误
                 *
                 *  采取笨办法，延迟半秒钟，等待前面的setSelectionFromTop操作完毕
                 *  由于人眼有一个反应时间，所以有这个延迟还正好
                 */

                @Override
                public void run() {
                    try {
                        /**
                         * todo
                         *  Thread或其runnable怎么延迟执行？
                         *  待查
                         */
                        Thread.sleep(500);
                    } catch (Exception e) {

                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            MyLog.d("", "goto unread: ObjectAnimator");

                            /**
                             * todo
                             *  用闪动来标示第一条未读消息
                             */
                            ObjectAnimator objectAnimator =
                                    ObjectAnimator.ofFloat(getViewByPosition(imsMainRoomRoot.ll_sub_msg.size() - mUnread + 1, lv_msg.getRefreshableView()),
                                            "alpha", 1, 0, 1).setDuration(MyConfig.ANIM_MAINROOM_UNREAD_FLASH_DURATION);
                            objectAnimator.setInterpolator(new LinearInterpolator());
                            objectAnimator.setRepeatCount(MyConfig.ANIM_MAINROOM_UNREAD_FLASH_COUNT);
                            objectAnimator.start();
                        }
                    });

                }
            }).start();


        } else {
            new AsyncTaskGetMoreMsg(new AfterGetMoreMsg() {
                @Override
                public void afterGetMoreMsg(int newItemCount) {
                    EventBus.getDefault().post(new EventMainroomGotoUnread());
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.switch_input:
                // 虚拟键盘隐藏 判断view是否为空
                View view = getWindow().peekDecorView();
                if (view != null) {
                    //隐藏虚拟键盘
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(ActivityMainGroup.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                MyLog.d("", "recording in mainroom: ButtonSwitchInput onClick");
                input_mode_record = !input_mode_record;
                if (input_mode_record) {
                    /**当切换成录音模式时隐藏表情*/
                    hideEmojiDialog();
                    iv_add_emoji.setVisibility(View.GONE);

                    ButtonRecord.setVisibility(View.VISIBLE);
                    ButtonSwitchInput.setBackgroundResource(R.drawable.button_keyboard);
                    et.setVisibility(View.GONE);
                } else {
                    iv_add_emoji.setVisibility(View.VISIBLE);
                    ButtonRecord.setVisibility(View.GONE);
                    et.setVisibility(View.VISIBLE);
                    ButtonSwitchInput.setBackgroundResource(R.drawable.button_record);
                }
                break;
            case R.id.mainroom_send_msg:
                getMsg2Send();
                break;
            case R.id.main_goto_unread:
                main_goto_unread.setVisibility(View.INVISIBLE);
                EventBus.getDefault().post(new EventMainroomGotoUnread());
                break;
            case R.id.mainroom_input_edit:
                if(emojiIsVisible){
                    hideEmojiDialog();
                }

                break;
            case R.id.iv_add_emoji:
                if(emojiIsVisible){
                    hideEmojiDialog();
                    EmojiUtils.show_keyboard_must_call_from_activity(this);
                }else{
                    showEmojiDialog();
                }
                break;
            case R.id.mainroom_plus:
               showPlusDialog();
                break;
        }
    }

    /**
     * 隐藏表情选择框
     */
    private void hideEmojiDialog() {
        fl_emoji_fragment.setVisibility(View.GONE);
        iv_add_emoji.setImageResource(R.drawable.emoji_button_3x);
        emojiIsVisible = false;
    }

    /**
     * 显示表情选择框
     */
    private void showEmojiDialog() {
        /**EditText没有获取焦点时点击事件不执行*/
       if(!et.hasFocus()){
           et.setFocusable(true);
           et.requestFocus();
       }
        EmojiUtils.hide_keyboard_must_call_from_activity(this);
        SystemClock.sleep(300);
        fl_emoji_fragment.setVisibility(View.VISIBLE);
        iv_add_emoji.setImageResource(R.drawable.button_keyboard);
        emojiIsVisible = true;
    }


    public interface AfterGetMoreMsg {
        public void afterGetMoreMsg(int newItemCount);
    }

    private class AsyncTaskGetMoreMsg extends AsyncTask<Void, Void, String> {
        AfterGetMoreMsg afterGetMoreMsg;
        int newItemsCount;

        public AsyncTaskGetMoreMsg(AfterGetMoreMsg afterGetMoreMsg) {
            this.afterGetMoreMsg = afterGetMoreMsg;
        }

        protected String doInBackground(Void... v) {

            msgPageCount++;//注意，这个变量指代表http获取次数。第一次进聊天室为1，每次下拉刷新则加1

            String param;
            if (msgPageCount > 1 && imsMainRoomRoot.ll_sub_msg.size() > 0) {
                param = "/?cursor=" + imsMainRoomRoot.ll_sub_msg.getFirst().server_createAt + "&page=1";
            } else {
                param = "/?page=" + msgPageCount;
            }
            MyLog.d("", "httptools: url " + MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GET + groupId + MyConfig.API2_GET_2 + param);
            String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GET + groupId + MyConfig.API2_GET_2 + param, MyConfig.usr_token);
            MyLog.d("", "http mainroom 1: " + groupId + " msg: " + s);

            return s;

        }

        protected void onPostExecute(final String s) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Gson gson = new Gson();
                    try {
                        if (s == null) {
//                    newItemsCount = -1;
                            return;
                        }
                        JSONObject jsonRoot = new JSONObject(s);
                        if (jsonRoot == null) {
//                    newItemsCount = -1;
                            return;
                        }

                        int code = jsonRoot.getInt("code");
                        if (code != MyConfig.retSuccess()) {
//                    newItemsCount = -1;
                            return;
                        }

                        JSONArray jsonData = jsonRoot.getJSONArray("data");
                        newItemsCount = jsonData.length();
                        for (int i = 0; i < jsonData.length(); i++) {
                            JSONObject jsonObject = jsonData.getJSONObject(i);
                            int type = jsonObject.getInt("type");

                            if (type == MyConfig.TYPE_SOUND) {
                                MyLog.d("", "sound ");
                                jsonSoundMsg = gson.fromJson(jsonObject.toString(), JsonSoundMsg.class);
                                addSoundMsg2Local(jsonSoundMsg);
                            } else if (type == MyConfig.TYPE_MSG) {
                                // message
                                getMsgRet = gson.fromJson(jsonObject.toString(), JsonGetMsgRet.GetMsgRet.class);
                                addGetMsgRet2Local(getMsgRet);
                            } else if (type == MyConfig.TYPE_TOPIC) {
                                // subject (topic)
                                getMsgTopicRet = gson.fromJson(jsonObject.toString(), JsonGetMsgTopicRet.GetMsgTopicRet.class);
                                addGetMsgTopicRet2Local(getMsgTopicRet);
                            } else if (type == MyConfig.TYPE_TOPIC_COMMANT) {
                                // subject (topic) comment
                                getMsgTopicCommentRet = gson.fromJson(jsonObject.toString(), JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet.class);

                                addGetMsgTopicCommentRet2Local(getMsgTopicCommentRet);
                            } else if (type == MyConfig.TYPE_CREATE) {

                                // event，格式与msg相近，用msg格式来获取
                                getEventRet = gson.fromJson(jsonObject.toString(), JsonGetEventRet.GetEventRet.class);
//                        getMsgRet.info.message = "创建了聊天室";
                                getEventRet.info.message = getString(R.string.group_event_create);
                                addGetEventRet2Local(getEventRet);
                            } else if (type == MyConfig.TYPE_JOIN) {

                                // event，格式与msg相近，用msg格式来获取
                                getEventRet = gson.fromJson(jsonObject.toString(), JsonGetEventRet.GetEventRet.class);
//                        getMsgRet.info.message = "加入了聊天室";
                                getEventRet.info.message = getString(R.string.group_event_join);
                                addGetEventRet2Local(getEventRet);
                            } else if (type == MyConfig.TYPE_LEAVE) {

                                // event，格式与msg相近，用msg格式来获取
                                getEventRet = gson.fromJson(jsonObject.toString(), JsonGetEventRet.GetEventRet.class);
//                        getMsgRet.info.message = "离开了聊天室";
                                getEventRet.info.message = getString(R.string.group_event_leave);
                                addGetEventRet2Local(getEventRet);
                            } else {
                                MyLog.d("", "MainGroup get unkown type msg: " + type);
                            }
                        }
                    } catch (JSONException e) {
                        MyLog.d("", "getGroupMsg JSONException: " + e.toString());
                        newItemsCount = -1;
                    } catch (JsonSyntaxException e) {
                        MyLog.d("", "getGroupMsg JsonSyntaxException: " + e.toString());
                        newItemsCount = -1;
                    } catch (Exception e) {
                        MyLog.d("", "AsyncTaskGetMoreMsg: e:" + e.toString() + " s:" + s);
                    }

                    MyConfig.buildTimeShowString(imsMainRoomRoot.ll_sub_msg);
                    afterGetMoreMsg.afterGetMoreMsg(newItemsCount);

                }
            });
        }
    }


    private void addSoundMsg2Local(JsonSoundMsg jsonSoundMsg) {
        IMsg msg = MyConfig.formIMsgFromSoundMsg(jsonSoundMsg, imsMainRoomRoot, MyConfig.MSG_CATEGORY_NORMAL);

        /**
         * 注意，插入到头部是因为后台返回的msg次序
         */
        imsMainRoomRoot.ll_sub_msg.addFirst(msg);
    }

    private void addGetEventRet2Local(JsonGetEventRet.GetEventRet getEventRet) {
        IMsg msg = MyConfig.formIMsgFromGetEventRet(getEventRet, imsMainRoomRoot, MyConfig.MSG_CATEGORY_EVENT);

        /**
         * 注意，插入到头部是因为后台返回的msg次序
         */
        imsMainRoomRoot.ll_sub_msg.addFirst(msg);
    }

    private void addGetMsgRet2Local(JsonGetMsgRet.GetMsgRet getMsgRet) {
        IMsg msg = MyConfig.formIMsgFromGetMsgRet(getMsgRet, imsMainRoomRoot, MyConfig.MSG_CATEGORY_NORMAL);

        /**
         * 注意，插入到头部是因为后台返回的msg次序
         */
        imsMainRoomRoot.ll_sub_msg.addFirst(msg);
    }


    private void addGetMsgTopicRet2Local(JsonGetMsgTopicRet.GetMsgTopicRet getMsgTopicRet) {

        IMsg msg = MyConfig.formIMsgFromGetMsgTopicRet(getMsgTopicRet, imsMainRoomRoot, MyConfig.MSG_CATEGORY_SUBJECT);

        imsMainRoomRoot.ll_sub_msg.addFirst(msg);
        MyConfig.addImageIMsg2ImageList(imsMainRoomRoot, msg, true);
        preLoadAttached(msg);
    }

    private void addGetMsgTopicCommentRet2Local(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet getMsgTopicCommentRet) {
        IMsg msg = MyConfig.formIMsgFromGetMsgTopicCommentRet(getMsgTopicCommentRet, imsMainRoomRoot, MyConfig.MSG_CATEGORY_SUBJECT_COMMENT);

        imsMainRoomRoot.ll_sub_msg.addFirst(msg);
        MyConfig.addImageIMsg2ImageList(imsMainRoomRoot, msg, true);
    }


    ListAdapterMsg.OnLinkClickedListener onLinkClickedListener = new ListAdapterMsg.OnLinkClickedListener() {

        @Override
        public void onLinkClicked(IMsg imsSub) {
//            initGroup(imsNew);
            MyConfig.subRoomRootNode = imsSub;
            Intent intent = new Intent(ActivityMainGroup.this, ActivitySubGroup.class);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_TOPICID, imsSub.topicId);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_TITLE, imsSub.title);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, imsSub.body);
//            intent.putExtra("type", imsSub.type);
//            intent.putExtra("subType", imsSub.subType);
            startActivity(intent);

        }
    };

    ListAdapterMsg.OnImageClickedListener onImageClickedListener = new ListAdapterMsg.OnImageClickedListener() {

        @Override
        public void onImageClicked(IMsg ims) {

            if(ims.type == MyConfig.TYPE_TOPIC && ims.subType == MyConfig.SUBTYPE_VIDEO){

                //刷新一下，看是否下载完毕
                ims.fileDownloadProgress = MyConfig.getFileDownloadProgress(ims.filePath_inLocal, ims.fileSize);

                MyLog.d("","video download: progress " + ims.filePath_inLocal + " " + ims.fileDownloadProgress);

                if(ims.fileDownloadProgress == 100){


                    Intent intent = new Intent(ActivityMainGroup.this, ActivityVideoPlayer.class);
                    intent.putExtra("mode", 1);
                    intent.putExtra("path", ims.filePath_inLocal);
                    intent.putExtra("recorder_os", ims.os);

                    startActivity(intent);


                }else {

                    Intent intent = new Intent(ActivityMainGroup.this, ActivityVideoPlayer.class);
                    intent.putExtra("mode", 0);
                    intent.putExtra("path", MyConfig.getApiDomain_NoSlash_GetResources() + ims.filePath_inServer);
                    intent.putExtra("recorder_os", ims.os);

                    startActivity(intent);
                }

            }else {

//            MyConfig.iMsgClicked = ims;
//            MyConfig.iMsgGalleryRoot = MyConfig.mainRoomRootNode;
                MyConfig.genGalleryFullSizeUrl(MyConfig.mainRoomRootNode, ims);

                Intent intent = new Intent(ActivityMainGroup.this, ActivityGallery.class);
                intent.putExtra("mode", MyConfig.GALLERY_MODE_PHOTOS_DOWNLOADED);
                intent.putExtra("from", ActivityMainGroup.class.getName());

                startActivity(intent);
            }
        }
    };

    ListAdapterMsg.OnSoundClickedListener onSoundClickedListener = new ListAdapterMsg.OnSoundClickedListener() {
        @Override
        public void onSoundClicked(IMsg imsg) {

            IMsg currentPlayedImag = null;

            if (player != null) {

                /**
                 * 如果已经存在一个player
                 *  就是当前点击的imsg
                 *      正在准备或正在播放
                 *          停止
                 *      已经播放完毕
                 *          再次播放
                 *  不是当前的imsg
                 *      清除player
                 *      新建一个当前imsg的player并播放
                 */

                currentPlayedImag = player.getIMsg();

                if (currentPlayedImag == imsg) {

                    switch (currentPlayedImag.audioPlaying) {
                        case MyConfig.MEDIAPLAYER_IS_PLAYING:
                            player.pause();
                            break;
                        case MyConfig.MEDIAPLAYER_IS_READY:
                            player.replay();
                            break;
                        case MyConfig.MEDIAPLAYER_NOT_INITIAL:
                            player.prepareAndPlay();
                            break;
                        case MyConfig.MEDIAPLAYER_IS_PREPARING:
                            player.reset();
                            break;
                    }

                } else {
                    player.release();
                    player = playSoundIMsg(imsg);
                }

            } else {
                player = playSoundIMsg(imsg);
            }

        }
    };

    public Player playSoundIMsg(IMsg imsg) {
        Player player = new Player(imsg, new Player.PlayingNotify() {
            @Override
            public void statusNotify() {

                lv_msg.getRefreshableView().setTranscriptMode(ListView.TRANSCRIPT_MODE_DISABLED);
                la_msg.notifyDataSetChanged();

            }
        });
        player.prepareAndPlay();

        return player;
    }


    public void onEventMainThread(EventIMsgSoundPlayEnd eventIMsgSoundPlayEnd) {

        IMsg lastPlayed = eventIMsgSoundPlayEnd.iMsg;

        /**
         * 如果下一条是语音、未读，那么就播放，否则停止
         */

        int next_index = MyConfig.searchNextMsgInList(imsMainRoomRoot.ll_sub_msg, lastPlayed);
        if (next_index != -1) {
            IMsg next_imsg = imsMainRoomRoot.ll_sub_msg.get(next_index);

            if (MyConfig.isSoundIMsg(next_imsg)) {

                if (next_imsg.readed) {

                } else {
                    onSoundClickedListener.onSoundClicked(next_imsg);
                }

            }
        }

    }

    private void getSoundMsg2Send(int duration) {
        IMsg iMsg = formSoundMsg();
        iMsg.duration = duration;
        sendSoundMsg(iMsg);
    }


    private void sendSoundMsg(IMsg iMsg) {

        if (!MyConfig.debug_msg_no_local_first) {
            sendSoundMsg2Local(iMsg);
        }
        SendSoundMsg2Server(iMsg);

    }


    private void sendSoundMsg2Local(IMsg iMsg) {
        iMsg.setLocalSendTag();
        socketAddMsg2Local(iMsg);
    }

    private void SendSoundMsg2Server(final IMsg imsg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
//                Gson gson = new Gson();
//
//                JsonSendMsg jsonSendMsg = new JsonSendMsg();
//                jsonSendMsg.accessToken = MyConfig.usr_token;
//                jsonSendMsg.groupId = groupId;
//                jsonSendMsg.clientId = imsg.clientId;
//
//                String jsonStr = gson.toJson(jsonSendMsg, JsonSendMsg.class);

                final JsonSendSoundMsgRet jsonSendSoundMsgRet = HttpTools.okhttpUploadSoundMsg(imsg, null);

                if (jsonSendSoundMsgRet != null && jsonSendSoundMsgRet.code == MyConfig.retSuccess()) {
                    MyLog.d("", getResources().getString(R.string.sound_success));
                }

            }
        }).start();
    }

    private IMsg formSoundMsg() {

        File f = new File(MyConfig.appCacheDirPath, MyConfig.tempSoundFile);
        if (f.exists() && f.length() > 0) {

            IMsg msg = new IMsg(
                    MyConfig.usr_id,
                    MyConfig.usr_nickname,
                    MyConfig.usr_avatar,

                    Calendar.getInstance().getTime(),

                    MyConfig.LEVEL_MSG_1,
                    imsMainRoomRoot,
                    MyConfig.MSG_CATEGORY_NORMAL,

                    groupId,
                    -1,
                    MyConfig.formClientCheckId(groupId),

                    null,
                    null
            );
            msg.attached = f;

            return msg;
        } else {
            return null;
        }

    }

    private IMsg formMsg(String s) {

        IMsg msg = new IMsg(
                MyConfig.usr_id,
                MyConfig.usr_nickname,
                MyConfig.usr_avatar,

                Calendar.getInstance().getTime(),

                MyConfig.LEVEL_MSG_1,
                imsMainRoomRoot,
                MyConfig.MSG_CATEGORY_NORMAL,

                groupId,
                -1,
                MyConfig.formClientCheckId(groupId),

                null,
                s
        );

        return msg;
    }

    private void getMsg2Send() {
        String str = MyConfig.trimAndTakeAwayStringFromEditText(et);

        if (str != null) {
            /**把表情的名字替换成和iOS协商好的字段*/
            str = EmojiUtils.replaceNameByField(this,str);
            IMsg iMsg = formMsg(str);

            sendMsg(iMsg);
        }

    }

    private void sendMsg(IMsg iMsg) {

        if (!MyConfig.debug_msg_no_local_first) {
            sendMsg2Local(iMsg);
        }
        SendMsg2Server(iMsg);

    }

    private void sendMsg2Local(IMsg iMsg) {
        iMsg.setLocalSendTag();
        socketAddMsg2Local(iMsg);
    }

    private void SendMsg2Server(final IMsg imsg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSendMsg jsonSendMsg = new JsonSendMsg();
                jsonSendMsg.accessToken = MyConfig.usr_token;
                jsonSendMsg.message = imsg.body;
                jsonSendMsg.groupId = groupId;
                jsonSendMsg.clientId = imsg.clientId;

                jsonSendMsg.targets = MyConfig.deleteRepeatFromCollection(targets);
                // targets.clear();

                String jsonStr = gson.toJson(jsonSendMsg, JsonSendMsg.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SEND, jsonStr);

                final JsonSendMsgRet jsonSendMsgRet = gson.fromJson(s, JsonSendMsgRet.class);


                MyLog.d("", "HttpTools: " + s);
                if (s != null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            targets.clear();
//                            MyLog.d("", "Snackbar is called");
//                            Snackbar.make(lv_msg, "You have sent a msg~", Snackbar.LENGTH_LONG).show();

                        }
                    });
                }
                MyLog.d("", "apitest: send msg ret " + s);

            }
        }).start();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        MyLog.d("","httptools video onActivityResult " + requestCode +" "+resultCode);


        /**
         * 第六类：创建报名
         */
        if (requestCode == MyConfig.REQUEST_CREATE_ACTIVITY && resultCode == RESULT_OK) {

            IMsg ims = formSubjectFromIntent(data);
            ims.title = MyConfig.tempActivity.title;
            ims.type = MyConfig.TYPE_TOPIC;
            ims.subType = MyConfig.SUBTYPE_ACTIVITY;
            ims.mActivity = MyConfig.tempActivity;

            sendSubjectActivity(ims);
        }




        /**
         * @某人回来
         */
        if (requestCode == MyConfig.PEG_SOME_BODY_REQUSET_CODE) {
            if (resultCode == RESULT_OK) {
                MyLog.d("kwwl", MyConfig.PEG_SOME_BODY_USER_ID + "被@了");
                targets.add(MyConfig.PEG_SOME_BODY_USER_ID);
                String s = et.getText().toString() + MyConfig.PEG_SOME_BODY_USER_NICK_NAME + " ";
                et.setText(s);
                et.setSelection(s.length());
            }

            MyConfig.show_keyboard_must_call_from_activity(ActivityMainGroup.this);
        }


        // ========= 第一部分是各种主题创建的返回 ================

        /**
         * 录制视频后返回，再创建话题
         */
        if (requestCode == MyConfig.REQUEST_VIDEO && resultCode == RESULT_OK) {
            MyLog.d("","httptools video requestCode " + requestCode);
            openSubjectInputWithAttach(MyConfig.REQUEST_SUBJECT_WITH_VIDEO, null, null, MyConfig.current_video_path, MyConfig.SHARE_IN_CATEGORY_NOT_SHARE_IN);

        }

        /**
         * 第四类：创建定位地图，再创建话题
         */
        if (requestCode == MyConfig.REQUEST_MAP && resultCode == RESULT_OK) {
            MyLog.d("","httptools video map requestCode " + requestCode);

            openSubjectInputWithAttach(MyConfig.REQUEST_SUBJECT_WITH_MAP, MyConfig.tempMapLocation.address, null, MyConfig.tempMapLocation.screenShotPath, MyConfig.SHARE_IN_CATEGORY_NOT_SHARE_IN);

        }
        /**
         * 带定位地图的话题创建
         */
        if (requestCode == MyConfig.REQUEST_SUBJECT_WITH_MAP && resultCode == RESULT_OK) {

            IMsg ims = formSubjectFromIntent(data);
            ims.type = MyConfig.TYPE_TOPIC;
            ims.subType = MyConfig.SUBTYPE_MAP;
            ims.mapLocation = MyConfig.tempMapLocation;

            sendSubjectMap(ims);

            //debug
//            socketAddSubject2Local(ims);
        }

        /**
         * 第三类：创建投票，再创建话题
         */
        if (requestCode == MyConfig.REQUEST_CREATE_VOTE && resultCode == RESULT_OK) {

            IMsg ims = formSubjectFromIntent(data);
            ims.title = MyConfig.tempVote.title;
            ims.type = MyConfig.TYPE_TOPIC;
            ims.subType = MyConfig.SUBTYPE_VOTE;
            ims.vote = MyConfig.tempVote;

            //投票时可能附带了图片，也可能没有
            String attachedPhoto = data.getStringExtra(MyConfig.INTENT_KEY_CREATE_VOTE_ATTACHED);
            if (attachedPhoto != null) {
                ims.attached = new File(attachedPhoto);
            }

            sendSubjectVote(ims);
        }

//        /**
//         * 带投票的话题创建
//         */
//        if (requestCode == MyConfig.REQUEST_SUBJECT_WITH_VOTE && resultCode == RESULT_OK) {
//
//            IMsg ims = formSubjectFromIntent(data);
//            ims.type = MyConfig.TYPE_TOPIC;
//            ims.subType = MyConfig.SUBTYPE_VOTE;
//            ims.vote = MyConfig.tempVote;
//
//            sendSubjectVote(ims);
//
//        }

        /**
         * 第二类：先获取附件，包括拍摄／图片／文件，再创建话题
         */
        if (requestCode == MyConfig.REQUEST_FILE && resultCode == RESULT_OK) {
            MyLog.d("", "fixbug---get file crash: after get file");
            Uri fileUri = data.getData();
            String realPath = MyConfig.getUriPath(this, fileUri);
//            MyConfig.sendSysMsg(imsMainRoomRoot, MyConfig.getUriPath(this, fileUri));
            tempAttach = new File(realPath);
            MyLog.d("", "file upload 2");
            MyLog.d("", "fixbug---get file crash: file path " + realPath);
            openSubjectInputWithAttach(MyConfig.REQUEST_SUBJECT_WITH_FILE, null, null, realPath, MyConfig.SHARE_IN_CATEGORY_NOT_SHARE_IN);
        }
        if (requestCode == MyConfig.REQUEST_GALLERY && resultCode == RESULT_OK) {

            Uri fileUri = data.getData();
            String realPath = MyConfig.getUriPath(this, fileUri);
            tempAttach = new File(realPath);
            openSubjectInputWithAttach(MyConfig.REQUEST_SUBJECT_WITH_IMAGE, null, null, realPath, MyConfig.SHARE_IN_CATEGORY_NOT_SHARE_IN);

            if (MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {
                /**
                 * 去掉，聊天室上传图片时不需要裁剪
                 */
                Intent innerIntent = new Intent("com.android.camera.action.CROP");
                innerIntent.setDataAndType(data.getData(), MyConfig.MIME_IMAGE_STAR);
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true");// 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
                startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);
            }
        }

        if (requestCode == MyConfig.REQUEST_CAMERA && resultCode == RESULT_OK) {

            openSubjectInputWithAttach(MyConfig.REQUEST_SUBJECT_WITH_IMAGE, null, null, tempAttach.getPath(), MyConfig.SHARE_IN_CATEGORY_NOT_SHARE_IN);

        }


        /**
         * 带附件的话题创建，包括拍摄／图片／文件
         */
        if (requestCode == MyConfig.REQUEST_SUBJECT_WITH_VIDEO && resultCode == RESULT_OK) {

            MyLog.d("","httptools video attachedType " + MyConfig.current_video_path +" length " + new File(MyConfig.current_video_path).length());

            IMsg ims = formSubjectFromIntent(data);
            ims.attachFileBeforeSend2Server(new File(MyConfig.current_video_path), tempAttachMime);

            sendSubjectVideo(ims);

        }
        if (requestCode == MyConfig.REQUEST_SUBJECT_WITH_FILE && resultCode == RESULT_OK) {

            IMsg ims = formSubjectFromIntent(data);
            ims.attachFileBeforeSend2Server(tempAttach, tempAttachMime);

            sendSubjectFile(ims);

        }

        if (requestCode == MyConfig.REQUEST_SUBJECT_WITH_IMAGE && resultCode == RESULT_OK) {

            IMsg ims = formSubjectFromIntent(data);
            ims.attachFileBeforeSend2Server(tempAttach, tempAttachMime);

            /**
             * 这里不再直接显示，而是等待服务器下发
             */
//            imsMainRoomRoot.ll_sub_msg.addLast(ims);
//            la_msg.notifyDataSetChanged();

            sendSubjectImage(ims);

        }

        /**
         * 第一类：单纯的话题创建
         */
        if (requestCode == MyConfig.REQUEST_SUBJECT_ONLY && resultCode == RESULT_OK) {

            IMsg ims = formSubjectFromIntent(data);
            sendSubjectText(ims);

        }

    }

    private void sendSubjectText(IMsg ims) {

        if (!MyConfig.debug_msg_no_local_first) {
            sendSubjectText2Local(ims);
        }
        sendSubjectText2Server(ims);

    }

    private void sendSubjectText2Local(IMsg ims) {
        ims.setLocalSendTag();
        socketAddSubject2Local(ims);
    }

    private void sendSubjectText2Server(final IMsg ims) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSendTopic jsonSendTopic = new JsonSendTopic();
                jsonSendTopic.accessToken = MyConfig.usr_token;
                jsonSendTopic.title = ims.title;
                jsonSendTopic.desc = ims.body;
                jsonSendTopic.groupId = groupId;
                jsonSendTopic.clientId = ims.clientId;

                if (jsonSendTopic.title.startsWith("今日头条")) {

                    String[] splitBodys = jsonSendTopic.desc.split("\n");
                    if (splitBodys.length > 0 && splitBodys[1].startsWith("http")) {
                        jsonSendTopic.title = jsonSendTopic.title + splitBodys[1];
                        jsonSendTopic.desc = "";
                        jsonSendTopic.news = "news";
                    }

                }


                String jsonStr = gson.toJson(jsonSendTopic, JsonSendTopic.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SEND_TOPIC, jsonStr);
                MyLog.d("", "HttpTools send topic ret: " + s);

                final JsonSendTopicRet jsonSendTopicRet = gson.fromJson(s, JsonSendTopicRet.class);

                MyLog.d("", "apitest: send msg ret " + s);

            }
        }).start();
    }

    private void sendSubjectVote(final IMsg ims) {

        if (!MyConfig.debug_msg_no_local_first) {
            sendSubjectVote2Local(ims);
        }

        sendSubjectVote2Server(ims);

    }

    private void sendSubjectVote2Local(IMsg ims) {

        ims.setLocalSendTag();
        socketAddSubject2Local(ims);
    }

    private void sendSubjectVote2Server(IMsg ims) {
        if (ims.attached == null) {
            sendSubjectVoteBody(ims);
        } else {
            sendSubjectVoteWithImage(ims);
        }
    }


    public void sendSubjectVoteWithImage(final IMsg iMsg) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonUploadImageRet jsonUploadImageRet = HttpTools.okhttpUploadImage(iMsg.attached, "image/jpeg");

                if (jsonUploadImageRet != null && jsonUploadImageRet.code == MyConfig.retSuccess()) {
                    iMsg.vote.imageId = jsonUploadImageRet.data.id;

                    sendSubjectVoteBody(iMsg);

                } else {

                }

            }
        }).start();
    }


    private void sendSubjectVoteBody(final IMsg ims) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSendTopicVoteCreate jsonSendTopicVoteCreate = new JsonSendTopicVoteCreate();
                jsonSendTopicVoteCreate.accessToken = MyConfig.usr_token;
                jsonSendTopicVoteCreate.groupId = groupId;
                jsonSendTopicVoteCreate.title = ims.title;
                jsonSendTopicVoteCreate.single = ims.vote.single;
                jsonSendTopicVoteCreate.anonymous = ims.vote.anonymous;
                jsonSendTopicVoteCreate.options = ims.vote.options;
                jsonSendTopicVoteCreate.imageId = ims.vote.imageId;
                jsonSendTopicVoteCreate.clientId = ims.clientId;

                String jsonStr = gson.toJson(jsonSendTopicVoteCreate, JsonSendTopicVoteCreate.class);
                MyLog.d("", "http mainroom 2: " + jsonStr);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_VOTE, jsonStr);
                MyLog.d("", "http mainroom 3: " + s);

                final JsonSendTopicVoteCreateRet jsonSendTopicVoteCreateRet = gson.fromJson(s, JsonSendTopicVoteCreateRet.class);

                if (jsonSendTopicVoteCreateRet != null) {
                    if (jsonSendTopicVoteCreateRet.code == MyConfig.retSuccess()) {

                    } else {

                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MyConfig.MyToast(-1, MyConfig.activityMainGroup,
                                    jsonSendTopicVoteCreateRet.message);

                        }
                    });
                }

                MyLog.d("", "apitest: send msg ret " + s);

            }
        }).start();
    }

    /**
     * 提交报名主题
     * @param ims
     */
    private void sendSubjectActivity(final IMsg ims) {

        if (!MyConfig.debug_msg_no_local_first) {
            /**因为发送报名到本地与发送投票到本地调用的方法是相同的，所以这里也调用这个方法*/
            sendSubjectVote2Local(ims);
        }
        sendSubjectActivity2Server(ims);
    }

    /**
     * 提交报名子话题到服务器
     * @param ims
     */
    private void sendSubjectActivity2Server(final IMsg ims) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSendTopicActivityCreate jsonSendTopicActivityCreate = new JsonSendTopicActivityCreate();
                jsonSendTopicActivityCreate.accessToken = MyConfig.usr_token;
                jsonSendTopicActivityCreate.groupId = groupId;
                jsonSendTopicActivityCreate.clientId = ims.clientId;

                jsonSendTopicActivityCreate.title = ims.title;
                jsonSendTopicActivityCreate.desc = ims.mActivity.desc;

                jsonSendTopicActivityCreate.closeTime = ims.mActivity.closeTime;
                jsonSendTopicActivityCreate.startTime = ims.mActivity.startTime;
                jsonSendTopicActivityCreate.endTime = ims.mActivity.endTime;

                jsonSendTopicActivityCreate.isPublic = ims.mActivity.isPublic;
                jsonSendTopicActivityCreate.limit = ims.mActivity.limit;
                jsonSendTopicActivityCreate.location = ims.mActivity.location;
                jsonSendTopicActivityCreate.fee = ims.mActivity.cost;

                jsonSendTopicActivityCreate.longitude = ims.mActivity.longitude;
                jsonSendTopicActivityCreate.latitude = ims.mActivity.latitude;


                String jsonStr = gson.toJson(jsonSendTopicActivityCreate, JsonSendTopicActivityCreate.class);
                MyLog.d("", "http mainroom 2: " + jsonStr);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_ACTIVIY, jsonStr);
                MyLog.d("", "http mainroom 3: " + s);
                MyLog.d("kwwl", "创建报名提交数据========: " + jsonStr);
                MyLog.d("kwwl", "创建报名返回数据========: " + s);

                final JsonSendTopicActivityCreateRet jsonSendTopicActivityCreateRet = gson.fromJson(s, JsonSendTopicActivityCreateRet.class);
                if (jsonSendTopicActivityCreateRet != null) {
                    if (jsonSendTopicActivityCreateRet.code == MyConfig.retSuccess()) {

                    } else {

                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MyConfig.MyToast(-1, MyConfig.activityMainGroup,
                                    jsonSendTopicActivityCreateRet.message);

                        }
                    });
                }
            }
        }).start();
    }


    public void sendSubjectMap(IMsg iMsg) {
        if (!MyConfig.debug_msg_no_local_first) {
            sendSubjectMap2Local(iMsg);
        }

        sendSubjectMap2Server(iMsg);
    }

    private void sendSubjectMap2Local(IMsg ims) {
        ims.setLocalSendTag();
        socketAddSubject2Local(ims);
    }


    public void sendSubjectMap2Server(final IMsg iMsg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final JsonSendMapRet jsonSendMapRet = HttpTools.okhttpUploadSubjectMap(iMsg);

                if (jsonSendMapRet != null && jsonSendMapRet.code != MyConfig.retSuccess()) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MyConfig.MyToast(-1, MyConfig.activityMainGroup,
                                    jsonSendMapRet.message);
                        }
                    });

                }
            }
        }).start();
    }


    public void sendSubjectImage(final IMsg iMsg) {
        if (!MyConfig.debug_msg_no_local_first) {
            sendSubjectImage2Local(iMsg);
        }

        sendSubjectImage2Server(iMsg);
    }

    private void sendSubjectImage2Local(IMsg iMsg) {

        iMsg.setLocalSendTag();
        socketAddSubject2Local(iMsg);

    }

    public void sendSubjectImage2Server(final IMsg iMsg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final JsonSendImageRet jsonSendImageRet = HttpTools.okhttpUploadSubjectImage(iMsg, null);

                if (jsonSendImageRet != null && jsonSendImageRet.code != MyConfig.retSuccess()) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MyConfig.MyToast(-1, MyConfig.activityMainGroup,
                                    jsonSendImageRet.message);
                        }
                    });

                }
            }
        }).start();
    }

    public void sendSubjectVideo(final IMsg iMsg) {
        if (!MyConfig.debug_msg_no_local_first) {
            sendSubjectFile2Local(iMsg);
        }

        sendSubjectVideo2Server(iMsg);
    }

    public void sendSubjectFile(final IMsg iMsg) {
        if (!MyConfig.debug_msg_no_local_first) {
            sendSubjectFile2Local(iMsg);
        }

        sendSubjectFile2Server(iMsg);
    }

    private void sendSubjectFile2Local(IMsg iMsg) {

        iMsg.setLocalSendTag();
        socketAddSubject2Local(iMsg);

    }


    public void sendSubjectVideo2Server(final IMsg iMsg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                MyLog.d("", "share in process: file upload 3");
                final JsonSendVideoRet jsonSendVideoRet = HttpTools.okhttpUploadSubjectVideo(iMsg, "*/*");
                MyLog.d("", "share in process: file upload 4");
                if (jsonSendVideoRet != null && jsonSendVideoRet.code != MyConfig.retSuccess()) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MyConfig.MyToast(-1, MyConfig.activityMainGroup,
                                    jsonSendVideoRet.message);
                        }
                    });

                }
            }
        }).start();
    }

    public void sendSubjectFile2Server(final IMsg iMsg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                MyLog.d("", "share in process: file upload 3");
                final JsonSendFileRet jsonSendFileRet = HttpTools.okhttpUploadSubjectFile(iMsg, "*/*");
                MyLog.d("", "share in process: file upload 4");
                if (jsonSendFileRet != null && jsonSendFileRet.code != MyConfig.retSuccess()) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MyConfig.MyToast(-1, MyConfig.activityMainGroup,
                                    jsonSendFileRet.message);
                        }
                    });

                }
            }
        }).start();
    }

    public void onEventMainThread(EventScreenSwitch eventScreenSwitch) {

        if (eventScreenSwitch.isScreenOn) {
            initRoomMsgList();
        }
    }

    public void onEventMainThread(EventSingleMsgDelete eventSingleMsgDelete) {

        /**
         * 如果子聊天室删除了comment，主聊天室要更新
         */
        initRoomMsgList();
    }

    public void onEventMainThread(EventTopicDelete eventTopicDelete) {

        /**
         * 简单起见，topic被删除时(附属的comments自然被删除)，重置整个聊天室
         * 因为逐个清除该主题的msg比较费事，或许以后考虑
         */

        initRoomMsgList();
    }

    private void initRoomMsgList() {
        msgPageCount = 0;
        imsMainRoomRoot.ll_sub_msg.clear();
        imsMainRoomRoot.ll_sub_msg_with_image.clear();

        new AsyncTaskGetMoreMsg(new AfterGetMoreMsg() {
            @Override
            public void afterGetMoreMsg(int newItemCount) {
//                lv_msg.onRefreshComplete();

                if (newItemCount > 0) {

                    //更新msg列表
//                    MyConfig.buildTimeShowString(imsMainRoomRoot.ll_sub_msg);

                    la_msg.notifyDataSetChanged();

//                    lv_msg.getRefreshableView().setSelectionFromTop(newItemCount + 1, 0);
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    public void onEventMainThread(JsonSoundMsg jsonSoundMsg) {

        MyLog.d("", "socketio event GetMsgRet: notifyDataSetChanged" + jsonSoundMsg.groupId);
        if (groupId != jsonSoundMsg.groupId) {
            MyLog.d("", "socketio event : onEventMainThread(JsonGetMsgRet.GetMsgRet jsonMsgSocket) ret for wrong groupId" + jsonSoundMsg.groupId);
            return;
        }

        IMsg iMsgRet = MyConfig.formIMsgFromSoundMsg(jsonSoundMsg, imsMainRoomRoot, MyConfig.MSG_CATEGORY_NORMAL);

        MyConfig.setSoundIMsgReadedForSocketInput(iMsgRet);


        if (jsonSoundMsg.type != MyConfig.TYPE_KICK) {
            socketAddMsg2Local(iMsgRet);
        }

    }


    public void onEventMainThread(JsonGetEventRet.GetEventRet getEventRet) {

        MyLog.d("", "socketio event GetMsgRet: notifyDataSetChanged" + getEventRet.groupId);
        if (groupId != getEventRet.groupId) {
            MyLog.d("", "socketio event : onEventMainThread(JsonGetMsgRet.GetMsgRet jsonMsgSocket) ret for wrong groupId" + getEventRet.groupId);
            return;
        }

        IMsg iMsgRet = MyConfig.formIMsgFromGetEventRet(getEventRet, imsMainRoomRoot, MyConfig.MSG_CATEGORY_EVENT);

        if (getEventRet.type != MyConfig.TYPE_KICK) {
            socketAddMsg2Local(iMsgRet);
        }

    }

    public void onEventMainThread(JsonGetMsgRet.GetMsgRet json_socket_msg) {

        MyLog.d("", "socketio event GetMsgRet: notifyDataSetChanged" + json_socket_msg.groupId);
        if (groupId != json_socket_msg.groupId) {
            MyLog.d("", "socketio event : onEventMainThread(JsonGetMsgRet.GetMsgRet jsonMsgSocket) ret for wrong groupId" + json_socket_msg.groupId);
            return;
        }

        IMsg iMsgRet = MyConfig.formIMsgFromGetMsgRet(json_socket_msg, imsMainRoomRoot, MyConfig.MSG_CATEGORY_NORMAL);

        if (json_socket_msg.type != MyConfig.TYPE_KICK) {
            socketAddMsg2Local(iMsgRet);
        }

    }

//    public void onEventMainThread(EventKick e) {
//      MyConfig.MyToast(0,this,"");
//    }

    public void onEventMainThread(JsonGetMsgTopicRet.GetMsgTopicRet json_socket_topic) {

        /**
         * todo
         *  先合并http和socket的结构
         *  再合并这个到MyConfig里面对应的函数中
         */

        MyLog.d("", "socketio event GetMsgTopicRet: notifyDataSetChanged)" + json_socket_topic.groupId);
        if (groupId != json_socket_topic.groupId) {
            MyLog.d("", "socketio event : onEventMainThread(Json_Socket_Topic json_socket_topic) ret for wrong groupId" + json_socket_topic.groupId);
            return;
        }

        IMsg iMsgSubjectRet = MyConfig.formIMsgFromGetMsgTopicRet(json_socket_topic, imsMainRoomRoot, MyConfig.MSG_CATEGORY_SUBJECT);

        /**
         * 对vote close信息做特殊处理
         * 收到这个信息后，需要刷新之前的vote topic信息
         */
        if (iMsgSubjectRet.type == MyConfig.TYPE_TOPIC && iMsgSubjectRet.subType == MyConfig.SUBTYPE_VOTE_CLOSE) {

            int topicId = iMsgSubjectRet.topicId;
            for (IMsg iMsg : imsMainRoomRoot.ll_sub_msg) {
                if (iMsg.type == MyConfig.TYPE_TOPIC && iMsg.subType == MyConfig.SUBTYPE_VOTE && iMsg.topicId == topicId) {
                    iMsg.vote.closed = true;
                }
            }
        }

        socketAddSubject2Local(iMsgSubjectRet);

    }


    /**
     * 当子聊天室中报名人数发生改变时，让主聊天室中显示的报名人数也跟随变化
     * @param eventUpdateActivityNumInMaint
     */
    public void onEventMainThread(EventUpdateActivityNumInMain eventUpdateActivityNumInMaint) {
            for (IMsg iMsg : imsMainRoomRoot.ll_sub_msg) {
                if (iMsg.type == MyConfig.TYPE_TOPIC && iMsg.subType == MyConfig.SUBTYPE_ACTIVITY && iMsg.topicId == eventUpdateActivityNumInMaint.id) {
                    iMsg.mActivity.joinedNum = eventUpdateActivityNumInMaint.num;
                }
            }
        la_msg.notifyDataSetChanged();
    }
    public void onEventMainThread(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet json_socket_topicComment) {
        MyLog.d("", "socketio event GetMsgTopicCommentRet: notifyDataSetChanged " + json_socket_topicComment.groupId);
        if (groupId != json_socket_topicComment.groupId) {
            MyLog.d("", "socketio event : onEventMainThread(Json_Socket_TopicComment json_socket_topicComment) ret for wrong groupId" + json_socket_topicComment.groupId);
            return;
        }

        IMsg iMsgRet = MyConfig.formIMsgFromGetMsgTopicCommentRet(json_socket_topicComment, imsMainRoomRoot, MyConfig.MSG_CATEGORY_SUBJECT_COMMENT);

        MyConfig.setSoundIMsgReadedForSocketInput(iMsgRet);

        socketAddComment2Local(iMsgRet);
    }


    private void socketAddMsg2Local(final IMsg msg) {
        appendIMsg2List(msg);
    }

    private void socketAddSubject2Local(final IMsg imsgSubject) {
        appendIMsg2List(imsgSubject);
    }

    private void socketAddComment2Local(final IMsg imsgComment) {
        appendIMsg2List(imsgComment);
    }

    public void appendIMsg2List(final IMsg msg) {

        int index = MyConfig.searchRepeatMsgInList(imsMainRoomRoot.ll_sub_msg, msg, MyConfig.REPEAT_CHECK_COUNT_MAX);

        if (-1 == index) {
            imsMainRoomRoot.ll_sub_msg.addLast(msg);
            MyLog.d("", "notifyDataSetChanged: addLast " + msg.clientId);
        } else {
            imsMainRoomRoot.ll_sub_msg.set(index, msg);
            MyLog.d("", "notifyDataSetChanged: set " + msg.clientId);
        }

        /**
         * 这里可能隐藏一个很深的小问题，及图片list的次序可能和消息list的次序不同，这样点进去到相册时会发现次序不同
         * 但是点进去还是正确的图片，因为寻找图片是根据clientId来比对的
         */
        int index_image = MyConfig.searchRepeatMsgInList(imsMainRoomRoot.ll_sub_msg_with_image, msg, MyConfig.REPEAT_CHECK_COUNT_MAX);
        if (-1 == index_image) {
            MyConfig.addImageIMsg2ImageList(imsMainRoomRoot, msg, false);
        } else {
            MyConfig.setImageIMsg2ImageList(imsMainRoomRoot, msg, index_image);
        }


        if (la_msg != null) {
            la_msg.notifyDataSetChanged();
            MyLog.d("", "notifyDataSetChanged: la_msg != null " + msg.clientId);
        }


        preLoadAttached(msg);
    }

    private void preLoadAttached(IMsg msg) {

        /**
         * 全部下载，可能负担很大，且没必要
         * 考虑先下载最低端的几个，看看负载是否能承受，然后再开启这个功能
         */
        if(MyConfig.TRUE_TAG){
//            return;
        }

        /**
         * 目前只预下载短视频
         */

        if(msg.type == MyConfig.TYPE_TOPIC && msg.subType == MyConfig.SUBTYPE_VIDEO){
            /**
             * 20160725
             * 如果加入的是短视频，立即下载
             */
            MyLog.d("","video download: msg type " + msg.type + " " + msg.subType + " filePath_inLocal " + msg.filePath_inLocal);
            new MyConfig.DownloadFileFromUrl(new MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo(ActivityMainGroup.class.getName(), MyConfig.ATTACHMENT_ACTION_DO_NOTHING))
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                            MyConfig.getApiDomain_NoSlash_GetResources() + msg.filePath_inServer,//在服务器的地址
                            msg.filePath_inLocal);

        }else{
            MyLog.d("","video download: msg type " + msg.type + " " + msg.subType);
        }
    }


    private IMsg formSubjectFromIntent(Intent data) {
        String title = data.getStringExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_TITLE);
        String body = data.getStringExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_BODY);
        int share_in_category = data.getIntExtra(MyConfig.CONST_PARAM_STR_SHARE_IN_CATEGORY, MyConfig.SHARE_IN_CATEGORY_NOT_SHARE_IN);

        IMsg imsCreateSubject = new IMsg(
                MyConfig.usr_id,
                MyConfig.usr_nickname,
                MyConfig.usr_avatar,

                Calendar.getInstance().getTime(),

                MyConfig.LEVEL_MSG_1,
                imsMainRoomRoot,
                MyConfig.MSG_CATEGORY_SUBJECT,

                groupId,
                -1,
                MyConfig.formClientCheckId(groupId),

                title,
                body
        );

        if (share_in_category == MyConfig.SHARE_IN_CATEGORY_NEWS) {

            imsCreateSubject.news = "news";
        }

//        imsMainRoomRoot.ll_sub_msg.addLast(imsCreateSubject);
        return imsCreateSubject;
    }


    private void openSubjectInput(int attachedType, String title, String body) {
        Intent intent = new Intent(this, ActivityCreateSubject.class);
        intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_ACTIVITYTITLE, getString(R.string.menu_plus_subject));
        if (title != null) {
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_TITLE, title);
        }
        if (body != null) {
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_BODY, body);
        }
        setSubjectAttachedType(attachedType, intent);
        startActivityForResult(intent, attachedType);
    }

    private void setSubjectAttachedType(int attachedType, Intent intent) {

        switch (attachedType) {
            case MyConfig.REQUEST_SUBJECT_WITH_FILE:
                intent.putExtra(MyConfig.SUBJECT_ATTACHED_TYPE, MyConfig.SUBJECT_ATTACHED_FILE);
                break;
            case MyConfig.REQUEST_SUBJECT_WITH_IMAGE:
                intent.putExtra(MyConfig.SUBJECT_ATTACHED_TYPE, MyConfig.SUBJECT_ATTACHED_IMAGE);
                break;
            case MyConfig.REQUEST_SUBJECT_WITH_MAP:
                intent.putExtra(MyConfig.SUBJECT_ATTACHED_TYPE, MyConfig.SUBJECT_ATTACHED_MAP);
                break;
            case MyConfig.REQUEST_SUBJECT_WITH_VIDEO:
                intent.putExtra(MyConfig.SUBJECT_ATTACHED_TYPE, MyConfig.SUBJECT_ATTACHED_VIDEO);
                break;
        }

    }

    private void openSubjectInputWithAttach(int attachedType, String title, String body, String path, int share_in_category) {
        MyLog.d("", "fixbug---get file crash in mainroom: path is " + path);
        Intent intent = new Intent(this, ActivityCreateSubject.class);
        if (path != null) {
            intent.putExtra(MyConfig.CONST_STRING_PARAM_ATTACHED, path);

            File attached = new File(path);
            MyLog.d("", "fixbug---get file crash in mainroom: file path is " + attached.getAbsolutePath());


        }
        if (title != null) {
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_TITLE, title);
        }
        if (body != null) {
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_BODY, body);
        }
        intent.putExtra(MyConfig.CONST_PARAM_STR_SHARE_IN_CATEGORY, share_in_category);

        MyLog.d("", "fixbug---get file crash in mainroom: intent title is " + intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_TITLE));
        setSubjectAttachedType(attachedType, intent);
        MyLog.d("", "fixbug---get file crash in mainroom: intent2 title is " + intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_TITLE));

        startActivityForResult(intent, attachedType);
    }

    public void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        tempAttach = MyConfig.getCameraFileBigPicture();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempAttach));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, MyConfig.REQUEST_CAMERA);
        }
    }

    public void openSetupVote(int requestType) {
        Intent intent = new Intent(this, ActivityCreateVote.class);
        startActivityForResult(intent, requestType);
    }

    /**
     * 打开报名页面
     * @param requestType
     */
    public void openSetupActivity(int requestType) {
        Intent intent = new Intent(this, ActivityCreateActivity.class);
        startActivityForResult(intent, requestType);
    }

    private void openFileChooser(String mime) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setType(mime);
        if (mime.equals(MyConfig.MIME_IMAGE_STAR)) {
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, MyConfig.REQUEST_GALLERY);
            }
        } else if (mime.equals(MyConfig.MIME_FILE_STAR)) {
            if (intent.resolveActivity(getPackageManager()) != null) {
                MyLog.d("", "fixbug---get file crash: before get file");
                startActivityForResult(intent, MyConfig.REQUEST_FILE);
            }
        }
    }

    private void openMap() {
        Intent intent = new Intent(this, ActivityMyLocation.class);
        startActivityForResult(intent, MyConfig.REQUEST_MAP);
    }

    /**
     * 录制视频
     */
    private void openVideoRecorder() {
        Intent intent = new Intent(this, ActivityVideoRecorder.class);
        startActivityForResult(intent, MyConfig.REQUEST_VIDEO);
    }




    public File getCameraFileThumbnail() {
        return new File(MyConfig.appCacheDirPath, MyConfig.getCurrentTimeStr() + "_" + MyConfig.usr_id + "_thumb" + MyConfig.EXT_JPG);
    }


    private OnClickListener universalButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            subject_button_universal_layout.setVisibility(View.GONE);
            switch (v.getId()) {
                case R.id.universal_subject:
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_subject);
                    openSubjectInput(MyConfig.REQUEST_SUBJECT_ONLY, null, null);
                    break;
                case R.id.universal_camera:
                    tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_camera);
                    openCamera();
                    break;
                case R.id.universal_video:
                    tempAttachMime = MyConfig.MIME_VIDEO_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_video);
                    openVideoRecorder();
                    break;
                case R.id.universal_gallery:
                    tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_gallery);
                    openFileChooser(MyConfig.MIME_IMAGE_STAR);
                    break;
                case R.id.universal_file:
                    tempAttachMime = MyConfig.MIME_FILE_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_file);
                    openFileChooser(MyConfig.MIME_FILE_STAR);
                    break;
                case R.id.universal_map:
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_map);
                    openMap();
                    break;
                case R.id.universal_vote:
                    openSetupVote(MyConfig.REQUEST_CREATE_VOTE);

                    break;
                case R.id.universal_activity:
                    openSetupActivity(MyConfig.REQUEST_CREATE_ACTIVITY);
                    break;
            }

        }
    };

    private OnClickListener fabClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.fab_plus_subject:
                    fam.close(true);
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_subject);
                    openSubjectInput(MyConfig.REQUEST_SUBJECT_ONLY, null, null);
                    break;
                case R.id.fab_plus_camera:
                    fam.close(true);
                    tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_camera);
                    openCamera();
                    break;
                case R.id.fab_plus_gallery:
                    fam.close(true);
                    tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_gallery);
                    openFileChooser(MyConfig.MIME_IMAGE_STAR);
                    break;
                case R.id.fab_plus_file:
                    fam.close(true);
                    tempAttachMime = MyConfig.MIME_FILE_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_file);
                    openFileChooser(MyConfig.MIME_FILE_STAR);
                    break;
                case R.id.fab_plus_map:
                    fam.close(true);
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_map);
                    openMap();
                    break;
                case R.id.fab_plus_vote:
                    fam.close(true);
                    openSetupVote(MyConfig.REQUEST_CREATE_VOTE);
                    break;
            }

        }
    };

    public void showPlusDialog() {
        MyConfig.hide_keyboard_must_call_from_activity(ActivityMainGroup.this);
        hideEmojiDialog();

        if (MyConfig.plusInvokeMode == MyConfig.PLUS_POPUP_MENU) {
            PopupMenu popup = new PopupMenu(this, ivPlus);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.menu_plus, popup.getMenu());
            popup.show();

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_plus_subject:
                            openSubjectInput(MyConfig.REQUEST_SUBJECT_ONLY, null, null);
                            return true;
                        case R.id.menu_plus_camera:
                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                            openCamera();
                            return true;
                        case R.id.menu_plus_gallery:
                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                            openFileChooser(MyConfig.MIME_IMAGE_STAR);
                            return true;
                        case R.id.menu_plus_file:
                            tempAttachMime = MyConfig.MIME_FILE_STAR;
                            openFileChooser(MyConfig.MIME_FILE_STAR);
                            return true;
                        case R.id.menu_plus_map:
                            openMap();
                            return true;
                        case R.id.menu_plus_vote:
                            openSetupVote(MyConfig.REQUEST_CREATE_VOTE);
                            return true;
                        default:
                            return false;
                    }
                }
            });

           /* popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                @Override
                public void onDismiss(PopupMenu menu) {
                    ((ToggleButton) ivPlus).setChecked(false);
                }
            });*/
        } else if (MyConfig.plusInvokeMode == MyConfig.PLUS_DIALOG) {
            new DialogFragment() {
                @Override
                public Dialog onCreateDialog(Bundle savedInstanceState) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            getActivity());
                    builder.setTitle("Please choose:")
                            .setItems(R.array.subjects, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    // of the selected item
                                    switch (which) {
                                        case 0:
                                            MyLog.d("", "directroom: item 0");
                                            openSubjectInput(MyConfig.REQUEST_SUBJECT_ONLY, null, null);
                                            return;
                                        case 1:
                                            MyLog.d("", "directroom: item 1");
                                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                                            openCamera();
                                            return;
                                        case 2:
                                            MyLog.d("", "directroom: item 2");
                                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                                            openFileChooser(MyConfig.MIME_IMAGE_STAR);
                                            return;
                                        case 3:
                                            MyLog.d("", "directroom: item 3");
                                            openMap();
                                            return;
                                        case 4:
                                            MyLog.d("", "directroom: item 4");
                                            tempAttachMime = MyConfig.MIME_FILE_STAR;
                                            openFileChooser(MyConfig.MIME_FILE_STAR);
                                            return;
                                        case 5:
                                            MyLog.d("", "directroom: item 5");
                                            openSetupVote(MyConfig.REQUEST_CREATE_VOTE);
                                            return;
                                        default:
                                            return;
                                    }
                                }
                            })
                            .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    return builder.create();
                }
            }.show(getFragmentManager(), "");
        } else if (MyConfig.plusInvokeMode == MyConfig.PLUS_UNIVERSAL) {

            subject_button_universal_layout.setVisibility(View.VISIBLE);

        }


    }


    Menu menu;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_room, menu);
        this.menu = menu;
        MyLog.d("", "sequence of optionmenu create: 3" + this.menu + "~");

        updatePagerMenu(room_type);

        return super.onCreateOptionsMenu(menu);
    }


    public void updatePagerMenu(int room_type) {

        if(menu == null){
            return;
        }
        menu.findItem(R.id.main_room_album).setVisible(true).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        if (room_type == -1) {
            menu.findItem(R.id.main_room_more).setVisible(false);
            menu.findItem(R.id.main_room_sub_list).setVisible(false);
        } else if(room_type == MyConfig.LOCAL_ROOM_CATEGORY_NORMAL){
            menu.findItem(R.id.main_room_more).setVisible(true).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            menu.findItem(R.id.main_room_sub_list).setVisible(false);
        } else if(room_type == MyConfig.LOCAL_ROOM_CATEGORY_DIRECT){
            menu.findItem(R.id.main_room_more).setVisible(false);
            menu.findItem(R.id.main_room_sub_list).setVisible(true).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finishWithNotify();
                return true;

            case R.id.main_room_album:

                Intent room_album = new Intent(ActivityMainGroup.this, ActivityGroupAlbum.class);
                room_album.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
                room_album.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, name);
                room_album.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, desc);
                startActivityForResult(room_album, MyConfig.REQUEST_GROUP_INFO);
                return true;

            case R.id.main_room_more:

                if (popupWindow.isShowing()) {
                    popupWindow.dismiss();
                } else {
                    int xoffInPixels = MyConfig.screenWidth - 400;
                    int xoffInDip = MyConfig.px2dip(this, xoffInPixels);
                    popupWindow.showAsDropDown(this.findViewById(R.id.main_room_more), -xoffInDip, -20);
                    popupWindow.update();
                }
                return true;


//            case R.id.main_room_info:
//
//                if (et.getText().toString().trim().equals("set")) {
//
//                    Intent intent_advance_setting = new Intent(ActivityMainGroup.this, ActivityAdvanceSetting.class);
//                    startActivity(intent_advance_setting);
//
//                    return true;
//                }
//
//                Intent room_info = new Intent(ActivityMainGroup.this, ActivityGroupInfo.class);
//                room_info.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
//                room_info.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, name);
//                room_info.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, desc);
//                startActivityForResult(room_info, MyConfig.REQUEST_GROUP_INFO);
//                return true;
//
            case R.id.main_room_sub_list:
                Intent subject_list = new Intent(ActivityMainGroup.this, ActivitySubjectList.class);
                subject_list.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
                subject_list.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, name);
                startActivityForResult(subject_list, MyConfig.REQUEST_SUBJECT_LIST);
                //   finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void initPopUp() {
        LinearLayout pop_mainroom_info_layout, pop_mainroom_subjects_layout;

        View popView = getLayoutInflater().inflate(R.layout.popwindow_mainroom_view, null);

        pop_mainroom_info_layout = (LinearLayout) popView.findViewById(R.id.pop_mainroom_info_layout);
        pop_mainroom_subjects_layout = (LinearLayout) popView.findViewById(R.id.pop_mainroom_subjects_layout);

        popupWindow = new PopupWindow(popView, LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, true);

        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());

        pop_mainroom_info_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et.getText().toString().trim().equals("set")) {
                    Intent intent_advance_setting = new Intent(ActivityMainGroup.this, ActivityAdvanceSetting.class);
                    startActivity(intent_advance_setting);
                    return;
                }

                Intent room_info = new Intent(ActivityMainGroup.this, ActivityGroupInfo.class);
                room_info.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
                room_info.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, name);
                room_info.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, desc);
                startActivityForResult(room_info, MyConfig.REQUEST_GROUP_INFO);

                popupWindow.dismiss();
            }
        });
        pop_mainroom_subjects_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent subject_list = new Intent(ActivityMainGroup.this, ActivitySubjectList.class);
                subject_list.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
                subject_list.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, name);
                startActivityForResult(subject_list, MyConfig.REQUEST_SUBJECT_LIST);

                popupWindow.dismiss();
            }
        });


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            finishWithNotify();
            return true; // 让系统不再处理这次回退键，因为系统收到回退键后会立即把app关了
        }
        return super.onKeyDown(keyCode, event);
    }


    private void finishWithNotify() {
        /**
         * todo
         *  从聊天室退出时需要通知服务器
         *  grouplist刷新一次
         */

        final JsonGroupResetUnread jsonGroupResetUnread = new JsonGroupResetUnread();
        jsonGroupResetUnread.accessToken = MyConfig.usr_token;
        jsonGroupResetUnread.groupId = groupId;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonGroupResetUnread, JsonGroupResetUnread.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_RESET_UNREAD + jsonGroupResetUnread.groupId + MyConfig.API2_GROUP_RESET_UNREAD_2, jsonStr);
                MyLog.d("", "HttpTools: finishWithNotify " + s);

            }
        }).start();

        /**
         * 从服务器更新group列表
         */
        EventBus.getDefault().post(new EventGroupListUpdateFromServer());


//        MyConfig.cleanAndFinishActivity(this);
        finish();

    }


    public void onEventMainThread(EventFileDownloadProgress eventFileDownloadProgress) {

        if (eventFileDownloadProgress.downloadFileFromUrlSenderInfo.sender.equals(ActivityMainGroup.class.getName())) {

            MyLog.d("", "share out, onEventMainThread mainrooom  " + eventFileDownloadProgress.downloadFileFromUrlSenderInfo.sender);

            IMsg iMsgNeedAction = null;

            //更新数据
            for (IMsg iMsg : imsMainRoomRoot.ll_sub_msg) {

                if (eventFileDownloadProgress.des.equals(iMsg.filePath_inLocal)) {

                    /**
                     *  所有本地文件相同的msg都得到更新
                     *      一个服务器上的文件可能对应客户端多个主题的文件
                     *      如果一个主题的文件下载进度更新了，则所有同一个文件的主题的下载进度均更新
                     */


                    iMsg.fileDownloadProgress = eventFileDownloadProgress.progress;

                    MyLog.d("", "share out, onEventMainThread iMsg.filePath_inLocal  " + iMsg.filePath_inLocal + " progress:" + iMsg.fileDownloadProgress + " action:"+eventFileDownloadProgress.downloadFileFromUrlSenderInfo.code);

                    //如果要分享的话，在download stoped且progress为100时找出被点击的msg
                    if ((eventFileDownloadProgress.downloadFileFromUrlSenderInfo.code != MyConfig.ATTACHMENT_ACTION_DO_NOTHING)
                            && (eventFileDownloadProgress.status == MyConfig.FILE_DOWNLOAD_STOPED)
                            && (iMsg.fileDownloadProgress == 100)) {

                        MyLog.d("", "share out, onEventMainThread 2 eventFileDownloadProgress.src  " + eventFileDownloadProgress.src + " filePath_inServer:" + MyConfig.getApiDomain_NoSlash_GetResources() + iMsg.filePath_inServer);

                        if (eventFileDownloadProgress.src.equals(MyConfig.getApiDomain_NoSlash_GetResources() + iMsg.filePath_inServer)) {
                            /**
                             *  比对服务器文件路径，这个才是真正被点击的msg
                             *  但是这里有个问题，文件如果被重定向过，这里会对比失败
                             */
                            iMsgNeedAction = iMsg;
                        }

                    }

                }
            }

            //刷新时不scroll，保持原有的transcriptMode
            int mode = lv_msg.getRefreshableView().getTranscriptMode();
            lv_msg.getRefreshableView().setTranscriptMode(ListView.TRANSCRIPT_MODE_DISABLED);
            la_msg.notifyDataSetChanged();
            //        lv_msg.getRefreshableView().setTranscriptMode(mode);//感觉notifyDataSetChanged被延迟执行了，而TranscriptMode模式是即时修改的。这里暂时这样处理，防止notify时发生scroll

            MyLog.d("", "share out, mainrooom onEventMainThread iMsgNeedShare 0");
            if (iMsgNeedAction != null) {
                MyLog.d("", "share out, mainrooom onEventMainThread iMsgNeedShare 1");

                /**
                 * 这里应该根据action的类型进行处理，不一定是share
                 */
                if(eventFileDownloadProgress.downloadFileFromUrlSenderInfo.code == MyConfig.ATTACHMENT_ACTION_DL_SHARE) {
                    MyConfig.shareTopic(this, iMsgNeedAction, eventFileDownloadProgress.downloadFileFromUrlSenderInfo.code);
                }
            }

        }

    }


    Dialog dialog_newbie;

    private void newbie_3() {

        MyConfig.app_open_count_newbie_guide_3 = MyConfig.loadIntFromPreference(ActivityMainGroup.this, "app_open_count_newbie_guide_3");

        if (MyConfig.app_open_count_newbie_guide_3 == -1) {

            MyConfig.app_open_count_newbie_guide_3 = 1;
            MyConfig.saveIntToPreference(ActivityMainGroup.this, "app_open_count_newbie_guide_3", MyConfig.app_open_count_newbie_guide_3);

            dialog_newbie = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
            ImageView view = new ImageView(this);
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog_newbie.dismiss();
                }
            });
            Picasso.with(this).load(R.drawable.newbie_guide_second)
//                    .resize(MyConfig.screenWidth, MyConfig.screenHeight)
                    .fit()
                    .into(view);
            dialog_newbie.setContentView(view);
            dialog_newbie.show();

            return;
        }
    }

    public void onEventMainThread(EventHeartOneSecond eventHeartOneSecond) {

        time_in_room++;

        if (time_in_room > MyConfig.room_timeout_to_clear_unread) {
            main_goto_unread.setVisibility(View.INVISIBLE);
        }

    }

    /**
     * 初始化距离传感器
     */
    private void initAudioMode() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);/**得到传感器管理器*/
        mAudioSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);/**距离传感器对象*/

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);/**得到音频管理器*/
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);/**听筒模式*/
        mySensorEventListener = new MySensorEventListener();
        mSensorManager.registerListener(mySensorEventListener, mAudioSensor,SensorManager.SENSOR_DELAY_NORMAL);
    }

    /**
     * 定义听筒距离监听器，根据距离判断使用听筒模式还是音响模式
     */
    class MySensorEventListener implements SensorEventListener{

        @Override
        public void onSensorChanged(SensorEvent event) {
            float range = event.values[0];
            if (range == mAudioSensor.getMaximumRange()) {
                audioManager.setMode(AudioManager.MODE_NORMAL);/**音响模式*/
            } else {
                audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);/**听筒模式*/
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    }



}
