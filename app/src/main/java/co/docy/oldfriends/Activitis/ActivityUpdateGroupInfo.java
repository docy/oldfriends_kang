package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonUpdateGroupInfo;
import co.docy.oldfriends.Messages.JsonUpdateGroupInfoRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;


public class ActivityUpdateGroupInfo extends ActivityBase {

    EditText company_update_et_name;
    EditText company_update_et_desc;
    String itemName;
    private String tempString;
    private TextView remindText;
    private MyConfig.IOnOkClickListener iOnOkClickListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_update);
        Intent intent = getIntent();
        itemName = intent.getStringExtra("type");
        init();
    }

    private void init() {

        remindText = (TextView) findViewById(R.id.company_update_remind_text);

        company_update_et_name = (EditText) findViewById(R.id.company_update_et_name);
        company_update_et_name.setHint(getResources().getString(R.string.group_name_null_remind));
        company_update_et_name.setText(MyConfig.jsonGetGroupInfoRet.data.name);
        company_update_et_name.setSelection(MyConfig.jsonGetGroupInfoRet.data.name.length());

        company_update_et_desc = (EditText) findViewById(R.id.company_update_et_desc);
        company_update_et_desc.setHint(getResources().getString(R.string.group_desc_null_remind));
        company_update_et_desc.setText(MyConfig.jsonGetGroupInfoRet.data.desc);
        company_update_et_desc.setSelection(MyConfig.jsonGetGroupInfoRet.data.desc.length());
         /**
         * 根据目前UI设计，只能同时修改一个参数
         */
        company_update_et_name.setVisibility(View.GONE);
        company_update_et_desc.setVisibility(View.GONE);
        if (itemName.equals(MyConfig.CONSTANT_PARAMETER_STRING_NAME)) {
            company_update_et_name.setVisibility(View.VISIBLE);
            setTitle(getResources().getString(R.string.group_name_change));
            remindText.setText(getResources().getString(R.string.group_name_null_remind));
        } else if (itemName.equals(MyConfig.CONSTANT_PARAMETER_STRING_DESC)) {
            company_update_et_desc.setVisibility(View.VISIBLE);
            setTitle(getResources().getString(R.string.group_desc_change));
            remindText.setText(getResources().getString(R.string.group_desc_null_remind));
        }
    }


    public void backWithoutResult() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
//        Toast.makeText(this, "input is discarded", Toast.LENGTH_LONG).show();
        backWithoutResult();
    }

    private void backWithResult() {
        Intent intent = new Intent();
//        intent.putExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_TITLE, create_subject_title_et.getText().toString());
//        intent.putExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_BODY, create_subject_body_et.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    private void updateCompanyInfoToServer() {
        MyLog.d("","updateCompanyInfoToServer 1");


        new Thread(new Runnable() {
            @Override
            public void run() {
                MyLog.d("","updateCompanyInfoToServer 2");

//                final JsonUpdateCompanyInfo jsonUpdateCompanyInfo = JsonUpdateCompanyInfo.fromJsonGetGroupUserRet(MyConfig.jsonGetCompanyInfoRet.data);
                final JsonUpdateGroupInfo jsonUpdateGroupInfo = new JsonUpdateGroupInfo();

                if (itemName.equals(MyConfig.CONSTANT_PARAMETER_STRING_NAME)) {
                    tempString = company_update_et_name.getText().toString().trim();
                    if (tempString.length()>10 || tempString.length()< 2){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MyConfig.MyToast(0, ActivityUpdateGroupInfo.this, getResources().getString(R.string.group_name_fail_remind));
                            }
                        });
                    }else {
                        jsonUpdateGroupInfo.name = tempString;
                    }


                } else if (itemName.equals(MyConfig.CONSTANT_PARAMETER_STRING_DESC)) {// jsonUpdateGroupInfo.desc
                   tempString =  company_update_et_desc.getText().toString().trim();
                    if (tempString.length()>30 || tempString.length()< 5){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MyConfig.MyToast(0, ActivityUpdateGroupInfo.this, getResources().getString(R.string.group_desc_fail_remind));
                            }
                        });
                    }else {
                        jsonUpdateGroupInfo.desc = tempString;
                    }
                }

                jsonUpdateGroupInfo.accessToken = MyConfig.usr_token;

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonUpdateGroupInfo, JsonUpdateGroupInfo.class);
                MyLog.d("", "HttpTools: jsonStr " + jsonStr);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_INFO_UPDATE + MyConfig.jsonGetGroupInfoRet.data.id, jsonStr);

                final JsonUpdateGroupInfoRet jsonUpdateGroupInfoRet = gson.fromJson(s, JsonUpdateGroupInfoRet.class);
                MyLog.d("", "HttpTools: " + s);
                if (jsonUpdateGroupInfoRet != null && jsonUpdateGroupInfoRet.code==MyConfig.retSuccess()) {
                   finish();
                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            iOnOkClickListener = new MyConfig.IOnOkClickListener() {
                                @Override
                                public void OnOkClickListener() {
                                }
                            };
                            MyConfig.showRemindDialog("",jsonUpdateGroupInfoRet.message, ActivityUpdateGroupInfo.this,false,iOnOkClickListener);
                        }
                    });
                }

            }
        }).start();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_update_submit, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                Toast.makeText(this, "home key is pressed, input is discarded", Toast.LENGTH_LONG).show();

                backWithoutResult();
                return true;
            case R.id.update_submit:

                updateCompanyInfoToServer();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
