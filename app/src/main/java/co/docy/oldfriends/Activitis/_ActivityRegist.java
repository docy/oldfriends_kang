package co.docy.oldfriends.Activitis;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventHeartOneSecond;
import co.docy.oldfriends.Messages.JsonLogin;
import co.docy.oldfriends.Messages.JsonRegistRet;
import co.docy.oldfriends.Messages.JsonRegistWithPhoto;
import co.docy.oldfriends.Messages.JsonSmsSignup;
import co.docy.oldfriends.Messages.JsonSmsSignupRet;
import co.docy.oldfriends.Messages.JsonWeixinLoginRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;


public class _ActivityRegist extends ActivityBase {
    LinearLayout regist_layout;
    Button bNext, regist_valid_button;
    EditText et_name;
    EditText et_email;
    EditText et_pwd;
    EditText et_phone;
    EditText et_code;
    TextView tv_return;

    AVLoadingIndicatorView regist_avloadingIndicatorView;

    JsonRegistWithPhoto jsonRegistWithPhoto;
    int count = 0;
    boolean waitting_server;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);

        //    relativeLayout = (RelativeLayout) findViewById(R.id.regist_layout);
//        relativeLayout.getLayoutParams().width = MyConfig.screenWidth * 3 / 4;
//        relativeLayout.getLayoutParams().height = MyConfig.screenHeight * 2 / 3;

        regist_layout = (LinearLayout)findViewById(R.id.activity_regist_layout);
        regist_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConfig.hide_keyboard_must_call_from_activity(_ActivityRegist.this);
            }
        });

        et_name = (EditText) findViewById(R.id.activity_regist_name);
        et_email = (EditText) findViewById(R.id.regist_v2_email);
        et_pwd = (EditText) findViewById(R.id.activity_regist_pwd);
        et_phone = (EditText) findViewById(R.id.activity_regist_phone);

        regist_valid_button = (Button) findViewById(R.id.activity_regist_code_button);
        regist_valid_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 0) {
                    smsSignup();
                }
            }
        });
        et_code = (EditText) findViewById(R.id.fragment_page_account_reset_code);

        bNext = (Button) findViewById(R.id.activity_regist_next);
        bNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotoRegist();

            }
        });


        tv_return = (TextView) findViewById(R.id.regist_v2_return);

        waitting_server = false;
        regist_avloadingIndicatorView = (AVLoadingIndicatorView)findViewById(R.id.regist_avloadingIndicatorView);
//        regist_avloadingIndicatorView.setVisibility(View.GONE);
    }

    private void showWaitAnimator(final boolean b){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(b){
                    regist_avloadingIndicatorView.setVisibility(View.VISIBLE);
                }else {
                    regist_avloadingIndicatorView.setVisibility(View.GONE);
                }
            }
        });
    }

    private void gotoRegist() {

        jsonRegistWithPhoto = new JsonRegistWithPhoto();
        jsonRegistWithPhoto.name = et_name.getText().toString().trim();
        et_name.setText(jsonRegistWithPhoto.name);
//        jsonRegistWithPhoto.email = et_email.getText().toString().trim();
//        et_email.setText(jsonRegistWithPhoto.email);
        jsonRegistWithPhoto.phone = et_phone.getText().toString().trim();
        et_phone.setText(jsonRegistWithPhoto.phone);
        jsonRegistWithPhoto.code = et_code.getText().toString().trim();
        jsonRegistWithPhoto.password = et_pwd.getText().toString().trim();
        et_pwd.setText(jsonRegistWithPhoto.password);


        jsonRegistWithPhoto.sex = true;//先假定为男，下一步更改

        /**
         * 这里客户端先自己做一个合法性检测
         */
        if (jsonRegistWithPhoto.name.length() == 0) {
            notifyRegistResualt(getResources().getString(R.string.null_name));
            return;
        }
        if (jsonRegistWithPhoto.phone.length() == 0) {
            notifyRegistResualt(getResources().getString(R.string.null_phone));
            return;
        }
//        if (jsonRegistWithPhoto.email.length() == 0) {
//            notifyRegistResualt(getResources().getString(R.string.null_email));
//            return;
//        }
        //客户端取消密码限制 20151216
//        if (jsonRegistWithPhoto.password.length() < getResources().getInteger(R.integer.mini_length_user_pwd)) {
//            notifyRegistResualt("密码不能少于6位");
//            return;
//        }
        if (jsonRegistWithPhoto.code.length() == 0) {
            notifyRegistResualt(getResources().getString(R.string.null_code));
            return;
        }


        /**
         * 新注册接口，包含可选的photo信息
         */
        new Thread(new Runnable() {
            @Override
            public void run() {

                showWaitAnimator(true);

                JsonRegistRet jsonRegistRet = HttpTools.okhttpUploadRegistWithPhoto(jsonRegistWithPhoto);


                if (jsonRegistRet != null) {


                    if (jsonRegistRet.code!=MyConfig.retSuccess()) {
                        notifyRegistResualt(jsonRegistRet.message);

                    } else {

                        notifyRegistResualt(getResources().getString(R.string.regist_success));

//                        MyConfig.prefEditor.putString(MyConfig.PREF_USER_NAME, jsonRegistWithPhoto.name);//下一步有，这里似乎多余
//                        MyConfig.prefEditor.putString(MyConfig.PREF_USER_PWD, jsonRegistWithPhoto.password);
//                        MyConfig.prefEditor.commit();

                        loginRequest();

                    }
                } else {
                    notifyRegistResualt(getResources().getString(R.string.server_not_return));
                }

                showWaitAnimator(false);

            }
        }).start();


//
//        if(MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//
//                    Gson gson = new Gson();
//                    String j = gson.toJson(jsonRegistCheck);
//
//                    final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_REGIST_CHECK, j);
//
//                    JsonRegistCheckRet jsonRegistCheckRet = gson.fromJson(s, JsonRegistCheckRet.class);
//
//                    if (jsonRegistCheckRet != null) {
//
//                        if (jsonRegistCheckRet.code==MyConfig.retSuccess()) {
//                            Intent intent_next = new Intent(ActivityRegist.this, ActivityRegistPersonInfo.class);
//                            intent_next.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, et_name.getText().toString());
//                            intent_next.putExtra("email", et_email.getText().toString());
//                            intent_next.putExtra("pwd", et_pwd.getText().toString());
//                            intent_next.putExtra("phone", et_phone.getText().toString());
////                        intent_next.putExtra("code", et_code.getText().toString());
//                            startActivity(intent_next);
//
//                        } else {
//                            notifyRegistResualt(jsonRegistCheckRet.message);
//                            if (jsonRegistCheckRet.message.contains("电话") && jsonRegistCheckRet.message.contains("注册")) {
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        showErrorDialog(getString(R.string.regist_telephone_remind));
//                                    }
//                                });
//                            } else if (jsonRegistCheckRet.message.contains("邮箱") && jsonRegistCheckRet.message.contains("注册")) {
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        showErrorDialog(getString(R.string.regist_mail_remind));
//                                    }
//                                });
//                            }
//
//                       /* Intent intent_next = new Intent(ActivityRegist.this, ActivityRegistRemind.class);
//                        startActivity(intent_next);*/
//
//                        }
//                    }
//
//
//                }
//            }).start();
//        }

    }

//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//
//        if (requestCode == MyConfig.REQUEST_LOGIN) {
//
//            if (resultCode == Activity.RESULT_OK) {
//
//                /**
//                 * 转到注册的第二步
//                 */
//
//
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//                /**
//                 * 转到登录前
//                 */
//            }
//        }
//
//    }


    public void loginRequest() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonLogin jsonLogin = new JsonLogin();
                jsonLogin.account = jsonRegistWithPhoto.name;
                jsonLogin.password = jsonRegistWithPhoto.password;
                jsonLogin.os = MyConfig.os_string;
                jsonLogin.uuid = MyConfig.uuid;

                Gson gson = new Gson();
                String json_login = gson.toJson(jsonLogin);

                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LOGIN, json_login);

                JsonWeixinLoginRet jsonLoginRet = gson.fromJson(s, JsonWeixinLoginRet.class);

                if (jsonLoginRet != null) {

                    if (jsonLoginRet != null){
                        if(jsonLoginRet.code==MyConfig.retSuccess()) {

                            MyConfig.logoutClear();

                            MyConfig.usr_login_ret_json = s;
                            MyConfig.jsonWeixinLoginRet = jsonLoginRet;
                            MyConfig.usr_id = jsonLoginRet.data.id;
                            MyConfig.usr_name = jsonLoginRet.data.name;//注意name要从这里取，因为login_name_or_phone也可能是phone
                            MyConfig.usr_nickname = jsonLoginRet.data.nickName;
                            MyConfig.usr_phone = jsonLoginRet.data.phone;
//                            MyConfig.usr_pwd = jsonLoginRet.data.password;//小心，或许以后这里不会返回密码，则需要从jsonRegistWithPhoto获取
                            MyConfig.usr_pwd = jsonRegistWithPhoto.password;
                            MyConfig.usr_token = jsonLoginRet.data.authToken;//常用变量就提取出来
                            MyConfig.usr_status = MyConfig.USR_STATUS_LOGIN;
                        //    MyConfig.usr_email = jsonLoginRet.data.email;
                            MyConfig.usr_phone = jsonLoginRet.data.phone;
                            if (jsonLoginRet.data.sex) {
                                MyConfig.usr_sex = 1;//男
                            } else {
                                MyConfig.usr_sex = 0;//女
                            }
                            MyConfig.usr_avatar = jsonLoginRet.data.avatar;//实际上这一步没有avatar信息
                            MyConfig.usr_origin = jsonLoginRet.data.origin;
                            if(jsonLoginRet.data.info != null){MyConfig.usr_name_changed = jsonLoginRet.data.info.changed;}

                            MyConfig.saveUserDataToPreference(_ActivityRegist.this);

                            Intent intent_person_info = new Intent(_ActivityRegist.this, _ActivityRegistPersonInfo.class);
                            startActivity(intent_person_info);

                            MyConfig.flag_open_invite_code_activity = 1;//0, no invite, 1,please start invite, 2, have started invite
                            MyLog.d("", "app start steps 0, regist set flag_open_invite_code_activity");

                            finish();

                        }else{
                            finishWithLoginFailed();

                        }
                    }

                } else {

                    finishWithLoginFailed();

//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
////                            Toast.makeText(ActivityLogin.this, getString(R.string.net_msg_not_connected), Toast.LENGTH_SHORT).show();
//                            MyConfig.MyToast(0, ActivityRegist.this, getString(R.string.net_msg_not_connected));
//                        }
//                    });

                }

            }
        }).start();
    }

    public void finishWithLoginFailed() {
        /**
         * 登录失败则跳转到登录页面
         */
        Intent intent_login = new Intent(_ActivityRegist.this, _ActivityLogin.class);
        startActivity(intent_login);

        finish();
    }

    /**
     * 电话号码已经被注册后，弹出对话框
     * *
     */
    private void showErrorDialog(String remindstr) {
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(
                R.layout.dialog_login_remind_layout, null);
        final Dialog setDialog = new Dialog(_ActivityRegist.this, R.style.DialogStyle);
        setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialog.setCancelable(false);
        setDialog.getWindow().setContentView(dialogView);
        WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
        lp.width = MyConfig.getWidthByScreenPercent(80);
        setDialog.getWindow().setAttributes(lp);
        Button cancleButton = (Button) dialogView.findViewById(R.id.cancle);
        Button loginButton = (Button) dialogView.findViewById(R.id.login);
        TextView remindText = (TextView) dialogView.findViewById(R.id.remind_text);
        remindText.setText(remindstr);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                startActivity(new Intent(_ActivityRegist.this, _ActivityLogin.class));
                setDialog.cancel();
            }

        });
        cancleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialog.cancel();
            }
        });
        setDialog.setCanceledOnTouchOutside(false);
        setDialog.show();
    }

    private void notifyRegistResualt(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MyConfig.MyToast(0, _ActivityRegist.this,
                        message);
            }
        });

    }



    public void onEventMainThread(EventHeartOneSecond eventHeartOneSecond) {

        if (count > 0) {
            count--;
        }
        if (count > 0) {
            String format = getResources().getString(R.string.please_wait);
            String result = String.format(format,count);
            regist_valid_button.setText(result);
        } else {
            regist_valid_button.setText(getString(R.string.prompt_valid_button));
        }

    }


    private void smsSignup() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSmsSignup jsonSmsSignup = new JsonSmsSignup();
                jsonSmsSignup.phone = et_phone.getText().toString();


                String jsonStr = gson.toJson(jsonSmsSignup, JsonSmsSignup.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SMS_SIGNUP, jsonStr);
                MyLog.d("", "apitest: send msg ret " + s);

                final JsonSmsSignupRet jsonSmsSignupRet = gson.fromJson(s, JsonSmsSignupRet.class);
                if (jsonSmsSignupRet != null) {

                    if (jsonSmsSignupRet.code==MyConfig.retSuccess()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                count = 60;
                                String format = getResources().getString(R.string.please_wait);
                                String result = String.format(format,count);
                                regist_valid_button.setText(result);
                                if (MyConfig.debug_autofill_validcode) {
                                    et_code.setText(jsonSmsSignupRet.data.code);
                                }

                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MyConfig.MyToast(0, _ActivityRegist.this, jsonSmsSignupRet.message);
                            }
                        });

                    }
                }else{
                    MyConfig.MyToast(0, _ActivityRegist.this, getResources().getString(R.string.server_connect_fail));
                }

            }
        }).start();
    }



}
