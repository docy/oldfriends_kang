package co.docy.oldfriends.Activitis;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventHeartOneSecond;
import co.docy.oldfriends.EventBus.EventRegistSubmitted;
import co.docy.oldfriends.Messages.JsonResetPassword;
import co.docy.oldfriends.Messages.JsonResetPasswordRet;
import co.docy.oldfriends.Messages.JsonSmsResetPassword;
import co.docy.oldfriends.Messages.JsonSmsResetPasswordRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;


public class _ActivityResetPassword extends ActivityBase {

    Button reset_password_next, reset_password_valid_button;
    EditText reset_password_pwd;
    EditText reset_password_phone;
    EditText reset_password_code;

    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        reset_password_pwd = (EditText) findViewById(R.id.reset_password_pwd);
        reset_password_phone = (EditText) findViewById(R.id.reset_password_phone);

        reset_password_valid_button = (Button) findViewById(R.id.reset_password_valid_button);
        reset_password_valid_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 0) {
                    smsResetPassword();
                }
            }
        });
        reset_password_code = (EditText) findViewById(R.id.reset_password_code);

        reset_password_next = (Button) findViewById(R.id.reset_password_next);
        reset_password_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                submitReset();

            }
        });


        /**
         * http和socket可能同时操作list，这里可能应该做一点互斥处理
         */
        //EventBus.getDefault().register(this);
    }


    private void submitReset() {

        final JsonResetPassword jsonResetPassword = new JsonResetPassword();

        jsonResetPassword.phone = reset_password_phone.getText().toString().trim();
        reset_password_phone.setText(jsonResetPassword.phone);
        jsonResetPassword.code = reset_password_code.getText().toString().trim();
        reset_password_code.setText(jsonResetPassword.code);
        jsonResetPassword.password = reset_password_pwd.getText().toString().trim();
        reset_password_pwd.setText(jsonResetPassword.password);


        if (jsonResetPassword.phone.length() == 0) {
            notifyFailure(getResources().getString(R.string.null_phone));
            return;
        }
        if (jsonResetPassword.password.length() == 0) {
            notifyFailure(getResources().getString(R.string.passowrd_null_remind));
            return;
        }
        if (jsonResetPassword.code.length() == 0) {
            notifyFailure(getResources().getString(R.string.null_code));
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();
                String strJsonResetPassword = gson.toJson(jsonResetPassword);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_RESET_PASSWORD, strJsonResetPassword);

                JsonResetPasswordRet jsonResetPasswordRet = gson.fromJson(s, JsonResetPasswordRet.class);

                if (jsonResetPasswordRet != null) {

                    if (jsonResetPasswordRet.code==MyConfig.retSuccess()) {
                        Intent intent_next = new Intent(_ActivityResetPassword.this, ActivityLoginTongChuang.class);

                        MyConfig.usr_pwd = jsonResetPassword.password;
                        MyConfig.prefEditor.putString(MyConfig.PREF_USER_PWD, MyConfig.usr_pwd);
                        MyConfig.prefEditor.commit();

                        startActivity(intent_next);

                        finish();

                    } else {
                        notifyFailure(jsonResetPasswordRet.message);

//                        if(jsonResetPasswordRet.message.contains("电话")&&jsonResetPasswordRet.message.contains("注册")){
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    showErrorDialog(getString(R.string.regist_telephone_remind));
//                                }
//                            });
//                        }else if(jsonResetPasswordRet.message.contains("邮箱")&&jsonResetPasswordRet.message.contains("注册")){
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    showErrorDialog(getString(R.string.regist_mail_remind));
//                                }
//                            });
//                        }


                    }
                }


            }
        }).start();


    }

    /**
     * 电话号码已经被注册后，弹出对话框
     * *
     */
    private void showErrorDialog(String remindstr) {
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(
                R.layout.dialog_login_remind_layout, null);
        final Dialog setDialog = new Dialog(_ActivityResetPassword.this, R.style.DialogStyle);
        setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialog.setCancelable(false);
        setDialog.getWindow().setContentView(dialogView);
        WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
        lp.width = MyConfig.getWidthByScreenPercent(80);
        setDialog.getWindow().setAttributes(lp);
        Button cancleButton = (Button) dialogView.findViewById(R.id.cancle);
        Button loginButton = (Button) dialogView.findViewById(R.id.login);
        TextView remindText = (TextView) dialogView.findViewById(R.id.remind_text);
        remindText.setText(remindstr);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                startActivity(new Intent(_ActivityResetPassword.this, _ActivityLogin.class));
                setDialog.cancel();
            }

        });
        cancleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialog.cancel();
            }
        });
        setDialog.setCanceledOnTouchOutside(false);
        setDialog.show();
    }

    private void notifyFailure(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Toast.makeText(
//                        ActivityRegist.this,
//                        message,
//                        Toast.LENGTH_LONG).show();
                MyConfig.MyToast(0, _ActivityResetPassword.this,
                        message);
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Unregister
        //EventBus.getDefault().unregister(this);
    }


    public void onEventMainThread(EventHeartOneSecond eventHeartOneSecond) {

        if (count > 0) {
            count--;
        }
        if (count > 0) {
            String format = getResources().getString(R.string.please_wait);
            String result = String.format(format,count);
            reset_password_valid_button.setText(result);
        } else {
            reset_password_valid_button.setText(getString(R.string.prompt_valid_button));
        }


    }

    public void onEventMainThread(EventRegistSubmitted eventRegistSubmitted) {
        finish();
    }


    private void smsResetPassword() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSmsResetPassword jsonSmsResetPassword = new JsonSmsResetPassword();
                jsonSmsResetPassword.phone = reset_password_phone.getText().toString();


                String jsonStr = gson.toJson(jsonSmsResetPassword, JsonSmsResetPassword.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SMS_RESET_PASSWORD, jsonStr);
                MyLog.d("", "apitest: send msg ret " + s);

                final JsonSmsResetPasswordRet jsonSmsResetPasswordRet = gson.fromJson(s, JsonSmsResetPasswordRet.class);
                if (jsonSmsResetPasswordRet != null) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (jsonSmsResetPasswordRet.code==MyConfig.retSuccess()) {
                                if (MyConfig.debug_autofill_validcode) {
                                    reset_password_code.setText(jsonSmsResetPasswordRet.data.code);
                                }

                                count = 60;
                                String format = getResources().getString(R.string.please_wait);
                                String result = String.format(format,count);
                                reset_password_valid_button.setText(result);
                            } else {
                                MyConfig.MyToast(0, _ActivityResetPassword.this,MyConfig.getErrorMsg(""+jsonSmsResetPasswordRet.code));
                            }
                        }
                    });

                }

            }
        }).start();
    }

}
