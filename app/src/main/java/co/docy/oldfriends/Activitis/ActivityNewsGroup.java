package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import co.docy.oldfriends.Adapters.ListAdapterNewsGroup;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonNewsGroupRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by khp on 2016/7/7.
 */
public class ActivityNewsGroup extends ActivityBase {
    RecyclerView news_listview;
    SwipyRefreshLayout swipyrefreshlayout;
    ListAdapterNewsGroup listAdapterNewsGroup;
    LinkedList<JsonNewsGroupRet.NewsGroupRet> list =new LinkedList();
    int group_Id;
    final int countPage = 20;
    boolean tag = false;
    JsonNewsGroupRet jsonNewsGroupRet = new  JsonNewsGroupRet();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_group);
        swipyrefreshlayout =(SwipyRefreshLayout) findViewById(R.id.activity_news_group_swipy);
        swipyrefreshlayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                tag = true;
                if (jsonNewsGroupRet.data != null && jsonNewsGroupRet.data.size() > 0) {
                    getNewsFromServer(group_Id, countPage, list.size() > 0 ? jsonNewsGroupRet.data.getLast().createdAt.toString() : null);
                }
                swipyrefreshlayout.setRefreshing(false);
            }
        });
        swipyrefreshlayout.setDirection(SwipyRefreshLayoutDirection.TOP);
        news_listview =(RecyclerView) findViewById(R.id.activity_news_group_listview);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        news_listview.setLayoutManager(layoutManager);
        setTitle(getIntent().getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME));
        group_Id = getIntent().getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID ,0);
        listAdapterNewsGroup = new ListAdapterNewsGroup(ActivityNewsGroup.this,list,null);
        news_listview.setAdapter(listAdapterNewsGroup);
        getNewsFromServer(group_Id,countPage,null);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getNewsFromServer(int id, int get_more_limit, String createdAt) {

        Call<JsonNewsGroupRet> repos = HttpTools.http_api_service().getGroupNews(id, createdAt, get_more_limit);
        repos.enqueue(new Callback<JsonNewsGroupRet>() {
            @Override
            public void onResponse(Call<JsonNewsGroupRet> call, Response<JsonNewsGroupRet> response) {

                swipyrefreshlayout.setRefreshing(false);

                if (response.code() == 200) {

                     jsonNewsGroupRet  = response.body();

                    if (jsonNewsGroupRet.data != null) {

                        list.addAll(list.size(), jsonNewsGroupRet.data);
                        listToArray(list);
                        listAdapterNewsGroup.notifyDataSetChanged();
                        if(tag) {
                            news_listview.scrollToPosition(countPage);
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonNewsGroupRet> call, Throwable t) {
                MyLog.d("", "retrofit: onFailure " + t.getMessage());
                swipyrefreshlayout.setRefreshing(false);
            }
        });

    }

    public void listToArray(LinkedList<JsonNewsGroupRet.NewsGroupRet> list_array){
        Collections.sort(list_array, new Comparator<JsonNewsGroupRet.NewsGroupRet>() {
            @Override
            public int compare(JsonNewsGroupRet.NewsGroupRet lhs, JsonNewsGroupRet.NewsGroupRet rhs) {
                return  (lhs).createdAt.compareTo(rhs.createdAt);
            }
        });
    }
}
