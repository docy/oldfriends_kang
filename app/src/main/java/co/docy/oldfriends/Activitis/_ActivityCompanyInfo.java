package co.docy.oldfriends.Activitis;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventHaveSetCurrentCompany;
import co.docy.oldfriends.Messages.JsonCompanyGroupListRet;
import co.docy.oldfriends.Messages.JsonDeleteCompany;
import co.docy.oldfriends.Messages.JsonDeleteCompanyRet;
import co.docy.oldfriends.Messages.JsonGetCompanyInfoRet;
import co.docy.oldfriends.Messages.JsonJoinCompany;
import co.docy.oldfriends.Messages.JsonLeaveCompany;
import co.docy.oldfriends.Messages.JsonLeaveCompanyRet;
import co.docy.oldfriends.Messages.JsonSetCompanyLogo;
import co.docy.oldfriends.Messages.JsonSetCompanyLogoRet;
import co.docy.oldfriends.Messages.JsonSetCurrentCompany;
import co.docy.oldfriends.Messages.JsonUploadFileRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;


public class _ActivityCompanyInfo extends ActivityBase implements View.OnClickListener {

    private int company_id, userCount, groupCount;
    private int creatorId;
    private String creatorName;
    private static boolean joined;
    private ImageView company_setting_photo, company_setting_chang_picture;
    private TextView company_setting_groupleader_name, company_setting_groupmemb_count, company_setting_group_count,
            company_setting_name, company_setting_desc, company_setting_address, company_setting_type;
    private LinearLayout company_setting_member_info, company_setting_allgroup,
            company_setting_inviteCode_layout, company_setting_allgroup_num;
    private Button company_setting_quit, company_setting_transfer, company_setting_delete;
    private Context mContext = this;
    public LinkedList<JsonCompanyGroupListRet.CompanyGroupListRet> list_group = new LinkedList<>();
    private CheckBox company_setting_inner_switch;
    private MyConfig.IOnOkClickListener onOkClickListener;
    private RelativeLayout company_setting_name_layout, company_setting_desc_layout;
    private MyConfig.IF_AfterHttpRequest IFAfterHttpRequest = new MyConfig.IF_AfterHttpRequest() {
        @Override
        public void doAfterHttpRequest() {
//            MyConfig.jsonGetCompanyUserListRet_temp = MyConfig.jsonGetCompanyUserListRet;
            Intent newIntent = new Intent(mContext, _ActivityChooseCompanyOwner.class);
            newIntent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, company_id);
            startActivity(newIntent);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_info);
        company_setting_address = (TextView) findViewById(R.id.company_setting_address);
        company_setting_type = (TextView) findViewById(R.id.company_setting_type);

        Intent intent = getIntent();
        company_id = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, -1);
        creatorId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORID, -1);
        joined = intent.getBooleanExtra(MyConfig.CONSTANT_PARAMETER_STRING_JOINED, true);
        creatorName = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORNICK);
        userCount = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_USERCOUNT, 0);
        groupCount = intent.getIntExtra(MyConfig.CONST_STRING_PARAM_GROUPCOUNT, 0);
        MyLog.d("", "MyConfig.CONST_STRING_PARAM_GROUPCOUNT" + groupCount);
        MyLog.d("", "ActivityCompanySetting " + company_id + " " + creatorId + " " + MyConfig.usr_id);

        company_setting_group_count = (TextView) findViewById(R.id.company_setting_group_count);
        company_setting_group_count.setText(groupCount + getResources().getString(R.string.ge));

        company_setting_inner_switch = (CheckBox) findViewById(R.id.company_setting_inner_switch);
        company_setting_inner_switch.setVisibility(View.GONE);
        company_setting_inviteCode_layout = (LinearLayout) findViewById(R.id.company_setting_invite_code_layout);
        company_setting_inviteCode_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_invite_code = new Intent(_ActivityCompanyInfo.this, _ActivityGenerateCompanyInviteCode.class);
                intent_invite_code.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, company_id);
                startActivity(intent_invite_code);
            }
        });

        if (creatorId == MyConfig.usr_id) {
            company_setting_inviteCode_layout.setVisibility(View.VISIBLE);
            company_setting_inner_switch.setEnabled(true);
        } else {
            company_setting_inviteCode_layout.setVisibility(View.GONE);
            company_setting_inner_switch.setEnabled(false);
        }

        company_setting_photo = (ImageView) findViewById(R.id.company_setting_photo);
        company_setting_name = (TextView) findViewById(R.id.company_setting_name);
        company_setting_name_layout = (RelativeLayout) findViewById(R.id.company_setting_name_layout);
        company_setting_desc = (TextView) findViewById(R.id.company_setting_desc);
        company_setting_desc_layout = (RelativeLayout) findViewById(R.id.company_setting_desc_layout);
        company_setting_quit = (Button) findViewById(R.id.company_setting_quit);
        company_setting_transfer = (Button) findViewById(R.id.company_setting_transfer);
        company_setting_delete = (Button) findViewById(R.id.company_setting_delete);

        company_setting_allgroup_num = (LinearLayout) findViewById(R.id.company_setting_allgroup_num);
        company_setting_allgroup_num.setVisibility(View.VISIBLE);

        company_setting_allgroup = (LinearLayout) findViewById(R.id.company_setting_allgroup);
        company_setting_allgroup.setVisibility(View.GONE);

        company_setting_member_info = (LinearLayout) findViewById(R.id.company_setting_member_info);
        company_setting_groupmemb_count = (TextView) findViewById(R.id.company_setting_groupmemb_count);
        company_setting_groupleader_name = (TextView) findViewById(R.id.company_setting_groupleader_name);
        company_setting_chang_picture = (ImageView) findViewById(R.id.company_setting_chang_picture);

        company_setting_groupleader_name.setText(creatorName);

        if (MyConfig.usr_id == creatorId) {
            company_setting_quit.setVisibility(View.GONE);
            company_setting_transfer.setVisibility(View.VISIBLE);
            company_setting_delete.setVisibility(View.VISIBLE);
            company_setting_chang_picture.setVisibility(View.VISIBLE);
        } else {
            company_setting_quit.setVisibility(View.VISIBLE);
            company_setting_chang_picture.setVisibility(View.GONE);
            company_setting_transfer.setVisibility(View.GONE);
            company_setting_delete.setVisibility(View.GONE);
            if (joined) {
                company_setting_quit.setText(getString(R.string.exitcompany_exit));
                setTitle(getResources().getString(R.string.company_set));
            } else {
                company_setting_quit.setText(getString(R.string.join_company));
                setTitle(getResources().getString(R.string.company_info));
            }
        }
        company_setting_allgroup.setOnClickListener(this);
        company_setting_quit.setOnClickListener(this);
        company_setting_transfer.setOnClickListener(this);
        company_setting_delete.setOnClickListener(this);

    }

    private void setUpdateClickListener() {
        company_setting_name_layout.setOnClickListener(this);
        company_setting_desc_layout.setOnClickListener(this);
        company_setting_photo.setOnClickListener(this);
//        company_setting_name_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                updateCompanyInfo(MyConfig.CONSTANT_PARAMETER_STRING_NAME);
//            }
//        });

//        company_setting_desc_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                updateCompanyInfo(MyConfig.CONSTANT_PARAMETER_STRING_DESC);
//            }
//        });

//        company_setting_photo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                intent.addCategory(Intent.CATEGORY_OPENABLE);
//                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
//                intent.setType(MyConfig.MIME_IMAGE_STAR);
//
//                if (intent.resolveActivity(ActivityCompanySetting.this.getPackageManager()) != null) {
//                    startActivityForResult(intent, MyConfig.REQUEST_GALLERY_AVATAR);
//                }
//
//            }
//        });

    }


    private void updateCompanyInfo(String itemName) {


        Intent intent = new Intent(this, _ActivityUpdateCompanyInfo.class);
        /**
         * 目前的UI只能一次修改一项，故只有这个被显示
         * 或以后全部显示
         */
        intent.putExtra("itemName", itemName);
        startActivityForResult(intent, MyConfig.REQUEST_COMPANY_UPDATE);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        company_id = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, -1);
        creatorId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORID, -1);
        creatorName = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORNICK);
        groupCount = intent.getIntExtra(MyConfig.CONST_STRING_PARAM_GROUPCOUNT, -1);
        company_setting_groupleader_name.setText(creatorName);
        company_setting_group_count.setText(groupCount +"");
        if (MyConfig.usr_id == creatorId) {
            company_setting_quit.setVisibility(View.GONE);
            company_setting_chang_picture.setVisibility(View.VISIBLE);
            company_setting_name_layout.setEnabled(true);
            company_setting_desc_layout.setEnabled(true);
            company_setting_photo.setEnabled(true);
            company_setting_inviteCode_layout.setVisibility(View.VISIBLE);
            company_setting_inner_switch.setEnabled(true);


        } else {
            company_setting_quit.setVisibility(View.VISIBLE);
            company_setting_chang_picture.setVisibility(View.GONE);
            company_setting_transfer.setVisibility(View.GONE);
            company_setting_delete.setVisibility(View.GONE);
            company_setting_name_layout.setEnabled(false);
            company_setting_desc_layout.setEnabled(false);
            company_setting_photo.setEnabled(false);
            company_setting_inner_switch.setEnabled(false);
            company_setting_inner_switch.setVisibility(View.GONE);
            company_setting_inviteCode_layout.setVisibility(View.GONE);
            if (joined) {
                company_setting_quit.setText(getString(R.string.exitcompany_exit));
                //company_setting_member_info.setVisibility(View.GONE);
                setTitle(getString(R.string.company_set));
            } else {
                company_setting_quit.setText(getString(R.string.join_company));
                //company_setting_member_info.setVisibility(View.VISIBLE);
                setTitle(getString(R.string.company_info));
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getCompanyInfo();
    }

    private void getCompanyInfo() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();

                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_INFO + company_id, MyConfig.usr_token);
                MyLog.d("", "OKHTTP: company " + company_id + " company info: " + s);

                MyConfig.jsonGetCompanyInfoRet = gson.fromJson(s, JsonGetCompanyInfoRet.class);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (MyConfig.jsonGetCompanyInfoRet != null && MyConfig.jsonGetCompanyInfoRet.code==MyConfig.retSuccess()) {
                            Picasso.with(_ActivityCompanyInfo.this).load(MyConfig.getApiDomain_NoSlash_GetResources() + MyConfig.jsonGetCompanyInfoRet.data.logoUrlOrigin)
                                    //  .fit()
                                    .resize(MyConfig.getWidthByScreenPercent(100), MyConfig.getPxFromDimen(_ActivityCompanyInfo.this, R.dimen.companyviews_height))
                                    .centerCrop()
                                    .into(company_setting_photo);

                            company_setting_desc.setText(MyConfig.jsonGetCompanyInfoRet.data.desc);
                            company_setting_name.setText(MyConfig.jsonGetCompanyInfoRet.data.name);
                            company_setting_groupmemb_count.setText(String.valueOf(userCount));
                            company_setting_groupleader_name.setText(creatorName);
                            company_setting_group_count.setText(String.valueOf(groupCount));
                            // company_setting_group_count.setText(MyConfig.jsonGetCompanyInfoRet.data.groupCount+"个") ;
                            company_setting_inner_switch.setChecked(MyConfig.jsonGetCompanyInfoRet.data.isPrivate);
                            if (null == MyConfig.jsonGetCompanyInfoRet.data.city) {
                                company_setting_address.setText(getResources().getString(R.string.unknow));
                            } else {
                                company_setting_address.setText(MyConfig.jsonGetCompanyInfoRet.data.city);
                            }
                            if (MyConfig.jsonGetCompanyInfoRet.data.category < MyConfig.categorylist.length) {
                                company_setting_type.setText(MyConfig.categorylist[MyConfig.jsonGetCompanyInfoRet.data.category]);
                            }
                            if (MyConfig.usr_id == MyConfig.jsonGetCompanyInfoRet.data.creator) {
                                setUpdateClickListener();
                            }


                        }
                    }
                });

            }
        }).start();

    }

    private void getCompanyMenberCount() {

    }

    private void getCompanyGroupList() {


        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();

                /**
                 * 获取可以加入，但还没有加入的rooms
                 */
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_GROUP_LIST + company_id + MyConfig.API2_COMPANY_GROUP_LIST_2, MyConfig.usr_token);
                MyLog.d("", "getCompanyGroupList ret " + s);
                JsonCompanyGroupListRet jsonCompanyGroupListRet = gson.fromJson(s, JsonCompanyGroupListRet.class);
                if (jsonCompanyGroupListRet != null && jsonCompanyGroupListRet.code==MyConfig.retSuccess()) {
                    list_group = jsonCompanyGroupListRet.data;
//                            list_group = jsonCompanyGroupListRet.data;
//                            adapter = new ListAdapterCompanyGroups(ActivityCompanyGroupList.this, list_group, null, null);
//                            activity_company_group_list.setAdapter(adapter);

                }

            }
        }).start();

    }

    public void DeleteCompany(final JsonDeleteCompany jsonDeleteComapny) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonDeleteComapny, JsonDeleteCompany.class);
                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_DELETE_COMPANY +
                        jsonDeleteComapny.companyId, jsonStr);
                final JsonDeleteCompanyRet jsonDeleteCompanyRet = gson.fromJson(s, JsonDeleteCompanyRet.class);
                MyLog.d("", "HttpTools: JsonDeleteCompanyRet " + s);
                if (jsonDeleteCompanyRet != null && jsonDeleteCompanyRet.code==MyConfig.retSuccess()) {
                    if (MyConfig.activityMainGroup != null) {
                        MyConfig.activityMainGroup.finish();
                    }
                    Intent intent = new Intent(mContext, _ActivityCompanyView.class);
                    startActivity(intent);
                }

            }
        }).start();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyLog.d("", "onActivityResult hit fragment! request code " + requestCode + " result code " + resultCode);
        if (requestCode == MyConfig.REQUEST_GALLERY_AVATAR && resultCode == Activity.RESULT_OK) {

            Intent innerIntent = new Intent("com.android.camera.action.CROP");
            innerIntent.setDataAndType(data.getData(), MyConfig.MIME_IMAGE_STAR);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true");// 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
            if (innerIntent.resolveActivity(_ActivityCompanyInfo.this.getPackageManager()) != null) {
                startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);
            }


//            String realPath = MyConfig.getUriPath(getActivity(), fileUri);
//            File tempAttach = new File(realPath);
//
//            uploadUserAvatar(tempAttach);
        }

        if (requestCode == MyConfig.REQUEST_IMAGE_CROP && resultCode == Activity.RESULT_OK) {

            Bitmap bitmap = data.getParcelableExtra("data");

            File tempAttach = new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg");

            // 图像保存到文件中
            FileOutputStream foutput = null;
            try {
                foutput = new FileOutputStream(tempAttach);
                if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, foutput)) {
//                    Toast.makeText(MyConfig.app,
//                            "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath(),
//                            Toast.LENGTH_LONG).show();
                    MyConfig.MyToast(-1, MyConfig.app,
                            getResources().getString(R.string.feil_exsit)+ tempAttach.getAbsolutePath());
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            uploadAvatar(tempAttach);

        }

    }

    public void uploadAvatar(final File f) {


        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonUploadFileRet jsonUploadFileRet = HttpTools.okhttpUploadFile(f, "image/jpeg");

                if (jsonUploadFileRet != null && jsonUploadFileRet.code==MyConfig.retSuccess()) {
                    final String path = jsonUploadFileRet.data.path;

                    JsonSetCompanyLogo jsonSetCompanyLogo = new JsonSetCompanyLogo();
                    jsonSetCompanyLogo.accessToken = MyConfig.usr_token;
                    jsonSetCompanyLogo.logo = path;

                    Gson gson = new Gson();
                    String jsonStr = gson.toJson(jsonSetCompanyLogo, JsonSetCompanyLogo.class);

                    final String ss = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_SETLOGO + company_id + MyConfig.API2_COMPANY_SETLOGO_2, jsonStr);

                    final JsonSetCompanyLogoRet jsonSetCompanyLogoRet = gson.fromJson(ss, JsonSetCompanyLogoRet.class);
                    MyLog.d("", "OKHTTP set avatar: " + ss);
                    if (jsonSetCompanyLogoRet != null && jsonSetCompanyLogoRet.code==MyConfig.retSuccess()) {


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Picasso.with(_ActivityCompanyInfo.this).load(MyConfig.getApiDomain_NoSlash_GetResources() + path)
                                        //  .fit()
                                        .resize(MyConfig.getWidthByScreenPercent(100), MyConfig.getPxFromDimen(_ActivityCompanyInfo.this, R.dimen.companyviews_height))
                                        .centerCrop()
                                        .into(company_setting_photo);
                            }
                        });

                    }

//                    Snackbar.make(getView(), jsonSetAvatarRet.message, Snackbar.LENGTH_LONG).show();
//                    MyConfig.snackbarTop(getView(), jsonSetGroupLogoRet.message, R.color.Red, Snackbar.LENGTH_SHORT);

                } else {
//                    Snackbar.make(getView(), "更换头像失败", Snackbar.LENGTH_LONG).show();
//                    MyConfig.snackbarTop(getView(), "更换头像失败", R.color.Red, Snackbar.LENGTH_SHORT);
                    //更换头像失败

                }

            }
        }).start();
    }

    public void leaveCompany(final JsonLeaveCompany jsonLeaveCompany, final Context context) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonLeaveCompany, JsonLeaveCompany.class);
                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_LEAVE + jsonLeaveCompany.companyId + MyConfig.API2_LEAVE_COMPANY_2, jsonStr);
                final JsonLeaveCompanyRet jsonLeaveCompanyRet = gson.fromJson(s, JsonLeaveCompanyRet.class);
                MyLog.d("", "HttpTools: JsonLeaveRet " + s);
                if (jsonLeaveCompanyRet != null && jsonLeaveCompanyRet.code==MyConfig.retSuccess()) {
                    if (MyConfig.activityMainGroup != null) {
                        MyConfig.activityMainGroup.finish();
                    }
                    Intent intent = new Intent(mContext, _ActivityCompanyView.class);
                    mContext.startActivity(intent);
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showRemindDialog("", jsonLeaveCompanyRet.message, context, company_id);
                        }
                    });
                }

            }
        }).start();

    }

    /**
     * 弹出解散集体对话框
     */
    public void showRemindDialog(String title, final String remindstr, final Context mContext, final int companyId) {
        View dialogView = LayoutInflater.from(mContext).inflate(
                R.layout.dialog_exit_remind_layout, null);
        final Dialog setDialog = new Dialog(mContext, R.style.DialogStyle);
        setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialog.setCancelable(false);
        setDialog.getWindow().setContentView(dialogView);
        WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
        lp.width = MyConfig.getWidthByScreenPercent(80);
        setDialog.getWindow().setAttributes(lp);
        final Button cancleButton = (Button) dialogView.findViewById(R.id.cancle);
        Button loginButton = (Button) dialogView.findViewById(R.id.login);
        TextView remindText = (TextView) dialogView.findViewById(R.id.remind_text);
        remindText.setText(remindstr);
        TextView titletext = (TextView) dialogView.findViewById(R.id.remind_title);
        titletext.setText(title);
        final boolean istransfer = (title.length() > 0) ? true : false;
        titletext.setVisibility(istransfer ? View.VISIBLE : View.GONE);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (istransfer) {
                    JsonDeleteCompany jsonDeleteCompany = new JsonDeleteCompany();
                    jsonDeleteCompany.accessToken = MyConfig.usr_token;
                    jsonDeleteCompany.companyId = companyId;
                    DeleteCompany(jsonDeleteCompany);
                }
                setDialog.cancel();
            }

        });
        cancleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialog.cancel();
            }
        });
        setDialog.setCanceledOnTouchOutside(false);
        setDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.company_setting_allgroup:
                Intent intent_groups = new Intent(_ActivityCompanyInfo.this, _ActivityCompanyGroupList.class);
                intent_groups.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, company_id);
                startActivityForResult(intent_groups, MyConfig.REQUEST_COMPANY_GROUPS);
                break;
            case R.id.company_setting_quit:
                if (joined) {
                    onOkClickListener = new MyConfig.IOnOkClickListener() {
                        @Override
                        public void OnOkClickListener() {
                            JsonLeaveCompany jsonLeaveCompany = new JsonLeaveCompany();
                            jsonLeaveCompany.accessToken = MyConfig.usr_token;
                            jsonLeaveCompany.companyId = company_id;
                            leaveCompany(jsonLeaveCompany, mContext);
                            getCompanyGroupList();
                        }
                    };
                    MyConfig.showRemindDialog("", getResources().getString(R.string.sure_exit), mContext, true, onOkClickListener);

                } else {
                    JsonJoinCompany jsonJoinCompany = new JsonJoinCompany();
                    jsonJoinCompany.accessToken = MyConfig.usr_token;
                    jsonJoinCompany.id = company_id;
                    MyLog.d("", "HttpTools: joinCompany: ");
                    MyConfig.joinCompany(jsonJoinCompany);
                    JsonSetCurrentCompany jsonSetCurrentCompany = new JsonSetCurrentCompany(company_id);
                    MyConfig.setCurrentCompany(jsonSetCurrentCompany);
                }
                break;
            case R.id.company_setting_delete:
                showRemindDialog(getResources().getString(R.string.exit_delete_group), getResources().getString(R.string.exit_deletegroup_remind), mContext, company_id);
                break;
            case R.id.company_setting_transfer:
                MyConfig.getCurrentYearUserAddressBooks(company_id, IFAfterHttpRequest);
                break;
            case R.id.company_setting_name_layout:
                updateCompanyInfo(MyConfig.CONSTANT_PARAMETER_STRING_NAME);
                break;
            case R.id.company_setting_desc_layout:
                updateCompanyInfo(MyConfig.CONSTANT_PARAMETER_STRING_DESC);
                break;
            case R.id.company_setting_photo:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                intent.setType(MyConfig.MIME_IMAGE_STAR);
                if (intent.resolveActivity(_ActivityCompanyInfo.this.getPackageManager()) != null) {
                    startActivityForResult(intent, MyConfig.REQUEST_GALLERY_AVATAR);
                }
                break;
        }
    }

    public void onEventMainThread(EventHaveSetCurrentCompany eventHaveSetCurrentCompany) {
        if (eventHaveSetCurrentCompany.success) {
            finish();
        }
    }

}

