package co.docy.oldfriends.Activitis;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventAppQuit;
import co.docy.oldfriends.EventBus.EventLogin;
import co.docy.oldfriends.EventBus.EventRegistSuccess;
import co.docy.oldfriends.Messages.JsonLogin;
import co.docy.oldfriends.Messages.JsonWeixinLoginRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;


public class ActivityLoginTongChuang extends ActivityBase {

//    FrameLayout login_framelayout;
//    RelativeLayout relativeLayout;
    LinearLayout linearLayout;
    ImageView login_frant_img;
//    ImageView iv_logo;
    Button bSend;
    EditText et_phone;
    EditText et_pwd;
    TextView login_weixin;
    Button login_goto_regist;
    Button login_forget_password;

    /**
     * if 0, no auto login
     * if 1, auto login
     * if -1, according to usr_status
     */
    int login_cmd;
    String auto_login_phone;
    String auto_login_pwd;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.child_activity_name = this.getClass().getName();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_tongchuang);

        MyLog.d("", "start ActivityLogin onCreate");

        MyConfig.activityLoginTongChuang = this;

        login_cmd = getIntent().getIntExtra(MyConfig.TAG_LOGIN_CMD, -1);
        MyLog.d("", "login_cmd " + login_cmd);

//        login_framelayout = (FrameLayout)findViewById(R.id.login_framelayout);
//        login_framelayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                MyConfig.hide_keyboard_must_call_from_activity(ActivityLoginTongChuang.this);
//            }
//        });
//        login_framelayout.setSoundEffectsEnabled(false);

        linearLayout = (LinearLayout) findViewById(R.id.login_layout);
//        relativeLayout.getLayoutParams().width = MyConfig.screenWidth * 3 / 4;
//        relativeLayout.getLayoutParams().height = MyConfig.screenHeight * 2 / 3;
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConfig.hide_keyboard_must_call_from_activity(ActivityLoginTongChuang.this);
            }
        });
        linearLayout.setSoundEffectsEnabled(false);

//        iv_logo = (ImageView) findViewById(R.id.login_logo);
//        iv_logo.getLayoutParams().width = MyConfig.screenWidth / 4;
//        iv_logo.getLayoutParams().height = MyConfig.screenWidth / 4;
//        iv_logo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                MyConfig.hide_keyboard_must_call_from_activity(ActivityLoginTongChuang.this);
//                return;
//            }
//        });
//        iv_logo.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//
//                MyLog.d("", "long click and single click: long");
//
//                if (et_phone.getText().toString().endsWith("set")) {
//                    Intent intent_advance_setting = new Intent(ActivityLoginTongChuang.this, ActivityAdvanceSetting.class);
//                    startActivity(intent_advance_setting);
//                }
//                return true;
//            }
//        });
//        iv_logo.setSoundEffectsEnabled(false);

        login_frant_img = (ImageView)findViewById(R.id.login_front_img);
        login_frant_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConfig.hide_keyboard_must_call_from_activity(ActivityLoginTongChuang.this);
            }
        });

        et_phone = (EditText) findViewById(R.id.login_phone);
        et_pwd = (EditText) findViewById(R.id.login_pwd);

        bSend = (Button) findViewById(R.id.login_send);
        bSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bSend.setEnabled(false);
                dialog.setMessage("正在登录...");
                dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                dialog.show();
                loginRequest();

            }
        });


        login_goto_regist = (Button) findViewById(R.id.login_goto_regist);
        login_goto_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_regist = new Intent(ActivityLoginTongChuang.this, ActivityNoDutyState.class);
                startActivity(intent_regist);
            }
        });

        login_forget_password = (Button) findViewById(R.id.login_foget_password);
        login_forget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pwd = et_pwd.getText().toString().trim();
                if(pwd.equals("set")){
                    Intent intent_advance_setting = new Intent(ActivityLoginTongChuang.this, ActivityAdvanceSetting.class);
                    startActivity(intent_advance_setting);
                }else {
                    Intent intent_regist = new Intent(ActivityLoginTongChuang.this, ActivityResetPasswordTongChuang.class);
                    startActivity(intent_regist);
                }
//                finish();
            }
        });


//
//
//        logout_weixin = (Button) findViewById(R.id.logout_weixin);
////        logout_weixin.setVisibility(View.VISIBLE);
//        logout_weixin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // 添加微信平台
//                UMWXHandler wxHandler = new UMWXHandler(ActivityLoginTongChuang.this, MyConfig.third_weixin_share_appID, MyConfig.third_weixin_share_appSecret);
//                wxHandler.addToSocialSDK();
//
//                // 首先在您的Activity中添加如下成员变量
//                final UMSocialService mController = UMServiceFactory.getUMSocialService("com.umeng.share");
//
//                mController.deleteOauth(ActivityLoginTongChuang.this, SHARE_MEDIA.WEIXIN,
//                        new SocializeListeners.SocializeClientListener() {
//                            @Override
//                            public void onStart() {
//                            }
//                            @Override
//                            public void onComplete(int status, SocializeEntity entity) {
//                                if (status == 200) {
//                                    Toast.makeText(ActivityLoginTongChuang.this, "删除成功.",
//                                            Toast.LENGTH_SHORT).show();
//                                } else {
//                                    Toast.makeText(ActivityLoginTongChuang.this, "删除失败",
//                                            Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        });
//            }
//        });
        login_weixin = (TextView) findViewById(R.id.login_weixin);
//        login_weixin.setVisibility(View.VISIBLE);
        login_weixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MyConfig.isWeixinAvilible(ActivityLoginTongChuang.this)) {
                    MyConfig.loginWeixin(ActivityLoginTongChuang.this);
                }else{
                    MyConfig.MyToast(1, ActivityLoginTongChuang.this, "请先安装微信");
                }
            }
        });


        dialog = new ProgressDialog(ActivityLoginTongChuang.this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        MyLog.d("", "ActivityLogin onResume");

        /**
         * load user data
         */
        if(MyConfig.prefShared != null) {
            auto_login_phone = MyConfig.prefShared.getString(MyConfig.PREF_USER_PHONE, "");
            et_phone.setText(auto_login_phone);
//            et_phone.setSelection(auto_login_phone.length());
            auto_login_pwd = MyConfig.prefShared.getString(MyConfig.PREF_USER_PWD, "");
            MyLog.d("","updateUserInforFromLogin autologin pwd:" + auto_login_pwd);
            et_pwd.setText(auto_login_pwd);
//            et_pwd.setSelection(auto_login_pwd.length());
        }

        switch (login_cmd) {
            case -1:
                autoLoginAccordingToUsrStatus();
                break;
            case 0:
                break;
            case 1:
                loginIfHavePwd();
                break;
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        MyConfig.activityLoginTongChuang = null;

    }

    /**
     * 注册成功之后关闭登录页面
     * @param eventRegistSuccess
     */
    public void onEventMainThread(EventRegistSuccess eventRegistSuccess) {
        finish();
    }
    private void autoLoginAccordingToUsrStatus() {
        /**
         * app可能意外重启等等，MyConfig.usr_status是不可靠的，只有preference里面保存的才是可靠的
         */
//        MyConfig.usr_status = MyConfig.prefShared.getInt(MyConfig.PREF_USR_STATUS, -1);
        MyConfig.loadUserDataFromPreference(this);
        MyConfig.getOraderDataToSharedPreferences();

        MyLog.d("", "autologin in resume " + MyConfig.usr_status);
        if (MyConfig.usr_status > MyConfig.USR_STATUS_LOGOUT) {

            MyLog.d("", "autologin: login()");

            loginIfHavePwd();
        }
    }

    private void loginIfHavePwd() {
        if (auto_login_phone != null && auto_login_pwd != null) {
            loginRequest();
        }
    }
    String login_name_or_phone,pwd;
    public void loginRequest() {
         login_name_or_phone = et_phone.getText().toString();
         pwd = et_pwd.getText().toString();

        if (login_name_or_phone.length() > 0 && pwd.length() > 0) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                    JsonLogin jsonLogin = new JsonLogin();
                    jsonLogin.account = login_name_or_phone;
                    jsonLogin.password = pwd;
                    jsonLogin.os = MyConfig.os_string;
                    jsonLogin.uuid = MyConfig.uuid;

                    Gson gson = new Gson();
                    String j = gson.toJson(jsonLogin);

                    String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LOGIN, j);
                    MyLog.d("查看登陆返回的数据",s);
                    final JsonWeixinLoginRet jsonLoginRet = gson.fromJson(s, JsonWeixinLoginRet.class);

                    if (jsonLoginRet != null) {

                        if (jsonLoginRet.code != MyConfig.retSuccess()) {

                            /**
                             * 密码错了就清除保存的密码，并保持在这个位置
                             */
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //  et_pwd.setText("");
//                                MyConfig.MyToast(0,ActivityLogin.this,getString(R.string.error_login));
                                    MyConfig.MyToast(0, ActivityLoginTongChuang.this, MyConfig.getErrorMsg("" + jsonLoginRet.code));
                                    bSend.setEnabled(true);
                                    dialog.dismiss();
                                }
                            });

                            MyConfig.usr_pwd = "";

                            MyConfig.saveUserDataToPreference(ActivityLoginTongChuang.this);

                        } else if (jsonLoginRet.data != null) {

                            MyLog.d("","app start steps 3, usr_status:" +MyConfig.usr_status);
                            MyConfig.updateUserInforFromLogin(ActivityLoginTongChuang.this, jsonLogin.password, s, jsonLoginRet);
                            MyLog.d("","app start steps 4, usr_status:" +MyConfig.usr_status);
                            MyConfig.getDataByOrder();
                            finishWithSuccess();
                        }

                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                            Toast.makeText(ActivityLogin.this, getString(R.string.net_msg_not_connected), Toast.LENGTH_SHORT).show();
                                MyConfig.MyToast(0, ActivityLoginTongChuang.this, getString(R.string.net_msg_not_connected));
                                bSend.setEnabled(true);
                                dialog.dismiss();
                            }
                        });
                    }
            }

            }).start();
        }else {
            Toast.makeText(ActivityLoginTongChuang.this,"用户名或密码不能为空",Toast.LENGTH_SHORT).show();
            bSend.setEnabled(true);
            dialog.dismiss();
        }

    }

    private void finishWithSuccess() {

        EventBus.getDefault().post(new EventLogin());

        MyLog.d("","login failed: 1");

        setResult(RESULT_OK, new Intent());
        startActivity(new Intent(this,ActivityViewPager.class));
        MyLog.d("","login failed: 2");
        dialog.dismiss();
        finish();
    }

    private void finishWithFailure() {
        setResult(RESULT_CANCELED, new Intent());
        finish();
    }

    private boolean backPressed = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            Toast.makeText(this, getResources().getString(R.string.sys_quit_notice), Toast.LENGTH_SHORT).show();
            MyConfig.MyToast(-1, this, getResources().getString(R.string.sys_quit_notice));

            if (backPressed == true) {
//                MyConfig.activityViewPager.finish();
//                finish();
                EventBus.getDefault().post(new EventAppQuit());
            } else {
                backPressed = true;
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        backPressed = false;
                    }
                }, 2000);

            }
            return true; // 让系统不再处理这次回退键，因为系统收到回退键后会立即把app关了
        }
        return super.onKeyDown(keyCode, event);
    }
}
