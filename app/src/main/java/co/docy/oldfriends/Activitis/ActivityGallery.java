package co.docy.oldfriends.Activitis;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.R;
import co.docy.oldfriends.TouchGallery.GalleryWidget.GalleryViewPager;
import co.docy.oldfriends.TouchGallery.GalleryWidget.UrlPagerAdapter;

/**
 * Created by youhy on 5/11/15.
 */
public class ActivityGallery extends ActivityBase{


    int mode;

    /**
     *  当从主聊天室点进来
     *      显示聊天室所有显示的图片
     *
     *  当从子聊天室点进来
     *  当从favorite点进来
     *  当从topic列表点进来
     *      只显示子话题里面的图片
     *
     */

    private boolean flag_longclick_dialog_opened;//在longclick之后无法阻止click流程，这里用临时方法进行规避

    private GalleryViewPager mViewPager;
    ImageView gallery_menu;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        MyConfig.activityGallery = this;

        getSupportActionBar().hide();


        mode = getIntent().getIntExtra("mode", -1);


        gallery_menu = (ImageView) findViewById(R.id.gallery_menu);
        if(mode == MyConfig.GALLERY_MODE_PHOTOS_DOWNLOADED) {
            gallery_menu.setVisibility(View.VISIBLE);
            Picasso.with(this).load(R.drawable.gallery_menu).into(gallery_menu);
            gallery_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    new DialogFragment() {

                        @Override
                        public void onCreate(Bundle savedInstanceState) {
                            super.onCreate(savedInstanceState);
                            flag_longclick_dialog_opened = true;
                        }

                        @Override
                        public void onDestroy() {
                            super.onDestroy();
                            flag_longclick_dialog_opened = false;
                        }

                        @Override
                        public Dialog onCreateDialog(Bundle savedInstanceState) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                    getActivity());
                            builder.setTitle(getResources().getString(R.string.please_select))
                                    .setItems(R.array.gallery_image_long_click, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // The 'which' argument contains the index position
                                            // of the selected item
                                            int mode;
                                            switch (which) {
                                                case 0:
//                                                mode = MyConfig.FILE_DOWNLOAD_ONLY;
                                                    MyConfig.MyToast(0, ActivityGallery.this, getResources().getString(R.string.save_already));
                                                    MyConfig.saveViewToMediaStore(ActivityGallery.this, mViewPager.mCurrentView, MyConfig.iMsgGalleryRoot.ll_sub_msg_with_image.get(mViewPager.getCurrentItem()).filePath_inLocal, MyConfig.ATTACHMENT_ACTION_DO_NOTHING);
                                                    return;
//                                                break;
                                                case 1:
                                                    MyConfig.saveViewToMediaStore(ActivityGallery.this, mViewPager.mCurrentView, MyConfig.iMsgGalleryRoot.ll_sub_msg_with_image.get(mViewPager.getCurrentItem()).filePath_inLocal, MyConfig.ATTACHMENT_ACTION_DL_SHARE);
                                                    return;
                                                default:
                                                    return;
                                            }

                                        }
                                    })
                                    .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                            return builder.create();
                        }
                    }.show(getFragmentManager(), "");
                }
            });
        }else if(mode == MyConfig.GALLERY_MODE_GROUP_ALBUM){
            gallery_menu.setVisibility(View.GONE);
        }
        UrlPagerAdapter urlPagerAdapter;
        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add(getIntent().getStringExtra("url"));
        if(getIntent().getIntExtra("type",-1)==10){
             urlPagerAdapter = new UrlPagerAdapter(this, linkedList);
        }else {
            urlPagerAdapter = new UrlPagerAdapter(this, MyConfig.iMsgGalleryRoot.ll_sub_image_url);
        }
        urlPagerAdapter.setOnItemClickListener(new UrlPagerAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(View view) {
                if(!flag_longclick_dialog_opened){
                    finish();//单击关闭相册
                }
            }
        });
//        urlPagerAdapter.setOnItemLongClickListener(new UrlPagerAdapter.OnItemLongClickListener() {
//            @Override
//            public void onItemLongClicked(final View v, final int position, final String link) {
//
//                new DialogFragment() {
//
//                    @Override
//                    public void onCreate(Bundle savedInstanceState) {
//                        super.onCreate(savedInstanceState);
//                        flag_longclick_dialog_opened = true;
//                    }
//
//                    @Override
//                    public void onDestroy() {
//                        super.onDestroy();
//                        flag_longclick_dialog_opened = false;
//                    }
//
//                    @Override
//                    public Dialog onCreateDialog(Bundle savedInstanceState) {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(
//                                getActivity());
//                        builder.setTitle("请选择")
//                                .setItems(R.array.gallery_image_long_click, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        // The 'which' argument contains the index position
//                                        // of the selected item
//                                        switch (which) {
//                                            case 0:
//                                                MyConfig.saveViewToMediaStore(v, "我的图片", "来自小集体");
//                                                MyConfig.MyToast(0, ActivityGallery.this, "本张图片已经存入系统图库");
//                                                return;
//                                            case 1:
//                                                //假装是mainroom点击过来的，让mainroom去处理分享
//                                                new MyConfig.DownloadFileFromUrl(new MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo(ActivityGallery.class.getName(), MyConfig.FILE_DOWNLOAD_SHARE)).execute(
//                                                        link,//在服务器的地址
//                                                        MyConfig.iMsgGalleryRoot.ll_sub_msg_with_image.get(position).filePath_inLocal);//本地地址
//                                                MyLog.d("", "share out, DownloadFileFromUrl from gallery");
//                                                return;
//                                            default:
//                                                return;
//                                        }
//                                    }
//                                })
//                                .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//
//                                    }
//                                });
//                        return builder.create();
//                    }
//                }.show(getFragmentManager(), "");
//
//
//                return;
//            }
//        });

        mViewPager = (GalleryViewPager)findViewById(R.id.viewer);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(urlPagerAdapter);
        mViewPager.setCurrentItem(MyConfig.galleryViewStartAt);

    }





}