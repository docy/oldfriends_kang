package co.docy.oldfriends.Activitis;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners;
import com.umeng.socialize.weixin.controller.UMWXHandler;
import com.umeng.socialize.weixin.media.WeiXinShareContent;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventGroupListUpdateFromServer;
import co.docy.oldfriends.EventBus.EventHaveSendInviteCode;
import co.docy.oldfriends.Messages.JsonGenerateCompanyInviteCode;
import co.docy.oldfriends.Messages.JsonGenerateCompanyInviteCodeRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;


public class _ActivityGenerateCompanyInviteCode extends ActivityBase {//todo

    int companyId;

    int generate_count;
    String invite_code;

    TextView generate_invite_code;
    TextView generate_invite_code_refresh;
    LinearLayout generate_invite_code_sms;
    LinearLayout generate_invite_code_weixin;
    LinearLayout generate_invite_code_clipboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_company_invite_code);

        companyId = getIntent().getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, -1);

        //EventBus.getDefault().register(this);

        generate_invite_code = (TextView) findViewById(R.id.generate_invite_code);
        generate_invite_code_refresh = (TextView) findViewById(R.id.generate_invite_code_refresh);
        generate_invite_code_sms = (LinearLayout) findViewById(R.id.generate_invite_code_sms);
        generate_invite_code_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent_invite = new Intent(_ActivityGenerateCompanyInviteCode.this, _ActivityInviteFriendFromPhoneContact.class);
                intent_invite.putExtra("invite_code", invite_code);
                startActivityForResult(intent_invite, MyConfig.REQUEST_INVITE_FROM_PHONE_CONTACT);

            }
        });
        generate_invite_code_weixin = (LinearLayout) findViewById(R.id.generate_invite_code_weixin);
        generate_invite_code_weixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String title = getResources().getString(R.string.umen_share_weixin);
                String format = getString(R.string.invite_code_share);
                String result = String.format(format, MyConfig.usr_nickname, invite_code);
                String url = getResources().getString(R.string.your_url);
//                MyConfig.shareStringToWeixin(_ActivityGenerateCompanyInviteCode.this, title, result, url);

            }
        });
        generate_invite_code_clipboard = (LinearLayout) findViewById(R.id.generate_invite_code_clipboard);
        generate_invite_code_clipboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //拷贝文本到剪贴板
                ClipboardManager clipboard = (ClipboardManager)
                        getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("simple text",invite_code);
                clipboard.setPrimaryClip(clip);

                MyConfig.MyToast(0, _ActivityGenerateCompanyInviteCode.this, getResources().getString(R.string.your_invite_code)+invite_code+getResources().getString(R.string.already_copy));
            }
        });

        generate_invite_code_refresh = (TextView) findViewById(R.id.generate_invite_code_refresh);
        generate_invite_code_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getInviteCodeFromServer();
            }
        });

        generate_count = 0;
        getInviteCodeFromServer();


    }


    public void getInviteCodeFromServer() {
        generate_count++;

        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonGenerateCompanyInviteCode jsonGenerateCompanyInviteCode = new JsonGenerateCompanyInviteCode();
                jsonGenerateCompanyInviteCode.accessToken = MyConfig.usr_token;
                jsonGenerateCompanyInviteCode.id = companyId;
                jsonGenerateCompanyInviteCode.target = "13800138000";

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonGenerateCompanyInviteCode, JsonGenerateCompanyInviteCode.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GENERATE_COMPANY_INVITE_CODE + jsonGenerateCompanyInviteCode.id + MyConfig.API2_GENERATE_COMPANY_INVITE_CODE_2, jsonStr);

                final JsonGenerateCompanyInviteCodeRet jsonGenerateCompanyInviteCodeRet = gson.fromJson(s, JsonGenerateCompanyInviteCodeRet.class);
                MyLog.d("", "HttpTools: JsonJoinRet " + s);
                if (jsonGenerateCompanyInviteCodeRet != null && jsonGenerateCompanyInviteCodeRet.code==MyConfig.retSuccess()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            invite_code = jsonGenerateCompanyInviteCodeRet.data.code;
                            generate_invite_code.setText(jsonGenerateCompanyInviteCodeRet.data.code);
//                            generate_invite_code.setText("您的邀请码是: " + jsonGenerateCompanyInviteCodeRet.data.code);

                        }
                    });
                }


                EventBus.getDefault().post(new EventGroupListUpdateFromServer());
            }
        }).start();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //EventBus.getDefault().unregister(this);
    }


    private void finishWithSuccess() {


        setResult(RESULT_OK, new Intent());
        finish();
    }

    private void finishWithFailure() {
        setResult(RESULT_CANCELED, new Intent());
        finish();
    }

    public void onEventMainThread(EventHaveSendInviteCode eventHaveSendInviteCode) {
        finish();
    }
}
