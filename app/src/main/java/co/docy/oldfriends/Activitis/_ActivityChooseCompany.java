package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import co.docy.oldfriends.R;


public class _ActivityChooseCompany extends ActivityBase {

    /**
     * 这是以前的选择当前公司页面，暂时未用
     * 调试页面中打开viewpager右上角的调试按钮，点击选择集体来抵达本页
     *
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_company);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_info, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                Toast.makeText(this, "home key is pressed, input is discarded", Toast.LENGTH_LONG).show();

                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
