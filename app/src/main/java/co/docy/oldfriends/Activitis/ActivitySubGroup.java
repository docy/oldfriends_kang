package co.docy.oldfriends.Activitis;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;

import co.docy.oldfriends.Adapters.ListAdapterMsg;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventFavoriteChanged;
import co.docy.oldfriends.EventBus.EventFileDownloadProgress;
import co.docy.oldfriends.EventBus.EventHeart200ms;
import co.docy.oldfriends.EventBus.EventScreenSwitch;
import co.docy.oldfriends.EventBus.EventTopicDelete;
import co.docy.oldfriends.EventBus.EventUpdateActivityNumInMain;
import co.docy.oldfriends.Media.Player;
import co.docy.oldfriends.Media.Recorder;
import co.docy.oldfriends.DataClass.IMsg;
import co.docy.oldfriends.Messages.JsonCloseVote;
import co.docy.oldfriends.Messages.JsonCloseVoteRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicCommentRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicRet;
import co.docy.oldfriends.Messages.JsonGetTopicDetailRet;
import co.docy.oldfriends.Messages.JsonSendSoundCommentRet;
import co.docy.oldfriends.Messages.JsonSendTopicActivityClose;
import co.docy.oldfriends.Messages.JsonSendTopicActivityJoin;
import co.docy.oldfriends.Messages.JsonSendTopicActivityLeave;
import co.docy.oldfriends.Messages.JsonSendTopicActivityRet;
import co.docy.oldfriends.Messages.JsonSendTopicVoteChoices;
import co.docy.oldfriends.Messages.JsonSendTopicVoteChooseRet;
import co.docy.oldfriends.Messages.Json_SubRoom_SendCommentImageRet;
import co.docy.oldfriends.Messages.Json_SubRoom_SendTopicComment;
import co.docy.oldfriends.Messages.Json_SubRoom_SendTopicCommentRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.emoji.EmojiFragment;
import co.docy.oldfriends.emoji.EmojiUtils;
import de.greenrobot.event.EventBus;


public class ActivitySubGroup extends ActivityBase implements View.OnClickListener {

    private  int REQUEST_CODE_INPUT_PEOPLE_NUM = 1;
    private ArrayList<Integer> targets = new ArrayList<>();
    LinearLayout subject_button_universal_layout,ll_universal_activity_2;
    TextView subject_button_universal_cancel;
    View subject_button_universal_gray;
    TextView universal_camera, universal_gallery, universal_subject, universal_file, universal_map, universal_vote,universal_activity;

    ActionBar ab;
    TextView bSend, remind_text;
    EditText et;
    ListView lv_msg;
    ImageView ivPlus;

    ListAdapterMsg la_msg;

    File tempAttach;
    String tempAttachMime;

    int groupId = -1;
    int topicId = -1;
    //    String title;
//    String desc;
    int topic_type;
    int topic_subType;

    IMsg subRoomRootNode;
//    int msgPageCount = 0;

//    Json_SubRoom_GetTopicCommentRet.GetTopicCommentRet jsonGetTopicCommentRet;
//    JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet jsonGetTopicCommentRet;
//    JsonGetMsgTopicRet.GetMsgTopicRet jsonGetMsgTopicRet;


    private TextView ButtonRecord;
    private Button ButtonSwitchInput;
    private boolean input_mode_record = false, isGiveUp = true;
    private Recorder mRecord;
    private ImageView soundImageView, cancel_view, microphone;
    private RelativeLayout recording_layout;
    private Player player = null;
    private float locationY;
    private Recorder.IUIApdate mIUiupdate = new Recorder.IUIApdate() {
        @Override
        public void UiApdate(double MaxAmplitude) {
            double amp = MyConfig.Amplitudescale(MaxAmplitude);
            MyLog.d("", "MaxAmplitude====+++=======" + amp);
            if (amp < MyConfig.SOUND_LEVEL_1) {
                soundImageView.setImageResource(R.drawable.sound1);
            } else if (amp < MyConfig.SOUND_LEVEL_2) {
                soundImageView.setImageResource(R.drawable.sound2);
            } else if (amp < MyConfig.SOUND_LEVEL_3) {
                soundImageView.setImageResource(R.drawable.sound3);
            } else if (amp < MyConfig.SOUND_LEVEL_4) {
                soundImageView.setImageResource(R.drawable.sound4);
            } else if (amp < MyConfig.SOUND_LEVEL_5) {
                soundImageView.setImageResource(R.drawable.sound5);
            } else {
                soundImageView.setImageResource(R.drawable.sound6);
            }
        }
    };
    private boolean touched_to_record = false;
    boolean last_touch_state = false;
    private EmojiFragment _fragment;
    private ImageView iv_add_emoji;
    private FrameLayout fl_emoji_fragment;
    private boolean emojiIsVisible = false;
    private GridLayout universal_subject_gridlayout;
    private SensorManager mSensorManager;
    private Sensor mAudioSensor;
    private AudioManager audioManager;
    private MySensorEventListener mySensorEventListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_room);

        Intent intent = getIntent();
        if (intent != null) {
            groupId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, -1);
            topicId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_TOPICID, -1);
            MyLog.d("", "comment: topic id: " + topicId);
//            title = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_TITLE);//在别处被修改
//            desc = intent.getStringExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC);
//            type = intent.getIntExtra("type", -1);
//            subType = intent.getIntExtra("subType", -1);

        } else {
            finish();
        }


        initSubRoomNode();

        initView();

        initSubRoomByHttpGet();
        initAudioMode();
    }

    private void initSubRoomNode() {

        subRoomRootNode = new IMsg(
                MyConfig.USER_INDEX_ADMIN,
                "admin",
                null,

                Calendar.getInstance().getTime(),

                MyConfig.LEVEL_MSG_1,
                MyConfig.mainRoomRootNode,
                MyConfig.MSG_CATEGORY_SUBJECT,

                groupId,
                -1,
                MyConfig.formClientCheckId(groupId),

                "room id:" + groupId,
                "this is sub root msg"
        );

        MyConfig.subRoomRootNode = subRoomRootNode;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (player != null) {
            player.release();
            player = null;
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**取消距离监听器*/
        if(mySensorEventListener != null) {
            mSensorManager.unregisterListener(mySensorEventListener);
        }
        // Unregister
        //EventBus.getDefault().unregister(this);
        if(mRecord!=null){
            mRecord.stopRecord();
        }
    }

    private void initView() {

        ButtonRecord = (TextView) findViewById(R.id.subroom_record_button);
        ButtonSwitchInput = (Button) findViewById(R.id.switch_input);
        soundImageView = (ImageView) findViewById(R.id.sound);
        cancel_view = (ImageView) findViewById(R.id.cancel_view);
        microphone = (ImageView) findViewById(R.id.microphone);
        recording_layout = (RelativeLayout) findViewById(R.id.recording_layout);
        remind_text = (TextView) findViewById(R.id.remind_text);

        lv_msg = (ListView) findViewById(R.id.sub_room_lv_msg);
        et = (EditText) findViewById(R.id.subroom_input_edit);
        et.setOnClickListener(this);
        /**这个Fragment用来显示表情，并将et和Fragment绑定在一起*/
        _fragment = (EmojiFragment) getSupportFragmentManager().findFragmentById(R.id.emoji_fragment);
        _fragment.setEditTextHolder(et);
        iv_add_emoji = (ImageView) findViewById(R.id.subroom_iv_add_emoji);
        iv_add_emoji.setOnClickListener(this);

        fl_emoji_fragment = (FrameLayout) findViewById(R.id.fl_emoji_fragment);



        bSend = (TextView) findViewById(R.id.subroom_send_msg);
        ivPlus = (ImageView) findViewById(R.id.subroom_plus);
        ivPlus.setOnClickListener(this);

        ButtonSwitchInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 虚拟键盘隐藏 判断view是否为空
                View view = getWindow().peekDecorView();
                if (view != null) {
                    //隐藏虚拟键盘
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(ActivityMainGroup.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                MyLog.d("", "recording in mainroom: ButtonSwitchInput onClick");
                input_mode_record = !input_mode_record;
                if (input_mode_record) {
                    /**当切换成录音模式时隐藏表情*/
                    hideEmojiDialog();
                    iv_add_emoji.setVisibility(View.GONE);

                    ButtonRecord.setVisibility(View.VISIBLE);
                    ButtonSwitchInput.setBackgroundResource(R.drawable.button_keyboard);
                    et.setVisibility(View.GONE);
                } else {
                    iv_add_emoji.setVisibility(View.VISIBLE);
                    ButtonRecord.setVisibility(View.GONE);
                    et.setVisibility(View.VISIBLE);
                    ButtonSwitchInput.setBackgroundResource(R.drawable.button_record);
                }
            }
        });

        ButtonRecord.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                MyLog.d("", "recording in mainroom: ButtonRecord any" + event.getAction());
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        locationY = event.getY();
                        MyLog.d("", "recording in mainroom: ButtonRecord ACTION_DOWN");
                        touched_to_record = true;
                        ButtonRecord.setBackgroundResource(R.drawable.button_recorder);
                        ButtonRecord.setText(getResources().getString(R.string.leave_to_send));
                        ButtonRecord.setTextColor(getResources().getColor(R.color.White));
                        recording_layout.setVisibility(View.VISIBLE);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        locationY = event.getY();
                        break;
                    case MotionEvent.ACTION_UP:
//                        MyLog.d("", "recording in mainroom: ButtonRecord ACTION_UP");
                        ButtonRecord.setBackgroundResource(R.drawable.button_record_up);
                        ButtonRecord.setText(getResources().getString(R.string.touch_to_speak));
                        ButtonRecord.setTextColor(getResources().getColor(R.color.xjt_time_link));
                        touched_to_record = false;
                        recording_layout.setVisibility(View.GONE);
                        MyLog.d("", "recording in mainroom: ButtonRecord ACTION_UP === " + event.getRawY() + "  " + event.getY() + "  " + event.getYPrecision());
                        if (event.getY() < -500) {
                            //give up
                            isGiveUp = true;
                        } else {
                            //send anyway
                            isGiveUp = false;
                        }

                        break;
                    default:
                        break;
                }
                return true;
            }
        });


        lv_msg.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {


                } else if (MotionEvent.ACTION_UP == event.getAction()) {
                   MyConfig.hide_keyboard_must_call_from_activity(ActivitySubGroup.this);
                    hideEmojiDialog();
                }
                return false;
            }
        });

        lv_msg.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                MyLog.d("", "listview scroll: " + firstVisibleItem + " " + visibleItemCount + " " + totalItemCount);
                if (firstVisibleItem + visibleItemCount < totalItemCount - MyConfig.room_listview_scroll_threshold) {
//                    MyLog.d("", "listview scroll: TRANSCRIPT_MODE_DISABLED " + firstVisibleItem + " " + visibleItemCount + " " + totalItemCount);
                    view.setTranscriptMode(ListView.TRANSCRIPT_MODE_DISABLED);
                } else {
                    view.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
//                    MyLog.d("", "listview scroll: TRANSCRIPT_MODE_ALWAYS_SCROLL " + firstVisibleItem + " " + visibleItemCount + " " + totalItemCount);
                }
            }
        });
        et.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN)) {
                    getTextComment2Send();
                    return true;
                }
                return false;
            }
        });
        et.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 1) {
                    bSend.setVisibility(View.VISIBLE);
                    ivPlus.setVisibility(View.GONE);

                } else {
                    bSend.setVisibility(View.GONE);
                    ivPlus.setVisibility(View.VISIBLE);
                }
                /**群聊@某人的功能*/
                MyConfig.pegSomeBody(ActivitySubGroup.this,groupId,s);

            }
        });


        bSend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getTextComment2Send();
            }

        });





        subject_button_universal_layout = (LinearLayout)findViewById(R.id.universal_subject_subroom);
        subject_button_universal_cancel = (TextView)findViewById(R.id.subject_button_universal_cancel);
        subject_button_universal_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subject_button_universal_layout.setVisibility(View.GONE);
            }
        });
        subject_button_universal_gray = findViewById(R.id.subject_button_universal_gray);
        subject_button_universal_gray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subject_button_universal_layout.setVisibility(View.GONE);
            }
        });

        universal_camera = (TextView)findViewById(R.id.universal_camera);
        universal_gallery = (TextView)findViewById(R.id.universal_gallery);
        universal_subject = (TextView)findViewById(R.id.universal_subject);
        universal_subject.setVisibility(View.GONE);
        universal_file = (TextView)findViewById(R.id.universal_file);
        universal_file.setVisibility(View.GONE);
        universal_map = (TextView)findViewById(R.id.universal_map);
        universal_vote = (TextView)findViewById(R.id.universal_vote);
        universal_activity = (TextView)findViewById(R.id.universal_activity);
        universal_vote.setVisibility(View.GONE);

        universal_subject.setOnClickListener(universalButtonClickListener);
        universal_camera.setOnClickListener(universalButtonClickListener);
        universal_gallery.setOnClickListener(universalButtonClickListener);
        universal_file.setOnClickListener(universalButtonClickListener);
        universal_map.setOnClickListener(universalButtonClickListener);
        universal_vote.setOnClickListener(universalButtonClickListener);

        universal_subject_gridlayout = (GridLayout) findViewById(R.id.universal_subject_gridlayout);
        universal_subject_gridlayout.setColumnCount(2);//设置每行显示两个，可以使按钮居中，若以后增加按钮的个数参数值啊哟做响应的改变
        removeGridLayoutChild();
    }

    /**
     * 移除plusDialog中的按钮。这个布局复用主聊天室中的布局，这个聊天室中的子话题种类少，把多余的移除。
     */
    private void removeGridLayoutChild() {
        for(int i = universal_subject_gridlayout.getChildCount()-1;i >= 0;i--){
            switch (universal_subject_gridlayout.getChildAt(i).getId()){
                case R.id.universal_subject://主题
                    universal_subject_gridlayout.removeViewAt(i);
                    break;
                case R.id.universal_vote://投票
                    universal_subject_gridlayout.removeViewAt(i);
                    break;
                case R.id.universal_activity://报名
                    universal_subject_gridlayout.removeViewAt(i);
                    break;
                case R.id.universal_file://文件
                    universal_subject_gridlayout.removeViewAt(i);
                    break;
                case R.id.universal_map://地图
                    universal_subject_gridlayout.removeViewAt(i);
                    break;
                case R.id.universal_video://视频
                    universal_subject_gridlayout.removeViewAt(i);
                    break;

            }
        }
    }

    /**
     * 隐藏表情选择框
     */
    private void hideEmojiDialog() {
        fl_emoji_fragment.setVisibility(View.GONE);
        iv_add_emoji.setImageResource(R.drawable.emoji_button_3x);
        emojiIsVisible = false;
    }

    /**
     * 显示表情选择框
     */
    private void showEmojiDialog() {
        if(!et.hasFocus()){
            et.setFocusable(true);
            et.requestFocus();
        }
        EmojiUtils.hide_keyboard_must_call_from_activity(this);
        SystemClock.sleep(300);
        fl_emoji_fragment.setVisibility(View.VISIBLE);
        iv_add_emoji.setImageResource(R.drawable.button_keyboard);
        emojiIsVisible = true;
    }


    private View.OnClickListener universalButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            subject_button_universal_layout.setVisibility(View.GONE);
            switch (v.getId()) {
                case R.id.universal_subject:
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_subject);
//                    openSubjectInput(MyConfig.REQUEST_SUBJECT_ONLY, null, null);
                    break;
                case R.id.universal_camera:
                    tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_camera);
                    openCamera();
                    break;
                case R.id.universal_gallery:
                    tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_gallery);
                    openFileChooser(MyConfig.MIME_IMAGE_STAR);
                    break;
                case R.id.universal_file:
                    tempAttachMime = MyConfig.MIME_FILE_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_file);
                    openFileChooser(MyConfig.MIME_FILE_STAR);
                    break;
                case R.id.universal_map:
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_map);
//                    openMap();
                    break;
                case R.id.universal_vote:
//                    openSetupVote(MyConfig.REQUEST_CREATE_VOTE);
                    break;
            }

        }
    };

    public void drawSubroomViewByHttpData() {

        if(subRoomRootNode.ll_sub_msg == null || subRoomRootNode.ll_sub_msg.size() == 0){
            MyLog.d("","drawSubroomViewByHttpData: found ll_sub_msg.size == 0");
            return;
        }

        IMsg topicMsg = subRoomRootNode.ll_sub_msg.get(0);//topic msg始终在链表头部
        topic_type = topicMsg.type;
        topic_subType = topicMsg.subType;


        ab = getSupportActionBar();

        if (ab != null) {
            ab.setTitle(MyConfig.getSubtypeName(topic_type, topic_subType));
        }

        la_msg = new ListAdapterMsg(ActivitySubGroup.this, MyConfig.LEVEL_ROOM_SUB, subRoomRootNode.ll_sub_msg, null, new ListAdapterMsg.OnImageClickedListener() {
            @Override
            public void onImageClicked(IMsg ims) {

                if(ims.type == MyConfig.TYPE_TOPIC && ims.subType == MyConfig.SUBTYPE_VIDEO){


                    //刷新一下，看是否下载完毕
                    ims.fileDownloadProgress = MyConfig.getFileDownloadProgress(ims.filePath_inLocal, ims.fileSize);

                    MyLog.d("","video download: progress " + ims.filePath_inLocal + " " + ims.fileDownloadProgress);

                    if(ims.fileDownloadProgress == 100){


                        Intent intent = new Intent(ActivitySubGroup.this, ActivityVideoPlayer.class);
                        intent.putExtra("mode", 1);
                        intent.putExtra("path", ims.filePath_inLocal);
                        intent.putExtra("recorder_os", ims.os);

                        startActivity(intent);


                    }else {

                        Intent intent = new Intent(ActivitySubGroup.this, ActivityVideoPlayer.class);
                        intent.putExtra("mode", 0);
                        intent.putExtra("path", MyConfig.getApiDomain_NoSlash_GetResources() + ims.filePath_inServer);
                        intent.putExtra("recorder_os", ims.os);

                        startActivity(intent);
                    }

                }else {


//                MyConfig.iMsgClicked = imsNew;
//                MyConfig.iMsgGalleryRoot = MyConfig.subRoomRootNode;
                    MyConfig.genGalleryFullSizeUrl(MyConfig.subRoomRootNode, ims);

                    Intent intent = new Intent(ActivitySubGroup.this, ActivityGallery.class);
                    intent.putExtra("mode", MyConfig.GALLERY_MODE_PHOTOS_DOWNLOADED);
                    intent.putExtra("from", ActivitySubGroup.class.getName());

                    startActivity(intent);
                }
            }
        });
        la_msg.soundClickedListener = onSoundClickedListener;

        la_msg.shareClickedListener = new ListAdapterMsg.OnShareClickedListener() {
            @Override
            public void onShareClicked(final IMsg imsg) {

                shareDialog(imsg);

            }

        };
        la_msg.forwardClickedListener = new ListAdapterMsg.OnForwardClickedListener() {
            @Override
            public void onForwardClicked(IMsg imsNew) {

                shareDialog(imsNew);

            }
        };
        la_msg.downloadClickedListener = new ListAdapterMsg.OnDownloadClickedListener() {
            @Override
            public void onDownloadClicked(IMsg imsg) {
                new MyConfig.DownloadFileFromUrl(new MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo(ActivitySubGroup.class.getName(), MyConfig.ATTACHMENT_ACTION_DO_NOTHING)).execute(
                        MyConfig.getApiDomain_NoSlash_GetResources() + imsg.filePath_inServer,//在服务器的地址
                        imsg.filePath_inLocal);//本地地址
            }
        };

        la_msg.fileClickedListener = new ListAdapterMsg.OnFileClickedListener() {
            @Override
            public void onFileClicked(final Context context, final IMsg imsg) {

                new MyConfig.DownloadFileFromUrl(new MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo(ActivitySubGroup.class.getName(), MyConfig.ATTACHMENT_ACTION_DL_OPEN)).execute(
                        MyConfig.getApiDomain_NoSlash_GetResources() + imsg.filePath_inServer,//在服务器的地址
                        imsg.filePath_inLocal);//本地地址
            }
        };

        la_msg.captureLongClickedListener = new MyConfig.ClassCaptureLongClick(
                ActivitySubGroup.this,
                subRoomRootNode.ll_sub_msg,
                subRoomRootNode.ll_sub_msg_with_image,
                la_msg,
                new MyConfig.ClassCaptureLongClick.AvatarLongPressed() {
                    @Override
                    public void avatarLongPressed(IMsg iMsg) {

                        targets.add(iMsg.user_id);
                        StringBuilder sb = new StringBuilder(et.getText());
                        sb.append("@"+iMsg.user_name);
                        et.setText(sb.toString());
                        et.setSelection(sb.length());

                    }
                });

        /**
         * 如果子话题是vote类型，需要设置相关操作listener
         */
        if (topic_subType == MyConfig.SUBTYPE_VOTE) {

            MyLog.d("", "voteSubmitClickedListener subType == MyConfig.SUBTYPE_VOTE");

            la_msg.voteSubmitClickedListener = new ListAdapterMsg.OnVoteSubmitClickedListener() {
                @Override
                public void onVoteSubmitClicked(int resourceId, LinkedList<Integer> choice) {
                    MyLog.d("", "voteSubmitClickedListener before sendVoteOption");
                    sendVoteOption(resourceId, choice);
                }
            };

            la_msg.votePersonListClickedListener = new ListAdapterMsg.OnVotePersonListClickedListener() {
                @Override
                public void onVotePersonListClicked(IMsg imsg) {

                    MyConfig.imsgVoteResult = imsg;
                    Intent intent_regist = new Intent(ActivitySubGroup.this, ActivityVoteVoterList.class);
                    startActivity(intent_regist);

                }
            };

            la_msg.voteFinishClickedListener = new ListAdapterMsg.OnVoteFinishClickedListener() {
                @Override
                public void onVoteFinishClicked(IMsg imsg) {
                    MyLog.d("", "HttpTools: vote debug voteFinishClickedListener ");
                    finishVote(imsg);

                }
            };
        }
        /**
         * 如果子话题是activity类型，需要设置相关操作listener
         */
        if (topic_subType == MyConfig.SUBTYPE_ACTIVITY) {

            /**报名回调接口*/
            la_msg.activitySubmitClickedListener = new ListAdapterMsg.OnActivitySubmitClickedListener() {
                @Override
                public void onActivitySubmitClicked(int resourceId,int num) {
                    sendMActivity(resourceId,num);
                }
            };

            /**取消报名回调接口*/
            la_msg.activityLeaveClickedListener = new ListAdapterMsg.OnActivityLeaveClickedListener() {
                @Override
                public void onActivityLeaveClicked(int resourceId) {
                    leaveMActivity(resourceId);
                }
            };

              /**关闭报名回调接口*/
            la_msg.activityCloseClickedListener = new ListAdapterMsg.OnActivityCloseClickedListener() {
                @Override
                public void onActivityCloseClicked(int resourceId) {
                    closeMActivity(resourceId);
                }
            };
            /**报名时选择人数*/
            la_msg.activityInputPeopleNumClickedListener = new ListAdapterMsg.OnActivityInputPeopleNumClickedListener() {
                @Override
                public void onActivityInputPeopleNumClicked() {
                    Intent addressIntent = new Intent(ActivitySubGroup.this,ActivityActivityInputPage.class);
                    addressIntent.putExtra("type","people_num");
                    startActivityForResult(addressIntent,REQUEST_CODE_INPUT_PEOPLE_NUM);
                }
            };
        }

        lv_msg.setAdapter(la_msg);
//        lv_msg.setSelection(0);
        subRoomRootNode.la_msg = la_msg;
    }


    public void shareDialog(final IMsg imsg) {
        /**
         * 考虑在这里弹出选择框，可选分享链接，或者
         */

        new DialogFragment() {

            @Override
            public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
            }

            @Override
            public void onDestroy() {
                super.onDestroy();
            }

            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        getActivity());
                builder.setTitle(getResources().getString(R.string.please_select))
                        .setItems(R.array.subroom_share_click, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        Intent intent_foward = new Intent(ActivitySubGroup.this, ActivityForwardToWindow.class);
                                        intent_foward.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_TOPICID, imsg.topicId);
                                        startActivity(intent_foward);
                                        return;
                                    case 1:
                                        MyConfig.shareTopic(ActivitySubGroup.this, imsg, MyConfig.ATTACHMENT_ACTION_DL_SHARE);
                                        return;
                                    case 2:
                                        MyConfig.shareTopic(ActivitySubGroup.this, imsg, MyConfig.ATTACHMENT_ACTION_DL_OPEN);
                                        return;
                                    default:
                                        return;
                                }
                            }
                        })
                        .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                return builder.create();
            }
        }.show(getFragmentManager(), "");
    }

    private void initSubRoomByHttpGet() {

        subRoomRootNode.ll_sub_msg.clear();
        subRoomRootNode.ll_sub_msg_with_image.clear();

//        msgPageCount++;

        new Thread(new Runnable() {
            @Override
            public void run() {


                Gson gson = new Gson();

//                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_DETAIL + topicId + "/?page=" + msgPageCount, MyConfig.usr_token);
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_DETAIL + topicId, MyConfig.usr_token);

                JsonGetTopicDetailRet jsonGetTopicDetailRet = gson.fromJson(s, JsonGetTopicDetailRet.class);

                if (jsonGetTopicDetailRet != null && jsonGetTopicDetailRet.code == MyConfig.retSuccess()) {

                    final JsonGetMsgTopicRet.GetMsgTopicRet jsonGetMsgTopicRet = MyConfig.transfer2GetGetMsgTopicRet(jsonGetTopicDetailRet);
                    addGetMsgTopicRet2Local(jsonGetMsgTopicRet);

                    for (JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet getMsgTopicCommentRet : jsonGetTopicDetailRet.data.comments) {
                        addGetTopicCommentRet2Local(getMsgTopicCommentRet);
                    }
                    updateActivityNumInMain(jsonGetTopicDetailRet);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //更新msg列表
//                        la_msg = new ListAdapterMsg(ActivitySubGroup.this, imsMainRoomRoot.level + 1, imsMainRoomRoot.ll_sub_msg, null, null);
//                        lv_msg.setAdapter(la_msg);
//                        imsMainRoomRoot.la_msg = la_msg;


                            //更新msg列表
                            MyConfig.buildTimeShowString(subRoomRootNode.ll_sub_msg);

                            drawSubroomViewByHttpData();
//                            la_msg.notifyDataSetChanged();
//                        lv_msg.onRefreshComplete();
                        }
                    });

                }

            }
        }).start();
    }

    /**
     * 当子聊天室中报名人数发生改变时，让主聊天室中显示的报名人数也跟随变化
     * @param jsonGetTopicDetailRet
     */
    private void updateActivityNumInMain(JsonGetTopicDetailRet jsonGetTopicDetailRet) {
        EventUpdateActivityNumInMain eventUpdateActivityNumInMain = new EventUpdateActivityNumInMain();
        eventUpdateActivityNumInMain.id = jsonGetTopicDetailRet.data.info.topicId;
        eventUpdateActivityNumInMain.num = jsonGetTopicDetailRet.data.info.joinedNum;
        EventBus.getDefault().post(eventUpdateActivityNumInMain);
    }

    public void finishVote(final IMsg imsg) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonCloseVote jsonCloseVote = new JsonCloseVote();
                jsonCloseVote.accessToken = MyConfig.usr_token;
                jsonCloseVote.id = imsg.vote.resouceId;

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonCloseVote, JsonCloseVote.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_VOTE + "/" + imsg.vote.resouceId + MyConfig.API2_SUBJECT_VOTE_CLOSE_2, jsonStr);

                final JsonCloseVoteRet jsonCloseVoteRet = gson.fromJson(s, JsonCloseVoteRet.class);
                MyLog.d("", "HttpTools: vote debug JsonCloseVoteRet " + s);
                if (jsonCloseVoteRet != null && jsonCloseVoteRet.code == MyConfig.retSuccess()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            msgPageCount = 0;
                            initSubRoomByHttpGet();
                        }
                    });
                }


            }
        }).start();


    }

    private void addGetMsgTopicRet2Local(JsonGetMsgTopicRet.GetMsgTopicRet getMsgTopicRet) {
        //todo
        // 和MainGroup中对应函数合并
        IMsg msg = MyConfig.formIMsgFromGetMsgTopicRet(getMsgTopicRet, subRoomRootNode, MyConfig.MSG_CATEGORY_SUBJECT);

        /**
         * 注意，由于后台返回的msg次序，在main room里面是插入到1这个位置，在sub room插入到最后
         * 以后再和后台核对
         */
        subRoomRootNode.ll_sub_msg.addFirst(msg);
        MyConfig.addImageIMsg2ImageList(subRoomRootNode, msg, false);
    }


    private void addGetTopicCommentRet2Local(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet getTopicCommentRet) {

        IMsg msg = MyConfig.formIMsgFromGetMsgTopicCommentRet(getTopicCommentRet, subRoomRootNode, MyConfig.MSG_CATEGORY_SUBJECT_COMMENT);

        /**
         * 注意，由于后台返回的msg次序，在main room里面是插入到1这个位置，在sub room插入到最后
         * 以后再和后台核对
         */
        subRoomRootNode.ll_sub_msg.addLast(msg);
        MyConfig.addImageIMsg2ImageList(subRoomRootNode, msg, false);
    }

    private void getSoundComment2Send(int duration) {
        IMsg iMsg = formSoundMsg();
        iMsg.duration = duration;
        iMsg.audioPath = MyConfig.tempSoundFile;
        sendSoundComment(iMsg);
    }


    private void sendSoundComment(IMsg iMsg) {

        if (!MyConfig.debug_msg_no_local_first) {
            sendSoundComment2Local(iMsg);
        }
        SendSoundComment2Server(iMsg);

    }


    private void sendSoundComment2Local(IMsg iMsg) {
        iMsg.setLocalSendTag();
        socketAddTopicComment2Local(iMsg);
    }

    private void SendSoundComment2Server(final IMsg imsg) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                final JsonSendSoundCommentRet jsonSendSoundCommentRet = HttpTools.okhttpUploadSoundComment(imsg, null);

                if (jsonSendSoundCommentRet != null && jsonSendSoundCommentRet.code == MyConfig.retSuccess()) {
                    MyLog.d("", getResources().getString(R.string.sound_success));
                }

            }
        }).start();
    }

    private IMsg formSoundMsg() {
        File f = new File(MyConfig.appCacheDirPath, MyConfig.tempSoundFile);
        if (f.exists() && f.length() > 0) {
            IMsg msg = new IMsg(
                    MyConfig.usr_id,
                    MyConfig.usr_nickname,
                    MyConfig.usr_avatar,

                    Calendar.getInstance().getTime(),

                    MyConfig.LEVEL_MSG_2,
                    subRoomRootNode,
                    MyConfig.MSG_CATEGORY_SUBJECT_COMMENT,

                    groupId,
                    topicId,
                    MyConfig.formClientCheckId(groupId),

                    null,
                    null
            );
            msg.attached = f;
            return msg;
        } else {
            return null;
        }

    }

    private void getTextComment2Send() {
        String str = MyConfig.trimAndTakeAwayStringFromEditText(et);
        if (str != null) {
            /**把表情的名字和iOS协商好的字段*/
            str = EmojiUtils.replaceNameByField(this,str);
            IMsg msg = new IMsg(
                    MyConfig.usr_id,
                    MyConfig.usr_nickname,
                    MyConfig.usr_avatar,

                    Calendar.getInstance().getTime(),

                    MyConfig.LEVEL_MSG_2,
                    subRoomRootNode,
                    MyConfig.MSG_CATEGORY_SUBJECT_COMMENT,

                    groupId,
                    topicId,
                    MyConfig.formClientCheckId(groupId),

                    null,
                    str
            );

            sendTextComment(msg);

        }
    }

    private void sendTextComment(IMsg msg) {


        if (!MyConfig.debug_msg_no_local_first) {
            sendTextComment2Local(msg);
        }

        sendTextComment2Server(msg);
    }

    private void sendTextComment2Local(IMsg msg) {
        msg.setLocalSendTag();
        socketAddTopicComment2Local(msg);
    }


    private void socketAddTopicComment2Local(final IMsg msg) {
        appendIMsg2List(msg);
    }

    private void appendIMsg2List(final IMsg msg) {

        int index = MyConfig.searchRepeatMsgInList(subRoomRootNode.ll_sub_msg, msg, MyConfig.REPEAT_CHECK_COUNT_MAX);

        if (-1 == index) {
            subRoomRootNode.ll_sub_msg.addLast(msg);
            MyLog.d("", "subroom, from " + msg.msg_from + " notifyDataSetChanged: addLast " + msg.clientId);
        } else {
            subRoomRootNode.ll_sub_msg.set(index, msg);
            MyLog.d("", "subroom, from " + msg.msg_from + " notifyDataSetChanged: set " + msg.clientId);
        }

        /**
         * 这里可能隐藏一个很深的小问题，及图片list的次序可能和消息list的次序不同，这样点进去到相册时会发现次序不同
         * 但是点进去还是正确的图片，因为寻找图片是根据clientId来比对的
         */
        int index_image = MyConfig.searchRepeatMsgInList(subRoomRootNode.ll_sub_msg_with_image, msg, MyConfig.REPEAT_CHECK_COUNT_MAX);
        if (-1 == index_image) {
            MyConfig.addImageIMsg2ImageList(subRoomRootNode, msg, false);
        } else {
            MyConfig.setImageIMsg2ImageList(subRoomRootNode, msg, index_image);
        }

        MyConfig.buildTimeShowString(subRoomRootNode.ll_sub_msg);
        la_msg.notifyDataSetChanged();

    }


    private void sendTextComment2Server(final IMsg ims) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                Json_SubRoom_SendTopicComment jsonSubRoomSendTopicComment = new Json_SubRoom_SendTopicComment();
                jsonSubRoomSendTopicComment.accessToken = MyConfig.usr_token;
                jsonSubRoomSendTopicComment.message = ims.body;
                jsonSubRoomSendTopicComment.topicId = topicId;
                jsonSubRoomSendTopicComment.clientId = ims.clientId;
                jsonSubRoomSendTopicComment.targets = targets;
                targets.clear();

                String jsonStr = gson.toJson(jsonSubRoomSendTopicComment, Json_SubRoom_SendTopicComment.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SEND_TOPIC_COMMENT + topicId + MyConfig.API2_SEND_TOPIC_COMMENT_2, jsonStr);
                MyLog.d("", "HttpTools send topic ret: " + s);

                final Json_SubRoom_SendTopicCommentRet jsonSubRoomSendTopicCommentRet = gson.fromJson(s, Json_SubRoom_SendTopicCommentRet.class);


//                MyLog.d("", "HttpTools: " + s);
//                if (s != null) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            MyConfig.sendSysMsg(imsMainRoomRoot, s);
//
//                        }
//                    });
//                }
                MyLog.d("", "apitest: send msg ret " + s);

            }
        }).start();
    }

    public int count = -1;

    public void onEventMainThread(EventHeart200ms eventHeart200ms) {
        if (count >= 0) {
            count++;
        }
        if (count >= (MyConfig.MAX_RECORD_TIME * 1000 / 200)) {
            int duration = 0;
            //stop record
            if (mRecord != null) {
                count = -1;
                duration = mRecord.stopRecord();
                mRecord = null;
                ButtonRecord.setBackgroundResource(R.drawable.button_record_up);
                ButtonRecord.setText(getResources().getString(R.string.touch_to_speak));
                ButtonRecord.setTextColor(getResources().getColor(R.color.xjt_time_link));
                touched_to_record = false;
                recording_layout.setVisibility(View.GONE);
                MyLog.d("", "count==========" + count);
                getSoundComment2Send(duration);
            }
        } else if (count >= ((MyConfig.MAX_RECORD_TIME - 10) * 1000 / 200)) {
            if (locationY < -500) {
                cancel_view.setVisibility(View.VISIBLE);
                microphone.setVisibility(View.GONE);
                soundImageView.setVisibility(View.GONE);
                remind_text.setText(getResources().getString(R.string.record_leave_to_giveup_remind));
            } else {
                cancel_view.setVisibility(View.GONE);
                microphone.setVisibility(View.VISIBLE);
                soundImageView.setVisibility(View.VISIBLE);
                String format = getResources().getString(R.string.record_last_time);
                String result = String.format(format, MyConfig.MAX_RECORD_TIME - (count * 200 / 1000));
                remind_text.setText(result);
            }

        } else {
            if (locationY < -500) {
                cancel_view.setVisibility(View.VISIBLE);
                microphone.setVisibility(View.GONE);
                soundImageView.setVisibility(View.GONE);
                remind_text.setText(getResources().getString(R.string.record_leave_to_giveup_remind));
            } else {
                cancel_view.setVisibility(View.GONE);
                microphone.setVisibility(View.VISIBLE);
                soundImageView.setVisibility(View.VISIBLE);
                remind_text.setText(getResources().getString(R.string.record_up_to_giveup_remind));
            }
        }

        if ((last_touch_state) && (touched_to_record)) {
            //start record
            if (mRecord == null) {
                if (player != null) {
                    player.release();
                    player = null;
                }
                mRecord = new Recorder(MyConfig.tempSoundFile);
                mRecord.startRecord();
                count = 0;
            } else {
                mIUiupdate.UiApdate(mRecord.mediaRecorder.getMaxAmplitude());
            }

        } else if ((!last_touch_state) && (!touched_to_record)) {
            int duration = 0;
            //stop record
            if (mRecord != null) {
                count = -1;
                duration = mRecord.stopRecord();
                mRecord = null;
                if (!isGiveUp && duration <= 60) {  //send Message
                    getSoundComment2Send(duration);
                }
            }
        }

        last_touch_state = touched_to_record;

    }

    ListAdapterMsg.OnSoundClickedListener onSoundClickedListener = new ListAdapterMsg.OnSoundClickedListener() {
        @Override
        public void onSoundClicked(IMsg imsg) {

            IMsg currentPlayedImag = null;
            if (player != null) {

                /**
                 * 如果已经存在一个player,就是当前点击的imsg
                 *  不是当前的imsg,清除player,新建一个当前imsg的player并播放
                 */
                currentPlayedImag = player.getIMsg();

                if (currentPlayedImag == imsg) {

                    switch (currentPlayedImag.audioPlaying) {
                        case MyConfig.MEDIAPLAYER_IS_PLAYING://正在准备或正在播放
                            player.pause();
                            break;
                        case MyConfig.MEDIAPLAYER_IS_READY:// 停止
                            player.replay();
                            break;
                        case MyConfig.MEDIAPLAYER_NOT_INITIAL://已经播放完毕
                            player.prepareAndPlay();
                            break;
                        case MyConfig.MEDIAPLAYER_IS_PREPARING: // 再次播放
                            player.reset();
                            break;
                    }

                } else {
                    player.release();
                    player = playSoundIMsg(imsg);
                }

            } else {

                player = playSoundIMsg(imsg);

            }

        }
    };

    public Player playSoundIMsg(IMsg imsg) {
        Player player = new Player(imsg, new Player.PlayingNotify() {
            @Override
            public void statusNotify() {

                la_msg.notifyDataSetChanged();

            }
        });
        player.prepareAndPlay();

        return player;
    }


    public void onEventMainThread(JsonGetMsgTopicRet.GetMsgTopicRet json_socket_topic) {

        /**
         * 子聊天室一般不接收topic类信息的
         * 但是比较特殊的，需要接收topic close信息
         * 收到后简单地做一个刷新处理
         */

        MyLog.d("", "socketio event GetMsgTopicRet: notifyDataSetChanged)" + json_socket_topic.groupId);
        if (groupId != json_socket_topic.groupId) {
            MyLog.d("", "socketio event : onEventMainThread(Json_Socket_Topic json_socket_topic) ret for wrong groupId" + json_socket_topic.groupId);
            return;
        }

        IMsg iMsgSubjectRet = MyConfig.formIMsgFromGetMsgTopicRet(json_socket_topic, subRoomRootNode, MyConfig.MSG_CATEGORY_SUBJECT);

        /**
         * 对vote close信息做特殊处理
         * 收到这个信息后，需要刷新之前的vote topic信息
         */
        if(iMsgSubjectRet.type == MyConfig.TYPE_TOPIC && iMsgSubjectRet.subType == MyConfig.SUBTYPE_VOTE_CLOSE){

            int topicId = iMsgSubjectRet.topicId;
            for(IMsg iMsg: subRoomRootNode.ll_sub_msg){
                if(iMsg.type == MyConfig.TYPE_TOPIC && iMsg.subType == MyConfig.SUBTYPE_VOTE && iMsg.topicId == topicId){
//                    iMsg.vote.closed = true;//可以省略
//                    la_msg.notifyDataSetChanged();//不能更新投票人数
                    initSubRoomByHttpGet();//强制刷新，可能导致用户界面闪动
                }
            }
        }

    }

    public void onEventMainThread(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet json_socket_topicComment) {
        MyLog.d("", "socketio event : onEventMainThread(Json_Socket_TopicComment json_socket_topicComment) subroom "
                + json_socket_topicComment.groupId
                + " " + json_socket_topicComment.type
                + " " + json_socket_topicComment.subType);
        if (topicId != json_socket_topicComment.info.topicId) {
            MyLog.d("", "socketio event : onEventMainThread(JsonSocketMsg jsonSocketMsg) ret for wrong topicId " + json_socket_topicComment.info.topicId);
            return;
        }


        IMsg iMsgRet = MyConfig.formIMsgFromGetMsgTopicCommentRet(json_socket_topicComment, subRoomRootNode, MyConfig.MSG_CATEGORY_SUBJECT_COMMENT);


        socketAddTopicComment2Local(iMsgRet);
    }


    public void onEventMainThread(EventTopicDelete eventTopicDelete) {
//        MyConfig.cleanAndFinishActivity(this);
        finish();
    }

    public void onEventMainThread(EventScreenSwitch eventScreenSwitch) {
//        la_msg.notifyDataSetChanged();
        initSubRoomByHttpGet();
    }


    public void onEventMainThread(EventFavoriteChanged eventFavoriteChanged) {
//        la_msg.notifyDataSetChanged();
        initSubRoomByHttpGet();

        /**
         * todo
         *  在EventFavoriteChanged里面添加了topic及add信息
         *  所以也可以直接更新，而不是从http更新
         *  从http更新可靠，但是有成本
         */


    }

    public void onEventMainThread(EventFileDownloadProgress eventFileDownloadProgress) {

        if (eventFileDownloadProgress.downloadFileFromUrlSenderInfo.sender.equals(ActivitySubGroup.class.getName())) {

            MyLog.d("", "share out, onEventMainThread subrooom  " + eventFileDownloadProgress.downloadFileFromUrlSenderInfo.sender);

            IMsg iMsgTopic = subRoomRootNode.ll_sub_msg.get(0);

//            MyLog.d("","share in process: progress " + eventFileDownloadProgress.progress + " "+eventFileDownloadProgress.url +" "+iMsgTopic.filePath_inServer);

            if (eventFileDownloadProgress.des.equals(iMsgTopic.filePath_inLocal)) {

                iMsgTopic.fileDownloadProgress = eventFileDownloadProgress.progress;
                la_msg.notifyDataSetChanged();


            }
            //在download stoped且progress为100时找出被点击的msg
            if (/*(eventFileDownloadProgress.downloadFileFromUrlSenderInfo.code == MyConfig.ATTACHMENT_ACTION_DL_SHARE)
                    &&*/ (eventFileDownloadProgress.status == MyConfig.FILE_DOWNLOAD_STOPED)
                    && (iMsgTopic.fileDownloadProgress == 100)) {

                MyLog.d("", "share out, onEventMainThread subrooom  " + eventFileDownloadProgress.downloadFileFromUrlSenderInfo.sender);

                MyConfig.shareTopic(this, iMsgTopic, eventFileDownloadProgress.downloadFileFromUrlSenderInfo.code);

//                if (iMsgTopic.subType == MyConfig.SUBTYPE_FILE) {
//                    if (eventFileDownloadProgress.src.equals(MyConfig.getApiDomain_NoSlash_GetResources() + iMsgTopic.filePath_inServer)) {
//                        MyConfig.shareTopic(this, iMsgTopic, eventFileDownloadProgress.downloadFileFromUrlSenderInfo.code);
//                    }
//                } else if (iMsgTopic.subType == MyConfig.SUBTYPE_IMAGE) {
////                    if (eventFileDownloadProgress.src.equals(MyConfig.getApiDomain_NoSlash_GetResources() + iMsgTopic.fullSize)) {
//                        MyConfig.shareTopic(this, iMsgTopic, eventFileDownloadProgress.downloadFileFromUrlSenderInfo.code);
////                    }
//                }
            }

        }

    }

    private void sendVoteOption(final int resouceId, final LinkedList<Integer> choice) {

        MyLog.d("", "voteSubmitClickedListener: choice.size() " + choice.size());
        if (choice.size() == 0) {
            return;//必须有一项被选择才能投票
        }

        MyLog.d("", "voteSubmitClickedListener: vote " + resouceId + " option.tostring: " + choice.toString());


        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();

                JsonSendTopicVoteChoices jsonSendTopicVoteChoices = new JsonSendTopicVoteChoices();
                jsonSendTopicVoteChoices.accessToken = MyConfig.usr_token;
                jsonSendTopicVoteChoices.choice = choice;

                String str = gson.toJson(jsonSendTopicVoteChoices);
                MyLog.d("", "httptool vote choose str: " + str);

                final String ret = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_VOTE + "/" + resouceId, str);

                final JsonSendTopicVoteChooseRet jsonSendTopicVoteChooseRet = gson.fromJson(ret, JsonSendTopicVoteChooseRet.class);

                if (jsonSendTopicVoteChooseRet != null && jsonSendTopicVoteChooseRet.code == MyConfig.retSuccess()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            msgPageCount = 0;
                            initSubRoomByHttpGet();
                        }
                    });
                }


            }
        }).start();

    }

    /**
     * 确认 报名
     * @param
     */
    public void sendMActivity(final int resouceId, final int num) {


        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();

                JsonSendTopicActivityJoin jsonSendTopicActivityJoin = new JsonSendTopicActivityJoin();
                jsonSendTopicActivityJoin.accessToken = MyConfig.usr_token;
                jsonSendTopicActivityJoin.num = num;



                String str = gson.toJson(jsonSendTopicActivityJoin);
               MyLog.d("kwwl","确认报名时向服务器提交的数据是================="+str);
               MyLog.d("kwwl","确认报名服务器接口是================="+MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_ACTIVIY + "/" + resouceId+"/join");
                final String ret = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_ACTIVIY + "/" + resouceId+"/join", str);
                MyLog.d("kwwl","确认报名时返回结果是================="+ret);

                final JsonSendTopicActivityRet jsonSendTopicActivityRet = gson.fromJson(ret, JsonSendTopicActivityRet.class);

                /**报名成功*/
                if (jsonSendTopicActivityRet != null && jsonSendTopicActivityRet.code == MyConfig.retSuccess()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initSubRoomByHttpGet();
                            if (jsonSendTopicActivityRet.data.status == 2) {/**处于等待状态*/
                                showActivityWaitHintDialog();
                            }
                        }
                    });
                }

                /**报名关闭*/
                if (jsonSendTopicActivityRet != null&& jsonSendTopicActivityRet.code == 500000 && jsonSendTopicActivityRet.message.equals("ActivityClosed")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            msgPageCount = 0;
                            initSubRoomByHttpGet();
                            Toast.makeText(ActivitySubGroup.this,"报名已经关闭，您报名失败",Toast.LENGTH_SHORT).show();
                        }
                    });
                }


            }
        }).start();
    }

    /**
     * 取消报名
     * @param
     */
    public void leaveMActivity(final int resouceId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                JsonSendTopicActivityLeave jsonSendTopicActivityLeave = new JsonSendTopicActivityLeave();
                jsonSendTopicActivityLeave.accessToken = MyConfig.usr_token;
                String str = gson.toJson(jsonSendTopicActivityLeave);
               MyLog.d("kwwl","取消报名时向服务器提交的数据是================="+str);
               MyLog.d("kwwl","取消报名服务器接口是================="+MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_ACTIVIY + "/" + resouceId+"/leave");
                final String ret = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_ACTIVIY + "/" + resouceId+"/leave", str);
                MyLog.d("kwwl","取消报名时返回结果是================="+ret);

                final JsonSendTopicActivityRet jsonSendTopicActivityRet = gson.fromJson(ret, JsonSendTopicActivityRet.class);

                if (jsonSendTopicActivityRet != null && jsonSendTopicActivityRet.code == MyConfig.retSuccess()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            msgPageCount = 0;
                            initSubRoomByHttpGet();
                        }
                    });
                }
            }
        }).start();
    }

    /**
     * 关闭报名
     * @param
     */
    public void closeMActivity(final int resouceId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                JsonSendTopicActivityClose jsonSendTopicActivityClose = new JsonSendTopicActivityClose();
                jsonSendTopicActivityClose.accessToken = MyConfig.usr_token;
                String str = gson.toJson(jsonSendTopicActivityClose);
               MyLog.d("kwwl","关闭报名时向服务器提交的数据是================="+str);
               MyLog.d("kwwl","关闭报名服务器接口是================="+MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_ACTIVIY + "/" + resouceId+"/close");
                final String ret = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_ACTIVIY + "/" + resouceId+"/close", str);
                MyLog.d("kwwl","关闭报名时返回结果是================="+ret);

                final JsonSendTopicActivityRet jsonSendTopicActivityRet = gson.fromJson(ret, JsonSendTopicActivityRet.class);

                if (jsonSendTopicActivityRet != null && jsonSendTopicActivityRet.code == MyConfig.retSuccess()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            msgPageCount = 0;
                            initSubRoomByHttpGet();
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sub_room, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
//                MyConfig.cleanAndFinishActivity(this);
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void showPlusDialog() {
        MyConfig.hide_keyboard_must_call_from_activity(ActivitySubGroup.this);
            hideEmojiDialog();
        if (MyConfig.plusInvokeMode == MyConfig.PLUS_POPUP_MENU) {
            PopupMenu popup = new PopupMenu(this, ivPlus);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.menu_plus_topic_comment, popup.getMenu());
            popup.show();

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_plus_camera:
                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                            openCamera();
                            return true;
                        case R.id.menu_plus_gallery:
                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                            openFileChooser(MyConfig.MIME_IMAGE_STAR);
                            return true;
                        default:
                            return false;
                    }
                }
            });

            /*popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                @Override
                public void onDismiss(PopupMenu menu) {
                    ((ToggleButton) view).setChecked(false);
                }
            });*/
        } else if (MyConfig.plusInvokeMode == MyConfig.PLUS_DIALOG) {
            new DialogFragment() {
                @Override
                public Dialog onCreateDialog(Bundle savedInstanceState) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            getActivity());
                    builder.setTitle("Please choose:")
                            .setItems(R.array.subjects, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    // of the selected item
                                    switch (which) {
                                        case 0:
                                            MyLog.d("", "directroom: item 0");
//                                            openSubjectInput(MyConfig.REQUEST_SUBJECT_ONLY);
                                            return;
                                        case 1:
                                            MyLog.d("", "directroom: item 1");
//                                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
//                                            openCamera();
                                            return;
                                        case 2:
                                            MyLog.d("", "directroom: item 2");
//                                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
//                                            openFileChooser(MyConfig.MIME_IMAGE_STAR);
                                            return;
                                        case 3:
                                            MyLog.d("", "directroom: item 3");
//                                            openMap();
                                            return;
                                        case 4:
                                            MyLog.d("", "directroom: item 4");
//                                            tempAttachMime = MyConfig.MIME_FILE_STAR;
//                                            openFileChooser(MyConfig.MIME_FILE_STAR);
                                            return;
                                        case 5:
                                            MyLog.d("", "directroom: item 5");
//                                            openSetupVote(MyConfig.REQUEST_CREATE_VOTE);
                                            return;
                                        default:
                                            return;
                                    }
                                }
                            })
                            .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    return builder.create();
                }
            }.show(getFragmentManager(), "");
        } else if (MyConfig.plusInvokeMode == MyConfig.PLUS_MYDIALOG) {
            createDialog(ActivitySubGroup.this, R.style.custom_dialog2);
        }else if(MyConfig.plusInvokeMode == MyConfig.PLUS_UNIVERSAL){

            subject_button_universal_layout.setVisibility(View.VISIBLE);

        }

    }

    public Dialog createDialog(Context context, int style) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout dialogView = (LinearLayout) inflater.inflate(R.layout.dialog_popmenu_layout, null);
        final Dialog customDialog = new Dialog(context, style);
        TextView cameraText, pictureText, mapText, cancleText;
        cameraText = (TextView) dialogView.findViewById(R.id.camera);
        pictureText = (TextView) dialogView.findViewById(R.id.picture);
        mapText = (TextView) dialogView.findViewById(R.id.map);
        cancleText = (TextView) dialogView.findViewById(R.id.cancle);
        WindowManager.LayoutParams localLayoutParams = customDialog.getWindow().getAttributes();
        localLayoutParams.gravity = Gravity.BOTTOM | Gravity.LEFT;
        localLayoutParams.x = 0;
        localLayoutParams.y = 0;
        dialogView.setMinimumWidth(MyConfig.getWidthByScreenPercent(100));
        customDialog.onWindowAttributesChanged(localLayoutParams);
        customDialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                        | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        customDialog.setCanceledOnTouchOutside(false);
        customDialog.setCancelable(true);
        customDialog.setCanceledOnTouchOutside(true);
        customDialog.setContentView(dialogView);
        cameraText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                openCamera();
                customDialog.cancel();
            }
        });

        mapText.setVisibility(View.GONE);

        pictureText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                openFileChooser(MyConfig.MIME_IMAGE_STAR);
                customDialog.cancel();
            }
        });

        cancleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.cancel();
            }
        });
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (!activity.isFinishing()) {
                customDialog.show();
            }
        }
        return customDialog;
    }

    public void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        tempAttach = MyConfig.getCameraFileBigPicture();
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(tempAttach));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, MyConfig.REQUEST_CAMERA);
        }
    }


    private void openFileChooser(String mime) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setType(mime);
        if (mime.equals(MyConfig.MIME_IMAGE_STAR)) {
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, MyConfig.REQUEST_GALLERY);
            }
        } else if (mime.equals(MyConfig.MIME_FILE_STAR)) {
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, MyConfig.REQUEST_FILE);
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /**在报名页面输入报名人数后返回，*/
        if (requestCode == REQUEST_CODE_INPUT_PEOPLE_NUM && resultCode == RESULT_OK) {
            la_msg.viewActivityInfoSubroom.activity_people_num.setText(data.getStringExtra("result"));
        }

//
//        // ========= 第一部分是各种主题创建的返回 ================
//
//        /**
//         * 第四类：创建定位地图，再创建话题
//         */
//        if (requestCode == MyConfig.REQUEST_MAP && resultCode == RESULT_OK) {
//
//            /**
//             * 地图发送走image接口
//             */
//            IMsg ims = formSubject();
//            ims.type = MyConfig.TYPE_TOPIC_MAP_DIRECTROOM;
//            ims.mapLocation = MyConfig.tempMapLocation;
//
//            sendImageComment(ims);
//
////            openSubjectInput(MyConfig.REQUEST_SUBJECT_WITH_MAP);
//
//        }
//        /**
//         * 带定位地图的话题创建
//         */
//        if (requestCode == MyConfig.REQUEST_SUBJECT_WITH_MAP && resultCode == RESULT_OK) {
//
//            /**
//             * 私信没有subject，所以不会到这里了
//             */
//
//            IMsg ims = formSubjectFromIntent(data);
//            ims.type = MyConfig.TYPE_TOPIC;
//            ims.subType = MyConfig.SUBTYPE_MAP;
//            ims.mapLocation = MyConfig.tempMapLocation;
//
//            sendSubjectMap(ims);
//
//            //debug
////            addSubject2Local(ims);
//        }
//
//        /**
//         * 第三类：创建投票，再创建话题
//         */
//        if (requestCode == MyConfig.REQUEST_CREATE_VOTE && resultCode == RESULT_OK) {
//
////            openSubjectInput(MyConfig.REQUEST_SUBJECT_WITH_VOTE);
//
//            IMsg ims = formSubjectFromIntent(data);
//            ims.title = MyConfig.tempVote.title;
//            ims.type = MyConfig.TYPE_TOPIC;
//            ims.subType = MyConfig.SUBTYPE_VOTE;
//            ims.vote = MyConfig.tempVote;
//
//            //投票时可能附带了图片，也可能没有
//            String attachedPhoto = data.getStringExtra("tempAttach");
//            if (attachedPhoto != null) {
//                ims.attached = new File(attachedPhoto);
//            }
//
//            sendSubjectVote(ims);
//        }
//        /**
//         * 带投票的话题创建
//         */
//        if (requestCode == MyConfig.REQUEST_SUBJECT_WITH_VOTE && resultCode == RESULT_OK) {
//
//            IMsg ims = formSubjectFromIntent(data);
//            ims.type = MyConfig.TYPE_TOPIC;
//            ims.subType = MyConfig.SUBTYPE_VOTE;
//            ims.vote = MyConfig.tempVote;
//
//            sendSubjectVote(ims);
//
//        }
//
//        /**
//         * 第二类：先获取附件，包括拍摄／图片／文件，再创建话题
//         */
//        if (requestCode == MyConfig.REQUEST_FILE && resultCode == RESULT_OK) {
//            Uri fileUri = data.getData();
//            String realPath = MyConfig.getUriPath(this, fileUri);
////            MyConfig.sendSysMsg(imsMainRoomRoot, MyConfig.getUriPath(this, fileUri));
//            tempAttach = new File(realPath);
//
//            openSubjectInput(MyConfig.REQUEST_SUBJECT_WITH_IMAGE);
//        }

        /**
         * @某人回来
         */
        if (requestCode == MyConfig.PEG_SOME_BODY_REQUSET_CODE ) {
            if(resultCode == RESULT_OK){
                MyLog.d("kwwl", MyConfig.PEG_SOME_BODY_USER_ID+"被@了");
                targets.add(MyConfig.PEG_SOME_BODY_USER_ID);
                String s = et.getText().toString() + MyConfig.PEG_SOME_BODY_USER_NICK_NAME+" ";
                et.setText(s);
                et.setSelection(s.length());
            }

            MyConfig.show_keyboard_must_call_from_activity(ActivitySubGroup.this);  
        }

        if (requestCode == MyConfig.REQUEST_GALLERY && resultCode == RESULT_OK) {

            Uri fileUri = data.getData();
            String realPath = MyConfig.getUriPath(this, fileUri);
            tempAttach = new File(realPath);

            formImageCommentAndSend();

//
////            openSubjectInputWithAttachImage(MyConfig.REQUEST_SUBJECT_WITH_IMAGE, tempAttach);..
//
//            if (MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {
//                /**
//                 * 去掉，聊天室上传图片时不需要裁剪
//                 */
//                Intent innerIntent = new Intent("com.android.camera.action.CROP");
//                innerIntent.setDataAndType(data.getData(), MyConfig.MIME_IMAGE_STAR);
//                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true");// 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
//                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
//                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
//                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
//                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
//                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
//                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
//                startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);
//            }
        }

        if (requestCode == MyConfig.REQUEST_CAMERA && resultCode == RESULT_OK) {

            formImageCommentAndSend();
        }

//
//        if (MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {
//            /**
//             * 以下代码接受裁剪后的图片
//             * 不过在主聊天室里面不需要裁剪图片
//             */
//            if (requestCode == MyConfig.REQUEST_IMAGE_CROP && resultCode == RESULT_OK) {
//
//                Bitmap bitmap = data.getParcelableExtra("data");
//
//                tempAttach = new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg");
//
//                // 图像保存到文件中
//                FileOutputStream foutput = null;
//                try {
//                    foutput = new FileOutputStream(tempAttach);
//                    if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, foutput)) {
////                        Toast.makeText(this,
////                                "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath(),
////                                Toast.LENGTH_LONG).show();
//                        MyConfig.MyToast(-1, this,
//                                "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath());
//                    }
//                } catch (FileNotFoundException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//
//
//                openSubjectInput(MyConfig.REQUEST_SUBJECT_WITH_IMAGE);
//            }
//        }

//
//        /**
//         * 带附件的话题创建，包括拍摄／图片／文件
//         */
//        if (requestCode == MyConfig.REQUEST_SUBJECT_WITH_IMAGE && resultCode == RESULT_OK) {
//
//
//        }
//
//        /**
//         * 第一类：单纯的话题创建
//         */
//        if (requestCode == MyConfig.REQUEST_SUBJECT_ONLY && resultCode == RESULT_OK) {
//
//            IMsg ims = formSubjectFromIntent(data);
//            sendSubject(ims);
////            la_msg.notifyDataSetChanged();
//
//        }

    }


    private void formImageCommentAndSend() {
        IMsg ims = formSubject();
        ims.type = MyConfig.TYPE_TOPIC_COMMANT;
        ims.attachFileBeforeSend2Server(tempAttach, tempAttachMime);
        sendImageComment(ims);
    }


    private IMsg formSubject() {

        IMsg imsCreateSubject = new IMsg(
                MyConfig.usr_id,
                MyConfig.usr_nickname,
                MyConfig.usr_avatar,

                Calendar.getInstance().getTime(),

                MyConfig.LEVEL_MSG_2,
                subRoomRootNode,
                MyConfig.MSG_CATEGORY_SUBJECT_COMMENT,

                groupId,
                topicId,
                MyConfig.formClientCheckId(groupId),

                null,
                null
        );
//        imsMainRoomRoot.ll_sub_msg.addLast(imsCreateSubject);
        return imsCreateSubject;
    }


    public void sendImageComment(final IMsg iMsg) {

        if (!MyConfig.debug_msg_no_local_first) {
            MyLog.d("", " sendImageComment2Local ");
            sendImageComment2Local(iMsg);
        }
        sendImageComment2Server(iMsg);
    }


    private void sendImageComment2Local(IMsg msg) {
        msg.setLocalSendTag();
        socketAddTopicComment2Local(msg);
    }

    public void sendImageComment2Server(final IMsg iMsg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Json_SubRoom_SendCommentImageRet json_subRoom_sendCommentImageRet = HttpTools.okhttpUploadCommentImage(iMsg, MyConfig.MIME_IMAGE_STAR);

                MyLog.d("", "json_subRoom_sendCommentImageRet: " + json_subRoom_sendCommentImageRet.toString());

                if (json_subRoom_sendCommentImageRet != null && json_subRoom_sendCommentImageRet.code != MyConfig.retSuccess()) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            Toast.makeText(
//                                    MyConfig.activityMainGroup,
//                                    json_directRoom_sendTopicImageRet.message,
//                                    Toast.LENGTH_LONG).show();
                            MyConfig.MyToast(0, ActivitySubGroup.this,
                                    json_subRoom_sendCommentImageRet.message);
                        }
                    });
                    MyLog.d("", "json_subRoom_sendCommentImageRet: " + json_subRoom_sendCommentImageRet.toString());

                }
            }
        }).start();
    }

    /**
     * 当报名人满处于淘汰状态时弹出一个dialog给予提示
     */
    public void showActivityWaitHintDialog(){
         new Dialog(this) {
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                requestWindowFeature(Window.FEATURE_NO_TITLE);
                View view = View.inflate(ActivitySubGroup.this, R.layout.dialog_activity_wait_hint, null);
                ImageView imageView = (ImageView) view.findViewById(R.id.dialog_activity_wait_hint_dismiss);
                setContentView(view);
                Window window = this.getWindow();
                WindowManager.LayoutParams params = window.getAttributes();
                params.gravity = Gravity.CENTER;
                window.setBackgroundDrawableResource(R.color.trans_dialog);
                try {
                    params.width =  getResources().getDrawable(R.drawable.activity_wait_hint_3x).getIntrinsicWidth();
                    params.height =  getResources().getDrawable(R.drawable.activity_wait_hint_3x).getIntrinsicHeight();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }

                window.setAttributes(params);
                view.findViewById(R.id.dialog_activity_wait_hint_dismiss).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       dismiss();
                    }
                });
            }
        }.show();
    }
//
//    public void httpAddImageIMsg2ImageList(IMsg iMsgListRoot, IMsg iMsg) {
//
//        if (iMsg.subType == MyConfig.SUBTYPE_IMAGE) {
//            iMsgListRoot.ll_sub_msg_with_image.addFirst(iMsg);
//        }
//
//        if (iMsg.subType == MyConfig.SUBTYPE_VOTE) {
//            if (iMsg.thumbNail != null) {
//                iMsgListRoot.ll_sub_msg_with_image.addFirst(iMsg);
//            }
//        }
//
//    }
//
//    public void addImageIMsg2ImageList(IMsg iMsgListRoot, IMsg iMsg) {
//
//        /**
//         * gallery activity显示图片时有一个list的复制操作，所以这里修改ll_sub_msg_with_image可以不在ui线程里面
//         */
//
//        if (iMsg.subType == MyConfig.SUBTYPE_IMAGE) {
//            iMsgListRoot.ll_sub_msg_with_image.addLast(iMsg);
//        }
//
//        if (iMsg.subType == MyConfig.SUBTYPE_VOTE) {
//            if (iMsg.thumbNail != null) {
//                iMsgListRoot.ll_sub_msg_with_image.addLast(iMsg);
//            }
//        }
//
//    }
//
//
//    public void setImageIMsg2ImageList(IMsg iMsgListRoot, IMsg iMsg, int index) {
//
//        /**
//         * gallery activity显示图片时有一个list的复制操作，所以这里修改ll_sub_msg_with_image可以不在ui线程里面
//         */
//
//        if (iMsg.subType == MyConfig.SUBTYPE_IMAGE) {
//            iMsgListRoot.ll_sub_msg_with_image.set(index, iMsg);
//        }
//
//        if (iMsg.subType == MyConfig.SUBTYPE_VOTE) {
//            if (iMsg.thumbNail != null) {
//                iMsgListRoot.ll_sub_msg_with_image.set(index, iMsg);
//            }
//        }
//
//    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.subroom_plus:
                showPlusDialog();
                break;
            case R.id.subroom_input_edit:
                hideEmojiDialog();
                break;
            case R.id.subroom_iv_add_emoji:
                if(emojiIsVisible){
                    hideEmojiDialog();
                    EmojiUtils.show_keyboard_must_call_from_activity(ActivitySubGroup.this);
                }else{
                    showEmojiDialog();
                }
                break;
        }
    }


    /**
     * 初始化距离传感器
     */
    private void initAudioMode() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);/**得到传感器管理器*/
        mAudioSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);/**距离传感器对象*/

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);/**得到音频管理器*/
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        mySensorEventListener = new MySensorEventListener();
        mSensorManager.registerListener(mySensorEventListener, mAudioSensor,SensorManager.SENSOR_DELAY_NORMAL);
    }

    /**
     * 定义听筒距离监听器，根据距离判断使用听筒模式还是音响模式
     */
    class MySensorEventListener implements SensorEventListener {

        @Override
        public void onSensorChanged(SensorEvent event) {
            float range = event.values[0];
            if (range == mAudioSensor.getMaximumRange()) {
                audioManager.setMode(AudioManager.MODE_NORMAL);
            } else {
                audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    }


}
