package co.docy.oldfriends.Activitis;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventAppVersionCheckInfo;
import co.docy.oldfriends.EventBus.EventHeartOneSecond;
import co.docy.oldfriends.EventBus.EventSwitchServer;
import co.docy.oldfriends.EventBus.EventSwitchSocketServer;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;


public class ActivityAdvanceSetting extends ActivityBase {

    TextView advance_setting_version_code;
    TextView advance_setting_version_name;
    TextView advance_setting_version_code_server;
    TextView advance_setting_version_name_server;
    TextView advance_setting_app_download_url;
    TextView advance_setting_compile_info;
    Button advance_setting_version_update;
    Button advance_setting_app_download_update;

    Switch advance_setting_quick_menu;
    Switch advance_setting_msg_show_debug;
    Switch advance_setting_use_toggle_button_mainroom;
    Switch advance_setting_close_debug_toast;
    Switch advance_setting_auto_fill_validcode;
    Switch advance_setting_show_not_joined_company;
    Switch advance_setting_repeat_clientid_check;
    Switch advance_setting_msg_no_local_first;
    Switch advance_setting_using_inner_server;
    LinearLayout advance_setting_server_choose;
    Switch advance_setting_not_using_api_v2;

    Button advance_setting_test_button_1;
    Button advance_setting_test_button_2;
    Button advance_setting_test_button_3;


    private class ServerOption extends LinearLayout {

        public int mIndex;
        public String mServer;
        public boolean mSelected;

        public TextView update_vote_option_num;
        public TextView update_vote_option_tv;
        public ImageView update_vote_option_tb;


        public ServerOption(Context context, final int index, String server, boolean b) {
            super(context);
            this.mIndex = index;
            this.mServer = server;
            this.mSelected = b;

            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.row_vote_option_update, this, true);

            update_vote_option_num = (TextView)v.findViewById(R.id.update_vote_option_num);
            update_vote_option_num.setText(""+index);
            update_vote_option_tv = (TextView)v.findViewById(R.id.update_vote_option_tv);
            if(index == 0) {
                update_vote_option_tv.setText(server + " (default)");
            }else{
                update_vote_option_tv.setText(server);
            }
            update_vote_option_tb = (ImageView)v.findViewById(R.id.update_vote_option_tb);
            setChecked(b);
            update_vote_option_tb.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new EventSwitchServer(index));
                    setChecked(true);
                }
            });

        }

        public void setChecked(boolean selected) {

            if(selected){
                update_vote_option_tb.setBackground(getResources().getDrawable(R.drawable.switch_selected_3x));
            }else{
                update_vote_option_tb.setBackground(getResources().getDrawable(R.drawable.switch_normal_3x));
            }
        }

    }

    public void onEventMainThread(EventSwitchServer eventSwitchServer) {

        int count = advance_setting_server_choose.getChildCount();
        for(int i = 0; i < count; i++){
            if(i != eventSwitchServer.server_index) {
                ((ServerOption) (advance_setting_server_choose.getChildAt(i))).setChecked(false);
            }
        }

        MyConfig.debug_using_inner_server = eventSwitchServer.server_index;
        MyConfig.saveDebugSettingToPreference(this);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advance_setting);

        MyConfig.loadDebugSettingFromPreference(ActivityAdvanceSetting.this);

        advance_setting_version_code = (TextView) findViewById(R.id.advance_setting_version_code);
        advance_setting_version_code.setText("local versionCode " + MyConfig.appVersion_from_local.versionCode);
        advance_setting_version_name = (TextView) findViewById(R.id.advance_setting_version_name);
        advance_setting_version_name.setText("local versionName " + MyConfig.appVersion_from_local.versionName);

        advance_setting_version_code_server = (TextView) findViewById(R.id.advance_setting_version_code_server);
        advance_setting_version_name_server = (TextView) findViewById(R.id.advance_setting_version_name_server);

        advance_setting_app_download_url = (TextView) findViewById(R.id.advance_setting_app_download_url);
        advance_setting_app_download_update = (Button) findViewById(R.id.advance_setting_app_download_update);
        advance_setting_app_download_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MyLog.d("","advance_setting_app_download_url is clicked");

                MyConfig.updateAppDownloadUrl(new MyConfig.CallBack_IfSuccess() {
                    @Override
                    public void ifSuccess() {
                        MyLog.d("","advance_setting_app_download_url updated success");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                advance_setting_app_download_url.setText("" + MyConfig.appDownloadUrl);
                            }
                        });
                    }

                    @Override
                    public void ifFailed(){
                        MyLog.d("","advance_setting_app_download_url fail to update");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                advance_setting_app_download_url.setText("获取更新地址失败。可以点击再次尝试");
                            }
                        });

                    }
                });
            }
        });


        advance_setting_compile_info = (TextView) findViewById(R.id.advance_setting_compile_info);
        advance_setting_compile_info.setText("当前编译版本:" + (MyConfig.compile_type_release ? "release":"debug"));

        advance_setting_version_update = (Button) findViewById(R.id.advance_setting_version_update);
        advance_setting_version_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConfig.appVersionCheck();
            }
        });

        advance_setting_quick_menu = (Switch) findViewById(R.id.advance_setting_quick_menu);
        advance_setting_quick_menu.setChecked(MyConfig.debug_quick_menu);
        advance_setting_quick_menu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MyConfig.debug_quick_menu = true;
                    MyConfig.activityViewPager.updatePagerMenu(0);
                } else {
                    MyConfig.debug_quick_menu = false;
                    MyConfig.activityViewPager.updatePagerMenu(0);
                }
                MyConfig.saveDebugSettingToPreference(ActivityAdvanceSetting.this);
            }
        });
        advance_setting_msg_show_debug = (Switch) findViewById(R.id.advance_setting_msg_show_debug);
        advance_setting_msg_show_debug.setChecked(MyConfig.debug_in_msg);
        advance_setting_msg_show_debug.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MyConfig.debug_in_msg = true;
                    MyConfig.activityViewPager.updatePagerMenu(0);
                } else {
                    MyConfig.debug_in_msg = false;
                    MyConfig.activityViewPager.updatePagerMenu(0);
                }
                MyConfig.saveDebugSettingToPreference(ActivityAdvanceSetting.this);
            }
        });
        advance_setting_use_toggle_button_mainroom = (Switch) findViewById(R.id.advance_setting_use_toggle_button_mainroom);
        advance_setting_use_toggle_button_mainroom.setChecked(MyConfig.debug_float_button_mainroom);
        advance_setting_use_toggle_button_mainroom.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MyConfig.debug_float_button_mainroom = true;
                } else {
                    MyConfig.debug_float_button_mainroom = false;
                }
                MyConfig.saveDebugSettingToPreference(ActivityAdvanceSetting.this);
            }
        });
        advance_setting_close_debug_toast = (Switch) findViewById(R.id.advance_setting_close_debug_toast);
        advance_setting_close_debug_toast.setChecked(MyConfig.debug_toast_all);
        advance_setting_close_debug_toast.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MyConfig.debug_toast_all = true;
                } else {
                    MyConfig.debug_toast_all = false;
                }
                MyConfig.saveDebugSettingToPreference(ActivityAdvanceSetting.this);
            }
        });

        advance_setting_auto_fill_validcode = (Switch) findViewById(R.id.advance_setting_auto_fill_validcode);
        advance_setting_auto_fill_validcode.setChecked(MyConfig.debug_autofill_validcode);
        advance_setting_auto_fill_validcode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MyConfig.debug_autofill_validcode = true;
                } else {
                    MyConfig.debug_autofill_validcode = false;
                }
                MyConfig.saveDebugSettingToPreference(ActivityAdvanceSetting.this);
            }
        });


        advance_setting_show_not_joined_company = (Switch) findViewById(R.id.advance_setting_show_not_joined_company);
        advance_setting_show_not_joined_company.setChecked(MyConfig.debug_show_not_joined_company);
        advance_setting_show_not_joined_company.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MyConfig.debug_show_not_joined_company = true;
                } else {
                    MyConfig.debug_show_not_joined_company = false;
                }
                MyConfig.saveDebugSettingToPreference(ActivityAdvanceSetting.this);
            }
        });

        advance_setting_repeat_clientid_check = (Switch) findViewById(R.id.advance_setting_repeat_clientid_check);
        advance_setting_repeat_clientid_check.setChecked(MyConfig.debug_last_clientid_via_socket_repeat_check);
        advance_setting_repeat_clientid_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MyConfig.debug_last_clientid_via_socket_repeat_check = true;
                } else {
                    MyConfig.debug_last_clientid_via_socket_repeat_check = false;
                }
                MyConfig.saveDebugSettingToPreference(ActivityAdvanceSetting.this);
            }
        });


        advance_setting_msg_no_local_first = (Switch) findViewById(R.id.advance_setting_msg_sendto_local_first);
        advance_setting_msg_no_local_first.setChecked(MyConfig.debug_msg_no_local_first);
        advance_setting_msg_no_local_first.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MyConfig.debug_msg_no_local_first = true;
                } else {
                    MyConfig.debug_msg_no_local_first = false;
                }
                MyConfig.saveDebugSettingToPreference(ActivityAdvanceSetting.this);
            }
        });

//
//        advance_setting_using_inner_server = (Switch) findViewById(R.id.advance_setting_using_inner_server);
//        advance_setting_using_inner_server.setChecked(MyConfig.debug_using_inner_server);
//        advance_setting_using_inner_server.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    MyConfig.debug_using_inner_server = true;
////                    MyConfig.debug_not_using_api_v2 = true;
//                    EventBus.getDefault().post(new EventSwitchSocketServer());
//                } else {
//                    MyConfig.debug_using_inner_server = false;
////                    MyConfig.debug_not_using_api_v2 = false;
//                    EventBus.getDefault().post(new EventSwitchSocketServer());
//                }
//                advance_setting_not_using_api_v2.setChecked(MyConfig.debug_not_using_api_v2);
//                MyConfig.saveDebugSettingToPreference(ActivityAdvanceSetting.this);
//            }
//        });

        advance_setting_server_choose = (LinearLayout)findViewById(R.id.advance_setting_server_choose);
        for(int i = 0; i < MyConfig.servers.length; i++){
            advance_setting_server_choose.addView(
                    new ServerOption(this, i, MyConfig.servers[i], (i==MyConfig.debug_using_inner_server)?true:false),
                    i);
        }


        advance_setting_server_choose = (LinearLayout)findViewById(R.id.advance_setting_server_choose);


        advance_setting_not_using_api_v2 = (Switch) findViewById(R.id.advance_setting_not_using_api_v2);
//        advance_setting_not_using_api_v2.setClickable(false);
        advance_setting_not_using_api_v2.setChecked(MyConfig.debug_not_using_api_v2);
        advance_setting_not_using_api_v2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    MyConfig.debug_not_using_api_v2 = true;
                    EventBus.getDefault().post(new EventSwitchSocketServer());
                } else {
                    MyConfig.debug_not_using_api_v2 = false;
                    EventBus.getDefault().post(new EventSwitchSocketServer());
                }
                MyConfig.saveDebugSettingToPreference(ActivityAdvanceSetting.this);
            }
        });

        advance_setting_test_button_1 = (Button) findViewById(R.id.advance_setting_test_button_1);
        advance_setting_test_button_1.setText(getResources().getString(R.string.open_guid_again));
        advance_setting_test_button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyConfig.saveIntToPreference(ActivityAdvanceSetting.this, "app_open_count_newbie_guide_1", -1);
                MyConfig.saveIntToPreference(ActivityAdvanceSetting.this, "app_open_count_newbie_guide_2", -1);
                MyConfig.saveIntToPreference(ActivityAdvanceSetting.this, "app_open_count_newbie_guide_3", -1);
                MyConfig.saveIntToPreference(ActivityAdvanceSetting.this, "app_open_count", -1);

            }
        });

        advance_setting_test_button_2 = (Button) findViewById(R.id.advance_setting_test_button_2);
        advance_setting_test_button_2.setText("产生一次异常");
        advance_setting_test_button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView textView = new TextView(ActivityAdvanceSetting.this);
                textView.setText("");
                LinkedList<String> ll = new LinkedList<String>();
                ll.get(0).toString();

//                Intent intent = new Intent(ActivityAdvanceSetting.this, ActivityVideoPlayer.class);
//                intent.putExtra("mode", 0);
//                intent.putExtra("path", MyConfig.getApiDomain_NoSlash_GetResources() + "/uploads/file-1468984454592-17.mp4");
//                startActivity(intent);


            }
        });



        advance_setting_test_button_3 = (Button) findViewById(R.id.advance_setting_test_button_3);
        advance_setting_test_button_3.setText("未使用");
        advance_setting_test_button_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(ActivityAdvanceSetting.this, ActivityVideoPlayer.class);
//                intent.putExtra("mode", 1);
//                intent.putExtra("path", MyConfig.appCacheDirPath + "/VID_20160720.mp4");
//                startActivity(intent);

//                MyConfig.getMoreCoursesBySlot("2016-01-01", 4);

            }
        });

//
//        Observer<String> observer = new Observer<String>() {
//            @Override
//            public void onNext(String s) {
//                MyLog.d("", "Item: " + s);
//            }
//
//            @Override
//            public void onCompleted() {
//                MyLog.d("", "Completed!");
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                MyLog.d("", "Error!");
//            }
//        };
//
//
//        Subscriber<String> subscriber = new Subscriber<String>() {
//            @Override
//            public void onNext(String s) {
//                MyLog.d("", "Item: " + s);
//            }
//
//            @Override
//            public void onCompleted() {
//                MyLog.d("", "Completed!");
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                MyLog.d("", "Error!");
//            }
//        };
//
//        Observable observable = Observable.create(new Observable.OnSubscribe<String>() {
//            @Override
//            public void call(Subscriber<? super String> subscriber) {
//                subscriber.onNext("Hello");
//                subscriber.onNext("Hi");
//                subscriber.onNext("Aloha");
//                subscriber.onCompleted();
//            }
//        });
//
//        class AAA implements Observable.OnSubscribe<String>{
//
//            Subscriber<? super String> subscriber;
//
//            @Override
//            public void call(Subscriber<? super String> subscriber) {
//                this.subscriber = subscriber;
//                subscriber.onNext("Hello");
//                subscriber.onNext("Hi");
//                subscriber.onNext("Aloha");
//                subscriber.onCompleted();
//            }
//        }
//        Observable observable2 = Observable.create(new AAA());

    }


    public void onEventMainThread(EventHeartOneSecond eventHeartOneSecond) {

//        advance_setting_compile_info.setText("" + recordAudioFile.length());

    }

    public void onEventMainThread(EventAppVersionCheckInfo eventAppVersionCheckInfo) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                advance_setting_version_code.setText("local versionCode " + MyConfig.appVersion_from_local.versionCode);
                advance_setting_version_name.setText("local versionName " + MyConfig.appVersion_from_local.versionName);
                advance_setting_version_code_server.setText("server versionCode " + MyConfig.appVersion_from_server_current.versionCode);
                advance_setting_version_name_server.setText("server versionName " + MyConfig.appVersion_from_server_current.versionName);
            }
        });
    }


}
