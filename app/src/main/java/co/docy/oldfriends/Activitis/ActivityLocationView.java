package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.Window;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.R;


public class ActivityLocationView extends ActivityBase  {
    private AMap aMap;
    private MapView mapView;

    double longitude;//经度
    double latitude;//纬度
    String address;//地址

    boolean isMapLoad = false;
    private float zoomLevel = 16;

    AMap.OnMapLoadedListener mOnMapLoadedListener = new AMap.OnMapLoadedListener() {
        @Override
        public void onMapLoaded() {
            isMapLoad = true;
        }
    };

    private void drawMyLocation(final String title, final double latitude,
                                final double longitude) {
        if (isMapLoad) {
            aMap.clear();
            LatLng latLng = new LatLng(latitude, longitude);
            MarkerOptions markerOption = new MarkerOptions();
            if(title != null && title.trim().length() > 0) {
                markerOption.title(title);
            }else{
                markerOption.title(getResources().getString(R.string.my_location));
            }
//            markerOption.snippet(getResources().getString(R.string.latitude) + latitude + getResources().getString(R.string.longitude) + longitude);
            markerOption.position(latLng);
            markerOption.perspective(true);
            markerOption.draggable(true);
            markerOption.anchor(0.5f, 0.5f);
            Marker marker = aMap.addMarker(markerOption);
            marker.showInfoWindow();
            marker.setVisible(true);

            aMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(latLng, zoomLevel, 0, 0)));
        } else {
            new Handler().postDelayed(new Runnable() {
                /**
                 * 留意，如果activity突然结束，这里会不会导致意外？
                 */
                @Override
                public void run() {
                    drawMyLocation(title, latitude, longitude);
                }
            }, 1000);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);// 不显示程序的标题栏
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_view);

        MyConfig.activityLocationView = this;

        mapView = (MapView) findViewById(R.id.location_view);
        mapView.onCreate(savedInstanceState);// 此方法必须重写
        init();
    }

    /**
     * 初始化
     */
    private void init() {
        if (aMap == null) {
            aMap = mapView.getMap();
            setUpMap();
        }
    }

    /**
     * 设置一些amap的属性
     */
    private void setUpMap() {
//        aMap.setLocationSource(locationSource);// 设置定位监听
        aMap.getUiSettings().setMyLocationButtonEnabled(true);// 设置默认定位按钮是否显示
        aMap.setMyLocationEnabled(false);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
        // 设置定位的类型为定位模式 ，可以由定位、跟随或地图根据面向方向旋转几种
        aMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);

        aMap.setOnMapLoadedListener(mOnMapLoadedListener);

        latitude = MyConfig.tempMapLocation.latitude;
        longitude = MyConfig.tempMapLocation.longitude;
        address = MyConfig.tempMapLocation.address;

        drawMyLocation(address, latitude, longitude);
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
//        locationSource.deactivate();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        finish();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_map_location, menu);
////        return true;
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
