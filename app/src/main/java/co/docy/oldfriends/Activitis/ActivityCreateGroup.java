package co.docy.oldfriends.Activitis;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Fragments.FragmentCreateGroup;
import co.docy.oldfriends.R;


public class ActivityCreateGroup extends ActivityBase {

    private FragmentCreateGroup fragmentCreateGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_room);

        init();

//        //EventBus.getDefault().register(this);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentCreateGroup = FragmentCreateGroup.newInstance();
        fragmentTransaction.add(R.id.activity_creategroup_linearLayout, fragmentCreateGroup);
        fragmentTransaction.commit();

    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        MyLog.d("", "hide_keyboard_must_call_from_activity in ActivityCreateGroup 1");
        super.onDestroy();

        MyLog.d("", "hide_keyboard_must_call_from_activity in ActivityCreateGroup 2");
    }

    private void init() {


    }

    /**
     * 由于在Fragment中的onActivityResult方法不执行，故在ACtivity中重写，然后调用Fragment中的方法。
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 31 && resultCode == Activity.RESULT_OK) {
            fragmentCreateGroup.forResult();
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
//    public void onEventMainThread(EventQuitGroup eventQuitGroup) {
//        finish();
//    }

}
