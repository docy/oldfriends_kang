package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonUpdatePersonInfo;
import co.docy.oldfriends.Messages.JsonUpdatePersonInfoRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;


public class _ActivityChangePassword extends ActivityBase implements View.OnClickListener {

    private EditText change_password_et_old, change_password_et_new, change_password_et_confirm;
    private Button mForgetPasswordBut, mSubmentBut;
    ImageView change_password_et_spliteLine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);


        init();
    }

    private void init() {

        change_password_et_old = (EditText) findViewById(R.id.change_password_et_old);

        change_password_et_new = (EditText) findViewById(R.id.change_password_et_new);

        change_password_et_confirm = (EditText) findViewById(R.id.change_password_et_confirm);

        mForgetPasswordBut = (Button) findViewById(R.id.forget_password_button);

        mSubmentBut = (Button) findViewById(R.id.change_password_subment_button);

        mSubmentBut.setOnClickListener(this);
        mForgetPasswordBut.setOnClickListener(this);

        //     change_password_et_spliteLine = (ImageView) findViewById(R.id.change_password_et_confirm_line);

    }


    public void backWithoutResult() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);

        finish();
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
//        Toast.makeText(this, "input is discarded", Toast.LENGTH_LONG).show();
        backWithoutResult();
    }

    private void backWithResult() {


        Intent intent = new Intent();
//        intent.putExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_TITLE, create_subject_title_et.getText().toString());
//        intent.putExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_BODY, create_subject_body_et.getText().toString());

        setResult(RESULT_OK, intent);

        finish();
    }

    private void updatePasswordToServer() {

        final String pwd_old = change_password_et_old.getText().toString().trim();
        if (pwd_old.length() == 0) {
            MyConfig.MyToast(0, this, getResources().getString(R.string.passowrd_null_remind));
            change_password_et_old.setText(pwd_old);
            return;
        }
        final String pwd_new = change_password_et_new.getText().toString().trim();
        if (pwd_new.length() == 0) {
            MyConfig.MyToast(0, this, getResources().getString(R.string.new_passowrd_null_remind));
            change_password_et_new.setText(pwd_new);
            return;
        }
        final String pwd_confirm = change_password_et_confirm.getText().toString().trim();
//        if (pwd_confirm.length() == 0) {
//            MyConfig.MyToast(0, this, "确认密码不能为空");
//            change_password_et_confirm.setText(pwd_confirm);
//            return;
//        }

        new Thread(new Runnable() {
            @Override
            public void run() {

                final JsonUpdatePersonInfo jsonUpdatePersonInfo = new JsonUpdatePersonInfo();
                jsonUpdatePersonInfo.accessToken = MyConfig.usr_token;
                jsonUpdatePersonInfo.oldpassword = pwd_old;
                jsonUpdatePersonInfo.password = pwd_new;
//                jsonUpdatePersonInfo.password = pwd_confirm;

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonUpdatePersonInfo, JsonUpdatePersonInfo.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_UPDATE, jsonStr);

                final JsonUpdatePersonInfoRet jsonUpdatePersonInfoRet = gson.fromJson(s, JsonUpdatePersonInfoRet.class);
                MyLog.d("", "HttpTools: " + s);
                if (jsonUpdatePersonInfoRet != null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (jsonUpdatePersonInfoRet.code==MyConfig.retSuccess()) {

                                MyConfig.usr_pwd = jsonUpdatePersonInfo.password;
                                MyConfig.prefEditor.putString(MyConfig.PREF_USER_PWD, MyConfig.usr_pwd);
                                MyConfig.prefEditor.commit();

                                backWithResult();
                            } else {
                                MyConfig.MyToast(0, _ActivityChangePassword.this, jsonUpdatePersonInfoRet.message);
                            }
                        }
                    });
                }

            }
        }).start();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //   getMenuInflater().inflate(R.menu.menu_update_submit, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                Toast.makeText(this, "home key is pressed, input is discarded", Toast.LENGTH_LONG).show();
                backWithoutResult();
                return true;
//            case R.id.update_submit:
//
//                updatePasswordToServer();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forget_password_button:
                Intent intent = new Intent(_ActivityChangePassword.this, _ActivityResetPassword.class);
                startActivity(intent);
                break;
            case R.id.change_password_subment_button:
                updatePasswordToServer();
                break;
        }
    }
}
