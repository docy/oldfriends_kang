package co.docy.oldfriends.Activitis;

import android.app.FragmentManager;
import android.os.Bundle;

import co.docy.oldfriends.Fragments.FragmentSearchContactPerson;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/5/4.
 */
public class ActivitySearchContactPerson extends ActivityBase {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_contact_person);

        FragmentManager fragmentManager = getFragmentManager();
        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragmentSearchContactPerson fragmentCompanyContact = FragmentSearchContactPerson.newInstance();
        fragmentTransaction.add(R.id.activity_search_contact_person_linear, fragmentCompanyContact);
        fragmentTransaction.commit();
    }
}
