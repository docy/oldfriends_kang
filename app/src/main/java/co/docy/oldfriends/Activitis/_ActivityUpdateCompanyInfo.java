package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonUpdateCompanyInfo;
import co.docy.oldfriends.Messages.JsonUpdateCompanyInfoRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;


public class _ActivityUpdateCompanyInfo extends ActivityBase {

    EditText company_update_et_name;
    EditText company_update_et_desc;
    private TextView remindText;
    String old_name;
    String old_desc;
    String itemName;
    private String tempString;
    private MyConfig.IOnOkClickListener iOnOkClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_update);
        Intent intent = getIntent();
//        Bundle bundle = intent.getExtras();
//        old_name = bundle.getString(MyConfig.CONSTANT_PARAMETER_STRING_NAME);
//        old_desc = bundle.getString(MyConfig.CONSTANT_PARAMETER_STRING_DESC);
        itemName = intent.getStringExtra("itemName");
        init();
    }

    private void init() {

        remindText = (TextView) findViewById(R.id.company_update_remind_text);

        company_update_et_name = (EditText) findViewById(R.id.company_update_et_name);
        company_update_et_name.setHint(getResources().getString(R.string.collective_name_null_remind));
        company_update_et_name.setText(MyConfig.jsonGetCompanyInfoRet.data.name);
        company_update_et_name.setSelection(MyConfig.jsonGetCompanyInfoRet.data.name.length());
        company_update_et_name.setMaxEms(20);

        company_update_et_desc = (EditText) findViewById(R.id.company_update_et_desc);
        company_update_et_desc.setHint(getResources().getString(R.string.collective_desc_null_remind));
        if(MyConfig.jsonGetCompanyInfoRet != null && MyConfig.jsonGetCompanyInfoRet.code==MyConfig.retSuccess()) {
            company_update_et_desc.setText(MyConfig.jsonGetCompanyInfoRet.data.desc);
            company_update_et_desc.setSelection(MyConfig.jsonGetCompanyInfoRet.data.desc.length());
        }
        company_update_et_desc.setMaxEms(60);
        /**
         * 根据目前UI设计，只能同时修改一个参数
         */
        company_update_et_name.setVisibility(View.GONE);
        company_update_et_desc.setVisibility(View.GONE);

        if (itemName.equals(MyConfig.CONSTANT_PARAMETER_STRING_NAME)) {
            company_update_et_name.setVisibility(View.VISIBLE);
            remindText.setText(getResources().getString(R.string.collective_name_null_remind));
            setTitle(getResources().getString(R.string.company_name_change));
        } else if (itemName.equals(MyConfig.CONSTANT_PARAMETER_STRING_DESC)) {
            company_update_et_desc.setVisibility(View.VISIBLE);
            remindText.setText(getResources().getString(R.string.collective_desc_null_remind));
            setTitle(getResources().getString(R.string.company_desc_change));
        }
    }


    public void backWithoutResult() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);

        finish();
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
//        Toast.makeText(this, "input is discarded", Toast.LENGTH_LONG).show();
        backWithoutResult();
    }

    private void backWithResult() {


        Intent intent = new Intent();
//        intent.putExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_TITLE, create_subject_title_et.getText().toString());
//        intent.putExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_BODY, create_subject_body_et.getText().toString());

        setResult(RESULT_OK, intent);

        finish();
    }

    private void updateCompanyInfoToServer() {
        MyLog.d("", "updateCompanyInfoToServer 1");


        new Thread(new Runnable() {
            @Override
            public void run() {
                MyLog.d("", "updateCompanyInfoToServer 2");

//                final JsonUpdateCompanyInfo jsonUpdateCompanyInfo = JsonUpdateCompanyInfo.fromJsonGetGroupUserRet(MyConfig.jsonGetCompanyInfoRet.data);
                final JsonUpdateCompanyInfo jsonUpdateCompanyInfo = new JsonUpdateCompanyInfo();

                if (itemName.equals(MyConfig.CONSTANT_PARAMETER_STRING_NAME)) {
                    tempString = company_update_et_name.getText().toString().trim();
                    if (tempString.length() > 20 || tempString.length() < 1) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MyConfig.MyToast(0, _ActivityUpdateCompanyInfo.this, getResources().getString(R.string.company_name_fail_remind));
                            }
                        });
                    } else {
                        jsonUpdateCompanyInfo.name = tempString;
                    }

                } else if (itemName.equals(MyConfig.CONSTANT_PARAMETER_STRING_DESC)) {
                    tempString = company_update_et_desc.getText().toString().trim();
                    if (tempString.length() > 60 || tempString.length() < 1) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MyConfig.MyToast(0, _ActivityUpdateCompanyInfo.this, getResources().getString(R.string.company_desc_fail_remind));
                            }
                        });
                    } else {
                        jsonUpdateCompanyInfo.desc = tempString;
                    }

                }

                jsonUpdateCompanyInfo.accessToken = MyConfig.usr_token;

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonUpdateCompanyInfo, JsonUpdateCompanyInfo.class);
                MyLog.d("", "HttpTools: jsonStr " + jsonStr);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_INFO_UPDATE + MyConfig.jsonGetCompanyInfoRet.data.id, jsonStr);

                final JsonUpdateCompanyInfoRet jsonUpdateCompanyInfoRet = gson.fromJson(s, JsonUpdateCompanyInfoRet.class);
                MyLog.d("", "HttpTools: " + s);
                if (jsonUpdateCompanyInfoRet != null && jsonUpdateCompanyInfoRet.code==MyConfig.retSuccess()) {
                    finish();
                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            iOnOkClickListener = new MyConfig.IOnOkClickListener() {
                                @Override
                                public void OnOkClickListener() {
                                }
                            };
                            MyConfig.showRemindDialog("",jsonUpdateCompanyInfoRet.message, _ActivityUpdateCompanyInfo.this,false,iOnOkClickListener);
                        }
                    });
                }

            }
        }).start();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_update_submit, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                Toast.makeText(this, "home key is pressed, input is discarded", Toast.LENGTH_LONG).show();

                backWithoutResult();
                return true;
            case R.id.update_submit:

                updateCompanyInfoToServer();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
