package co.docy.oldfriends.Activitis;


import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Calendar;

import co.docy.oldfriends.Adapters.ListAdapterMsg;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventGroupListUpdateFromServer;
import co.docy.oldfriends.EventBus.EventHeart200ms;
import co.docy.oldfriends.EventBus.EventMainroomGotoUnread;
import co.docy.oldfriends.EventBus.EventScreenSwitch;
import co.docy.oldfriends.EventBus.EventSetSoundRemind;
import co.docy.oldfriends.Media.Player;
import co.docy.oldfriends.Media.Recorder;
import co.docy.oldfriends.DataClass.IMsg;
import co.docy.oldfriends.Messages.JsonGetEventRet;
import co.docy.oldfriends.Messages.JsonGetMsgRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicCommentRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicRet;
import co.docy.oldfriends.Messages.JsonGroupResetUnread;
import co.docy.oldfriends.Messages.JsonJoinChatRoom;
import co.docy.oldfriends.Messages.JsonJoinChatRoomRet;
import co.docy.oldfriends.Messages.JsonLeaveChatRoom;
import co.docy.oldfriends.Messages.JsonLeaveChatRoomRet;
import co.docy.oldfriends.Messages.JsonSendMapRet;
import co.docy.oldfriends.Messages.JsonSendMsg;
import co.docy.oldfriends.Messages.JsonSendMsgRet;
import co.docy.oldfriends.Messages.JsonSendSoundMsgRet;
import co.docy.oldfriends.Messages.JsonSendTopic;
import co.docy.oldfriends.Messages.JsonSendTopicRet;
import co.docy.oldfriends.Messages.JsonSendTopicVoteCreate;
import co.docy.oldfriends.Messages.JsonSendTopicVoteCreateRet;
import co.docy.oldfriends.Messages.JsonSoundMsg;
import co.docy.oldfriends.Messages.JsonUploadImageRet;
import co.docy.oldfriends.Messages.JsonUserGroupsRet;
import co.docy.oldfriends.Messages.Json_DirectRoom_GetTopicImageRet;
import co.docy.oldfriends.Messages.Json_DirectRoom_SendTopicImageRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.emoji.EmojiFragment;
import co.docy.oldfriends.emoji.EmojiUtils;
import de.greenrobot.event.EventBus;
import info.hoang8f.android.segmented.SegmentedGroup;


public class ActivityDirectChatGroup extends ActivityBase implements OnClickListener {

    /**
     * 本activity从ActivityMainGroup拷贝而来
     * <p/>
     * 主要做了简化操作
     * 只允许普通msg、图片、地图
     * 没有topic或subject的概念，所以其中图片、地图均是直接发送，没有title和body
     * <p/>
     * 主要的代码更改在于
     * 获取图片及地图后，直接调用专门的接口发送，无需到ActivityCreateSubject
     */

    LinearLayout subject_button_universal_layout, ll_universal_activity_2;
    TextView subject_button_universal_cancel;
    View subject_button_universal_gray;
    TextView universal_camera, universal_gallery, universal_subject, universal_file, universal_map, universal_vote, universal_activity;

    TextView main_goto_unread, remind_text;
    ImageView ivPlus;
    TextView bSend;
    EditText et;
    PullToRefreshListView lv_msg;
    FloatingActionMenu fam;
    FloatingActionButton fab_plus_subject, fab_plus_camera, fab_plus_gallery, fab_plus_file, fab_plus_map, fab_plus_vote;

    ListAdapterMsg la_msg;
    JsonGetMsgRet.GetMsgRet getMsgRet;
    Json_DirectRoom_GetTopicImageRet.DirectRoom_GetTopicImageRet getImageRet;
    //    JsonGetMsgTopicRet.GetMsgTopicRet getMsgTopicRet;
//    JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet getMsgTopicCommentRet;
    public IMsg imsPrivateRoomRoot;
    int msgPageCount = 0;
    int mUnread = 0;

    int groupId = -1;
    String name;
    String desc;

    File tempAttach;
    String tempAttachMime;

    ActionBar ab;
    ImageView fam_image;

    // sound views
    private JsonSoundMsg jsonSoundMsg;
    private TextView ButtonRecord;
    private Button ButtonSwitchInput;
    private boolean input_mode_record = false, isGiveUp = true;
    private Recorder mRecord;
    private ImageView soundImageView, cancle_view, microphone;
    private RelativeLayout recording_layout;
    private Player player = null;
    private float locationY;
    private SegmentedGroup mainroom_segment;
    private View main_fragment_subject_list;
    private Recorder.IUIApdate mIUiupdate = new Recorder.IUIApdate() {
        @Override
        public void UiApdate(double MaxAmplitude) {
            double amp = MyConfig.Amplitudescale(MaxAmplitude);
            MyLog.d("", "MaxAmplitude====+++=======" + amp);
            if (amp < MyConfig.SOUND_LEVEL_1) {
                soundImageView.setImageResource(R.drawable.sound1);
            } else if (amp < MyConfig.SOUND_LEVEL_2) {
                soundImageView.setImageResource(R.drawable.sound2);
            } else if (amp < MyConfig.SOUND_LEVEL_3) {
                soundImageView.setImageResource(R.drawable.sound3);
            } else if (amp < MyConfig.SOUND_LEVEL_4) {
                soundImageView.setImageResource(R.drawable.sound4);
            } else if (amp < MyConfig.SOUND_LEVEL_5) {
                soundImageView.setImageResource(R.drawable.sound5);
            } else {
                soundImageView.setImageResource(R.drawable.sound6);
            }
        }
    };
    private boolean touched_to_record = false;
    boolean last_touch_state = false;
    private EmojiFragment _fragment;
    private ImageView iv_add_emoji;
    private FrameLayout fl_emoji_fragment;
    private boolean emojiIsVisible = false;
    private GridLayout universal_subject_gridlayout;
    private String chatRoomNickname;
    private int chatRoomOnlinnum;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_room);

        mainroom_segment = (SegmentedGroup) findViewById(R.id.mainroom_segment);
//        main_fragment_subject_list = findViewById(R.id.main_fragment_subject_list);
//
//        main_fragment_subject_list.setVisibility(View.GONE);
        mainroom_segment.setVisibility(View.GONE);

        EventBus.getDefault().post(new EventSetSoundRemind(true));
        MyConfig.activityDirectChatGroup = this;
//        socketTools = new SocketTools();

        ab = getSupportActionBar();

//        slidingMenuSetting(savedInstanceState);
//        slidingMenuAttach();

        initView();


        Intent intent = getIntent();
        if (intent != null) {
            groupId = intent.getIntExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, -1);
            name = intent.getStringExtra(MyConfig.CHAT_ROOM_NAME);
            chatRoomOnlinnum = intent.getIntExtra(MyConfig.CHAT_ROOM_ONLINE_NUM,0);
            chatRoomNickname = intent.getStringExtra(MyConfig.CHAT_ROOM_NICK_NAME);
        } else {
            finish();
        }

        if (ab != null) {
            if ("null".equals(name)) {
                name = "";
            }
            ab.setTitle(name + "·" + chatRoomOnlinnum + "人");
//            ab.setSubtitle(desc);

            MyLog.d("", "actionbar set title");
        }

        /**
         *
         * 在http get msg和socket event可能同时修改list，可能应该防止冲突
         *
         */
        //EventBus.getDefault().register(ActivityDirectChatGroup.this);

//        if(SocketTools.mSocket != null)
//        SocketTools.mSocket.on(MyConfig.RTM_MESSAGE_ADDRESS, onNewMessage);

        initGroup();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (player != null) {
//            player.pause();
            player.release();
            player = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        leaveChatRoom();
        EventBus.getDefault().post(new EventSetSoundRemind(false));
    }


    private void addSoundMsg2Local(JsonSoundMsg jsonSoundMsg) {
        IMsg msg = MyConfig.formIMsgFromSoundMsg(jsonSoundMsg, imsPrivateRoomRoot, MyConfig.MSG_CATEGORY_NORMAL);

        /**
         * 注意，插入到头部是因为后台返回的msg次序
         */
        imsPrivateRoomRoot.ll_sub_msg.addFirst(msg);
    }

    public int count = -1;

    public void onEventMainThread(EventHeart200ms eventHeart200ms) {
        if (count >= 0) {
            count++;
        }
        if (count >= (MyConfig.MAX_RECORD_TIME * 1000 / 200)) {
            int duration = 0;
            //stop record
            if (mRecord != null) {
                count = -1;
                duration = mRecord.stopRecord();
                mRecord = null;
                ButtonRecord.setBackgroundResource(R.drawable.button_record_up);
                ButtonRecord.setText(getResources().getString(R.string.touch_to_speak));
                ButtonRecord.setTextColor(getResources().getColor(R.color.xjt_time_link));
                touched_to_record = false;
                recording_layout.setVisibility(View.GONE);
                MyLog.d("", "count==========" + count);
                getSoundMsg2Send(duration);
            }
        } else if (count >= ((MyConfig.MAX_RECORD_TIME - 10) * 1000 / 200)) {
            if (locationY < -500) {
                cancle_view.setVisibility(View.VISIBLE);
                microphone.setVisibility(View.GONE);
                soundImageView.setVisibility(View.GONE);
                remind_text.setText(getResources().getString(R.string.record_leave_to_giveup_remind));
            } else {
                cancle_view.setVisibility(View.GONE);
                microphone.setVisibility(View.VISIBLE);
                soundImageView.setVisibility(View.VISIBLE);
                String format = getResources().getString(R.string.record_last_time);
                String result = String.format(format, MyConfig.MAX_RECORD_TIME - (count * 200 / 1000));
                remind_text.setText(result);
            }

        } else {
            if (locationY < -500) {
                cancle_view.setVisibility(View.VISIBLE);
                microphone.setVisibility(View.GONE);
                soundImageView.setVisibility(View.GONE);
                remind_text.setText(getResources().getString(R.string.record_leave_to_giveup_remind));
            } else {
                cancle_view.setVisibility(View.GONE);
                microphone.setVisibility(View.VISIBLE);
                soundImageView.setVisibility(View.VISIBLE);
                remind_text.setText(getResources().getString(R.string.record_up_to_giveup_remind));
            }
        }

        if ((last_touch_state) && (touched_to_record)) {
            //start record
            if (mRecord == null) {
                if (player != null) {
                    player.release();
                    player = null;
                }
                mRecord = new Recorder(MyConfig.tempSoundFile);
                mRecord.startRecord();
                count = 0;
            } else {
                mIUiupdate.UiApdate(mRecord.mediaRecorder.getMaxAmplitude());
            }

        } else if ((!last_touch_state) && (!touched_to_record)) {
            int duration = 0;
            //stop record
            if (mRecord != null) {
                count = -1;
                duration = mRecord.stopRecord();
                mRecord = null;
                if (!isGiveUp && duration <= 60) {  //send Message
                    getSoundMsg2Send(duration);
                }
            }
        }

        last_touch_state = touched_to_record;

    }

    ListAdapterMsg.OnSoundClickedListener onSoundClickedListener = new ListAdapterMsg.OnSoundClickedListener() {
        @Override
        public void onSoundClicked(IMsg imsg) {

            IMsg currentPlayedImag = null;

            if (player != null) {

                /**
                 * 如果已经存在一个player,就是当前点击的imsg
                 *  不是当前的imsg,清除player,新建一个当前imsg的player并播放
                 */
                currentPlayedImag = player.getIMsg();

                if (currentPlayedImag == imsg) {

                    switch (currentPlayedImag.audioPlaying) {
                        case MyConfig.MEDIAPLAYER_IS_PLAYING://正在准备或正在播放
                            player.pause();
                            break;
                        case MyConfig.MEDIAPLAYER_IS_READY:// 停止
                            player.replay();
                            break;
                        case MyConfig.MEDIAPLAYER_NOT_INITIAL://已经播放完毕
                            player.prepareAndPlay();
                            break;
                        case MyConfig.MEDIAPLAYER_IS_PREPARING: // 再次播放
                            player.reset();
                            break;
                    }

                } else {
                    player.release();

                    player = playSoundIMsg(imsg);
                }

            } else {

                player = playSoundIMsg(imsg);

            }

        }
    };

    public Player playSoundIMsg(IMsg imsg) {
        Player player = new Player(imsg, new Player.PlayingNotify() {
            @Override
            public void statusNotify() {

                la_msg.notifyDataSetChanged();

            }
        });
        player.prepareAndPlay();

        return player;
    }


    private void initView() {

        main_goto_unread = (TextView) findViewById(R.id.main_goto_unread);

        ButtonRecord = (TextView) findViewById(R.id.mainroom_record_button);
        ButtonSwitchInput = (Button) findViewById(R.id.switch_input);
        soundImageView = (ImageView) findViewById(R.id.sound);
        cancle_view = (ImageView) findViewById(R.id.cancel_view);
        microphone = (ImageView) findViewById(R.id.microphone);
        recording_layout = (RelativeLayout) findViewById(R.id.recording_layout);
        remind_text = (TextView) findViewById(R.id.remind_text);


        ButtonSwitchInput.setOnClickListener(this);

        ButtonRecord.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                MyLog.d("", "recording in mainroom: ButtonRecord any" + event.getAction());
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        locationY = event.getY();
                        MyLog.d("", "recording in mainroom: ButtonRecord ACTION_DOWN");
                        touched_to_record = true;
                        ButtonRecord.setBackgroundResource(R.drawable.button_recorder);
                        ButtonRecord.setText(getResources().getString(R.string.leave_to_send));
                        ButtonRecord.setTextColor(getResources().getColor(R.color.White));
                        recording_layout.setVisibility(View.VISIBLE);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        locationY = event.getY();
                        break;
                    case MotionEvent.ACTION_UP:
//                        MyLog.d("", "recording in mainroom: ButtonRecord ACTION_UP");
                        ButtonRecord.setBackgroundResource(R.drawable.button_record_up);
                        ButtonRecord.setText(getResources().getString(R.string.touch_to_speak));
                        ButtonRecord.setTextColor(getResources().getColor(R.color.xjt_time_link));
                        touched_to_record = false;
                        recording_layout.setVisibility(View.GONE);
                        MyLog.d("", "recording in mainroom: ButtonRecord ACTION_UP === " + event.getRawY() + "  " + event.getY() + "  " + event.getYPrecision());
                        if (event.getY() < -500) {
                            //give up
                            isGiveUp = true;
                        } else {
                            //send anyway
                            isGiveUp = false;
                        }

                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        lv_msg = (PullToRefreshListView) findViewById(R.id.main_lv_msg);
        lv_msg.setPullToRefreshEnabled(false);/**聊天室中不允许刷新，也不显示出“下拉刷新字样”*/

//        lv_msg = (ListView) findViewById(R.id.main_lv_msg);
        lv_msg.getRefreshableView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {


                } else if (MotionEvent.ACTION_UP == event.getAction()) {
                    MyConfig.hide_keyboard_must_call_from_activity(ActivityDirectChatGroup.this);
                    hideEmojiDialog();

                }
                return false;
            }
        });

        lv_msg.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

//                MyLog.d("", "listview scroll: onScrollStateChanged scrollState" + scrollState);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                MyLog.d("", "listview scroll: onScroll" + firstVisibleItem + " " + visibleItemCount + " " + totalItemCount);

                if (firstVisibleItem + visibleItemCount < totalItemCount - MyConfig.room_listview_scroll_threshold) {
//                    MyLog.d("", "listview scroll: onScroll TRANSCRIPT_MODE_DISABLED " + firstVisibleItem + " " + visibleItemCount + " " + totalItemCount);
                    view.setTranscriptMode(ListView.TRANSCRIPT_MODE_DISABLED);
                } else {
                    view.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
//                    MyLog.d("", "listview scroll: onScroll TRANSCRIPT_MODE_ALWAYS_SCROLL " + firstVisibleItem + " " + visibleItemCount + " " + totalItemCount);
                }
            }
        });

        /**聊天室不允许刷新,所以去掉下拉刷新功能*/
        /*lv_msg.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                // ...
//                new AsyncTask<Void, Void, Void>() {
//                    @Override
//                    protected Void doInBackground(Void... params) {
//                        // 处理刷新任务
//                        try {
//                            Thread.sleep(2000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                        return null;
//                    }
//
//                    @Override
//                    protected void onPostExecute(Void reslst) {
//                        // 更行内容，通知 PullToRefresh 刷新结束
//                        lv_msg.onRefreshComplete();
//                    }
//                }.execute();

//                getGroupMsg();
                new AsyncTaskGetMoreMsg(new AfterGetMoreMsg() {
                    @Override
                    public void afterGetMoreMsg(int newItemCount) {

                        lv_msg.onRefreshComplete();

                        if (newItemCount > 0) {
                            //更新msg列表
//                            MyConfig.buildTimeShowString(imsPrivateRoomRoot.ll_sub_msg);

                            la_msg.notifyDataSetChanged();


                            lv_msg.getRefreshableView().setSelectionFromTop(newItemCount + 1, 0);
                        }
                    }
                }).execute();

            }
        });*/


        et = (EditText) findViewById(R.id.mainroom_input_edit);

        /**这个Fragment用来显示表情，并将et和Fragment绑定在一起*/
        _fragment = (EmojiFragment) getSupportFragmentManager().findFragmentById(R.id.emoji_fragment_main);
        _fragment.setEditTextHolder(et);

        iv_add_emoji = (ImageView) findViewById(R.id.iv_add_emoji);
        iv_add_emoji.setOnClickListener(this);
        fl_emoji_fragment = (FrameLayout) findViewById(R.id.fl_emoji_fragment);
        et.setOnClickListener(this);

        et.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN)) {
                    getMsg2Send();
                    return true;
                }
                return false;
            }
        });
        et.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 1) {
                    bSend.setVisibility(View.VISIBLE);
                    ivPlus.setVisibility(View.GONE);
                } else {
                    bSend.setVisibility(View.GONE);
                    ivPlus.setVisibility(View.VISIBLE);
                }
            }
        });

        bSend = (TextView) findViewById(R.id.mainroom_send_msg);
       /* LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        params.bottomMargin = 16;
        params.rightMargin = (int) getResources().getDimension(R.dimen.view_marge);
        bSend.setLayoutParams(params);*/
        bSend.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                getMsg2Send();
            }

        });

        ivPlus = (ImageView) findViewById(R.id.mainroom_plus);
        ivPlus.setOnClickListener(this);
        ivPlus.setVisibility(View.VISIBLE);


        fam = (FloatingActionMenu) findViewById(R.id.fab_menu);
        fam.setVisibility(View.GONE);
        fam_image = fam.getMenuIconView();
        fam_image.setImageResource(R.drawable.fab_plus);
        fam.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean b) {
                if (b) {
                    fam_image.setImageResource(R.drawable.fab_back);
                } else {
                    fam_image.setImageResource(R.drawable.fab_plus);
                }
            }
        });

        fab_plus_subject = (FloatingActionButton) findViewById(R.id.fab_plus_subject);
        fab_plus_camera = (FloatingActionButton) findViewById(R.id.fab_plus_camera);
        fab_plus_gallery = (FloatingActionButton) findViewById(R.id.fab_plus_gallery);
        fab_plus_file = (FloatingActionButton) findViewById(R.id.fab_plus_file);
        fab_plus_map = (FloatingActionButton) findViewById(R.id.fab_plus_map);
        fab_plus_vote = (FloatingActionButton) findViewById(R.id.fab_plus_vote);
        fab_plus_subject.setOnClickListener(fabClickListener);
        fab_plus_camera.setOnClickListener(fabClickListener);
        fab_plus_gallery.setOnClickListener(fabClickListener);
        fab_plus_file.setOnClickListener(fabClickListener);
        fab_plus_map.setOnClickListener(fabClickListener);
        fab_plus_vote.setOnClickListener(fabClickListener);


        subject_button_universal_layout = (LinearLayout) findViewById(R.id.universal_subject_main);
        subject_button_universal_cancel = (TextView) findViewById(R.id.subject_button_universal_cancel);
        subject_button_universal_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                subject_button_universal_layout.setVisibility(View.GONE);
            }
        });
        subject_button_universal_gray = findViewById(R.id.subject_button_universal_gray);
        subject_button_universal_gray.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                subject_button_universal_layout.setVisibility(View.GONE);
            }
        });

        universal_camera = (TextView) findViewById(R.id.universal_camera);
        universal_gallery = (TextView) findViewById(R.id.universal_gallery);
        universal_subject = (TextView) findViewById(R.id.universal_subject);
        universal_subject.setVisibility(View.GONE);
        universal_file = (TextView) findViewById(R.id.universal_file);
        universal_map = (TextView) findViewById(R.id.universal_map);
        universal_vote = (TextView) findViewById(R.id.universal_vote);
        universal_activity = (TextView) findViewById(R.id.universal_activity);
        universal_vote.setVisibility(View.GONE);

        universal_subject.setOnClickListener(universalButtonClickListener);
        universal_camera.setOnClickListener(universalButtonClickListener);
        universal_gallery.setOnClickListener(universalButtonClickListener);
        universal_file.setOnClickListener(universalButtonClickListener);
        universal_map.setOnClickListener(universalButtonClickListener);
        universal_vote.setOnClickListener(universalButtonClickListener);

        universal_subject_gridlayout = (GridLayout) findViewById(R.id.universal_subject_gridlayout);
        universal_subject_gridlayout.setColumnCount(3);//设置每行显示两个，可以使按钮居中，若以后增加按钮的个数参数值啊哟做响应的改变
        removeGridLayoutChild();

    }

    /**
     * 移除plusDialog中的按钮。这个布局复用主聊天室中的布局，这个聊天室中的子话题种类少，把多余的移除。
     */
    private void removeGridLayoutChild() {
        for (int i = universal_subject_gridlayout.getChildCount() - 1; i >= 0; i--) {
            switch (universal_subject_gridlayout.getChildAt(i).getId()) {
                case R.id.universal_subject://主题
                    universal_subject_gridlayout.removeViewAt(i);
                    break;
                case R.id.universal_vote://投票
                    universal_subject_gridlayout.removeViewAt(i);
                    break;
                case R.id.universal_activity://报名
                    universal_subject_gridlayout.removeViewAt(i);
                    break;
                case R.id.universal_file://文件
                    universal_subject_gridlayout.removeViewAt(i);
                    break;
                case R.id.universal_video://视频
                    universal_subject_gridlayout.removeViewAt(i);
                    break;

            }
        }
    }


    private OnClickListener universalButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            subject_button_universal_layout.setVisibility(View.GONE);
            switch (v.getId()) {
                case R.id.universal_subject:
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_subject);
//                    openSubjectInput(MyConfig.REQUEST_SUBJECT_ONLY, null, null);
                    break;
                case R.id.universal_camera:
                    tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_camera);
                    openCamera();
                    break;
                case R.id.universal_gallery:
                    tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_gallery);
                    openFileChooser(MyConfig.MIME_IMAGE_STAR);
                    break;
                case R.id.universal_file:
                    tempAttachMime = MyConfig.MIME_FILE_STAR;
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_file);
                    openFileChooser(MyConfig.MIME_FILE_STAR);
                    break;
                case R.id.universal_map:
                    MyConfig.CREATE_SUBJECT_TITLE = getString(R.string.menu_plus_map);
                    openMap();
                    break;
                case R.id.universal_vote:
                    openSetupVote(MyConfig.REQUEST_CREATE_VOTE);
                    break;
            }

        }
    };

    public void initGroup() {

        /**
         * TODO:
         * 实际上已经不需要root msg了，只需要链表头指针，考虑逐步移除这个root msg
         * 同时，msg里面的parant msg信息也不用了，而且实际上也得不到了
         */

        joinChatRoom();


        imsPrivateRoomRoot = new IMsg(
                MyConfig.USER_INDEX_ADMIN,
                "admin",
                null,

                Calendar.getInstance().getTime(),

                MyConfig.LEVEL_MSG_0,
                null,
                MyConfig.MSG_CATEGORY_SUBJECT,

                groupId,
                -1,
                MyConfig.formClientCheckId(groupId),

                "room id:" + groupId,
                "this is root msg"
        );
        imsPrivateRoomRoot.ll_sub_msg.clear();
        MyConfig.privateRoomRootNode = imsPrivateRoomRoot;

//        //初始化以防null指针
        la_msg = new ListAdapterMsg(ActivityDirectChatGroup.this, MyConfig.LEVEL_ROOM_PRIVATE, imsPrivateRoomRoot.ll_sub_msg, onLinkClickedListener, onImageClickedListener);
        la_msg.soundClickedListener = onSoundClickedListener;

        la_msg.captureLongClickedListener = new MyConfig.ClassCaptureLongClick(
                ActivityDirectChatGroup.this,
                imsPrivateRoomRoot.ll_sub_msg,
                imsPrivateRoomRoot.ll_sub_msg_with_image,
                la_msg,
                null);

        lv_msg.setAdapter(la_msg);
        imsPrivateRoomRoot.la_msg = la_msg;

        setUnRead();

//        getGroupMsg();
        //initRoomMsgList();

    }

    /**
     * 加入聊天室
     */
    private void joinChatRoom() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                final JsonJoinChatRoom jsonJoinChatRoom = new JsonJoinChatRoom();
                jsonJoinChatRoom.accessToken = MyConfig.usr_token;
                jsonJoinChatRoom.nickName = chatRoomNickname;
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonJoinChatRoom, JsonJoinChatRoom.class);
                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_USERS_INVITE + groupId + MyConfig.API2_CHAT_ROOM_JOIN_2, jsonStr);
                final JsonJoinChatRoomRet jsonJoinChatRoomRet = gson.fromJson(s, JsonJoinChatRoomRet.class);
                if (jsonJoinChatRoomRet != null && jsonJoinChatRoomRet.code == MyConfig.retSuccess()) {

                }

            }
        }).start();
    }

    /**
     * 退出聊天室
     */
    private void leaveChatRoom() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                final JsonLeaveChatRoom jsonLeaveChatRoom = new JsonLeaveChatRoom();
                jsonLeaveChatRoom.accessToken = MyConfig.usr_token;
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonLeaveChatRoom, JsonLeaveChatRoom.class);
                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_USERS_INVITE + groupId + MyConfig.API2_CHAT_ROOM_LEAVE_2, jsonStr);
                final JsonLeaveChatRoomRet jsonLeaveChatRoomRet = gson.fromJson(s, JsonLeaveChatRoomRet.class);
                if (jsonLeaveChatRoomRet != null && jsonLeaveChatRoomRet.code == MyConfig.retSuccess()) {
                }
            }
        }).start();
    }

    /**
     * 当加入小组后服务器会下发一个socket，然后发送一个eventBus事件，在这儿接收这个事件。离开小组和被踢出小组都是发送的这个Event事件
     *
     * @param getEventRet
     */
    public void onEventMainThread(JsonGetEventRet.GetEventRet getEventRet) {
        if (groupId != getEventRet.groupId) {
            return;
        }
        if (MyConfig.usr_id == getEventRet.userId) {
            MyConfig.chat_room_nick_name = getEventRet.nickName;
            MyConfig.chat_room_avatar = getEventRet.avatar;
        }
        switch (getEventRet.type) {
            case MyConfig.TYPE_JOIN:
                chatRoomOnlinnum++;
                ab.setTitle(name + "·" + chatRoomOnlinnum + "人");
                getEventRet.info.message = "加入了聊天室";
                break;
            case MyConfig.TYPE_LEAVE:
                chatRoomOnlinnum--;
                ab.setTitle(name + "·" + chatRoomOnlinnum + "人");
                getEventRet.info.message = "离开了聊天室";
                break;
        }
        IMsg iMsgRet = MyConfig.formIMsgFromGetEventRet(getEventRet, imsPrivateRoomRoot, MyConfig.MSG_CATEGORY_EVENT);
        if (getEventRet.type != MyConfig.TYPE_KICK) {
            socketAddMsg2Local(iMsgRet);
        }
    }

    public void initRoomMsgList() {

        msgPageCount = 0;
        imsPrivateRoomRoot.ll_sub_msg.clear();
        imsPrivateRoomRoot.ll_sub_msg_with_image.clear();

        new AsyncTaskGetMoreMsg(new AfterGetMoreMsg() {
            @Override
            public void afterGetMoreMsg(int newItemCount) {
//                lv_msg.onRefreshComplete();

                if (newItemCount > 0) {

                    //更新msg列表
//                    MyConfig.buildTimeShowString(imsPrivateRoomRoot.ll_sub_msg);

                    la_msg.notifyDataSetChanged();

//                    lv_msg.getRefreshableView().setSelectionFromTop(newItemCount + 1, 0);
                }
            }
        }).execute();
    }

    public void onEventMainThread(EventScreenSwitch eventScreenSwitch) {

        if (eventScreenSwitch.isScreenOn) {
            // initRoomMsgList();
        }
    }

    private void setUnRead() {
        if (MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {
            if (MyConfig.jsonUserGroupsRet != null) {
                for (JsonUserGroupsRet.UserGroupsRet urr : MyConfig.jsonUserGroupsRet.data) {
                    if (urr.id == groupId) {
                        if (urr.unreadCount > 0) {
                            mUnread = urr.unreadCount;
                            main_goto_unread.setText("" + urr.unreadCount);
                            main_goto_unread.setVisibility(View.VISIBLE);
                            main_goto_unread.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    main_goto_unread.setVisibility(View.INVISIBLE);

                                    EventBus.getDefault().post(new EventMainroomGotoUnread());
                                }
                            });
                        }
                    }
                }
            }
        } else {

            main_goto_unread.setVisibility(View.GONE);
        }
    }


    public View getViewByPosition(int pos, ListView listView) {
        /**
         * 来自http://stackoverflow.com/questions/24811536/android-listview-get-item-view-by-position
         * 未仔细考察
         */
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            MyLog.d("", "getViewByPosition 1: " + pos + " " + firstListItemPosition + " " + lastListItemPosition);
            /**
             * 说明此position不在listview的当前view里面
             * 此刻返回的view不管用
             */
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            MyLog.d("", "getViewByPosition 2: " + pos + " " + firstListItemPosition + " " + lastListItemPosition + " " + childIndex);
            /**
             * 此position在listview的当前view里面
             * 此刻返回的view可以被操作
             */
            return listView.getChildAt(childIndex);
        }
    }

    public void onEventMainThread(EventMainroomGotoUnread eventMainroomGotoUnread) {

        if (imsPrivateRoomRoot.ll_sub_msg.size() > mUnread) {

            lv_msg.getRefreshableView().setSelectionFromTop(imsPrivateRoomRoot.ll_sub_msg.size() - mUnread + 1, 0);

            new Thread(new Runnable() {

                /**
                 * todo
                 *  前面对listview做了setSelectionFromTop操作
                 *  后续获取某个子view并企图对其进行闪动
                 *  怀疑这个setSelectionFromTop操作是异步的，且不知如何追踪操作的结束
                 *  所以后续获取子view时该子view可能还没来得及换入进listview，也即该子view获取不到
                 *  这导致闪动操作的对象错误
                 *
                 *  采取笨办法，延迟半秒钟，等待前面的setSelectionFromTop操作完毕
                 *  由于人眼有一个反应时间，所以有这个延迟还正好
                 */

                @Override
                public void run() {
                    try {
                        /**
                         * todo
                         *  Thread或其runnable怎么延迟执行？
                         *  待查
                         */
                        Thread.sleep(500);
                    } catch (Exception e) {

                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            /**
                             * todo
                             *  用闪动来标示第一条未读消息
                             */
                            ObjectAnimator objectAnimator =
                                    ObjectAnimator.ofFloat(getViewByPosition(imsPrivateRoomRoot.ll_sub_msg.size() - mUnread + 1, lv_msg.getRefreshableView()),
                                            "alpha", 1, 0, 1).setDuration(1000);
                            objectAnimator.setInterpolator(new LinearInterpolator());
                            objectAnimator.setRepeatCount(2);
                            objectAnimator.start();
                        }
                    });

                }
            }).start();


        } else {
            new AsyncTaskGetMoreMsg(new AfterGetMoreMsg() {
                @Override
                public void afterGetMoreMsg(int newItemCount) {
                    EventBus.getDefault().post(new EventMainroomGotoUnread());
                }
            }).execute();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.switch_input:
                // 虚拟键盘隐藏 判断view是否为空
                View view = getWindow().peekDecorView();
                if (view != null) {
                    //隐藏虚拟键盘
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(ActivityDirectChatGroup.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                MyLog.d("", "recording in mainroom: ButtonSwitchInput onClick");
                input_mode_record = !input_mode_record;
                if (input_mode_record) {
                    /**当切换成录音模式时隐藏表情*/
                    hideEmojiDialog();
                    iv_add_emoji.setVisibility(View.GONE);

                    ButtonRecord.setVisibility(View.VISIBLE);
                    ButtonSwitchInput.setBackgroundResource(R.drawable.button_keyboard);
                    et.setVisibility(View.GONE);
                } else {
                    iv_add_emoji.setVisibility(View.VISIBLE);
                    ButtonRecord.setVisibility(View.GONE);
                    et.setVisibility(View.VISIBLE);
                    ButtonSwitchInput.setBackgroundResource(R.drawable.button_record);
                }
                break;
            case R.id.mainroom_input_edit:
                if (emojiIsVisible) {
                    hideEmojiDialog();
                }
                break;
            case R.id.iv_add_emoji:
                if (emojiIsVisible) {
                    hideEmojiDialog();
                    EmojiUtils.show_keyboard_must_call_from_activity(this);
                } else {
                    showEmojiDialog();
                }
                break;
            case R.id.mainroom_plus:
                showPlusDialog();
                break;
        }

    }


    /**
     * 隐藏表情选择框
     */
    private void hideEmojiDialog() {
        fl_emoji_fragment.setVisibility(View.GONE);
        iv_add_emoji.setImageResource(R.drawable.emoji_button_3x);
        emojiIsVisible = false;
    }

    /**
     * 显示表情选择框
     */
    private void showEmojiDialog() {
        if (!et.hasFocus()) {
            et.setFocusable(true);
            et.requestFocus();
        }
        EmojiUtils.hide_keyboard_must_call_from_activity(this);
        SystemClock.sleep(300);
        fl_emoji_fragment.setVisibility(View.VISIBLE);
        iv_add_emoji.setImageResource(R.drawable.button_keyboard);
        emojiIsVisible = true;
    }

    public interface AfterGetMoreMsg {
        public void afterGetMoreMsg(int newItemCount);
    }

    private class AsyncTaskGetMoreMsg extends AsyncTask<Void, Void, String> {
        AfterGetMoreMsg afterGetMoreMsg;
        int newItemsCount;

        public AsyncTaskGetMoreMsg(AfterGetMoreMsg afterGetMoreMsg) {
            this.afterGetMoreMsg = afterGetMoreMsg;
        }

        protected String doInBackground(Void... v) {

            msgPageCount++;

            String param;
            if (msgPageCount > 1 && imsPrivateRoomRoot.ll_sub_msg.size() > 0) {
                param = "/?cursor=" + imsPrivateRoomRoot.ll_sub_msg.getFirst().server_createAt + "&page=1";
            } else {
                param = "/?page=" + msgPageCount;
            }
            MyLog.d("", "httptools: url " + MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GET + groupId + MyConfig.API2_GET_2 + param);
            String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GET + groupId + MyConfig.API2_GET_2 + param, MyConfig.usr_token);
            MyLog.d("", "http directroom 1: " + groupId + " msg: " + s);

            return s;

        }

        protected void onPostExecute(String s) {

            Gson gson = new Gson();
            try {
                if (s == null) {
//                    newItemsCount = -1;
                    return;
                }
                JSONObject jsonRoot = new JSONObject(s);
                if (jsonRoot == null) {
//                    newItemsCount = -1;
                    return;
                }

                int code = jsonRoot.getInt("code");
                if (code != MyConfig.retSuccess()) {
//                    newItemsCount = -1;
                    return;
                }

                JSONArray jsonData = jsonRoot.getJSONArray("data");
                newItemsCount = jsonData.length();
                int firstFlag = 0;
                for (int i = 0; i < jsonData.length(); i++) {
                    JSONObject jsonObject = jsonData.getJSONObject(i);

                    /**设置从通知过了的私人聊天室的Title*/
                    if ("".equals(name) && !jsonObject.getString("nickName").equals(MyConfig.usr_nickname) && firstFlag == 0) {
                        firstFlag++;
                        if (ab != null) {
                            ab.setTitle(jsonObject.getString("nickName"));
                        }
                    }

                    int type = jsonObject.getInt("type");
                    if (type == MyConfig.TYPE_SOUND) {
                        MyLog.d("", "sound ");
                        jsonSoundMsg = gson.fromJson(jsonObject.toString(), JsonSoundMsg.class);
                        addSoundMsg2Local(jsonSoundMsg);
                    } else if (type == MyConfig.TYPE_MSG) {
                        // message
                        getMsgRet = gson.fromJson(jsonObject.toString(), JsonGetMsgRet.GetMsgRet.class);
                        addGetMsgRet2Local(getMsgRet);
//                    } else if (type == MyConfig.TYPE_TOPIC) {
//                        // subject (topic)
//                        getMsgTopicRet = gson.fromJson(jsonObject.toString(), JsonGetMsgTopicRet.GetMsgTopicRet.class);
//                        addGetMsgTopicRet2Local(getMsgTopicRet);
//                    } else if (type == MyConfig.TYPE_TOPIC_COMMANT) {
//                        // subject (topic) comment
//                        getMsgTopicCommentRet = gson.fromJson(jsonObject.toString(), JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet.class);
//                        addGetMsgTopicCommentRet2Local(getMsgTopicCommentRet);
                    } else if (type == MyConfig.TYPE_CREATE) {

                        // event，格式与msg相近，用msg格式来获取
                        getMsgRet = gson.fromJson(jsonObject.toString(), JsonGetMsgRet.GetMsgRet.class);
                        getMsgRet.info.message = getString(R.string.group_event_create);
                        addGetMsgRet2Local(getMsgRet);
                    } else if (type == MyConfig.TYPE_JOIN) {

                        // event，格式与msg相近，用msg格式来获取
                        getMsgRet = gson.fromJson(jsonObject.toString(), JsonGetMsgRet.GetMsgRet.class);
                        getMsgRet.info.message = getString(R.string.group_event_join);
                        addGetMsgRet2Local(getMsgRet);
                    } else if (type == MyConfig.TYPE_LEAVE) {

                        // event，格式与msg相近，用msg格式来获取
                        getMsgRet = gson.fromJson(jsonObject.toString(), JsonGetMsgRet.GetMsgRet.class);
                        getMsgRet.info.message = getString(R.string.group_event_leave);
                        addGetMsgRet2Local(getMsgRet);
                    } else if (type == MyConfig.TYPE_TOPIC_IMAGE_DIRECTROOM || type == MyConfig.TYPE_TOPIC_MAP_DIRECTROOM) {

                        getImageRet = gson.fromJson(jsonObject.toString(), Json_DirectRoom_GetTopicImageRet.DirectRoom_GetTopicImageRet.class);
                        addGetImageRet2Local(getImageRet);
                    } else {
                        MyLog.d("", "MainGroup get unkown type msg: " + type);
                    }
                }
            } catch (JSONException e) {
                MyLog.d("", "getGroupMsg JSONException: " + e.toString());
                newItemsCount = -1;
            } catch (JsonSyntaxException e) {
                MyLog.d("", "getGroupMsg JsonSyntaxException: " + e.toString());
                newItemsCount = -1;
            } catch (Exception e) {
                MyLog.d("", "AsyncTaskGetMoreMsg: e:" + e.toString() + " s:" + s);
            }


            MyConfig.buildTimeShowString(imsPrivateRoomRoot.ll_sub_msg);
            afterGetMoreMsg.afterGetMoreMsg(newItemsCount);
        }
    }


    private void addGetImageRet2Local(Json_DirectRoom_GetTopicImageRet.DirectRoom_GetTopicImageRet getImageRet) {
        IMsg msg = MyConfig.formIMsgFromGetImageRet_DirectRoom(getImageRet, imsPrivateRoomRoot, MyConfig.MSG_CATEGORY_PRIVATE);

        /**
         * 注意，插入到头部是因为后台返回的msg次序
         */
        imsPrivateRoomRoot.ll_sub_msg.addFirst(msg);
        MyConfig.addImageIMsg2ImageList(imsPrivateRoomRoot, msg, true);
    }

    private void addGetMsgRet2Local(JsonGetMsgRet.GetMsgRet getMsgRet) {
        IMsg msg = MyConfig.formIMsgFromGetMsgRet(getMsgRet, imsPrivateRoomRoot, MyConfig.MSG_CATEGORY_PRIVATE);

        /**
         * 注意，插入到头部是因为后台返回的msg次序
         */
        imsPrivateRoomRoot.ll_sub_msg.addFirst(msg);
    }


    private void addGetMsgTopicRet2Local(JsonGetMsgTopicRet.GetMsgTopicRet getMsgTopicRet) {

        IMsg msg = MyConfig.formIMsgFromGetMsgTopicRet(getMsgTopicRet, imsPrivateRoomRoot, MyConfig.MSG_CATEGORY_PRIVATE);

        imsPrivateRoomRoot.ll_sub_msg.addFirst(msg);
        MyConfig.addImageIMsg2ImageList(imsPrivateRoomRoot, msg, true);
    }

    private void addGetMsgTopicCommentRet2Local(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet getMsgTopicCommentRet) {
        IMsg msg = MyConfig.formIMsgFromGetMsgTopicCommentRet(getMsgTopicCommentRet, imsPrivateRoomRoot, MyConfig.MSG_CATEGORY_PRIVATE);

//        imsPrivateRoomRoot.ll_sub_msg.add(msg);


        imsPrivateRoomRoot.ll_sub_msg.addFirst(msg);
    }


    ListAdapterMsg.OnLinkClickedListener onLinkClickedListener = new ListAdapterMsg.OnLinkClickedListener() {

        @Override
        public void onLinkClicked(IMsg imsSub) {
//            initGroup(imsNew);
            MyConfig.subRoomRootNode = imsSub;
            Intent intent = new Intent(ActivityDirectChatGroup.this, ActivitySubGroup.class);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_TOPICID, imsSub.topicId);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_TITLE, imsSub.title);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, imsSub.body);
            startActivity(intent);

        }
    };

    ListAdapterMsg.OnImageClickedListener onImageClickedListener = new ListAdapterMsg.OnImageClickedListener() {

        @Override
        public void onImageClicked(IMsg ims) {
//            MyConfig.iMsgClicked = ims.imsParent;
//            MyConfig.iMsgClicked = ims;
//            MyConfig.iMsgGalleryRoot = MyConfig.privateRoomRootNode;
            MyConfig.genGalleryFullSizeUrl(MyConfig.privateRoomRootNode, ims);

            Intent intent = new Intent(ActivityDirectChatGroup.this, ActivityGallery.class);
            intent.putExtra("mode", MyConfig.GALLERY_MODE_PHOTOS_DOWNLOADED);
            intent.putExtra("from", ActivityDirectChatGroup.class.getName());

            startActivity(intent);

        }
    };

    private IMsg formSoundMsg() {

        File f = new File(MyConfig.appCacheDirPath, MyConfig.tempSoundFile);
        if (f.exists() && f.length() > 0) {

            IMsg msg = new IMsg(
                    MyConfig.usr_id,
                    MyConfig.chat_room_nick_name,
                    MyConfig.chat_room_avatar,

                    Calendar.getInstance().getTime(),

                    MyConfig.LEVEL_MSG_1,
                    imsPrivateRoomRoot,
                    MyConfig.MSG_CATEGORY_NORMAL,

                    groupId,
                    -1,
                    MyConfig.formClientCheckId(groupId),

                    null,
                    null
            );
            msg.attached = f;

            return msg;
        } else {
            return null;
        }

    }

    private IMsg formMsg(String s) {

        IMsg msg = new IMsg(
                MyConfig.usr_id,
                MyConfig.chat_room_nick_name,//发送到本地要使用昵称
                MyConfig.chat_room_avatar,//发送到本地要使用昵称头像

                Calendar.getInstance().getTime(),

                MyConfig.LEVEL_MSG_1,
                imsPrivateRoomRoot,
                MyConfig.MSG_CATEGORY_PRIVATE,

                groupId,
                -1,
                MyConfig.formClientCheckId(groupId),

                null,
                s
        );

        return msg;
    }

    private void getSoundMsg2Send(int duration) {
        IMsg iMsg = formSoundMsg();
        iMsg.duration = duration;
        sendSoundMsg(iMsg);
    }


    private void sendSoundMsg(IMsg iMsg) {

        if (!MyConfig.debug_msg_no_local_first) {
            sendSoundMsg2Local(iMsg);
        }
        SendSoundMsg2Server(iMsg);

    }


    private void sendSoundMsg2Local(IMsg iMsg) {
        iMsg.setLocalSendTag();
        socketAddMsg2Local(iMsg);
    }

    private void SendSoundMsg2Server(final IMsg imsg) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                final JsonSendSoundMsgRet jsonSendSoundMsgRet = HttpTools.okhttpUploadSoundMsg(imsg, null);

                if (jsonSendSoundMsgRet != null && jsonSendSoundMsgRet.code == MyConfig.retSuccess()) {
                    MyLog.d("", getResources().getString(R.string.sound_success));
                }

            }
        }).start();
    }

    private void getMsg2Send() {
        String str = MyConfig.trimAndTakeAwayStringFromEditText(et);
        if (str != null) {
            /**把表情的名字和iOS协商好的字段*/
            str = EmojiUtils.replaceNameByField(this, str);
            IMsg iMsg = formMsg(str);
            sendMsg(iMsg);
        }

    }

    private void sendMsg(IMsg iMsg) {

        if (!MyConfig.debug_msg_no_local_first) {
            iMsg.setLocalSendTag();
            socketAddMsg2Local(iMsg);
        }
        SendMsg2Server(iMsg);

    }

    private void SendMsg2Server(final IMsg imsg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSendMsg jsonSendMsg = new JsonSendMsg();
                jsonSendMsg.accessToken = MyConfig.usr_token;
                jsonSendMsg.message = imsg.body;
                jsonSendMsg.groupId = groupId;
                jsonSendMsg.clientId = imsg.clientId;

                String jsonStr = gson.toJson(jsonSendMsg, JsonSendMsg.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SEND, jsonStr);

                final JsonSendMsgRet jsonSendMsgRet = gson.fromJson(s, JsonSendMsgRet.class);


                MyLog.d("", "HttpTools: " + s);
                if (s != null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            MyLog.d("", "Snackbar is called");
//                            Snackbar.make(lv_msg, "You have sent a msg~", Snackbar.LENGTH_LONG).show();

                        }
                    });
                }
                MyLog.d("", "apitest: send msg ret " + s);

            }
        }).start();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        // ========= 第一部分是各种主题创建的返回 ================

        /**
         * 第四类：创建定位地图，再创建话题
         */
        if (requestCode == MyConfig.REQUEST_MAP && resultCode == RESULT_OK) {

            /**
             * 地图发送走image接口
             */
            IMsg ims = formSubject();
            ims.type = MyConfig.TYPE_TOPIC_MAP_DIRECTROOM;
            ims.mapLocation = MyConfig.tempMapLocation;

            sendSubjectImage(ims);

//            openSubjectInput(MyConfig.REQUEST_SUBJECT_WITH_MAP);

        }
        /**
         * 带定位地图的话题创建
         */
        if (requestCode == MyConfig.REQUEST_SUBJECT_WITH_MAP && resultCode == RESULT_OK) {

            /**
             * 私信没有subject，所以不会到这里了
             */

            IMsg ims = formSubjectFromIntent(data);
            ims.type = MyConfig.TYPE_TOPIC;
            ims.subType = MyConfig.SUBTYPE_MAP;
            ims.mapLocation = MyConfig.tempMapLocation;

            sendSubjectMap(ims);

            //debug
//            socketAddSubject2Local(ims);
        }

        /**
         * 第三类：创建投票，再创建话题
         */
        if (requestCode == MyConfig.REQUEST_CREATE_VOTE && resultCode == RESULT_OK) {

//            openSubjectInput(MyConfig.REQUEST_SUBJECT_WITH_VOTE);

            IMsg ims = formSubjectFromIntent(data);
            ims.title = MyConfig.tempVote.title;
            ims.type = MyConfig.TYPE_TOPIC;
            ims.subType = MyConfig.SUBTYPE_VOTE;
            ims.vote = MyConfig.tempVote;

            //投票时可能附带了图片，也可能没有
            String attachedPhoto = data.getStringExtra(MyConfig.INTENT_KEY_CREATE_VOTE_ATTACHED);
            if (attachedPhoto != null) {
                ims.attached = new File(attachedPhoto);
            }

            sendSubjectVote(ims);
        }
//        /**
//         * 带投票的话题创建
//         */
//        if (requestCode == MyConfig.REQUEST_SUBJECT_WITH_VOTE && resultCode == RESULT_OK) {
//
//            IMsg ims = formSubjectFromIntent(data);
//            ims.type = MyConfig.TYPE_TOPIC;
//            ims.subType = MyConfig.SUBTYPE_VOTE;
//            ims.vote = MyConfig.tempVote;
//
//            sendSubjectVote(ims);
//
//        }

        /**
         * 第二类：先获取附件，包括拍摄／图片／文件，再创建话题
         */
        if (requestCode == MyConfig.REQUEST_FILE && resultCode == RESULT_OK) {
            Uri fileUri = data.getData();
            String realPath = MyConfig.getUriPath(this, fileUri);
//            MyConfig.sendSysMsg(imsPrivateRoomRoot, MyConfig.getUriPath(this, fileUri));
            tempAttach = new File(realPath);

            openSubjectInput(MyConfig.REQUEST_SUBJECT_WITH_IMAGE);
        }
        if (requestCode == MyConfig.REQUEST_GALLERY && resultCode == RESULT_OK) {

            Uri fileUri = data.getData();
            String realPath = MyConfig.getUriPath(this, fileUri);
            tempAttach = new File(realPath);

            formImsgAndSend2Server();


//            openSubjectInputWithAttachImage(MyConfig.REQUEST_SUBJECT_WITH_IMAGE, tempAttach);..

            if (MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {
                /**
                 * 去掉，聊天室上传图片时不需要裁剪
                 */
                Intent innerIntent = new Intent("com.android.camera.action.CROP");
                innerIntent.setDataAndType(data.getData(), MyConfig.MIME_IMAGE_STAR);
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true");// 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
                startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);
            }
        }

        if (requestCode == MyConfig.REQUEST_CAMERA && resultCode == RESULT_OK) {

            formImsgAndSend2Server();
        }

        if (MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {
            /**
             * 以下代码接受裁剪后的图片
             * 不过在主聊天室里面不需要裁剪图片
             */
            if (requestCode == MyConfig.REQUEST_IMAGE_CROP && resultCode == RESULT_OK) {

                Bitmap bitmap = data.getParcelableExtra("data");

                tempAttach = new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg");

                // 图像保存到文件中
                FileOutputStream foutput = null;
                try {
                    foutput = new FileOutputStream(tempAttach);
                    if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, foutput)) {
//                        Toast.makeText(this,
//                                "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath(),
//                                Toast.LENGTH_LONG).show();
                        MyConfig.MyToast(-1, this,
                                getResources().getString(R.string.feil_exsit) + tempAttach.getAbsolutePath());
                    }
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                openSubjectInput(MyConfig.REQUEST_SUBJECT_WITH_IMAGE);
            }
        }


        /**
         * 带附件的话题创建，包括拍摄／图片／文件
         */
        if (requestCode == MyConfig.REQUEST_SUBJECT_WITH_IMAGE && resultCode == RESULT_OK) {


        }

        /**
         * 第一类：单纯的话题创建
         */
        if (requestCode == MyConfig.REQUEST_SUBJECT_ONLY && resultCode == RESULT_OK) {

            IMsg ims = formSubjectFromIntent(data);
            sendSubject(ims);
//            la_msg.notifyDataSetChanged();

        }

    }

    private void formImsgAndSend2Server() {
        IMsg ims = formSubject();
        ims.type = MyConfig.TYPE_TOPIC_IMAGE_DIRECTROOM;
        ims.attachFileBeforeSend2Server(tempAttach, tempAttachMime);
        sendSubjectImage(ims);
    }


    private void sendSubject(final IMsg ims) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSendTopic jsonSendTopic = new JsonSendTopic();
                jsonSendTopic.accessToken = MyConfig.usr_token;
                jsonSendTopic.title = ims.title;
                jsonSendTopic.desc = ims.body;
                jsonSendTopic.groupId = groupId;
                jsonSendTopic.clientId = ims.clientId;

                String jsonStr = gson.toJson(jsonSendTopic, JsonSendTopic.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SEND_TOPIC, jsonStr);
                MyLog.d("", "HttpTools send topic ret: " + s);

                final JsonSendTopicRet jsonSendTopicRet = gson.fromJson(s, JsonSendTopicRet.class);


//                MyLog.d("", "HttpTools: " + s);
//                if (s != null) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            MyConfig.sendSysMsg(imsPrivateRoomRoot, s);
//
//                        }
//                    });
//                }
                MyLog.d("", "apitest: send msg ret " + s);

            }
        }).start();
    }

    private void sendSubjectVote(final IMsg ims) {

        if (ims.attached == null) {
            sendSubjectVoteBody(ims);
        } else {
            sendSubjectVoteWithImage(ims);
        }
    }


    public void sendSubjectVoteWithImage(final IMsg iMsg) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonUploadImageRet jsonUploadImageRet = HttpTools.okhttpUploadImage(iMsg.attached, "image/jpeg");

                if (jsonUploadImageRet != null && jsonUploadImageRet.code == MyConfig.retSuccess()) {
                    iMsg.vote.imageId = jsonUploadImageRet.data.id;

                    sendSubjectVoteBody(iMsg);

                } else {

                }

            }
        }).start();
    }


    private void sendSubjectVoteBody(final IMsg ims) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSendTopicVoteCreate jsonSendTopicVoteCreate = new JsonSendTopicVoteCreate();
                jsonSendTopicVoteCreate.accessToken = MyConfig.usr_token;
                jsonSendTopicVoteCreate.groupId = groupId;
                jsonSendTopicVoteCreate.title = ims.title;
                jsonSendTopicVoteCreate.single = ims.vote.single;
                jsonSendTopicVoteCreate.anonymous = ims.vote.anonymous;
                jsonSendTopicVoteCreate.options = ims.vote.options;
                jsonSendTopicVoteCreate.imageId = ims.vote.imageId;
                jsonSendTopicVoteCreate.clientId = ims.clientId;

                String jsonStr = gson.toJson(jsonSendTopicVoteCreate, JsonSendTopicVoteCreate.class);
                MyLog.d("", "http mainroom 2: " + jsonStr);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_VOTE, jsonStr);
                MyLog.d("", "http mainroom 3: " + s);

                final JsonSendTopicVoteCreateRet jsonSendTopicVoteCreateRet = gson.fromJson(s, JsonSendTopicVoteCreateRet.class);

                if (jsonSendTopicVoteCreateRet != null) {
                    if (jsonSendTopicVoteCreateRet.code == MyConfig.retSuccess()) {

                    } else {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MyConfig.MyToast(0, MyConfig.activityDirectChatGroup, jsonSendTopicVoteCreateRet.message);
                            }
                        });

                    }

                }

                MyLog.d("", "apitest: send msg ret " + s);

            }
        }).start();
    }


    public void sendSubjectMap(final IMsg iMsg) {
        if (!MyConfig.debug_msg_no_local_first) {
            sendSubjectMap2Local(iMsg);
        }
        sendSubjectMap2Server(iMsg);
    }

    private void sendSubjectMap2Local(IMsg ims) {
        ims.setLocalSendTag();
        socketAddSubject2Local(ims);
    }

    public void sendSubjectMap2Server(final IMsg iMsg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final JsonSendMapRet jsonSendMapRet = HttpTools.okhttpUploadSubjectMap(iMsg);

                if (jsonSendMapRet != null) {
                    if (jsonSendMapRet.code == MyConfig.retSuccess()) {

                    } else {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MyConfig.MyToast(0, MyConfig.activityDirectChatGroup, jsonSendMapRet.message);
                            }
                        });

                    }

                }
            }
        }).start();
    }


    public void sendSubjectImage(final IMsg iMsg) {
        if (!MyConfig.debug_msg_no_local_first) {
            sendSubjectImage2Local(iMsg);
        }
        sendSubjectImage2Server(iMsg);
    }


    private void sendSubjectImage2Local(IMsg iMsg) {

        iMsg.setLocalSendTag();
        socketAddSubject2Local(iMsg);

    }

    public void sendSubjectImage2Server(final IMsg iMsg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Json_DirectRoom_SendTopicImageRet json_directRoom_sendTopicImageRet = HttpTools.okhttpUploadSubjectImage_DirectRoom(iMsg, null);

                if (json_directRoom_sendTopicImageRet != null) {
                    if (json_directRoom_sendTopicImageRet.code == MyConfig.retSuccess()) {

                    } else {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MyConfig.MyToast(0, MyConfig.activityDirectChatGroup, json_directRoom_sendTopicImageRet.message);
                            }
                        });

                    }

                }
            }
        }).start();
    }


    public void onEventMainThread(JsonSoundMsg jsonSoundMsg) {

        MyLog.d("", "socketio event GetMsgRet: notifyDataSetChanged" + jsonSoundMsg.groupId);
        if (groupId != jsonSoundMsg.groupId) {
            MyLog.d("", "socketio event : onEventMainThread(JsonGetMsgRet.GetMsgRet jsonMsgSocket) ret for wrong groupId" + jsonSoundMsg.groupId);
            return;
        }

        IMsg iMsgRet = MyConfig.formIMsgFromSoundMsg(jsonSoundMsg, imsPrivateRoomRoot, MyConfig.MSG_CATEGORY_PRIVATE);

        socketAddMsg2Local(iMsgRet);


    }

    public void onEventMainThread(JsonGetMsgRet.GetMsgRet json_socket_msg) {

        MyLog.d("", "socketio event : onEventMainThread(JsonGetMsgRet.GetMsgRet jsonMsgSocket)" + json_socket_msg.groupId);
        if (groupId != json_socket_msg.groupId) {
            MyLog.d("", "socketio event : onEventMainThread(JsonGetMsgRet.GetMsgRet jsonMsgSocket) ret for wrong groupId" + json_socket_msg.groupId);
            return;
        }

        IMsg iMsgRet = MyConfig.formIMsgFromGetMsgRet(json_socket_msg, imsPrivateRoomRoot, MyConfig.MSG_CATEGORY_PRIVATE);

//        IMsg iMsgRet = new IMsg(
//                json_socket_msg.userId,
//                json_socket_msg.sender,
//                json_socket_msg.avatar,
//
//                MyConfig.UTCString2Date(json_socket_msg.updatedAt),
//
//                MyConfig.LEVEL_MSG_1,
//                imsPrivateRoomRoot,
//                MyConfig.MSG_CATEGORY_NORMAL,
//
//                json_socket_msg.groupId,
//                -1,
//                json_socket_msg.info.clientId,
//
//                null,
//                json_socket_msg.info.message
//        );
//        iMsgRet.originDataFromServer.getMsgRet = json_socket_msg;
////        iMsgRet.originDataFromServer.json_socket_msg = json_socket_msg;
//        iMsgRet.type = json_socket_msg.type;
//        iMsgRet.subType = json_socket_msg.subType;


        socketAddMsg2Local(iMsgRet);
    }

    public void onEventMainThread(Json_DirectRoom_GetTopicImageRet.DirectRoom_GetTopicImageRet json_socket_directroom_topic) {

        /**
         * todo
         *  先合并http和socket的结构
         *  再合并这个到MyConfig里面对应的函数中
         */

        MyLog.d("", "socketio event : onEventMainThread(Json_Socket_Topic json_socket_topic)" + json_socket_directroom_topic.groupId);
        if (groupId != json_socket_directroom_topic.groupId) {
            MyLog.d("", "socketio event : onEventMainThread(Json_Socket_Topic json_socket_topic) ret for wrong groupId" + json_socket_directroom_topic.groupId);
            return;
        }

        IMsg iMsgSubjectRet = MyConfig.formIMsgFromGetImageRet_DirectRoom(json_socket_directroom_topic, imsPrivateRoomRoot, MyConfig.MSG_CATEGORY_PRIVATE);


        socketAddSubject2Local(iMsgSubjectRet);
    }

    public void onEventMainThread(JsonGetMsgTopicRet.GetMsgTopicRet json_socket_topic) {

        /**
         * todo
         *  先合并http和socket的结构
         *  再合并这个到MyConfig里面对应的函数中
         */

        MyLog.d("", "socketio event : onEventMainThread(Json_Socket_Topic json_socket_topic)" + json_socket_topic.groupId);
        if (groupId != json_socket_topic.groupId) {
            MyLog.d("", "socketio event : onEventMainThread(Json_Socket_Topic json_socket_topic) ret for wrong groupId" + json_socket_topic.groupId);
            return;
        }

        IMsg iMsgSubjectRet = MyConfig.formIMsgFromGetMsgTopicRet(json_socket_topic, imsPrivateRoomRoot, MyConfig.MSG_CATEGORY_PRIVATE);


        socketAddSubject2Local(iMsgSubjectRet);
    }

    public void onEventMainThread(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet json_socket_topicComment) {
        MyLog.d("", "socketio event : onEventMainThread(Json_Socket_TopicComment json_socket_topicComment) mainroom " + json_socket_topicComment.groupId);
        if (groupId != json_socket_topicComment.groupId) {
            MyLog.d("", "socketio event : onEventMainThread(Json_Socket_TopicComment json_socket_topicComment) ret for wrong groupId" + json_socket_topicComment.groupId);
            return;
        }


        IMsg iMsgRet = MyConfig.formIMsgFromGetMsgTopicCommentRet(json_socket_topicComment, imsPrivateRoomRoot, MyConfig.MSG_CATEGORY_PRIVATE);


        socketAddComment2Local(iMsgRet);
    }


    private void socketAddMsg2Local(final IMsg msg) {
        appendIMsg2List(msg);
    }

    private void socketAddSubject2Local(final IMsg imsgSubject) {
        appendIMsg2List(imsgSubject);
//        addImageIMsg2ImageList(imsPrivateRoomRoot, imsgSubject);
    }

    private void socketAddComment2Local(final IMsg imsgComment) {
        appendIMsg2List(imsgComment);
//        addImageIMsg2ImageList(imsPrivateRoomRoot, imsgComment);
    }


    private void appendIMsg2List(final IMsg msg) {

        int index = MyConfig.searchRepeatMsgInList(imsPrivateRoomRoot.ll_sub_msg, msg, MyConfig.REPEAT_CHECK_COUNT_MAX);

        if (-1 == index) {
            imsPrivateRoomRoot.ll_sub_msg.addLast(msg);
            MyLog.d("", "notifyDataSetChanged: addLast " + msg.clientId);
        } else {
            imsPrivateRoomRoot.ll_sub_msg.set(index, msg);
            MyLog.d("", "notifyDataSetChanged: set " + msg.clientId);
        }

        /**
         * 这里可能隐藏一个很深的小问题，及图片list的次序可能和消息list的次序不同，这样点进去到相册时会发现次序不同
         * 但是点进去还是正确的图片，因为寻找图片是根据clientId来比对的
         */
        int index_image = MyConfig.searchRepeatMsgInList(imsPrivateRoomRoot.ll_sub_msg_with_image, msg, MyConfig.REPEAT_CHECK_COUNT_MAX);
        if (-1 == index_image) {
            MyConfig.addImageIMsg2ImageList(imsPrivateRoomRoot, msg, false);
        } else {
            MyConfig.setImageIMsg2ImageList(imsPrivateRoomRoot, msg, index_image);
        }


        if (la_msg != null) {
            la_msg.notifyDataSetChanged();
            MyLog.d("", "notifyDataSetChanged: la_msg != null " + msg.clientId);
        }

    }


    private IMsg formSubject() {

        IMsg imsCreateSubject = new IMsg(
                MyConfig.usr_id,
                MyConfig.chat_room_nick_name,
                MyConfig.chat_room_avatar,

                Calendar.getInstance().getTime(),

                MyConfig.LEVEL_MSG_1,
                imsPrivateRoomRoot,
                MyConfig.MSG_CATEGORY_SUBJECT,

                groupId,
                -1,
                MyConfig.formClientCheckId(groupId),

                null,
                null
        );
//        imsPrivateRoomRoot.ll_sub_msg.addLast(imsCreateSubject);
        return imsCreateSubject;
    }

    private IMsg formSubjectFromIntent(Intent data) {
        String title = data.getStringExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_TITLE);
        String body = data.getStringExtra(MyConfig.INTENT_KEY_CREATE_SUBJECT_BODY);

        IMsg imsCreateSubject = new IMsg(
                MyConfig.usr_id,
                MyConfig.chat_room_nick_name,
                MyConfig.chat_room_avatar,

                Calendar.getInstance().getTime(),

                MyConfig.LEVEL_MSG_1,
                imsPrivateRoomRoot,
                MyConfig.MSG_CATEGORY_SUBJECT,

                groupId,
                -1,
                MyConfig.formClientCheckId(groupId),

                title,
                body
        );
//        imsPrivateRoomRoot.ll_sub_msg.addLast(imsCreateSubject);
        return imsCreateSubject;
    }

    private String getUserNameById(int user_id) {
        return null;
    }

    private String getUserAvatarById(int user_id) {
        return null;
    }

    private void openSubjectInput(int requestType) {
        Intent intent = new Intent(this, ActivityCreateSubject.class);
        startActivityForResult(intent, requestType);
    }

    private void openSubjectInputWithAttachImage(int requestType, File f) {
        Intent intent = new Intent(this, ActivityCreateSubject.class);
        intent.putExtra(MyConfig.CONST_STRING_PARAM_ATTACHED, f.getAbsolutePath());
        startActivityForResult(intent, requestType);
    }

    public void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        tempAttach = getCameraFileBigPicture();
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(tempAttach));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, MyConfig.REQUEST_CAMERA);
        }
    }

    public void openSetupVote(int requestType) {
        Intent intent = new Intent(this, ActivityCreateVote.class);
        startActivityForResult(intent, requestType);
    }

    private void openFileChooser(String mime) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setType(mime);
        if (mime.equals(MyConfig.MIME_IMAGE_STAR)) {
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, MyConfig.REQUEST_GALLERY);
            }
        } else if (mime.equals(MyConfig.MIME_FILE_STAR)) {
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, MyConfig.REQUEST_FILE);
            }
        }
    }

    private void openMap() {
        Intent intent = new Intent(this, ActivityMyLocation.class);
        startActivityForResult(intent, MyConfig.REQUEST_MAP);
    }


    public File getCameraFileBigPicture() {
        return new File(MyConfig.appCacheDirPath, MyConfig.getCurrentTimeStr() + "_" + MyConfig.usr_id + MyConfig.EXT_JPG);
    }

    public File getCameraFileThumbnail() {
        return new File(MyConfig.appCacheDirPath, MyConfig.getCurrentTimeStr() + "_" + MyConfig.usr_id + "_thumb" + MyConfig.EXT_JPG);
    }


    private OnClickListener fabClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.fab_plus_subject:
                    fam.close(true);
                    openSubjectInput(MyConfig.REQUEST_SUBJECT_ONLY);
                    break;
                case R.id.fab_plus_camera:
                    fam.close(true);
                    tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                    openCamera();
                    break;
                case R.id.fab_plus_gallery:
                    fam.close(true);
                    tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                    openFileChooser(MyConfig.MIME_IMAGE_STAR);
                    break;
                case R.id.fab_plus_file:
                    fam.close(true);
                    tempAttachMime = MyConfig.MIME_FILE_STAR;
                    openFileChooser(MyConfig.MIME_FILE_STAR);
                    break;
                case R.id.fab_plus_map:
                    fam.close(true);
                    openMap();
                    break;
                case R.id.fab_plus_vote:
                    fam.close(true);
                    openSetupVote(MyConfig.REQUEST_CREATE_VOTE);
                    break;
            }

        }
    };

    public void showPlusDialog() {
        MyConfig.hide_keyboard_must_call_from_activity(this);
        hideEmojiDialog();
        if (MyConfig.plusInvokeMode == MyConfig.PLUS_POPUP_MENU) {
            PopupMenu popup = new PopupMenu(this, ivPlus);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.menu_plus_directroom, popup.getMenu());
            popup.show();

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_plus_camera:
                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                            openCamera();
                            return true;
                        case R.id.menu_plus_gallery:
                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                            openFileChooser(MyConfig.MIME_IMAGE_STAR);
                            return true;
                        case R.id.menu_plus_map:
                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                            openMap();
                            return true;
                        default:
                            return false;
                    }
                }
            });

           /* popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                @Override
                public void onDismiss(PopupMenu menu) {
                    ((ToggleButton) view).setChecked(false);
                }
            });*/
        } else if (MyConfig.plusInvokeMode == MyConfig.PLUS_DIALOG) {
            new DialogFragment() {
                @Override
                public Dialog onCreateDialog(Bundle savedInstanceState) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            getActivity());
                    builder.setTitle("Please choose:")
                            .setItems(R.array.subjects, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    // of the selected item
                                    switch (which) {
                                        case 0:
                                            MyLog.d("", "directroom: item 0");
                                            openSubjectInput(MyConfig.REQUEST_SUBJECT_ONLY);
                                            return;
                                        case 1:
                                            MyLog.d("", "directroom: item 1");
                                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                                            openCamera();
                                            return;
                                        case 2:
                                            MyLog.d("", "directroom: item 2");
                                            tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                                            openFileChooser(MyConfig.MIME_IMAGE_STAR);
                                            return;
                                        case 3:
                                            MyLog.d("", "directroom: item 3");
                                            openMap();
                                            return;
                                        case 4:
                                            MyLog.d("", "directroom: item 4");
                                            tempAttachMime = MyConfig.MIME_FILE_STAR;
                                            openFileChooser(MyConfig.MIME_FILE_STAR);
                                            return;
                                        case 5:
                                            MyLog.d("", "directroom: item 5");
                                            openSetupVote(MyConfig.REQUEST_CREATE_VOTE);
                                            return;
                                        default:
                                            return;
                                    }
                                }
                            })
                            .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    return builder.create();
                }
            }.show(getFragmentManager(), "");
        } else if (MyConfig.plusInvokeMode == MyConfig.PLUS_MYDIALOG) {
            createDialog(ActivityDirectChatGroup.this, R.style.custom_dialog2);
        } else if (MyConfig.plusInvokeMode == MyConfig.PLUS_UNIVERSAL) {

            subject_button_universal_layout.setVisibility(View.VISIBLE);

        }


    }

    public Dialog createDialog(Context context, int style) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout dialogView = (LinearLayout) inflater.inflate(R.layout.dialog_popmenu_layout, null);
        final Dialog customDialog = new Dialog(context, style);
        TextView cameraText, pictureText, mapText, cancleText;
        cameraText = (TextView) dialogView.findViewById(R.id.camera);
        pictureText = (TextView) dialogView.findViewById(R.id.picture);
        mapText = (TextView) dialogView.findViewById(R.id.map);
        cancleText = (TextView) dialogView.findViewById(R.id.cancle);
        WindowManager.LayoutParams localLayoutParams = customDialog.getWindow().getAttributes();
        localLayoutParams.gravity = Gravity.BOTTOM | Gravity.LEFT;
        localLayoutParams.x = 0;
        localLayoutParams.y = 0;
        dialogView.setMinimumWidth(MyConfig.getWidthByScreenPercent(100));
        customDialog.onWindowAttributesChanged(localLayoutParams);
        customDialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                        | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        customDialog.setCanceledOnTouchOutside(false);
        customDialog.setCancelable(true);
        customDialog.setCanceledOnTouchOutside(true);
        customDialog.setContentView(dialogView);
        cameraText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                openCamera();
                customDialog.cancel();
            }
        });
        mapText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                openMap();
                customDialog.cancel();
            }
        });

        pictureText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tempAttachMime = MyConfig.MIME_IMAGE_STAR;
                openFileChooser(MyConfig.MIME_IMAGE_STAR);
                customDialog.cancel();
            }
        });

        cancleText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.cancel();
            }
        });
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (!activity.isFinishing()) {
                customDialog.show();
            }
        }
        return customDialog;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_direct_room, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finishWithNotify();
                return true;
//            case R.id.direct_room_info:
//                Intent room_info = new Intent(ActivityDirectChatGroup.this, ActivityGroupInfo.class);
//                room_info.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
//                room_info.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, name);
//                room_info.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, desc);
//                startActivityForResult(room_info, MyConfig.REQUEST_GROUP_INFO);
//                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            finishWithNotify();
            return true; // 让系统不再处理这次回退键，因为系统收到回退键后会立即把app关了
        }
        return super.onKeyDown(keyCode, event);
    }


    private void finishWithNotify() {
        /**
         * todo
         *  从聊天室退出时需要通知服务器
         *  grouplist刷新一次
         */

        final JsonGroupResetUnread jsonGroupResetUnread = new JsonGroupResetUnread();
        jsonGroupResetUnread.accessToken = MyConfig.usr_token;
        jsonGroupResetUnread.groupId = groupId;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonGroupResetUnread, JsonGroupResetUnread.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_RESET_UNREAD + jsonGroupResetUnread.groupId + MyConfig.API2_GROUP_RESET_UNREAD_2, jsonStr);
                MyLog.d("", "HttpTools: finishWithNotify " + s);

            }
        }).start();

        /**
         * 从服务器更新group列表
         */
        EventBus.getDefault().post(new EventGroupListUpdateFromServer());

        finish();

    }


}
