package co.docy.oldfriends.Activitis;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import co.docy.oldfriends.EventBus.EventHaveSetCurrentCompany;
import co.docy.oldfriends.Fragments._FragmentCanJoinCompanyList;
import co.docy.oldfriends.R;


public class _ActivityCanJoinCompanyList extends ActivityBase {

    int groupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canjoincompany_list);

        init();

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        _FragmentCanJoinCompanyList fragmentCanJoinCompanyList = _FragmentCanJoinCompanyList.newInstance(this);

        fragmentTransaction.add(R.id.activity_canjoincompanyt_list_linearLayout, fragmentCanJoinCompanyList);
        fragmentTransaction.commit();

    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onPause() {
        super.onPause();

    }

    private void init() {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onEventMainThread(EventHaveSetCurrentCompany eventHaveSetCurrentCompany) {
        if (eventHaveSetCurrentCompany.success) {
            finish();
        }
    }
}
