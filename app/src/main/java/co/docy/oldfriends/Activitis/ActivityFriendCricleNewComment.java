package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import co.docy.oldfriends.Adapters.ListAdapterFriendCricleNewComent;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonFriendCricleNewCommentToMeRet;
import co.docy.oldfriends.Messages.JsonMoveToNewComment;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/8/1.
 */
public class ActivityFriendCricleNewComment extends ActivityBase {
    ListView listview;
    ListAdapterFriendCricleNewComent listAdapterFriendCricleNewComent;
    List<JsonFriendCricleNewCommentToMeRet.FriendCricleNewCommentToMeRet> list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_cricle_new_comment);
        listview =(ListView) findViewById(R.id.activity_friend_cricle_new_comment_listview);
        listAdapterFriendCricleNewComent = new ListAdapterFriendCricleNewComent(this,list,clickitem);
        listview.setAdapter(listAdapterFriendCricleNewComent);
        getData();
        MoveData();
    }
    ListAdapterFriendCricleNewComent.OnclickItem clickitem = new ListAdapterFriendCricleNewComent.OnclickItem() {
        @Override
        public void onclickitem(int postion) {
            Intent intent = new Intent(ActivityFriendCricleNewComment.this,ActivityFriendCricleDetail.class);
            intent.putExtra("id",list.get(postion).momentId);
            startActivity(intent);
        }
    };
    public void getData(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2()+MyConfig.API_NEW_COMMNET_LIST , MyConfig.usr_token);
                MyLog.d("kanghongpu","查看自己最新评论"+s);
                final JsonFriendCricleNewCommentToMeRet newCommentToMeRet = gson.fromJson(s,JsonFriendCricleNewCommentToMeRet.class);
                if(newCommentToMeRet!=null&&newCommentToMeRet.code==MyConfig.retSuccess()&&newCommentToMeRet.data!=null){
                 runOnUiThread(new Runnable() {
                     @Override
                     public void run() {
                         list.addAll(list.size(),newCommentToMeRet.data);
                         listAdapterFriendCricleNewComent.notifyDataSetChanged();
                     }
                 });

                }
            }
        }).start();
    }

    public void MoveData(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                JsonMoveToNewComment move = new JsonMoveToNewComment();
                move.accessToken = MyConfig.usr_token;
                String strMove = gson.toJson(move,JsonMoveToNewComment.class);
                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2()+MyConfig.API_MOVE_NEW_COMMENT ,strMove);
                MyLog.d("kanghongpu","清除评论"+s);
            }
        }).start();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
