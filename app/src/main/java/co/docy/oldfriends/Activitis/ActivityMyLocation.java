package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ListView;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import co.docy.oldfriends.Adapters.ListAdapterGetLocationNear;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.DataClass.MapLocation;
import co.docy.oldfriends.R;


public class ActivityMyLocation extends ActivityBase implements LocationSource,
        AMapLocationListener, AMap.OnMapScreenShotListener,PoiSearch.OnPoiSearchListener,AMap.OnCameraChangeListener,
        GeocodeSearch.OnGeocodeSearchListener{
    private AMap aMap;
    private MapView mapView;

    private LocationSource.OnLocationChangedListener mListener;
    private AMapLocationClient mlocationClient;
    private AMapLocationClientOption mLocationOption;

    double longitude;//经度
    double latitude;//纬度
    String address;//地址
    String city;
    String screenShotPath;//地图截图
    boolean tag= false;
    boolean isFrist=false;

    private PoiSearch.Query query;
    private ListView listview;
    private GeocodeSearch geocoderSearch;
    private LatLonPoint latLng_getName;


    private void drawMyLocation(final String title, final double latitude,
                                final double longitude) {

            aMap.clear();
            LatLng latLng = new LatLng(latitude, longitude);
            MarkerOptions markerOption = new MarkerOptions();
            markerOption.title(title);
//            markerOption.snippet(getResources().getString(R.string.latitude) + latitude + getResources().getString(R.string.longitude)+ longitude);
            markerOption.position(latLng);
            markerOption.perspective(true);
            markerOption.draggable(true);
            markerOption.anchor(0.5f, 0.5f);
            Marker marker = aMap.addMarker(markerOption);
            marker.showInfoWindow();
            marker.setVisible(true);
    }


    @Override
    public void onLocationChanged(AMapLocation amapLocation) {
        if (mListener != null && amapLocation != null) {
            if (amapLocation != null
                    && amapLocation.getErrorCode() == 0) {

                mListener.onLocationChanged(amapLocation);// 显示系统小蓝点

                latitude = amapLocation.getLatitude();
                longitude = amapLocation.getLongitude();
                address = amapLocation.getAddress();

                tag=true;
//                drawMyLocation(address,latitude,longitude);
//                aMap.moveCamera(CameraUpdateFactory.zoomTo(15));
                LatLng latLng=new LatLng(latitude,longitude);
                aMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(latLng,15,0,0)));
                isFrist = true;
                search(latitude,longitude);

            } else {
                String errText = "定位失败," + amapLocation.getErrorCode()+ ": " + amapLocation.getErrorInfo();
                Log.e("AmapErr",errText);

            }
        }
    }

    @Override
    public void activate(LocationSource.OnLocationChangedListener listener) {
        mListener = listener;
        if (mlocationClient == null) {
            mlocationClient = new AMapLocationClient(this);
            mLocationOption = new AMapLocationClientOption();
            //设置定位监听
            mlocationClient.setLocationListener(this);
            //设置为高精度定位模式
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
            mLocationOption.setInterval(120 * 1000);
            //设置定位参数
            mlocationClient.setLocationOption(mLocationOption);
            // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
            // 注意设置合适的定位时间的间隔（最小间隔支持为2000ms），并且在合适时间调用stopLocation()方法来取消定位请求
            // 在定位结束后，在合适的生命周期调用onDestroy()方法
            // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
            mlocationClient.startLocation();
        }
    }

    @Override
    public void deactivate () {
        mListener = null;
        if (mlocationClient != null) {
            mlocationClient.stopLocation();
            mlocationClient.onDestroy();
        }
        mlocationClient = null;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);// 不显示程序的标题栏
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_location);

        MyConfig.activityMyLocation = this;

        mapView = (MapView) findViewById(R.id.my_location);
        mapView.onCreate(savedInstanceState);// 此方法必须重写
        listview =(ListView) findViewById(R.id.activity_my_location_listview);
        init();

    }

    /**
     * 初始化
     */
    private void init() {
        if (aMap == null) {
            aMap = mapView.getMap();
            setUpMap();
        }

    }

    /**
     * 设置一些amap的属性
     */
    private void setUpMap() {
        aMap.setLocationSource(this);// 设置定位监听
        aMap.setOnCameraChangeListener(this);
        geocoderSearch = new GeocodeSearch(this);
        geocoderSearch.setOnGeocodeSearchListener(this);
        aMap.getUiSettings().setMyLocationButtonEnabled(true);// 设置默认定位按钮是否显示
        aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
        // 设置定位的类型为定位模式 ，可以由定位、跟随或地图根据面向方向旋转几种
        aMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);
    }


    /**
     * 方法必须重写
     */
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
        deactivate();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    public void backWithoutLocation() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
//        Toast.makeText(this, "back key is pressed, map location is discarded", Toast.LENGTH_LONG).show();
        backWithoutLocation();
    }

    private void backWithLocation() {

//        MyConfig.sendSysMsg(MyConfig.activityMain.imsMainRoomRoot, "backWithLocation()");

        /**
         * 在这里截屏
         */
//        aMap.getMapScreenShot(onMapScreenShotListener);
        aMap.getMapScreenShot(this);


        MyConfig.MyToast(-1, this, getString(R.string.screen_shot_wait));

    }

    private void resultOkIntent() {
        MyConfig.tempMapLocation = new MapLocation();
        MyConfig.tempMapLocation.address = address;
        MyConfig.tempMapLocation.longitude = longitude;
        MyConfig.tempMapLocation.latitude = latitude;
        MyConfig.tempMapLocation.screenShotPath = screenShotPath;

        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map_location_sreach, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
//                Toast.makeText(this, "home key is pressed, input is discarded", Toast.LENGTH_LONG).show();

                backWithoutLocation();
                return true;

            case R.id.map_location_sreach:
                Intent intent = new Intent(ActivityMyLocation.this,ActivitySearchMapPlaceName.class);
                intent.putExtra("city",city);
                startActivityForResult(intent,1);
                return true;

            case R.id.map_location_sreach_submit:
                backWithLocation();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       tag = true;
        if(resultCode == RESULT_OK){
            String name = data.getExtras().getString("name");
            double latitudeFromActivty = data.getExtras().getDouble("Latitude");
            double longitudeFromActivty = data.getExtras().getDouble("Longitude");
            city =name;
            address=name;
            latitude =latitudeFromActivty;
            longitude =longitudeFromActivty;
            LatLng latLng=new LatLng(latitudeFromActivty,longitudeFromActivty);
            aMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(latLng,15,0,0)));
//            drawMyLocation(name,latitudeFromActivty,longitudeFromActivty);

        }
    }

    /**
     * 截屏调用的方法
     * @param bitmap
     */
    @Override
    public void onMapScreenShot(Bitmap bitmap) {

        try {
            // 保存在SD卡根目录下，图片为png格式。
            File f = MyConfig.getCameraFileBigPicture();
            screenShotPath = f.getPath();
            FileOutputStream fos = new FileOutputStream(f);
            boolean b = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            try {
                fos.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (b) {
                MyConfig.MyToast(-1, MyConfig.activityMyLocation, getString(R.string.screen_shot_success));
            } else {
                MyConfig.MyToast(-1, MyConfig.activityMyLocation, getString(R.string.screen_shot_fail));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        resultOkIntent();
    }

    @Override
    public void onMapScreenShot(Bitmap bitmap, int i) {

    }

    /**
     * 搜索周边返回时调用
     * @param poiResult
     * @param i
     */
    @Override
    public void onPoiSearched(final PoiResult poiResult, int i) {
        Log.d("查看搜索出来的位置1111",i+"");
        Log.d("查看搜索出来的位置",poiResult.getPois().toString());
        if(poiResult.getPois()!=null) {
            ListAdapterGetLocationNear mAdapter = new ListAdapterGetLocationNear(ActivityMyLocation.this,0, poiResult.getPois(), new ListAdapterGetLocationNear.layoutClick() {
                @Override
                public void layoutClickLister(int i) {
                    address = String.valueOf(poiResult.getPois().get(i));
                    latitude = poiResult.getPois().get(i).getLatLonPoint().getLatitude();
                    longitude = poiResult.getPois().get(i).getLatLonPoint().getLongitude();
                    drawMyLocation(String.valueOf(poiResult.getPois().get(i)), poiResult.getPois().get(i).getLatLonPoint().getLatitude(),
                            poiResult.getPois().get(i).getLatLonPoint().getLongitude());
                }
            });
            listview.setAdapter(mAdapter);
        }else{
            Toast.makeText(ActivityMyLocation.this, "周边无建筑", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPoiItemSearched(PoiItem poiItem, int i) {

    }

    private void search(double longitude,
                        double latitude) {
    //搜索周边的方法
        query = new PoiSearch.Query("", "", "");
        // keyWord表示搜索字符串，第二个参数表示POI搜索类型，默认为：生活服务、餐饮服务、商务住宅
        // 共分为以下20种：汽车服务|汽车销售|
        // 汽车维修|摩托车服务|餐饮服务|购物服务|生活服务|体育休闲服务|医疗保健服务|
        // 住宿服务|风景名胜|商务住宅|政府机构及社会团体|科教文化服务|交通设施服务|
        // 金融保险服务|公司企业|道路附属设施|地名地址信息|公共设施
        // cityCode表示POI搜索区域，（这里可以传空字符串，空字符串代表全国在全国范围内进行搜索）
//            query.setPageSize(15);// 设置每页最多返回多少条poiitem
//            query.setPageNum(1);// 设置查第一页
        PoiSearch poiSearch = new PoiSearch(this, query);
        if(latitude!=0.0&&longitude!=0.0){
            poiSearch.setBound(new PoiSearch.SearchBound(new LatLonPoint(latitude,
                    longitude), 500));// 设置周边搜索的中心点以及区域
            poiSearch.setOnPoiSearchListener(this);// 设置数据返回的监听器
            poiSearch.searchPOIAsyn();// 开始搜索
        }else{
            Toast.makeText(ActivityMyLocation.this, "定位失败", Toast.LENGTH_SHORT).show();
        }


    }

    /**
     * 滑动地图时调用
     * @param cameraPosition
     */
    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

    }

    @Override
    public void onCameraChangeFinish(CameraPosition cameraPosition) {

        latLng_getName = new LatLonPoint(cameraPosition.target.latitude, cameraPosition.target.longitude);
        if(isFrist) {
            getAddress(latLng_getName);
        }

    }
    public void getAddress(final LatLonPoint latLonPoint) {
        //根据经纬度获取地名
                RegeocodeQuery query = new RegeocodeQuery(latLonPoint, 200,
                        GeocodeSearch.AMAP);// 第一个参数表示一个Latlng，第二参数表示范围多少米，第三个参数表示是火系坐标系还是GPS原生坐标系
                geocoderSearch.getFromLocationAsyn(query);// 设置同步逆地理编码请求
    }
    @Override
    public void onRegeocodeSearched(RegeocodeResult result, int rCode) {
        //反编译

        if (rCode == 1000) {
            if (result != null && result.getRegeocodeAddress() != null
                    && result.getRegeocodeAddress().getFormatAddress() != null) {
                String addressName = result.getRegeocodeAddress().getFormatAddress();

                    if (tag) {
                        drawMyLocation(address, latitude, longitude);
                        tag = false;
                    } else {
                        address = addressName;
                        latitude = latLng_getName.getLatitude();
                        longitude = latLng_getName.getLongitude();
                        drawMyLocation(addressName, latLng_getName.getLatitude(), latLng_getName.getLongitude());
                    }

                    search(latLng_getName.getLatitude(), latLng_getName.getLongitude());

            } else {

            }
        } else {

        }
    }

    @Override
    public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {

    }
}
