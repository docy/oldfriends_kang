package co.docy.oldfriends.Activitis;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import co.docy.oldfriends.Fragments._FragmentSchooolContactEveryClass;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/5/4.
 */
public class _ActivitySchoolContactEveryClass extends ActivityBase implements _FragmentSchooolContactEveryClass.getClassPersonNum {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        String title = getIntent().getStringExtra("title");
//        setTitle(title);

        setContentView(R.layout.activity_school_contact_every_class);

        int num = getIntent().getIntExtra("press_id",0);
        Bundle bundle = new Bundle();
        bundle.putInt("num_id",num);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        _FragmentSchooolContactEveryClass fragmentSchooolContactEveryClass = _FragmentSchooolContactEveryClass.newInstance();
        fragmentSchooolContactEveryClass.setArguments(bundle);
        fragmentTransaction.add(R.id.activity_school_contact_every_class_linear, fragmentSchooolContactEveryClass);
        fragmentTransaction.commit();
        String title = getIntent().getStringExtra("title");
        int peoplenum = getIntent().getIntExtra("peopleNum",0);
        setTitle(title+"("+peoplenum+"人)");
    }
    @Override
    public void getPersonNum(int Num) {
        /*String title = getIntent().getStringExtra("title");
        setTitle(title+"("+Num+"人)");*/
    }



    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
