package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners;
import com.umeng.socialize.exception.SocializeException;
import com.umeng.socialize.weixin.controller.UMWXHandler;

import java.util.Map;
import java.util.Set;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventAppQuit;
import co.docy.oldfriends.EventBus.EventLogin;
import co.docy.oldfriends.Messages.JsonLogin;
import co.docy.oldfriends.Messages.JsonWeixinLoginRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;


public class _ActivityLogin extends ActivityBase {

    FrameLayout login_framelayout;
    RelativeLayout relativeLayout;
    ImageView iv_logo;
    Button bSend;
    EditText et_name;
    EditText et_pwd;
    Button login_weixin;
    Button logout_weixin;
    Button login_goto_regist;
    Button login_reset_password;

    /**
     * if 0, no auto login
     * if 1, auto login
     * if -1, according to usr_status
     */
    int login_cmd;
    String auto_login_name;
    String auto_login_pwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.child_activity_name = this.getClass().getName();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        MyLog.d("", "start ActivityLogin onCreate");

        MyConfig.activityLogin = this;

        login_cmd = getIntent().getIntExtra(MyConfig.TAG_LOGIN_CMD, -1);
        MyLog.d("", "login_cmd " + login_cmd);

        login_framelayout = (FrameLayout)findViewById(R.id.login_framelayout);
        login_framelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConfig.hide_keyboard_must_call_from_activity(_ActivityLogin.this);
            }
        });
        login_framelayout.setSoundEffectsEnabled(false);

        relativeLayout = (RelativeLayout) findViewById(R.id.login_layout);
//        relativeLayout.getLayoutParams().width = MyConfig.screenWidth * 3 / 4;
//        relativeLayout.getLayoutParams().height = MyConfig.screenHeight * 2 / 3;
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConfig.hide_keyboard_must_call_from_activity(_ActivityLogin.this);
            }
        });
        relativeLayout.setSoundEffectsEnabled(false);

        iv_logo = (ImageView) findViewById(R.id.login_logo);
        iv_logo.getLayoutParams().width = MyConfig.screenWidth / 4;
        iv_logo.getLayoutParams().height = MyConfig.screenWidth / 4;
        iv_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyConfig.hide_keyboard_must_call_from_activity(_ActivityLogin.this);
                return;
            }
        });
        iv_logo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                MyLog.d("", "long click and single click: long");

                if (et_name.getText().toString().endsWith("set")) {
                    Intent intent_advance_setting = new Intent(_ActivityLogin.this, ActivityAdvanceSetting.class);
                    startActivity(intent_advance_setting);
                }
                return true;
            }
        });
        iv_logo.setSoundEffectsEnabled(false);


        et_name = (EditText) findViewById(R.id.login_name);
        et_pwd = (EditText) findViewById(R.id.login_pwd);

        bSend = (Button) findViewById(R.id.login_send);
        bSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginRequest();

            }
        });


        login_goto_regist = (Button) findViewById(R.id.login_goto_regist);
        login_goto_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_regist = new Intent(_ActivityLogin.this, _ActivityRegist.class);
                startActivity(intent_regist);

                finish();
            }
        });

        login_reset_password = (Button) findViewById(R.id.login_reset_password);
        login_reset_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_regist = new Intent(_ActivityLogin.this, _ActivityResetPassword.class);
                startActivity(intent_regist);

                finish();
            }
        });




        logout_weixin = (Button) findViewById(R.id.logout_weixin);
//        logout_weixin.setVisibility(View.VISIBLE);
        logout_weixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 添加微信平台
                UMWXHandler wxHandler = new UMWXHandler(_ActivityLogin.this, MyConfig.third_weixin_share_appID, MyConfig.third_weixin_share_appSecret);
                wxHandler.addToSocialSDK();

                // 首先在您的Activity中添加如下成员变量
                final UMSocialService mController = UMServiceFactory.getUMSocialService("com.umeng.share");

                mController.deleteOauth(_ActivityLogin.this, SHARE_MEDIA.WEIXIN,
                        new SocializeListeners.SocializeClientListener() {
                            @Override
                            public void onStart() {
                            }
                            @Override
                            public void onComplete(int status, SocializeEntity entity) {
                                if (status == 200) {
                                    Toast.makeText(_ActivityLogin.this, "删除成功.",
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(_ActivityLogin.this, "删除失败",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
        login_weixin = (Button) findViewById(R.id.login_weixin);
//        login_weixin.setVisibility(View.VISIBLE);
        login_weixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 添加微信平台
                UMWXHandler wxHandler = new UMWXHandler(_ActivityLogin.this, MyConfig.third_weixin_share_appID, MyConfig.third_weixin_share_appSecret);
                wxHandler.addToSocialSDK();

                // 首先在您的Activity中添加如下成员变量
                final UMSocialService mController = UMServiceFactory.getUMSocialService("com.umeng.share");

                mController.doOauthVerify(_ActivityLogin.this, SHARE_MEDIA.WEIXIN, new SocializeListeners.UMAuthListener() {
                    @Override
                    public void onStart(SHARE_MEDIA platform) {
                        Toast.makeText(_ActivityLogin.this, getResources().getString(R.string.license_to_start), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(SocializeException e, SHARE_MEDIA platform) {
                        Toast.makeText(_ActivityLogin.this, getResources().getString(R.string.authorization_error), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete(Bundle value, SHARE_MEDIA platform) {
                        Toast.makeText(_ActivityLogin.this, getResources().getString(R.string.authorization_to_complete), Toast.LENGTH_SHORT).show();
                        //获取相关授权信息
                        mController.getPlatformInfo(_ActivityLogin.this, SHARE_MEDIA.WEIXIN, new SocializeListeners.UMDataListener() {
                            @Override
                            public void onStart() {
                                Toast.makeText(_ActivityLogin.this, getResources().getString(R.string.get_platform_data_start), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onComplete(int status, Map<String, Object> info) {
                                if (status == 200 && info != null) {
                                    StringBuilder sb = new StringBuilder();
                                    Set<String> keys = info.keySet();
                                    for (String key : keys) {
                                        sb.append(key + "=" + info.get(key).toString() + "\r\n");
                                    }
                                    Log.d("TestData", sb.toString());
                                } else {
                                    Log.d("TestData", getResources().getString(R.string.error_occurred) + status);
                                }
                            }
                        });
                    }

                    @Override
                    public void onCancel(SHARE_MEDIA platform) {
                        Toast.makeText(_ActivityLogin.this, getResources().getString(R.string.License_cancellation), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        MyLog.d("", "ActivityLogin onResume");

        /**
         * load user data
         */
        if(MyConfig.prefShared != null) {
            auto_login_name = MyConfig.prefShared.getString(MyConfig.PREF_USER_NAME, "");
            et_name.setText(auto_login_name);
            et_name.setSelection(auto_login_name.length());
            auto_login_pwd = MyConfig.prefShared.getString(MyConfig.PREF_USER_PWD, "");
            et_pwd.setText(auto_login_pwd);
            et_pwd.setSelection(auto_login_pwd.length());
        }

        switch (login_cmd) {
            case -1:
                autoLoginAccordingToUsrStatus();
                break;
            case 0:
                break;
            case 1:
                loginIfHavePwd();
                break;
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        MyConfig.activityLogin = null;

    }

    private void autoLoginAccordingToUsrStatus() {
        /**
         * app可能意外重启等等，MyConfig.usr_status是不可靠的，只有preference里面保存的才是可靠的
         */
//        MyConfig.usr_status = MyConfig.prefShared.getInt(MyConfig.PREF_USR_STATUS, -1);
        MyConfig.loadUserDataFromPreference(this);

        MyLog.d("", "autologin in resume " + MyConfig.usr_status);
        if (MyConfig.usr_status > MyConfig.USR_STATUS_LOGOUT) {

            MyLog.d("", "autologin: login()");

            loginIfHavePwd();
        }
    }

    private void loginIfHavePwd() {
        if (auto_login_name != null && auto_login_pwd != null) {
            loginRequest();
        }
    }

    public void loginRequest() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                String login_name_or_phone = et_name.getText().toString();
                String pwd = et_pwd.getText().toString();

                JsonLogin jsonLogin = new JsonLogin();
                jsonLogin.account = login_name_or_phone;
                jsonLogin.password = pwd;
                jsonLogin.os = MyConfig.os_string;
                jsonLogin.uuid = MyConfig.uuid;

                Gson gson = new Gson();
                String j = gson.toJson(jsonLogin);

                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LOGIN, j);

                final JsonWeixinLoginRet jsonLoginRet = gson.fromJson(s, JsonWeixinLoginRet.class);

                if (jsonLoginRet != null) {

                    if (jsonLoginRet.code!=MyConfig.retSuccess()) {

                        /**
                         * 密码错了就清除保存的密码，并保持在这个位置
                         */
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                              //  et_pwd.setText("");
//                                MyConfig.MyToast(0,ActivityLogin.this,getString(R.string.error_login));
                                MyConfig.MyToast(0,_ActivityLogin.this,MyConfig.getErrorMsg(""+jsonLoginRet.code));
                            }
                        });

                        MyConfig.usr_pwd = "";
                        MyConfig.saveUserDataToPreference(_ActivityLogin.this);

                    }else if (jsonLoginRet.data != null) {

                        MyConfig.logoutClear();

                        MyConfig.usr_login_ret_json = s;
                        MyConfig.jsonWeixinLoginRet = jsonLoginRet;
                        MyConfig.usr_id = jsonLoginRet.data.id;
                        MyConfig.usr_name = jsonLoginRet.data.name;//注意name要从这里取，因为login_name_or_phone也可能是phone
                        MyConfig.usr_nickname = jsonLoginRet.data.nickName;
                        MyConfig.usr_phone = jsonLoginRet.data.phone;
                        MyConfig.usr_pwd = pwd;
                        MyConfig.usr_token = jsonLoginRet.data.authToken;//常用变量就提取出来
                        MyConfig.usr_status = MyConfig.USR_STATUS_LOGIN;
                     //   MyConfig.usr_email = jsonLoginRet.data.email;
                        MyConfig.usr_phone = jsonLoginRet.data.phone;
                        if (jsonLoginRet.data.sex) {
                            MyConfig.usr_sex = 1;//男
                        } else {
                            MyConfig.usr_sex = 0;//女
                        }
                        MyConfig.usr_avatar = jsonLoginRet.data.avatar;
                        MyConfig.usr_origin = jsonLoginRet.data.origin;
                        if(jsonLoginRet.data.info != null){MyConfig.usr_name_changed = jsonLoginRet.data.info.changed;}

                        MyConfig.saveUserDataToPreference(_ActivityLogin.this);

                        finishWithSuccess();
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            Toast.makeText(ActivityLogin.this, getString(R.string.net_msg_not_connected), Toast.LENGTH_SHORT).show();
                            MyConfig.MyToast(0, _ActivityLogin.this, getString(R.string.net_msg_not_connected));
                        }
                    });
                }

            }
        }).start();
    }

    private void finishWithSuccess() {

        EventBus.getDefault().post(new EventLogin());

        setResult(RESULT_OK, new Intent());
        finish();
    }

    private void finishWithFailure() {
        setResult(RESULT_CANCELED, new Intent());
        finish();
    }

    private boolean backPressed = false;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            Toast.makeText(this, getResources().getString(R.string.sys_quit_notice), Toast.LENGTH_SHORT).show();
            MyConfig.MyToast(-1, this, getResources().getString(R.string.sys_quit_notice));

            if (backPressed == true) {
//                MyConfig.activityViewPager.finish();
//                finish();
                EventBus.getDefault().post(new EventAppQuit());
            } else {
                backPressed = true;
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        backPressed = false;
                    }
                }, 2000);

            }
            return true; // 让系统不再处理这次回退键，因为系统收到回退键后会立即把app关了
        }
        return super.onKeyDown(keyCode, event);
    }
}
