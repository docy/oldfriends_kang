package co.docy.oldfriends.Activitis;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.Vote;
import co.docy.oldfriends.Messages.JsonUploadImageRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.ViewVoteCreateOption;

/**
 * Created by youhy on 5/13/15.
 */
public class ActivityCreateVote extends ActivityBase {

    EditText create_vote_et_title;
    ImageView create_vote_add_photo_avatar;
    ImageView create_vote_photo;
    TextView create_vote_add_photo_info;
    CheckBox create_vote_multi_option;
    CheckBox create_vote_anonymous_option;
    LinearLayout create_vote_option_layout;

    LinkedList<ViewVoteCreateOption> viewVoteCreateOptionList = new LinkedList<>();
    LinkedList<ViewVoteCreateOption> ll_blank = new LinkedList<>();

    File tempAttach;
    int imageId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_vote);

        ActionBar ab = getActionBar();
        if (ab != null) {
            ab.show();
        }

        create_vote_et_title = (EditText) findViewById(R.id.create_vote_et_title);

        create_vote_add_photo_avatar = (ImageView) findViewById(R.id.create_vote_add_photo_avatar);

        create_vote_photo = (ImageView) findViewById(R.id.create_vote_photo);
        create_vote_photo.setOnClickListener(onClickListener);
        create_vote_add_photo_info = (TextView) findViewById(R.id.create_vote_add_photo_info);
        create_vote_add_photo_info.setOnClickListener(onClickListener);


        create_vote_option_layout = (LinearLayout) findViewById(R.id.create_vote_option_layout);
        addNewOption();


        create_vote_multi_option = (CheckBox) findViewById(R.id.create_vote_multi_option);
        create_vote_anonymous_option = (CheckBox) findViewById(R.id.create_vote_anonymous_option);


    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            intent.setType(MyConfig.MIME_IMAGE_STAR);

            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, MyConfig.REQUEST_GALLERY);
            }

        }
    };

    private class CustomTextWatcher implements TextWatcher {
        private ViewVoteCreateOption mViewVoteCreateOption;
//        private int optNum;

        public CustomTextWatcher(ViewVoteCreateOption e) {
            mViewVoteCreateOption = e;
//            optNum = position;
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(Editable s) {
            ll_blank.clear();
            for (ViewVoteCreateOption v : viewVoteCreateOptionList) {
                if (v.tv_vote_option_create_et.getText().length() == 0) {
                    ll_blank.add(v);

                    MyLog.d("", "opt " + mViewVoteCreateOption.getNum() + " add blank " + v.getNum());
                }
            }
            if (ll_blank.size() == 1) {

            } else if (ll_blank.size() > 1) {
                /**
                 * delete the other blank row, not this one
                 */
                if (ll_blank.get(0).getNum() != mViewVoteCreateOption.getNum()) {
                    //del position 0
                    MyLog.d("", "opt " + mViewVoteCreateOption.getNum() + " del " + ll_blank.get(0).getNum());
                    delOption(ll_blank.get(0).getNum());

                } else {
                    //del position 1
                    MyLog.d("", "opt " + mViewVoteCreateOption.getNum() + "  del " + ll_blank.get(1).getNum());
                    delOption(ll_blank.get(1).getNum());
                }
            } else {
                if (create_vote_option_layout.getChildCount() < MyConfig.VOTE_OPT_MAX) {
                    addNewOption();
                }
            }

        }
    }

    private void delOption(int position) {
        create_vote_option_layout.removeViewAt(position);
        viewVoteCreateOptionList.remove(position);

        int i = 0;
        for (ViewVoteCreateOption v : viewVoteCreateOptionList) {
            v.setNum(i);
            v.tv_vote_option_create_et.addTextChangedListener(
                    new CustomTextWatcher(v));
            i++;
        }
    }

    private void addNewOption() {
        int newPosition = viewVoteCreateOptionList.size();
        ViewVoteCreateOption viewVoteCreateOption = new ViewVoteCreateOption(this, newPosition);
        viewVoteCreateOption.tv_vote_option_create_et.addTextChangedListener(
                new CustomTextWatcher(viewVoteCreateOption));
        viewVoteCreateOptionList.addLast(viewVoteCreateOption);
        create_vote_option_layout.addView(viewVoteCreateOption, create_vote_option_layout.getChildCount());
    }


    public void backWithoutResult() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);

//        MyConfig.cleanAndFinishActivity(this);
        finish();
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        backWithoutResult();
    }

    private void backWithResult() {

        MyConfig.tempVote = new Vote();
        int i = 0;
        int j = 0;
        for (ViewVoteCreateOption v : viewVoteCreateOptionList) {
            String opt = v.tv_vote_option_create_et.getText().toString();
            if (opt.length() != 0) {
//                MyConfig.tempVote.ll_option.add(opt);
                MyConfig.tempVote.options.put("" + i, opt);
                j++;
            }
            i++;
        }

        if (j < MyConfig.VOTE_OPT_MIN) {

            MyConfig.MyToast(0, this, getResources().getString(R.string.atleast_two_option));
            return;
        }


        MyConfig.tempVote.title = create_vote_et_title.getText().toString().trim();
        create_vote_et_title.setText(MyConfig.tempVote.title);
        if ((MyConfig.tempVote.title == null) || (MyConfig.tempVote.title.length() == 0)) {

            MyConfig.MyToast(0, this, getResources().getString(R.string.null_title_remind));
            return;

        }
        MyConfig.tempVote.single = !create_vote_multi_option.isChecked();//如果multi为true，则single为false
        MyConfig.tempVote.anonymous = create_vote_anonymous_option.isChecked();
        MyConfig.tempVote.imageId = imageId;

        Intent intent = new Intent();
        if (tempAttach != null) {
            intent.putExtra(MyConfig.INTENT_KEY_CREATE_VOTE_ATTACHED, tempAttach.getAbsolutePath());
        }
        setResult(RESULT_OK, intent);
//        MyConfig.cleanAndFinishActivity(this);
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_vote_create, menu);
//        return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
//                Toast.makeText(this, "home key is pressed, input is discarded", Toast.LENGTH_LONG).show();

                backWithoutResult();
                return true;
            case R.id.create_vote_submit:
                backWithResult();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyLog.d("", "onActivityResult hit ActivityRegistPersonInfo! request code " + requestCode + " result code " + resultCode);

        if (requestCode == MyConfig.REQUEST_GALLERY && resultCode == RESULT_OK) {

            Uri fileUri = data.getData();
            String realPath = MyConfig.getUriPath(this, fileUri);
            tempAttach = new File(realPath);
            if (tempAttach != null) {
                create_vote_photo.setVisibility(View.VISIBLE);
                MyConfig.imageViewShowWithLimit(create_vote_photo, tempAttach, MyConfig.getWidthByScreenPercent(66));
                create_vote_add_photo_info.setText(getResources().getString(R.string.click_to_change_image));
            }
        }


        if (MyConfig.FALSE_TAG) {
            if (requestCode == MyConfig.REQUEST_GALLERY_AVATAR && resultCode == Activity.RESULT_OK) {

                Intent innerIntent = new Intent("com.android.camera.action.CROP");
                innerIntent.setDataAndType(data.getData(), MyConfig.MIME_IMAGE_STAR);
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true");// 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
                innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
                if (innerIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);
                }

            }

            if (requestCode == MyConfig.REQUEST_IMAGE_CROP && resultCode == Activity.RESULT_OK) {
                MyLog.d("", "onActivityResult hit ActivityRegistPersonInfo! REQUEST_IMAGE_CROP" + resultCode);

                Bitmap bitmap = data.getParcelableExtra("data");

                tempAttach = new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg");

                // 图像保存到文件中
                FileOutputStream foutput = null;
                try {
                    foutput = new FileOutputStream(tempAttach);
                    if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, foutput)) {

//                        Toast.makeText(MyConfig.app,
//                                "截图已生成缓存文件，等待上传：" + tempAttach.getAbsolutePath(),
//                                Toast.LENGTH_LONG).show();
                        MyConfig.MyToast(-1, MyConfig.app,
                                getResources().getString(R.string.image_feil_exsit) + tempAttach.getAbsolutePath());

//                    Picasso.with(this).load(tempAttach)
//                            .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
//                            .transform(new RoundedTransformation(MyConfig.AVATAR_RADIUS, 0))
//                            .into(create_vote_add_photo_avatar);
//

                        MyConfig.imageViewShowWithLimit(create_vote_add_photo_avatar, tempAttach, MyConfig.getWidthByScreenPercent(66));

                    }
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                MyLog.d("", "onActivityResult hit fragment! uploadUserAvatar() file length " + tempAttach.length());

            }
        }

    }

    public void uploadImage() {
        MyLog.d("", "OKHTTP: uploadImage() : " + tempAttach.getAbsolutePath());
        create_vote_add_photo_info.setText(getResources().getString(R.string.up_image_wait));

        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonUploadImageRet jsonUploadImageRet = HttpTools.okhttpUploadImage(tempAttach, "image/jpeg");

                if (jsonUploadImageRet != null && jsonUploadImageRet.code == MyConfig.retSuccess()) {
                    imageId = jsonUploadImageRet.data.id;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            create_vote_add_photo_info.setText(getResources().getString(R.string.image_ready_to_vote));

                        }
                    });

                } else {

                }

            }
        }).start();
    }


}
