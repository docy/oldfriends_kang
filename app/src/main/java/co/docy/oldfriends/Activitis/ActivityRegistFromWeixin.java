package co.docy.oldfriends.Activitis;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonUpdatePersonInfo;
import co.docy.oldfriends.Messages.JsonUpdatePersonInfoRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ActivityRegistFromWeixin extends ActivityBase {

    EditText regist_from_weixin_name, regist_from_weixin_pwd;
    Button regist_from_weixin_create, regist_from_weixin_skip;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist_from_weixin);

        MyConfig.flag_open_regist_from_weixin_activity = 0;

//        MyConfig.MyToast(1, this, "ActivityRegistFromWeixin");

        regist_from_weixin_name = (EditText)findViewById(R.id.regist_from_weixin_name);
        regist_from_weixin_pwd = (EditText)findViewById(R.id.regist_from_weixin_pwd);
        regist_from_weixin_create = (Button)findViewById(R.id.regist_from_weixin_create);
        regist_from_weixin_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String name = regist_from_weixin_name.getText().toString().trim();
                final String pwd = regist_from_weixin_pwd.getText().toString();
                JsonUpdatePersonInfo jsonUpdatePersonInfo  = new JsonUpdatePersonInfo();
                jsonUpdatePersonInfo.name = name;
                jsonUpdatePersonInfo.password = pwd;

//                MyConfig.IF_RegistFromWeixin service = HttpTools.getRetrofit().create(MyConfig.IF_RegistFromWeixin.class);
                Call<JsonUpdatePersonInfoRet> repos = HttpTools.http_api_service().registFromWeinxin(jsonUpdatePersonInfo);
                repos.enqueue(new Callback<JsonUpdatePersonInfoRet>() {
                    @Override
                    public void onResponse(Call<JsonUpdatePersonInfoRet> call, Response<JsonUpdatePersonInfoRet> response) {
                        // Get result Repo from response.body()

                        MyLog.d("", "retrofit: code() " + response.code());
                        MyLog.d("", "retrofit: message() " + response.message());

                        if(response.code()==200){

                            JsonUpdatePersonInfoRet jsonUpdatePersonInfoRet = response.body();
                            if(jsonUpdatePersonInfoRet.code == MyConfig.retSuccess()){
                                MyConfig.usr_name = name;
                                MyConfig.usr_pwd = pwd;
                            }

                            MyConfig.MyToast(1, ActivityRegistFromWeixin.this, jsonUpdatePersonInfoRet.message);

                        }

                    }

                    @Override
                    public void onFailure(Call<JsonUpdatePersonInfoRet> call, Throwable t) {
                        MyLog.d("", "retrofit: onFailure " + t.getMessage());
                    }
                });


            }
        });
        regist_from_weixin_skip = (Button)findViewById(R.id.regist_from_weixin_skip);
        regist_from_weixin_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }


}
