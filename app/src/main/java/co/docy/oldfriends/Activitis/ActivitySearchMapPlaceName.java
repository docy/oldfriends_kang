package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.amap.api.services.core.PoiItem;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;

import co.docy.oldfriends.Adapters.ListAdapterGetLocationNear;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/6/22.
 */
public class ActivitySearchMapPlaceName extends ActivityBase implements PoiSearch.OnPoiSearchListener{
    ListView listview;
    EditText editText;
    TextView button_text;
    ImageView image_black;
    private PoiSearch.Query query;// Poi查询条件类
    private PoiSearch poiSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_search_map_place_name);
        listview =(ListView) findViewById(R.id.activity_search_map_listview);
        editText =(EditText) findViewById(R.id.activity_search_map_edittext);
        button_text =(TextView) findViewById(R.id.activity_search_map_button);
        image_black =(ImageView) findViewById(R.id.activity_search_map_arrow);
        image_black.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        button_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchByName();
            }
        });

    }
    public void searchByName(){
        String city = getIntent().getStringExtra("city");
        String placeName = editText.getText().toString();
        query = new PoiSearch.Query(placeName, "", "");// 第一个参数表示搜索字符串，第二个参数表示poi搜索类型，第三个参数表示poi搜索区域（空字符串代表全国）
        /*query.setPageSize(10);// 设置每页最多返回多少条poiitem
        query.setPageNum(currentPage);// 设置查第一页*/

        poiSearch = new PoiSearch(this, query);
        poiSearch.setOnPoiSearchListener(this);
        poiSearch.searchPOIAsyn();

    }

    @Override
    public void onPoiSearched(final PoiResult poiResult, int i) {
        if (poiResult.getPois() != null) {
            ListAdapterGetLocationNear mAdapter = new ListAdapterGetLocationNear(this,1, poiResult.getPois(), new ListAdapterGetLocationNear.layoutClick() {
                @Override
                public void layoutClickLister(int i) {
                    Intent intent = new Intent();
                    intent.putExtra("Latitude",poiResult.getPois().get(i).getLatLonPoint().getLatitude());
                    intent.putExtra("Longitude",poiResult.getPois().get(i).getLatLonPoint().getLongitude());
                    intent.putExtra("name",String.valueOf(poiResult.getPois().get(i)));
                    setResult(RESULT_OK,intent);
                    finish();
                }
            });
            listview.setAdapter(mAdapter);
        }
    }
    @Override
    public void onPoiItemSearched(PoiItem poiItem, int i) {

    }
}
