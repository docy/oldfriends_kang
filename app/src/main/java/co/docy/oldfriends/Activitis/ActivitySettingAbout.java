package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import co.docy.oldfriends.BuildConfig;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventReConnectSocket;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import de.greenrobot.event.EventBus;

/**
 * Created by khp on 2016/4/22.
 * 我的设置中关于的Activity
 */
public class ActivitySettingAbout extends ActivityBase {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_about);

        TextView versionCode = (TextView) findViewById(R.id.activity_setting_about_version);
        ImageView activity_setting_about_icon = (ImageView) findViewById(R.id.activity_setting_about_icon);
        TextView activity_setting_about_compile = (TextView) findViewById(R.id.activity_setting_about_compile);

        Picasso.with(this).load(R.drawable.c_ic_launcher)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(20, 0))
                .into(activity_setting_about_icon);

        MyConfig.appVersionCheck();

        /**显示当前版本号*/
        versionCode.setText(getResources().getString(R.string.app_name)+" V"+MyConfig.appVersion_from_local.versionName);

        /*编译时间*/
        Date buildDate = new Date(BuildConfig.TIMESTAMP);
        Format formatter = new SimpleDateFormat(MyConfig.app.getString(R.string.time_yyyy_MM_dd_HH_mm));

        activity_setting_about_compile.setText("安装包生成时间: "+ formatter.format(buildDate));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_about, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_share_app:
//                MyConfig.shareLinkByUMeng(this, "https://www.pgyer.com/laopengyou");
                MyConfig.updateAppDownloadUrl(new MyConfig.CallBack_IfSuccess() {
                    @Override
                    public void ifSuccess() {
                        MyConfig.shareAppToWeixin(ActivitySettingAbout.this, "老朋友", "有伙伴，无寂寞", MyConfig.appDownloadUrl, R.drawable.c_ic_launcher);
                    }

                    @Override
                    public void ifFailed(){

                    }

                });
//                MyConfig.shareStringToWeixin(this, "分享老朋友", "老朋友", "https://www.pgyer.com/laopengyou");
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
