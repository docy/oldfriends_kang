package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.LinkedList;

import co.docy.oldfriends.Adapters.ListAdapterGroups;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.Group;
import co.docy.oldfriends.Messages.JsonUserGroupsRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by wuxue on 15/10/29.
 */
public class ActivityShareInWindow extends ActivityBase {
    // public LinkedList<JsonUserGroupsRet.UserGroupsRet> list_group = new LinkedList<>();
    public LinkedList<Group> list_group = new LinkedList<>();
    ListAdapterGroups adapter;
    ListView activity_company_group_list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        /**
         * 可能有如下情况
         *
         *  viewpager已经启动，并且已经登录、设定当前公司，进入正常使用
         *      在onCreate中设置flag_open_share_in_window为true
         *      在onResume中设置flag_open_share_in_window为false
         *      不影响其他activity
         *
         *  viewpager已经启动，但没有登录
         *      在onCreate中设置flag_open_share_in_window为true
         *      启动viewpager或login，等等用户登录
         *      关掉自己
         *      在viewpager的onResume中根据flag_open_share_in_window来启动ShareInWindow
         *
         *  viewpager没有启动，也即app没有启动
         *      在onCreate中设置flag_open_share_in_window为true
         *      启动viewpager或login，等等用户登录
         *      在viewpager的onResume中根据flag_open_share_in_window来启动ShareInWindow
         *
         */

        if (MyConfig.flag_have_share_in) {
            MyLog.d("", "share in process: start 1 ActivityShareInWindow.... " + MyConfig.flag_have_share_in);
            /**
             * 上次的sharein还没处理，这里应该是第二次进入
             * 如果某种意外，这里是第一次进入，那么发送上次的sharein，抛弃本次sharein
             * 未来可考虑做一个FIFO队列，但一般没必要
             */

        } else {
            MyLog.d("", "share in process: start 2 ActivityShareInWindow.... " + MyConfig.flag_have_share_in);
            MyConfig.flag_have_share_in = true;
            MyConfig.intent_share_in = getIntent();
            MyConfig.shareParse(MyConfig.intent_share_in);//注意不能放在后面处理，除非深clone这个intent

        }

        if (MyConfig.activityViewPager == null) {
            MyLog.d("", "share in process: close for no activityViewPager");
            Intent intent = new Intent(this, ActivityViewPager.class);
            startActivity(intent);
            finish();
//            return;//不用运行finish之后的代码了
        }
        if (MyConfig.usr_status < MyConfig.USR_STATUS_HAVE_CURRENT_COMPANY) {
            MyLog.d("", "share in process: close for no current company");

            Intent intent = new Intent(this, ActivityViewPager.class);
            startActivity(intent);

            finish();
//            return;
        }

        super.child_activity_name = this.getClass().getName();
        super.onCreate(savedInstanceState);

        setTitle(getResources().getString(R.string.joined_group));
        setContentView(R.layout.activity_company_group_list);
        activity_company_group_list = (ListView) findViewById(R.id.activity_company_group_list);

    }


    @Override
    protected void onResume() {
        super.onResume();

        MyConfig.flag_have_share_in = false;

        useGroupList();
    }

    private void useGroupList() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();
                /**
                 * 获取已经加入的rooms
                 */
                String user_rooms = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_ROOMS, MyConfig.usr_token);
                MyLog.d("", "apitest: user rooms " + user_rooms);
                MyConfig.jsonUserGroupsRet = gson.fromJson(user_rooms, JsonUserGroupsRet.class);

                if (MyConfig.jsonUserGroupsRet != null && MyConfig.jsonUserGroupsRet.code==MyConfig.retSuccess()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            list_group.clear();
                            //    list_group = MyConfig.jsonUserGroupsRet.data;
                            for (JsonUserGroupsRet.UserGroupsRet urr : MyConfig.jsonUserGroupsRet.data) {
                                if (
                                        urr.category == MyConfig.SERVER_ROOM_CATEGORY_NORMAL
                                        || urr.category == MyConfig.SERVER_ROOM_CATEGORY_PRIVATE
                                        || urr.category == MyConfig.SERVER_ROOM_CATEGORY_DIRECT
                                        || urr.category == MyConfig.SERVER_ROOM_CATEGORY_CLASS
                                        ) {
                                    list_group.add(Group.fromUserGroupsRet(urr));
                                }
                            }
                            adapter = new ListAdapterGroups(ActivityShareInWindow.this, list_group, MyConfig.GROUP_LIST_TYPE_GROUP_SIMPLE_INFO, null, null);
                            activity_company_group_list.setAdapter(adapter);
                            activity_company_group_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                    Intent intent_main = new Intent(ActivityShareInWindow.this, ActivityMainGroup.class);
                                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, list_group.get(position).createGroupRet.id);
                                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, list_group.get(position).createGroupRet.name);
                                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, list_group.get(position).createGroupRet.desc);
                                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_MAINROOM_START_MODE, MyConfig.MAINROOM_STARTED_FOR_SHARE_IN);
                                    MyLog.d("", "share in process: invode mainroom: " + list_group.get(position).createGroupRet.name);
                                    startActivity(intent_main);
                                    finish();
                                }
                            });
                        }
                    });

                }
                MyLog.d("", "grouplist refresh ActivityViewPager groupListUpdateFromServer 2");

            }
        }).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_info, menu);
        // return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_test:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
