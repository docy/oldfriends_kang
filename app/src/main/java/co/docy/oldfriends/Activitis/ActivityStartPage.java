package co.docy.oldfriends.Activitis;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import cn.jpush.android.api.JPushInterface;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.R;


public class ActivityStartPage extends AppCompatActivity {

    private ImageView start_page;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_page);
        start_page = (ImageView) findViewById(R.id.start_page);
        Picasso.with(ActivityStartPage.this).load(R.drawable.c_start_page_3x)
                .fit()
                .into(start_page);
        getSupportActionBar().hide();
        MyConfig.app_open_count = MyConfig.loadIntFromPreference(ActivityStartPage.this, "app_open_count");
        MyConfig.loadAppLang(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1500);
                    if(MyConfig.app_open_count == -1){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Picasso.with(ActivityStartPage.this).load(R.drawable.c_start_page_3x)
                                        .fit()
                                        .into(start_page);
                            }
                        });

                    }else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                               /* Picasso.with(ActivityStartPage.this).load(R.drawable.new_loading_two_2x)
                                        .fit()
                                        .into(start_page);*/
                              if (MyConfig.getOrderStratPage() != null) {
                                    Picasso.with(ActivityStartPage.this)
                                            .load(MyConfig.getOrderStratPage())
                                            .fit()
                                            .into(start_page);


                                }
                            }
                        });
                        Thread.sleep(1000);
                    }

                    if (MyConfig.app_open_count == -1) {
                        MyLog.d("查看是否进入此页面", "app start first times(not enter viewpager), app_open_count:" + MyConfig.app_open_count);

                        Intent newIntent = new Intent(ActivityStartPage.this, ActivityIntroducePages.class);
                        startActivity(newIntent);
                        finish();

                        return;
                    } else {
                        MyLog.d("查看是否进入此页面", "app start first times(entered viewpager), app_open_count:" + MyConfig.app_open_count);

                        Intent newIntent = new Intent(ActivityStartPage.this, ActivityViewPager.class);
                        startActivity(newIntent);

                        finish();
                        return;
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        JPushInterface.onResume(this);
    }
    @Override
    protected void onPause() {
        super.onPause();
        JPushInterface.onPause(this);
    }
}
