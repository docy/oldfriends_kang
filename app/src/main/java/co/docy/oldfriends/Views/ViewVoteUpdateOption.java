package co.docy.oldfriends.Views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.docy.oldfriends.Adapters.ListAdapterMsg;
import co.docy.oldfriends.DataClass.IMsg;
import co.docy.oldfriends.R;

/**
 * Created by youhy on 5/14/15.
 */
public class ViewVoteUpdateOption extends LinearLayout {
    /**
     * if var's change involve other var, set it to private to force using setter/getter
     */
    public IMsg imsg;
    public int num;
    private String option;
    private boolean checked;
    public TextView tv_vote_option_update_num;
    public TextView tv_vote_option_update;
    public ImageView tb_vote_option_update;
    public LinearLayout ll_vote_option_update;

    public ListAdapterMsg.OnVoteOptionClickedListener voteOptionClickedListener;

    public ViewVoteUpdateOption(Context context, IMsg iMsg, String optionStr, int optionNum) {

        super(context);

        this.imsg = iMsg;
        this.option = optionStr;
        this.num = optionNum;

        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.row_vote_option_update, this, true);

        ll_vote_option_update = (LinearLayout)v.findViewById(R.id.update_vote_option_layout);
        tv_vote_option_update_num = (TextView) v.findViewById(R.id.update_vote_option_num);
        tv_vote_option_update_num.setText(""+num);
        tv_vote_option_update = (TextView)v.findViewById(R.id.update_vote_option_tv);
        tv_vote_option_update.setText(option);
        tb_vote_option_update = (ImageView)v.findViewById(R.id.update_vote_option_tb);

        tb_vote_option_update.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if(imsg.vote.checked.contains(num)){
                    imsg.vote.checked.remove(num);
                }else{
                    imsg.vote.checked.add(num);
                }

                showChecked();

                if(voteOptionClickedListener != null){
                    voteOptionClickedListener.onVoteOptionClicked(imsg, num, imsg.vote.checked.contains(num));
                }
            }
        });

        showChecked();

    }


    public void showChecked() {



        if(imsg.vote.checked.contains(num)){
            tb_vote_option_update.setBackground(getResources().getDrawable(R.drawable.switch_selected_3x));
        }else{
            tb_vote_option_update.setBackground(getResources().getDrawable(R.drawable.switch_normal_3x));
        }

    }

//    public void notifyBottonGroup(int num, boolean checked){
//        if(voteOptionClickedListener != null){
//            voteOptionClickedListener.onVoteOptionClicked(imsg, num, checked);
//        }
//    }
//
//    public int getNum(){
//        return num;
//    }
//
//    public void setNum(int num){
//        this.num = num;
//        tv_vote_option_update_num.setText("" + (num+1));
//
//    }
//
//    public String getOption(){
//        return option;
//    }
//    public void setOption(String option){
//        this.option = option;
//        tv_vote_option_update.setText(this.option);
//    }
//
//    public boolean isChecked(){
//        return checked;
//    }
//
//    private void notifyIMsg(boolean b) {
//        if(b){
//            imsg.vote.checked.add(num);
//        }else{
//            imsg.vote.checked.remove(num);
//        }
//    }


}
