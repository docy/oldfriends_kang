package co.docy.oldfriends.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import co.docy.oldfriends.DataClass.CalendarDay;
import co.docy.oldfriends.DataClass.CalendarWeek;
import co.docy.oldfriends.R;

/**
 * Created by youhy on 5/14/15.
 */
public class ViewCalendarWeekWithActivity extends LinearLayout {

    CalendarWeek calendarWeek;

    public LinearLayout calendar_week_day_layout;
    public ViewCalendarDayWithActivity calendar_day1;
    public ViewCalendarDayWithActivity calendar_day2;
    public ViewCalendarDayWithActivity calendar_day3;
    public ViewCalendarDayWithActivity calendar_day4;
    public ViewCalendarDayWithActivity calendar_day5;
    public ViewCalendarDayWithActivity calendar_day6;
    public ViewCalendarDayWithActivity calendar_day7;


    public interface DayOnClickListener{
        void dayOnClicked(CalendarDay day); //1~7
    }
    public DayOnClickListener dayOnClickListener;


    public ViewCalendarWeekWithActivity(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context);
    }

    public ViewCalendarWeekWithActivity(Context context) {

        super(context);

        inflate(context);

    }

    public class WeekOnClickListener implements OnClickListener{

        CalendarDay day;

        public WeekOnClickListener(CalendarDay day){
            this.day = day;
        }

        @Override
        public void onClick(View view) {
            if(dayOnClickListener != null){
                dayOnClickListener.dayOnClicked(day);
            }
        }


    }


    private void inflate(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.calendar_week_with_activity, this, true);

        calendar_week_day_layout = (LinearLayout)v.findViewById(R.id.calendar_week_day_layout);

        calendar_day1 = (ViewCalendarDayWithActivity) v.findViewById(R.id.calendar_day1);
        calendar_day2 = (ViewCalendarDayWithActivity) v.findViewById(R.id.calendar_day2);
        calendar_day3 = (ViewCalendarDayWithActivity) v.findViewById(R.id.calendar_day3);
        calendar_day4 = (ViewCalendarDayWithActivity) v.findViewById(R.id.calendar_day4);
        calendar_day5 = (ViewCalendarDayWithActivity) v.findViewById(R.id.calendar_day5);
        calendar_day6 = (ViewCalendarDayWithActivity) v.findViewById(R.id.calendar_day6);
        calendar_day7 = (ViewCalendarDayWithActivity) v.findViewById(R.id.calendar_day7);

    }


    private void setDayOnClickListener() {
        calendar_day1.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(0)));
        calendar_day2.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(1)));
        calendar_day3.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(2)));
        calendar_day4.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(3)));
        calendar_day5.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(4)));
        calendar_day6.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(5)));
        calendar_day7.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(6)));
    }

    public void setViewCalendarWeek(CalendarWeek calendarWeek, int highLightMonth){

        if(calendarWeek == null){
            calendar_week_day_layout.setVisibility(INVISIBLE);
            return;
        }
        calendar_week_day_layout.setVisibility(VISIBLE);

        this.calendarWeek = calendarWeek;

        calendar_day1.setViewCalendarDay(calendarWeek.calendarDayList.get(0), highLightMonth);
        calendar_day2.setViewCalendarDay(calendarWeek.calendarDayList.get(1), highLightMonth);
        calendar_day3.setViewCalendarDay(calendarWeek.calendarDayList.get(2), highLightMonth);
        calendar_day4.setViewCalendarDay(calendarWeek.calendarDayList.get(3), highLightMonth);
        calendar_day5.setViewCalendarDay(calendarWeek.calendarDayList.get(4), highLightMonth);
        calendar_day6.setViewCalendarDay(calendarWeek.calendarDayList.get(5), highLightMonth);
        calendar_day7.setViewCalendarDay(calendarWeek.calendarDayList.get(6), highLightMonth);

        setDayOnClickListener();

    }

}
