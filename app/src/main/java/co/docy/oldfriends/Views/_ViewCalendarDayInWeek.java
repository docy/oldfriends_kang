package co.docy.oldfriends.Views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.joda.time.LocalDate;

import co.docy.oldfriends.DataClass.CalendarDay;
import co.docy.oldfriends.R;

/**
 * Created by youhy on 5/14/15.
 */
public class _ViewCalendarDayInWeek extends LinearLayout {

    public ImageView calendar_day_have_activity;
    public TextView calendar_day_in_week;
    public TextView calendar_day_in_month;
    public LinearLayout calendar_day_layout;


    public _ViewCalendarDayInWeek(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context);
    }

    public _ViewCalendarDayInWeek(Context context) {

        super(context);

        inflate(context);

    }

    private void inflate(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.calendar_day_in_week, this, true);

        calendar_day_layout = (LinearLayout) v.findViewById(R.id.calendar_week_layout);
        calendar_day_have_activity = (ImageView) v.findViewById(R.id.calendar_day_have_activity);
        calendar_day_in_week = (TextView) v.findViewById(R.id.calendar_day_in_week);
        calendar_day_in_month = (TextView) v.findViewById(R.id.calendar_day_in_month);
    }

    public void setViewCalendarDay(CalendarDay calendarDay){

        calendar_day_in_week.setText(getLocalDayOfWeek(calendarDay.day.getDayOfWeek()));
        calendar_day_in_month.setText(""+calendarDay.day.getDayOfMonth());

        if(calendarDay.have_activity){
            calendar_day_have_activity.setVisibility(VISIBLE);
        }else{
            calendar_day_have_activity.setVisibility(INVISIBLE);
        }

        if(calendarDay.day.equals(new LocalDate())){
            calendar_day_in_month.setBackgroundColor(getResources().getColor(R.color.Red));
        }else{
            calendar_day_in_month.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    public String getLocalDayOfWeek(int dayOfWeek){

        switch (dayOfWeek){
            case 1:
                return "一";
            case 2:
                return "二";
            case 3:
                return "三";
            case 4:
                return "四";
            case 5:
                return "五";
            case 6:
                return "六";
            case 7:
                return "日";

        }
        return null;
    }

}
