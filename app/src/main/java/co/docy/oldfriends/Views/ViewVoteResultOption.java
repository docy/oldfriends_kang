package co.docy.oldfriends.Views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.R;

/**
 * Created by youhy on 5/14/15.
 */
public class ViewVoteResultOption extends LinearLayout {

    public int num;
    public String option;
    public int count;
    public int total;

    public TextView create_vote_option_result_num;
    public TextView create_vote_option_result_desc;
    public TextView create_vote_option_result_count;
    public TextView create_vote_option_result_chart_background;
    public TextView create_vote_option_result_chart_foreground;
    public LinearLayout update_vote_option_result_layout;

    public ViewVoteResultOption(Context context, int num, String option, int count, int total) {

        super(context);

        MyLog.d("","httptool vote result option: "+ option);

        this.num = num;
        this.option = option;
        this.count = count;
        this.total = total;

        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.row_vote_option_result, this, true);

        update_vote_option_result_layout = (LinearLayout) v.findViewById(R.id.update_vote_option_result_layout);
        create_vote_option_result_num = (TextView) v.findViewById(R.id.create_vote_option_result_num);
        create_vote_option_result_num.setText(""+(num+1)+": ");
        create_vote_option_result_desc = (TextView) v.findViewById(R.id.create_vote_option_result_desc);
        create_vote_option_result_desc.setText(option);
        create_vote_option_result_count = (TextView) v.findViewById(R.id.create_vote_option_result_count);
        create_vote_option_result_count.setText(""+count);

        create_vote_option_result_chart_background = (TextView) v.findViewById(R.id.create_vote_option_result_chart_background);
        create_vote_option_result_chart_foreground = (TextView) v.findViewById(R.id.create_vote_option_result_chart_foreground);

        float percent = (float)count / (float)total;
        create_vote_option_result_chart_foreground.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, percent));
        create_vote_option_result_chart_foreground.setBackgroundResource(MyConfig.getVoteResultColor(num));
        create_vote_option_result_chart_background.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1-percent));

    }

}
