package co.docy.oldfriends.Views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.docy.oldfriends.DataClass.IMsg;
import co.docy.oldfriends.R;

/**
 * Created by youhy on 5/14/15.
 */
public class ViewVoteResultInfo extends LinearLayout {

    public TextView update_vote_sum_anonymou_single;
    public TextView update_vote_sum_vote_num;
    public LinearLayout update_vote_sum_layout;

    public ViewVoteResultInfo(Context context, IMsg iMsg) {

        super(context);

        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.row_vote_info, this, true);

        update_vote_sum_layout = (LinearLayout) v.findViewById(R.id.update_vote_sum_layout);

        update_vote_sum_anonymou_single = (TextView) v.findViewById(R.id.update_vote_sum_anonymou_single);
        StringBuilder sb = new StringBuilder();
        if(iMsg.vote.anonymous){
            sb.append(context.getString(R.string.vote_anonymous));
        }else{
            sb.append(context.getString(R.string.vote_register));
        }
        if(iMsg.vote.single){
            sb.append(context.getString(R.string.vote_single_choose));
        }else{
            sb.append(context.getString(R.string.vote_multi_choose));
        }
        update_vote_sum_anonymou_single.setText(sb.toString());

        if (iMsg.vote.voterNum >= 0) {
            update_vote_sum_vote_num = (TextView) v.findViewById(R.id.update_vote_sum_vote_num);//iMsg.vote.voterNum + context.getString(R.string.vote_people)
            update_vote_sum_vote_num.setText(String.format(context.getString(R.string.vote_people), iMsg.vote.voterNum));
            if (!iMsg.vote.anonymous) {//记名投票则这里可以点击
                update_vote_sum_vote_num.setTextColor(getResources().getColor(R.color.voter_num));
            }
        }

    }

}
