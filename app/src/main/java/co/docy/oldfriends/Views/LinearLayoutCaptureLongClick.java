package co.docy.oldfriends.Views;

import android.content.Context;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

import co.docy.oldfriends.Configure.MyLog;

/**
 * Created by youhy on 5/14/15.
 */
public class LinearLayoutCaptureLongClick extends LinearLayout {

    /**
     * 这个设计在当前环境下工作受限，因为touch事件可能被父层及祖父层截走
     *  祖父层：pulltorefresh
     *  父层：listview
     */


    Context mContext;

    public interface OnOverLayerTouchListener {
        boolean onOverLayerTouch(MotionEvent event);
    }
    public OnOverLayerTouchListener mTouchListener;

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

            if(mTouchListener != null){

                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        MyLog.d("","setOnItemLongClickListener: get ACTION_DOWN! " + SystemClock.elapsedRealtime());
                        break;
                    case MotionEvent.ACTION_MOVE:
                        MyLog.d("","setOnItemLongClickListener: get ACTION_MOVE! " + SystemClock.elapsedRealtime());
                        break;
                    case MotionEvent.ACTION_UP:
                        MyLog.d("","setOnItemLongClickListener: get ACTION_UP! " + SystemClock.elapsedRealtime());
                        break;
                }

                if(mTouchListener.onOverLayerTouch(event)){
                    /**
                     * 怎样发送一条cancel信息？
                     */
//                    dispatchTouchEvent(MotionEvent.ACTION_CANCEL);
                }else{
                    super.dispatchTouchEvent(event); // 继续传播event
                }

                return true;

            }else {
                super.dispatchTouchEvent(event);
                return false;//我也不用再接收了
            }
    }

    public LinearLayoutCaptureLongClick(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        MyLog.d("", "GestureButtonLayout(3)");
        mContext = context;

    }

    public LinearLayoutCaptureLongClick(Context context, AttributeSet attrs) {
        super(context, attrs);

        MyLog.d("", "GestureButtonLayout(2)");
        mContext = context;

    }

    public LinearLayoutCaptureLongClick(Context context) {
        super(context);

        MyLog.d("", "GestureButtonLayout(1)");
        mContext = context;

    }
}
