package co.docy.oldfriends.Views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import co.docy.oldfriends.Activitis.ActivityActivityMoreMemberList;
import co.docy.oldfriends.Activitis.ActivityLocationView;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.DataClass.IMsg;
import co.docy.oldfriends.DataClass.MapLocation;
import co.docy.oldfriends.Messages.JsonGetTopicDetailRet;
import co.docy.oldfriends.R;

/**
 * Created by fighting on 2016/6/6.
 */
public class ViewActivityInfoSubroom extends LinearLayout{
    private IMsg mIMsg;
    private LinearLayout ll_activity_people_join;
    private Context mContext;
    private TextView activity_people_max;
    private TextView activity_title;
    private TextView activity_desc;

    private LinearLayout ll_activity_people_join_container;
    private LinearLayout ll_activity_people_num_max;
    private LinearLayout ll_activity_address;
    private TextView activity_join_people_num;
    private LinearLayout ll_activity_time_start;
    private LinearLayout ll_activity_time_limit;
    private TextView activity_time_start;
    private TextView activity_address;
    private TextView create_activity_time_limit;

    public LinearLayout ll_activity_people_num;
    public TextView activity_people_num;
    private LinearLayout ll_activity_expend;
    private boolean isExpend;
    private LinearLayout ll_activity_time_end;
    private TextView activity_time_end;
    private TextView activity_cost;
    private ImageView activity_expend_icon;
    private TextView create_activity_sender;
    private ImageView activity_result;
    private TextView activity_expend_text;

    public ViewActivityInfoSubroom(Context context, IMsg iMsg) {

        super(context);
        mContext = context;
        mIMsg = iMsg;

        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.row_activity_info_subroom, this, true);

        initView(v);
        initListener();
        initData();

    }

    /**
     * 初始化view
     * @param v
     */
    private void initView(View v) {
        /**下面是初始化容器控件*/
        ll_activity_expend = (LinearLayout) v.findViewById(R.id.ll_activity_expend);
        ll_activity_time_start = (LinearLayout) v.findViewById(R.id.ll_activity_time_start);
        ll_activity_time_end = (LinearLayout) v.findViewById(R.id.ll_activity_time_end);
        ll_activity_address = (LinearLayout) v.findViewById(R.id.ll_activity_address);
        ll_activity_time_limit = (LinearLayout) v.findViewById(R.id.ll_activity_time_limit);
        ll_activity_people_join = (LinearLayout) v.findViewById(R.id.ll_activity_people_join);
        ll_activity_people_num = (LinearLayout) v.findViewById(R.id.ll_activity_people_num);

        ll_activity_people_join_container = (LinearLayout) v.findViewById(R.id.ll_activity_people_join_container);

        /**下面是初始化子控件*/
        activity_desc = (TextView) v.findViewById(R.id.activity_desc);
        activity_expend_icon = (ImageView) v.findViewById(R.id.activity_expend_icon);
        activity_expend_text = (TextView) v.findViewById(R.id.activity_expend_text);
        activity_result = (ImageView) v.findViewById(R.id.activity_result);
        activity_time_start = (TextView) v.findViewById(R.id.activity_time_start);
        activity_time_end = (TextView) v.findViewById(R.id.activity_time_end);
        activity_address = (TextView) v.findViewById(R.id.activity_address);
        activity_cost = (TextView) v.findViewById(R.id.activity_cost);
        create_activity_time_limit = (TextView) v.findViewById(R.id.create_activity_time_limit);
        activity_join_people_num = (TextView) v.findViewById(R.id.activity_join_people_num);
        activity_people_max = (TextView) v.findViewById(R.id.activity_people_max);
        activity_people_num = (TextView) v.findViewById(R.id.activity_people_num);
        create_activity_sender = (TextView) v.findViewById(R.id.create_activity_sender);

        if(mIMsg.originDataFromServer.getMsgTopicRet.userId == MyConfig.usr_id){/**如果是自己创建的*/
            ll_activity_people_num.setVisibility(View.GONE);
           // ll_activity_people_join.setVisibility(View.VISIBLE);
        }else{
            ll_activity_people_num.setVisibility(View.GONE);/**目前全部GONE，下版再支持这个功能*/

           // ll_activity_people_join.setVisibility(View.GONE);
        }
    }

    /**
     * 初始化监听器
     */
    private void initListener() {
        /**点击展开描述*/
        ll_activity_expend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isExpend){/**如果展开了*/
                    resetDescTextViewHeight(getDescTextView3LinesHeight());
                    activity_expend_icon.setImageResource(R.drawable.activity_expend_down_3x);
                    activity_expend_text.setText("展开");
                }else {/**如果没展开*/
                    resetDescTextViewHeight(getDescTextViewAllHeight());
                    activity_expend_icon.setImageResource(R.drawable.activity_expend_up_3x);
                    activity_expend_text.setText("收起");
                }
                isExpend = !isExpend;
            }
        });

        /**点击位置进入地图*/
        activity_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.tempMapLocation = new MapLocation();
                MyConfig.tempMapLocation.address = mIMsg.mActivity.location;
                MyConfig.tempMapLocation.longitude = mIMsg.mActivity.longitude;
                MyConfig.tempMapLocation.latitude = mIMsg.mActivity.latitude;

                Intent intent = new Intent(mContext, ActivityLocationView.class);
                mContext.startActivity(intent);
            }
        });

        /**点击发起人姓名，进入发起人详情页面*/
        create_activity_sender.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.gotoUserInfo(mContext,mIMsg.user_id);
            }
        });
    }


    /**
     * 初始化数据
     */
    private void initData() {
        activity_desc.setText(mIMsg.mActivity.title);
        if(getDescTextViewAllHeight()> getDescTextView3LinesHeight()){
            ll_activity_expend.setVisibility(View.VISIBLE);
        }else {
            ll_activity_expend.setVisibility(View.GONE);
        }
        showActivityResult();

        String startTime = MyConfig.getLocalDateStringWithWeek(MyConfig.String2DateUTC0(mIMsg.mActivity.startTime));
        activity_time_start.setText(startTime);
        String endTime = MyConfig.getLocalDateStringWithWeek(MyConfig.String2DateUTC0(mIMsg.mActivity.endTime));
        activity_time_end.setText(endTime);

        activity_address.setText(mIMsg.mActivity.location);
        activity_cost.setText(setActivityCostText(mIMsg.mActivity.cost));

        String limitTime = MyConfig.getLocalDateStringWithWeek(MyConfig.String2DateUTC0(mIMsg.mActivity.closeTime));
        create_activity_time_limit.setText(limitTime);
        create_activity_sender.setText(mIMsg.user_name);
        activity_join_people_num.setText(mIMsg.mActivity.joinedNum+"");
        activity_people_max.setText("/"+mIMsg.mActivity.limit);
        activity_people_num.setText("1");

        showCustomer();
    }

    /**
     * 因为费用是float类型，故当是整数时也会显示一位小数，做if判断后整数就不会显示小数点
     * 正常情况下数字会显示科学计数样式，转成成String后就不会显示这个样式了。
     * @param cost
     * @return
     */
    private String setActivityCostText(float cost){
        String text ;
        if(cost % 1.0 == 0.0){
            text =  String.valueOf((long)cost);
        }else{
            text =  String.valueOf(cost);
        }
        return text+"元";
    }


    /**
     * 显示已经报名的人
     */
    private void showCustomer() {
        if(mIMsg.mActivity.joinedUsers.size() > 6) {
            for (int i = 0; i < 6; i++) {
                showCustomerIcon(mIMsg.mActivity.joinedUsers,i);
            }
            showMoreCustomerIcon();
        }else {
            for (int i = 0; i < mIMsg.mActivity.joinedUsers.size(); i++) {
                showCustomerIcon(mIMsg.mActivity.joinedUsers,i);
            }
        }
    }

    /**
     * 显示报名结果的图标
     */
    private void showActivityResult() {
        switch (mIMsg.mActivity.status) {
            case 0://未报名
                activity_result.setVisibility(View.GONE);
                break;
            case 1://已经报名
                activity_result.setVisibility(View.VISIBLE);
                activity_result.setImageResource(R.drawable.activity_result_success_3x);
                break;
            case 2://排队
                activity_result.setVisibility(View.VISIBLE);
                activity_result.setImageResource(R.drawable.activity_result_waiting_3x);
                break;
        }
        if(mIMsg.mActivity.closed){
            activity_result.setVisibility(View.VISIBLE);
            activity_result.setImageResource(R.drawable.activity_result_end_3x);
        }
    }

    /**
     * 显示更多的图标
     */
    private void showMoreCustomerIcon() {
        ImageView moreCustomerIcon = new ImageView(mContext);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MyConfig.dp2Px(mContext,50), MyConfig.dp2Px(mContext,50));
        layoutParams.gravity = Gravity.CENTER_VERTICAL;
        moreCustomerIcon.setPadding(MyConfig.dp2Px(mContext,20),MyConfig.dp2Px(mContext,20),MyConfig.dp2Px(mContext,20),MyConfig.dp2Px(mContext,20));

        moreCustomerIcon.setImageResource(R.drawable.day_activity_arrow_right_3x);
        moreCustomerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.joinedUsers = mIMsg.mActivity.joinedUsers;
                mContext.startActivity(new Intent(mContext,ActivityActivityMoreMemberList.class));
            }
        });

        ll_activity_people_join_container.addView(moreCustomerIcon, layoutParams);
    }


    /**
     * 显示活动参与人头像和姓名
     */
    private void showCustomerIcon(final LinkedList<JsonGetTopicDetailRet.JoinedUsers> joinedUsers, final int i) {
        View join_people_list = View.inflate(mContext, R.layout.row_activity_join_people, null);
        ImageView activity_join_people_avatar = (ImageView) join_people_list.findViewById(R.id.activity_join_people_avatar);
        ImageView activity_join_people_groupuser_host = (ImageView) join_people_list.findViewById(R.id.activity_join_people_groupuser_host);
        TextView activity_join_people_name = (TextView) join_people_list.findViewById(R.id.activity_join_people_name);

        Picasso.with(mContext).load("" + MyConfig.getApiDomain_NoSlash_GetResources() + joinedUsers.get(i).avatar)
                .resize(60, 60)
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(activity_join_people_avatar);
        activity_join_people_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.gotoUserInfo(mContext, joinedUsers.get(i).id);
            }
        });
        activity_join_people_name.setText(joinedUsers.get(i).nickName);

        if(i == 0){
            activity_join_people_groupuser_host.setVisibility(View.VISIBLE);
        }
        ll_activity_people_join_container.addView(join_people_list);
    }


    /**
     * 重置描述TextView的高度
     * @param linesHeight
     */
    private void resetDescTextViewHeight(int linesHeight) {
        ViewGroup.LayoutParams layoutParams = activity_desc.getLayoutParams();
        layoutParams.height = linesHeight;
        activity_desc.setLayoutParams(layoutParams);
    }

    /**
     * 得到描述TextView显示3行的高度
     * @return
     */
    private int getDescTextView3LinesHeight() {
        TextView textView = new TextView(mContext);
        textView.setText("\n\n");
        textView.setTextSize(15);
        textView.setMaxLines(3);
        textView.setPadding(MyConfig.dp2Px(mContext,10),MyConfig.dp2Px(mContext,12),MyConfig.dp2Px(mContext,10),MyConfig.dp2Px(mContext,12));
        // 指定测量规格
        int widthMeasureSpec = MeasureSpec.makeMeasureSpec(activity_desc.getWidth(), MeasureSpec.EXACTLY);
        int heightMeasureSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }
    /**
     * 得到描述TextView的总高度
     * @return
     */
    private int getDescTextViewAllHeight() {
        TextView textView = new TextView(mContext);
        textView.setTextSize(15);
        textView.setText(activity_desc.getText());
        textView.setPadding(MyConfig.dp2Px(mContext,10),MyConfig.dp2Px(mContext,12),MyConfig.dp2Px(mContext,10),MyConfig.dp2Px(mContext,12));
        // 指定测量规格

        int width = ((Activity) mContext).getWindowManager().getDefaultDisplay().getWidth();/**由于activity_desc的高度不一定能获取到，所以使用Window的宽度代替*/
        int widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        int heightMeasureSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }

}
