package co.docy.oldfriends.Views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.docy.oldfriends.DataClass.IMsg;
import co.docy.oldfriends.R;

/**
 * Created by fighting on 2016/6/6.
 */
public class ViewActivityInfoMainroom extends LinearLayout {

    public LinearLayout activity_in_main_layout;
    public TextView activity_in_main_desc;
    public TextView activity_in_main_join_num;

    public ViewActivityInfoMainroom(Context context, IMsg iMsg) {

        super(context);

        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.row_activity_info_mainroom, this, true);

        activity_in_main_desc = (TextView) v.findViewById(R.id.activity_in_main_desc);
        activity_in_main_join_num = (TextView) v.findViewById(R.id.activity_in_main_join_num);
        activity_in_main_layout = (LinearLayout) v.findViewById(R.id.activity_in_main_layout);

        activity_in_main_desc.setText(iMsg.mActivity.title);
        activity_in_main_join_num.setText("已报名"+iMsg.mActivity.joinedNum+"人");


    }

}
