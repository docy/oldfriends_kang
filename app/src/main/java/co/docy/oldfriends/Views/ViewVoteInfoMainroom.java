package co.docy.oldfriends.Views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.IMsg;
import co.docy.oldfriends.R;

/**
 * Created by youhy on 5/14/15.
 */
public class ViewVoteInfoMainroom extends LinearLayout {

    public ImageView vote_in_main_avatar;
    public TextView vote_in_main_text;
    public LinearLayout vote_in_main_layout;

    public ViewVoteInfoMainroom(Context context, IMsg iMsg) {

        super(context);

        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.row_vote_info_mainroom, this, true);

        vote_in_main_layout = (LinearLayout) v.findViewById(R.id.vote_in_main_layout);
        vote_in_main_avatar = (ImageView) v.findViewById(R.id.vote_in_main_avatar);
        vote_in_main_avatar.setImageResource(R.drawable.button_vote_3x_201603);
        vote_in_main_text = (TextView) v.findViewById(R.id.vote_in_main_text);

        StringBuilder sb = new StringBuilder();
        if(iMsg.vote.closed){
            sb.append(context.getString(R.string.vote_ended));
        }else {
            sb.append(context.getString(R.string.vote_going));
        }
        sb.append("\n");
        if(iMsg.vote.single){
            sb.append(context.getString(R.string.vote_with_single_choose));
        }else{
            sb.append(context.getString(R.string.vote_with_multi_choose));
        }
        if(iMsg.vote.anonymous){
            sb.append(context.getString(R.string.vote_anonymous_2));
        }else{
            sb.append(context.getString(R.string.vote_register_2));
        }
        vote_in_main_text.setText(sb.toString());

        MyLog.d("", "httptool vote ViewVoteInMainroom: " + sb.toString());

    }

}
