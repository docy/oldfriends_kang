package co.docy.oldfriends.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.docy.oldfriends.R;

/**
 * Created by youhy on 5/14/15.
 */
public class ViewCalendarTitle extends LinearLayout {

    public Button calendar_title_today;
    public Button calendar_title_previous;
    public Button calendar_title_next;
    public TextView calendar_title_indicator;

    public interface GotoToday{
        void gotoToday();
    }
    public GotoToday gotoToday;
    public interface GotoPrevious {
        void gotoPrevious();
    }
    public GotoPrevious gotoPrevious;
    public interface GotoNext {
        void gotoNext();
    }
    public GotoNext gotoNext;


    public ViewCalendarTitle(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context);
    }

    public ViewCalendarTitle(Context context) {

        super(context);
        inflate(context);

    }

    private void inflate(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.calendar_title, this, true);


        calendar_title_today = (Button)v.findViewById(R.id.calendar_title_today);
        calendar_title_today.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(gotoToday != null){
                    gotoToday.gotoToday();
                }
            }
        });
        calendar_title_previous = (Button)v.findViewById(R.id.calendar_title_previous);
        calendar_title_previous.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(gotoPrevious != null){
                    gotoPrevious.gotoPrevious();
                }
            }
        });
        calendar_title_next = (Button)v.findViewById(R.id.calendar_title_next);
        calendar_title_next.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (gotoNext != null) {
                    gotoNext.gotoNext();
                }
            }
        });

        calendar_title_indicator = (TextView)v.findViewById(R.id.calendar_title_indicator);
    }

    public void setCalendarIndicator(String indicator){

        calendar_title_indicator.setText(indicator);
        calendar_title_today.setText(indicator);

    }


}
