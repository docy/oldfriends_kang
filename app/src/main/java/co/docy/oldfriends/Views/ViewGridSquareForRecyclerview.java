package co.docy.oldfriends.Views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by youhy on 5/14/15.
 */
public class ViewGridSquareForRecyclerview extends RelativeLayout {

    public ViewGridSquareForRecyclerview(Context context) {
        super(context);
    }

    public ViewGridSquareForRecyclerview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ViewGridSquareForRecyclerview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ViewGridSquareForRecyclerview(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Set a square layout.
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

}
