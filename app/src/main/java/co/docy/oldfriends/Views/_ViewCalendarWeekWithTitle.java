package co.docy.oldfriends.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.joda.time.LocalDate;

import co.docy.oldfriends.DataClass.CalendarWeek;
import co.docy.oldfriends.R;

/**
 * Created by youhy on 5/14/15.
 */
public class _ViewCalendarWeekWithTitle extends LinearLayout {

    CalendarWeek calendarWeek;

    public _ViewCalendarDayInWeek calendar_day1;
    public _ViewCalendarDayInWeek calendar_day2;
    public _ViewCalendarDayInWeek calendar_day3;
    public _ViewCalendarDayInWeek calendar_day4;
    public _ViewCalendarDayInWeek calendar_day5;
    public _ViewCalendarDayInWeek calendar_day6;
    public _ViewCalendarDayInWeek calendar_day7;

    public Button calendar_week_today;
    public Button calendar_week_previous;
    public Button calendar_week_next;
    public TextView calendar_week_indicator;

    public interface GotoToday{
        void gotoToday();
    }
    public GotoToday gotoToday;
    public interface GotoPreviousWeek{
        void gotoPreviousWeek();
    }
    public GotoPreviousWeek gotoPreviousWeek;
    public interface GotoNextWeek{
        void gotoNextWeek();
    }
    public GotoNextWeek gotoNextWeek;
    public interface DayOnClickListener{
        void dayOnClicked(LocalDate day); //1~7
    }
    public DayOnClickListener dayOnClickListener;


    public _ViewCalendarWeekWithTitle(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context);
    }

    public _ViewCalendarWeekWithTitle(Context context) {

        super(context);

        inflate(context);

    }

    public class WeekOnClickListener implements View.OnClickListener{

        LocalDate day;

        public WeekOnClickListener(LocalDate day){
            this.day = day;
        }

        @Override
        public void onClick(View view) {
            if(dayOnClickListener != null){
                dayOnClickListener.dayOnClicked(day);
            }
        }


    }


    private void inflate(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.calendar_week_with_title, this, true);

        calendar_day1 = (_ViewCalendarDayInWeek) v.findViewById(R.id.calendar_day1);
        calendar_day2 = (_ViewCalendarDayInWeek) v.findViewById(R.id.calendar_day2);
        calendar_day3 = (_ViewCalendarDayInWeek) v.findViewById(R.id.calendar_day3);
        calendar_day4 = (_ViewCalendarDayInWeek) v.findViewById(R.id.calendar_day4);
        calendar_day5 = (_ViewCalendarDayInWeek) v.findViewById(R.id.calendar_day5);
        calendar_day6 = (_ViewCalendarDayInWeek) v.findViewById(R.id.calendar_day6);
        calendar_day7 = (_ViewCalendarDayInWeek) v.findViewById(R.id.calendar_day7);


        calendar_week_today = (Button)v.findViewById(R.id.calendar_week_today);
        calendar_week_today.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(gotoToday != null){
                    gotoToday.gotoToday();
                }
            }
        });
        calendar_week_previous = (Button)v.findViewById(R.id.calendar_week_previous);
        calendar_week_previous.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(gotoPreviousWeek != null){
                    gotoPreviousWeek.gotoPreviousWeek();
                }
            }
        });
        calendar_week_next = (Button)v.findViewById(R.id.calendar_week_next);
        calendar_week_next.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(gotoNextWeek != null){
                    gotoNextWeek.gotoNextWeek();
                }
            }
        });

        calendar_week_indicator = (TextView)v.findViewById(R.id.calendar_week_indicator);
    }

    private String generateWeekIndicator(CalendarWeek calendarWeek){

        if(calendarWeek == null){
            return null;
        }

        return calendarWeek.calendarDayList.getFirst().day.toString();

    }

    private void setDayOnClickListener() {
        calendar_day1.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(0).day));
        calendar_day2.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(1).day));
        calendar_day3.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(2).day));
        calendar_day4.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(3).day));
        calendar_day5.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(4).day));
        calendar_day6.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(5).day));
        calendar_day7.setOnClickListener(new WeekOnClickListener(calendarWeek.calendarDayList.get(6).day));
    }

    public void setViewCalendarWeek(CalendarWeek calendarWeek){

        this.calendarWeek = calendarWeek;

        calendar_week_indicator.setText("this week Monday is " + generateWeekIndicator(calendarWeek));

        calendar_day1.setViewCalendarDay(calendarWeek.calendarDayList.get(0));
        calendar_day2.setViewCalendarDay(calendarWeek.calendarDayList.get(1));
        calendar_day3.setViewCalendarDay(calendarWeek.calendarDayList.get(2));
        calendar_day4.setViewCalendarDay(calendarWeek.calendarDayList.get(3));
        calendar_day5.setViewCalendarDay(calendarWeek.calendarDayList.get(4));
        calendar_day6.setViewCalendarDay(calendarWeek.calendarDayList.get(5));
        calendar_day7.setViewCalendarDay(calendarWeek.calendarDayList.get(6));

        setDayOnClickListener();

    }

}
