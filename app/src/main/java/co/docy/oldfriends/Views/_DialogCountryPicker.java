package co.docy.oldfriends.Views;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.DataClass.Country;
import co.docy.oldfriends.R;

public class _DialogCountryPicker extends Dialog {

    public interface CountryChoose {
        void choosed(Country country);
    }

    CountryChoose countryChoose;

    Context context;

    public _DialogCountryPicker(Context context) {
        super(context);
        this.context = context;
    }

    public _DialogCountryPicker(Context context, int width, int height, CountryChoose countryChoose) {
        super(context);
        this.context = context;
        this.width = width;
        this.height = height;
        this.countryChoose = countryChoose;
    }

    public int width;
    public int height;

    ListView fragment_page_account_country_listview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_page_account_country);

        setTitle(R.string.regist_country);


        fragment_page_account_country_listview = (ListView) findViewById(R.id.fragment_page_account_country_listview);
//        fragment_page_account_country_listview.getLayoutParams().width = width;
//        fragment_page_account_country_listview.getLayoutParams().height = height;

        fragment_page_account_country_listview.setAdapter(new ListAdapterCountryChoose(context, MyConfig.supportedCountriesList));

        return;
    }


    public class ListAdapterCountryChoose extends BaseAdapter {

        private class ViewHolder {
            ImageView country_avatar;
            TextView country_name;
            TextView country_phone_prefix;
            RelativeLayout country_layout;
        }


        Context context;
        List<Country> list;

        private LayoutInflater inflater = null;

        @Override
        public Country getItem(int position) {
            return list.get(position);
        }

        public ListAdapterCountryChoose(Context context,
                                        List<Country> objects) {
            this.context = context;
            this.list = objects;

            this.inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return list.size();
        }


        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final Country country = getItem(position);

            ViewHolder viewHolder;

            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_country, null);
                viewHolder.country_avatar = (ImageView) convertView.findViewById(R.id.country_flag);
                viewHolder.country_name = (TextView) convertView.findViewById(R.id.country_name);
                viewHolder.country_phone_prefix = (TextView) convertView.findViewById(R.id.country_phone_prefix);
                viewHolder.country_layout = (RelativeLayout) convertView.findViewById(R.id.country_layout);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }


            Picasso.with(context).load(country.flag)
//                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
//                    .transform(new RoundedTransformation(MyConfig.AVATAR_RADIUS, 0))
                    .into(viewHolder.country_avatar);

            viewHolder.country_name.setText(country.name);
            viewHolder.country_phone_prefix.setText(country.phone_prefix);

            viewHolder.country_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (countryChoose != null) {
                        countryChoose.choosed(country);
                    }
                    dismiss();

                }
            });


            return convertView;

        }


    }

}
