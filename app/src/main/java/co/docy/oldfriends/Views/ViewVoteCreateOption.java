package co.docy.oldfriends.Views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.docy.oldfriends.R;

/**
 * Created by youhy on 5/14/15.
 */
public class ViewVoteCreateOption extends LinearLayout {
    /**
     * if var's change involve other var, set it to private to force using setter/getter
     */
    private int num;
    public TextView tv_vote_option_create_num;
    public EditText tv_vote_option_create_et;
    public ImageView create_vote_split_line;

    public ViewVoteCreateOption(Context context, int num) {

        super(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.row_vote_option_create, this, true);

        tv_vote_option_create_num = (TextView) v.findViewById(R.id.create_vote_option_num);
        tv_vote_option_create_et = (EditText) v.findViewById(R.id.create_vote_option_et);
        create_vote_split_line = (ImageView) v.findViewById(R.id.create_vote_split_line);

        setNum(num);
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
        tv_vote_option_create_num.setText("" + (num + 1));
        if (num == 0) {
            create_vote_split_line.setVisibility(GONE);
        }
    }

}
