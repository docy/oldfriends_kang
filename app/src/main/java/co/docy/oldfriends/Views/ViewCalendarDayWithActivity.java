package co.docy.oldfriends.Views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.joda.time.LocalDate;

import co.docy.oldfriends.DataClass.CalendarDay;
import co.docy.oldfriends.R;

/**
 * Created by youhy on 5/14/15.
 */
public class ViewCalendarDayWithActivity extends LinearLayout {

    CalendarDay calendarDay;

    public LinearLayout calendar_day_layout;
    public ImageView calendar_day_have_activity;
    public TextView calendar_day_in_month;


    public ViewCalendarDayWithActivity(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context);
    }

    public ViewCalendarDayWithActivity(Context context) {

        super(context);

        inflate(context);

    }

    private void inflate(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.calendar_day_with_activity, this, true);

        calendar_day_layout = (LinearLayout)v.findViewById(R.id.calendar_day_layout);
        calendar_day_have_activity = (ImageView) v.findViewById(R.id.calendar_day_have_activity);
        calendar_day_in_month = (TextView) v.findViewById(R.id.calendar_day_in_month);
    }

    public void setViewCalendarDay(CalendarDay calendarDay, int highLightMonth){
        this.calendarDay = calendarDay;

        calendar_day_in_month.setText(""+calendarDay.day.getDayOfMonth());

        if(calendarDay.have_activity){
            calendar_day_have_activity.setVisibility(VISIBLE);
        }else{
            calendar_day_have_activity.setVisibility(INVISIBLE);
        }

        if(calendarDay.day.equals(new LocalDate())){
//            calendar_day_in_month.setBackgroundColor(getResources().getColor(R.color.Red));
            calendar_day_in_month.setBackground(getResources().getDrawable(R.drawable.text_background_circle));
        }else{
            calendar_day_in_month.setBackgroundColor(Color.TRANSPARENT);
        }

        if(calendarDay.day.getMonthOfYear() == highLightMonth){
//            calendar_day_layout.setBackgroundColor(getResources().getColor(R.color.LightGrey));
            calendar_day_in_month.setTextColor(getResources().getColor(R.color.White));
        }else{
//            calendar_day_layout.setBackgroundColor(Color.TRANSPARENT);
            calendar_day_in_month.setTextColor(getResources().getColor(R.color.c_no_mouth_text_color));
        }


    }


}
