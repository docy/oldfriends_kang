package co.docy.oldfriends.Service;


import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;

import com.github.nkzawa.emitter.Emitter;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.EventBus.EventBeInvitedInGroup;
import co.docy.oldfriends.EventBus.EventContactDownloadList;
import co.docy.oldfriends.EventBus.EventDisconnectSocket;
import co.docy.oldfriends.EventBus.EventGroupListUpdateFromServer;
import co.docy.oldfriends.EventBus.EventHeart200ms;
import co.docy.oldfriends.EventBus.EventHeartOneSecond;
import co.docy.oldfriends.EventBus.EventKick;
import co.docy.oldfriends.EventBus.EventNetConnectFail;
import co.docy.oldfriends.EventBus.EventNetConnectStatusChanged;
import co.docy.oldfriends.EventBus.EventReConnectSocket;
import co.docy.oldfriends.EventBus.EventScreenSwitch;
import co.docy.oldfriends.EventBus.EventSetSoundRemind;
import co.docy.oldfriends.EventBus.EventSwitchSocketServer;
import co.docy.oldfriends.Messages.JsonDeviceSetNotificationLevel;
import co.docy.oldfriends.Messages.JsonDeviceSetNotificationLevelRet;
import co.docy.oldfriends.Messages.JsonFriendCricleSocketRet;
import co.docy.oldfriends.Messages.JsonGetCompanyUserListRet;
import co.docy.oldfriends.Messages.JsonGetEventRet;
import co.docy.oldfriends.Messages.JsonGetMsgRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicCommentRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicRet;
import co.docy.oldfriends.Messages.JsonGroupListUpdatedMsgFromSocket;
import co.docy.oldfriends.Messages.JsonNotificationMsgFromSocket;
import co.docy.oldfriends.Messages.JsonSoundMsg;
import co.docy.oldfriends.Messages.Json_DirectRoom_GetTopicImageRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.NetTools.SocketTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;

public class MyService extends Service {

    int second_count;

    /**控制心跳的变量，当服务开启时置为true，当服务停止时置为false*/
    boolean heartIsRunning = true;

    private final IBinder mBinder = new LocalBinder();

    JsonGetEventRet.GetEventRet getEventRet;
    JsonSoundMsg jsonSoundMsg = new JsonSoundMsg();
    JsonGetMsgRet.GetMsgRet json_socket_msg = new JsonGetMsgRet.GetMsgRet();
    JsonGetMsgTopicRet.GetMsgTopicRet json_socket_topic = new JsonGetMsgTopicRet.GetMsgTopicRet();
    JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet json_socket_topicComment = new JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet();
    Json_DirectRoom_GetTopicImageRet.DirectRoom_GetTopicImageRet json_socket_directroom_topic = new Json_DirectRoom_GetTopicImageRet.DirectRoom_GetTopicImageRet();
    JsonNotificationMsgFromSocket jsonNotificationMsgFromSocket;
    JsonGroupListUpdatedMsgFromSocket jsonGroupListUpdatedMsgFromSocket;
    JsonFriendCricleSocketRet jsonFriendCricleSocketRet =new JsonFriendCricleSocketRet();


    public Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            MyLog.d("", "socketio switch -- onNewMessage.call");

            JSONObject data = (JSONObject) args[0];
            dispatchJsonObject(data);

        }
    };


    public void dispatchJsonObject(JSONObject data) {

        usingGson(data);

    }


    public void usingGson(JSONObject data) {


        Gson gson = new Gson();

        try {
            int type = data.getInt("type");
            MyLog.d("", "socketio in service -- type: " + type +" string: " + data.toString());



            if(type == MyConfig.TYPE_GROUP_LIST_UPDATED){
                MyLog.d("", "socketio in service -- type: TYPE_GROUP_LIST_UPDATED! " + type +" string: " + data.toString());
                jsonGroupListUpdatedMsgFromSocket = gson.fromJson(data.toString(), JsonGroupListUpdatedMsgFromSocket.class);
                EventBus.getDefault().post(new EventGroupListUpdateFromServer());//通知小组列表有变化

            }else if(type == MyConfig.TYPE_NOTIFICATION){
                MyLog.d("", "socketio in service -- type: TYPE_NOTIFICATION! " + type +" string: " + data.toString());
                jsonNotificationMsgFromSocket = gson.fromJson(data.toString(), JsonNotificationMsgFromSocket.class);
                EventBus.getDefault().post(jsonNotificationMsgFromSocket);

            }else if (type == MyConfig.TYPE_TOPIC_IMAGE_DIRECTROOM || type == MyConfig.TYPE_TOPIC_MAP_DIRECTROOM) {
//                MyLog.d("", "socketio in service: socketio event type: " + type);
                json_socket_directroom_topic = gson.fromJson(data.toString(), Json_DirectRoom_GetTopicImageRet.DirectRoom_GetTopicImageRet.class);
                EventBus.getDefault().post(json_socket_directroom_topic);

            } else if (type == MyConfig.TYPE_SOUND) {
                MyLog.d("", "sound from socket");
                jsonSoundMsg = gson.fromJson(data.toString(), JsonSoundMsg.class);
                EventBus.getDefault().post(jsonSoundMsg);
            } else if (type == MyConfig.TYPE_MSG) {//message
//                MyLog.d("", "socketio in service: socketio event type: " + type);

                json_socket_msg = gson.fromJson(data.toString(), JsonGetMsgRet.GetMsgRet.class);
                EventBus.getDefault().post(json_socket_msg);

            } else if (type == MyConfig.TYPE_TOPIC) {//subject, or say topic
//                MyLog.d("", "socketio in service: socketio event type: " + type);

                json_socket_topic = gson.fromJson(data.toString(), JsonGetMsgTopicRet.GetMsgTopicRet.class);
                EventBus.getDefault().post(json_socket_topic);

            } else if (type == MyConfig.TYPE_TOPIC_COMMANT) {//comment
//                MyLog.d("", "socketio in service: socketio event type: " + type);
                json_socket_topicComment = gson.fromJson(data.toString(), JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet.class);
                EventBus.getDefault().post(json_socket_topicComment);

            } else if (type == MyConfig.TYPE_CREATE) {//message
//                MyLog.d("", "socketio in service: socketio event type: " + type);

                getEventRet = gson.fromJson(data.toString(), JsonGetEventRet.GetEventRet.class);
                getEventRet.info.message = getString(R.string.maingroup_create);
                EventBus.getDefault().post(getEventRet);

            } else if (type == MyConfig.TYPE_JOIN) {//message
//                MyLog.d("", "socketio in service: socketio event type: " + type);

                getEventRet = gson.fromJson(data.toString(), JsonGetEventRet.GetEventRet.class);
                getEventRet.info.message = getString(R.string.maingroup_join);
                EventBus.getDefault().post(getEventRet);
                if (getEventRet.userId == MyConfig.usr_id) {
                    EventBus.getDefault().post(new EventBeInvitedInGroup());
                }

            } else if (type == MyConfig.TYPE_LEAVE) {//message
//                MyLog.d("", "socketio in service: socketio event type: " + type);

                getEventRet = gson.fromJson(data.toString(), JsonGetEventRet.GetEventRet.class);
                getEventRet.info.message = getString(R.string.maingroup_leave);
                EventBus.getDefault().post(getEventRet);
            } else if (type == MyConfig.TYPE_KICK) {//message
//                MyLog.d("", "socketio in service: socketio event type: " + type);
                getEventRet = gson.fromJson(data.toString(), JsonGetEventRet.GetEventRet.class);
                getEventRet.info.message = getResources().getString(R.string.maingroup_kick);
                EventBus.getDefault().post(getEventRet);
                if (getEventRet.userId == MyConfig.usr_id) {
                    EventBus.getDefault().post(new EventKick(getEventRet));
                }

            }else if(type == MyConfig.TYPE_FRIEND_CRICLE){
                MyLog.d("kanghongpu", "朋友圈socket" + data.toString());
                jsonFriendCricleSocketRet = gson.fromJson(data.toString(),JsonFriendCricleSocketRet.class);
                EventBus.getDefault().post(jsonFriendCricleSocketRet);

            }
            else {
//                MyLog.d("", "socketio in service: socketio event type: " + type);

            }
        } catch (JSONException e) {

//            MyLog.d("", "socketio in service: socketio event exception: " + e.toString());
            return;
        }
    }


    private void socketDisconnectThread() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                socketDisconnect();
            }
        }).start();
    }

    private boolean socketDisconnect() {

        for (int i = 0; i < 50; i++) {

            if (!SocketTools.connected()) {
                MyLog.d("", "service heart !socketTools.connected()");

                /**
                 * 已经断开
                 */
                MyConfig.rtm_status = 0;
                return true;
            } else {
                SocketTools.disconnect();
            }

            try {
                Thread.sleep(50);
            } catch (Exception e) {

            }
        }

        /**
         * 没断开，是哪里错了
         */
        MyLog.d("", "socketio connection disconnect failed !!");
        return false;
    }

    private void socketConnectThread() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                socketConnect();
            }
        }).start();
    }

    private boolean socketConnect() {
        /**
         * 开始连接socket
         */
        for (int i = 0; i < 50; i++) {

            if (SocketTools.connected()) {
                MyConfig.rtm_status = 1;//已经连接，但未认证
                MyLog.d("", "socketio in service: connected return true");

                return true;
            } else {
                SocketTools.connect();
            }

            try {
                Thread.sleep(50);
            } catch (Exception e) {

            }
        }

        /**
         * 没连上，是哪里错了
         */
        MyLog.d("", "socketio in service: connection connect failed !!");
        return false;

    }


    private void socketReConnectThread() {

        /**
         * 先断开再重连
         * 注意本函数不可少
         * 因为在切换当前公司时需要有一个先断开再重连socket的动作，使得服务器能够更新用户的群组
         */
        new Thread(new Runnable() {
            @Override
            public void run() {
                socketReConnect();
            }
        }).start();
    }

    private void socketReConnect() {
        socketDisconnect();
        socketConnect();
    }


    private void socketAddListener() {
        SocketTools.addListener();
    }

    private void socketAuthenticate() {
        SocketTools.authenticate();
    }

    private boolean socketConnected() {
        return SocketTools.connected();
    }

    private boolean socketHasListener() {
        return SocketTools.hasListener();
    }

    public MyService() {

    }


    public class LocalBinder extends Binder {
        public MyService getService() {
            // Return this instance of LocalService so clients can call public
            // methods
            return MyService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    int timer_200ms_opened = 0;

    public void timer200msOpen(int b) {
        timer_200ms_opened = b;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        MyLog.d("", "service: onCreate()");

        // 监控锁屏的receiver
        final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        MyConfig.screenOnReceiver = new ScreenOnReceiver();
        registerReceiver(MyConfig.screenOnReceiver, filter);


        EventBus.getDefault().register(this);

        MyConfig.rtm_listener = onNewMessage;

        SocketTools.init();

        /**
         * heart for debug
         */
        new Thread(new Runnable() {
            @Override
            public void run() {



                while (heartIsRunning) {
                    try {
//                        Thread.sleep(1000);
//                        EventBus.getDefault().post(new EventHeartOneSecond());


                        for (int timer_count = 0; timer_count < 5; timer_count++) {
                            Thread.sleep(200);
                            if (timer_200ms_opened > 0) {
                                EventBus.getDefault().post(new EventHeart200ms());
                            }
                        }

//                        Thread.sleep(1000);
                        EventBus.getDefault().post(new EventHeartOneSecond(second_count++));

//                        MyLog.d("", "socketio in service: service heart");

                        /**
                         * 必须在这个心跳里面监测网络连接
                         * 只计算两种状态：大于等于1，小于1
                         */

                        int net_status_last = MyConfig.net_status;
                        MyConfig.netConnectCheck();

                        if (net_status_last >= 1 && MyConfig.net_status >= 1) {
                            //持续联网
                            //暂不考虑极端情况，比如这两秒内有断网
                            normalProcessWithNetwork();
//                            MyLog.d("", "socketio in service: with connection");
                        } else if (net_status_last < 1 && MyConfig.net_status >= 1) {
                            //恢复联网
                            EventBus.getDefault().post(new EventNetConnectStatusChanged());

//                            MyLog.d("", "socketio in service: get connection");
                            //在socket上发一条disconnect操作，清除服务器的retry操作
                            //先连上，再做这个断开操作，确保服务器能够收到
                            socketConnect();
                            socketDisconnect();
                        } else if (net_status_last < 1 && MyConfig.net_status < 1) {
                            //持续断网
//                            MyLog.d("", "socketio in service: no connection");

                        } else if (net_status_last >= 1 && MyConfig.net_status < 1) {
                            //断网
//                            MyLog.d("", "socketio in service: lose connection");
                            EventBus.getDefault().post(new EventNetConnectFail());

                        }


                    } catch (Exception e) {
                        MyLog.d("", "socketio in service: exception " + e.toString());

                    }
                }

            }
        }).start();


    }

    private void normalProcessWithNetwork() {

        if (MyConfig.usr_status < MyConfig.USR_STATUS_LOGIN) {

            /**
             * 未登入，断开socket
             */

            socketDisconnect();

        } else {

            /**
             * 在用户登入后
             *
             *  如果非锁屏，检查并保持socket
             *  如果锁屏，断开socket，使得服务器推送消息
             *  是否推送，取决于是否锁屏，而不是app在前台还是后台
             *
             */

            if (MyConfig.isScreenOn) {
                MyLog.d("", "notification push: MyConfig.isScreenOn true");

                if (socketConnected()) {
//                    MyLog.d("", "socketio in service: socket connected");
                    if (MyConfig.rtm_status < 2) {
//                        MyLog.d("", "socketio in service: before authenticate");
                        socketAuthenticate();
                    } else {
                        if (socketHasListener()) {
//                            MyLog.d("", "socketio in service: normal loop");

                        } else {
//                            MyLog.d("", "socketio in service: before add listener");
                            socketAddListener();
                        }
                    }
                } else {
//                    MyLog.d("", "socketio in service: before socket connected");
                    socketConnect();
                }

            } else {
                MyLog.d("", "notification push: MyConfig.isScreenOn false");

                socketDisconnect();

            }

        }
    }

    private boolean ifAppRunningInFront() {

        /**
         * 已经登录了
         *
         * 要即时刷新两类数据
         *  group信息，包括最后发送的消息
         *  socket信息
         *
         *  刷新条件
         *      本app在前台运行
         *
         */

        String currentTopRunningAppName = MyConfig.getTopAppPackageName();
        if (currentTopRunningAppName.startsWith(MyConfig.packageName)) {
//            MyLog.d("", "socketio in service: ifAppRunningInFront return true");
            return true;
        }

//        MyLog.d("", "socketio in service: ifAppRunningInFront return false");
        return false;
    }


    @Override
    public void onDestroy() {
        heartIsRunning = false;
        MyLog.d("", "service: onDestroy()");

        // 锁屏监控
        if (MyConfig.screenOnReceiver != null) {
            unregisterReceiver(MyConfig.screenOnReceiver);
            MyConfig.screenOnReceiver = null;
        }

        EventBus.getDefault().unregister(this);

        try {
//            SocketTools socketTools = SocketTools.getInstance();

            if (SocketTools.connected()) {
                socketDisconnectThread();
            }

        } catch (Exception e) {

        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }


    public void onEventMainThread(EventDisconnectSocket eventDisconnectSocket) {
        socketDisconnectThread();
    }


    public void onEventMainThread(EventSwitchSocketServer eventSwitchSocketServer) {
        MyLog.d("", "socketio switch: EventSwitchSocketServer in MyService.java");
        new Thread(new Runnable() {
            @Override
            public void run() {
//                socketDisconnect();
                SocketTools.init();
            }
        }).start();
    }


    public void onEventMainThread(EventReConnectSocket eventReConnectSocket) {
        MyLog.d("", "eventbus: socketReConnectThread() in MyService.java");
        socketReConnectThread();
    }


    public void onEventMainThread(EventHeartOneSecond eventHeartOneSecond) {

        /**
         * 需要service定期完成的事务都放在这里
         */


        /**
         * app版本更新检测
         */
        if (eventHeartOneSecond.heart_second_count % 60 * 10 == 0) {
//        if(eventHeartOneSecond.heart_second_count % 5  == 0){
            MyConfig.appVersionCheck();
        }


    }

    public void onEventMainThread(EventHeart200ms eventHeart200ms) {

    }

    public void onEventMainThread(final EventScreenSwitch eventScreenSwitch) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                JsonDeviceSetNotificationLevel jsonDeviceSetNotificationLevel = new JsonDeviceSetNotificationLevel();
                jsonDeviceSetNotificationLevel.accessToken = MyConfig.usr_token;
                jsonDeviceSetNotificationLevel.os = MyConfig.os_string;
                jsonDeviceSetNotificationLevel.pushToken = MyConfig.jpush_regist_id;
                jsonDeviceSetNotificationLevel.uuid = MyConfig.uuid;
                if (eventScreenSwitch.isScreenOn) {
                    jsonDeviceSetNotificationLevel.level = 0;
                } else {
                    jsonDeviceSetNotificationLevel.level = 1;
                }
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonDeviceSetNotificationLevel, JsonDeviceSetNotificationLevel.class);
                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_DEVICE_SET_NOTIFICATION_LEVEL, jsonStr);
                final JsonDeviceSetNotificationLevelRet jsonDeviceSetNotificationLevelRet = gson.fromJson(s, JsonDeviceSetNotificationLevelRet.class);
                MyLog.d("", "HttpTools: " + MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_DEVICE_SET_NOTIFICATION_LEVEL + " jsonDeviceSetNotificationLevelRet " + s + eventScreenSwitch.isScreenOn);
                if (jsonDeviceSetNotificationLevelRet != null && jsonDeviceSetNotificationLevelRet.code == MyConfig.retSuccess()) {

                }

            }
        }).start();
    }

    public void onEventMainThread(EventSetSoundRemind eventSetSoundRemind) {
        if (!eventSetSoundRemind.isSend) {
            timer_200ms_opened--;
        } else {
            timer_200ms_opened++;
        }
    }


    public void onEventMainThread(final EventContactDownloadList eventContactDownloadList) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java");

        if(eventContactDownloadList.flag == EventContactDownloadList.DOWNLOAD_BEGIN) {

            new Thread(new Runnable() {
                @Override
                public void run() {

                    MyLog.d("", "insertOneContact: begin");
                    MyConfig.count_insert_contact = 0;

                    eventContactDownloadList.failed = 0;
                    eventContactDownloadList.successed = 0;

                    for (JsonGetCompanyUserListRet.GetCompanyUserListRet getCompanyUserListRet : eventContactDownloadList.data) {

                        if (0 == MyConfig.insertOneContact(ContactUniversal.fromJsonGetCompanyUserListRet(getCompanyUserListRet))) {
                            eventContactDownloadList.successed++;
                        }else{
                            eventContactDownloadList.failed++;
                        }

                    }

                    MyLog.d("", "insertOneContact: end");

                    eventContactDownloadList.flag = EventContactDownloadList.DOWNLOAD_END;

                    EventBus.getDefault().post(eventContactDownloadList);

                }
            }).start();

        }

    }


}
