package co.docy.oldfriends.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventScreenSwitch;
import de.greenrobot.event.EventBus;

public class ScreenOnReceiver extends BroadcastReceiver {

    public ScreenOnReceiver() {
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {

        /**
         * 如果没有登录，似乎就没必要通知了？20160414
         */
        if(MyConfig.usr_status < MyConfig.USR_STATUS_LOGIN){
            return;
        }

        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            // do whatever you need to do here
            MyConfig.isScreenOn = false;
            EventBus.getDefault().post(new EventScreenSwitch(false));
            MyLog.d("","screen is off");

        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            // and do whatever you need to do here
            MyConfig.isScreenOn = true;
            EventBus.getDefault().post(new EventScreenSwitch(true));
            MyLog.d("","screen is on");
        }
    }

}
