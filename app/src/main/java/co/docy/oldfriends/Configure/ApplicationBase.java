package co.docy.oldfriends.Configure;

import android.app.Application;
import android.content.Context;
import android.provider.Settings;
import android.support.multidex.MultiDex;

import org.acra.*;
import org.acra.annotation.*;
import org.acra.ReportField;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import cn.jpush.android.api.JPushInterface;
import co.docy.oldfriends.BuildConfig;
import co.docy.oldfriends.DataClass.Country;
import co.docy.oldfriends.R;

/**
 * Created by youhy on 10/15/15.
 */

@ReportsCrashes(
        formUri = ""+ MyConfig.API_DOMAIN_CRASH_SERVER + MyConfig.API_DOMAIN_CRASH_LOG,
        mode = ReportingInteractionMode.TOAST,
        customReportContent = {
                ReportField.REPORT_ID,
                ReportField.USER_CRASH_DATE,
                ReportField.USER_IP,
                ReportField.BRAND,
                ReportField.ANDROID_VERSION,
                ReportField.DISPLAY,
                ReportField.APPLICATION_LOG,
                ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.STACK_TRACE,
                ReportField.CUSTOM_DATA
        },
        reportType = org.acra.sender.HttpSender.Type.JSON,
        resToastText = R.string.crash_toast_text
)

public class ApplicationBase extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        MyConfig.uuid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        //===== init app data ======

        MyConfig.app = getApplicationContext();
        MyConfig.cacheDir = getCacheDir().toString();

        // The following line triggers the initialization of ACRA
        ACRA.init(this);

        //===== init user data ======

        MyConfig.resetAndSaveDebugSettingToPreference(this);

        initAssets();
        MyConfig.getErrorCodeMessage();

        //===== init location and language ======
        initLocation();

        MyLog.d("", "appVersion_from_local.versionName ApplicationBase");

//        MyConfig.switchSocketServer(MyConfig.SOCKET_SERVER_2);
//        MyLog.d("", "server ip:" + MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_DOMAIN_WITH_SLASH);


        //===== 极光推送 ======

        JPushInterface.init(this);
        JPushInterface.setDebugMode(true);
        MyLog.d("", "[JPushReceiver] init in ApplicationBase");

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void initLocation() {
        /**
         * 关于国家和语言
         *
         *  国家和语言应该分开，是两套体系
         *
         */
//        MyConfig.getAllCountries(this);
        MyConfig.getSupportedCountries(this);
        MyConfig.app_country = new Country("CN","China", "+86");
        MyConfig.app_country.flag = R.drawable.country_flag_cn;
    }

    public void initAssets(){
        MyConfig.citylist = MyConfig.app.getResources().getStringArray(R.array.city_list);
        MyConfig.categorylist = MyConfig.app.getResources().getStringArray(R.array.category_list);
    }


}
