package co.docy.oldfriends.Configure;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


//import android.widget.Toast;


/**
 * http://stackoverflow.com/questions/2018263/android-logging
 * 
 * improved by Max You
 *
 */
public class MyLog {

	public static final String LIFECYCLE = "lifecycle";
	
	
	public static int LEVEL = android.util.Log.DEBUG;

	/**
	 *	switch:
	 *		outputSwitch = 0
	 *			no output
	 * 		outputSwitch = 1
	 * 			admit output by tagFilter
	 * 		outputSwitch = 2
	 * 			ban output by tagFilter
	 * 		outputSwitch = 3
	 * 			output all
	 */
	public static int outputSwitch = 3;
	public static String tagFilter = LIFECYCLE;
	public static int log2FileSwitch = 0;

	/**
	 * 
	 */
	public static final String logFileName = "my_log";
	public static File logFile = null;

	static public void log2File(String log) {

		logFile = new File(MyConfig.appCacheDirPath,
				logFileName);

		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(logFile,
					true));
			bw.write(log + "\n");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	static public void d(String tag, String msgFormat, Object... args) {

		if (MyConfig.compile_type_release){
			return;
		}

		if(outputSwitch == 0)
			return;
		
		if(outputSwitch == 1){//admit
			if(!tagFilter.equals(tag)){
				return;
			}
		}
		if(outputSwitch == 2){//ban
			if(tagFilter.equals(tag)){
				return;
			}
		}
		
		if (LEVEL <= android.util.Log.DEBUG) {
			android.util.Log.d(tag, String.format(msgFormat, args));
			
			if(log2FileSwitch == 1){
				log2File(tag +": " + String.format(msgFormat, args));
			}			
		}
	}

	static public void d(String tag, String msg) {

		if(msg == null){
			return;
		}

		if(MyConfig.compile_type_release){
			return;
		}

		if(outputSwitch == 0)
			return;

		if(outputSwitch == 1){//admit
			if(!tagFilter.equals(tag)){
				return;
			}
		}
		if(outputSwitch == 2){//ban
			if(tagFilter.equals(tag)){
				return;
			}
		}

		if (LEVEL <= android.util.Log.DEBUG) {
			android.util.Log.d(tag, msg);

			if(log2FileSwitch == 1){
				log2File(tag +": " + msg);
			}
		}
	}

//	static public void d(String tag, Throwable t, String msgFormat,
//			Object... args) {
//		
//		if(output != 1)
//			return;
//		
//		if (LEVEL <= android.util.Log.DEBUG) {
//			android.util.Log.d(tag, String.format(msgFormat, args), t);
//		}
//	}

}
