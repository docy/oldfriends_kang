package co.docy.oldfriends.Configure;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.github.nkzawa.emitter.Emitter;
import com.google.gson.Gson;

import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.MediaType;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXVideoObject;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners;
import com.umeng.socialize.exception.SocializeException;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMVideo;
import com.umeng.socialize.weixin.controller.UMWXHandler;
import com.umeng.socialize.weixin.media.CircleShareContent;
import com.umeng.socialize.weixin.media.WeiXinShareContent;

import org.acra.ACRA;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Collator;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import co.docy.oldfriends.Activitis._ActivityCreateCompany;
import co.docy.oldfriends.Activitis.ActivityCreateGroup;
import co.docy.oldfriends.Activitis.ActivityDirectChatGroup;
import co.docy.oldfriends.Activitis.ActivityGallery;
import co.docy.oldfriends.Activitis._ActivityInputCompanyInviteCode;
import co.docy.oldfriends.Activitis.ActivityLocationView;
import co.docy.oldfriends.Activitis._ActivityLogin;
import co.docy.oldfriends.Activitis._ActivityAccount;
import co.docy.oldfriends.Activitis.ActivityLoginTongChuang;
import co.docy.oldfriends.Activitis.ActivityMainGroup;
import co.docy.oldfriends.Activitis.ActivityMyLocation;
import co.docy.oldfriends.Activitis.ActivityPegSomeBody;
import co.docy.oldfriends.Activitis.ActivityStartPage;
import co.docy.oldfriends.Activitis.ActivitySubGroup;
import co.docy.oldfriends.Activitis.ActivityUserInfo;
import co.docy.oldfriends.Activitis.ActivityViewPager;
import co.docy.oldfriends.Adapters.ListAdapterMsg;
import co.docy.oldfriends.BuildConfig;
import co.docy.oldfriends.DataClass.AppVersion;
import co.docy.oldfriends.DataClass.CalendarMonth;
import co.docy.oldfriends.DataClass.CalendarWeek;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.DataClass.Country;
import co.docy.oldfriends.DataClass.DayActivity;
import co.docy.oldfriends.DataClass.DayCourse;
import co.docy.oldfriends.DataClass.DaySchedule;
import co.docy.oldfriends.DataClass.FriendCricleMsg;
import co.docy.oldfriends.DataClass.MActivity;
import co.docy.oldfriends.DataClass.MapLocation;
import co.docy.oldfriends.DataClass.ScheduleNotification;
import co.docy.oldfriends.DataClass.Vote;
import co.docy.oldfriends.EventBus.EventActivityRecreate;
import co.docy.oldfriends.EventBus.EventAppQuit;
import co.docy.oldfriends.EventBus.EventAppVersionCheckInfo;
import co.docy.oldfriends.EventBus.EventCalendarCourseUpdate;
import co.docy.oldfriends.EventBus.EventCompanyJoined;
import co.docy.oldfriends.EventBus.EventCompanyListUpdateUi;
import co.docy.oldfriends.EventBus.EventFavoriteChanged;
import co.docy.oldfriends.EventBus.EventFileDownloadProgress;
import co.docy.oldfriends.EventBus.EventGroupListUpdateFromServer;
import co.docy.oldfriends.EventBus.EventHaveGetCurrentCompany;
import co.docy.oldfriends.EventBus.EventHaveSetCurrentCompany;
import co.docy.oldfriends.EventBus.EventLogout;
import co.docy.oldfriends.EventBus.EventReConnectSocket;
import co.docy.oldfriends.EventBus.EventSingleMsgDelete;
import co.docy.oldfriends.EventBus.EventTopicDelete;
import co.docy.oldfriends.EventBus.EventUserStatusUpdate;
import co.docy.oldfriends.DataClass.IMsg;
import co.docy.oldfriends.Messages.JsonAddFavoriteRet;
import co.docy.oldfriends.Messages.JsonAddFavority;
import co.docy.oldfriends.Messages.JsonCompanyCanJoinRet;
import co.docy.oldfriends.Messages.JsonCreateAulmCardRet;
import co.docy.oldfriends.Messages.JsonFriendCricleRet;
import co.docy.oldfriends.Messages.JsonCurrentYearClassRet;
import co.docy.oldfriends.Messages.JsonDelTopic;
import co.docy.oldfriends.Messages.JsonDelTopicRet;
import co.docy.oldfriends.Messages.JsonDeleteGroup;
import co.docy.oldfriends.Messages.JsonDeleteGroupRet;
import co.docy.oldfriends.Messages.JsonDeleteMsgRet;
import co.docy.oldfriends.Messages.JsonGetAppDownloadUrlRet;
import co.docy.oldfriends.Messages.JsonGetClassDetailRet;
import co.docy.oldfriends.Messages.JsonGetCompanyInfoRet;
import co.docy.oldfriends.Messages.JsonGetCompanyUserList;
import co.docy.oldfriends.Messages.JsonGetCompanyUserListRet;
import co.docy.oldfriends.Messages.JsonGetCoursesBySlotRet;
import co.docy.oldfriends.Messages.JsonGetCurrentCompanyRet;
import co.docy.oldfriends.Messages.JsonGetEventRet;
import co.docy.oldfriends.Messages.JsonGetGroupAlbumRet;
import co.docy.oldfriends.Messages.JsonGetGroupInfoRet;
import co.docy.oldfriends.Messages.JsonGetGroupUserRet;
import co.docy.oldfriends.Messages.JsonGetMsgRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicCommentRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicRet;
import co.docy.oldfriends.Messages.JsonGetMyPrivilege;
import co.docy.oldfriends.Messages.JsonGetPerDayCourseRet;
import co.docy.oldfriends.Messages.JsonGetSubjectListRet;
import co.docy.oldfriends.Messages.JsonGetTopicDetailRet;
import co.docy.oldfriends.Messages.JsonGetVersionsRet;
import co.docy.oldfriends.Messages.JsonGroupCanJoinRet;
import co.docy.oldfriends.Messages.JsonHttpRet_Universal;
import co.docy.oldfriends.Messages.JsonJoinCompany;
import co.docy.oldfriends.Messages.JsonJoinCompanyRet;
import co.docy.oldfriends.Messages.JsonJoinCompanyWithInviteCode;
import co.docy.oldfriends.Messages.JsonJoinCompanyWithInviteCodeRet;
import co.docy.oldfriends.Messages.JsonLeaveGroup;
import co.docy.oldfriends.Messages.JsonLeaveGroupRet;
import co.docy.oldfriends.Messages.JsonLoginUseWeixinData;
import co.docy.oldfriends.Messages.JsonMadeInOrderFromServiceRet;
import co.docy.oldfriends.Messages.JsonMarkAudioReadedRet;
import co.docy.oldfriends.Messages.JsonNewsGroupRet;
import co.docy.oldfriends.Messages.JsonSchoolContactEveryClass;
import co.docy.oldfriends.Messages.JsonSchoolContactRet;
import co.docy.oldfriends.Messages.JsonSetAvatar;
import co.docy.oldfriends.Messages.JsonSetAvatarRet;
import co.docy.oldfriends.Messages.JsonSetCurrentCompany;
import co.docy.oldfriends.Messages.JsonSetCurrentCompanyRet;
import co.docy.oldfriends.Messages.JsonSetPushToken;
import co.docy.oldfriends.Messages.JsonSetPushTokenRet;
import co.docy.oldfriends.Messages.JsonSmsValidCodeUpdatePhone;
import co.docy.oldfriends.Messages.JsonSmsValidCodeUpdatePhoneRet;
import co.docy.oldfriends.Messages.JsonSoundMsg;
import co.docy.oldfriends.Messages.JsonTransferGroupOwner;
import co.docy.oldfriends.Messages.JsonTransferGroupOwnerRet;
import co.docy.oldfriends.Messages.JsonUpdatePersonInfo;
import co.docy.oldfriends.Messages.JsonUpdatePersonInfoRet;
import co.docy.oldfriends.Messages.JsonUploadFileRet;
import co.docy.oldfriends.Messages.JsonUserCompaniesRet;
import co.docy.oldfriends.Messages.JsonUserGroupsRet;
import co.docy.oldfriends.Messages.JsonWeixinLoginRet;
import co.docy.oldfriends.Messages.Json_DirectRoom_GetTopicImageRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Service.ScreenOnReceiver;
import co.docy.oldfriends.emoji.EmojiUtils;
import de.greenrobot.event.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public class MyConfig {

    /**
     * system setting
     */
    public final static boolean compile_type_release = !BuildConfig.DEBUG;

    public static String uuid;//实际上填入的是android_id
    public static String os_string = "android";
    public static int os_int = 2;//2: android


    public static Context app;
    public static String packageName;
    public static ActivityViewPager activityViewPager;//注意这是主activity，可能在整个app中用到
    public static ActivityMainGroup activityMainGroup;
    public static ActivityGallery activityGallery;
    public static ActivityMyLocation activityMyLocation;
    public static ActivityLocationView activityLocationView;
    public static _ActivityLogin activityLogin;
    public static ActivityLoginTongChuang activityLoginTongChuang;
    public static _ActivityAccount activityAccount;
    public static ActivityCreateGroup activityCreateGroup;
    public static _ActivityCreateCompany activityCreateCompany;
    public static ActivityDirectChatGroup activityDirectChatGroup;
    public static _ActivityInputCompanyInviteCode activityInputCompanyInviteCode;

    public static String cacheDir;

    public static final String pathRoot = Environment
            .getExternalStorageDirectory().getPath();
    public static final String appDirName = "TongChuang";
    public static String appDirPath = null;
    public static final String attachedDirName = "Attached";
    public static String appCacheDirPath;


    public static WindowManager wm;
    public static int screenWidth;
    public static JsonGetPerDayCourseRet.PerDayCourseInfo tempPerDayCourseInfo;

    public static int getWidthByScreenPercent(int percent) {
        return MyConfig.screenWidth * percent / 100;
    }
    public static int getHeightByScreenPercent(int percent) {
        return MyConfig.screenHeight * percent / 100;
    }


    public static int screenHeight;
    public static float scale;
    public static int maxSizeInRoomImage = 360;


    public static boolean have_company_view = false;

    public static int dp2Px(Context context,float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f); //from http://developer.android.com/guide/practices/screens_support.html#screen-independence
//        return (int) Math.round(scale * dp);
    }

    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static int getPxFromDimen(Context context, int dimen_id) {
        return context.getResources().getDimensionPixelSize(dimen_id);
    }


    /**
     * 语言、国别
     */

    public static Country app_country;
    public final static int APP_LANG_CN = 0;
    public final static int APP_LANG_EN = 1;
    public static int app_lang = 0;
    public final static String PREFERENCE_APP_LANG = "app_lang";

    public static void switchAppLang(Context context, int lang) {

        setAppLang(context, lang);
        app_lang = lang;
        saveIntToPreference(context, PREFERENCE_APP_LANG, lang);

    }

    public static void loadAppLang(Context context) {

        int app_lang_preference = loadIntFromPreference(context, PREFERENCE_APP_LANG);

        if (app_lang_preference >= 0) { //如果preference里面有设置，就使用这个设置

            app_lang = app_lang_preference;
            setAppLang(context, app_lang_preference);

        } else {//如果preference里面没有设置，那么就用系统当前设置

            String sys_lang = getAppLanguage(app);
            MyLog.d("", "app lang: " + sys_lang);

            if (sys_lang.contains("zh")) {
                app_lang = APP_LANG_CN;
            } else if (sys_lang.contains("en")) {
                app_lang = APP_LANG_EN;
            } else {
                //缺省为中文，但是不保存到preference中
                app_lang = APP_LANG_CN;
            }

        }

    }

    public static void setAppLang(Context context, int lang) {


        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();

        switch (lang) {
            case APP_LANG_CN:
                conf.locale = new Locale("zh", "CN");
                break;
            case APP_LANG_EN:
                conf.locale = new Locale("en");
                break;
        }

        res.updateConfiguration(conf, dm);

    }

    public static String getAppLanguage(Context context) {

        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();

        return conf.locale.getLanguage();

    }

    /**
     * 签名、id、第三方相关id等等
     */
//    public static String third_weixin_share_appID = "wx3af78a0a78d27a1d";
    public static String third_weixin_share_appID = "wx3288ead58cec1d03";
    //    public static String third_weixin_share_appID = "wx5640274decb96d97";
    public static String third_weixin_share_appSecret = "084bb8dd9994d47a6b2a25f2efd19c17";
    //    public static String third_weixin_share_appSecret = "70bf1f9249b55ae18544509ee1135b54";
    public static String third_baidu_push_id = "uovnYGf1jCMS5NDYgKqjxAym";
    public static String debug_app_signature = "5e3f136fdfddb7c27203e04bc9b3623b";//微信要验证这个，代码里面不用


    /**
     * h5 game
     */
    public static String h5game_url;// = "http://www.3366.com";


    /**
     * power on/off
     * screen on/off
     */
    public static ScreenOnReceiver screenOnReceiver = null;
    public static boolean isScreenOn = true;

    public static int app_open_count;

    public static int app_open_count_newbie_guide_1;
    public static int app_open_count_newbie_guide_2;
    public static int app_open_count_newbie_guide_3;


    /**
     * ACRA crash repost
     */
    public static void ACRA_custom_add_usr_info() {
        ACRA.getErrorReporter().putCustomData("usr_id", "" + MyConfig.usr_id);
        ACRA.getErrorReporter().putCustomData("usr_name", "" + MyConfig.usr_name);
        Date buildDate = new Date(BuildConfig.TIMESTAMP);   /*编译时间*/
        Format formatter = new SimpleDateFormat(MyConfig.app.getString(R.string.time_yyyy_MM_dd_HH_mm));
        ACRA.getErrorReporter().putCustomData("APK_GENERATE_TIME", "" + formatter.format(buildDate));


//        if(MyConfig.jsonGetCurrentCompanyRet != null && MyConfig.jsonGetCurrentCompanyRet.code==MyConfig.retSuccess()){
//            ACRA.getErrorReporter().putCustomData("usr_company_id", "" + MyConfig.jsonGetCurrentCompanyRet.data.id);
//        }

//        ACRA.getErrorReporter().putCustomData("ipAddress", "" + getIPAddress(true));

    }

//    public static String getIPAddress(boolean useIPv4) {
//        try {
//            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
//            for (NetworkInterface intf : interfaces) {
//                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
//                for (InetAddress addr : addrs) {
//                    if (!addr.isLoopbackAddress()) {
//                        String sAddr = addr.getHostAddress().toUpperCase();
//                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
//                        if (useIPv4) {
//                            if (isIPv4)
//                                return sAddr;
//                        } else {
//                            if (!isIPv4) {
//                                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
//                                return delim<0 ? sAddr : sAddr.substring(0, delim);
//                            }
//                        }
//                    }
//                }
//            }
//        } catch (Exception ex) { } // for now eat exceptions
//        return "";
//    }


    /**
     * debug
     */

    public static boolean debug_quick_menu = false;
    public static boolean debug_in_msg = false;
    public static boolean debug_toast_all = false;
    public static boolean debug_float_button_mainroom = false;
    public static boolean debug_autofill_validcode = false;
    public static boolean debug_show_not_joined_company = false;
    public static boolean debug_last_clientid_via_socket_repeat_check = false;
    public static boolean debug_msg_no_local_first = false;
    public static int debug_using_inner_server = 0;
    public static boolean debug_not_using_api_v2 = false;

    public static void resetAndSaveDebugSettingToPreference(Context context) {
        if (prefShared == null) {
            initPreferenceOperation(context);
        }

        MyConfig.debug_quick_menu = false;
        MyConfig.debug_in_msg = false;
        MyConfig.debug_toast_all = false;
        MyConfig.debug_float_button_mainroom = false;
        MyConfig.debug_autofill_validcode = false;
        MyConfig.debug_show_not_joined_company = false;
        MyConfig.debug_last_clientid_via_socket_repeat_check = false;
        MyConfig.debug_msg_no_local_first = false;
        MyConfig.debug_using_inner_server = 0;
        MyConfig.debug_not_using_api_v2 = false;

        saveDebugSettingToPreference(context);
    }

    public static void saveDebugSettingToPreference(Context context) {
        if (prefEditor == null) {
            initPreferenceOperation(context);
        }
        MyConfig.prefEditor.putBoolean("debug_quick_menu", MyConfig.debug_quick_menu);
        MyConfig.prefEditor.putBoolean("debug_in_msg", MyConfig.debug_in_msg);
        MyConfig.prefEditor.putBoolean("debug_toast_all", MyConfig.debug_toast_all);
        MyConfig.prefEditor.putBoolean("debug_float_button_mainroom", MyConfig.debug_float_button_mainroom);
        MyConfig.prefEditor.putBoolean("debug_autofill_validcode", MyConfig.debug_autofill_validcode);
        MyConfig.prefEditor.putBoolean("debug_show_not_joined_company", MyConfig.debug_show_not_joined_company);
        MyConfig.prefEditor.putBoolean("debug_last_clientid_via_socket_repeat_check", MyConfig.debug_last_clientid_via_socket_repeat_check);
        MyConfig.prefEditor.putBoolean("debug_msg_no_local_first", MyConfig.debug_msg_no_local_first);
        MyConfig.prefEditor.putInt("debug_using_inner_server", MyConfig.debug_using_inner_server);
        MyConfig.prefEditor.putBoolean("debug_not_using_api_v2", MyConfig.debug_not_using_api_v2);
        MyConfig.prefEditor.commit();
        MyLog.d("", "DebugSettingToPreference: save " + MyConfig.debug_in_msg + " " + MyConfig.debug_toast_all + " " + MyConfig.debug_float_button_mainroom);
    }

    public static void loadDebugSettingFromPreference(Context context) {
        if (prefShared == null) {
            initPreferenceOperation(context);
        }
        MyConfig.debug_quick_menu = MyConfig.prefShared.getBoolean("debug_quick_menu", false);
        MyConfig.debug_in_msg = MyConfig.prefShared.getBoolean("debug_in_msg", false);
        MyConfig.debug_toast_all = MyConfig.prefShared.getBoolean("debug_toast_all", false);
        MyConfig.debug_float_button_mainroom = MyConfig.prefShared.getBoolean("debug_float_button_mainroom", true);
        MyConfig.debug_autofill_validcode = MyConfig.prefShared.getBoolean("debug_autofill_validcode", true);
        MyConfig.debug_show_not_joined_company = MyConfig.prefShared.getBoolean("debug_show_not_joined_company", true);
        MyConfig.debug_last_clientid_via_socket_repeat_check = MyConfig.prefShared.getBoolean("debug_last_clientid_via_socket_repeat_check", true);
        MyConfig.debug_msg_no_local_first = MyConfig.prefShared.getBoolean("debug_msg_no_local_first", true);
        MyConfig.debug_using_inner_server = MyConfig.prefShared.getInt("debug_using_inner_server", 0);//index 0 is default server which get from gradle compile
        MyConfig.debug_not_using_api_v2 = MyConfig.prefShared.getBoolean("debug_not_using_api_v2", true);
        MyLog.d("", "DebugSettingToPreference: load " + MyConfig.debug_in_msg + " " + MyConfig.debug_toast_all + " " + MyConfig.debug_float_button_mainroom);
    }


    public static void MyToast(int level, Context context, String s) {

        if (debug_toast_all) {
            Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
        } else {

            /**
             * 如果想暂时屏蔽，将level设置为-1
             */

            if (level == 0) {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
            } else if (level == 1) {
                Toast.makeText(context, s, Toast.LENGTH_LONG).show();

            }
        }

    }


    /**
     * sys start
     */

//    public static int flag_viewpager_start_______seems_no_use = 0;
    public static int flag_open_invite_code_activity = 0;
    public static int flag_open_label_activity = 0;
    public static int flag_open_regist_from_weixin_activity = 0;
    public static boolean flag_have_share_in = false;


    /**
     * -1  网络服务未打开，或联网失败
     * 0   网络服务打开
     * 1   联网成功
     * 2   wifi可用
     * 3   3g/4g可用
     */
    public static int net_status;
    public static ConnectivityManager connMgr;

    public static int netConnectCheck() {
        if (connMgr == null) {
            connMgr = (ConnectivityManager)
                    MyConfig.app.getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null) {
            if (!networkInfo.isConnected()) {
                net_status = 0;
                return 0;
            } else {
                NetworkInfo mWiFiNetworkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                NetworkInfo mMobileNetworkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                if (mWiFiNetworkInfo != null && mWiFiNetworkInfo.isAvailable()) {
                    net_status = 2;
                    return 2;
                }
                if (mMobileNetworkInfo != null && mMobileNetworkInfo.isAvailable()) {
                    net_status = 3;
                    return 3;
                }
                net_status = 1;
                return 1;//联网
            }
//            if (networkInfo.isConnected()) {
//                net_status = 1;
//                return 1;//联网
//            } else {
//                net_status = 0;
//                return 0;//没联网
//            }
        }
        net_status = -1;
        return -1;//未知
    }

    public static void notifyNetworkError() {

    }

    /**
     * 相册相关
     */
    public static int GALLERY_MODE_PHOTOS_DOWNLOADED = 1;
    public static int GALLERY_MODE_GROUP_ALBUM = 2;
    public static IMsg iMsgGalleryRoot;
//    public static LinkedList<String> ll_group_album_thumbnail_url = new LinkedList<>();
    public static int galleryViewStartAt;


    public static void genGroupAlbumFullSizeUrl(IMsg root, LinkedList<JsonGetGroupAlbumRet.GetGroupAlbumRet> list, int position){

        MyConfig.iMsgGalleryRoot = root;
        MyConfig.iMsgGalleryRoot.ll_sub_image_url.clear();

        for(JsonGetGroupAlbumRet.GetGroupAlbumRet getGroupAlbumRet : list){

            MyConfig.iMsgGalleryRoot.ll_sub_image_url.add(MyConfig.getApiDomain_NoSlash_GetResources() + getGroupAlbumRet.fullSize);
        }

        galleryViewStartAt = position;
    }

    public static void genGalleryFullSizeUrl(IMsg root, IMsg iMsgClicked) {

        MyConfig.iMsgGalleryRoot = root;


        /**
         * 两件事
         *  1、把所有带图片的msg里面的图片地址加入ll_sub_gallery列表
         *  2、找到当前msg（即currentGalleryMsg）在ll_sub_gallery列表中的索引位置，以便在viewpager中设置为当前位置
         */
        int i = 0;
        boolean found = false;
        MyConfig.iMsgGalleryRoot.ll_sub_image_url.clear();

//        MyLog.d("", "gallery sort: MyConfig.iMsgClicked.clientId = " + MyConfig.iMsgClicked.clientId);
//        MyLog.d("","gallery sort: MyConfig.iMsgClicked = "+MyConfig.iMsgClicked);

        for(IMsg iMsg : MyConfig.iMsgGalleryRoot.ll_sub_msg_with_image){

            MyConfig.iMsgGalleryRoot.ll_sub_image_url.add(MyConfig.getApiDomain_NoSlash_GetResources() + iMsg.fullSize);

//            MyLog.d("", "comment image: ll_sub_image_url " + MyConfig.getApiDomain_NoSlash_GetResources() + iMsg.fullSize);
//            MyLog.d("","gallery sort: iMsg.clientId = "+iMsg.clientId);
//            MyLog.d("","gallery sort: iMsg = "+iMsg);

            if(iMsg.clientId == null){
                MyLog.d("", "comment image: iMsg.clientId == null" );
            }
//            if(MyConfig.iMsgClicked == null){
//                MyLog.d("", "comment image: MyConfig.iMsgClicked == null" );
//            }

//            if(iMsg.clientId != null && iMsg.clientId.equals(MyConfig.iMsgClicked.clientId)){
//                found = true;
//            }

            /**
             * 注意！
             * 这里是故意做的“==”比较，也即比较两个iMsg的内存地址以判断是否同一个iMsg
             * 比较clientId有风险，这是因为发现某些iMsg的clientId为空，这就无法比较了
             */
            if(iMsg == iMsgClicked){
                found = true;
            }

            if(!found) {
                i++;
            }
        }

//        ll_group_album_thumbnail_url = MyConfig.iMsgGalleryRoot.ll_sub_image_url;
        galleryViewStartAt = i;
    }


//    public static void httpAddImageIMsg2ImageList(IMsg iMsgListRoot, IMsg iMsg) {
//
//        if (iMsg.subType == MyConfig.SUBTYPE_IMAGE) {
//            iMsgListRoot.ll_sub_msg_with_image.addFirst(iMsg);
//        }
//
//        if (iMsg.subType == MyConfig.SUBTYPE_VOTE) {
//            if (iMsg.thumbNail != null) {
//                iMsgListRoot.ll_sub_msg_with_image.addFirst(iMsg);
//            }
//        }
//
//    }

    public static void addImageIMsg2ImageList(IMsg iMsgListRoot, IMsg iMsg, boolean atFirst) {

        boolean match = false;

        if (iMsg.type == MyConfig.TYPE_TOPIC_IMAGE_DIRECTROOM) {
            match = true;
        } else if (iMsg.type == TYPE_TOPIC) {
            if (iMsg.subType == MyConfig.SUBTYPE_IMAGE) {
                match = true;
            } else if (iMsg.subType == MyConfig.SUBTYPE_VOTE) {
                if (iMsg.thumbNail != null) {
                    match = true;
                }
            }
        } else if (iMsg.type == TYPE_TOPIC_COMMANT) {
            if (iMsg.subType == MyConfig.SUBTYPE_IMAGE_COMMENT) {
                match = true;
            }
        }

        if (match) {
            if (atFirst) {
                iMsgListRoot.ll_sub_msg_with_image.addFirst(iMsg);
            } else {
                iMsgListRoot.ll_sub_msg_with_image.addLast(iMsg);
            }
        }

    }

    public static void setImageIMsg2ImageList(IMsg iMsgListRoot, IMsg iMsg, int index) {

        /**
         * gallery activity显示图片时有一个list的复制操作，所以这里修改ll_sub_msg_with_image可以不在ui线程里面
         */

        boolean match = false;

        if (iMsg.type == MyConfig.TYPE_TOPIC_IMAGE_DIRECTROOM) {
            match = true;
        } else if (iMsg.type == TYPE_TOPIC) {
            if (iMsg.subType == MyConfig.SUBTYPE_IMAGE) {
                match = true;
            } else if (iMsg.subType == MyConfig.SUBTYPE_VOTE) {
                if (iMsg.thumbNail != null) {
                    match = true;
                }
            }
        } else if (iMsg.type == TYPE_TOPIC_COMMANT) {
            if (iMsg.subType == MyConfig.SUBTYPE_IMAGE_COMMENT) {
                match = true;
            }
        }

        if (match) {
            iMsgListRoot.ll_sub_msg_with_image.set(index, iMsg);
        }

    }


    /**
     * tag
     */
    public static final String TAG_LIFECYCLE = "LIFECYCLE";
    public static final String TAG_LOGIN_CMD = "login_cmd";
    public static boolean FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE = false;
    public static boolean FALSE_TAG = false;
    public static boolean TRUE_TAG = true;

    public static final int MODE_CHOOSE_SINGLE = 0;
    public static final int MODE_CHOOSE_SINGLE_WITHOUT_SUBMIT = 2;
    public static final int MODE_CHOOSE_MULTI = 1;
    public static final int MODE_CHOOSE_MULTI_DELETE = 3;

    public static final int MSG_FROM_SERVER = 0;
    public static final int MSG_FROM_LOCAL = 1;

    /**
     * Preferences & save
     */
    public static final String SHARED_PREFERENCES = "shared_preferences";
    public static SharedPreferences prefShared;
    public static SharedPreferences.Editor prefEditor;
    public static final String PREF_USER_ID = "preference_user_id";
    public static final String PREF_USER_NAME = "preference_user_name";
    public static final String PREF_USER_NICKNAME = "preference_user_nickname";
    public static final String PREF_USER_PWD = "preference_user_pwd";
    public static final String PREF_USER_TOKEN = "preference_user_token";
    public static final String PREF_USR_STATUS = "preference_user_status";
    public static final String PREF_USER_EMAIL = "preference_user_email";
    public static final String PREF_USER_PHONE = "preference_user_phone";
    public static final String PREF_USER_SEX = "preference_user_sex";
    public static final String PREF_USER_NAME_CHANGED = "preference_user_name_changed";
    public static final String PREF_USER_ORIGIN = "preference_user_origin";
    public static final String PREF_USR_AVATAR = "preference_user_avatar";
    public static final String PREF_JSON_LOGIN_RET = "preference_json_login_ret";
    public static final String PREF_USER_TYPE ="preference_user_type";

    public static final String PREF_USR_WEIXIN_NAME = "preference_usr_weixin_name";
    public static final String PREF_USR_WEIXIN_ACCESS_TOKEN = "preference_usr_weixin_token";
    public static final String PREF_USR_WEIXIN_UID = "preference_usr_weixin_uid";
    public static final String PREF_USR_WEIXIN_OPENID = "preference_usr_weixin_openid";
    public static final String PREF_USR_WEIXIN_HEADIMGURL = "preference_usr_weixin_headimgurl";


    public static final String PREF_USR_WEIXIN_LANG = "preference_usr_weixin_lang";
    public static final String PREF_USR_WEIXIN_COUNTRY_CODE = "preference_usr_weixin_country_code";
    public static final String PREF_USR_WEIXIN_COMPANY = "preference_usr_weixin_company";
    public static final String PREF_USR_WEIXIN_TITLE = "preference_usr_weixin_title";
    public static final String PREF_USR_WEIXIN_ENABLE_NOTI = "preference_usr_weixin_enable_noti";
    public static final String PREF_USR_WEIXIN_DUTY = "preference_usr_weixin_duty";
    public static final String PREF_USR_WEIXIN_COMPANY_INTRO = "preference_usr_weixin_company_intro";
    public static final String PREF_USR_WEIXIN_CITY = "preference_usr_weixin_city";
    public static final String PREF_USR_WEIXIN_CURRENT_CLASS = "preference_usr_weixin_current_class";
    public static final String PREF_USR_WEIXIN_CURRENT_CLASS_NAME = "preference_usr_weixin_current_class_name";

    public static final String PREF_JPUSH_REGIST_ID = "preference_jpush_regist_id";

    public static void updateUserStatus(int new_status) {
        MyConfig.usr_status = new_status;
        MyConfig.prefEditor.putInt(MyConfig.PREF_USR_STATUS, MyConfig.usr_status);
        MyConfig.prefEditor.commit();
        MyLog.d("", "company change flow: updateUserStatus " + new_status);
        EventBus.getDefault().post(new EventUserStatusUpdate(MyConfig.usr_status));
    }

    public static void updateUserInforFromLogin(Context context, String pwd, String s, JsonWeixinLoginRet jsonLoginRet) {
        MyConfig.logoutClear();

        MyConfig.usr_login_ret_json = s;
        MyConfig.jsonWeixinLoginRet = jsonLoginRet;
        MyConfig.usr_id = jsonLoginRet.data.id;
        MyConfig.usr_name = jsonLoginRet.data.name;//注意name要从这里取，因为login_name_or_phone也可能是phone
        MyConfig.usr_nickname = jsonLoginRet.data.nickName;
        MyConfig.usr_phone = jsonLoginRet.data.phone;
        MyConfig.usr_pwd = pwd;//小心，服务器不会返回密码
        MyLog.d("", "updateUserInforFromLogin : get pwd when login: " + MyConfig.usr_pwd);
        MyConfig.usr_token = jsonLoginRet.data.authToken;//常用变量就提取出来
        MyConfig.usr_status = MyConfig.USR_STATUS_LOGIN;
        MyLog.d("","login failed: 5" + MyConfig.usr_status);
        MyConfig.usr_email = jsonLoginRet.data.email;
        MyConfig.usr_phone = jsonLoginRet.data.phone;
        if (jsonLoginRet.data.sex) {
            MyConfig.usr_sex = 1;//男
        } else {
            MyConfig.usr_sex = 0;//女
        }
        MyConfig.usr_avatar = jsonLoginRet.data.avatar;//实际上这一步没有avatar信息
        MyConfig.usr_origin = jsonLoginRet.data.origin;

        MyConfig.usr_lang = jsonLoginRet.data.lang;
        MyConfig.usr_country_code = jsonLoginRet.data.countryCode;
        MyConfig.usr_type = jsonLoginRet.data.userType;
        MyConfig.usr_company = jsonLoginRet.data.company;
        MyConfig.usr_title = jsonLoginRet.data.title;
        MyConfig.usr_enableNoti = jsonLoginRet.data.enableNoti;
        MyConfig.usr_duty = jsonLoginRet.data.duty;
        MyConfig.usr_company_intro = jsonLoginRet.data.companyIntro;
        MyConfig.usr_city = jsonLoginRet.data.city;
        MyConfig.usr_current_class = jsonLoginRet.data.currentClass;
        MyConfig.usr_current_class_name = jsonLoginRet.data.currentClassName;

        MyLog.d("", "updateUserInforFromLogin : " + usr_lang + " === " + usr_country_code);

        if (jsonLoginRet.data.info != null) {
            MyConfig.usr_name_changed = jsonLoginRet.data.info.changed;
        }

        MyConfig.saveUserDataToPreference(context);
    }

    public static void saveUserDataToPreference(Context context) {
        if (prefEditor == null) {
            initPreferenceOperation(context);
        }
        MyConfig.prefEditor.putInt(MyConfig.PREF_USER_ID, MyConfig.usr_id);
        MyConfig.prefEditor.putString(MyConfig.PREF_USER_NAME, MyConfig.usr_name);
        MyConfig.prefEditor.putString(MyConfig.PREF_USER_NICKNAME, MyConfig.usr_nickname);
        MyConfig.prefEditor.putString(MyConfig.PREF_USER_PWD, MyConfig.usr_pwd);
        MyLog.d("", "updateUserInforFromLogin : save pwd: " + MyConfig.usr_pwd);
        MyConfig.prefEditor.putString(MyConfig.PREF_USER_TOKEN, MyConfig.usr_token);
        MyConfig.prefEditor.putInt(MyConfig.PREF_USR_STATUS, MyConfig.usr_status);
        MyConfig.prefEditor.putString(MyConfig.PREF_USER_EMAIL, MyConfig.usr_email);
        MyConfig.prefEditor.putString(MyConfig.PREF_USER_PHONE, MyConfig.usr_phone);
        MyConfig.prefEditor.putInt(MyConfig.PREF_USER_SEX, MyConfig.usr_sex);
        MyConfig.prefEditor.putBoolean(MyConfig.PREF_USER_NAME_CHANGED, MyConfig.usr_name_changed);
        MyConfig.prefEditor.putString(MyConfig.PREF_USER_ORIGIN, MyConfig.usr_origin);
        MyConfig.prefEditor.putString(MyConfig.PREF_USR_AVATAR, MyConfig.usr_avatar);
        MyConfig.prefEditor.putInt(MyConfig.PREF_USER_TYPE ,MyConfig.usr_type);
        MyConfig.prefEditor.putString(MyConfig.PREF_JSON_LOGIN_RET, MyConfig.usr_login_ret_json);//把jsonLoginRet的json字符串保存在prefence中

        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_NAME, MyConfig.usr_weixin_name);
        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_ACCESS_TOKEN, MyConfig.usr_weixin_access_token);
        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_UID, MyConfig.usr_weixin_uid);
        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_OPENID, MyConfig.usr_weixin_openid);
        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_HEADIMGURL, MyConfig.usr_weixin_headimgurl);

        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_LANG, MyConfig.usr_lang);
        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_COUNTRY_CODE, MyConfig.usr_country_code);
        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_COMPANY, MyConfig.usr_company);
        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_TITLE, MyConfig.usr_title);
        MyConfig.prefEditor.putBoolean(MyConfig.PREF_USR_WEIXIN_ENABLE_NOTI, MyConfig.usr_enableNoti);
        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_DUTY, MyConfig.usr_duty);
        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_COMPANY_INTRO, MyConfig.usr_company_intro);
        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_CITY, MyConfig.usr_city);
        MyConfig.prefEditor.putInt(MyConfig.PREF_USR_WEIXIN_CURRENT_CLASS, MyConfig.usr_current_class);
        MyConfig.prefEditor.putString(MyConfig.PREF_USR_WEIXIN_CURRENT_CLASS_NAME, MyConfig.usr_current_class_name);


        MyConfig.prefEditor.commit();

        ACRA_custom_add_usr_info();
    }


    public static void loadUserDataFromPreference(Context context) {
        if (prefShared == null) {
            initPreferenceOperation(context);
        }
        MyConfig.usr_id = MyConfig.prefShared.getInt(MyConfig.PREF_USER_ID, -1);
        MyConfig.usr_name = MyConfig.prefShared.getString(MyConfig.PREF_USER_NAME, null);
        MyConfig.usr_nickname = MyConfig.prefShared.getString(MyConfig.PREF_USER_NICKNAME, null);
        MyConfig.usr_pwd = MyConfig.prefShared.getString(MyConfig.PREF_USER_PWD, null);
        MyLog.d("", "updateUserInforFromLogin : load pwd: " + MyConfig.usr_pwd);
        MyConfig.usr_token = MyConfig.prefShared.getString(MyConfig.PREF_USER_TOKEN, null);
        MyConfig.usr_status = MyConfig.prefShared.getInt(MyConfig.PREF_USR_STATUS, MyConfig.USR_STATUS_UNKNOWN);
        MyConfig.usr_email = MyConfig.prefShared.getString(MyConfig.PREF_USER_EMAIL, null);
        MyConfig.usr_phone = MyConfig.prefShared.getString(MyConfig.PREF_USER_PHONE, null);
        MyConfig.usr_origin = MyConfig.prefShared.getString(MyConfig.PREF_USER_ORIGIN, null);
        MyConfig.usr_name_changed = MyConfig.prefShared.getBoolean(MyConfig.PREF_USER_NAME_CHANGED, false);
        MyConfig.usr_sex = MyConfig.prefShared.getInt(MyConfig.PREF_USER_SEX, -1);
        MyConfig.usr_avatar = MyConfig.prefShared.getString(MyConfig.PREF_USR_AVATAR, null);
        MyConfig.usr_login_ret_json = MyConfig.prefShared.getString(MyConfig.PREF_JSON_LOGIN_RET, null);
        MyConfig.usr_type = MyConfig.prefShared.getInt(MyConfig.PREF_USER_TYPE,-1);

        MyConfig.jsonWeixinLoginRet = new Gson().fromJson(MyConfig.usr_login_ret_json, JsonWeixinLoginRet.class);

        MyConfig.usr_weixin_name = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_NAME, null);
        MyConfig.usr_weixin_access_token = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_ACCESS_TOKEN, null);
        MyConfig.usr_weixin_uid = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_UID, null);
        MyConfig.usr_weixin_openid = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_OPENID, null);
        MyConfig.usr_weixin_headimgurl = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_HEADIMGURL, null);

        MyConfig.usr_lang = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_LANG, null);
        MyConfig.usr_country_code = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_COUNTRY_CODE, null);
        MyConfig.usr_company = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_COMPANY, null);
        MyConfig.usr_title = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_TITLE, null);
        MyConfig.usr_enableNoti = MyConfig.prefShared.getBoolean(MyConfig.PREF_USR_WEIXIN_ENABLE_NOTI, false);
        MyConfig.usr_duty = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_DUTY, null);
        MyConfig.usr_company_intro = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_COMPANY_INTRO, null);
        MyConfig.usr_city = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_CITY, null);
        MyConfig.usr_current_class = MyConfig.prefShared.getInt(MyConfig.PREF_USR_WEIXIN_CURRENT_CLASS, -1);
        MyConfig.usr_current_class_name = MyConfig.prefShared.getString(MyConfig.PREF_USR_WEIXIN_CURRENT_CLASS_NAME, null);

        ACRA_custom_add_usr_info();
    }

    public static void initPreferenceOperation(Context context) {
        MyConfig.prefShared = context.getSharedPreferences(MyConfig.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        MyConfig.prefEditor = MyConfig.prefShared.edit();
    }


    /**
     * 使用如下四个函数要留意，其中的name不能重复
     * 故推荐name加上父层级为前缀
     */
    public static void saveStringToPreference(Context context, String name, String str) {
        if (prefEditor == null) {
            initPreferenceOperation(context);
        }
        MyConfig.prefEditor.putString(name, str);
        MyConfig.prefEditor.commit();
    }

    public static String loadStringFromPreference(Context context, String name) {
        if (prefShared == null) {
            initPreferenceOperation(context);
        }
        return MyConfig.prefShared.getString(name, null);
    }

    public static void saveIntToPreference(Context context, String name, int i) {
        if (prefEditor == null) {
            initPreferenceOperation(context);
        }
        MyConfig.prefEditor.putInt(name, i);
        MyConfig.prefEditor.commit();
    }

    public static int loadIntFromPreference(Context context, String name) {
        if (prefShared == null) {
            initPreferenceOperation(context);
        }
        return MyConfig.prefShared.getInt(name, -1);
    }

    /**
     * user
     */
    public static int usr_status = MyConfig.USR_STATUS_UNKNOWN;//0:logout, 1:login but no current company, 2:login and have current company
    public static final int USR_STATUS_LOGOUT = 0;
    public static final int USR_STATUS_LOGIN = 1;
    public static final int USR_STATUS_HAVE_CURRENT_COMPANY = 2;
    public static final int USR_STATUS_UNKNOWN = -1;

    public static final int USER_SEX_MAN = 1;
    public static final int USER_SEX_WOMAN = 0;

    /**
     leader: 1,   //   管理员
     headTeacher: 2,  // 班主任
     classMonitor: 3,   // 班长
     student: 4,
     guest: 5,
     stuff: 6
     */
    public static final int USER_TYPE_IS_TEACHER = 2;//代表老师身份
    public static final int USER_TYPE_IS_STUDENT = 4;//代表学生身份

    public static int usr_push_status = -1;//0:not regist, 1:registed but havn't notify server, 2:notified server
    public static int usr_id;   //用户id
    public static String usr_name; //用户名
    public static String usr_nickname;//用户昵称
    public static int usr_sex; //目前服务器用false表示女，true表示男，从长久考虑，本地转换为int类型
    public static String usr_avatar;
    public static String usr_email;
    public static String usr_pwd;
    public static String usr_phone;
    public static String usr_token;
    public static String usr_login_ret_json;
    public static boolean usr_name_changed;
    public static String usr_origin;
//    public static String usr_name_or_phone_temply_for_login_or_reset;

    public static String usr_lang;
    public static String usr_country_code;
    public static String usr_company;
    public static String usr_title;
    public static boolean usr_enableNoti;
    public static int usr_type;
    public static String usr_duty;
    public static String usr_company_intro;
    public static String usr_city;
    public static int usr_current_class;
    public static String usr_current_class_name;


    /**
     * weixin login account infor
     */
    public static String usr_weixin_name;
    public static String usr_weixin_access_token;
    public static String usr_weixin_uid;
    public static String usr_weixin_openid;
    public static String usr_weixin_headimgurl;

    public static String usr_weixin_refresh_token;
    public static String usr_weixin_expires_in;
    public static String usr_weixin_refresh_token_expires;
    public static String usr_weixin_scope;


    /**
     * data from server
     */
    public static JsonWeixinLoginRet jsonWeixinLoginRet;
    //    public static JsonLoginRet jsonLoginRet;
    public static JsonGroupCanJoinRet jsonGroupCanJoinRet;//用户未加入的小组
    public static JsonUserGroupsRet jsonUserGroupsRet;//用户已经加入的小组
    public static JsonCompanyCanJoinRet jsonCompanyCanJoinRet;//用户未加入的公司
    public static JsonCompanyCanJoinRet jsonCompanyCanJoinRet_sort;//用户未加入的公司，有排序
    public static JsonUserCompaniesRet jsonUserCompaniesRet;//用户已经加入的公司
    public static JsonGetCurrentCompanyRet jsonGetCurrentCompanyRet; //保存当前公司
    public static JsonGetVersionsRet jsonGetVersionsRet_from_server;//当前版本信息
    public static JsonGetCompanyInfoRet jsonGetCompanyInfoRet;//某个公司的信息
    public static JsonGetGroupInfoRet jsonGetGroupInfoRet;//某个小组的信息
    public static JsonGetGroupUserRet jsonGetGroupUserRet;//某个小组的用户列表
    public static JsonGetCompanyUserListRet jsonGetCompanyUserListRet;//某个公司的用户列表
    public static LinkedList<ContactUniversal> jsonGetCompanyUserListRet_universal;//jsonGetCompanyUserListRet里面的联系人链表，并且已经转换为universal，添加首字母
    public static JsonGetCoursesBySlotRet jsonGetCoursesBySlotRet;//保存为map的日期课程信息
    public static JsonGetMyPrivilege jsonGetMyPrivilege;//保存为map的本人权限
    public static JsonSchoolContactRet jsonSchoolContactRet;//校友录中所有班级信息
    public static JsonSchoolContactEveryClass jsonSchoolContactEveryClass; //校友录中某个班级的成员列表
    public static JsonCreateAulmCardRet jsonCreateAulmCardRet;//二维码数据从后台获取
    public static JsonCurrentYearClassRet jsonCurrentYearClassRet;//返回同届班级信息
	public static JsonMadeInOrderFromServiceRet jsonMadeInOrderFromServiceRet;//返回APP定制化的数据
    public static JsonGetGroupAlbumRet jsonGetGroupAlbumRet;//当前班级的相册

    public static LinkedList<JsonCurrentYearClassRet.CurrentYearClassRet> current_classes = new LinkedList<>();

    public static void filterClassFromGroupList(final IF_AfterHttpRequest iGetClassList) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();
                String s = new HttpTools().httpURLConnGet(getApiDomain_HaveSlash_ApiV2() + API_ADDRESSBOOKS_CURRENTYEAR, usr_token);
                MyLog.d("所有同届的班级", s);
                MyConfig.jsonCurrentYearClassRet = gson.fromJson(s, JsonCurrentYearClassRet.class);
                if (MyConfig.jsonCurrentYearClassRet.data != null && MyConfig.jsonCurrentYearClassRet.code == retSuccess()) {

                    current_classes.clear();
                    for (JsonCurrentYearClassRet.CurrentYearClassRet current : MyConfig.jsonCurrentYearClassRet.data) {
                        current_classes.add(current);
                    }

                    if (iGetClassList != null) {
                        iGetClassList.doAfterHttpRequest();
                    }

                }
            }
        }).start();


    }


    //    public static boolean hasGetUserlist ;
    public static final int ERROR_INVALID_TOKEN = 402001;

    public static void HttpRetCodeCheck(String ret) {
        JsonHttpRet_Universal jsonHttpRet_universal = new Gson().fromJson(ret, JsonHttpRet_Universal.class);
        if (jsonHttpRet_universal.code == MyConfig.retSuccess()) {//登陆成功

        } else if (jsonHttpRet_universal.code == ERROR_INVALID_TOKEN && MyConfig.usr_status != MyConfig.USR_STATUS_LOGOUT) {
            //说明token失效,关闭viewpager之外所有activity打开登录页面
            MyLog.d("", "app start steps 2.1 --- HttpRetCodeCheck");
            //  MyConfig.MyToast(0,app, MyConfig.getErrorMsg("" + jsonHttpRet_universal.code));
            EventBus.getDefault().post(new EventLogout(jsonHttpRet_universal.code));
        } else {//登陆失败

        }

    }

    public static void logoutClear() {
        usr_id = -1;
        if (usr_weixin_name != null) {
            /**
             * 用这个来判断是否微信登录
             * 由于微信登录后再登录server返回的用户名及密码不能再次登录
             * 所以这里将其清除
             */
            usr_name = null;
            usr_pwd = null;
        }
        usr_token = null;
        usr_status = MyConfig.USR_STATUS_LOGOUT;//0:logout, 1:login
//        usr_email = null;
//        usr_phone = null;
//        usr_sex = -1;
//        usr_avatar = null;
        usr_login_ret_json = null;

        jsonWeixinLoginRet = null;
        jsonGroupCanJoinRet = null;
        jsonUserGroupsRet = null;
        jsonCompanyCanJoinRet = null;
        jsonUserCompaniesRet = null;
        jsonGetCurrentCompanyRet = null;
        jsonCompanyCanJoinRet_sort = null;
        usr_push_status = 0;


        usr_weixin_name = null;
        usr_weixin_access_token = null;
        usr_weixin_openid = null;
        usr_weixin_uid = null;
        usr_weixin_headimgurl = null;


    }


    public static final int USER_INDEX_ADMIN = 0;
    public static final int USER_INDEX_ME = 1;
    public static final int USER_INDEX_OTHER = 2;

    /**
     * room
     */

    public static int subjectListGroupId;

    public static final int SERVER_ROOM_CATEGORY_NORMAL = 1;// 普通群组
    public static final int SERVER_ROOM_CATEGORY_PRIVATE = 4;// 私有群组, 在群组列表中看不到, 只能邀请别人加入
    public static final int SERVER_ROOM_CATEGORY_DIRECT = 3;// 两人私信聊天
    public static final int SERVER_ROOM_CATEGORY_SINGLE = 5;// 只有一个人的群组, 通知等信息可以发给该群组
    public static final int SERVER_ROOM_CATEGORY_CLASS = 6;// 班级
    public static final int SERVER_ROOM_CATEGORY_NEWS = 2;//新闻小组
    public static final int SERVER_ROOM_CATEGORY_CHAT_ROOM = 8;//聊天室小组

    /**
     * 20160712 私聊使用普通小组框架
     */
    public static final int LOCAL_ROOM_CATEGORY_NORMAL = 0;//普通小组
    public static final int LOCAL_ROOM_CATEGORY_DIRECT = 1;//私聊小组

    /**
     * company
     */
    public static String[] citylist;
    public static String[] categorylist;

    /**
     * 如果threshold为3
     * 当listview的下端隐藏了3条以上的消息时
     * 可以假定用户正在查看以前的信息，所以listview在收到新msg时不滑动到最低端
     * 如果新消息到达时listview滑动到底，这会导致用户难以查看过去的信息
     * 当隐藏了不到3条消息
     * 可以认为用户正在等待最新msg，故listview在收到新msg时始终滑动到最低端
     */
    public static int room_listview_scroll_threshold = 3;
    public static int room_timeout_to_clear_unread = 2;

    /**
     * Intent
     */
    public static final String INTENT_KEY_CREATE_SUBJECT_TITLE = "create_subject_edit_title";
    public static final String INTENT_KEY_CREATE_SUBJECT_BODY = "create_subject_edit_body";
    public static final String INTENT_KEY_CREATE_SUBJECT_ATTACHED = "create_subject_edit_attached";
    public static final String INTENT_KEY_CREATE_VOTE_ATTACHED = "create_vote_edit_attached";
    public static final String INTENT_KEY_MAP_LOCATION_LONGITUDE = "map_location_longitude";
    public static final String INTENT_KEY_MAP_LOCATION_LATITUDE = "map_location_latitude";
    public static final String INTENT_KEY_MAP_LOCATION_SCREENSHOT = "map_location_screen_shoot";
    public static final int REQUEST_SUBJECT_ONLY = 300;
    public static final int REQUEST_SUBJECT_WITH_IMAGE = 301;
    //    public static final int REQUEST_SUBJECT_WITH_VOTE = 302;
    public static final int REQUEST_SUBJECT_WITH_MAP = 303;
    public static final int REQUEST_SUBJECT_WITH_FILE = 304;
    public static final int REQUEST_SUBJECT_WITH_VIDEO = 305;
    public static final int REQUEST_IMAGE_CROP = 400;
    public static final int REQUEST_CAMERA = 401;
    public static final int REQUEST_GALLERY = 402;
    public static final int REQUEST_FILE = 403;
    public static final int REQUEST_MAP = 404;
    public static final int REQUEST_VIDEO = 405;
    public static final int REQUEST_CREATE_VOTE = 208;
    public static final int REQUEST_LOGIN = 209;
    public static final int REQUEST_CREATE_ROOM = 210;
    public static final int REQUEST_CREATE_COMPANY = 211;
    public static final int REQUEST_CHOOSE_COMPANY = 212;
    public static final int REQUEST_GROUP_INFO = 213;
    public static final int REQUEST_SUBJECT_LIST = 214;
    public static final int REQUEST_CHOOSE_GROUP = 215;
    public static final int REQUEST_GALLERY_AVATAR = 216;
    public static final int REQUEST_COMPANY_INVITE = 217;
    public static final int REQUEST_USER_UPDATE = 219;
    public static final int REQUEST_CANJOIN_COMPANY = 220;
    public static final int REQUEST_COMPANY_SETTING = 221;
    public static final int REQUEST_COMPANY_GROUPS = 222;
    public static final int REQUEST_COMPANY_UPDATE = 223;
    public static final int REQUEST_CHANGE_PASSWORD = 224;
    public static final int REQUEST_CONTACT_MULTI_CHOOSE = 225;
    public static final int REQUEST_COMPANY_VIEWS = 226;
    public static final int REQUEST_INVITE_FROM_PHONE_CONTACT = 227;
    public static final int REQUEST_CHOOSE_CONTACT = 228;

    /**
     * @某人的请求码
     */
    public static int PEG_SOME_BODY_REQUSET_CODE = 229;
    /**
     * @某人的响应码
     */
    public static int PEG_SOME_BODY_RESPONSE_CODE = 230;
/**创建报名请求码*/
    public static final int REQUEST_CREATE_ACTIVITY = 231;

    /**注册标签页请求码*/
    public static final int REQUEST_REGIST_LABEL = 232;



    /**
     * share in or out
     */
    public static final int MAINROOM_STARTED_NORMAL = 0;
    public static final int MAINROOM_STARTED_FOR_SHARE_IN = 1;

    public static final int SHARE_IN_CATEGORY_NOT_SHARE_IN = -1;
    public static final int SHARE_IN_CATEGORY_EMPT = 0;
    public static final int SHARE_IN_CATEGORY_TEXT = 1;
    public static final int SHARE_IN_CATEGORY_IMAGE = 2;
    public static final int SHARE_IN_CATEGORY_IMAGE_LIST = 3;
    public static final int SHARE_IN_CATEGORY_FILE = 4;
    public static final int SHARE_IN_CATEGORY_UNKNOWN = 5;
    public static final int SHARE_IN_CATEGORY_NEWS = 6;
    /**
     * 不带图片的今日头条
     */
    public static final int SHARE_IN_TODAY_TOP1 = 7;

    public static Intent intent_share_in;
    public static int share_in_category;
    public static String extra_text;
    public static String extra_subject;
    public static Uri extra_stream;
    public static ArrayList<Uri> extra_stream_list = new ArrayList<>();


    public static void shareParse(Intent intent) {

        /**
         *
         * 外部资料分享到小集体，根据type中mime进行分类
         *
         *  text/plain
         *      文本、地图
         *          EXTRA_TEXT
         *          EXTRA_SUBJECT
         *
         *  image/png或image/*
         *      图片或图文
         *          EXTRA_TEXT
         *          EXTRA_SUBJECT
         *          EXTRA_STREAM: file:///storage/emulated/0/Android/data/com.android.browser/cache/share.jpg
         *
         *  *\/*
         *      文件或图片
         *          EXTRA_STREAM: file:///storage/emulated/0/Android/data/com.tencent.androidqqmail/cache/attachment/ActivityUserInfo.java
         *
         * ===== 现实很残酷 =============
         * 每一个app可能有不同的理解，已知：
         *      小米文档管理器分享文本文档时设置type为text/plain
         *      分享图片时为image/* ----- 这个ok
         *      分享
         *
         *
         *
         *
         */

        // Get intent, action and MIME type

        String action = intent.getAction();
        String type = intent.getType();
        MyLog.d("", "share in process: get: " + action + " type : " + type);

        MyLog.d("", "share in process: get EXTRA_TEXT: " + intent.getStringExtra(Intent.EXTRA_TEXT));
        MyLog.d("", "share in process: get EXTRA_SUBJECT: " + intent.getStringExtra(Intent.EXTRA_SUBJECT));
        MyLog.d("", "share in process: get EXTRA_TITLE: " + intent.getStringExtra(Intent.EXTRA_TITLE));
        MyLog.d("", "share in process: get EXTRA_HTML_TEXT: " + intent.getStringExtra(Intent.EXTRA_HTML_TEXT));
        MyLog.d("", "share in process: get EXTRA_STREAM: " + intent.getParcelableExtra(Intent.EXTRA_STREAM));

        //通常这两项都有，先取出来
        MyConfig.extra_text = intent.getStringExtra(Intent.EXTRA_TEXT);
        MyConfig.extra_subject = intent.getStringExtra(Intent.EXTRA_SUBJECT);
        MyConfig.extra_stream = intent.getParcelableExtra(Intent.EXTRA_STREAM);
//        MyConfig.extra_stream_list = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);//java.lang.ClassCastException: android.net.Uri$HierarchicalUri cannot be cast to java.util.ArrayList

        if (Intent.ACTION_SEND.equals(action)) {
            if (type != null) {
                if (type.startsWith("text/")) {
                    MyLog.d("", "share in process: text/plain");

                    if (MyConfig.extra_stream == null) {
                        MyConfig.share_in_category = SHARE_IN_CATEGORY_TEXT;
                    } else {
                        MyConfig.share_in_category = SHARE_IN_CATEGORY_FILE;
                    }
                    return;

                } else if (type.startsWith("image/")) {
                    MyLog.d("", "share in process: image/");


                    MyConfig.share_in_category = SHARE_IN_CATEGORY_IMAGE;
                    return;

                } else {    //其他类型
                    MyLog.d("", "share in process: unknown: " + type);

                    /**
                     * 未知类型，按file处理
                     */
                    MyConfig.share_in_category = SHARE_IN_CATEGORY_FILE;
                    return;

                }
            } else {  //无类型
                MyLog.d("", "share in process: type is null");

                MyConfig.share_in_category = SHARE_IN_CATEGORY_UNKNOWN;
                return;
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            MyLog.d("", "share in process: action is ACTION_SEND_MULTIPLE");
            if (type.startsWith("image/")) {


                MyConfig.share_in_category = SHARE_IN_CATEGORY_IMAGE_LIST;
                return;

            }
        } else {
            // Handle other intents, such as being started from the home screen
            MyLog.d("", "share in process: action is unknown");

            MyConfig.share_in_category = SHARE_IN_CATEGORY_UNKNOWN;
            return;

        }

        MyConfig.share_in_category = SHARE_IN_CATEGORY_UNKNOWN;
        return;
    }

    /**
     * 极光推送
     */
    public static String jpush_regist_id;


    /**
     * push
     */
    public static final int PUSH_onMessage = 1;
    public static final int PUSH_onNotificationClicked = 2;
    public static final int PUSH_onNotificationArrived = 3;

    //baidu push
    public static String baidupush_appid;
    public static String baidupush_userId;
    public static String baidupush_channelId;
    public static String baidupush_requestId;


    public static void baidupush_startWork() {
        PushManager.startWork(MyConfig.app, PushConstants.LOGIN_TYPE_API_KEY, third_baidu_push_id);
    }


    public static void cancelAllNotification() {
        /**
         * 什么时候清除notifications？
         *      切换公司
         *      点击notification
         *      退出登录
         */
        if (app != null) {
            NotificationManager nm = (NotificationManager) app.getSystemService(Context.NOTIFICATION_SERVICE);
            nm.cancelAll();
        }
    }

    /**
     * http setting
     */
    public static final int METHOD_HTTP_GET = 1;
    public static final int METHOD_HTTP_POST = 2;

    public static final String API_DOMAIN_CRASH_SERVER = "http://olddev.datacanvas.io/";
    public static final String API_DOMAIN_CRASH_LOG = "logs/androidcrash";


    /**
     * 原先的内部测试服务器   http://im.datacanvas.io
     * 小集体生产环境      http://docy.datacanvas.io
     * 同窗校友内部测试服务器  http://embadev.datacanvas.io
     */


    public static String servers[] = {
            BuildConfig.SERVER_DEFAULT,
//            "http://im.datacanvas.io",
//            "http://docy.datacanvas.io",
          "http://olddev.datacanvas.io"
//            "http://embadev.datacanvas.io",
//            "http://embatest.datacanvas.io"
    };


//    public static final String SERVER_1 = "http://im.datacanvas.io";
//    public static final String SERVER_2 = "http://docy.datacanvas.io";  //"http://docy.datacanvas.io";
//    public static final String SERVER_3 = "http://embadev.datacanvas.io";
//    public static final String SERVER_4 = "http://embatest.datacanvas.io";


//    public static boolean returnSuccess(int code){
//
//        if(code == retSuccess()){
//            return true;
//        }else{
//            String errMsg = getErrorMsg(code);
//            return false;
//        }
//
//    }

    public static int retSuccess() {
        if (debug_not_using_api_v2) {
            return 200;//在同窗服务器上即便不使用v2也是返回200
        } else {
            return 200;
        }
    }

    public static String getApiDomain_NoSlash_GetResources() {
        /**
         * look like:
         *  http://im.docy.co + /resource.jpg
         */

        return getServer();
    }

    @NonNull
    public static String getServer() {

        return servers[debug_using_inner_server];

//        if (debug_using_inner_server) {
//            return SERVER_4;
//        } else {
//            return SERVER_4;
//        }
    }

    public static String getApiDomain_NoSlash_ApiV2() {
        /**
         * look like:
         *  http://im.docy.co/ + login
         */

        if (debug_not_using_api_v2) {
            return getApiDomain_NoSlash_GetResources();
        } else {
            return getApiDomain_NoSlash_GetResources() + "/v2";
        }

    }

    public static String getRtmUrl() {

        return getServer();
    }

    public static String getApiDomain_HaveSlash_ApiV2() {
        /**
         * look like:
         *  http://im.docy.co/ + login
         */

        if (debug_not_using_api_v2) {
            return getApiDomain_NoSlash_GetResources() + "/";
        } else {
            return getApiDomain_NoSlash_GetResources() + "/v2/";
        }

    }

    public static String rtm_tag = "message";
    public static Emitter.Listener rtm_listener;

//    public static String API_DOMAIN_WITH_SLASH = SOCKET_SERVER_2 + "/";
//    public static String getApiDomain_NoSlash_GetResources() = SOCKET_SERVER_2;


    //    public static String API_AVATAR = getApiDomain_NoSlash_GetResources();
    public static String API_LOGOUT = "users/logout";
    public static String API_GET_USER_INFO = "users/";
    public static String API_REGIST = "users/regist";
    public static String API_REGIST_CHECK = "users/registCheck";
    public static String API_LOGIN = "users/login";
    public static String API_WEIXINLOGIN = "users/weixinlogin";
    public static String API_GET = "groups/";
    public static String API2_GET_2 = "/messages";
    public static String API_SEND = "events/message";
    public static String API_SEND_IMAGE_DIRECTROOM = "events/image";
    public static String API_SEND_TOPIC = "topics/post";

    public static String API_FORWARD_TOPIC = "topics/";
    public static String API2_FORWARD_TOPIC_2 = "/forward";

    public static String API_GET_TOPIC_COMMENT = "topics/";
    public static String API2_GET_TOPIC_COMMENT_2 = "/comment";
    public static String API_SEND_TOPIC_COMMENT = "topics/";
    public static String API2_SEND_TOPIC_COMMENT_2 = "/comment";

    public static String API_CREATE_DIRECT_CHAT_ROOM = "groups/directChat";

    public static String API_CREATE_ROOM = "groups";
    public static String API_CREATE_COMPANY = "companies";

    public static String API_COMPANY_INFO = "companies/";
    public static String API_COMPANY_INFO_UPDATE = "companies/";
    public static String API_COMPANY_GROUP_LIST = "companies/";
    public static String API2_COMPANY_GROUP_LIST_2 = "/groups";

    public static String API_GENERATE_COMPANY_INVITE_CODE = "companies/";
    public static String API2_GENERATE_COMPANY_INVITE_CODE_2 = "/invite";

    public static String API_JOIN_GROUP = "groups/";
    public static String API2_JOIN_GROUP_2 = "/join";
    public static String API_JOIN_COMPANY = "users/company/";
    public static String API_JOIN_COMPANY_WITH_INVITE_CODE = "companies/join/";

    public static String API_SET_GROUP_PRIVATE = "groups/";
    public static String API2_SET_GROUP_PRIVATE_2 = "/setPrivate";

    public static String API_LEAVE_GROUP = "groups/";
    public static String API2_LEAVE_GROUP_2 = "/leave";

    public static String API_GROUP_USERS = "groups/";
    public static String API2_GROUP_USERS_2 = "/users";
    public static String API_GROUP_USERS_INVITE = "groups/";
    public static String API2_GROUP_USERS_INVITE_2 = "/invite";

    public static String API2_GROUP_USERS_KICK_2 = "/kick";
    public static String API_GROUP_USERS_KICK_GROUP = "groups/";

    public static String API_GROUP_CAN_JOIN = "groups/canJoin";
    public static String API_COMPANY_CAN_JOIN = "companies/canJoin";

    public static String API_COMPANY_USER_LIST = "companies/";
    public static String API2_COMPANY_USER_LIST_2 = "/users";

    public static String API_USER_ROOMS = "groups";
    public static String API_USER_COMPANIES = "users/companies";
    public static String API_SCHOOL_CONTACT_CLASS = "addressBooks";
    public static String API_SCHOOL_EVERY_CLASS_CONTACT = "addressBooks/";
    public static String API_SCHOOL_EVERY_CLASS_CONTACT_2 = "/users";

    public static String API_COMPANY_SETLOGO = "companies/";
    public static String API2_COMPANY_SETLOGO_2 = "/logo";

    public static String API_GROUP_SETGROUPTOP ="groups/";//小组置顶接口
    public static String API_GROUP_SETGROUPTOP_2 ="/setTop";

    public static String API_GROUP_SET_LIVE ="groups/";//小组直播
    public static String API_GROUP_SET_LIVE_2 ="/toggleBroad";

    public static String API_USER_PUSH_TOKEN = "users/setpushToken";

    public static String API_SET_CURRENT_COMPANIES = "users/currentCompany/";
    public static String API_GET_CURRENT_COMPANIES = "users/currentCompany";

    public static String API_SUBJECT = "topics";

    public static String API_SET_AVATAR = "users/avatar";
    public static String API_UPLOAD_FILE = "files/upload";
    public static String API_UPLOAD_IMAGE = "images";

    public static String API_UPLOAD_SOUND = "events/audio";

    public static String API_GROUP_SETLOGO = "groups/";
    public static String API2_GROUP_SETLOGO_2 = "/logo";
    public static String API_GROUP_INFO = "groups/";
    public static String API2_GROUP_INFO_NOTIFY_SETTING_2 = "/notification";

    public static String API_SUBJECT_VIDEO = "topics/video";
    public static String API_SUBJECT_FILE = "topics/file";
    public static String API_SUBJECT_IMAGE = "topics/image";
    public static String API_SUBJECT_MAP = "topics/map";
    public static String API_SUBJECT_VOTE = "topics/vote";
    public static String API2_SUBJECT_VOTE_CLOSE_2 = "/close";

    public static String API_SUBJECT_ACTIVIY = "topics/activity";
    public static String API2_SUBJECT_ACTIVIY_CLOSE = "/close";

    public static String API_SUBJECT_DETAIL = "topics/";

    public static String API_RESET_PASSWORD = "users/resetPassword";
    public static String API_USER_UPDATE = "users/update";
    public static String API_USER_INFO = "users/";
    public static String API_USER_SETNOTIFICATION = "users/notification";
    public static String API_USER_RECEIVE_NOTIFICATION = "messages/userMessages";
    public static String API_USER_SETMASSAGEREADED = "messages/setreaded";
    public static String API_USER_ALUM_CARD = "users/qrcode";

    public static String API_SMS_SIGNUP = "sms/signup";
    public static String API_SMS_RESET_PASSWORD = "sms/forgotPassword";

    public static String API_GROUP_RESET_UNREAD = "groups/";
    public static String API2_GROUP_RESET_UNREAD_2 = "/resetunread";

    public static String API_FAVORITY_LIST = "users/favorites";
    public static String API_FAVORITY_ADD = "users/favorite/";

    public static String API_TOPIC_DEL = "topics/del/";

    public static String API_GROUP_OWNER_TRANCEFER = "groups/";

    public static String API_GROUP_DELETE_GROUP = "groups/delete/";
    public static String API_GROUP_DELETE_DIRECTCHAT = "groups/directchat/del/";

    public static String API_COMPANY_TRANCEFER = "companies/";
    public static String API_COMPANY_DELETE_COMPANY = "companies/del/";
    public static String API_COMPANY_LEAVE = "companies/";
    public static String API2_LEAVE_COMPANY_2 = "/leave";

    public static String API_GROUP_INFO_UPDATE = "groups/";
    public static String API_DEVICE_SET_NOTIFICATION_LEVEL = "devices/notification";

    public static String API_VERSION = "versions";
    public static String API_APPDOWNLOADURL = "androidDownloadPage";

    public static String API_COURSES_BY_SLOT = "courses/byDateSlot";
    public static String API_GET_PRIVILEGES = "users/privileges";

    public static String API_COURSES_BY_DATE = "courses/byDate";
    public static String API_SREACH_SCHOOL_CONTACT = "addressBooks/searchUser";
    public static String API_MADE_IN_ORDER = "companySettings";//定制化的接口

    /**注册时提交标签的接口*/
    public static String API_REGISTER_LABEL = "follows/setTag";
    public static String API_GET_REGISTER_LABEL = "follows/tags";
    /**
     * 提交反馈
     */
    public static String API_SETTING_FEEDBACK = "feedbacks";

    /**关于聊天室的接口*/
    public static String API_GROUPS_CHATROOMS = "groups/chatRooms";
    public static String API2_CHAT_ROOM_JOIN_2 = "/join";
    public static String API2_CHAT_ROOM_LEAVE_2  = "/leaveChatRoom";






    /**
     * 获取当前通讯录
     */
    public static String API_ADDRESSBOOKS_CURRENTYEARUSER = "addressBooks/currentYearUser";
    public static String API_ADDRESSBOOKS_CURRENTYEAR = "addressBooks/currentYear";
    public static String API_CREATE_CHECKIN = "checkin";//签到的接口
    public static String API_CHOOSE_GROUP = "companies/2/groups?category=6";//发通知时获取全部小组借口
    public static String API_CREATE_MESSAGE = "messages";//发通知的接口

    /**
     * 朋友圈的相关接口
     */
    public static String API_GET_DATA_OF_FRIEND = "moments";//获取朋友圈数据
    public static String API_CHANGE_FRIEND_LIKE = "moments/";
    public static String API_CHANGE_FRIEND_LIKE2 = "/like";
    public static String API_DEL_FRIEND = "/del";
    public static String API_COMMENT_FRIEND = "/comment";
    public static String API_GET_DATA_OF_FRIEND_WITH_HEAD = "profilePicture";
    public static String API_GET_UNREAD_MOMENT = "moments/newMomentCount";
    public static String API_SIGN_FRIEND_CRICLE_MOVE_TIME = "moments/moveCursor";
    public static String API_GET_NEW_DATA_SIGN = "moments/newCommentCount";
    public static String API_NEW_COMMNET_LIST = "moments/newComment";
    public static String API_MOVE_NEW_COMMENT = "moments/moveCommentCursor";
    public static String API_IN_OR_OUT_CRICLE = "moments/toggleReading";//切换朋友圈reading状态
    public static String API_GET_USERIF_DATA = "moments/self";

    public interface IF_HTTP_API {
        @GET("groups/{id}/messages")
        Call<JsonNewsGroupRet> getGroupNews(@Path("id") int id, @Query("cursor") String createdAt, @Query("limit") int limit);

        @GET("moments")
        Call<JsonFriendCricleRet> getFriengMsg(@Query("page") int page);

        @GET("groups/{id}/images")
        Call<JsonGetGroupAlbumRet> getGroupAlbum(@Path("id") int id, @Query("createdAt") String createdAt, @Query("limit") int limit);

        @GET("courses/byDate")
        Call<JsonGetCompanyUserListRet> getCourseListByDate(@Query("date") String date);

        @GET("companies/{id}/users")
        Call<JsonGetCompanyUserListRet> getContacts(@Path("id") String id);

        @GET("follows")
        Call<JsonGetCompanyUserListRet> getContact();

        @GET("addressBooks/{id}/users")
        Call<JsonGetGroupUserRet> getGroupContacts(@Path("id") String id);

        @POST("events/readAudio/{id}")
        Call<JsonMarkAudioReadedRet> markAudioReaded(@Path("id") String id);

        @POST("events/del/{id}")
        Call<JsonDeleteMsgRet> deleteMsg(@Path("id") String id);

        @POST("users/update")
        Call<JsonUpdatePersonInfoRet> registFromWeinxin(@Body JsonUpdatePersonInfo jsonUpdatePersonInfo);

        @POST("sms/updatePhone")
        Observable<JsonSmsValidCodeUpdatePhoneRet> smsValidCodeUpdatePhone(@Body JsonSmsValidCodeUpdatePhone jsonSmsValidCodeUpdatePhone);

    }


    public static int rtm_status = -1;//0: 未连接，1：连接但未认证，2：连接并且认证成功

    public static Map<String, Map<String, String>> errorCodeMessages;

    public static String getErrorMsg(String error_code) {

        if (errorCodeMessages == null) {

            getErrorCodeMessage();

            return "";
        }

        /**
         * 输入
         *  error code
         *  当前语言设置
         *  errorCodeMessages
         * 输出
         *  对应code及语言的字符串
         */
        try {

            MyLog.d("", "new error code " + app_lang + " =: " + errorCodeMessages.get(error_code).get("zh").toString());
            if (app_lang == APP_LANG_CN) {
                return errorCodeMessages.get(error_code).get("zh").toString();
            } else if (app_lang == APP_LANG_EN) {
                return errorCodeMessages.get(error_code).get("en").toString();
            } else {
                return errorCodeMessages.get(error_code).get("zh").toString();
            }
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * UI setting
     */
    public static final int PLUS_POPUP_MENU = 1;
    public static final int PLUS_DIALOG = 2;
    public static final int PLUS_MYDIALOG = 3;
    public static final int PLUS_UNIVERSAL = 4;
    //public static final int plusInvokeMode = PLUS_POPUP_MENU;
//    public static final int plusInvokeMode = PLUS_DIALOG;
//    public static final int plusInvokeMode = PLUS_MYDIALOG;
    public static final int plusInvokeMode = PLUS_UNIVERSAL;
    public static final int AVATAR_RADIUS = 10; //px
    public static final int AVATAR_RESIZE = 120; //px

    public static final int UNREAD_MAX = 99;//dp


    /**
     * media
     */
    public static final int MEDIAPLAYER_NOT_INITIAL = 0;
    public static final int MEDIAPLAYER_IS_PREPARING = 1;
    public static final int MEDIAPLAYER_IS_PLAYING = 2;
    public static final int MEDIAPLAYER_IS_READY = 3;

    public static final String tempSoundFile = "sound.m4a";
    public static final String tempVideoFile = "video.mp4";


    /**
     * Animator setting
     */
    public static final int ANIM_MAINROOM_UNREAD_FLASH_COUNT = 2;//repeat count
    public static final int ANIM_MAINROOM_UNREAD_FLASH_DURATION = 800;//flash duration ms

    /**
     * ============ 服务器端的消息分类 ===============
     */
    //以下是普通消息类
    public static final int TYPE_NEWS = 18;
    public static final int TYPE_MSG = 1;
    public static final int TYPE_SOUND = 15;
    public static final int TYPE_NOTIFICATION = 16;
    public static final int TYPE_GROUP_LIST_UPDATED = 17;

    //以下是控制类
    public static final int TYPE_CREATE = 2;
    public static final int TYPE_JOIN = 3;
    public static final int TYPE_LEAVE = 4;
    public static final int TYPE_KICK = 5;

    //以下是topic主题类及其子类
    public static final int TYPE_TOPIC = 7;
    public static final int SUBTYPE_FILE = 1;
    public static final int SUBTYPE_IMAGE = 2;
    public static final int SUBTYPE_MAP = 3;
    public static final int SUBTYPE_VOTE = 4;
    public static final int SUBTYPE_TEXT = 5;
    public static final int SUBTYPE_VOTE_CLOSE = 6;
    public static final int SUBTYPE_LINK = 7;
    public static final int SUBTYPE_ACTIVITY = 8;
    public static final int SUBTYPE_ACTIVITY_CLOSE = 9;
    public static final int SUBTYPE_VIDEO = 10;

    //以下是comment评论类及其子类
    public static final int TYPE_TOPIC_COMMANT = 8;
    public static final int SUBTYPE_TEXT_COMMENT = 1;
    public static final int SUBTYPE_IMAGE_COMMENT = 2;
    public static final int SUBTYPE_LINK_COMMENT = 4;
    public static final int SUBTYPE_SOUND_COMMENT = 5;

    //以下是私信专有图片及地图类
    public static final int TYPE_TOPIC_IMAGE_DIRECTROOM = 12;
    public static final int TYPE_TOPIC_MAP_DIRECTROOM = 13;
    //朋友圈的类型
    public static final int TYPE_FRIEND_CRICLE = 19;


    /**
     * ============= 客户端消息分类 ===================
     */
    public static final int LEVEL_MSG_0 = 0;//main room root msg
    public static final int LEVEL_MSG_1 = 1;//main room normal msg and subject or say topic
    public static final int LEVEL_MSG_2 = 2;//sub room subject reply, or say topic comment
    public static final int LEVEL_ROOM_MAIN = 1;//main room
    public static final int LEVEL_ROOM_SUB = 2;//sub room
    public static final int LEVEL_ROOM_PRIVATE = 3;//private room

    public static final int MSG_CATEGORY_ROOT = 0;
    public static final int MSG_CATEGORY_NORMAL = 1;
    public static final int MSG_CATEGORY_SUBJECT = 2;
    public static final int MSG_CATEGORY_SUBJECT_COMMENT = 3;
    public static final int MSG_CATEGORY_EVENT = 4;
    public static final int MSG_CATEGORY_PRIVATE = 5;


    /**
     *
     */
    public static IMsg mainRoomRootNode;
    public static IMsg subRoomRootNode;
    public static IMsg privateRoomRootNode;


    public static final String EXT_JPG = ".jpg";

    public static final String MIME_FILE_PREFIX = "file/";
    public static final String MIME_FILE_STAR = "file/*";
    public static final String MIME_TEXT_PREFIX = "text/";
    public static final String MIME_TEXT_STAR = "text/*";
    public static final String MIME_IMAGE_PREFIX = "image/";
    public static final String MIME_IMAGE_STAR = "image/*";
    public static final String MIME_VIDEO_PREFIX = "video/";
    public static final String MIME_VIDEO_STAR = "video/*";
    public static final String MIME_AUDIO_PREFIX = "audio/";
    public static final String MIME_AUDIO_STAR = "audio/*";

    public static final MediaType MEDIA_TYPE_MARKDOWN = MediaType.parse("text/x-markdown; charset=utf-8");
    public static final MediaType MEDIA_TYPE_IMAGE = MediaType.parse("image/jpeg");


    /**
     * vote
     */
    public static Vote tempVote;
    public static IMsg imsgVoteResult;
    public static int VOTE_OPT_MAX = 10;
    public static int VOTE_OPT_MIN = 2;

    /**
     * acticity报名
     */
    public static MActivity tempActivity;

    /**
     * map
     */
    public static MapLocation tempMapLocation;

//
//    public static void sendSysMsg(IMsg imsLocalSubject, String text) {
//
//        /**
//         * TODO: 这里应该改成更加通用的样式，最好使用eventbus发送，避免在activity或view层级之间切换
//         */
//
//        IMsg sys_msg = new IMsg(
//                MyConfig.USER_INDEX_ADMIN,
//                "admin",
//                null,
//
//                Calendar.getInstance().getTime(),
//
//                imsLocalSubject.level + 1,
//                imsLocalSubject,
//                MyConfig.MSG_CATEGORY_SUBJECT,
//
//                -1,
//                -1,
//                null,
//
//                MyConfig.app.getResources().getString(R.string.sys_notice),
//                text
//        );
//
//        imsLocalSubject.ll_sub_msg.add(sys_msg);
//        imsLocalSubject.la_msg.notifyDataSetChanged();
//
//    }


    public static void buildTimeShowString(LinkedList<IMsg> ll_sub_msg) {

        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(MyConfig.app.getString(R.string.time_yyyy_mm_dd_week));

        String today = sdf.format(date);

        date.setTime(date.getTime() - 24 * 60 * 60 * 1000);//昨天
        String yestoday = sdf.format(date);

        String last = null;
        for (IMsg iMsg : ll_sub_msg) {

            /**
             * 主聊天室时间分割线
             */
            if (iMsg.time_date.equals(last)) {
                // last day msg
                iMsg.first_in_a_day = false;
            } else {
                // a new day msg
                iMsg.first_in_a_day = true;
                last = iMsg.time_date;
                if (iMsg.time_date.equals(today)) {
                    iMsg.time_date_show = MyConfig.app.getString(R.string.time_string_today);
                } else if (iMsg.time_date.equals(yestoday)) {
                    iMsg.time_date_show = MyConfig.app.getString(R.string.time_string_yesterday);
                } else {
                    iMsg.time_date_show = iMsg.time_date;
                }
            }


            /**
             *  子聊天室时间
             */
            if (iMsg.time_date.equals(today)) {
                iMsg.time_subroom = iMsg.time_ampm;
            } else {

            }

        }
    }

    public static String getCurrentTimeStr() {
        Date today = Calendar.getInstance().getTime();
        Format formatter = new SimpleDateFormat(MyConfig.app.getString(R.string.time_yyyymmddhhmmss));
        return formatter.format(today);
    }

    public static String Date2UTCString(Date date) {
        Format formatter = new SimpleDateFormat(MyConfig.app.getString(R.string.time_utc));
        return formatter.format(date);
    }

    /**
     * 返回值格式是：yy/M/d
     *
     * @param date
     * @return
     */
    public static String Date2GroupInfoTime(Date date) {
        if (date == null) {
            return null;
        }
        Format formatter = new SimpleDateFormat(MyConfig.app.getString(R.string.time_yy_mm_dd));
        return formatter.format(date);
    }

    /**
     * 返回值格式是：yyyy-MM-dd
     *
     * @param date
     * @return
     */
    public static String Date2GroupInfoTimehorizon(Date date) {
        if (date == null) {
            return null;
        }
        Format formatter = new SimpleDateFormat(MyConfig.app.getString(R.string.time_yyyy_mm_dd));
        return formatter.format(date);
    }

    /**
     * 返回值格式是：HH:mm
     *
     * @param date
     * @return
     */
    public static String Date2GroupInfoTimeToday(Date date) {
        if (date == null) {
            return null;
        }
        Format formatter = new SimpleDateFormat(MyConfig.app.getString(R.string.time_HH_mm));
        return formatter.format(date);
    }

 /**
     * 返回值格式是：yyyy-MM-dd HH:mm
     *
     * @param date
     * @return
     */
    public static String Date2StringDateAndTime(Date date) {
        return Date2GroupInfoTimehorizon(date)+" "+Date2GroupInfoTimeToday(date);
    }


    public static String Date2GroupInfoTimeYestoday(Date date) {
        return app.getString(R.string.yesterday);
    }

    public static Date UTCString2Date(String time) {
        Date date;
//        MyLog.d("", "UTCString2Date from " + time);

        SimpleDateFormat df = new SimpleDateFormat(MyConfig.app.getString(R.string.time_utc));
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            date = df.parse(time);
//            MyLog.d("", "UTCString2Date trans as " + date.toString());
        } catch (Exception e) {
            date = null;
//            MyLog.d("", "UTCString2Date get null");
        }
        return date;
    }

    /**
     * 由2016-06-20T22:10:00.000Z转换为date,
     * @param time
     * @return
     */
    public static Date String2DateUTC0(String time) {
        Date date;

        SimpleDateFormat df = new SimpleDateFormat(MyConfig.app.getString(R.string.time_utc));
        //df.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        try {
            date = df.parse(time);
        } catch (Exception e) {
            date = null;
        }
        return date;
    }
 /**
     * 由2016-06-20 22:10转换为date,
     * @param time
     * @return
     */
    public static Date string_yyyy_MM_dd_HH_mm2Date(String time) {
        Date date;

        SimpleDateFormat df = new SimpleDateFormat(MyConfig.app.getString(R.string.time_yyyy_MM_dd_HH_mm));
        //df.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        try {
            date = df.parse(time);
        } catch (Exception e) {
            date = null;
        }
        return date;
    }

    /**
     * 得到报名时上传服务器的时间格式
     */
    public static String getServerDateString(int year, int month, int day, int hour, int minute) {
        String time = showDateFormatter(year) + "-" + showDateFormatter(month + 1) + "-" + showDateFormatter(day)
                + "  " + showDateFormatter(hour) + ":" + showDateFormatter(minute);
        return time;
    }

    /**
     * 得到报名显示在本地的时间格式,并带有周
     */
    public static String getLocalDateStringWithWeek(int year, int month, int day,  int hour, int minute) {
        String time =  showDateFormatter(month + 1) + "/" + showDateFormatter(day)+ "(" +getWeek(new LocalDate(year, month+1, day).getDayOfWeek())
                + ")" + showDateFormatter(hour) + ":" + showDateFormatter(minute);
        return time;
    }
    /**
     * 得到报名显示在本地的时间格式,并带有周。传递的参数是Date
     */
    public static String getLocalDateStringWithWeek(Date date) {

        if(date == null){
           return "未指定活动结束时间";
        }
        LocalDate localDate = new LocalDate(date.getTime());
        LocalTime localTime = new LocalTime(date.getTime());
        int year = localDate.getYear();
        int month = localDate.getMonthOfYear();
        int day = localDate.getDayOfMonth();
        int hour = localTime.getHourOfDay();
        int minute = localTime.getMinuteOfHour();

        String dateAndTime =  showDateFormatter(month ) + "/" + showDateFormatter(day)+ " " +getWeek(new LocalDate(year, month, day).getDayOfWeek())
                + " " + showDateFormatter(hour) + ":" + showDateFormatter(minute);
        return dateAndTime;
    }

    //朋友圈中的时间格式
    public static String getTime(String str){
        Date date = Calendar.getInstance().getTime();
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        date.setTime(date.getTime() - 24 * 60 * 60 * 1000);//昨天
        String yestoday = formatter.format(date);

        String timeStr = formatter.format(new Date());
        String timeMsg = MyConfig.Date2GroupInfoTimehorizon(MyConfig.UTCString2Date(str));
        if(timeStr.equals(timeMsg)){
            if(MyConfig.time_interval(str)!=null) {
                if(MyConfig.time_interval(str).contains("h")){
                    return MyConfig.time_interval(str).substring(0,MyConfig.time_interval(str).length()-1)+"小时前";
                }else{

                    return MyConfig.time_interval(str);
                }

            }else {
                return MyConfig.Date2GroupInfoTimeToday(MyConfig.UTCString2Date(str));
            }
        }else if(yestoday.equals(timeMsg)){
            return "昨天";

        } else{
            Format format = new SimpleDateFormat("M月d日");
            String time_Str = format.format(MyConfig.UTCString2Date(str));
            return time_Str;
        }
    }

    public static String time_interval(String str){
        //时间差
        Date date = Calendar.getInstance().getTime();
        Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String timestr = format.format(date);
        try {
            Date  date2 = formatter.parse(MyConfig.Date2StringDateAndTime(MyConfig.UTCString2Date(str)));
            Date  date1 = formatter.parse(timestr);

        long times= date1.getTime() -date2.getTime();//这样得到的差值是微秒级别
        long days = times/ (1000 * 60* 60 * 24); //换算成天数
        long hours =(times-days*(1000 * 60 * 60 * 24))/(1000* 60 * 60); //换算成小时
        long minutes =(times-days*(1000 * 60 * 60 * 24)-hours*(1000* 60 * 60))/(1000* 60); //换算成分钟
            if(hours>0){
                return hours+"h";
            }else {
                if(minutes>0) {
                    return minutes + "分钟前";
                }else{
                    return "刚刚";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    private static String getWeek(int mDayOfWeek){
        String s = "";
        switch (mDayOfWeek){
            case 1:
                s = "星期一";
                break;
            case 2:
                s = "星期二";
                break;
            case 3:
                s = "星期三";
                break;
            case 4:
                s = "星期四";
                break;
            case 5:
                s = "星期五";
                break;
            case 6:
                s = "星期六";
                break;
            case 7:
                s = "星期日";
                break;
        }
        return s;
    }
    /**
     * 格式化日期,让月、日、时、分，都显示两位
     *
     * @param i
     * @return
     */
    private static String showDateFormatter(int i) {
        return i < 10 ? "0" + i : "" + i;
    }


    public static String formClientCheckId(int groupId) {

        String s;

        try {
            /**
             * todo
             * new SimpleDataFormat一行发生空指针异常，怀疑是MyConfig.app为空，也即viewpager没有启动
             * 赶时间，这里临时处理一下
             */
            StringBuilder sb = new StringBuilder();
            sb.append(usr_id);
            sb.append("_" + groupId);

            Date today = Calendar.getInstance().getTime();
            Format formatter = new SimpleDateFormat(MyConfig.app.getString(R.string.time_yyyymmddhhmmssssss));

            sb.append("_" + formatter.format(today));

            s = sb.toString();
        } catch (Exception e) {
            s = "" + Math.random();
        }
        return s;
    }


    public static int REPEAT_CHECK_COUNT_MAX = 10;

    public static int searchRepeatMsgInList(LinkedList<IMsg> list, IMsg iMsg, int maxSearchCount) {

        /**
         * maxSearchCount：
         *  -1或0，顺序检查整个list
         *  其他，顺序检查list尾部『list.size() - maxSearchCount』个元素
         *
         * 返回：
         *  -1，没找到
         *  其他，返回index，也即 0 ~ list.size-1
         *
         * 什么情况下需要做这个检查？
         *  在新msg替换旧msg的时候
         *  重复接收socket的时候
         */

        int bypassCount;

        if (maxSearchCount <= 0) {
            bypassCount = 0;
        } else {
            bypassCount = list.size() - maxSearchCount;
            if (bypassCount < 0) {
                bypassCount = 0;
            }
        }

        int count = 0;
        for (IMsg iMsgInList : list) {

            count++;

            if (count <= bypassCount) {
                continue;
            }

            /**
             * 怎样界定一条msg？
             *  根据客户端打的唯一tag：clientId。但如果是服务器产生的msg则没有这个tag。另外客户端出错时可能没有这个tag
             *  根据服务器打的唯一tag：createAt。但如果是客户端先发送到本地list的msg里面没有这个tag。可以假定服务器不会错误滴遗失这个tag
             *
             * 判断步骤
             *  先判断clientId是否重复
             *  如果没有clientId，再判断createAt
             */

            if ((iMsgInList.clientId != null) && (iMsg.clientId != null)) {//some msg donot have clientId
                if (iMsgInList.clientId.equals(iMsg.clientId)) {
                    return count - 1;   //return index
                }
            } else if ((iMsgInList.server_tag_id != null) && (iMsg.server_tag_id != null)) {
                if (iMsgInList.server_tag_id.equals(iMsg.server_tag_id)) {
                    return count - 1;   //return index
                }
            }

        }

        return -1;
    }


    public static int searchNextMsgInList(LinkedList<IMsg> list, IMsg iMsg) {

        int index = searchRepeatMsgInList(list, iMsg, -1);

        if (index == -1) {//没找到
            return -1;
        } else if (index == list.size() - 1) {//最后一条
            return -1;
        } else {
            return index + 1;
        }

    }

    public static boolean isSoundIMsg(IMsg iMsg) {

        if (iMsg.type == MyConfig.TYPE_SOUND ||
                (iMsg.type == MyConfig.TYPE_TOPIC_COMMANT && iMsg.subType == MyConfig.SUBTYPE_SOUND_COMMENT)) {

            return true;

        } else {
            return false;
        }

    }

    public static void setSoundIMsgReadedForSocketInput(IMsg iMsg) {

        if (isSoundIMsg(iMsg)) {

            if (iMsg.user_id == MyConfig.usr_id) {
                iMsg.readed = true;
            }
        }

    }

//
//    /**
//     * pic show level
//     * 1, thumbnail
//     * 2, preview
//     * 3, full screen
//     */
//
//    public static final int MSG_PHOTO_SHOW_THUMBNAIL = 1;
//    public static final int MSG_PHOTO_SHOW_PREVIEW = 2;
//    public static final int msg_photo_show_mode = MSG_PHOTO_SHOW_THUMBNAIL;


    public static File getCameraFileBigPicture() {
        return new File(MyConfig.appCacheDirPath, MyConfig.getCurrentTimeStr() + "_" + MyConfig.usr_id + MyConfig.EXT_JPG);
    }

    public static void imageViewShowWithLimit(ImageView iv, File lrf, int limitPx) {

        Bitmap bit;
        int orientation;
        int scale;

        /**
         * 第一步 不真正读回完整的bitmap，因为可能超过内存大小 取回长宽比例，看看fit into screen的话需要缩小多少
         */
        BitmapFactory.Options opts = new BitmapFactory.Options();

        opts.inJustDecodeBounds = true;
        bit = BitmapFactory.decodeFile(lrf.getAbsolutePath(), opts);

        if (opts.outHeight == opts.outWidth) {
            orientation = 0;
            iv.getLayoutParams().height = limitPx;
            iv.getLayoutParams().width = limitPx;
            scale = opts.outHeight / limitPx;
        } else if (opts.outHeight > opts.outWidth) {
            orientation = 1;
            iv.getLayoutParams().height = limitPx;
            iv.getLayoutParams().width = limitPx * opts.outWidth / opts.outHeight;
            scale = opts.outHeight / limitPx;
        } else {
            orientation = 2;
            iv.getLayoutParams().width = limitPx;
            iv.getLayoutParams().height = limitPx * opts.outHeight / opts.outWidth;
            scale = opts.outWidth / limitPx;
        }

        /**
         * 第二步 按需要的比例进行压缩
         */
        opts.inJustDecodeBounds = false;
        opts.inSampleSize = scale;
        bit = BitmapFactory.decodeFile(lrf.getAbsolutePath(), opts);

        iv.setImageBitmap(bit);
    }

    /**
     * this preview size should be adjust carefully to avoid out of memory(OOM)
     */
    public static int MSG_PHOTO_PREVIEW_WIDTH = 600;
    public static int MSG_PHOTO_PREVIEW_HEIGHT = 600;

    public static void imageViewShow(ImageView iv, File lrf) {

        Bitmap bit;

        /**
         * 第一步 不真正读回完整的bitmap，因为可能超过内存大小 取回长宽比例，看看fit into screen的话需要缩小多少
         */
        BitmapFactory.Options opts = new BitmapFactory.Options();

        opts.inJustDecodeBounds = true;
        bit = BitmapFactory.decodeFile(lrf.getAbsolutePath(), opts);
        int scaleX = opts.outWidth / MyConfig.MSG_PHOTO_PREVIEW_WIDTH;
        int scaleY = opts.outHeight / MyConfig.MSG_PHOTO_PREVIEW_HEIGHT;
        // int scaleX = opts.outWidth / iv.getLayoutParams().width;
        // int scaleY = opts.outHeight / iv.getLayoutParams().height;

        /**
         * 按长边缩放，还是按短边缩放？
         * 有选项比较好
         */
        int scale;
        scale = scaleX >= scaleY ? scaleX : scaleY; //按长边
//          scale = scaleX >= scaleY ? scaleY : scaleX;//按短边
        MyLog.d("", "imageViewShow " + opts.outWidth + " " + MyConfig.MSG_PHOTO_PREVIEW_WIDTH + " " + opts.outHeight + " " + MyConfig.MSG_PHOTO_PREVIEW_HEIGHT + " " + scale);


        /**
         * 第二步 按需要的比例进行压缩
         */
        opts.inJustDecodeBounds = false;
        opts.inSampleSize = scale;
        bit = BitmapFactory.decodeFile(lrf.getAbsolutePath(), opts);
        iv.setImageBitmap(bit);
    }

    public static Bitmap decodeSampledBitmapFromFile(int reqWidth,
                                                     int reqHeight, File lrf) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(lrf.getAbsolutePath(), options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(lrf.getAbsolutePath(),
                options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static int qualityBitmap2File = 85;

    public static void saveBitmap2File(Bitmap bmp, File file) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, qualityBitmap2File, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getFilenameFromPath(String path) {

        if (path == null) {
            return null;
        }

        if (path.lastIndexOf("/") != -1 && path.lastIndexOf("/") != 0) {
            return path.substring(path.lastIndexOf("/") + 1);
        } else {
            return "";
        }
    }

    public static String getExtFromPathOrName(String fileName) {

        if (fileName == null) {
            return null;
        }

        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    /**
     * following method to get real path in a uri is come from:
     *  http://stackoverflow.com/questions/20067508/get-real-path-from-uri-android-kitkat-new-storage-access-framework/20559175#20559175
     *  https://github.com/iPaulPro/aFileChooser
     */

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @SuppressLint("NewApi")
    public static String getUriPath(final Context context, final Uri uri) {

        if (uri == null) {
            return null;
        }

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    public static void uploadUserAvatar(final File f) {
        MyLog.d("", "OKHTTP: uploadUserAvatar() : " + f.getAbsolutePath());


        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonUploadFileRet jsonUploadFileRet = HttpTools.okhttpUploadFile(f, "image/jpeg");

                if (jsonUploadFileRet != null && jsonUploadFileRet.code == MyConfig.retSuccess()) {
                    final String path = jsonUploadFileRet.data.path;

                    JsonSetAvatar jsonSetAvatar = new JsonSetAvatar();
                    jsonSetAvatar.accessToken = MyConfig.usr_token;
                    jsonSetAvatar.avatar = path;

                    Gson gson = new Gson();
                    String jsonStr = gson.toJson(jsonSetAvatar, JsonSetAvatar.class);

                    final String ss = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SET_AVATAR, jsonStr);

                    final JsonSetAvatarRet jsonSetAvatarRet = gson.fromJson(ss, JsonSetAvatarRet.class);
                    MyLog.d("", "OKHTTP set avatar: " + ss);
                    if (jsonSetAvatarRet != null && jsonSetAvatarRet.code == MyConfig.retSuccess()) {

                        MyConfig.usr_avatar = path;
                        MyConfig.prefEditor.putString(MyConfig.PREF_USR_AVATAR, MyConfig.usr_avatar);
                        MyConfig.prefEditor.commit();

                        EventBus.getDefault().post(new EventUserStatusUpdate(MyConfig.usr_status));

                    }

                } else {

                }

            }
        }).start();
    }


    public static void gotoUserInfo(final Context context, final int user_id) {
        Intent intent = new Intent(context, ActivityUserInfo.class);
        intent.putExtra("user_id", user_id);
        context.startActivity(intent);
    }

    public static String trimAndTakeAwayStringFromEditText(EditText et) {
        String str = et.getText().toString().trim();
        et.setText(str);

        if (str.length() == 0) {
            return null;
        } else {
            et.setText("");
            return str;
        }
    }


//    public static void hideSoftKeyboard(Activity activity, View view) {
////        MyLog.d("", "hideSoftKeyboard");
//        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
//    }

    /**
     * 隐藏软键盘
     *
     * @param activity
     */
    public static void hide_keyboard_must_call_from_activity(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 弹出软键盘
     * 注意：1，showSoftInput方法中参数是一个View对象，但这个View对象必须获取焦点才能有效，所以必须获取带焦点的View对象。直接传递EditText也是可以的，其他的没有测试过。
     * 2，如果是页面刚刚加载可能存在控件还没有获取焦点的情况，所以sleep100ms；
     *
     * @param activity
     */
    public static void show_keyboard_must_call_from_activity(final Activity activity) {
        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                View view = activity.getCurrentFocus();
                //If no view currently has focus, create a new one, just so we can grab a window token from it
                if (view == null) {
                    view = new View(activity);
                }
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                inputMethodManager.showSoftInput(view, 0);
            }
        }.start();

    }

    /**
     * 当确定为某个EditText弹出键盘时用这个方法即可，而上述方法当EditText在dialog上时就不能弹出键盘了
     * @param activity
     * @param et
     */
    public static void show_keyboard_must_call_from_editText(final Activity activity, final EditText et) {
        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(et, 0);
            }
        }.start();
    }

    public interface ISnackBarListener {
        void doListener();
    }


    public static void snackbarTop(View v, String msg, int backgroundColor, int time, final ISnackBarListener iSnackBarListener) {
        Snackbar snack = Snackbar.make(v, msg, time);

        Snackbar.SnackbarLayout view = (Snackbar.SnackbarLayout) snack.getView();
        view.setBackgroundResource(backgroundColor);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.TOP;
        TextView textView = (TextView) view.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.BLACK);
        TextView actionText = (TextView) view.findViewById(R.id.snackbar_action);
        actionText.setBackgroundResource(R.drawable.ic_action_next_item);
        actionText.setHeight(20);
        actionText.setWidth(20);
        actionText.setVisibility(View.VISIBLE);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iSnackBarListener.doListener();
            }
        });
        view.setLayoutParams(params);
        snack.show();
    }

    public static <V extends View> Collection<V> findChildrenByClass(ViewGroup viewGroup, Class<V> clazz) {

        return gatherChildrenByClass(viewGroup, clazz, new ArrayList<V>());
    }

    private static <V extends View> Collection<V> gatherChildrenByClass(ViewGroup viewGroup, Class<V> clazz, Collection<V> childrenFound) {

        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            final View child = viewGroup.getChildAt(i);
            if (clazz.isAssignableFrom(child.getClass())) {
                childrenFound.add((V) child);
            }
            if (child instanceof ViewGroup) {
                gatherChildrenByClass((ViewGroup) child, clazz, childrenFound);
            }
        }

        return childrenFound;
    }

    public static class MySearchView extends SearchView {
        public MySearchView(Context context) {
            super(context);

            this.setBackground(context.getResources().getDrawable(R.drawable.bg_searchview));
//            this.setBackgroundColor(Color.BLUE);

            for (TextView textView : findChildrenByClass(this, TextView.class)) {
//                textView.setTextColor(Color.WHITE);
//                textView.setBackgroundColor(Color.GREEN);
//                textView.setBackground(null);
            }
        }

        // The normal SearchView doesn't clear its search text when
        // collapsed, so we will do this for it.
        @Override
        public void onActionViewCollapsed() {
            MyLog.d("", "searchview: onActionViewCollapsed");

            setQuery("", false);
            super.onActionViewCollapsed();

        }

        @Override
        public void onActionViewExpanded() {
            MyLog.d("", "searchview: onActionViewExpanded");

            setQuery("", false);
            super.onActionViewExpanded();

        }
    }


    public static void companyListUpdateFromServer() {

        MyLog.d("", "updateCompanyInfoRefresh 2.1: companyListUpdateFromServer");

        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();

                /**
                 * 获取可以加入，但还没有加入的company
                 */
                String companies_can_join = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_CAN_JOIN, MyConfig.usr_token);
                MyLog.d("", "HttpTools: get company can join ret " + companies_can_join);
                JsonCompanyCanJoinRet jsonCompanyCanJoinRet = gson.fromJson(companies_can_join, JsonCompanyCanJoinRet.class);
                if (jsonCompanyCanJoinRet != null && jsonCompanyCanJoinRet.code == MyConfig.retSuccess()) {
                    MyConfig.jsonCompanyCanJoinRet = jsonCompanyCanJoinRet;
                }

                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("order[userCount]", "desc");
                hm.put("limit", "" + 10);

                String companies_can_join_sort = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_CAN_JOIN, hm, MyConfig.usr_token);
                JsonCompanyCanJoinRet jsonCompanyCanJoinRet_sort = gson.fromJson(companies_can_join_sort, JsonCompanyCanJoinRet.class);
                if (jsonCompanyCanJoinRet_sort != null && jsonCompanyCanJoinRet_sort.code == MyConfig.retSuccess()) {
                    MyConfig.jsonCompanyCanJoinRet_sort = jsonCompanyCanJoinRet_sort;
                }

                /**
                 * 获取已经加入的company
                 */
                String user_companies = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_COMPANIES, MyConfig.usr_token);
                MyLog.d("", "HttpTools: get user company " + user_companies);
                JsonUserCompaniesRet jsonUserCompaniesRet = gson.fromJson(user_companies, JsonUserCompaniesRet.class);
                if (jsonUserCompaniesRet != null && jsonUserCompaniesRet.code == MyConfig.retSuccess()) {

                    MyConfig.jsonUserCompaniesRet = jsonUserCompaniesRet;

                    MyLog.d("", "HttpTools:  companyListUpdateFromServer "
                            + MyConfig.jsonUserCompaniesRet.data.size());

                    EventBus.getDefault().post(new EventCompanyListUpdateUi());
                }

            }
        }).start();
    }


    public static int INVITE_CODE_LENGTH = 6;

    public static void joinCompanyWithInviteCode(final JsonJoinCompanyWithInviteCode jsonJoinCompanyWithInviteCode) {

        /**
         * 加入一家公司
         */
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonJoinCompanyWithInviteCode, JsonJoinCompanyWithInviteCode.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_JOIN_COMPANY_WITH_INVITE_CODE + jsonJoinCompanyWithInviteCode.code, jsonStr);

                final JsonJoinCompanyWithInviteCodeRet jsonJoinCompanyWithInviteCodeRet = gson.fromJson(s, JsonJoinCompanyWithInviteCodeRet.class);
                MyLog.d("", "FragmentCompanyInvite: joinCompanyWithInviteCode " + s);
                if (jsonJoinCompanyWithInviteCodeRet != null) {
                    if (jsonJoinCompanyWithInviteCodeRet.code == MyConfig.retSuccess()) {
                        EventBus.getDefault().post(new EventCompanyJoined(jsonJoinCompanyWithInviteCodeRet.data.companyId, ""));
                    } else {
                        EventBus.getDefault().post(new EventCompanyJoined(-1, jsonJoinCompanyWithInviteCodeRet.message));
                    }
                } else {
                    EventBus.getDefault().post(new EventCompanyJoined(-1, app.getString(R.string.prompt_return_failure)));
                }

            }
        }).start();

    }

    public static void joinCompany(final JsonJoinCompany jsonJoinCompany) {
        /**
         * 如果不需要输入code则使用这个接口
         * 如果需要使用code则使用joinCompanyWithInviteCode
         */

        new Thread(new Runnable() {
            @Override
            public void run() {

                MyLog.d("", "HttpTools: joinCompany: in thread");

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonJoinCompany, JsonJoinCompany.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_JOIN_COMPANY + jsonJoinCompany.id, jsonStr);

                final JsonJoinCompanyRet jsonJoinCompanyRet = gson.fromJson(s, JsonJoinCompanyRet.class);
                MyLog.d("", "HttpTools: JsonJoinCompanyRet " + s);
                if (jsonJoinCompanyRet != null) {
                    if (jsonJoinCompanyRet.code == MyConfig.retSuccess()) {
                        EventBus.getDefault().post(new EventCompanyJoined(jsonJoinCompany.id, ""));
                    } else {
                        EventBus.getDefault().post(new EventCompanyJoined(-1, jsonJoinCompanyRet.message));
                    }
                } else {
                    EventBus.getDefault().post(new EventCompanyJoined(-1, app.getString(R.string.server_fail)));
                }


            }
        }).start();

    }

    public static void setCurrentCompany(final JsonSetCurrentCompany jsonSetCurrentCompany) {
        MyLog.d("choosecompany", "company change flow: goto setCurrentCompany " + jsonSetCurrentCompany.id);

        // TODO: 12/11/15 这里应该判断一下，设置当前公司未必就切换当前公司了
        cancelAllNotification();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonSetCurrentCompany, JsonSetCurrentCompany.class);
                MyLog.d("", "company change flow: jsonSetCurrentCompany " + jsonStr);
                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SET_CURRENT_COMPANIES + jsonSetCurrentCompany.id, jsonStr);

                final JsonSetCurrentCompanyRet jsonSetCurrentCompanyRet = gson.fromJson(s, JsonSetCurrentCompanyRet.class);
                MyLog.d("", "company change flow: JsonSetCurrentCompanyRet " + s);
                if (jsonSetCurrentCompanyRet != null && jsonSetCurrentCompanyRet.code == MyConfig.retSuccess()) {
                    //MyConfig.updateUserStatus(USR_STATUS_HAVE_CURRENT_COMPANY);

//                    Snackbar.make(getView(), "set current company sucess!", Snackbar.LENGTH_LONG).show();
                    MyLog.d("choosecompany", "company change flow: setCurrentCompany success " + jsonSetCurrentCompany.id);
                    EventBus.getDefault().post(new EventHaveSetCurrentCompany(true, jsonSetCurrentCompany.id));//to close activity

                } else {
                    MyLog.d("choosecompany", "company change flow: setCurrentCompany failed");
                    EventBus.getDefault().post(new EventHaveSetCurrentCompany(false, -1));//to close activity
//                    Snackbar.make(getView(), "set current company failed!", Snackbar.LENGTH_LONG).show();
                }


            }
        }).start();
    }


    public static int getCurrentCompany_failures = 0;

    public static void getCurrentCompany() {

        MyLog.d("choosecompany", "app start steps: enter getCurrentCompany ");

        new Thread(new Runnable() {
            @Override
            public void run() {

                /**
                 * 获取当前company
                 */
                String current_company = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GET_CURRENT_COMPANIES, MyConfig.usr_token);
                MyLog.d("", "company change flow: " + MyConfig.usr_token + " checkCurrentCompany " + current_company);

                Gson gson = new Gson();
                JsonGetCurrentCompanyRet jsonGetCurrentCompanyRet = gson.fromJson(current_company, JsonGetCurrentCompanyRet.class);

                if (jsonGetCurrentCompanyRet != null) {
                    if (jsonGetCurrentCompanyRet.code == MyConfig.retSuccess()) {//收到新的当前公司
                        MyLog.d("choosecompany", "app start steps: company change flow, getCurrentCompany success, new id:" + jsonGetCurrentCompanyRet.data.id);

                        if (MyConfig.jsonGetCurrentCompanyRet != null && //已经有当前公司
                                MyConfig.jsonGetCurrentCompanyRet.data.id != jsonGetCurrentCompanyRet.data.id) { //当前公司有改变
                            /**
                             * 根据后台的需求，当前公司变化要重连socket
                             */
                            MyLog.d("", "company change flow: current company change drive socketReConnect()");
                            EventBus.getDefault().post(new EventReConnectSocket());

                        }

                        MyConfig.jsonGetCurrentCompanyRet = jsonGetCurrentCompanyRet;

                        MyLog.d("", "company change flow: checkCurrentCompany " + MyConfig.usr_status);
                        MyConfig.updateUserStatus(USR_STATUS_HAVE_CURRENT_COMPANY);
//                        MyConfig.flag_viewpager_start_______seems_no_use = 2;

                        EventBus.getDefault().post(new EventHaveGetCurrentCompany(jsonGetCurrentCompanyRet.code, jsonGetCurrentCompanyRet.data.id));

                    } else {
                        MyLog.d("choosecompany", "app start steps: company change flow, getCurrentCompany failed");
                        EventBus.getDefault().post(new EventHaveGetCurrentCompany(jsonGetCurrentCompanyRet.code, -1));
                    }
                } else {
                    /**
                     * 可能是网络故障
                     */
                    EventBus.getDefault().post(new EventHaveGetCurrentCompany(-1, -1));

                }

            }
        }).start();

    }


    public static ListAdapterMsg.OnLongClickedListener onLongClickedListener = new ListAdapterMsg.OnLongClickedListener() {
        @Override
        public void onLongClicked(final IMsg iMsg) {
            View dialogView = LayoutInflater.from(activityMainGroup).inflate(R.layout.dialog_two_choice_layout, null);
            final Dialog setDialog = new Dialog(activityMainGroup, R.style.DialogStyle);
            setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            setDialog.setCancelable(false);
            setDialog.getWindow().setContentView(dialogView);
            WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
            lp.width = MyConfig.getWidthByScreenPercent(80);
            setDialog.getWindow().setAttributes(lp);
            TextView shareText = (TextView) dialogView.findViewById(R.id.group_into);
            TextView copyText = (TextView) dialogView.findViewById(R.id.cancle_favour);
            shareText.setText(app.getString(R.string.share_system));
            copyText.setText(app.getString(R.string.copy_to_poast));
            shareText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareTextTopic(activityMainGroup, iMsg, MyConfig.ATTACHMENT_ACTION_DL_SHARE);
                    setDialog.cancel();
                }
            });
            copyText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClipboardManager clipboard = (ClipboardManager) app.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("simple text", iMsg.toString());
                    clipboard.setPrimaryClip(clip);
                    setDialog.cancel();

                }
            });
            setDialog.setCanceledOnTouchOutside(true);
            setDialog.show();

        }
    };



    public static void shareTopic(Activity context, IMsg iMsg, int action) {

        if(isShareInNews(iMsg)){
            shareNewsToWeixin(context, iMsg);
            return;
        }

        if (iMsg.type == TYPE_TOPIC) {
            if (iMsg.subType == SUBTYPE_TEXT) {
                shareTextTopic(context, iMsg, action);
            } else if (iMsg.subType == SUBTYPE_MAP) {

            } else if (iMsg.subType == SUBTYPE_VOTE) {

            } else if (iMsg.subType == SUBTYPE_IMAGE) {
                /**
                 * type
                 *  1:  image
                 *  2:  file
                 *  3:  video
                 */
                shareImageTopic(context, iMsg, action);
            } else if (iMsg.subType == SUBTYPE_FILE) {
                shareImageTopic(context, iMsg, action);
//                shareFileTopic(context, iMsg);
            } else if(iMsg.subType == SUBTYPE_VIDEO){
                shareImageTopic(context, iMsg, action);
//                shareVideoTopic(context, iMsg);
            }
        }

    }

    /**
     *
     * share method
     *      intent  0
     *      umeng   1
     *      weixin  2
     *      webpage 3
     */

    public static int share_method_text = 0;
    public static int share_method_file = 0;
    public static int share_method_video = 0;
    public static int share_method_image = 0;

    public static void shareVideoTopic(final Context context, IMsg msg){

        switch (share_method_video){
            case 0:
                //直接调用image的分享
                shareImageOnly(context, switchSubtype2AttachedType(msg.subType), new File(msg.filePath_inLocal), ATTACHMENT_ACTION_DL_SHARE );
                break;
            case 1:
                shareVideoTopicByUMeng(context, msg);
                break;
            case 2:
                shareVideoTopicByWeixin(context, msg);
                break;
            case 3:
                shareVideoTopicByWeixinWebpage(context, msg);
                break;
        }

    }


    private static void shareVideoTopicByWeixinWebpage(final Context context, IMsg msg) {


        MyLog.d("", "share out, shareVideoTopicByWeixin 1 " );


        WXWebpageObject video = new WXWebpageObject();
//        video.webpageUrl = MyConfig.getApiDomain_NoSlash_GetResources() + "/uploads/file-1468984454592-17.mp4";
        video.webpageUrl = "http://embadev.datacanvas.io/assets/avatars/videoviewdemo.mp4";
//        video.videoUrl = MyConfig.getApiDomain_NoSlash_GetResources() + msg.filePath_inServer;
//        video.videoUrl = "http://v.youku.com/v_show/id_XODkyNjAyMzg4.html";

        WXMediaMessage wxMediaMessage = new WXMediaMessage(video);
        wxMediaMessage.title = "the title";
        wxMediaMessage.description = "the description";
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.choose_3x);
        Bitmap thumbBmp = Bitmap.createScaledBitmap(bitmap,120,150, true);
        wxMediaMessage.thumbData= bmpToByteArray(thumbBmp, true);


        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = msg.clientId;
        req.scene = req.WXSceneSession;
        req.message = wxMediaMessage;

        IWXAPI api = WXAPIFactory.createWXAPI(context,MyConfig.third_weixin_share_appID );
        boolean regapp = api.registerApp(MyConfig.third_weixin_share_appID);
//        boolean openwx = api.openWXApp();
        boolean sendreq = api.sendReq(req);

        MyLog.d("", "share out, shareVideoTopicByWeixin 2 " + regapp  + " "+sendreq);
    }

    public static byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, output);
        if (needRecycle) {
            bmp.recycle();
        }

        byte[] result = output.toByteArray();
        try {
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private static void shareVideoTopicByWeixin(final Context context, IMsg msg) {


        MyLog.d("", "share out, shareVideoTopicByWeixin 1 " );


        WXVideoObject video = new WXVideoObject();
        video.videoUrl = "http://embadev.datacanvas.io/assets/avatars/videoviewdemo.mp4";
//        video.videoUrl = MyConfig.getApiDomain_NoSlash_GetResources() + "/uploads/file-1468984454592-17.mp4";
//        video.videoUrl = MyConfig.getApiDomain_NoSlash_GetResources() + msg.filePath_inServer;
//        video.videoUrl = "http://v.youku.com/v_show/id_XODkyNjAyMzg4.html";

        WXMediaMessage wxMediaMessage = new WXMediaMessage(video);
        wxMediaMessage.title = "the title";
        wxMediaMessage.description = "the description";
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.choose_3x);
        Bitmap thumbBmp = Bitmap.createScaledBitmap(bitmap,120,150, true);
        wxMediaMessage.thumbData= bmpToByteArray(thumbBmp, true);


        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = msg.clientId;
        req.scene = req.WXSceneSession;
        req.message = wxMediaMessage;

        IWXAPI api = WXAPIFactory.createWXAPI(context,MyConfig.third_weixin_share_appID );
        boolean regapp = api.registerApp(MyConfig.third_weixin_share_appID);
//        boolean openwx = api.openWXApp();
        boolean sendreq = api.sendReq(req);

        MyLog.d("", "share out, shareVideoTopicByWeixin 2 " + regapp  + " "+sendreq);
    }

    private static void shareVideoTopicByUMeng(final Context context, IMsg msg) {
        /**
         * 使用友盟分享
         */

        // 添加微信平台
        UMWXHandler wxHandler = new UMWXHandler(context, MyConfig.third_weixin_share_appID, MyConfig.third_weixin_share_appSecret);
        wxHandler.setToCircle(true);
        wxHandler.addToSocialSDK();

        // 首先在您的Activity中添加如下成员变量
        final UMSocialService mController = UMServiceFactory.getUMSocialService("com.umeng.share");

        //设置微信好友分享内容
        WeiXinShareContent weixinContent = new WeiXinShareContent();
//        //设置分享文字
//        weixinContent.setShareContent("weixin share");
//        //  weixinContent.setShareContent(MyConfig.usr_id+"您的小集体邀请码:" + invite_code);
//        //设置title
//        weixinContent.setTitle(context.getResources().getString(R.string.umen_share_weixin));
//        //设置分享内容跳转URL
//        weixinContent.setTargetUrl(context.getResources().getString(R.string.your_url));

        //设置分享图片
//                weixinContent.setShareImage(localImage);

        UMVideo umVideo = new UMVideo(MyConfig.getApiDomain_NoSlash_GetResources() + msg.filePath_inServer);
//        UMVideo umVideo = new UMVideo(MyConfig.getHttpURLConnectionRedirected(MyConfig.getApiDomain_NoSlash_GetResources() + msg.filePath_inServer).url_redirected);
        umVideo.setThumb(MyConfig.getApiDomain_NoSlash_GetResources() + msg.thumbNail);
        umVideo.setTitle("来自圆桌的视频");
        weixinContent.setShareVideo(umVideo);
        mController.setShareMedia(weixinContent);


//                mController.openShare(ActivityGenerateCompanyInviteCode.this, false);
        mController.postShare(context, SHARE_MEDIA.WEIXIN, new SocializeListeners.SnsPostListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete(SHARE_MEDIA share_media, int i, SocializeEntity socializeEntity) {
                MyConfig.MyToast(-1, context, context.getResources().getString(R.string.weixin_share_finish));
            }
        });
    }


    public static void shareNewsToWeixin(final Activity context, IMsg iMsg) {


        // 首先在您的Activity中添加如下成员变量
        String s1;
        String s2;
        int index_http = iMsg.title.indexOf("http");
        if (index_http >= 0) {
            s1 = iMsg.title.substring(0, index_http);
            s2 = iMsg.title.substring(index_http, iMsg.title.length());
        }else{
            return;
        }

        final UMSocialService mController = UMServiceFactory.getUMSocialService("com.umeng.share");
        mController.getConfig().removePlatform(SHARE_MEDIA.RENREN, SHARE_MEDIA.DOUBAN, SHARE_MEDIA.TENCENT, SHARE_MEDIA.SINA);

        // 添加微信平台
        UMWXHandler wxHandler = new UMWXHandler(context, MyConfig.third_weixin_share_appID, MyConfig.third_weixin_share_appSecret);
        wxHandler.addToSocialSDK();
        //设置微信好友分享内容
        WeiXinShareContent weixinContent = new WeiXinShareContent();
        //设置分享文字
        weixinContent.setShareContent(s1);
        //设置title
        weixinContent.setTitle("新闻转发");
        //设置分享内容跳转URL
        weixinContent.setTargetUrl(s2);
        //设置分享图片
        UMImage urlImages = new UMImage(context, MyConfig.getApiDomain_NoSlash_GetResources() + iMsg.thumbNail);
        weixinContent.setShareImage(urlImages);
        mController.setShareMedia(weixinContent);


        // 支持微信朋友圈
        UMWXHandler wxCircleHandler = new UMWXHandler(context, MyConfig.third_weixin_share_appID, MyConfig.third_weixin_share_appSecret);
        wxCircleHandler.setToCircle(true);
        wxCircleHandler.addToSocialSDK();
        CircleShareContent circleMedia = new CircleShareContent();
        circleMedia.setShareContent("新闻");
        circleMedia.setTitle(s1);
        circleMedia.setShareImage(urlImages);
        circleMedia.setTargetUrl(s2);
        mController.setShareMedia(circleMedia);

        /**
         * 如果有多个分享，比如同时分享到微信和朋友圈，则需要openShare，否则直接postShare
         */
        mController.openShare(context, false);
//        mController.postShare(context, SHARE_MEDIA.WEIXIN, new SocializeListeners.SnsPostListener() {
//            @Override
//            public void onStart() {
//                MyConfig.MyToast(1, context, context.getResources().getString(R.string.weixin_share_start));
//            }
//
//            @Override
//            public void onComplete(SHARE_MEDIA share_media, int i, SocializeEntity socializeEntity) {
//                MyConfig.MyToast(1, context, context.getResources().getString(R.string.weixin_share_finish));
//            }
//        });
    }

    public static void shareAppToWeixin(final Activity context, String title, String s, String url, int drawable_id) {

        // 首先在您的Activity中添加如下成员变量

        final UMSocialService mController = UMServiceFactory.getUMSocialService("com.umeng.share");
        mController.getConfig().removePlatform(SHARE_MEDIA.RENREN, SHARE_MEDIA.DOUBAN, SHARE_MEDIA.TENCENT, SHARE_MEDIA.SINA);


        // 添加微信平台
        UMWXHandler wxHandler = new UMWXHandler(context, MyConfig.third_weixin_share_appID, MyConfig.third_weixin_share_appSecret);
        wxHandler.addToSocialSDK();
        //设置微信好友分享内容
        WeiXinShareContent weixinContent = new WeiXinShareContent();
        //设置分享文字
        weixinContent.setShareContent(s);
        //设置title
        weixinContent.setTitle(title);
        //设置分享内容跳转URL
        weixinContent.setTargetUrl(url);
        //设置分享图片
        UMImage urlImages = new UMImage(context, drawable_id);
        weixinContent.setShareImage(urlImages);
        mController.setShareMedia(weixinContent);


        // 支持微信朋友圈
        UMWXHandler wxCircleHandler = new UMWXHandler(context, MyConfig.third_weixin_share_appID, MyConfig.third_weixin_share_appSecret);
        wxCircleHandler.setToCircle(true);
        wxCircleHandler.addToSocialSDK();
        CircleShareContent circleMedia = new CircleShareContent();
        circleMedia.setShareContent(s);
        circleMedia.setTitle(title);
        circleMedia.setShareImage(urlImages);
        circleMedia.setTargetUrl(url);
        mController.setShareMedia(circleMedia);

        /**
         * 如果有多个分享，比如同时分享到微信和朋友圈，则需要openShare，否则直接postShare
         */
        mController.openShare(context, false);
//        mController.postShare(context, SHARE_MEDIA.WEIXIN, new SocializeListeners.SnsPostListener() {
//            @Override
//            public void onStart() {
//                MyConfig.MyToast(1, context, context.getResources().getString(R.string.weixin_share_start));
//            }
//
//            @Override
//            public void onComplete(SHARE_MEDIA share_media, int i, SocializeEntity socializeEntity) {
//                MyConfig.MyToast(1, context, context.getResources().getString(R.string.weixin_share_finish));
//            }
//        });
    }


    public static void shareTopicAsLink(Context context, IMsg msg) {

        /**
         * 时间缘故，临时这样处理，以后分开为文本分享及图片分享
         */

        MyLog.d("", "shareTopic()");

        Intent dealIntent = new Intent();
        dealIntent.setAction(Intent.ACTION_SEND);
        dealIntent.setType("text/plain");
        dealIntent.putExtra(Intent.EXTRA_SUBJECT, msg.title);

        StringBuilder sb = new StringBuilder();
//        sb.append("标题:" + msg.title);

        if (msg.type == TYPE_TOPIC) {
            if (msg.subType == SUBTYPE_IMAGE) {
                if (msg.body != null) {
                    sb.append("" + msg.body);
                }
                sb.append(app.getString(R.string.image_link) + MyConfig.getApiDomain_NoSlash_GetResources() + msg.fullSize);
            } else if (msg.subType == SUBTYPE_TEXT) {
                if (msg.body != null) {
                    sb.append("" + msg.body);
                }
            } else if (msg.subType == SUBTYPE_MAP) {
                if (msg.body != null) {
                    sb.append("" + msg.body);
                }
                sb.append(app.getString(R.string.map) + MyConfig.getApiDomain_NoSlash_GetResources() + msg.fullSize);
            } else if (msg.subType == SUBTYPE_VOTE) {
                if (msg.fullSize != null) {
                    sb.append(app.getString(R.string.have_a_vote));
                    sb.append(app.getString(R.string.image_link) + MyConfig.getApiDomain_NoSlash_GetResources() + msg.fullSize);
                } else {
                    return;//没啥能分享的
                }
            }
        }

        dealIntent.putExtra(Intent.EXTRA_TEXT, sb.toString());

        context.startActivity(Intent.createChooser(dealIntent,
                context.getResources().getText(R.string.share_dealby)));

    }

    public static void shareTextTopic(Context context, IMsg msg, int action) {
        MyLog.d("", "shareTopic()");

        Intent dealIntent = new Intent();
        dealIntent.setAction(getIntentAction(action));
        dealIntent.setType("text/plain");

        /**
         * 文本主题格式改动，没有body，只有title
         * 所以按如下改动
         */
//        dealIntent.putExtra(Intent.EXTRA_SUBJECT, msg.title);
//        dealIntent.putExtra(Intent.EXTRA_TEXT, msg.body);
        dealIntent.putExtra(Intent.EXTRA_TEXT, msg.title);

        MyLog.d("", "share out, shareTextTopic " + dealIntent.getDataString() + " " + " mime " + dealIntent.getType() + " action " + dealIntent.getAction());


        context.startActivity(Intent.createChooser(dealIntent,
                context.getResources().getText(R.string.share_dealby)));

    }

    public static void shareFileTopic(Context context, IMsg msg) {

        /**
         * 如何处理未知或者没有mime的文件？
         */

        String url = MyConfig.getHttpURLConnectionRedirected(MyConfig.getApiDomain_NoSlash_GetResources() + msg.filePath_inServer).url_redirected;
//        String url = MyConfig.getApiDomain_NoSlash_GetResources() + msg.filePath_inServer;
//        String mime = MyConfig.guessMimeByUrlStream(url);

        Intent dealIntent = new Intent();

        if (msg.fileDownloadProgress != 100) {
            dealIntent.setDataAndType(Uri.parse(url), "text/link");
            dealIntent.setAction(Intent.ACTION_SEND);
            MyLog.d("", "share in process: url " + url);
        } else {

            String mime = MyConfig.guessMimeByFilename(msg.fileName);
            if (mime == "*/*") {
//                mime = "application/zip";
                dealIntent.setAction(Intent.ACTION_SEND);
            } else {
//                dealIntent.setAction(Intent.ACTION_VIEW);
                dealIntent.setAction(Intent.ACTION_SEND);
//                dealIntent.putExtra(Intent.EXTRA_SUBJECT, msg.title);
//                dealIntent.putExtra(Intent.EXTRA_TEXT, msg.body);
            }
//            dealIntent.setDataAndType(Uri.fromFile(new File(msg.filePath_inLocal)), mime);
            dealIntent.setType(mime);
            dealIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(msg.filePath_inLocal)));
            MyLog.d("", "share in process: local " + msg.filePath_inLocal);
        }

//        MyLog.d("", "share out, shareFileTopic " + Uri.fromFile(new File(msg.filePath_inLocal)).toString() + " " + " mime " + mime + " " + dealIntent.getAction());
        MyLog.d("", "share out, shareFileTopic " + dealIntent.getDataString() + " " + " mime " + dealIntent.getType() + " action " + dealIntent.getAction());

        context.startActivity(Intent.createChooser(dealIntent,
                context.getResources().getText(R.string.share_dealby)));

    }


    public static final int ATTACHMENT_ACTION_DO_NOTHING = 0;
    public static final int ATTACHMENT_ACTION_DL_SHARE = 1;
    public static final int ATTACHMENT_ACTION_DL_OPEN = 2;
    public static final int ATTACHMENT_ACTION_DL_OPEN_INNER = 3;

    public static String getIntentAction(int action){

        switch (action){
            case MyConfig.ATTACHMENT_ACTION_DO_NOTHING:
                return null;
            case MyConfig.ATTACHMENT_ACTION_DL_SHARE:
                return Intent.ACTION_SEND;
            case MyConfig.ATTACHMENT_ACTION_DL_OPEN:
                return Intent.ACTION_VIEW;
            case MyConfig.ATTACHMENT_ACTION_DL_OPEN_INNER:
                return null;
            default:
                return null;
        }
    }

    /**
     * type
     *      1:  image
     *      2:  file
     *      3:  video
     */
    public static final int ATTACHED_TYPE_UNKNOWN = -1;
    public static final int ATTACHED_TYPE_IMAGE = 1;
    public static final int ATTACHED_TYPE_FILE = 2;
    public static final int ATTACHED_TYPE_VIDEO = 3;

    public static int switchSubtype2AttachedType(int subtype){
        switch (subtype){
            case SUBTYPE_IMAGE:
                return ATTACHED_TYPE_IMAGE;
            case SUBTYPE_FILE:
                return ATTACHED_TYPE_FILE;
            case SUBTYPE_VIDEO:
                return ATTACHED_TYPE_VIDEO;
        }
        return ATTACHED_TYPE_UNKNOWN;
    }

    public static void shareImageOnly(Context context, int type, File f, int action) {

        if (f == null) {
            return;
        }

        MyLog.d("", "[shareImageOnly] share out, onEventMainThread: " + " action "+ action +" path "+ f.getAbsolutePath());

        Intent dealIntent = new Intent();

        String intent_action = getIntentAction(action);
        if(intent_action == null){
            return;
        }

        dealIntent.setAction(intent_action);

        switch (type){
            case ATTACHED_TYPE_IMAGE:
                dealIntent.setType(MyConfig.MIME_IMAGE_STAR);
                break;
            case ATTACHED_TYPE_FILE:
                dealIntent.setType(MyConfig.guessMimeByFilename(f.getName()));
                break;
            case ATTACHED_TYPE_VIDEO:
                dealIntent.setType("*/*");
                break;

        }
        Uri uri = Uri.fromFile(f);
        dealIntent.putExtra(Intent.EXTRA_STREAM, uri);

        context.startActivity(Intent.createChooser(dealIntent,
                context.getResources().getText(R.string.share_dealby)));

    }



    public static void shareImageTopic(Context context, IMsg msg, int action) {
        /**
         * type
         *  1:  image
         *  2:  file
         *  3:  video
         */

        MyLog.d("", "share out, onEventMainThread fileDownloadProgress " + msg.fileDownloadProgress);

        if (msg.fileDownloadProgress == 0) {
            /**
             * 准备下载图片
             */

            new DownloadFileFromUrl(
                    new DownloadFileFromUrl.DownloadFileFromUrlSenderInfo(
                            ActivitySubGroup.class.getName(),
                            action))
                    .execute(
                            MyConfig.getApiDomain_NoSlash_GetResources() + ((msg.subType == SUBTYPE_IMAGE)?msg.fullSize:msg.filePath_inServer),
                            msg.filePath_inLocal);
            MyLog.d("", "share out, onEventMainThread DownloadFileFromUrl " );



        } else if (msg.fileDownloadProgress == 100) {

            if(FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {

                Intent dealIntent = new Intent();
                dealIntent.setAction(Intent.ACTION_SEND);
                dealIntent.setType(MyConfig.MIME_IMAGE_STAR);
                dealIntent.putExtra(Intent.EXTRA_SUBJECT, msg.title);
                dealIntent.putExtra(Intent.EXTRA_TEXT, msg.body);
                Uri uri = Uri.fromFile(new File(msg.filePath_inLocal));
                dealIntent.putExtra(Intent.EXTRA_STREAM, uri);

                context.startActivity(Intent.createChooser(dealIntent,
                        context.getResources().getText(R.string.share_dealby)));
            }else{

                /**
                 * 只有share image only分享到微信的图片能直接显示，否则会被微信当成文件处理，不能直接显示
                 */
                shareImageOnly(context, switchSubtype2AttachedType(msg.subType), new File(msg.filePath_inLocal), action);

            }
        }

    }

    public static Bitmap getScreenViewBitmap(View v) {
        v.setDrawingCacheEnabled(true);
        v.buildDrawingCache(true);

        // creates immutable clone
        Bitmap b = Bitmap.createBitmap(v.getDrawingCache());

        v.setDrawingCacheEnabled(false); // clear drawing cache

        return b;
    }

    public static void saveViewToMediaStore(Activity activity, View v, String path, int share) {

//        v.setDrawingCacheEnabled(true);
//        Bitmap bitmap = v.getDrawingCache();

        Bitmap bitmap = getScreenViewBitmap(v);

//        MediaStore.Images.Media.insertImage(app.getContentResolver(), bitmap, title, desc);


        File f = new File(path);
        String fileName = f.getName();
        int pos = fileName.lastIndexOf(".");
        if (pos > 0) {
            fileName = fileName.substring(0, pos);
        }

        saveBitmapToJpgInSystemPictureDirectory(activity, bitmap, fileName + ".jpg", share);

//        v.setDrawingCacheEnabled(false);
    }


    public static void saveBitmapToJpgInSystemPictureDirectory(final Activity activity, final Bitmap bm, final String imgName, final int share) {

        new Thread(new Runnable() {
            @Override
            public void run() {


                OutputStream fOut;
                String strDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();

                File f = new File(strDirectory, imgName);
                try {
                    fOut = new FileOutputStream(f);

                    /**Compress image**/
                    bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                    fOut.flush();
                    fOut.close();

                    /**Update image to gallery**/
                    MediaStore.Images.Media.insertImage(activity.getContentResolver(),
                            f.getAbsolutePath(), f.getName(), f.getName());

                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Intent mediaScanIntent = new Intent(
                            Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri contentUri = Uri.fromFile(f);
                    mediaScanIntent.setData(contentUri);
                    activity.sendBroadcast(mediaScanIntent);
                } else {
                    activity.sendBroadcast(new Intent(
                            Intent.ACTION_MEDIA_MOUNTED,
                            Uri.parse("file://"
                                    + Environment.getExternalStorageDirectory())));
                }


                shareImageOnly(activity, MyConfig.ATTACHED_TYPE_IMAGE, f, share);

//        MediaScannerConnection.scanFile(context, new String[] {strDirectory}, null,
//                new MediaScannerConnection.OnScanCompletedListener() {
//                    public void onScanCompleted(String path, Uri uri){
//
//                    }
//                });
            }
        }).start();

    }


    public static void delTopic(final int topicId) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                JsonDelTopic jsonDelTopic = new JsonDelTopic();
                jsonDelTopic.accessToken = MyConfig.usr_token;
                jsonDelTopic.id = topicId;

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonDelTopic, JsonDelTopic.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_TOPIC_DEL + topicId, jsonStr);

                final JsonDelTopicRet jsonDelTopicRet = gson.fromJson(s, JsonDelTopicRet.class);
//                MyLog.d("", "HttpTools: "+topicId+" "+add+" addFavorite " + s);
                if (jsonDelTopicRet != null) {
                    if (jsonDelTopicRet.code == MyConfig.retSuccess()) {

                        EventBus.getDefault().post(new EventTopicDelete());
                        MyLog.d("", "delTopic return(success): " + jsonDelTopicRet.message);

                    } else {
                        MyLog.d("", "delTopic return(failed): " + jsonDelTopicRet.message);
                    }
                }

            }
        }).start();

    }

    public static void addFavorite(final int topicId, final boolean add) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                JsonAddFavority jsonAddFavority = new JsonAddFavority();
                jsonAddFavority.accessToken = MyConfig.usr_token;
                jsonAddFavority.id = topicId;
                if (add) {

                } else {
                    jsonAddFavority.unfavorite = true;
                }

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonAddFavority, JsonAddFavority.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_FAVORITY_ADD + topicId, jsonStr);

                final JsonAddFavoriteRet jsonAddFavoriteRet = gson.fromJson(s, JsonAddFavoriteRet.class);
                MyLog.d("", "HttpTools: " + topicId + " " + add + " addFavorite " + s);
                if (jsonAddFavoriteRet != null && jsonAddFavoriteRet.code == MyConfig.retSuccess()) {

                    EventBus.getDefault().post(new EventFavoriteChanged(topicId, add));

                } else {

                }


            }
        }).start();

    }
    public static void setLocalFileAnd(FriendCricleMsg msg){
        if(msg.videoPath!=null){
            if(msg.hashCode!=null){
                msg.videoPath_local = getLocalFileByHashcodeAndFileName(msg.hashCode, "video").getAbsolutePath();
            }else{
                msg.videoPath_local = getLocalFileByUrlAndFileName(msg.videoPath, "video").getAbsolutePath();
            }
            msg.videoProgress = getFileDownloadProgress(msg.videoPath_local, msg.videosize);
        }
    }

    public static void setLocalFileAndProcessUniversal(IMsg msg) {
        if (msg.type == TYPE_TOPIC) {
            if (msg.subType == SUBTYPE_FILE || msg.subType == SUBTYPE_VIDEO) {
                if (msg.hashCode != null) {
                    msg.filePath_inLocal = getLocalFileByHashcodeAndFileName(msg.hashCode, msg.fileName).getAbsolutePath();
                } else {
                    msg.filePath_inLocal = getLocalFileByUrlAndFileName(msg.filePath_inServer, msg.fileName).getAbsolutePath();
                }
                msg.fileDownloadProgress = getFileDownloadProgress(msg.filePath_inLocal, msg.fileSize);
            } else if (msg.subType == SUBTYPE_IMAGE) {
                msg.filePath_inLocal = getLocalFileByUrlAndFileName(msg.fullSize, null).getAbsolutePath();
                msg.fileDownloadProgress = 0;//服务器目前没有给出图片大小，故这里无法判断下载进度
            } else if (msg.subType == SUBTYPE_VOTE) {
                if (msg.fullSize != null) {//有的vote没有图片
                    msg.filePath_inLocal = getLocalFileByUrlAndFileName(msg.fullSize, null).getAbsolutePath();
                    msg.fileDownloadProgress = 0;//服务器目前没有给出图片大小，故这里无法判断下载进度
                }
            }
        } else if (msg.type == TYPE_TOPIC_COMMANT) {
            if (msg.subType == SUBTYPE_IMAGE_COMMENT) {
                msg.filePath_inLocal = getLocalFileByUrlAndFileName(msg.fullSize, null).getAbsolutePath();
                msg.fileDownloadProgress = 0;//服务器目前没有给出图片大小，故这里无法判断下载进度
            }

        } else if (msg.type == TYPE_TOPIC_IMAGE_DIRECTROOM) {
            msg.filePath_inLocal = getLocalFileByUrlAndFileName(msg.fullSize, null).getAbsolutePath();
            msg.fileDownloadProgress = 0;//服务器目前没有给出图片大小，故这里无法判断下载进度
        }
    }

    public static IMsg formIMsgFromGetImageRet_DirectRoom(Json_DirectRoom_GetTopicImageRet.DirectRoom_GetTopicImageRet getImageRet, IMsg iMsgParent, int category) {
        IMsg msg = new IMsg(
                getImageRet.userId,
                getImageRet.sender,
                getImageRet.avatar,

                MyConfig.UTCString2Date(getImageRet.updatedAt),

                MyConfig.LEVEL_MSG_1,
                iMsgParent,
                category,

                getImageRet.groupId,
                -1,
                getImageRet.info.clientId,

                null,
                null
        );
        msg.originDataFromServer.getImageRet = getImageRet;
        msg.id = getImageRet.id;
        msg.type = getImageRet.type;
        msg.subType = getImageRet.subType;

        msg.mapLocation.latitude = getImageRet.info.latitude;
        msg.mapLocation.longitude = getImageRet.info.longitude;
        msg.mapLocation.screenShotPath = getImageRet.info.thumbNail;

        msg.thumbNail = getImageRet.info.thumbNail;
        msg.thumbNail_h = getImageRet.info.thumbNail_h;
        msg.thumbNail_w = getImageRet.info.thumbNail_w;
        msg.fullSize = getImageRet.info.fullSize;
        msg.fullSize_h = getImageRet.info.fullSize_h;
        msg.fullSize_w = getImageRet.info.fullSize_w;

        setLocalFileAndProcessUniversal(msg);

        msg.server_createAt = getImageRet.createdAt;
        msg.server_tag_id = getImageRet.id;

        return msg;
    }

    public static IMsg formIMsgFromSoundMsg(JsonSoundMsg jsonSoundMsg, IMsg iMsgParent, int category) {
        IMsg msg = new IMsg(
                jsonSoundMsg.userId,
                jsonSoundMsg.sender,
                jsonSoundMsg.avatar,

                MyConfig.UTCString2Date(jsonSoundMsg.updatedAt),

                MyConfig.LEVEL_MSG_1,
                iMsgParent,
                category,

                jsonSoundMsg.groupId,
                -1,
                jsonSoundMsg.info.clientId,

                null,
                null
        );
        msg.originDataFromServer.jsonSoundMsg = jsonSoundMsg;
        msg.readed = jsonSoundMsg.info.readed;
        msg.audioId = jsonSoundMsg.info.audioId;
        msg.audioPath = jsonSoundMsg.info.audioPath;
        msg.duration = jsonSoundMsg.info.duration;

        msg.id = jsonSoundMsg.id;
        msg.type = jsonSoundMsg.type;
        msg.subType = jsonSoundMsg.subType;

        msg.server_createAt = jsonSoundMsg.createdAt;
        msg.server_tag_id = jsonSoundMsg.id;

        return msg;
    }

    public static IMsg formIMsgFromGetEventRet(JsonGetEventRet.GetEventRet getEventRet, IMsg iMsgParent, int category) {
        IMsg msg = new IMsg(
                getEventRet.userId,
                getEventRet.sender,
                getEventRet.avatar,

                MyConfig.UTCString2Date(getEventRet.updatedAt),

                MyConfig.LEVEL_MSG_1,
                iMsgParent,
                category,

                getEventRet.groupId,
                -1,
                getEventRet.info.clientId,

                null,
                getEventRet.info.message
        );
        msg.originDataFromServer.getEventRet = getEventRet;
        msg.id = getEventRet.id;
        msg.type = getEventRet.type;
        msg.subType = getEventRet.subType;

        msg.server_createAt = getEventRet.createdAt;
        msg.server_tag_id = getEventRet.id;

        return msg;
    }

    public static IMsg formIMsgFromGetMsgRet(JsonGetMsgRet.GetMsgRet getMsgRet, IMsg iMsgParent, int category) {
        IMsg msg = new IMsg(
                getMsgRet.userId,
                getMsgRet.sender,
                getMsgRet.avatar,

                MyConfig.UTCString2Date(getMsgRet.updatedAt),

                MyConfig.LEVEL_MSG_1,
                iMsgParent,
                category,

                getMsgRet.groupId,
                -1,
                getMsgRet.info.clientId,

                null,
                getMsgRet.info.message
        );
        msg.originDataFromServer.getMsgRet = getMsgRet;
        msg.id = getMsgRet.id;
        msg.type = getMsgRet.type;
        msg.subType = getMsgRet.subType;

        msg.server_createAt = getMsgRet.createdAt;
        msg.server_tag_id = getMsgRet.id;

        return msg;
    }

    public static IMsg formIMsgFromGetMsgTopicRet(JsonGetMsgTopicRet.GetMsgTopicRet getMsgTopicRet, IMsg iMsgParent, int category) {
        IMsg msg = new IMsg(
                getMsgTopicRet.userId,
                getMsgTopicRet.sender,
                getMsgTopicRet.avatar,

                MyConfig.UTCString2Date(getMsgTopicRet.updatedAt),

                MyConfig.LEVEL_MSG_1,
                iMsgParent,
                category,

                getMsgTopicRet.groupId,
                getMsgTopicRet.info.topicId,
                getMsgTopicRet.info.clientId,

                getMsgTopicRet.info.title,
                getMsgTopicRet.info.desc
        );
        msg.originDataFromServer.getMsgTopicRet = getMsgTopicRet;
        msg.id = getMsgTopicRet.id;
        msg.type = getMsgTopicRet.type;
        msg.subType = getMsgTopicRet.subType;

        msg.vote.title = getMsgTopicRet.info.title;
        msg.vote.anonymous = getMsgTopicRet.info.anonymous;
        msg.vote.single = getMsgTopicRet.info.single;
        msg.vote.options = getMsgTopicRet.info.options;
        msg.vote.choices = getMsgTopicRet.info.choices;
        msg.vote.voted = getMsgTopicRet.info.voted;
        msg.vote.closed = getMsgTopicRet.info.closed;
        MyLog.d("", "vote debug: if closed? " + msg.vote.closed);
        msg.vote.resouceId = getMsgTopicRet.info.resouceId;
        msg.vote.voterNum = getMsgTopicRet.info.voterNum;

        /**报名*/
        msg.mActivity.desc = getMsgTopicRet.info.desc;
        msg.mActivity.title = getMsgTopicRet.info.title;
        msg.mActivity.closeTime = getMsgTopicRet.info.closeTime;
        msg.mActivity.startTime = getMsgTopicRet.info.startTime;
        msg.mActivity.endTime = getMsgTopicRet.info.endTime;
        msg.mActivity.isPublic = getMsgTopicRet.info.isPublic;
        msg.mActivity.closed = getMsgTopicRet.info.closed;
        msg.mActivity.resouceId = getMsgTopicRet.info.resouceId;
        msg.mActivity.joinedUsers = getMsgTopicRet.info.joinedUsers;
        msg.mActivity.status = getMsgTopicRet.info.status;
        msg.mActivity.joinedNum = getMsgTopicRet.info.joinedNum;
        msg.mActivity.limit = getMsgTopicRet.info.limit;
        msg.mActivity.location = getMsgTopicRet.info.location;
        msg.mActivity.longitude = getMsgTopicRet.info.longitude;
        msg.mActivity.latitude = getMsgTopicRet.info.latitude;
        msg.mActivity.cost = getMsgTopicRet.info.fee;

        msg.mapLocation.screenShotPath = getMsgTopicRet.info.thumbNail;
        msg.mapLocation.latitude = getMsgTopicRet.info.latitude;
        msg.mapLocation.longitude = getMsgTopicRet.info.longitude;

        msg.thumbNail = getMsgTopicRet.info.thumbNail;
        msg.thumbNail_h = getMsgTopicRet.info.thumbNail_h;
        msg.thumbNail_w = getMsgTopicRet.info.thumbNail_w;
        msg.fullSize = getMsgTopicRet.info.fullSize;
        msg.fullSize_h = getMsgTopicRet.info.fullSize_h;
        msg.fullSize_w = getMsgTopicRet.info.fullSize_w;

        msg.fileName = getMsgTopicRet.info.fileName;
        msg.filePath_inServer = getMsgTopicRet.info.filePath;
        msg.fileSize = getMsgTopicRet.info.fileSize;
        msg.hashCode = getMsgTopicRet.info.hashCode;

        msg.link = getMsgTopicRet.info.link;

        setLocalFileAndProcessUniversal(msg);

//        msg.fileDownloadProgress = getFileDownloadProgress(msg.filePath_inLocal, msg.fileSize);

        msg.favorited = getMsgTopicRet.favorited;

        msg.server_createAt = getMsgTopicRet.createdAt;
        msg.server_tag_id = getMsgTopicRet.id;

        msg.news = getMsgTopicRet.info.news;
        msg.os = getMsgTopicRet.info.os;

        return msg;
    }


    public static IMsg formIMsgFromGetMsgTopicCommentRet(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet getMsgTopicCommentRet, IMsg iMsgParent, int category) {
        IMsg msg = new IMsg(
                getMsgTopicCommentRet.userId,
                getMsgTopicCommentRet.sender,
                getMsgTopicCommentRet.avatar,

                MyConfig.UTCString2Date(getMsgTopicCommentRet.updatedAt),

                MyConfig.LEVEL_MSG_2,
                iMsgParent,//这里应该得不到parant信息
                category,

                getMsgTopicCommentRet.groupId,
                getMsgTopicCommentRet.info.topicId,
                getMsgTopicCommentRet.info.clientId,

                getMsgTopicCommentRet.info.title,
                getMsgTopicCommentRet.info.message
        );
        msg.creatorName = getMsgTopicCommentRet.info.creatorName;
        MyLog.d("", "http TopicComment: " + msg.creatorName);
        msg.creatorAvatar = getMsgTopicCommentRet.info.creatorAvatar;

        msg.originDataFromServer.getMsgTopicCommentRet = getMsgTopicCommentRet;
        msg.id = getMsgTopicCommentRet.id;
        msg.type = getMsgTopicCommentRet.type;
        msg.subType = getMsgTopicCommentRet.subType;

        msg.thumbNail = getMsgTopicCommentRet.info.thumbNail;
        msg.thumbNail_h = getMsgTopicCommentRet.info.thumbNail_h;
        msg.thumbNail_w = getMsgTopicCommentRet.info.thumbNail_w;
        msg.fullSize = getMsgTopicCommentRet.info.fullSize;
        msg.fullSize_h = getMsgTopicCommentRet.info.fullSize_h;
        msg.fullSize_w = getMsgTopicCommentRet.info.fullSize_w;

        msg.readed = getMsgTopicCommentRet.info.readed;
        msg.audioId = getMsgTopicCommentRet.info.audioId;
        msg.audioPath = getMsgTopicCommentRet.info.audioPath;
        msg.duration = getMsgTopicCommentRet.info.duration;

        setLocalFileAndProcessUniversal(msg);

        msg.server_createAt = getMsgTopicCommentRet.createdAt;
        msg.server_tag_id = getMsgTopicCommentRet.id;
        msg.news = getMsgTopicCommentRet.info.news;
        return msg;
    }


    public static JsonGetMsgTopicRet.GetMsgTopicRet transfer2GetGetMsgTopicRet(JsonGetTopicDetailRet jsonGetTopicDetailRet) {
        JsonGetMsgTopicRet.GetMsgTopicRet jsonGetMsgTopicRet = new JsonGetMsgTopicRet.GetMsgTopicRet();
        jsonGetMsgTopicRet.id = jsonGetTopicDetailRet.data.id;
        jsonGetMsgTopicRet.type = jsonGetTopicDetailRet.data.type;
        jsonGetMsgTopicRet.subType = jsonGetTopicDetailRet.data.subType;
        jsonGetMsgTopicRet.groupId = jsonGetTopicDetailRet.data.groupId;
        jsonGetMsgTopicRet.userId = jsonGetTopicDetailRet.data.userId;
        jsonGetMsgTopicRet.deleted = jsonGetTopicDetailRet.data.deleted;
        jsonGetMsgTopicRet.updatedAt = jsonGetTopicDetailRet.data.updatedAt;
        jsonGetMsgTopicRet.createdAt = jsonGetTopicDetailRet.data.createdAt;
        jsonGetMsgTopicRet.sender = jsonGetTopicDetailRet.data.sender;
        jsonGetMsgTopicRet.avatar = jsonGetTopicDetailRet.data.avatar;
        jsonGetMsgTopicRet.favorited = jsonGetTopicDetailRet.data.favorited;

        jsonGetMsgTopicRet.info.topicId = jsonGetTopicDetailRet.data.info.topicId;
        jsonGetMsgTopicRet.info.title = jsonGetTopicDetailRet.data.info.title;
        jsonGetMsgTopicRet.info.desc = jsonGetTopicDetailRet.data.info.desc;
        jsonGetMsgTopicRet.info.clientId = jsonGetTopicDetailRet.data.info.clientId;

        /**
         * 有的属性在subroom没有用到
         */
        jsonGetMsgTopicRet.info.single = jsonGetTopicDetailRet.data.info.single;
        jsonGetMsgTopicRet.info.anonymous = jsonGetTopicDetailRet.data.info.anonymous;
        jsonGetMsgTopicRet.info.choices = jsonGetTopicDetailRet.data.info.choices;
        jsonGetMsgTopicRet.info.options = jsonGetTopicDetailRet.data.info.options;
        jsonGetMsgTopicRet.info.voted = jsonGetTopicDetailRet.data.info.voted;
        jsonGetMsgTopicRet.info.closed = jsonGetTopicDetailRet.data.info.closed;
        MyLog.d("", "vote debug: if closed? " + jsonGetMsgTopicRet.info.closed);
        jsonGetMsgTopicRet.info.resouceId = jsonGetTopicDetailRet.data.info.resouceId;
        jsonGetMsgTopicRet.info.voterNum = jsonGetTopicDetailRet.data.info.voterNum;

        jsonGetMsgTopicRet.info.latitude = jsonGetTopicDetailRet.data.info.latitude;
        jsonGetMsgTopicRet.info.longitude = jsonGetTopicDetailRet.data.info.longitude;

        jsonGetMsgTopicRet.info.thumbNail = jsonGetTopicDetailRet.data.info.thumbNail;
        jsonGetMsgTopicRet.info.fullSize = jsonGetTopicDetailRet.data.info.fullSize;
        jsonGetMsgTopicRet.info.fullSize_h = jsonGetTopicDetailRet.data.info.fullSize_h;
        jsonGetMsgTopicRet.info.fullSize_w = jsonGetTopicDetailRet.data.info.fullSize_w;
        jsonGetMsgTopicRet.info.thumbNail_h = jsonGetTopicDetailRet.data.info.thumbNail_h;
        jsonGetMsgTopicRet.info.thumbNail_w = jsonGetTopicDetailRet.data.info.thumbNail_w;

        jsonGetMsgTopicRet.info.fileName = jsonGetTopicDetailRet.data.info.fileName;
        jsonGetMsgTopicRet.info.filePath = jsonGetTopicDetailRet.data.info.filePath;
        jsonGetMsgTopicRet.info.fileSize = jsonGetTopicDetailRet.data.info.fileSize;
        jsonGetMsgTopicRet.info.hashCode = jsonGetTopicDetailRet.data.info.hashCode;

        jsonGetMsgTopicRet.info.link = jsonGetTopicDetailRet.data.info.link;
        jsonGetMsgTopicRet.info.news = jsonGetTopicDetailRet.data.info.news;
        jsonGetMsgTopicRet.info.os = jsonGetTopicDetailRet.data.info.os;

        /**报名*/
        jsonGetMsgTopicRet.info.isPublic = jsonGetTopicDetailRet.data.info.isPublic;
        jsonGetMsgTopicRet.info.closeTime = jsonGetTopicDetailRet.data.info.closeTime;
        jsonGetMsgTopicRet.info.startTime = jsonGetTopicDetailRet.data.info.startTime;
        jsonGetMsgTopicRet.info.endTime = jsonGetTopicDetailRet.data.info.endTime;
        jsonGetMsgTopicRet.info.joinedUsers = jsonGetTopicDetailRet.data.info.joinedUsers;
        jsonGetMsgTopicRet.info.status = jsonGetTopicDetailRet.data.info.status;
        jsonGetMsgTopicRet.info.joinedNum = jsonGetTopicDetailRet.data.info.joinedNum;
        jsonGetMsgTopicRet.info.limit = jsonGetTopicDetailRet.data.info.limit;
        jsonGetMsgTopicRet.info.location = jsonGetTopicDetailRet.data.info.location;
        jsonGetMsgTopicRet.info.fee = jsonGetTopicDetailRet.data.info.fee;

        return jsonGetMsgTopicRet;
    }

    public static int getVoteResultColor(int num) {

        switch (num % 10) {
            case 0:
                return R.color.xjt_vote_result_0;
            case 1:
                return R.color.xjt_vote_result_1;
            case 2:
                return R.color.xjt_vote_result_2;
            case 3:
                return R.color.xjt_vote_result_3;
            case 4:
                return R.color.xjt_vote_result_4;
            case 5:
                return R.color.xjt_vote_result_5;
            case 6:
                return R.color.xjt_vote_result_6;
            case 7:
                return R.color.xjt_vote_result_7;
            case 8:
                return R.color.xjt_vote_result_8;
            case 9:
                return R.color.xjt_vote_result_9;
        }
        return R.color.xjt_vote_result_0;
    }

    public static int getSchoolContactPeopleResultColor(int num, List list) {
        int[] schoolcontact = {R.color.xjt_vote_result_11, R.color.xjt_vote_result_14, R.color.xjt_vote_result_12,
                R.color.xjt_vote_result_13, R.color.xjt_vote_result_15, R.color.xjt_vote_result_16,
                R.color.xjt_vote_result_17, R.color.xjt_vote_result_18,};
        if (list.size() < schoolcontact.length) {
            return schoolcontact[num];
        } else {
            if (num - schoolcontact.length >= 0) {
                return schoolcontact[num - schoolcontact.length];
            } else {
                return schoolcontact[num];
            }
        }
    }


    public static void subjectListUpdateLastComment(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet json_socket_topicComment, JsonGetSubjectListRet.GetSubjectListRet getSubjectListRet) {
        JsonGetSubjectListRet.LastComment lastComment = new JsonGetSubjectListRet.LastComment();

        lastComment.nickName = json_socket_topicComment.sender;
        if (json_socket_topicComment.subType == MyConfig.SUBTYPE_TEXT_COMMENT) {
            lastComment.message = json_socket_topicComment.info.message;
            lastComment.type = MyConfig.SUBTYPE_TEXT_COMMENT;
        } else if (json_socket_topicComment.subType == MyConfig.SUBTYPE_IMAGE_COMMENT) {
            lastComment.message = app.getString(R.string.menu_plus_gallery);
            lastComment.type = MyConfig.SUBTYPE_IMAGE_COMMENT;
        }

        getSubjectListRet.comments.clear();
        getSubjectListRet.comments.add(lastComment);
    }

    public static void leaveGroup(final JsonLeaveGroup jsonLeaveGroup) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonLeaveGroup, JsonLeaveGroup.class);
                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LEAVE_GROUP + jsonLeaveGroup.groupId + MyConfig.API2_LEAVE_GROUP_2, jsonStr);
                final JsonLeaveGroupRet jsonLeaveGroupRet = gson.fromJson(s, JsonLeaveGroupRet.class);
                MyLog.d("", "HttpTools: JsonLeaveRet " + s);
                if (jsonLeaveGroupRet != null && jsonLeaveGroupRet.code == MyConfig.retSuccess()) {
                    if (null != MyConfig.activityMainGroup) {
                        MyConfig.activityMainGroup.finish();
                    }

                    EventBus.getDefault().post(new EventGroupListUpdateFromServer());
                }

            }
        }).start();

    }

    public static void GroupTransferOwner(final JsonTransferGroupOwner jsonTransferGroupOwner) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonTransferGroupOwner, JsonTransferGroupOwner.class);
                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_OWNER_TRANCEFER + jsonTransferGroupOwner.groupId
                        + "/transfer/" + jsonTransferGroupOwner.userId, jsonStr);
                final JsonTransferGroupOwnerRet jsonTransferGroupOwnerRet = gson.fromJson(s, JsonTransferGroupOwnerRet.class);
                MyLog.d("", "HttpTools: JsontransferownerRet " + s);
                if (jsonTransferGroupOwnerRet != null && jsonTransferGroupOwnerRet.code == MyConfig.retSuccess()) {
                    if (null != MyConfig.activityMainGroup) {
                        MyConfig.activityMainGroup.finish();
                    }

                    EventBus.getDefault().post(new EventGroupListUpdateFromServer());

                }

            }
        }).start();

    }

    public static void DeleteDirectTalk(final JsonDeleteGroup jsonDeleteGroup) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonDeleteGroup, JsonDeleteGroup.class);
                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_DELETE_DIRECTCHAT +
                        jsonDeleteGroup.groupId, jsonStr);
                final JsonDeleteGroupRet jsonDeleteGroupRet = gson.fromJson(s, JsonDeleteGroupRet.class);
                MyLog.d("", "HttpTools: JsonDeleteRet " + s);
                if (jsonDeleteGroupRet != null && jsonDeleteGroupRet.code == MyConfig.retSuccess()) {
                    if (null != MyConfig.activityMainGroup) {
                        MyConfig.activityMainGroup.finish();
                    }

                    EventBus.getDefault().post(new EventGroupListUpdateFromServer());

                }

            }
        }).start();

    }
    public static void DeleteGroup(final JsonDeleteGroup jsonDeleteGroup) {

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Gson gson = new Gson();
                            String jsonStr = gson.toJson(jsonDeleteGroup, JsonDeleteGroup.class);
                            final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_DELETE_GROUP +
                                    jsonDeleteGroup.groupId, jsonStr);
                            final JsonDeleteGroupRet jsonDeleteGroupRet = gson.fromJson(s, JsonDeleteGroupRet.class);
                            MyLog.d("", "HttpTools: JsonDeleteRet " + s);
                            if (jsonDeleteGroupRet != null && jsonDeleteGroupRet.code == MyConfig.retSuccess()) {
                                if (null != MyConfig.activityMainGroup) {
                                    MyConfig.activityMainGroup.finish();
                                }

                                EventBus.getDefault().post(new EventGroupListUpdateFromServer());

                            }

                        }
                    }).start();

    }

    /**
     * 弹出对话框
     */
    public static void showRemindDialog(final String titlestr, final String remindstr, final Context mContext, boolean haveCancle, final IOnOkClickListener listener) {
        View dialogView = LayoutInflater.from(mContext).inflate(
                R.layout.dialog_exit_remind_layout, null);
        final Dialog setDialog = new Dialog(mContext, R.style.DialogStyle);
        setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialog.setCancelable(false);
        setDialog.getWindow().setContentView(dialogView);
        WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
        lp.width = MyConfig.getWidthByScreenPercent(80);
        setDialog.getWindow().setAttributes(lp);
        final Button cancleButton = (Button) dialogView.findViewById(R.id.cancle);
        cancleButton.setVisibility(haveCancle ? View.VISIBLE : View.GONE);
        Button OkButton = (Button) dialogView.findViewById(R.id.login);
        TextView remindText = (TextView) dialogView.findViewById(R.id.remind_text);
        remindText.setText(remindstr);
        TextView titletext = (TextView) dialogView.findViewById(R.id.remind_title);
        titletext.setText(titlestr);
        titletext.setVisibility(titlestr.length() > 0 ? View.VISIBLE : View.GONE);
        OkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (null != listener) {
                    listener.OnOkClickListener();
                }
                setDialog.cancel();
            }
        });
        cancleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialog.cancel();
            }
        });
        setDialog.setCanceledOnTouchOutside(false);
        setDialog.show();
    }

    //    public static IOnOkClickListener onOkClickListener;
    public interface IOnOkClickListener {
        void OnOkClickListener();
    }

    public static String getSubtypeName(int type, int subType) {
        String subTypeName = new String();
        if (type == TYPE_TOPIC)
            switch (subType) {
                case SUBTYPE_VIDEO:
                    subTypeName = app.getString(R.string.menu_plus_video);
                    break;
                case SUBTYPE_IMAGE:
                    subTypeName = app.getString(R.string.menu_plus_gallery);
                    break;
                case SUBTYPE_MAP:
                    subTypeName = app.getString(R.string.menu_plus_map);
                    break;
                case SUBTYPE_VOTE:
                case SUBTYPE_VOTE_CLOSE:
                    subTypeName = app.getString(R.string.menu_plus_vote);
                    break;
                case SUBTYPE_TEXT:
                    subTypeName = app.getString(R.string.menu_plus_subject);
                    break;
                case SUBTYPE_FILE:
                    subTypeName = app.getString(R.string.menu_plus_file);
                    break;
                case SUBTYPE_LINK:
                    subTypeName = app.getString(R.string.menu_plus_link);
                    break;
                case SUBTYPE_ACTIVITY:
                    subTypeName = app.getString(R.string.menu_plus_activity);
                    break;
            }
        return subTypeName;
    }

    public final static int GROUP_LIST_TYPE_SEARCH_GROUP = 0;
    public final static int GROUP_LIST_TYPE_GROUP_SIMPLE_INFO = 1;

    public final static String CONST_PARAM_STR_OS = "os";
    public final static String CONSTANT_PARAMETER_STRING_CLIENTID = "clientId";
    public final static String CONSTANT_PARAMETER_STRING_DURATION = "duration";
    public final static String CONSTANT_PARAMETER_STRING_GROUPID = "groupId";
    public final static String CONSTANT_PARAMETER_STRING_NAME = "name";
    public final static String CONSTANT_PARAMETER_STRING_AUDIO = "audio";
    public final static String CONSTANT_PARAMETER_STRING_DESC = "desc";
    public final static String CONSTANT_PARAMETER_STRING_MAINROOM_START_MODE = "mainroom_started_mode";
    public final static String CONST_PARAM_STR_CONTACT_UNIVERAL = "contactUniversal";
    public final static String CONSTANT_PARAMETER_STRING_GETGROUPINFORET = "getGroupUserRet";
    public final static String CONSTANT_PARAMETER_STRING_BUNDLE = "bundle";

    public final static String CONSTANT_PARAMETER_STRING_COMPANYID = "CompanyId";
    public final static String CONSTANT_PARAMETER_STRING_USERCOUNT = "userCount";
    public final static String CONSTANT_PARAMETER_STRING_CREATORID = "creatorId";
    public final static String CONSTANT_PARAMETER_STRING_CREATORNICK = "creatorNick";
    public final static String CONSTANT_PARAMETER_STRING_JOINED = "joined";
    public final static String CONSTANT_PARAMETER_STRING_TOPICID = "topicId";
    public final static String CONSTANT_PARAMETER_STRING_TITLE = "title";
    public final static String CONSTANT_PARAMETER_STRING_BODY = "body";
    public final static String CONSTANT_PARAMETER_STRING_NEWS = "news";
    public final static String CONST_PARAM_STR_SHARE_IN_CATEGORY = "share_in_mode";
    public final static String CONST_STRING_PARAM_ATTACHED = "attached";
    public final static String CONSTANT_PARAMETER_STRING_ACTIVITYTITLE = "activitytitle";
    public final static String CONST_STRING_PARAM_GROUPCOUNT = "groupCount";

    public final static String CONSTANT_ATTRIBUTE_STRING_CROP = "crop";
    public final static String CONSTANT_ATTRIBUTE_STRING_ASPECTX = "aspectX";
    public final static String CONSTANT_ATTRIBUTE_STRING_ASPECTY = "aspectY";
    public final static String CONSTANT_ATTRIBUTE_STRING_OUTPUTX = "outputX";
    public final static String CONSTANT_ATTRIBUTE_STRING_OUTPUTY = "outputY";
    public final static String CONSTANT_ATTRIBUTE_STRING_RETURN_DATA = "return-data";
    public final static String CONSTANT_ATTRIBUTE_STRING_SCALE = "scale";


    /**
     * 定义的回调接口，当访问数据完成后回调。
     */
    public interface IF_AfterHttpRequest {
        void doAfterHttpRequest();
    }


    /**
     * 得到当前班级的通讯录
     *
     * @param groupId            所在组的id，最早是根据id获得，现在更改了接口，不需要id了。但也没有去掉。再次调用这个方法时传递0即可。
     * @param IFAfterHttpRequest 回调接口对象，当数据获取成功后的操作写在这个接口的抽象方法中。
     */

    public static void getCurrentYearUserAddressBooks(int groupId, final IF_AfterHttpRequest IFAfterHttpRequest) {
        if (MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {
            getCurrentYearUserAddressBooksByURLConnection(groupId, IFAfterHttpRequest);
        } else {
            getCurrentYearUserAddressBooksByOKHttp(groupId, IFAfterHttpRequest);
        }


    }

    /**
     * 使用URLConnection获取当前班级的通讯录。这个是最早写的方法。根据被调用处可以看出这个方法不可能被执行，目前就放在这儿。
     *
     * @param groupId
     * @param IFAfterHttpRequest
     */
    public static void getCurrentYearUserAddressBooksByURLConnection(int groupId, final IF_AfterHttpRequest IFAfterHttpRequest) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonGetCompanyUserList jsonGetCompanyUserList = new JsonGetCompanyUserList();
                jsonGetCompanyUserList.accessToken = MyConfig.usr_token;
                //jsonGetCompanyUserList.id = groupId;

//                String jsonStr = gson.toJson(jsonGetCompanyUserList, JsonGetCompanyUserList.class);

                final String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_ADDRESSBOOKS_CURRENTYEARUSER, MyConfig.usr_token);

                final JsonGetCompanyUserListRet jsonGetCompanyUserListRet = gson.fromJson(s, JsonGetCompanyUserListRet.class);
                MyConfig.jsonGetCompanyUserListRet = jsonGetCompanyUserListRet;
                if (jsonGetCompanyUserListRet != null && jsonGetCompanyUserListRet.code == MyConfig.retSuccess()) {
                    if (null != IFAfterHttpRequest) {
                        IFAfterHttpRequest.doAfterHttpRequest();
                    }
                }
            }
        }).start();
    }

    /**
     * 使用OKHttp获取当前班级的通讯录，根据被调用处可以看出获取通讯录时一直实行这个方法。
     *
     * @param groupId
     * @param IFAfterHttpRequest
     */
    public static void getCurrentYearUserAddressBooksByOKHttp(int groupId, final IF_AfterHttpRequest IFAfterHttpRequest) {
        Call<JsonGetCompanyUserListRet> repos = HttpTools.http_api_service().getContact();
        repos.enqueue(new Callback<JsonGetCompanyUserListRet>() {
            @Override
            public void onResponse(Call<JsonGetCompanyUserListRet> call, Response<JsonGetCompanyUserListRet> response) {
                // Get result Repo from response.body()

                MyLog.d("", "retrofit: message() " + response.message());

                if (response.code() == 200) {

                    MyConfig.jsonGetCompanyUserListRet = response.body();

                    MyLog.d("", "retrofit: body() 2 " + response.body().code);
                    MyLog.d("", "retrofit: body() 3 " + response.body().message);

                    if (response.body().data != null) {
//                       if (null != iGetUserList) {
                        IFAfterHttpRequest.doAfterHttpRequest();
                    }

                }

            }


            @Override
            public void onFailure(Call<JsonGetCompanyUserListRet> call, Throwable t) {
                MyLog.d("", "retrofit: onFailure " + t.getMessage());
            }
        });
    }

    public static boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    public static class HttpRedirected {
        public String url_redirected;
        public HttpURLConnection conn_redirected;

        public HttpRedirected(String url, HttpURLConnection conn) {
            this.url_redirected = url;
            this.conn_redirected = conn;
        }
    }

    public static HttpRedirected getHttpURLConnectionRedirected(final String url) {

        /**
         * 关于url跳转
         *
         *  其一，哪些会跳转，怎样跳转
         *      凡是用户上传的文件或图片资源都会跳转
         *      都是http跳转，不会跨协议跳到https
         *
         *  其二，不跨协议，仅仅在http范围内的话
         *      okhttp会自动跳转
         *      Picasso会自动跳转
         *      HttpURLConnection似乎不能自动跳转
         *
         *  其三
         *      本函数的跳转涉及网络，很可能导致阻塞，所以应该放在UI线程之外运行
         *
         */

        String url_redirected = url;
        HttpURLConnection conn_redirected = null;

        try {

            for (int i = 0; i < 5; i++) {//最多只跳转5次

                conn_redirected = (HttpURLConnection) (new URL(url_redirected).openConnection());
                conn_redirected.setInstanceFollowRedirects(false);
                conn_redirected.setConnectTimeout(15000);
                conn_redirected.setReadTimeout(15000);
                conn_redirected.connect();

                int responseCode = conn_redirected.getResponseCode();
                switch (responseCode) {
                    case HttpURLConnection.HTTP_MOVED_PERM:
                    case HttpURLConnection.HTTP_MOVED_TEMP:
                        url_redirected = conn_redirected.getHeaderField("Location");
                        continue;

                        /**
                         * 留意何为relative url
                         */
//                            location = conn.getHeaderField("Location");
//                            base     = new URL(url);
//                            next     = new URL(base, location);  // Deal with relative URLs
//                            url      = next.toExternalForm();
                }
                break;
            }
        } catch (Exception e) {

        }

        return new HttpRedirected(url_redirected, conn_redirected);
    }


    public static String guessMimeByFileExt(String fileName) {

        /**
         * 假定能够得到子类型
         *  直接返回子类型
         *  假定不能得到子类型
         *  返回"主类型/*"
         */


        /**
         * 先判断常见扩展名
         */
        String ext = MyConfig.getExtFromPathOrName(fileName);
        if (ext == null) {
            return "*/*";
        } else if (ext.startsWith("pdf")) {
            return "pdf";
        } else if (ext.startsWith("doc")) {
            return "doc";
        } else if (ext.startsWith("ppt")) {
            return "ppt";
        } else if (ext.startsWith("txt")) {
            return "txt";
        } else if (ext.startsWith("zip")) {
            return "zip";
        } else {
            /**
             * 再根据mime
             */
            String mime = MyConfig.guessMimeByFilename(fileName);

//            if(mime == null){
//                return "*/*";
//            }else{
//                return mime;
//            }


            if (mime == null) {
                return "*/*";
            } else if (mime.startsWith(MyConfig.MIME_FILE_PREFIX)) {
                return "file/*";
            } else if (mime.startsWith(MyConfig.MIME_TEXT_PREFIX)) {
                return "text/*";
            } else if (mime.startsWith(MyConfig.MIME_AUDIO_PREFIX)) {
                return "audio/*";
            } else if (mime.startsWith(MyConfig.MIME_VIDEO_PREFIX)) {
                return "vedio/*";
            } else if (mime.startsWith(MyConfig.MIME_IMAGE_PREFIX)) {
                return "image/*";
            } else {
                return "*/*";
            }
        }
    }


    public static String guessMimeByFilename(String filename) {
        return HttpURLConnection.guessContentTypeFromName(filename);
    }

    public static String guessMimeByUrlStream(String strUrl) {
        MyLog.d("", "guessTypeFromStream strUrl:" + strUrl);
        BufferedInputStream bis = null;
        HttpURLConnection urlconnection = null;
        URL url = null;
        String type = null;
        try {
            url = new URL(strUrl);
            urlconnection = (HttpURLConnection) url.openConnection();
            urlconnection.connect();
            bis = new BufferedInputStream(urlconnection.getInputStream());
            type = HttpURLConnection.guessContentTypeFromStream(bis);
        } catch (Exception e) {

        }

        MyLog.d("", "guessTypeFromStream ext:" + type);
        return type;
    }

    public static String byteConvert(long bytes) {
        return humanReadableByteCount(bytes, true);
    }

    //copy from http://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java
    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }


    public static void mkAppDir() {
        File appDirFile = new File(MyConfig.pathRoot, MyConfig.appDirName);
        if (!MyConfig.mkDir(appDirFile)) {
            return;
        }
        MyConfig.appDirPath = appDirFile.getAbsolutePath();

        File attachedDirFile = new File(MyConfig.appDirPath, MyConfig.attachedDirName);

        if (!MyConfig.mkDir(attachedDirFile)) {
            return;
        }
        MyConfig.appCacheDirPath = attachedDirFile.getAbsolutePath();
    }

    public static void cleanAppDir() {

        MyConfig.deleteFileOrDir(new File(MyConfig.appDirPath));

        mkAppDir();

    }

    public static boolean deleteFileOrDir(File file) {

        if (file.isFile()) {
            return file.delete();
        }

        if (file.isDirectory()) {
            File files[] = file.listFiles();
            for (File f : files) {
                if (!deleteFileOrDir(f)) {
                    return false;
                }
            }
            return file.delete();
        }
        return false;
    }

    public static boolean mkDir(File dirFile) {
        if (dirFile.exists()) {
            if (dirFile.isDirectory()) {
                return true;
            } else {
                try {
                    dirFile.delete();
                } catch (Exception e) {
                    MyConfig.MyToast(0, app,
                            app.getResources().getString(
                                    R.string.error_make_app_dir_failed));
                    return false;
                }
            }
        }

        try {
            dirFile.mkdir();
            return true;
        } catch (Exception e) {
            MyConfig.MyToast(0, app,
                    app.getResources()
                            .getString(R.string.error_make_app_dir_failed));
            return false;
        }
    }

    public static File getLocalFileByUrlAndFileName(String url, String fileNameReplaced) { //可以指定文件名，而不是用url里面的可能被处理过的文件名
        /**
         * 按文件的url的hash值给其创建一个唯一的目录，唯一的url对应唯一的目录，这样就可以判断一个文件是否已经下载过
         * 在新建目录中创建同名的File对象并返回
         */

        File localDir = new File(appCacheDirPath, "" + url.hashCode());

        mkDir(localDir);

        if (fileNameReplaced == null) {
            return new File(localDir, getFilenameFromPath(url));
        } else {
            return new File(localDir, fileNameReplaced);
        }

    }

    public static File getLocalFileByHashcodeAndFileName(String hashcode, String fileName) { //可以指定文件名，而不是用url里面的可能被处理过的文件名
        /**
         * 根据hash值创建文件夹
         * 再创建同名文件
         * 所以如果一个文件与另外一个文件的hash值相同，并且名字相同，则认为是同一个文件
         */

        File localDir = new File(appCacheDirPath, "" + hashcode.substring(0, 12));//假定hashcode长度大于12

        mkDir(localDir);

        if (fileName == null) {
            return new File(localDir, getFilenameFromPath(hashcode.substring(12, 16)));
        } else {
            return new File(localDir, fileName);
        }

    }

    public static int getFileDownloadProgress(String local, int fileSize) {
        if (local == null) {
            return 0;
        }
        /**
         * 判断文件是否已经或曾经下载
         */
        try{
            File f = new File(local);
            if ((f != null) && f.exists()) {
                MyLog.d("", "share in process: local size " + f.length() + ", server size " + fileSize);

                if (fileSize == 0) {
                    return 100;
                }

                if (f.length() <= fileSize) {
                    return (int) ((f.length() * 100) / fileSize);
                } else {
                    return -1;//something wrong
                }
            } else {
                return 0;
            }
        }catch(Exception e){
            return 0;
        }
    }

    //download action
//    public static final int FILE_DOWNLOAD_ONLY = 0;
//    public static final int FILE_DOWNLOAD_SHARE = 1;
    //download status
    public static final int FILE_DOWNLOAD_BEFORE = 0;
    public static final int FILE_DOWNLOAD_ING = 1;
    public static final int FILE_DOWNLOAD_STOPED = 2;

    /**
     * Background Async Task to download file
     */
    public static class DownloadFileFromUrl extends AsyncTask<String, Integer, Void> {
        /**
         * 注意，local地址可能在子文件夹里面，此刻子文件夹必须预先存在
         * 否则，『OutputStream output = new FileOutputStream(des_url)』这一步会失败
         */
        public static class DownloadFileFromUrlSenderInfo {

            public String sender;
            public int code;

            public DownloadFileFromUrlSenderInfo(String sender, int code) {
                this.sender = sender;
                this.code = code;
            }
        }

        public DownloadFileFromUrlSenderInfo downloadFileFromUrlSenderInfo;

        public DownloadFileFromUrl(DownloadFileFromUrlSenderInfo downloadFileFromUrlSenderInfo) {
            this.downloadFileFromUrlSenderInfo = downloadFileFromUrlSenderInfo;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(String... f_url) {
            String src_url = f_url[0];
            String des_url = f_url[1];
            int count;
            int progress = 0;

            MyLog.d("", "share out, DownloadFileFromUrl 1: src " + src_url + " des " + des_url);

            try {

                EventBus.getDefault().post(new EventFileDownloadProgress(src_url, des_url, progress, FILE_DOWNLOAD_BEFORE, downloadFileFromUrlSenderInfo));

                HttpURLConnection connection = MyConfig.getHttpURLConnectionRedirected(src_url).conn_redirected;
                connection.connect();

                int lenghtOfFile = connection.getContentLength();
                MyLog.d("", "share out, DownloadFileFromUrl 2: lenghtOfFile " + lenghtOfFile);

                /**
                 *  如果已经下载就不重复下载
                 */
                int download_progress = getFileDownloadProgress(des_url, lenghtOfFile);

                MyLog.d("", "share out, DownloadFileFromUrl 2: download_progress " + download_progress);

                if (download_progress == 100) { //已经100%下载了
                    progress = 100;
                } else {//清除以前的，重新下载
                    MyLog.d("", "share out, DownloadFileFromUrl 31: download_progress " + download_progress);

                    InputStream input = connection.getInputStream();
                    MyLog.d("", "share out, DownloadFileFromUrl 32: download_progress " + download_progress);
                    OutputStream output = new FileOutputStream(des_url);

                    MyLog.d("", "share out, DownloadFileFromUrl 33: download_progress " + download_progress);

                    byte data[] = new byte[1024];
                    long total = 0;

                    int last_progress = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        progress = (int) ((total * 100) / lenghtOfFile);
                        publishProgress(progress);

                        if (progress != last_progress) {
                            //不能太过频繁调用EventBus
                            last_progress = progress;
                            EventBus.getDefault().post(new EventFileDownloadProgress(src_url, des_url, progress, FILE_DOWNLOAD_ING, downloadFileFromUrlSenderInfo));
//                            MyLog.d("", "share out, DownloadFileFromUrl: " + progress);
                        }

                        output.write(data, 0, count);
                    }

                    MyLog.d("", "share out, DownloadFileFromUrl 4: download_progress " + download_progress);

                    output.flush();
                    output.close();
                    input.close();

                    MyLog.d("", "share out, DownloadFileFromUrl 5: download_progress " + download_progress);
                }

            } catch (Exception e) {
                MyLog.d("", "file download exception: " +e.getMessage());
            }

            EventBus.getDefault().post(new EventFileDownloadProgress(src_url, des_url, progress, FILE_DOWNLOAD_STOPED, downloadFileFromUrlSenderInfo));
            MyLog.d("", "share out, DownloadFileFromUrl download stoped");
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected void onPostExecute(Void v) {

            /**
             * 暂时不用downloadFinishedListener
             * 这是因为progress进度广播是通过EventBus，这个比较慢到达
             * 通过progress来传递downloadFinishedListener可以保证时序不错乱
             */


//            if(downloadFinishedListener != null){
//                downloadFinishedListener.onDownloadFinished();
//            }
        }

    }

    /**
     * 文字颜色
     */
    public static void addForeColorSpan(String spanStr, int color, int beginIndex, int endIndex, TextView view) {
//        SpannableString spanString = new SpannableString(spanStr);
        SpannableString spanString = EmojiUtils.showEmoji(app,spanStr);
        ForegroundColorSpan span = new ForegroundColorSpan(color);
        spanString.setSpan(span, beginIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        view.setText(spanString);
    }


    public static void getAppVersionFromLocal() {

        try {
            PackageManager packageManager = app.getPackageManager();
            // getPackageName()是你当前类的包名，0代表是获取版本信息
            PackageInfo packInfo = packageManager.getPackageInfo(app.getPackageName(), 0);
            MyConfig.appVersion_from_local.versionName = packInfo.versionName;
            MyConfig.appVersion_from_local.versionCode = packInfo.versionCode;
            MyLog.d("", "appVersion_from_local.versionName " + MyConfig.appVersion_from_local.versionName + " appVersion_from_local.versionCode " + MyConfig.appVersion_from_local.versionCode);//1.0 1
        } catch (Exception e) {
            MyLog.d("", "appVersion_from_local.versionName Exception");

        }

    }

    public static DialogFragment appVersionUpdateReminderDialogFragment;

    public static void appVersionUpdateReminder() {

//        if (MyConfig.appVersion_from_server.versionCode > MyConfig.appVersion_from_local.versionCode) {
        if (MyConfig.activityViewPager != null) {

            if (appVersionUpdateReminderDialogFragment != null) {
                appVersionUpdateReminderDialogFragment.dismiss();
            }

            appVersionUpdateReminderDialogFragment = new DialogFragment() {

                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                }

                @Override
                public void onDestroy() {
                    super.onDestroy();
                }

                @Override
                public Dialog onCreateDialog(Bundle savedInstanceState) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            getActivity());
                    builder.setTitle(app.getString(R.string.have_new_version) + MyConfig.appVersion_from_server_current.versionName + ", " + app.getString(R.string.your_now_version) + MyConfig.appVersion_from_local.versionName)
                            .setPositiveButton(getString(R.string.prompt_confirm), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    MyConfig.updateAppDownloadUrl(new MyConfig.CallBack_IfSuccess() {
                                        @Override
                                        public void ifSuccess() {
                                            Uri uri = Uri.parse(appDownloadUrl);
                                            MyConfig.activityViewPager.startActivity(new Intent(Intent.ACTION_VIEW, uri));
                                        }

                                        @Override
                                        public void ifFailed(){

                                        }
                                    });
                                }
                            })
                            .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    return builder.create();
                }
            };
            appVersionUpdateReminderDialogFragment.show(MyConfig.activityViewPager.getFragmentManager(), "");


        }
    }

    public interface CallBack_IfSuccess {
        void ifSuccess();
        void ifFailed();
    }

    public static void updateAppDownloadUrl(final CallBack_IfSuccess callBack_If_success){


        new Thread(new Runnable() {
            @Override
            public void run() {

                final String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_APPDOWNLOADURL, MyConfig.usr_token);

                JsonGetAppDownloadUrlRet jsonGetAppDownloadUrlRet = new Gson().fromJson(s, JsonGetAppDownloadUrlRet.class);

                if (jsonGetAppDownloadUrlRet != null && jsonGetAppDownloadUrlRet.code == MyConfig.retSuccess()) {

                    MyConfig.appDownloadUrl = jsonGetAppDownloadUrlRet.data;

                    if(callBack_If_success != null){
                        callBack_If_success.ifSuccess();
                    }

                }else {

                    if (callBack_If_success != null) {
                        callBack_If_success.ifFailed();
                    }
                }

            }
        }).start();


    }

    //public static String appDownloadUrl = "http://docy.co";
    public static String appDownloadUrl = "http://docy.co/";
    public static AppVersion appVersion_from_local = new AppVersion();
    public static AppVersion appVersion_from_server_last = new AppVersion();
    public static AppVersion appVersion_from_server_current = new AppVersion();
    public static String appVersion_reminded_day;

    public static void saveAppVersionRemindedDay(Context context) {
        if (prefEditor == null) {
            initPreferenceOperation(context);
        }
        MyConfig.prefEditor.putString("appVersion_reminded_day", appVersion_reminded_day);
        MyConfig.prefEditor.commit();
    }

    public static void loadAppVersionRemindedDay(Context context) {
        if (prefShared == null) {
            initPreferenceOperation(context);
        }
        appVersion_reminded_day = MyConfig.prefShared.getString("appVersion_reminded_day", null);
        return;
    }

    public static void saveAppVersionCode(Context context) {
        if (prefEditor == null) {
            initPreferenceOperation(context);
        }
        MyConfig.prefEditor.putInt("appVersion_from_server_last", appVersion_from_server_current.versionCode);
        MyConfig.prefEditor.commit();
    }

    public static void loadAppVersionCode(Context context) {

        MyConfig.getAppVersionFromLocal();

        if (prefShared == null) {
            initPreferenceOperation(context);
        }
        appVersion_from_server_last.versionCode = MyConfig.prefShared.getInt("appVersion_from_server_last", -1);
        return;
    }


    public static void appVersionCheck() {


        new Thread(new Runnable() {
            @Override
            public void run() {

                final String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_VERSION, MyConfig.usr_token);
                MyLog.d("", "appVersionCheck: API_VERSION " + s);
                jsonGetVersionsRet_from_server = new Gson().fromJson(s, JsonGetVersionsRet.class);
                if (jsonGetVersionsRet_from_server != null && jsonGetVersionsRet_from_server.code == MyConfig.retSuccess()) {

                    MyConfig.appVersion_from_server_current.versionName = jsonGetVersionsRet_from_server.data.android.versionName;
                    MyConfig.appVersion_from_server_current.versionCode = jsonGetVersionsRet_from_server.data.android.versionCode;
//                    MyConfig.appVersion_from_server_current.versionCode = 300;

                    loadAppVersionCode(app);
                    EventBus.getDefault().post(new EventAppVersionCheckInfo(
                            appVersion_from_local.versionCode,
                            appVersion_from_server_last.versionCode,
                            appVersion_from_server_current.versionCode,
                            appVersion_reminded_day));
                    MyLog.d("", "appVersionCheck: " + appVersion_from_local.versionCode + "/" +
                            appVersion_from_server_last.versionCode + "/" +
                            appVersion_from_server_current.versionCode + "/" +
                            appVersion_reminded_day);
                    saveAppVersionCode(app);
                }

            }
        }).start();

    }

    public static void eventbusDelayPost(final Object o, final int ms) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    Thread.sleep((long) ms);
                    EventBus.getDefault().post(o);

                } catch (Exception e) {

                }

            }
        }).start();

    }


    public static String getTopAppPackageName() {

        /**
         * from: http://stackoverflow.com/questions/27915956/how-to-get-topactivity-name-in-android-lollipop
         */

        final ActivityManager am = (ActivityManager) app.getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);
        if (am != null) {
            String topAppPackageName = "";
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                //For above Kitkat version
                List<ActivityManager.RunningAppProcessInfo> tasks = am
                        .getRunningAppProcesses();
                if (tasks.get(0).importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    topAppPackageName = tasks.get(0).processName;
//                    System.out.println("topActivityName1 : " + topAppPackageName);
                    return topAppPackageName;
                }
            } else {
                topAppPackageName = am.getRunningTasks(1).get(0).topActivity
                        .getClassName();
//                System.out.println("topActivityName2 : " + topAppPackageName);
                return topAppPackageName;
            }
        }
        return null;

    }

    public static double Amplitudescale(double Amplitude) {
        double amp = (Math.log10(Amplitude) - 1);
        if (amp < 0) {
            amp = 0;
        }
        amp *= 5;
        return amp;
    }

    public static final int SOUND_LEVEL_0 = 0;
    public static final int SOUND_LEVEL_1 = 3;
    public static final int SOUND_LEVEL_2 = 4;
    public static final int SOUND_LEVEL_3 = 9;
    public static final int SOUND_LEVEL_4 = 12;
    public static final int SOUND_LEVEL_5 = 15;
    public static final int SOUND_LEVEL_6 = 18;
    public static final int MAX_RECORD_TIME = 60;

    public static String CREATE_SUBJECT_TITLE;//todo: 通过全局变量来传递这个title是不好的设计

    public static final int SUBJECT_ATTACHED_IMAGE = 0;
    public static final int SUBJECT_ATTACHED_FILE = 1;
    public static final int SUBJECT_ATTACHED_MAP = 2;
    public static final int SUBJECT_ATTACHED_VIDEO = 3;
    public static final String SUBJECT_ATTACHED_TYPE = "attachedType";


    public static void restartApp(Context context) {

        restartAppKillProcess(context);
    }

    private static void restartAppKillProcess(Context context) {

        Intent mStartActivity = new Intent(context, ActivityStartPage.class);
        mStartActivity.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS | Intent.FLAG_ACTIVITY_NEW_TASK);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 200, mPendingIntent);

//        ProcessPhoenix.triggerRebirth(context);
//        Runtime.getRuntime().exit(0);
//        System.exit(1);
//        android.os.Process.killProcess(android.os.Process.myPid());
        EventBus.getDefault().post(new EventAppQuit());
    }

    private static void recreateActivity() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(new EventActivityRecreate());
            }
        }).start();
    }

    public static Country getCountryByCode(String s, LinkedList<Country> linkedList) {
        for (Country country : linkedList) {
            if (s.equals(country.code)) {
                return country;
            }
        }
        return null;
    }

    public static LinkedList<Country> supportedCountriesList = new LinkedList<>();

    public static LinkedList<Country> getSupportedCountries(Context context) {

        /**
         * 暂时仅仅支持中国、美国、加拿大
         */

        Country us = new Country("US", "United States (USA)", "+1");
        us.flag = R.drawable.country_flag_us;
        supportedCountriesList.add(us);

        Country ca = new Country("CA", "Canada", "+1");
        ca.flag = R.drawable.country_flag_ca;
        supportedCountriesList.add(ca);

        Country cn = new Country("CN", "中国", "+86");
        cn.flag = R.drawable.country_flag_cn;
        supportedCountriesList.add(cn);

        return supportedCountriesList;

    }

    public static List<Country> allCountriesList;

    public static List<Country> getAllCountries(Context context) {
        if (allCountriesList == null) {
            try {
                allCountriesList = new ArrayList<Country>();

                // Read from local file
                String allCountriesString = readFileAsString(context);
                MyLog.d("", "countrypicker country: " + allCountriesString);
                JSONObject jsonObject = new JSONObject(allCountriesString);
                Iterator<?> keys = jsonObject.keys();

                // Add the data to all countries list
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    Country country = new Country();
                    country.code = key;
                    country.name = jsonObject.getString(key);
                    MyLog.d("", "countrypicker country list: " + country.code + " " + country.name);
                    allCountriesList.add(country);
                }

                Collections.sort(allCountriesList, new Comparator<Country>() {
                    @Override
                    public int compare(Country o1, Country o2) {
                        return Collator.getInstance().compare(o1.name, o2.name);
                    }
                });

                // Return
                return allCountriesList;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static String readFileAsString(Context context)
            throws java.io.IOException {
        String base64 = context.getResources().getString(R.string.countries);
        byte[] data = Base64.decode(base64, Base64.DEFAULT);
        return new String(data, "UTF-8");
    }

    public static final int LOGOUT_EVENT_TYPE_INVALIDE_TOKEN = 0;
    public static final int LOGOUT_EVENT_TYPE_LOGOUT = 1;
    public static final int LOGOUT_EVENT_TYPE_LOGOUT_RESET_DATE =2;
    public static boolean NOTICE_UNREADED = false;
    public static boolean FRIEND_CRICLE_UNREADED = false;


    public static CalendarWeek getWeekIncludeThisDay(LocalDate localDate) {

        LocalDate monday = localDate.withDayOfWeek(DateTimeConstants.MONDAY);
        LocalDate tuesday = localDate.withDayOfWeek(DateTimeConstants.TUESDAY);
        LocalDate wednesday = localDate.withDayOfWeek(DateTimeConstants.WEDNESDAY);
        LocalDate thursday = localDate.withDayOfWeek(DateTimeConstants.THURSDAY);
        LocalDate friday = localDate.withDayOfWeek(DateTimeConstants.FRIDAY);
        LocalDate saturday = localDate.withDayOfWeek(DateTimeConstants.SATURDAY);
        LocalDate sunday = localDate.withDayOfWeek(DateTimeConstants.SUNDAY);

        CalendarWeek calendarWeek = new CalendarWeek(
                monday,
                tuesday,
                wednesday,
                thursday,
                friday,
                saturday,
                sunday
        );
        calendarWeek.firstDayOfCurrentMonth = localDate.withDayOfMonth(1);
        calendarWeek.originDate = localDate;

        return calendarWeek;
    }


    public static CalendarMonth getMonthIncludeThisDay(LocalDate localDate) {


        CalendarMonth calendarMonth = new CalendarMonth();

        calendarMonth.firstDayOfCurrentMonth = localDate.withDayOfMonth(1);

        for (int i = 0; i < 6; i++) {//每个月最多6周，最少4周
            CalendarWeek w = getWeekIncludeThisDay(calendarMonth.firstDayOfCurrentMonth.plusDays(7 * i));

            if (i < 4) {
                calendarMonth.calendarWeeks.addLast(w);
            } else if (w.calendarDayList.getFirst().day.getMonthOfYear() == calendarMonth.firstDayOfCurrentMonth.getMonthOfYear()) {
                /**
                 * 从第5周开始检
                 * 如果w的第一天的月份等于本月第一天的月份，那么这一周也算本月的周
                 */
                calendarMonth.calendarWeeks.addLast(w);
            } else {
                break;
            }

        }
        calendarMonth.originDate = localDate;

        return calendarMonth;
    }

    public static boolean isWeixinAvilible(Context context) {
        final PackageManager packageManager = context.getPackageManager();// 获取packagemanager
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);// 获取所有已安装程序的包信息
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                if (pn.equals("com.tencent.mm")) {
                    return true;
                }
            }
        }

        return false;
    }

    public static void loginWeixin(final Activity activity) {
        //                com.umeng.socialize.utils.Log.LOG = true;

        UMWXHandler wxHandler = new UMWXHandler(activity, MyConfig.third_weixin_share_appID, MyConfig.third_weixin_share_appSecret);
        wxHandler.addToSocialSDK();

        // 首先在您的Activity中添加如下成员变量
        final UMSocialService mController = UMServiceFactory.getUMSocialService("com.umeng.login");

        mController.doOauthVerify(activity, SHARE_MEDIA.WEIXIN, new SocializeListeners.UMAuthListener() {
            @Override
            public void onStart(SHARE_MEDIA platform) {
                MyLog.d("TestData", "weixin login: onStart");
                Toast.makeText(activity, activity.getResources().getString(R.string.license_to_start), Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onError(SocializeException e, SHARE_MEDIA platform) {
                MyLog.d("TestData", "weixin login: onError");
                Toast.makeText(activity, activity.getResources().getString(R.string.authorization_error), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onComplete(Bundle value, SHARE_MEDIA platform) {
                MyLog.d("TestData", "weixin login: onComplate" + value.toString());

                MyConfig.usr_weixin_access_token = value.getString("access_token");
                MyLog.d("TestData", "weixin login: bundle value " + MyConfig.usr_weixin_access_token);
                MyConfig.usr_weixin_refresh_token = value.getString("refresh_token");
                MyLog.d("TestData", "weixin login: bundle value " + MyConfig.usr_weixin_refresh_token);
                MyConfig.usr_weixin_openid = value.getString("openid");
                MyLog.d("TestData", "weixin login: bundle value " + MyConfig.usr_weixin_openid);
                MyConfig.usr_weixin_expires_in = value.getString("expires_in");
                MyLog.d("TestData", "weixin login: bundle value " + MyConfig.usr_weixin_expires_in);
                MyConfig.usr_weixin_refresh_token_expires = value.getString("refresh_token_expires");
                MyLog.d("TestData", "weixin login: bundle value " + MyConfig.usr_weixin_refresh_token_expires);
                MyConfig.usr_weixin_uid = value.getString("uid");
                MyLog.d("TestData", "weixin login: bundle value " + MyConfig.usr_weixin_uid);
                MyConfig.usr_weixin_scope = value.getString("scope");
                MyLog.d("TestData", "weixin login: bundle value " + MyConfig.usr_weixin_scope);


                Toast.makeText(activity, activity.getResources().getString(R.string.authorization_to_complete), Toast.LENGTH_SHORT).show();
                //获取相关授权信息
                mController.getPlatformInfo(activity, SHARE_MEDIA.WEIXIN, new SocializeListeners.UMDataListener() {
                    @Override
                    public void onStart() {
                        MyLog.d("TestData", "weixin login: getPlatformInfo.onStart");
                        Toast.makeText(activity, activity.getResources().getString(R.string.get_platform_data_start), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete(int status, Map<String, Object> info) {
                        MyLog.d("TestData", "weixin login: getPlatformInfo.onComplate");
                        if (status == 200 && info != null) {
                            StringBuilder sb = new StringBuilder();
                            Set<String> keys = info.keySet();
                            for (String key : keys) {
                                sb.append(key + "=" + info.get(key).toString() + "\r\n");
                            }
                            MyLog.d("TestData", "weixin login " + sb.toString());
//                                    MyConfig.MyToast(1, ActivityAccount.this, "weixin login: " + sb.toString());
                        } else {
                            MyConfig.MyToast(1, activity, "weixin login error");
                            MyLog.d("TestData", "weixin login " + activity.getResources().getString(R.string.error_occurred) + status);
                        }

                        MyConfig.usr_weixin_name = info.get("nickname").toString();
                        MyConfig.usr_weixin_headimgurl = info.get("headimgurl").toString();
                        MyLog.d("TestData", "weixin login: usr_weixin_headimgurl " + MyConfig.usr_weixin_headimgurl);


                        new Thread(new Runnable() {

                            /**
                             * 微信授权之后的步骤
                             *      下载头像
                             *      登录服务器，获得token
                             *      用新token上传头像
                             *
                             */


                            @Override
                            public void run() {
                                JsonLoginUseWeixinData jsonLoginUseWeixinData = new JsonLoginUseWeixinData();
                                jsonLoginUseWeixinData.openid = MyConfig.usr_weixin_openid;
                                jsonLoginUseWeixinData.avatar = MyConfig.usr_weixin_headimgurl;
                                jsonLoginUseWeixinData.name = MyConfig.usr_weixin_name;
                                jsonLoginUseWeixinData.access_token = MyConfig.usr_weixin_access_token;

                                Gson gson = new Gson();
                                String json_str = gson.toJson(jsonLoginUseWeixinData);
                                MyLog.d("TestData", "weixin login: bundle value, get server send " + json_str);

                                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_WEIXINLOGIN, json_str);
                                MyLog.d("TestData", "weixin login: bundle value, get server ret " + s);

                                final JsonWeixinLoginRet jsonLoginRet = gson.fromJson(s, JsonWeixinLoginRet.class);
                                MyLog.d("TestData", "weixin login: bundle value, after 0 " + MyConfig.usr_weixin_headimgurl);

                                if (jsonLoginRet != null) {
                                    MyLog.d("TestData", "weixin login: bundle value, after 1 " + MyConfig.usr_weixin_headimgurl);

                                    if (jsonLoginRet.code == MyConfig.retSuccess()) {

                                        MyLog.d("TestData", "weixin login: bundle value, after 2 " + MyConfig.usr_weixin_headimgurl);
                                        MyConfig.getDataByOrder();

                                        MyConfig.usr_login_ret_json = s;
                                        MyConfig.jsonWeixinLoginRet = jsonLoginRet;
                                        MyConfig.usr_id = jsonLoginRet.data.id;
                                        MyConfig.usr_name = jsonLoginRet.data.name;
                                        MyConfig.usr_nickname = jsonLoginRet.data.nickName;
                                        MyConfig.usr_phone = jsonLoginRet.data.phone;
                                        MyConfig.usr_pwd = jsonLoginRet.data.password;
                                        MyConfig.usr_token = jsonLoginRet.data.authToken;//常用变量就提取出来
                                        MyConfig.usr_status = MyConfig.USR_STATUS_LOGIN;
                                        MyConfig.usr_email = jsonLoginRet.data.email;
                                        MyConfig.usr_phone = jsonLoginRet.data.phone;
                                        if (jsonLoginRet.data.sex) {
                                            MyConfig.usr_sex = 1;//男
                                        } else {
                                            MyConfig.usr_sex = 0;//女
                                        }
                                        MyConfig.usr_avatar = jsonLoginRet.data.avatar;
                                        MyConfig.usr_origin = jsonLoginRet.data.origin;
                                        if (jsonLoginRet.data.info != null) {
                                            MyConfig.usr_name_changed = jsonLoginRet.data.info.changed;
                                        }

                                        MyLog.d("TestData", "weixin login: bundle value, after 3 " + MyConfig.usr_weixin_headimgurl);
                                        MyConfig.saveUserDataToPreference(activity);
                                        MyLog.d("TestData", "weixin login: bundle value, after 4 " + MyConfig.usr_weixin_headimgurl);

                                        String s1 = MyConfig.formClientCheckId(0);
                                        MyLog.d("TestData", "weixin login: bundle value, after 4-1 " + MyConfig.usr_weixin_headimgurl);
                                        File f1 = new File(MyConfig.appCacheDirPath, s1);
                                        MyLog.d("TestData", "weixin login: bundle value, after 40 " + MyConfig.usr_weixin_headimgurl);

                                        try {
                                            if (MyConfig.usr_weixin_headimgurl != null) {
                                                File randomAttach = HttpTools.okhttpGetFile(MyConfig.usr_weixin_headimgurl, f1);
                                                MyLog.d("TestData", "weixin login: bundle value, after 5 ");
                                                MyConfig.uploadUserAvatar(randomAttach);
                                                MyLog.d("TestData", "weixin login: bundle value, after 6 ");
                                            }
                                        } catch (Exception e) {

                                        }

                                        if (jsonLoginRet.data.origin.equals("weixin") && !jsonLoginRet.data.info.changed) {
                                            /**
                                             * 如果从微信登录，并且没有修改过用户名
                                             */
                                            MyConfig.flag_open_regist_from_weixin_activity = 1;
                                        }


                                        MyLog.d("TestData", "weixin login: finish ");
                                        MyConfig.getDataByOrder();
                                        Intent intent_viewpager = new Intent(activity, ActivityViewPager.class);
                                        activity.startActivity(intent_viewpager);
                                        activity.finish();

                                    }

                                }

                            }
                        }).start();

//                                MyConfig.saveUserDataToPreference(ActivityAccount.this);

                    }
                });


            }

            @Override
            public void onCancel(SHARE_MEDIA platform) {
                MyLog.d("TestData", "weixin login: onCancel");
                Toast.makeText(activity, activity.getResources().getString(R.string.License_cancellation), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void logoutWeixin(final Activity activity) {
        // 添加微信平台
        UMWXHandler wxHandler = new UMWXHandler(activity, MyConfig.third_weixin_share_appID, MyConfig.third_weixin_share_appSecret);
        wxHandler.addToSocialSDK();

        // 首先在您的Activity中添加如下成员变量
        final UMSocialService mController = UMServiceFactory.getUMSocialService("com.umeng.share");

        mController.deleteOauth(activity, SHARE_MEDIA.WEIXIN,
                new SocializeListeners.SocializeClientListener() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onComplete(int status, SocializeEntity entity) {
                        if (status == 200) {
                            Toast.makeText(activity, "删除成功.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "删除失败",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    /**
     * schedule/activity/course temp variable
     */

    public static LocalDate tempLocalDate;
    public static DayCourse tempDayCourse;
    public static DaySchedule tempDaySchedule;
    public static DayActivity tempDayActivity;


    public static void getMoreCoursesBySlot(final String start, final int weeks) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                MyLog.d("", "API_COURSES_BY_SLOT begin in thread ");
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COURSES_BY_SLOT
                        + "?start=" + start + "&weeks=" + weeks, MyConfig.usr_token);
                MyLog.d("", "app start steps 5.6: API_COURSES_BY_SLOT ret " + s);

                Gson gson = new Gson();
                JsonGetCoursesBySlotRet jsonGetCoursesBySlotRet = gson.fromJson(s, JsonGetCoursesBySlotRet.class);

//                MyLog.d("", "API_COURSES_BY_SLOT ret: map size " + jsonGetCoursesBySlotRet.data.size());
//                MyLog.d("", "API_COURSES_BY_SLOT ret: map to string " + jsonGetCoursesBySlotRet.data.toString());

                if (jsonGetCoursesBySlotRet != null && jsonGetCoursesBySlotRet.data != null) {
                    if (MyConfig.jsonGetCoursesBySlotRet != null && MyConfig.jsonGetCoursesBySlotRet.data != null) {
                        MyConfig.jsonGetCoursesBySlotRet.data.putAll(jsonGetCoursesBySlotRet.data);
                    } else {
                        MyConfig.jsonGetCoursesBySlotRet = jsonGetCoursesBySlotRet;
                    }
                }

//                MyLog.d("", "app start steps 5.7: MyConfig.jsonGetCoursesBySlotRet.data.size() " +MyConfig.jsonGetCoursesBySlotRet.data.size());

                EventBus.getDefault().post(new EventCalendarCourseUpdate(start, weeks));

            }
        }).start();
    }

    /**
     * 小组成员标题
     */
    public static String activityGroupUserChooserListTitle;

    public static LinkedList<ContactUniversal> activityGroupUserChooserListList;
    /**
     * 每天日程的日程类型
     */
    public static final String scheduleCotegory = "scheduleCotegory";


    /**
     * 课程详情
     */
    public static JsonGetClassDetailRet classDetailRetRet;
    /**
     * 创建小组时添加成员数据
     */
    public static ArrayList<ContactUniversal> contactUniversals;

    /**
     * 报名人员列表
     */
    public static LinkedList<JsonGetTopicDetailRet.JoinedUsers> joinedUsers;




    /**
     * 创建小组时，调到选择添加成员的类型，是邀请，还是减少
     */
    public static final String CREATE_GROUP_PEOPLE_CATEGORY = "create_group_people_category";

    public static final String CREATE_GROUP_PEOPLE_CATEGORY_INVITE = "create_group_people_category_invite";
    public static final String CREATE_GROUP_PEOPLE_CATEGORY_KICK = "create_group_people_category_kick";
    public static final String CREATE_GROUP_PEOPLE_CATEGORY_MORE = "create_group_people_category_more";

    /**
     * 判读数据是否为空，若为空就提示暂无数据
     */
    public static String showDataByAnalyse(String data) {
        if (TextUtils.isEmpty(data)) {
            return "[暂无数据]";
        } else {
            return data;
        }
    }

    /**
     * 从map中根据key得到value，当不存在这个key时默认返回null，当返回null我们把它默认返回true
     * @param map
     * @param key
     * @return
     */
    public static boolean getValueFromMapNoNull(Map<String, Boolean> map,String key){
        if (map.get(key) == null) {
            return true;
        } else {
            return map.get(key);
        }
    }

    /**
     * webView的类型，需要WebView显示的内容使用同一个Activity，根据不同类型判断显示不同数据
     */
    public static final String WEB_VIEW_URL_CATEGORY = "web_view_url_category";

    public static final String WEB_VIEW_URL_CATEGORY_CLASS_EVALUATE = "web_view_url_category_class_evaluate";
    public static final String WEB_VIEW_URL_CATEGORY_CREDIT = "web_view_url_category_credit";
    public static final String WEB_VIEW_URL_CATEGORY_PAPER = "web_view_url_category_paper";

    /**
     * 加载HTML路径
     */
    public static final String WEB_VIEW_URL_REVIEW = "http://www.docy.co/review.html";
    /**
     * 课程评价
     */
    public static final String WEB_VIEW_URL_CREDIT = "http://www.docy.co/credit.html";
    /**
     * 挣学分
     */
    public static final String WEB_VIEW_URL_ARTICLE = "http://www.docy.co/article.html";    /**做论文*/


    /**
     * 用户权限种类
     */
    public static final String PRIVILEGE_CATEGORY_UPDATE_USER_AVATAR = "UpdateUserAvatar";
    public static final String PRIVILEGE_CATEGORY_UPDATE_USER_PHONE = "UpdateUserPhone";
    public static final String PRIVILEGE_CATEGORY_UPDATE_USER_COMPANY = "UpdateUserCompany";


    /**
     * 记录被@的人的id
     */
    public static int PEG_SOME_BODY_USER_ID;
    public static String PEG_SOME_BODY_USER_NICK_NAME;
    public static int PEG_SOME_BODY_GROUP_ID;

    /**
     * 录制视频的路径
     */
    public static String current_video_path;

    /**聊天室用到的常量*/
    public static String CHAT_ROOM_NAME = "chat_room_name";
    public static String CHAT_ROOM_NICK_NAME = "chat_room_nick_name";
    public static String CHAT_ROOM_ONLINE_NUM = "chat_room_online_num";

    public static String chat_room_nick_name ;
    public static String chat_room_avatar ;



    /**
     * 实现在群聊时@某人的功能
     *
     * @param s
     */
    public static void pegSomeBody(Activity activity, int groupId, Editable s) {
        int length = s.toString().length();
        if (length >= 1) {
            if (s.toString().endsWith("@")) {

                //getGroupUsers();

                MyConfig.PEG_SOME_BODY_GROUP_ID = groupId;
                Intent pgeIntent = new Intent(activity, ActivityPegSomeBody.class);
                activity.startActivityForResult(pgeIntent, MyConfig.PEG_SOME_BODY_REQUSET_CODE);

            }
        }
    }

    /**
     * 去除ArrayList集合中重复的元素
     *
     * @param targets
     * @return
     */
    public static ArrayList<Integer> deleteRepeatFromCollection(ArrayList<Integer> targets) {
        HashSet<Integer> set = new HashSet<Integer>();
        set.addAll(targets);
        targets.clear();
        targets.addAll(set);
        return targets;
    }

    /**
     * 返回不同的时间样式，若是今天则显示时间，若是昨天则显示昨天，若是更早则显示日期
     *
     * @param current
     * @return
     */
    public static String dateFromatTodayOrYestodayOrDate(Date current) {

        String update_time = "";
        SimpleDateFormat formatter_date = new SimpleDateFormat(MyConfig.app.getString(R.string.time_yyyy_mm_dd_week));

        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(MyConfig.app.getString(R.string.time_yyyy_mm_dd_week));
        String today = sdf.format(date);
        date.setTime(date.getTime() - 24 * 60 * 60 * 1000);//昨天
        String yestoday = sdf.format(date);
        if (current == null) {
            MyLog.d("", "userGroupsRet.messages.getLast().updatedAt: current == null");

        }
        String current_date = formatter_date.format(current);
        if (current_date.equals(today)) {

            update_time = MyConfig.Date2GroupInfoTimeToday(current);
        } else if (current_date.equals(yestoday)) {

            update_time = MyConfig.Date2GroupInfoTimeYestoday(current);
        } else {

            update_time = MyConfig.Date2GroupInfoTime(current);
        }
        return update_time;
    }


    /**
     * 未分类的其他临时中间数据
     */
    public static ContactUniversal tempContactUniversal_FragmentUserInfoTongChuang;
    public static LinkedList<ContactUniversal> tempContactUniversalLinkedList;
    public static LinkedList<ContactUniversal> byPassContactUniversalList_temp;//临时中间数据


    public static int unReadCount_temp;
//    public static int companyMemberCount_temp;

    public static ScheduleNotification scheduleNotification = null;
    public static int unRead_notification = 0;
    public static int unRead_friend_cricle = 0;
    public static int unRead_notification_remind = 0;


    private static long time_long_pressed = -1;
    private static float long_press_move_check = 1500;

    public static boolean captureLongClick(MotionEvent event) {

        float down_point_x = 0;
        float down_point_y = 0;
        float deltaX;
        float deltaY;


            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    down_point_x = event.getX();
                    down_point_y = event.getY();
                    time_long_pressed = SystemClock.elapsedRealtime();
                    MyLog.d("", "setOnItemLongClickListener: find down! " + SystemClock.elapsedRealtime());
                    return false;
                case MotionEvent.ACTION_MOVE:
                    deltaX = Math.abs(event.getX() - down_point_x);
                    deltaY = Math.abs(event.getY() - down_point_y);
                    if (deltaX > long_press_move_check || deltaY > long_press_move_check) {
                        time_long_pressed = -1;//停止计时
                    }
                    return false;
                case MotionEvent.ACTION_UP:
                    if (time_long_pressed > 0) {//还在计时

                        if ((SystemClock.elapsedRealtime() - time_long_pressed) > 500) {

                            MyLog.d("", "setOnItemLongClickListener: find a long click! " + SystemClock.elapsedRealtime());

                            time_long_pressed = -1;
                            return true;
                        } else {
                            time_long_pressed = -1;
                            return false;
                        }

                    } else {
                        return false;
                    }

                case MotionEvent.ACTION_CANCEL:
                    time_long_pressed = -1;
                    return false;
            }

        return false;
    }


    public static void deleteMsgInServer(final Activity activity, IMsg iMsg) {
        /**
         * 删除聊天室中信息
         */

        Call<JsonDeleteMsgRet> repos = HttpTools.http_api_service().deleteMsg("" + iMsg.id);
        repos.enqueue(new Callback<JsonDeleteMsgRet>() {
            @Override
            public void onResponse(Call<JsonDeleteMsgRet> call, Response<JsonDeleteMsgRet> response) {
                // Get result Repo from response.body()

                MyLog.d("", "retrofit: code() " + response.code());
                MyLog.d("", "retrofit: message() " + response.message());

                if (response.code() == 200) {

                    JsonDeleteMsgRet jsonDeleteMsgRet = response.body();

                    MyLog.d("", "retrofit: body() 1 " + response.body().toString());
                    MyLog.d("", "retrofit: body() 2 " + response.body().code);
                    MyLog.d("", "retrofit: body() 3 " + response.body().message);

                    EventBus.getDefault().post(new EventSingleMsgDelete(activity));

                }

            }

            @Override
            public void onFailure(Call<JsonDeleteMsgRet> call, Throwable t) {
                MyLog.d("", "retrofit: onFailure " + t.getMessage());
            }
        });

    }


    public static void deleteIMsg(Activity activity, int position, IMsg iMsg, LinkedList<IMsg> iMsgs, LinkedList<IMsg> iMsgs_with_image, ListAdapterMsg la_msg) {

        MyLog.d("", "setOnItemLongClickListener: deleteIMsg! " + SystemClock.elapsedRealtime());
        MyLog.d("", "setOnItemLongClickListener: deleteIMsg! check type: " + iMsg.type + " " + iMsg.subType);


        /**
         * 哪些类型的msg可以被删除？
         *  普通msg
         *  回复comment
         */

       /* if(iMsg.type == MyConfig.TYPE_MSG
                || iMsg.type == MyConfig.TYPE_SOUND
                || iMsg.type == MyConfig.TYPE_TOPIC_COMMANT
                || iMsg.type == MyConfig.TYPE_TOPIC_IMAGE_DIRECTROOM
                || iMsg.type == MyConfig.TYPE_TOPIC_MAP_DIRECTROOM
                ){
*/
        MyLog.d("", "setOnItemLongClickListener: deleteIMsg! 2 " + SystemClock.elapsedRealtime());

        /**
         * 删除服务器数据
         */
        MyConfig.deleteMsgInServer(activity, iMsg);

        /**
         * 删除本地数据
         */
        MyLog.d("", "setOnItemLongClickListener: deleteIMsg! size before delete " + iMsgs.size());
        iMsgs.remove(position);
        MyLog.d("", "setOnItemLongClickListener: deleteIMsg! size after delete " + iMsgs.size());
        la_msg.notifyDataSetChanged();

        if (iMsgs_with_image != null && iMsgs_with_image.size() > 0) {
            int image_position = MyConfig.searchRepeatMsgInList(iMsgs_with_image, iMsg, -1);
            if (image_position >= 0) {
                MyLog.d("", "setOnItemLongClickListener: deleteIMsg! remove image at " + image_position + " size " + iMsgs_with_image.size());
                iMsgs_with_image.remove(image_position);
            }
        }

//        }

    }


    public static class ClassCaptureLongClick implements ListAdapterMsg.OnCaptureLongClickedListener {

        public interface AvatarLongPressed{
            void avatarLongPressed(IMsg iMsg);
        }
        AvatarLongPressed avatarLongPressed;


        final Activity activity;
        final LinkedList<IMsg> iMsgs;
        final LinkedList<IMsg> iMsgs_with_image;
        final ListAdapterMsg la_msg;

        public ClassCaptureLongClick(Activity context, LinkedList<IMsg> iMsgs, LinkedList<IMsg> iMsgs_with_image, ListAdapterMsg la_msg, AvatarLongPressed avatarLongPressed) {
            this.activity = context;
            this.iMsgs = iMsgs;
            this.iMsgs_with_image = iMsgs_with_image;
            this.la_msg = la_msg;
            this.avatarLongPressed = avatarLongPressed;
        }

        @Override
        public boolean onLongClicked(final int position, final IMsg iMsg, MotionEvent event) {

            boolean isLongPressed = MyConfig.captureLongClick(event);

            if (isLongPressed) {


                int spot_width = getPxFromDimen(app, R.dimen.avatar_width) + 20;
                int spot_height = getPxFromDimen(app, R.dimen.avatar_height) + 20;

                if((event.getX() < spot_width) && (event.getY() < spot_height)){

                    MyLog.d("","avatar is pressed: spot: "+spot_width+" "+spot_height);
                    MyLog.d("","avatar is pressed: event: "+event.getX()+" "+event.getY());


                    if(avatarLongPressed != null){
                        avatarLongPressed.avatarLongPressed(iMsg);
                    }

                }else {

                    /**
                     * 有没有文本
                     * 有没有删除权限
                     *      有文本，有删除权限------有拷贝，有删除
                     *      有文本，没删除权限------仅有拷贝
                     *      没文本，有删除权限------仅有删除
                     *      没文本，没删除权限------无
                     */

                    final String s = MyConfig.getTextFromIMsg(iMsg);
                    if (s == null && iMsg.user_id != MyConfig.usr_id) {

                    } else {

                        new DialogFragment() {

                            @Override
                            public Dialog onCreateDialog(Bundle savedInstanceState) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(
                                        getActivity());


                                if (s != null && iMsg.user_id == MyConfig.usr_id) {//有文本，有删除权限------有拷贝，有删除
                                    builder.setTitle(getString(R.string.please_select))
                                            .setItems(R.array.single_msg_operation, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // The 'which' argument contains the index position
                                                    // of the selected item
                                                    switch (which) {
                                                        case 0:
                                                            MyConfig.copyTextToClipBoard(activity, s);
                                                            return;
                                                        case 1:
                                                            MyConfig.deleteIMsg(activity, position, iMsg, iMsgs, iMsgs_with_image, la_msg);
                                                            return;
                                                        default:
                                                            return;
                                                    }
                                                }
                                            })
                                            .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                }
                                            });
                                } else if (s == null && iMsg.user_id == MyConfig.usr_id) {//没文本，有删除权限------仅有删除
                                    builder.setTitle(getString(R.string.prompt_delete_msg))
                                            .setPositiveButton(getString(R.string.prompt_confirm), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    MyConfig.deleteIMsg(activity, position, iMsg, iMsgs, iMsgs_with_image, la_msg);
                                                }
                                            })
                                            .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                }
                                            });
                                } else {//有文本，没删除权限------仅有拷贝
                                    builder.setTitle(getString(R.string.prompt_copy_text))
                                            .setPositiveButton(getString(R.string.prompt_confirm), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    MyConfig.copyTextToClipBoard(activity, s);
                                                }
                                            })
                                            .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                }
                                            });
                                }
                                return builder.create();
                            }
                        }.show(activity.getFragmentManager(), "");
                    }
                }
            }




            return isLongPressed;
        }
    }


        public static void copyTextToClipBoard(Context context, String s) {

            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(s);
            } else {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData.newPlainText("text", s);
                clipboard.setPrimaryClip(clip);
            }
        }

        public static String getTextFromIMsg(IMsg iMsg) {
           /* if (iMsg.type == MyConfig.TYPE_MSG
                    || iMsg.type == MyConfig.TYPE_SOUND
                    || iMsg.type == MyConfig.TYPE_TOPIC_COMMANT
                    || iMsg.type == MyConfig.TYPE_TOPIC_IMAGE_DIRECTROOM
                    || iMsg.type == MyConfig.TYPE_TOPIC_MAP_DIRECTROOM
                    ) {*/
            if (iMsg.type == MyConfig.TYPE_MSG
                    || (iMsg.type == MyConfig.TYPE_TOPIC_COMMANT && iMsg.subType == MyConfig.SUBTYPE_TEXT_COMMENT)
                    ) {

                return iMsg.body;

            } else {
                return null;
            }

        }

        public static void seperateShareInBodyLink(Context context, String news, TextView textView, TextView linkView) {

            if (news == null) {
                return;
            }

            int index_http = news.indexOf("http");
            if (index_http >= 0) {
                String s1 = news.substring(0, index_http);
                String s2 = news.substring(index_http, news.length());

                textView.setText(s1);

                StringBuilder sb = new StringBuilder("<a href=" + s2 + " >" + context.getString(R.string.prompt_click_origin) + "</a>");

                linkView.setMovementMethod(LinkMovementMethod.getInstance());
                linkView.setText(Html.fromHtml(sb.toString()));

            } else {
                textView.setText(news);
            }
        }

        public static void shrinkShareInBodyLink(Context context, String news, TextView textView) {

            if (news == null) {
                return;
            }

            int index_http = news.indexOf("http");
            if (index_http >= 0) {
                String s1 = news.substring(0, index_http);
                String s2 = news.substring(index_http, news.length());

                StringBuilder sb = new StringBuilder(s1 + "<a href=" + s2 + " >" + context.getString(R.string.prompt_click_origin) + "</a>");

                textView.setMovementMethod(LinkMovementMethod.getInstance());
                textView.setText(Html.fromHtml(sb.toString()));

        }else{
            textView.setText(EmojiUtils.showEmoji(context,news));
        }
    }

        public static boolean isShareInNews(IMsg iMsg) {

            /**
             * 目前，只在图片主题里面可能存在news。也可能是从“今日头条中分享过来的”
             */

            if (iMsg.type == TYPE_TOPIC) {

                if (iMsg.subType == SUBTYPE_IMAGE || iMsg.subType == SUBTYPE_TEXT) {

                    if (iMsg.news != null && iMsg.news.equals("news")) {
                        return true;
                    }
                }

            }

            return false;

        }


        public static void getMyPrivilege() {
            new Thread(new Runnable() {
                @Override
                public void run() {

                    String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GET_PRIVILEGES, MyConfig.usr_token);

                    Gson gson = new Gson();
                    JsonGetMyPrivilege jsonGetMyPrivilege = gson.fromJson(s, JsonGetMyPrivilege.class);

                    if (jsonGetMyPrivilege != null && jsonGetMyPrivilege.code == MyConfig.retSuccess()) {
                        MyConfig.jsonGetMyPrivilege = jsonGetMyPrivilege;

                        MyLog.d("", "JsonGetMyPrivilege: " + jsonGetMyPrivilege.data.toString());
                    }


                }
            }).start();
        }


        public static void setPushToken() {

            if (MyConfig.jpush_regist_id == null) {

                MyConfig.jpush_regist_id = loadStringFromPreference(MyConfig.activityViewPager, PREF_JPUSH_REGIST_ID);

                if (MyConfig.jpush_regist_id == null) {

                    return;
                }
            }

            MyLog.d("", "API_USER_PUSH_TOKEN: setPushToken()" + MyConfig.jpush_regist_id);

            MyConfig.usr_push_status = 1;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    Gson gson = new Gson();
                    JsonSetPushToken jsonSetPushToken = new JsonSetPushToken();
                    jsonSetPushToken.accessToken = MyConfig.usr_token;
                    jsonSetPushToken.os = MyConfig.os_string;
                    jsonSetPushToken.uuid = MyConfig.uuid;
                    jsonSetPushToken.pushToken = MyConfig.jpush_regist_id;
                    String jsonStr = gson.toJson(jsonSetPushToken, JsonSetPushToken.class);

                    final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_PUSH_TOKEN, jsonStr);

                    MyLog.d("", "API_USER_PUSH_TOKEN: httpURLConnPostJson() 1:" + jsonStr);
                    MyLog.d("", "API_USER_PUSH_TOKEN: httpURLConnPostJson() 2:" + s);

                    final JsonSetPushTokenRet jsonSetCurrentCompanyRet = gson.fromJson(s, JsonSetPushTokenRet.class);
                    if (jsonSetCurrentCompanyRet != null && jsonSetCurrentCompanyRet.code == MyConfig.retSuccess()) {

                        MyLog.d("", "API_USER_PUSH_TOKEN: " + jsonStr + " token:" + jsonSetPushToken.pushToken);

                        MyConfig.usr_push_status = 2;

                    }

                }
            }).start();
        }


        public static void getErrorCodeMessage() {
            new Thread(new Runnable() {
                @Override
                public void run() {

                    InputStream is = new HttpTools().doGetActionAndReturnInputStream(MyConfig.getServer() + "/assets/errors.json", "");
                    if (is != null) {
                        BufferedReader bufferedReader =
                                new BufferedReader(
                                        new InputStreamReader(is));

                        final java.lang.reflect.Type mapType = new TypeToken<Map<String, Map<String, String>>>() {
                        }.getType();
                        final Gson gson = new Gson();
                        MyConfig.errorCodeMessages = gson.fromJson(bufferedReader, mapType);
                        MyLog.d("", "new error code: keySet " + MyConfig.errorCodeMessages.keySet().toString());
                        MyLog.d("", "new error code: values " + MyConfig.errorCodeMessages.values().toString());
                    }

                }
            }).start();
        }



    public static class MyDialog extends android.support.v4.app.DialogFragment {

        String title;

        public interface IF_MyDialogClicked {
            void dialogButtonClicked();
        }
        IF_MyDialogClicked clickPositiveButton, clickNegativeButton;

        public MyDialog(){

        }

        public static MyDialog newInstance(String title, IF_MyDialogClicked clickPositiveButton, IF_MyDialogClicked clickNegativeButton){
            MyDialog myDialog = new MyDialog();
            myDialog.title = title;
            myDialog.clickPositiveButton = clickPositiveButton;
            myDialog.clickNegativeButton = clickNegativeButton;

            return myDialog;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(
                    getActivity());
            builder.setTitle(title)
                    .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(clickNegativeButton != null) {
                                clickNegativeButton.dialogButtonClicked();
                            }
                        }
                    })
                    .setPositiveButton(getString(R.string.prompt_confirm), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(clickPositiveButton != null) {
                                clickPositiveButton.dialogButtonClicked();
                            }
                        }
                    });
            return builder.create();
        }
    }
    /**
     * 版本定制化的数据
     */
    public static void getDataByOrder(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                final String str= new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_MADE_IN_ORDER, MyConfig.usr_token);
                MyLog.d("查看定制化的数据",str);
                MyConfig.jsonMadeInOrderFromServiceRet = gson.fromJson(str,JsonMadeInOrderFromServiceRet.class);
                if(MyConfig.jsonMadeInOrderFromServiceRet!=null&&MyConfig.jsonMadeInOrderFromServiceRet.data!=null&&MyConfig.jsonMadeInOrderFromServiceRet.code == MyConfig.retSuccess()){
                    MyConfig.setOraderDataToSharedPreferences(str);
                    if(MyConfig.jsonMadeInOrderFromServiceRet.data.startPics!=null&&MyConfig.jsonMadeInOrderFromServiceRet.data.startPics.size()>0) {
                        MyConfig.setOrderStartPageTophone(MyConfig.jsonMadeInOrderFromServiceRet.data.startPics.getFirst().url);
                        EventBus.getDefault().post(MyConfig.jsonMadeInOrderFromServiceRet);
                    }
                }
            }
        }).start();
    }

    public static void setOraderDataToSharedPreferences(String s){
        MyConfig.prefEditor.putString("MadeinOrder",s);
        MyConfig.prefEditor.commit();
    }
    public static void getOraderDataToSharedPreferences(){
       String s = MyConfig.prefShared.getString("MadeinOrder",null);
        MyLog.d("查看存储的数据",s);
       MyConfig.jsonMadeInOrderFromServiceRet = new Gson().fromJson(s,JsonMadeInOrderFromServiceRet.class);

    }
    public static void setOrderStartPageTophone(final String str){
        new Thread(new Runnable() {
            @Override
            public void run() {
                File file = new File(MyConfig.appCacheDirPath, "stratpage.jpg");
                File file1 = new HttpTools().okhttpGetFile(MyConfig.getApiDomain_NoSlash_GetResources()+str,file);
                if(file1!=null){
                    MyLog.d("查看图片下载",file.toString());
                    MyConfig.prefEditor.putString("startpagefile",file1.getAbsolutePath().toString());
                    MyConfig.prefEditor.commit();
                }
            }
        }).start();
    }

    public static File getOrderStratPage() {
        //闪屏页图片路径
        String s = MyConfig.prefShared.getString("startpagefile",null);

        if(s == null){
            return null;
        }

        File file = new File(s);
        if(file.exists()) {
            return file;
        }

        return null;
    }


    public static int count_insert_contact = 0;
    public static int insertOneContact(ContactUniversal contactUniversal) {

        int ret;

        count_insert_contact++;

        /*
        * Prepares the batch operation for inserting a new raw contact and its data. Even if
        * the Contacts Provider does not have any data for this person, you can't add a Contact,
        * only a raw contact. The Contacts Provider will then add a Contact automatically.
        */

        // Creates a new array of ContentProviderOperation objects.
        ArrayList<ContentProviderOperation> ops =
                new ArrayList<ContentProviderOperation>();

        /*
        * Creates a new raw contact with its account type (server type) and account name
        * (user's account). Remember that the display name is not stored in this row, but in a
        * StructuredName data row. No other data is required.
        */
        ContentProviderOperation.Builder op =
                ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                        .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, "co.docy")
                        .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, contactUniversal.nickName);

        // Builds the operation and adds it to the array of operations
        ops.add(op.build());

        // Creates the display name for the new raw contact, as a StructuredName data row.
        op =
                ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            /*
            * withValueBackReference sets the value of the first argument to the value of
            * the ContentProviderResult indexed by the second argument. In this particular
            * call, the raw contact ID column of the StructuredName data row is set to the
            * value of the result returned by the first operation, which is the one that
            * actually adds the raw contact row.
            */
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)

                        // Sets the data row's MIME type to StructuredName
                        .withValue(ContactsContract.Data.MIMETYPE,
                                ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)

                        // Sets the data row's display name to the name in the UI.
                        .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contactUniversal.nickName);

        // Builds the operation and adds it to the array of operations
        ops.add(op.build());

        // Inserts the specified phone number and type as a Phone data row
        op =
                ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            /*
            * Sets the value of the raw contact id column to the new raw contact ID returned
            * by the first operation in the batch.
            */
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)

                        // Sets the data row's MIME type to Phone
                        .withValue(ContactsContract.Data.MIMETYPE,
                                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)

                        // Sets the phone number and type
                        .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, contactUniversal.phone)
                        .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);

        // Builds the operation and adds it to the array of operations
        ops.add(op.build());


        // Inserts photo
        if(contactUniversal.photo_bytes != null) {

            MyLog.d("","tempContactUniversal_FragmentUserInfoTongChuang.bitmap insert bytes "+contactUniversal.photo_bytes.length);

            op =
                    ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                            .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, contactUniversal.photo_bytes);

            ops.add(op.build());
        }


        //Organization details
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE )
                .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY, contactUniversal.company)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE )
                .withValue(ContactsContract.CommonDataKinds.Organization.TITLE, contactUniversal.title)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE )
                .withValue(ContactsContract.CommonDataKinds.Organization.TYPE, ContactsContract.CommonDataKinds.Organization.TYPE_WORK)

                .build());



            /*
            * Demonstrates a yield point. At the end of this insert, the batch operation's thread
            * will yield priority to other threads. Use after every set of operations that affect a
            * single contact, to avoid degrading performance.
            */
        op.withYieldAllowed(true);

        // Builds the operation and adds it to the array of operations
        ops.add(op.build());




        // Inserts the specified email and type as a Phone data row
        op =
                ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            /*
            * Sets the value of the raw contact id column to the new raw contact ID returned
            * by the first operation in the batch.
            */
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)

                        // Sets the data row's MIME type to Email
                        .withValue(ContactsContract.Data.MIMETYPE,
                                ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)

                        // Sets the email address and type
                        .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, contactUniversal.email)
                        .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK);

        ops.add(op.build());

        try {

            app.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);

            ret = 0;
        } catch (Exception e) {
            // Display a warning
            MyLog.d("", "insertOneContact: Exception encountered while inserting contact: " + e);

            ret = -1;
        }

        return ret;
    }

//
//    private getByteArray(){
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        InputStream is = null;
//        try {
//            is = url.openStream ();
//            byte[] byteChunk = new byte[4096]; // Or whatever size you want to read in at a time.
//            int n;
//
//            while ( (n = is.read(byteChunk)) > 0 ) {
//                baos.write(byteChunk, 0, n);
//            }
//        }
//        catch (IOException e) {
//            System.err.printf ("Failed while reading bytes from %s: %s", url.toExternalForm(), e.getMessage());
//            e.printStackTrace ();
//            // Perform any other exception handling that's appropriate.
//        }
//        finally {
//            if (is != null) { is.close(); }
//        }
//    }

}

