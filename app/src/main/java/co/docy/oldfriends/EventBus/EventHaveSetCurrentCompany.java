package co.docy.oldfriends.EventBus;

/**
 * Created by youhy on 6/16/15.
 */
public class EventHaveSetCurrentCompany {
    public boolean success;
    public int companyId;

    public EventHaveSetCurrentCompany(boolean success, int companyId){
        this.success = success;
        this.companyId = companyId;
    }
}
