package co.docy.oldfriends.EventBus;

import co.docy.oldfriends.Configure.MyConfig;

/**
 * Created by youhy on 6/16/15.
 */
public class EventFileDownloadProgress {

    public String src;
    public String des;
    public int progress;
    public int status;//0:刚启动，2:下载中，3:下载结束，是否全部下载完成则看progress
    public MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo downloadFileFromUrlSenderInfo;

    public EventFileDownloadProgress(String src, String des, int progress, int status, MyConfig.DownloadFileFromUrl.DownloadFileFromUrlSenderInfo downloadFileFromUrlSenderInfo){
        this.src = src;
        this.des = des;
        this.progress = progress;
        this.status = status;
        this.downloadFileFromUrlSenderInfo = downloadFileFromUrlSenderInfo;
    }

}
