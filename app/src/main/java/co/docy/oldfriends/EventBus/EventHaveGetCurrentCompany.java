package co.docy.oldfriends.EventBus;

/**
 * Created by youhy on 6/16/15.
 */
public class EventHaveGetCurrentCompany {
    public int code;
    public int companyId;

    public EventHaveGetCurrentCompany(int code, int companyId){
        this.code = code;
        this.companyId = companyId;
    }
}
