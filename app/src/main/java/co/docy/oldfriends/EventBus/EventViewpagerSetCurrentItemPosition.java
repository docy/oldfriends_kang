package co.docy.oldfriends.EventBus;

/**
 * Created by youhy on 6/16/15.
 */
public class EventViewpagerSetCurrentItemPosition {
    public int current_item_position;

    public EventViewpagerSetCurrentItemPosition(int current_item_position){
        this.current_item_position = current_item_position;
    }
}
