package co.docy.oldfriends.EventBus;

/**
 * Created by youhy on 6/16/15.
 */
public class EventFavoriteChanged {

    public int topicId;
    public boolean add;

    public EventFavoriteChanged(int topicId, boolean add){
        this.topicId = topicId;
        this.add = add;
    }
}
