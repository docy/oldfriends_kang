package co.docy.oldfriends.EventBus;

import java.util.LinkedList;

import co.docy.oldfriends.Messages.JsonGetCompanyUserListRet;

/**
 * Created by youhy on 6/16/15.
 */
public class EventContactDownloadList {

    public static int DOWNLOAD_BEGIN = 0;
    public static int DOWNLOAD_END = 1;

    public int successed;
    public int failed;

    public LinkedList<JsonGetCompanyUserListRet.GetCompanyUserListRet> data;
    public int flag;

    public EventContactDownloadList(LinkedList<JsonGetCompanyUserListRet.GetCompanyUserListRet> data, int flag){
        this.data = data;
        this.flag = flag;
    }

}
