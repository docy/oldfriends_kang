package co.docy.oldfriends.EventBus;

import co.docy.oldfriends.DataClass.IMsg;

/**
 * Created by youhy on 6/16/15.
 */
public class EventIMsgSoundPlayEnd {

    public IMsg iMsg;

    public EventIMsgSoundPlayEnd(IMsg iMsg){
        this.iMsg = iMsg;
    }

}
