package co.docy.oldfriends.EventBus;

/**
 * Created by youhy on 6/16/15.
 */
public class EventCompanyJoined {
    public int companyId;
    public String message;

    public EventCompanyJoined(int companyId, String message){
        this.companyId = companyId;
        this.message = message;
    }
}
