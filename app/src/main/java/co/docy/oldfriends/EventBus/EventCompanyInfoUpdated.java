package co.docy.oldfriends.EventBus;

/**
 * Created by youhy on 6/16/15.
 */
public class EventCompanyInfoUpdated {

    public int companyId;
    public String itemName;

    public EventCompanyInfoUpdated(int companyId, String itemName){
        this.companyId = companyId;
        this.itemName = itemName;
    }

}
