package co.docy.oldfriends.EventBus;

/**
 * Created by youhy on 6/16/15.
 */
public class EventAppVersionCheckInfo {
    public int server_current_versionCode;
    public int server_last_versionCode;
    public int local_versionCode;
    public String reminded_day;

    public EventAppVersionCheckInfo(
            int local_versionCode,
            int server_last_versionCode,
            int server_current_versionCode,
            String reminded_day ){
        this.server_current_versionCode = server_current_versionCode;
        this.server_last_versionCode = server_last_versionCode;
        this.local_versionCode = local_versionCode;
        this.reminded_day = reminded_day;
    }
}
