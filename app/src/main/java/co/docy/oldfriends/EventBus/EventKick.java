package co.docy.oldfriends.EventBus;

import co.docy.oldfriends.Messages.JsonGetEventRet;

/**
 * Created by wuxue on 15/11/23.
 */
public class EventKick {
    JsonGetEventRet.GetEventRet getEventRet;
    public EventKick(JsonGetEventRet.GetEventRet json_socket_msg){
        this.getEventRet = json_socket_msg;
    }
    public  JsonGetEventRet.GetEventRet getMsgRet(){
        return  getEventRet;
    }
}
