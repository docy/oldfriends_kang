package co.docy.oldfriends.emoji;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import co.docy.oldfriends.R;

/**
 * Created by fighting on 2016/5/17.
 */
public class EmojiUtils {
    /**这个map的key为emojiName*/
    public static HashMap<String,String> emojiNameMap;

    /**这个map的key为emojiField*/
    public static HashMap<String,String> emojiFieldMap;

    /**
     * 读取xml文件，得到表情图片的文件名，并根据文件名得到id，封装到emojiBean类中
     * @param ctx
     * @param name
     * @return
     */
    public static ArrayList<EmojiBean> readXML(Context ctx, String name) {
        try {
            //InputStream in = ctx.getResources().getAssets().open("emojis.xml");"emojis_name.xml"
            InputStream in = ctx.getResources().getAssets().open(name);
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser saxParser = spf.newSAXParser();
            XMLContentHandler handler = new XMLContentHandler(ctx);
            saxParser.parse(in, handler);

            in.close();
            ArrayList<EmojiBean> emojis = handler.getEmojis();
           // emojis.add(emojis.size() - 1, new EmojiBean(R.drawable.emoji_delete, "emoji_delete"));
            return emojis;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 把文件名替换成表情字段的核心代码，注意递归的使用
     * @param s
     *
     * @return
     */
    public static String replaceNameByField(Context context, String s) {
        getemojiNameMap(context);
        if(s.indexOf("emoji_1f")== -1){
            return s;
        }else {
            String str = s.substring(s.indexOf("emoji_1f"),s.indexOf("emoji_1f")+"emoji_1f000".length());
            String name = emojiNameMap.get(str);
            Log.d("kwwl","表情："+str+"对应的key是："+name);

            if(!TextUtils.isEmpty(name)){
                s = s.replace(str,name);
                s = replaceNameByField(context,s);
            }
        }
        return s;
    }

    /**
     * 创建map对象，此时表情名称为key,表情字段为name
     * @return
     */
    private static void  getemojiNameMap(Context context) {
        if(emojiNameMap == null){
            ArrayList<EmojiBean> emojiKey = EmojiUtils.readXML(context, "emojis_key.xml");
            ArrayList<EmojiBean> emojiName = EmojiUtils.readXML(context, "emojis_name.xml");

            /**此时表情名称为key,表情字段为name*/
            emojiNameMap = new HashMap<>();
            for (int i = 0 ;i<emojiKey.size();i++ ){
                emojiNameMap.put(emojiName.get(i).getName(),emojiKey.get(i).getName());
               // Log.d("kwwl","表情名称=================="+emojiName.get(i).getName()+"《》《》《》"+"表情字段========================"+emojiKey.get(i).getName());
            }
        }

    }


    /**
     * 把文件名替换成表情字段
     * @return
     */
    private static  void   getEmojiFieldMap(Context context) {
        if(emojiFieldMap==null){
            ArrayList<EmojiBean> emojiKey = EmojiUtils.readXML(context, "emojis_key.xml");
            ArrayList<EmojiBean> emojiName = EmojiUtils.readXML(context, "emojis_name.xml");

            /**此时表情名称为name,表情字段为key*/
            emojiFieldMap = new HashMap<>();
            int len = 0;
            for (int i = 0 ;i<emojiKey.size();i++ ){
                emojiFieldMap.put(emojiKey.get(i).getName(),emojiName.get(i).getName());
                if(emojiKey.get(i).getName().length() > len){
                    len = emojiKey.get(i).getName().length();
                }
            }
            Log.d("kwwl","最长的key============="+len);
        }

    }

    /**
     * 把表情字段替换成表情文件名的核心代码，注意递归的使用
     * @param s
     * @return
     */
    public static String replaceFieldByName(Context context, String s) {
        getEmojiFieldMap(context);
        if(s.indexOf("[")== -1){
            return s;
        }else {
            for(int i = 5 ;i>=3 ;i--) {
                if(s.length() >= s.indexOf("[") + i) {
                    String str = s.substring(s.indexOf("["), s.indexOf("[") + i);
                    String name = emojiFieldMap.get(str);
                    if (TextUtils.isEmpty(name)) {
                        continue;
                    }else {
                        i = 0;
                        s = s.replace(str, name);
                        s = replaceFieldByName(context,s);
                    }
                }
            }
        }
        return s;
    }


    /**
     * 隐藏软键盘
     * @param activity
     */
    public static void hide_keyboard_must_call_from_activity(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 弹出软键盘
     * 注意：1，showSoftInput方法中参数是一个View对象，但这个View对象必须获取焦点才能有效，所以必须获取带焦点的View对象。直接传递EditText也是可以的，其他的没有测试过。
     *      2，如果是页面刚刚加载可能存在控件还没有获取焦点的情况，所以sleep100ms；
     * @param activity
     */
    public static  void show_keyboard_must_call_from_activity(final Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        inputMethodManager.showSoftInput(view, 0);
    }

    /**
     * 将dp值转换为px
     * @param dp
     * @return
     */
    public static  int dp2Px(Context context, float dp) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }


    public static void dealExpression(Context context, SpannableString spannableString, Pattern patten, int start) throws SecurityException, NoSuchFieldException, NumberFormatException, IllegalArgumentException, IllegalAccessException {
        Matcher matcher = patten.matcher(spannableString);
        while (matcher.find()) {
            String key = matcher.group();
            if (matcher.start() < start) {
                continue;
            }
            Field field = R.drawable.class.getDeclaredField(key);
            int resId = Integer.parseInt(field.get(null).toString());		//通过上面匹配得到的字符串来生成图片资源id
            if (resId != 0) {
                BitmapFactory.Options opt = new BitmapFactory.Options();
                // opt.inSampleSize = 1;
                Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resId,opt);
                BitmapDrawable bit = new BitmapDrawable(bitmap);
                bit.setBounds(0,0,EmojiUtils.dp2Px(context,22),EmojiUtils.dp2Px(context,22));
                VerticalImageSpan imageSpan = new VerticalImageSpan(bit);				//通过图片资源id来得到bitmap，用一个ImageSpan来包装

                int end = matcher.start() + key.length();					//计算该图片名字的长度，也就是要替换的字符串的长度
                spannableString.setSpan(imageSpan, matcher.start(), end, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);	//将该图片替换字符串中规定的位置中SPAN_INCLUSIVE_EXCLUSIVE
                if (end < spannableString.length()) {						//如果整个字符串还未验证完，则继续。。
                    dealExpression(context,spannableString,  patten, end);
                }
                break;
            }
        }
    }

    /**
     * 得到一个SpanableString对象，通过传入的字符串,并进行正则判断
     * @param context
     * @param str
     * @return
     */
    public static SpannableString getExpressionString(Context context, String str, String zhengze){

        SpannableString spannableString = new SpannableString(str);
        Pattern sinaPatten = Pattern.compile(zhengze, Pattern.CASE_INSENSITIVE);		//通过传入的正则表达式来生成一个pattern
        try {
            dealExpression(context,spannableString, sinaPatten, 0);
        } catch (Exception e) {
            Log.e("dealExpression", e.getMessage());
        }
        return spannableString;
    }

    /**
     * 显示表情
     * @param context
     * @param body
     * @return
     */
    public static SpannableString showEmoji(Context context,String body){
        body= EmojiUtils.replaceFieldByName(context,body);
        String regex = "emoji_1f[0-9]{3}";
        SpannableString spannableString = EmojiUtils.getExpressionString(context, body, regex);
        return spannableString;
    }

    /**
     * 垂直居中的ImageSpan
     *
     *
     */
    public static class VerticalImageSpan extends ImageSpan {

        public VerticalImageSpan(Drawable drawable) {
            super(drawable);
        }

        public int getSize(Paint paint, CharSequence text, int start, int end,
                           Paint.FontMetricsInt fontMetricsInt) {
            Drawable drawable = getDrawable();
            Rect rect = drawable.getBounds();
            if (fontMetricsInt != null) {
                Paint.FontMetricsInt fmPaint = paint.getFontMetricsInt();
                int fontHeight = fmPaint.bottom - fmPaint.top;
                int drHeight = rect.bottom - rect.top;

                int top = drHeight / 2 - fontHeight / 4;
                int bottom = drHeight / 2 + fontHeight / 4;

                fontMetricsInt.ascent = -bottom;
                fontMetricsInt.top = -bottom;
                fontMetricsInt.bottom = top;
                fontMetricsInt.descent = top;
            }
            return rect.right;
        }

        @Override
        public void draw(Canvas canvas, CharSequence text, int start, int end,
                         float x, int top, int y, int bottom, Paint paint) {
            Drawable drawable = getDrawable();
            canvas.save();
            int transY = 0;
            /**表情一直有些偏下，所以再减2*/
            transY = ((bottom - top) - (drawable.getBounds().bottom-drawable.getBounds().top)) / 2 + top-2;
            canvas.translate(x, transY);
            drawable.draw(canvas);
            canvas.restore();
        }
    }
}

