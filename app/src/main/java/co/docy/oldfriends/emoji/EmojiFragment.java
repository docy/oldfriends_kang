package co.docy.oldfriends.emoji;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.R;

/**
 *
 * 
 * @author ChenLei
 * 
 */
public class EmojiFragment extends Fragment implements OnPageChangeListener {
	private List<EmojiBean> _eBeans;
	private ViewPager _viewPager;
	private ViewGroup _group;
	private EditText _editText;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_emoji, null);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		_eBeans = EmojiUtils.readXML(getActivity(),"emojis_name.xml");

		initViews(view);
	}

	private final void initViews(View rootView) {
		_viewPager = (ViewPager) rootView.findViewById(R.id.emoji_viewpager);
		_group = (ViewGroup) rootView.findViewById(R.id.emoji_navi_pointer);
	}

	/**
	 *
	 * 
	 * @param edit
	 *
	 */
	public void setEditTextHolder(EditText edit) {
		_editText = edit;
		init();
	}

	private void init() {
		if (_eBeans != null && _editText != null) {
			LinkedList<View> _views = new LinkedList<View>();
			int size = _eBeans.size()-1;
			int pages =size%20==0? size / 20 :size/ 20+ 1;
			MyLog.d("kwwl","表情个数共"+_eBeans.size()+"页数是："+pages+"==========="+(size%20));
			makeNaviPointer(pages);
			final Activity activity = getActivity();
			if (activity != null) {
				for (int i = 0; i < pages; i++) {
					// EmojiGrid layout
					View expressionView = activity.getLayoutInflater().inflate(R.layout.emoji_grid_layoutt, null);
					final GridView gridView = (GridView) expressionView.findViewById(R.id.emoji_gridview);
					final EmojiAdapter emojiAdapter = new EmojiAdapter(getExpressionPage(_eBeans, i), activity);
					gridView.setAdapter(emojiAdapter);
					//gridView.setSelector(getResources().getDrawable(R.drawable.selector_emojiclick_gray));
					gridView.setTag(i);
					/**gridView的条目点击事件*/
					gridView.setOnItemClickListener(new OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
							int lastIndex = gridView.getLastVisiblePosition();
							if (position == lastIndex) {
								setOnDeletaCilck();
							} else {
								EmojiBean item = emojiAdapter.getItem(position);
								String name = item.getName();
								int resId = item.getResId();

								ImageSpan span = new ImageSpan(getExpressionDrawable(resId));
								int startIndex = _editText.getSelectionStart();
								int endIndex = startIndex + name.length();

								Editable edit = _editText.getEditableText();
								edit.insert(startIndex, name);
								edit.setSpan(span, startIndex, endIndex, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
							}
						}
					});
					_views.addLast(expressionView);
				}
				_viewPager.setAdapter(new EmojiPagerAdapter(_views));
				_viewPager.setOnPageChangeListener(this);
			}
		}
	}

	private final Drawable getExpressionDrawable(int resId) {
		Bitmap bmp = BitmapFactory.decodeResource(getResources(), resId);
		BitmapDrawable drawable = new BitmapDrawable(bmp);
		drawable.setBounds(0, 0, dp2Px(22), dp2Px(22));
		return drawable;
	}

	/**
	 * 将dp值转换为px
	 * @param dp
	 * @return
     */
	public int dp2Px(float dp) {
		float scale = getActivity().getResources().getDisplayMetrics().density;
		return (int) (dp * scale + 0.5f);
	}



	public static List<EmojiBean> getExpressionPage(List<EmojiBean> eBeans, int page) {

		List<EmojiBean> beans = new ArrayList<EmojiBean>();

		int size = eBeans.size()-1;
		int totalPage = size%20==0? size / 20 :size / 20+ 1;
		if (page < totalPage - 1) {
			for (int i = 0; i < 21; i++) {
				if (i == 20) {

					beans.add(eBeans.get(size ));
				} else {
					beans.add(eBeans.get(i + page * 20));
				}
			}
		} else {

			int index = size - page * 20;
			for (int i = 0; i <= index; i++) {
				if (i == index) {
					beans.add(eBeans.get(size ));
				} else {
					beans.add(eBeans.get(i + page * 20));
				}
			}
		}
		return beans;
	}

	/**
	 * 手动创建ViewPager下边的指示点
	 * @param pageCount
     */
	private final void makeNaviPointer(int pageCount) {
		for (int index = 0; index < pageCount; index++) {
			ImageView img = new ImageView(getActivity().getApplicationContext());
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(10, 10);
			layoutParams.weight = 1;
			img.setLayoutParams(layoutParams);
			_group.addView(img);
			setCurrentPointPositon(0);
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int position) {
		setCurrentPointPositon(position);
	}

	private void setOnDeletaCilck() {
		if (_editText != null) {
			final KeyEvent keyEventDown = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL);
			_editText.onKeyDown(KeyEvent.KEYCODE_DEL, keyEventDown);
		}
	}

	private void setCurrentPointPositon(int selectedPosition) {
		if (_group != null) {
			int childCount = _group.getChildCount();
			for (int index = 0; index < childCount; index++) {
				if (selectedPosition == index) {
					((ImageView) _group.getChildAt(index)).setImageResource(R.drawable.emoji_point_select);
				} else {
					((ImageView) _group.getChildAt(index)).setImageResource(R.drawable.emoji_point_normal);
				}
			}
		}
	}
}
