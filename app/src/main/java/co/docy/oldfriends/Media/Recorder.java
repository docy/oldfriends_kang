package co.docy.oldfriends.Media;

import android.media.MediaRecorder;
import android.os.AsyncTask;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;

/**
 * Created by wuxue on 15/12/21.
 */
public class Recorder {

    public MediaRecorder mediaRecorder;

    public File recordAudioFile;

    public long startTime = 0, stopTime = 0;

    GetMaxAmplitudeTask getMaxAmplitudeTask;
    public Recorder(String fileName) {
        recordAudioFile = new File(MyConfig.appCacheDirPath, fileName);
        mediaRecorder = new MediaRecorder();
    }

    LinkedList<Integer> list = new LinkedList();

    boolean recording = false;

    public interface IUIApdate {
        public void UiApdate(double MaxAmplitude);
    }

    public IUIApdate Ui;

    public void startRecord() {
        // http://stackoverflow.com/questions/15871407/audio-format-for-ios-and-android/15871970#15871970

        /**
         * 录音就在当前线程
         */
        MyLog.d("", "mediaRecorder file: " + recordAudioFile.getAbsolutePath());
        //mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mediaRecorder.setOutputFile(recordAudioFile.getAbsolutePath());
        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mediaRecorder.start();
        startTime = System.currentTimeMillis();
        getMaxAmplitudeTask = new GetMaxAmplitudeTask();
        getMaxAmplitudeTask.execute();

    }

    public int stopRecord() {
        // TODO Auto-generated method stub
        if (mediaRecorder != null) {
            try {
                mediaRecorder.stop();
                stopTime = System.currentTimeMillis();
                mediaRecorder.release();
                mediaRecorder = null;
                // TODO: 12/23/15 应该利用200ms心跳来处理，而不是自己再启动一个定时器
                //  可考虑先关闭getMaxAmplitudeTask，再关闭mediaRecorder。不过这个不紧要
                getMaxAmplitudeTask.cancel(true);
                MyLog.d("", "tabRecord: record end");
            } catch (Exception e) {

            }

        }
        int recordTime = (int) ((stopTime - startTime) / 1000);
        MyLog.d("","recordTime:startTime======="+startTime);
        MyLog.d("","recordTime:stopTime======="+stopTime);
        MyLog.d("","recordTime:startTime-stopTime======="+(startTime-stopTime));

        if (recordTime < 1){
            recordTime = 1;
        }
        return recordTime;
    }

    protected class GetMaxAmplitudeTask extends AsyncTask<Void, Integer, List> {
        @Override
        protected List doInBackground(Void... params) {
            publishProgress(0);
            for (; ; ) {
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (isCancelled()) {
                    break;
                } else {
                    int amplitude = mediaRecorder.getMaxAmplitude();
                    list.add(amplitude);
                    publishProgress(amplitude);
                }
            }
            return list;
        }

        /**
         * 在调用publishProgress之后被调用，在ui线程执行.
         * publishProgress 触发一次，则该方法被调用一次。
         * 在此方法中可以进行ＵＩ组件的更新。
         * 例如给textview组件设置值等
         */
        @Override
        protected void onProgressUpdate(Integer... progress) {
            if(Ui != null) {
                Ui.UiApdate(Integer.valueOf(progress[0]));
            }
        }

        /**
         * 该方法是后台任务执行完之后被调用，在ui线程执行
         * 可以进行UI组件更新，
         * 并且该组件只返回最后一次结果，一次任务也只调用一次
         */

        protected void onPostExecute(Integer result) {
            Ui.UiApdate(-1);
        }

    }


}



