package co.docy.oldfriends.Media;

import android.app.Activity;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.io.File;
import java.io.IOException;
import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.EventBus.EventRepeatRecordVideo;
import de.greenrobot.event.EventBus;

/**
 * Created by fighting on 2016/7/19.
 */
public class VideoRecorder {

    private Activity mActivity;
    private SurfaceView mSurfaceView;
    private SurfaceHolder mHolder;
    private Camera mCamera;
    private MediaRecorder mMediaRecorder;
    private Display screenDisplay;
    private int screenWidth;
    private int screenHeight;
    private String currentVideoPath;

    /**是否正在预览，当为true时表示正在预览；当为false表示停止了预览，停止预览时不能调用自动对焦的方法*/
    private boolean isPriview = true;

    /**打开的是否是后置摄像头，默认是true，首次打开后置摄像头*/
    private boolean isBackCamera = true;

    public VideoRecorder(Activity activity, SurfaceView surfaceView) {
        mActivity = activity;
        mSurfaceView = surfaceView;
        initDate();
    }

    /**
     * 初始化预览
     */
    private void initDate() {
        screenDisplay = mActivity.getWindowManager().getDefaultDisplay();
        screenWidth = screenDisplay.getWidth();
        screenHeight = screenDisplay.getHeight();

        mHolder = mSurfaceView.getHolder();
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mHolder.addCallback(new SurfaceCallBack());

        /**点击surfaceView，自动对焦*/
        mSurfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAutoFocus();
            }
        });
    }

    /**
     * 准备录制视频
     */
    private void prepareRecordVideo() {
        try {
            mMediaRecorder = new MediaRecorder();
            mMediaRecorder.reset();
            mCamera.unlock();
            mMediaRecorder.setCamera(mCamera);
            mMediaRecorder.setPreviewDisplay(mHolder.getSurface());
            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);// 视频源
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);// 音频源
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);// 视频输出格式mp4
            mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);// 视频录制格式
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);// 音频格式

            mMediaRecorder.setVideoEncodingBitRate(1000 * 600);/** 设置编码比特率,数字越大清晰度越高，**对文件大小起关键性作用*/
            //mMediaRecorder.setVideoFrameRate(24);/**帧率，**这个对文件大小不起作用*//**注意：华为honor6plus不支持这行代码*/
            mMediaRecorder.setOrientationHint(isBackCamera?90:270);/**播放视频时的角度，调用后置摄像头时旋转90度，调用前置摄像头时旋转270度，保证播放时是竖直向上的 */
            mMediaRecorder.setVideoSize(640, 480);/** 设置分辨率,数字越大清晰度越高，**对文件大小起关键性作用，还支持720*480的*/
            mMediaRecorder.setMaxDuration(5000);/**5秒*/
            File videoFile = new File(MyConfig.appCacheDirPath, MyConfig.tempVideoFile);
            currentVideoPath = videoFile.getAbsolutePath();
            mMediaRecorder.setOutputFile(videoFile.getAbsolutePath());
            mMediaRecorder.prepare();
        } catch (Exception e) {
            releaseMediaRecorder();
        }


    }

    /**
     * surfaceView预览回调方法
     */
    class SurfaceCallBack implements SurfaceHolder.Callback {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {

        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int arg1, int width, int height) {
            switchCamera(isBackCamera);/**由于每次surfaceView重现时都会执行这段代码，所以此处也应该动态切换摄像头*/
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder arg0) {
            releaseCamera();
        }
    }

    /**
     * 设置Camera的相关参数
     */
    private void setCameraParameters() {
        try {
            Camera.Parameters parameters = mCamera.getParameters();
            //parameters.set("orientation", "landscape");/**横向，但好像不起作用*/
            parameters.set("orientation", "portrait");/**竖向，但好像不起作用*/
            parameters.setRotation(90);/**拍照时照片的方向*/

            Camera.Size previewSize = getBestPreviewSize(mCamera.getParameters().getSupportedPreviewSizes());
            parameters.setPreviewSize(previewSize.width, previewSize.height);
            Camera.Size pictureSize = getBestPreviewSize(mCamera.getParameters().getSupportedPictureSizes());
            parameters.setPictureSize(pictureSize.width, pictureSize.height);
            parameters.setJpegQuality(100);
            mCamera.setParameters(parameters);

            mCamera.setPreviewDisplay(mHolder);
            mCamera.setDisplayOrientation(90);/**显示的方向*/
            mCamera.startPreview();
            isPriview = true;
            mCamera.autoFocus(mAutoFocus);/**自动对焦*/
        } catch (IOException e) {
            releaseCamera();
        }
    }


    /**
     * 自动对焦
     */
    public void setAutoFocus() {
        if (mCamera != null && isPriview) {
            mCamera.autoFocus(mAutoFocus);
        }
    }


    /**
     * 开始录制视频
     */
    public void startRecordVideo() {
        prepareRecordVideo();
        if (mMediaRecorder != null) {
            mMediaRecorder.start();
        }
    }

    /**
     * 停止录制视频
     */
    public void stopRecordVideo() {
        if (mMediaRecorder != null) {
            try {   /**这个必须try起来，不然录制时间小于1秒时会出现crash，本质就是 mMediaRecorder.stop()这个代码crash掉的*/
                    /**注意：华为honor6plus不会出现这个crash*/
                mMediaRecorder.stop();
                releaseMediaRecorder();
                mCamera.stopPreview();
                isPriview = false;
            } catch (Exception e) { /**当出现crash时自动调用“重拍”方法*/
                releaseMediaRecorder();
                EventBus.getDefault().post(new EventRepeatRecordVideo());
            }
        }
    }

    public String getCurrentVideoPath() {
        return currentVideoPath;
    }

    /**
     * 开始录制预览
     */
    public void startPreview() {
        if (mCamera != null) {
            mCamera.startPreview();
            isPriview = true;
        }
    }


    /**
     * 释放录制
     */
    public void releaseMediaRecorder() {
        if (mMediaRecorder != null) {

            mMediaRecorder.reset();
            mMediaRecorder.release();
            mCamera.lock();
            mMediaRecorder = null;
        }
    }

    /**
     * 释放相机
     */
    public void releaseCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            isPriview = false;
            mCamera.release();
            mCamera = null;
        }
    }

    /**
     * 切换摄像头,当参数是true时打开后置摄像头
     */
    public void switchCamera(boolean isBackCamera) {
        this.isBackCamera = isBackCamera;
        if(mCamera != null) {
            mCamera.stopPreview();//停掉原来摄像头的预览
            mCamera.release();//释放资源
            mCamera = null;//取消原来摄像头
        }

        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras();//得到摄像头的个数
        for (int i = 0; i < cameraCount; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK && isBackCamera) {/**打开后置摄像头*/
                mCamera = Camera.open(i);
                setCameraParameters();
            }else if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT && !isBackCamera) {/**打开前置摄像头*/
                mCamera = Camera.open(i);
                setCameraParameters();
            }
        }
    }

    /**
     * 自动对焦
     */
    private Camera.AutoFocusCallback mAutoFocus = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
        }
    };

    /**
     * 得到匹配的预览分辨率
     *
     * @param sizeList
     * @return
     */
    private Camera.Size getBestPreviewSize(List<Camera.Size> sizeList) {

        Log.d("kwwl", "screenHeight=======================================" + screenHeight);
        int height = screenHeight;
        height = screenHeight / 9 * 7;
        Camera.Size bestSize = mCamera.new Size(320, 240);
        if (sizeList != null && sizeList.size() > 0) {
            bestSize = sizeList.get(0);
            for (int i = 0; i < sizeList.size(); i++) {
                Camera.Size z = sizeList.get(i);
                Log.d("kwwl", "z.width=============" + z.width + ";;;;;;z.height===============" + z.height + ";;;;;;;;;;z.width/z.height================" + (double) z.width / z.height);
                if (Math.abs((double) z.width / z.height - (double) height / screenWidth) > 0.1)
                    continue;
                if (z.height < screenWidth) {
                    Log.d("kwwl", "z.width=======================================" + z.width + ";;;;;;z.height=================================" + z.height);
                    return z;
                }
            }
        }
        return bestSize;
    }


}
