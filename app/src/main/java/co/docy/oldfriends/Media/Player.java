package co.docy.oldfriends.Media;

import android.media.AudioManager;
import android.media.MediaPlayer;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventIMsgSoundPlayEnd;
import co.docy.oldfriends.DataClass.IMsg;
import co.docy.oldfriends.Messages.JsonMarkAudioReadedRet;
import co.docy.oldfriends.NetTools.HttpTools;
import de.greenrobot.event.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wuxue on 15/12/22.
 */
public class Player {

    public interface PlayingNotify {
        void statusNotify();
    }
    public PlayingNotify playingNotify;


    MediaPlayer mediaPlayer;
    String path;
    IMsg iMsg;

    public Player(String path) {
        /**
         * 应该删除，否则会iMsg指针空
         */
        this.path = path;
        mediaPlayer = new MediaPlayer();
    }

    public Player(IMsg iMsg) {
        this.iMsg = iMsg;
        this.path = MyConfig.getApiDomain_NoSlash_GetResources() + iMsg.audioPath;
        mediaPlayer = new MediaPlayer();
    }

    public Player(IMsg iMsg, PlayingNotify playingNotify) {
        this.iMsg = iMsg;
        this.path = MyConfig.getApiDomain_NoSlash_GetResources() + iMsg.audioPath;
        mediaPlayer = new MediaPlayer();
        this.playingNotify = playingNotify;
    }

    public IMsg getIMsg(){
        return iMsg;
    }

    public void release(){
        mediaPlayer.release();
        mediaPlayer = null;
        iMsg.audioPlaying = MyConfig.MEDIAPLAYER_NOT_INITIAL;
        notifyStatus();
    }

    public void reset(){

        mediaPlayer.reset();
        iMsg.audioPlaying = MyConfig.MEDIAPLAYER_NOT_INITIAL;
        notifyStatus();
    }
    public void pause() {

        mediaPlayer.pause();
        iMsg.audioPlaying = MyConfig.MEDIAPLAYER_IS_READY;
        notifyStatus();
    }

    public void replay(){

        mediaPlayer.seekTo(0);
        mediaPlayer.start();
        iMsg.audioPlaying = MyConfig.MEDIAPLAYER_IS_PLAYING;
        notifyStatus();
    }

    public void prepareAndPlay() {

        iMsg.audioPlaying = MyConfig.MEDIAPLAYER_IS_PREPARING;
        notifyStatus();

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {//MyConfig.getApiDomain_NoSlash_GetResources() + "/uploads/b2d5ecd15ba117199fb0942630e75d98.aac"
            mediaPlayer.setDataSource(path);
        } catch (Exception e) {
            iMsg.audioPlaying = MyConfig.MEDIAPLAYER_NOT_INITIAL;
            notifyStatus();
            return;
        }
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                iMsg.audioPlaying = MyConfig.MEDIAPLAYER_IS_READY;
                notifyStatus();

                EventBus.getDefault().post(new EventIMsgSoundPlayEnd(iMsg));
            }
        });
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {

                mediaPlayer.start();

                iMsg.readed = true;
                notifyServerAudioReaded();

                iMsg.audioPlaying = MyConfig.MEDIAPLAYER_IS_PLAYING;
                notifyStatus();

            }
        });
        mediaPlayer.prepareAsync();

    }


    public void notifyStatus() {
        if (playingNotify != null) {
            playingNotify.statusNotify();
        }
    }

    public void notifyServerAudioReaded(){


//        MyConfig.IF_HTTP_API service = HttpTools.getRetrofit().create(MyConfig.IF_HTTP_API.class);
        Call<JsonMarkAudioReadedRet> repos = HttpTools.http_api_service().markAudioReaded("" + iMsg.audioId);
        repos.enqueue(new Callback<JsonMarkAudioReadedRet>() {
            @Override
            public void onResponse(Call<JsonMarkAudioReadedRet> call, Response<JsonMarkAudioReadedRet> response) {
                // Get result Repo from response.body()

                MyLog.d("", "retrofit: code() " + response.code());
                MyLog.d("", "retrofit: message() " + response.message());

                if(response.code()==200){

                    JsonMarkAudioReadedRet jsonMarkAudioReadedRet = response.body();

                    MyLog.d("", "retrofit: body() 1 " + response.body().toString());
                    MyLog.d("", "retrofit: body() 2 " + response.body().code);
                    MyLog.d("", "retrofit: body() 3 " + response.body().message);

                }

            }

            @Override
            public void onFailure(Call<JsonMarkAudioReadedRet> call, Throwable t) {
                MyLog.d("", "retrofit: onFailure " + t.getMessage());
            }
        });

    }
}
