package co.docy.oldfriends.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;

import org.joda.time.LocalDate;

import java.util.HashMap;
import java.util.LinkedList;

import co.docy.oldfriends.Activitis.ActivityDayActivities;
import co.docy.oldfriends.Adapters.ListAdapterDaySchedule;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonGetPerDayCourseRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

public class FragmentDaySchedule extends FragmentBase {

    LocalDate day;

    ListAdapterDaySchedule listAdapterDaySchedule;
    LinkedList<JsonGetPerDayCourseRet.PerDayCourseInfo> list ;
    ListView fragment_day_sch_listview;

    public static FragmentDaySchedule newInstance() {
        FragmentDaySchedule f = new FragmentDaySchedule();
        f.day = MyConfig.tempLocalDate;
        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MyLog.d("", "viewpager get samplelistfragment");
        View v = inflater.inflate(R.layout.fragment_day_schedule, null);

        fragment_day_sch_listview = (ListView)v.findViewById(R.id.fragment_listview);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getCourseListByDate(getActivity(), MyConfig.tempLocalDate.toString());


    }



    /**
     * 得到每天日程
     * @param context
     * @param date
     */
    public  void getCourseListByDate(final Context context, final String date) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                /**得到当天日程数据*/
                String url = MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COURSES_BY_DATE;
                HashMap<String,String> map = new HashMap();
                map.put("date",date);
               // final String s = new HttpTools().httpURLConnGet(url , MyConfig.usr_token);
                final String s = new HttpTools().httpURLConnGetWithKeyValue(url,map,MyConfig.usr_token);


                MyLog.d("kwwl", "CourseList " + s);

                //MyLog.d("kwwl", "url " + url);
                final JsonGetPerDayCourseRet jsonGetPerDayCourse = gson.fromJson(s, JsonGetPerDayCourseRet.class);


               /**添加适配器属于修改UI，运行到主线程*/
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (jsonGetPerDayCourse != null && jsonGetPerDayCourse.code==MyConfig.retSuccess()) {

                            list = jsonGetPerDayCourse.data;
                            setAdapter();
                        }

                    }
                });

            }
        }).start();
    }

    /**
     * 设置适配器
     */
    private void setAdapter() {
        listAdapterDaySchedule = new ListAdapterDaySchedule(getActivity(), list , new ListAdapterDaySchedule.OnClickedListener() {
            @Override
            public void OnClicked(int position) {
                JsonGetPerDayCourseRet.PerDayCourseInfo perDayCourseInfo = list.get(position);


                //MyConfig.tempDayActivity = new DayActivity("this is activity");
                MyConfig.tempPerDayCourseInfo = perDayCourseInfo;

                Intent intent = new Intent(getActivity(), ActivityDayActivities.class);


//                if(position == 0){
  //                  intent.putExtra(MyConfig.scheduleCotegory,"class");
//                }else {
//                    intent.putExtra(MyConfig.scheduleCotegory,"action");
//                }

                startActivity(intent);

            }
        });



        fragment_day_sch_listview.setAdapter(listAdapterDaySchedule);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
