package co.docy.oldfriends.Fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.LinkedList;

import co.docy.oldfriends.Adapters.ListAdapterContactMultiChoose;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.Messages.JsonGetCompanyUserListRet;
import co.docy.oldfriends.R;

public class FragmentContactMultiChoose extends FragmentBase {

    ListView listView;
    LinkedList<JsonGetCompanyUserListRet.GetCompanyUserListRet> list = new LinkedList<>();
    LinkedList<ContactUniversal> list_universal = new LinkedList<>();
    LinkedList<ContactUniversal> list_universal_selected = new LinkedList<>();
    ListAdapterContactMultiChoose listAdapterContactMultiChoose;
    MenuItem item;
    int mode;//0:single choose，1: multi choose

    public static FragmentContactMultiChoose newInstance(int mode) {
        FragmentContactMultiChoose f = new FragmentContactMultiChoose();

        Bundle args = new Bundle();
        args.putInt("mode", mode);
        f.setArguments(args);

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle args = getArguments();
        mode = args.getInt("mode", -1);

        if (mode == MyConfig.MODE_CHOOSE_MULTI) {
            setHasOptionsMenu(true);
        } else if (mode == MyConfig.MODE_CHOOSE_SINGLE) {
            setHasOptionsMenu(true);
        } else if (mode == MyConfig.MODE_CHOOSE_SINGLE_WITHOUT_SUBMIT) {
            setHasOptionsMenu(false);
        } else if (mode == MyConfig.MODE_CHOOSE_MULTI_DELETE) {
            setHasOptionsMenu(true);
            //       item.setTitle("删除");
        }

        View v = inflater.inflate(R.layout.fragment_contact_multi_choose, null);

        listView = (ListView) v.findViewById(R.id.contact_xjt_listview);
        listView = (ListView) v.findViewById(R.id.contact_xjt_listview);

        return v;
    }

    /**
     * 在Activity中被调用，用来传递数据
     * @param list
     */
    public void setList(final LinkedList<ContactUniversal> list) {



        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if(list == null){
                    list_universal = new LinkedList<ContactUniversal>();
                }else {
                    list_universal = list;
                }
                listAdapterContactMultiChoose = new ListAdapterContactMultiChoose(getActivity(), list_universal, mode, onClickedListener, null, null);
                listView.setAdapter(listAdapterContactMultiChoose);
            }
        });
    }

    /**
     * 在Activity中被调用，用来设置Activity的标题
     * @param title
     */
    public void setTitle(String title) {
        getActivity().setTitle(title);
    }

    @Override
    public void onResume() {
        super.onResume();

        MyLog.d("", "USR_STATUS FragmentContact onResume" + MyConfig.usr_status);
        MyLog.d("", "ListAdapterContactOfCompanyMultiChoose FragmentContactOfCompanyMultiChoose ");

//        getUserList();
    }
//
//    private void getUserList() {
//
//        if (MyConfig.usr_status < 2) {//要先设置当前公司，然后才有公司通讯录的显示
//            return;
//        }
//
//        if (MyConfig.jsonGetCurrentCompanyRet == null || MyConfig.jsonGetCurrentCompanyRet.data == null) {
//            MyLog.d("", "USR_STATUS FragmentContact getUserList jsonGetCurrentCompanyRet " + MyConfig.usr_status);
//            /**
//             * 一般情况下，只要usr_status >= 2，则MyConfig.jsonGetCurrentCompanyRet肯定不空，不用担心后面空指针
//             * 但在调试时，或者app重启时，从preference获取到上次的user status为2，但是还没有获取current company，所以这个指针会为空，后面会崩
//             */
//            return;
//        }
//
//        MyLog.d("", "USR_STATUS FragmentContact getUserList " + MyConfig.usr_status);
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Gson gson = new Gson();
//
//
//                final String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_USER_LIST + MyConfig.jsonGetCurrentCompanyRet.data.id + MyConfig.API2_COMPANY_USER_LIST_2, MyConfig.usr_token);
//                MyLog.d("", "xjt contact: s " + s);
//
//                final JsonGetCompanyUserListRet jsonGetCompanyUserListRet = gson.fromJson(s, JsonGetCompanyUserListRet.class);
//
//                if (jsonGetCompanyUserListRet != null && jsonGetCompanyUserListRet.code==MyConfig.retSuccess()) {
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            MyLog.d("", "xjt contact: setAdapter" + s);
//
//                            list = jsonGetCompanyUserListRet.data;
//                            MyLog.d("", "ListAdapterContactOfCompanyMultiChoose list.size " + list.size());
//                            for (JsonGetCompanyUserListRet.GetCompanyUserListRet getCompanyUserListRet : list) {
//                                boolean found = false;
//                                for (ContactUniversal contactUniversal : MyConfig.byPassContactUniversalList_temp) {
//                                    if (getCompanyUserListRet.id == contactUniversal.id) {
//                                        found = true;
//                                        break;
//                                    }
//                                }
//                                if (!found) {
//                                    list_universal.add(ContactUniversal.fromJsonGetCompanyUserListRet(getCompanyUserListRet));
//                                }
//                            }
//                            MyLog.d("", "ListAdapterContactOfCompanyMultiChoose byPassContactUniversalList_temp.size " + MyConfig.byPassContactUniversalList_temp.size());
//                            MyLog.d("", "ListAdapterContactOfCompanyMultiChoose list_universal.size " + list_universal.size());
//                            listAdapterContactMultiChoose = new ListAdapterContactOfCompanyMultiChoose(getActivity(), list_universal, onClickedListener, null, null);
//                            listView.setAdapter(listAdapterContactMultiChoose);
//
//                        }
//                    });
//
//                }
//
//            }
//        }).start();
//    }

    public ListAdapterContactMultiChoose.OnClickedListener onClickedListener = new ListAdapterContactMultiChoose.OnClickedListener() {
        @Override
        public void OnClicked(int position) {

            ContactUniversal contactUniversal = list_universal.get(position);
            contactUniversal.selected = !contactUniversal.selected;


            if (mode == MyConfig.MODE_CHOOSE_SINGLE) {

                if (contactUniversal.selected) {
                    /**
                     * 清空其他选项
                     */
                    for (ContactUniversal other : list_universal) {
                        if (other.id != list_universal.get(position).id) {
                            other.selected = false;
                        }
                    }
                }
            } else if (mode == MyConfig.MODE_CHOOSE_MULTI) {

            } else if (mode == MyConfig.MODE_CHOOSE_SINGLE_WITHOUT_SUBMIT) {

                submit();

            } else if (mode == MyConfig.MODE_CHOOSE_MULTI_DELETE) {

               // submit();
            }
            listAdapterContactMultiChoose.notifyDataSetChanged();
        }
    };
//
//    public ListAdapterContactOfCompanyMultiChoose.OnChooseChangedListener onChooseChangedListener = new ListAdapterContactOfCompanyMultiChoose.OnChooseChangedListener() {
//        @Override
//        public void OnClicked(int position, boolean isChecked) {
//
//            list_universal.get(position).selected = isChecked;
//
//            if (isChecked && mode == MyConfig.MODE_CHOOSE_SINGLE) {
//                /**
//                 * 清空其他选项
//                 */
//                for (ContactUniversal other : list_universal) {
//                    if (other.id != list_universal.get(position).id) {
//                        other.selected = false;
//                    }
//                }
//
//                listAdapterContactMultiChoose.notifyDataSetChanged();
//
//            }
//        }
//    };

//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        // Register
//        //EventBus.getDefault().register(this);
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        // Unregister
//        //EventBus.getDefault().unregister(this);
//    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_group_create, menu);
        if (mode == MyConfig.MODE_CHOOSE_MULTI_DELETE){
            menu.findItem(R.id.create_group_submit).setTitle(getActivity().getString(R.string.delete));
        }
        super.onCreateOptionsMenu(menu, inflater);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                Toast.makeText(this, "home key is pressed, input is discarded", Toast.LENGTH_LONG).show();

//                backWithoutResult();
                return true;
            case R.id.create_group_submit:
                MyLog.d("", "ListAdapterContactOfCompanyMultiChoose onOptionsItemSelected ");
                submit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void submit() {

        for (ContactUniversal contactUniversal : list_universal) {
            if (contactUniversal.selected) {
                list_universal_selected.add(contactUniversal);
            }
        }

        /**
         * 向服务器提交被选择的联系人
         */

        if (submitListener != null) {
            submitListener.OnClicked(list_universal_selected);
        }

//        getActivity().finish();

    }

    public interface OnSubmitClicked {
        public void OnClicked(LinkedList<ContactUniversal> list_universal_selected);
    }

    public OnSubmitClicked submitListener;

}
