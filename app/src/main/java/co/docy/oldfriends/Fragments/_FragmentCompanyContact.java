package co.docy.oldfriends.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import co.docy.oldfriends.Adapters.ListAdapterUserList;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.EventBus.EventUserStatusUpdate;
import co.docy.oldfriends.Messages.JsonGetCompanyUserListRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.CustomFastScrollView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class _FragmentCompanyContact extends FragmentBase {

    /**
     * 本fragment提供对公司某一个成员的各种操作，其功能与FragmentContactMultiChoose很不同，故不用整合
     * <p/>
     * 可能的操作
     * 点击进入私信
     * 查看个人信息
     * 设置标签、备注
     */

    ListView contact_xjt_lv;
    // private ListView listView;
    private CustomFastScrollView fastScrollView;
    FrameLayout contact_layout;
    private LinearLayout list_null_background_layout;
    ListAdapterUserList listAdapterCompanyUserList;
    float view_y_origin;
    private MyConfig.IF_AfterHttpRequest IFAfterHttpRequest = new MyConfig.IF_AfterHttpRequest() {
        @Override
        public void doAfterHttpRequest() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showJsonGetCompanyUserListRet();
                }
            });
        }
    };

    private void showJsonGetCompanyUserListRet() {
        listAdapterCompanyUserList = new ListAdapterUserList(getActivity(),
                ContactUniversal.convertListFromGetCompanyUserListRet(MyConfig.jsonGetCompanyUserListRet.data), null,
                null, null);
        contact_xjt_lv.setAdapter(listAdapterCompanyUserList);
        int count = MyConfig.jsonGetCompanyUserListRet.data.size() < 2 ? 0:MyConfig.jsonGetCompanyUserListRet.data.size();
        String format = getActivity().getString(R.string.contact_num);
        String result = String.format(format,count);
        contact_count.setText(result);
        if (MyConfig.jsonGetCompanyUserListRet.data.size() < 2) {
            list_null_background_layout.setVisibility(View.VISIBLE);
        } else {
            list_null_background_layout.setVisibility(View.GONE);
        }
        contact_xjt_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MyConfig.gotoUserInfo(getActivity(), MyConfig.jsonGetCompanyUserListRet.data.get(position).id);
            }
        });
    }

    private TextView contact_count;

    public static _FragmentCompanyContact newInstance(String s) {
        _FragmentCompanyContact f = new _FragmentCompanyContact();
        MyLog.d("", "read contact newInstance ");

        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString("index", s);
        f.setArguments(args);

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MyLog.d("", "read contact onCreateView");
        View v = inflater.inflate(R.layout.fragment_contact_with_scrollbutton, null);
        contact_xjt_lv = (ListView) v.findViewById(android.R.id.list);
        fastScrollView = (CustomFastScrollView) v.findViewById(R.id.fast_scroll_view);
        contact_count = (TextView) v.findViewById(R.id.contact_count);
        list_null_background_layout = (LinearLayout) v.findViewById(R.id.list_null_background_layout);

        return v;
    }



    @Override
    public void onResume() {
        super.onResume();

        MyLog.d("", "company change flow: USR_STATUS FragmentContact onResume" + MyConfig.usr_status);
        super.onResume();
        if (MyConfig.netConnectCheck() <= 0) {
            MyConfig.MyToast(0, getActivity(), getString(R.string.net_connect_error));
            return;
        }

        getCompanyContacts();
    }



    private void getCompanyContacts() {
        MyLog.d("", "company change flow: USR_STATUS FragmentContact getCompanyContacts a " + MyConfig.usr_status);
        if (MyConfig.usr_status < MyConfig.USR_STATUS_HAVE_CURRENT_COMPANY) {//要先设置当前公司，然后才有公司通讯录的显示
            return;
        }
        MyLog.d("", "company change flow: USR_STATUS FragmentContact getCompanyContacts b " + MyConfig.usr_status);
        if (MyConfig.jsonGetCurrentCompanyRet == null) {
            return;
        }
        MyLog.d("", "company change flow: USR_STATUS FragmentContact getCompanyContacts c " + MyConfig.usr_status);

        if(MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {
            MyConfig.getCurrentYearUserAddressBooks(MyConfig.jsonGetCurrentCompanyRet.data.id, IFAfterHttpRequest);
        }else{
            MyLog.d("", "retrofit: we use retrofit ");

            Call<JsonGetCompanyUserListRet> repos = HttpTools.http_api_service().getContacts("" + MyConfig.jsonGetCurrentCompanyRet.data.id);
            repos.enqueue(new Callback<JsonGetCompanyUserListRet>() {
                @Override
                public void onResponse(Call<JsonGetCompanyUserListRet> call, Response<JsonGetCompanyUserListRet> response) {
                    // Get result Repo from response.body()

                    MyLog.d("", "retrofit: code() " + response.code());
                    MyLog.d("", "retrofit: message() " + response.message());

                    if(response.code()==200){

                        MyConfig.jsonGetCompanyUserListRet = response.body();

                        MyLog.d("", "retrofit: body() 1 " + response.body().toString());
                        MyLog.d("", "retrofit: body() 2 " + response.body().code);
                        MyLog.d("", "retrofit: body() 3 " + response.body().message);

                        if(response.body().data != null) {
//                            iGetUserList.doAfterHttpRequest();
                            showJsonGetCompanyUserListRet();
                        }
                    }

                }

                @Override
                public void onFailure(Call<JsonGetCompanyUserListRet> call, Throwable t) {
                    MyLog.d("", "retrofit: onFailure " + t.getMessage());
                }
            });
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyLog.d("", "company change flow: USR_STATUS FragmentContact onCreate" + MyConfig.usr_status);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void onEventMainThread(EventUserStatusUpdate eventUserStatusUpdate) {
        MyLog.d("", "company change flow: onEventMainThread in FragmentSettingList.java: EventLogin");

        getCompanyContacts();
    }




}
