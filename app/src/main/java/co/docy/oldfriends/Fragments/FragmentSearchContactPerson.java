package co.docy.oldfriends.Fragments;

import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Adapters.ListAdapterUserList;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/5/4.
 */
public class FragmentSearchContactPerson extends FragmentBase{

    EditText ed;
    TextView close,search_hint;
    ImageView remove;
    LinearLayout linear;
    List<ContactUniversal> list = new ArrayList<>();

    public static FragmentSearchContactPerson newInstance() {
        FragmentSearchContactPerson f = new FragmentSearchContactPerson();
        return f;
    }
    ListView search_contact;
    ListAdapterUserList listAdapterUserList_;
    SearchView mSearchView;
    LinkedList<ContactUniversal> contactUniversalList = new LinkedList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search_contact_person,null);
        search_contact =(ListView) v.findViewById(R.id.fragment_search_contact_person_listview);
        listAdapterUserList_ = new ListAdapterUserList(getActivity(),
                contactUniversalList, null,
                onClickedListener_person_search, null);
        View view = getActivity().getLayoutInflater().inflate(R.layout.add_people_to_contact,null);
        search_contact.addHeaderView(view);
        search_contact.setAdapter(listAdapterUserList_);
        initWithHead(view);
        getsearchcontact();
        return  v;
    }
    public void initWithHead(View v){
        ed =(EditText) v.findViewById(R.id.activity_search_contact_edittext);
        close =(TextView) v.findViewById(R.id.activity_search_contact_close);
        remove =(ImageView) v.findViewById(R.id.activity_search_contact_remove);
        linear =(LinearLayout) v.findViewById(R.id.activity_search_contact_linear);
        search_hint =(TextView) v.findViewById(R.id.activity_search_contact_hint);
        search_hint.setVisibility(View.GONE);
        close.setVisibility(View.GONE);
        remove.setVisibility(View.GONE);

    }

    @Override
    public void onResume() {
        super.onResume();
        ed.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){//获得焦点

                    close.setVisibility(View.VISIBLE);
                    remove.setVisibility(View.VISIBLE);
                }else{//失去焦点
                    close.setVisibility(View.GONE);
                    remove.setVisibility(View.GONE);
                }
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed.setText("");
                ed.requestFocus();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed.setText("");
                ed.clearFocus();
                MyConfig.hide_keyboard_must_call_from_activity(getActivity());
            }
        });


        ed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                MyLog.d("kanghongpu","edittext的内容："+s);
                if(s==null||s.length()==0){
                    getsearchcontact();
                }else {
                    uiSearchcontact(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    public void getsearchcontact(){
        search_hint.setVisibility(View.GONE);
        contactUniversalList.clear();
        listAdapterUserList_.searchContactKey="";
        contactUniversalList.addAll(contactUniversalList.size(),
                ContactUniversal.convertListFromGetCompanyUserListRet(MyConfig.jsonGetCompanyUserListRet.data));
        search_contact.setAdapter(listAdapterUserList_);
    }

    private ListAdapterUserList.OnClickedListener onClickedListener_person = new ListAdapterUserList.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
            MyConfig.gotoUserInfo(getActivity(), ContactUniversal.convertListFromGetCompanyUserListRet(MyConfig.jsonGetCompanyUserListRet.data).get(position).id);
        }
    };

    private ListAdapterUserList.OnClickedListener onClickedListener_person_search = new ListAdapterUserList.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
            MyConfig.gotoUserInfo(getActivity(), contactUniversalList.get(position).id);
        }
    };
    //按条件搜索的方法
    private void uiSearchcontact(final String mCurFilter) {

                search_hint.setVisibility(View.GONE);
                contactUniversalList.clear();
                for (ContactUniversal contact : MyConfig.jsonGetCompanyUserListRet_universal) {

                    if (mCurFilter == null) {
                        contactUniversalList.add(contact);
                    } else if (
                            (contact.nickName != null && contact.nickName.contains(mCurFilter))
                                    || (contact.title != null && contact.title.contains(mCurFilter))
                                    || (contact.company != null && contact.company.contains(mCurFilter))
                            ) {
                        contactUniversalList.add(contact);
                    }
                }
                if(contactUniversalList==null||contactUniversalList.size()==0){
                    search_hint.setVisibility(View.VISIBLE);
                }
                listAdapterUserList_.searchContactKey = mCurFilter;
                listAdapterUserList_.notifyDataSetChanged();


    }

}
