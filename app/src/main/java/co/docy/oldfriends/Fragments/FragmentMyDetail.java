package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;

import co.docy.oldfriends.Activitis.ActivityUpdatePersonInfo;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;

public class FragmentMyDetail extends FragmentBase implements View.OnClickListener {


    private ImageView my_detail_portrait_photo;
    private TextView my_detail_name;
    private TextView my_detail_phone;
    private TextView my_detail_email;
    private TextView my_detail_class;
    private TextView my_detail_location;
    private TextView my_detail_company;
    private TextView my_detail_title;
    private TextView my_detail_company_introduce;
    private TextView my_detail_duty;
    private LinearLayout ll_my_detail_portrait_photo;
    private MyConfig.IOnOkClickListener onOkClickListener;
    private LinearLayout ll_my_detail_name;
    private LinearLayout ll_my_detail_phone;
    private LinearLayout ll_my_detail_email;
    private LinearLayout ll_my_detail_class;
    private LinearLayout ll_my_detail_location;
    private LinearLayout ll_my_detail_company;
    private LinearLayout ll_my_detail_title;
    private LinearLayout ll_my_detail_company_introduce;
    private LinearLayout ll_my_detail_duty;
    private ImageView my_detail_portrait_photo_arrow;
    private ImageView my_detail_phone_arrow;
    private ImageView my_detail_company_arrow;

    public static FragmentMyDetail newInstance(String s) {
        FragmentMyDetail f = new FragmentMyDetail();
        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_my_detail, null);

        initView(v);

        initListener();

        return v;
    }

    /**
     * 初始化View
     *
     * @param view
     */
    private void initView(View view) {

        /**下面为容器控件*/
        ll_my_detail_portrait_photo = (LinearLayout) view.findViewById(R.id.ll_my_detail_portrait_photo);
        ll_my_detail_name = (LinearLayout) view.findViewById(R.id.ll_my_detail_name);
        ll_my_detail_phone = (LinearLayout) view.findViewById(R.id.ll_my_detail_phone);
        ll_my_detail_email = (LinearLayout) view.findViewById(R.id.ll_my_detail_email);
        ll_my_detail_class = (LinearLayout) view.findViewById(R.id.ll_my_detail_class);
        ll_my_detail_location = (LinearLayout) view.findViewById(R.id.ll_my_detail_location);
        ll_my_detail_company = (LinearLayout) view.findViewById(R.id.ll_my_detail_company);
        ll_my_detail_title = (LinearLayout) view.findViewById(R.id.ll_my_detail_title);
        ll_my_detail_company_introduce = (LinearLayout) view.findViewById(R.id.ll_my_detail_company_introduce);
        ll_my_detail_duty = (LinearLayout) view.findViewById(R.id.ll_my_detail_duty);

        /**下面为子控件*/
        my_detail_portrait_photo = (ImageView) view.findViewById(R.id.my_detail_portrait_photo);
        my_detail_portrait_photo_arrow = (ImageView) view.findViewById(R.id.my_detail_portrait_photo_arrow);

        my_detail_name = (TextView) view.findViewById(R.id.my_detail_name);
        my_detail_phone = (TextView) view.findViewById(R.id.my_detail_phone);
        my_detail_phone_arrow = (ImageView) view.findViewById(R.id.my_detail_phone_arrow);

        my_detail_email = (TextView) view.findViewById(R.id.my_detail_email);
        my_detail_class = (TextView) view.findViewById(R.id.my_detail_class);
        my_detail_location = (TextView) view.findViewById(R.id.my_detail_location);
        my_detail_company = (TextView) view.findViewById(R.id.my_detail_company);
        my_detail_company_arrow = (ImageView) view.findViewById(R.id.my_detail_company_arrow);

        my_detail_title = (TextView) view.findViewById(R.id.my_detail_title);
        my_detail_company_introduce = (TextView) view.findViewById(R.id.my_detail_company_introduce);
        my_detail_duty = (TextView) view.findViewById(R.id.my_detail_duty);
    }

    /**
     * 注册监听器
     */
    private void initListener() {

        ll_my_detail_name.setOnClickListener(this);
        ll_my_detail_email.setOnClickListener(this);
        ll_my_detail_title.setOnClickListener(this);
        ll_my_detail_company_introduce.setOnClickListener(this);
        ll_my_detail_duty.setOnClickListener(this);

        /**班级不让修改*/
        // ll_my_detail_class.setOnClickListener(this);
        /**位置不让修改*/
        //ll_my_detail_location.setOnClickListener(this);

        /**下面这些根据用户的权限判断是否需要修改*/
        if (MyConfig.jsonGetMyPrivilege != null && MyConfig.jsonGetMyPrivilege.data != null) {
            Map<String, Boolean> privilege = MyConfig.jsonGetMyPrivilege.data;

            if (MyConfig.getValueFromMapNoNull(privilege, MyConfig.PRIVILEGE_CATEGORY_UPDATE_USER_AVATAR)) {
                ll_my_detail_portrait_photo.setOnClickListener(this);
                my_detail_portrait_photo_arrow.setVisibility(View.VISIBLE);
            }

            if (MyConfig.getValueFromMapNoNull(privilege, MyConfig.PRIVILEGE_CATEGORY_UPDATE_USER_PHONE)) {
                ll_my_detail_phone.setOnClickListener(this);
                my_detail_phone_arrow.setVisibility(View.VISIBLE);
            }

            if (MyConfig.getValueFromMapNoNull(privilege, MyConfig.PRIVILEGE_CATEGORY_UPDATE_USER_COMPANY)) {
                ll_my_detail_company.setOnClickListener(this);
                my_detail_company_arrow.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
    }

    /**
     * 初始化数据
     */
    private void initData() {


        Picasso.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + MyConfig.usr_avatar)
//                .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(my_detail_portrait_photo);


        my_detail_name.setText(MyConfig.usr_nickname);
        my_detail_phone.setText(MyConfig.usr_phone);
        my_detail_email.setText(MyConfig.usr_email);
        my_detail_class.setText(MyConfig.usr_current_class_name);
        my_detail_location.setText(MyConfig.usr_city);
        my_detail_company.setText(MyConfig.usr_company);
        my_detail_title.setText(MyConfig.usr_title);
        my_detail_company_introduce.setText(MyConfig.showDataByAnalyse(MyConfig.usr_company_intro));
        my_detail_duty.setText(MyConfig.showDataByAnalyse(MyConfig.usr_duty));


        /**打印个人信息，便于查看*/
        // printMyDetial();


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**点击头像*/
            case R.id.ll_my_detail_portrait_photo:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                intent.setType(MyConfig.MIME_IMAGE_STAR);

                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(intent, MyConfig.REQUEST_GALLERY_AVATAR);
                }
                break;


            case R.id.ll_my_detail_name:
                updatePersonInfo("nick_name");
                break;
            case R.id.ll_my_detail_phone:
                updatePersonInfo("phone");
                break;
            case R.id.ll_my_detail_email:
                updatePersonInfo("email");
                break;
            case R.id.ll_my_detail_class:
                updatePersonInfo("class");
                break;
            case R.id.ll_my_detail_location:
                updatePersonInfo("location");
                break;
            case R.id.ll_my_detail_company:
                updatePersonInfo("company");
                break;
            case R.id.ll_my_detail_title:
                updatePersonInfo("title");
                break;
            case R.id.ll_my_detail_company_introduce:
                updatePersonInfo("company_introduce");
                break;
            case R.id.ll_my_detail_duty:
                updatePersonInfo("duty");
                break;

           /* case R.id.fragment_setting_about:
                getActivity().startActivity(new Intent(getActivity(), ActivityAbout.class));
                break;
            case R.id.setting_company_invite:
                Intent inviteintent = new Intent(getActivity(), ActivityInputCompanyInviteCode.class);
                inviteintent.putExtra("createCompany", false);
                startActivityForResult(inviteintent, MyConfig.REQUEST_COMPANY_INVITE);
                break;
            case R.id.setting_person_quit:
                onOkClickListener = new MyConfig.IOnOkClickListener() {
                    @Override
                    public void OnOkClickListener() {
                        EventBus.getDefault().post(new EventLogout(MyConfig.LOGOUT_EVENT_TYPE_LOGOUT));
                    }
                };
                MyConfig.showRemindDialog("", getString(R.string.exit_sure), getActivity(), true, onOkClickListener);
                break;
            case R.id.setting_create_company:
                Intent create_company = new Intent(getActivity(), ActivityCreateCompany.class);
                startActivityForResult(create_company, MyConfig.REQUEST_CREATE_COMPANY);
                break;*/

        }

    }

    private void updatePersonInfo(String itemName) {


        Intent intent = new Intent(getActivity(), ActivityUpdatePersonInfo.class);
        /**
         * 所有可能修改的参数
         */


        Bundle bundle = new Bundle();
        bundle.putString("nick_name", MyConfig.usr_nickname);
        bundle.putString("phone", MyConfig.usr_phone);
        bundle.putString("email", MyConfig.usr_email);
        bundle.putString("class", MyConfig.usr_current_class_name);
        bundle.putString("location", MyConfig.usr_city);
        bundle.putString("company", MyConfig.usr_company);
        bundle.putString("title", MyConfig.usr_title);
        bundle.putString("company_introduce", MyConfig.usr_company_intro);
        bundle.putString("duty", MyConfig.usr_duty);
        intent.putExtras(bundle);
        /**
         * bundle中包含所有三项，但是目前的UI是一次修改一项
         * 故只有这个itemName所指项被显示
         */
        intent.putExtra("itemName", itemName);
        startActivityForResult(intent, MyConfig.REQUEST_USER_UPDATE);

    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putSerializable("list", (Serializable) list);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyLog.d("", "onActivityResult hit fragment! request code " + requestCode + " result code " + resultCode);
        if (requestCode == MyConfig.REQUEST_GALLERY_AVATAR && resultCode == Activity.RESULT_OK) {

            Intent innerIntent = new Intent("com.android.camera.action.CROP");
            innerIntent.setDataAndType(data.getData(), MyConfig.MIME_IMAGE_STAR);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true");// 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
            if (innerIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);
            }

        }

        if (requestCode == MyConfig.REQUEST_IMAGE_CROP && resultCode == Activity.RESULT_OK) {
            MyLog.d("", "onActivityResult hit fragment! REQUEST_IMAGE_CROP" + resultCode);

            Bitmap bitmap = data.getParcelableExtra("data");

            File tempAttach = new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg");

            // 图像保存到文件中
            FileOutputStream foutput = null;
            try {
                foutput = new FileOutputStream(tempAttach);
                if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, foutput)) {
//                    Toast.makeText(MyConfig.app,
//                            "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath(),
//                            Toast.LENGTH_LONG).show();
                    MyConfig.MyToast(-1, MyConfig.app,
                            "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath());
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            MyLog.d("", "onActivityResult hit fragment! uploadUserAvatar() file length " + tempAttach.length());
            MyConfig.uploadUserAvatar(tempAttach);
            // my_detail_portrait_photo.setImageBitmap(bitmap);
            Picasso.with(getActivity()).load(tempAttach)
                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(my_detail_portrait_photo);

        }
        if (requestCode == MyConfig.REQUEST_USER_UPDATE && resultCode == Activity.RESULT_OK) {
            /**更新页面*/
            initData();

        }


    }


    /**
     * 打印个人信息，便于查看
     */
    private void printMyDetial() {
        MyLog.d("kwwl", "MyConfig.usr_avatar==" + MyConfig.usr_avatar);
        MyLog.d("kwwl", "MyConfig.usr_city==" + MyConfig.usr_city);
        MyLog.d("kwwl", "MyConfig.usr_company==" + MyConfig.usr_company);
        MyLog.d("kwwl", "MyConfig.usr_company_intro==" + MyConfig.usr_company_intro);
        MyLog.d("kwwl", "MyConfig.usr_country_code==" + MyConfig.usr_country_code);
        MyLog.d("kwwl", "MyConfig.usr_current_class==" + MyConfig.usr_current_class_name);
        MyLog.d("kwwl", "MyConfig.usr_duty==" + MyConfig.usr_duty);
        MyLog.d("kwwl", "MyConfig.usr_email==" + MyConfig.usr_email);
        MyLog.d("kwwl", "MyConfig.usr_lang==" + MyConfig.usr_lang);
        MyLog.d("kwwl", "MyConfig.usr_login_ret_json==" + MyConfig.usr_login_ret_json);
        MyLog.d("kwwl", "MyConfig.usr_name==" + MyConfig.usr_name);
        MyLog.d("kwwl", "MyConfig.usr_nickname==" + MyConfig.usr_nickname);
        MyLog.d("kwwl", "MyConfig.usr_phone==" + MyConfig.usr_phone);
        MyLog.d("kwwl", "MyConfig.usr_pwd==" + MyConfig.usr_pwd);
        MyLog.d("kwwl", "MyConfig.usr_title==" + MyConfig.usr_title);
        MyLog.d("kwwl", "MyConfig.usr_origin==" + MyConfig.usr_origin);
        MyLog.d("kwwl", "MyConfig.usr_token==" + MyConfig.usr_token);

        MyLog.d("kwwl", "MyConfig.usr_title==" + MyConfig.usr_title);
    }

}
