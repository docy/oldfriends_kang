package co.docy.oldfriends.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import co.docy.oldfriends.Activitis.ActivityViewPager;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.R;

public class FragmentIntroducePage extends FragmentBaseV4 {

    public int page_position;

    TextView intro_frag_page_position_textview;
    TextView intro_frag_page_enter;
    FrameLayout intro_frag_page_position_layout;

    public static FragmentIntroducePage newInstance(int page_position) {
        FragmentIntroducePage f = new FragmentIntroducePage();
        f.page_position = page_position;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // The last two arguments ensure LayoutParams are inflated
        // properly.
        View rootView = inflater.inflate(
                R.layout.fragment_introduce_page, container, false);

        intro_frag_page_position_textview = (TextView)rootView.findViewById(R.id.intro_frag_page_position_textview);
        intro_frag_page_position_textview.setText("" + page_position);

        intro_frag_page_enter = (TextView)rootView.findViewById(R.id.intro_frag_page_enter);
        intro_frag_page_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MyConfig.app_open_count = 1;
//                MyConfig.saveAppOpenCountToPreference(getActivity());
                MyConfig.saveIntToPreference(getActivity(), "app_open_count", MyConfig.app_open_count);

                Intent newIntent = new Intent(getActivity(), ActivityViewPager.class);
                startActivity(newIntent);

                getActivity().finish();
            }
        });


        intro_frag_page_position_layout = (FrameLayout)rootView.findViewById(R.id.intro_frag_page_position_layout);
//        Picasso.with(getActivity()).load(R.drawable.intro_1_3x).fit().into(intro_frag_page_position_layout);

        switch (page_position){
            case 0:
                intro_frag_page_position_layout.setBackground(getActivity().getResources().getDrawable(R.drawable.c_intro_page_1_3x));
                break;
            case 1:
                intro_frag_page_position_layout.setBackground(getActivity().getResources().getDrawable(R.drawable.c_intro_page_2_3x));
                break;
            case 2:
                intro_frag_page_position_layout.setBackground(getActivity().getResources().getDrawable(R.drawable.c_intro_page_3_3x));
                break;
            case 3:
                intro_frag_page_position_layout.setBackground(getActivity().getResources().getDrawable(R.drawable.c_intro_page_4_3x));
                intro_frag_page_enter.setVisibility(View.VISIBLE);
                break;
        }

        return rootView;
    }


}
