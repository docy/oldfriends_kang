package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.joda.time.LocalDate;

import java.util.HashMap;
import java.util.LinkedList;

import co.docy.oldfriends.Activitis.ActivityAlumCard;
import co.docy.oldfriends.Activitis.ActivityClassEvaluate;
import co.docy.oldfriends.Activitis.ActivityFriendCircle;
import co.docy.oldfriends.Activitis.ActivityDaySchedule;
import co.docy.oldfriends.Activitis.ActivityH5Games;
import co.docy.oldfriends.Activitis.ActivityScheduleNotifications;
import co.docy.oldfriends.Activitis.ActivitySigned;
import co.docy.oldfriends.Adapters.GridAdapter_MadeInOrder_vertical;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.CalendarDay;
import co.docy.oldfriends.DataClass.CalendarMonth;
import co.docy.oldfriends.DataClass.CalendarWeek;
import co.docy.oldfriends.EventBus.EventCalendarCourseUpdate;
import co.docy.oldfriends.EventBus.EventLightNotificationRedDot;
import co.docy.oldfriends.Messages.JsonFriendCricleUnreadMsgRet;
import co.docy.oldfriends.Messages.JsonCreateAulmCardRet;
import co.docy.oldfriends.Messages.JsonGetCompanyUserListRet;
import co.docy.oldfriends.Messages.JsonMadeInOrderFromServiceRet;
import co.docy.oldfriends.Messages.JsonNotificationMsgFromSocket;
import co.docy.oldfriends.Messages.JsonNotificationReceiveRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.ViewCalendarTitle;
import co.docy.oldfriends.Views.ViewCalendarWeekWithActivity;
import de.greenrobot.event.EventBus;
import fr.castorflex.android.verticalviewpager.VerticalViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentV4AppVerticalViewpager extends FragmentBaseV4 {

    int mode_calendar_week_row = 6;//这个数字表示显示几行week

    VerticalViewPager verticalViewPager;

    /**
     * calendar
     */
    CalendarMonth currentCalendarMonth;
    CalendarWeek currentCalendarWeek;

    ViewCalendarTitle viewCalendarTitle;
    LinearLayout fragment_page_app_calendar_weeks;
    Space fragment_page_app_calendar_weeks_space;
    Space fragment_page_app_calendar_weeks_space_2;
    ViewCalendarWeekWithActivity viewCalendarWeekWithActivity_0;
    ViewCalendarWeekWithActivity viewCalendarWeekWithActivity_1;
    ViewCalendarWeekWithActivity viewCalendarWeekWithActivity_2;
    ViewCalendarWeekWithActivity viewCalendarWeekWithActivity_3;
    ViewCalendarWeekWithActivity viewCalendarWeekWithActivity_4;
    ViewCalendarWeekWithActivity viewCalendarWeekWithActivity_5;

    /**
     * app
     */
    RecyclerView fragment_page_app_icons;
    private GridLayoutManager mLayoutManager;
    RelativeLayout layout_notifications;
    RelativeLayout layout_alum_card, layout_signed;
    TextView fragment_page_app_notify_unread;
    ImageView fragment_page_notify_unread;
    TextView fragment_page_app_alum_card;
    GridAdapter_MadeInOrder_vertical gridAdapter_MadeInOrder_vertical;
    LinkedList<JsonMadeInOrderFromServiceRet.JsonMadeInOrder_appCenter> list = new LinkedList<>();

    public static FragmentV4AppVerticalViewpager newInstance() {
        FragmentV4AppVerticalViewpager f = new FragmentV4AppVerticalViewpager();

        return f;
    }

    ViewCalendarWeekWithActivity.DayOnClickListener dayOnClickListener =
            new ViewCalendarWeekWithActivity.DayOnClickListener() {
                @Override
                public void dayOnClicked(CalendarDay calendarDay) {
//                MyConfig.MyToast(0, getActivity(), "you clicked day "+day.toString());


                    if (!calendarDay.have_activity) {
                        Toast.makeText(FragmentV4AppVerticalViewpager.this.getActivity(), "暂时没有课程或活动安排", Toast.LENGTH_SHORT).show();
                        return;

                    }

                    MyConfig.tempLocalDate = calendarDay.day;
                    Intent intent = new Intent(getActivity(), ActivityDaySchedule.class);
                    startActivity(intent);

                }
            };

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_page_app_vertical_viewpager, null);

        viewCalendarTitle = (ViewCalendarTitle) v.findViewById(R.id.fragment_page_app_calendar_title);

        verticalViewPager = (VerticalViewPager) v.findViewById(R.id.fragment_page_app_calendar2);
        WizardPagerAdapter adapter = new WizardPagerAdapter();
        verticalViewPager.setAdapter(adapter);
        verticalViewPager.setOffscreenPageLimit(3);
        verticalViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                MyLog.d("", "fragment_page_app_calendar_weeks: pager change to " + position);

                if (position == 0) {
                    /**
                     * 向上抹动，导致position为0
                     * 此刻需要将日历模式切换为周模式
                     */
                    mode_calendar_week_row = 6;

                } else {

                    mode_calendar_week_row = 1;
                }

                updateCalendarPage();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        fragment_page_app_calendar_weeks = (LinearLayout) v.findViewById(R.id.fragment_page_app_calendar_weeks);
        fragment_page_app_calendar_weeks_space = (Space) v.findViewById(R.id.fragment_page_app_calendar_weeks_space);
        fragment_page_app_calendar_weeks_space_2 = (Space) v.findViewById(R.id.fragment_page_app_calendar_weeks_space_2);
        fragment_page_app_icons = (RecyclerView) v.findViewById(R.id.fragment_page_app_icons);
//        fragment_page_app_calendar_weeks.setOnTouchListener(onTouchListener);

        viewCalendarWeekWithActivity_0 = (ViewCalendarWeekWithActivity) v.findViewById(R.id.fragment_page_app_week_with_activity_0);
        viewCalendarWeekWithActivity_1 = (ViewCalendarWeekWithActivity) v.findViewById(R.id.fragment_page_app_week_with_activity_1);
        viewCalendarWeekWithActivity_2 = (ViewCalendarWeekWithActivity) v.findViewById(R.id.fragment_page_app_week_with_activity_2);
        viewCalendarWeekWithActivity_3 = (ViewCalendarWeekWithActivity) v.findViewById(R.id.fragment_page_app_week_with_activity_3);
        viewCalendarWeekWithActivity_4 = (ViewCalendarWeekWithActivity) v.findViewById(R.id.fragment_page_app_week_with_activity_4);
        viewCalendarWeekWithActivity_5 = (ViewCalendarWeekWithActivity) v.findViewById(R.id.fragment_page_app_week_with_activity_5);

        viewCalendarWeekWithActivity_0.dayOnClickListener = dayOnClickListener;
        viewCalendarWeekWithActivity_1.dayOnClickListener = dayOnClickListener;
        viewCalendarWeekWithActivity_2.dayOnClickListener = dayOnClickListener;
        viewCalendarWeekWithActivity_3.dayOnClickListener = dayOnClickListener;
        viewCalendarWeekWithActivity_4.dayOnClickListener = dayOnClickListener;
        viewCalendarWeekWithActivity_5.dayOnClickListener = dayOnClickListener;

        currentCalendarMonth = MyConfig.getMonthIncludeThisDay(new LocalDate());
        currentCalendarWeek = MyConfig.getWeekIncludeThisDay(new LocalDate());

        updateCalendarPage();

        viewCalendarTitle.gotoNext = new ViewCalendarTitle.GotoNext() {
            @Override
            public void gotoNext() {
                if (mode_calendar_week_row == 6) {
                    currentCalendarMonth = MyConfig.getMonthIncludeThisDay(currentCalendarMonth.firstDayOfCurrentMonth.plusDays(32));
                } else {
                    currentCalendarWeek = MyConfig.getWeekIncludeThisDay(currentCalendarWeek.calendarDayList.getLast().day.plusDays(1));
                }
                updateCalendarPage();
            }
        };
        viewCalendarTitle.gotoPrevious = new ViewCalendarTitle.GotoPrevious() {
            @Override
            public void gotoPrevious() {
                if (mode_calendar_week_row == 6) {
                    currentCalendarMonth = MyConfig.getMonthIncludeThisDay(currentCalendarMonth.firstDayOfCurrentMonth.plusDays(-1));
                } else {
                    currentCalendarWeek = MyConfig.getWeekIncludeThisDay(currentCalendarWeek.calendarDayList.getFirst().day.plusDays(-1));
                }
                updateCalendarPage();
            }
        };
        viewCalendarTitle.gotoToday = new ViewCalendarTitle.GotoToday() {
            @Override
            public void gotoToday() {

                if (mode_calendar_week_row == 6) {
                    currentCalendarMonth = MyConfig.getMonthIncludeThisDay(new LocalDate());
                } else {
                    currentCalendarWeek = MyConfig.getWeekIncludeThisDay(new LocalDate());
                }
                updateCalendarPage();
            }
        };

        if (
                MyConfig.jsonMadeInOrderFromServiceRet != null
                        && MyConfig.jsonMadeInOrderFromServiceRet.data != null
                        && MyConfig.jsonMadeInOrderFromServiceRet.data.appCenter != null
                        && MyConfig.jsonMadeInOrderFromServiceRet.data.appCenter.size() > 0
                ) {
            list = MyConfig.jsonMadeInOrderFromServiceRet.data.appCenter;
        } else {
//            EventBus.getDefault().post(new EventLogout(MyConfig.LOGOUT_EVENT_TYPE_LOGOUT_RESET_DATE));
        }
        mLayoutManager = new GridLayoutManager(getActivity(), 4);

        fragment_page_app_icons.setLayoutManager(mLayoutManager);
        gridAdapter_MadeInOrder_vertical = new GridAdapter_MadeInOrder_vertical(getActivity(), list, new GridAdapter_MadeInOrder_vertical.OnClickedListener() {
            @Override
            public void OnClicked(int position) {
                setDisplayPage(position);
            }
        });
        fragment_page_app_icons.setAdapter(gridAdapter_MadeInOrder_vertical);
       /* fragment_page_app_alum_card = (TextView) v.findViewById(R.id.fragment_page_app_alum_card);
        layout_alum_card = (RelativeLayout) v.findViewById(R.id.fragment_page_app_aulm_card_layout);
        layout_alum_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Gson gson = new Gson();
                        final String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_ALUM_CARD , MyConfig.usr_token);
                        MyLog.d("获取二维码数据：",s);
                        MyConfig.jsonCreateAulmCardRet = gson.fromJson(s,JsonCreateAulmCardRet.class);
                        Intent intent = new Intent(getActivity(), ActivityAlumCard.class);
                        startActivity(intent);
                    }
                }).start();

            }
        });
        fragment_page__notify_unread =(ImageView) v.findViewById(R.id.fragment_page_app_notify_unread);
     //   fragment_page_app_notify_unread = (TextView) v.findViewById(R.id.fragment_page_app_notify_unread);
        layout_notifications = (RelativeLayout) v.findViewById(R.id.fragment_page_app_notify);
        layout_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), ActivityScheduleNotifications.class);
                startActivity(intent);
                MyConfig.unRead_notification = 1;
            }
        });
        lightNotificationRedDot(false);
        if(MyConfig.unRead_notification != 1) {
            update_notify_unread_num();
        }
        layout_signed =(RelativeLayout) v.findViewById(R.id.fragment_page_app_sign_layout);
        layout_signed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ActivitySigned.class);
                startActivity(intent);
            }
        });*/
//        lightNotificationRedDot(false);
        if (MyConfig.unRead_notification != 1) {
            update_notify_unread_num();
        }

        return v;
    }

    public void setDisplayPage(int position) {
        //判断按键进入后显示哪一个页面
        if (list.get(position).name.trim().equals("通知")) {

            Intent intent = new Intent(getActivity(), ActivityScheduleNotifications.class);
            startActivity(intent);
            MyConfig.unRead_notification = 1;

        } else if (list.get(position).name.trim().equals("校友卡")) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    Gson gson = new Gson();
                    final String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_ALUM_CARD, MyConfig.usr_token);
                    MyLog.d("获取二维码数据：", s);
                    MyConfig.jsonCreateAulmCardRet = gson.fromJson(s, JsonCreateAulmCardRet.class);
                    Intent intent = new Intent(getActivity(), ActivityAlumCard.class);
                    startActivity(intent);
                }
            }).start();

        } else if (list.get(position).name.trim().equals("签到")) {

            Intent intent = new Intent(getActivity(), ActivitySigned.class);
            startActivity(intent);

        }else if(list.get(position).name.trim().equals("好友圈")){
            Intent intent = new Intent(getActivity(), ActivityFriendCircle.class);
            startActivity(intent);
            MyConfig.unRead_friend_cricle = 1;
        }else if(list.get(position).url!=null&&list.get(position).url.length()>0){
              if (list.get(position).name.trim().equals("游戏")){

            MyConfig.h5game_url = list.get(position).url;

            Intent intent = new Intent(getActivity(), ActivityH5Games.class);
//            intent.putExtra("h5game_url", list.get(position).url);
            startActivity(intent);
        }else{
                  Intent creditIntent = new Intent(getActivity(), ActivityClassEvaluate.class);
                  creditIntent.putExtra(MyConfig.WEB_VIEW_URL_CATEGORY,"H5page");
                  creditIntent.putExtra("url",list.get(position).url);
                  creditIntent.putExtra("name",list.get(position).name);
                  getActivity().startActivity(creditIntent);
              }
    }
    }

    public void update_notify_unread_num() {
        //通知上显示小红点
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                HashMap<String, String> map = new HashMap<String, String>();
                map.put("unread", true + "");
                final String s = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_RECEIVE_NOTIFICATION, map, MyConfig.usr_token);
                MyLog.d("判断是否有新消息", s);

                final JsonNotificationReceiveRet jsonNotificationReceiveRet = gson.fromJson(s, JsonNotificationReceiveRet.class);


                if (jsonNotificationReceiveRet != null && jsonNotificationReceiveRet.code == MyConfig.retSuccess()) {

                    MyConfig.unRead_notification_remind = jsonNotificationReceiveRet.data.size();

                    Activity activity = getActivity();
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (MyConfig.unRead_notification_remind > 0) {
                                    lightNotificationRedDot(true);
                                } else {
                                    lightNotificationRedDot(false);
                                }
                            }
                        });
                    }

                }

            }
        }).start();
    }

    public void update_friend_unread_num(){
        //判断朋友圈是否未读信息
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2()+MyConfig.API_GET_UNREAD_MOMENT,MyConfig.usr_token);
                MyLog.d("查看朋友圈未读信息",s);
                final JsonFriendCricleUnreadMsgRet friendCricleUnreadMsgRet = gson.fromJson(s,JsonFriendCricleUnreadMsgRet.class);
                if (friendCricleUnreadMsgRet != null&&friendCricleUnreadMsgRet.code==MyConfig.retSuccess()) {
                    Activity activity = getActivity();
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (friendCricleUnreadMsgRet.data!=null&&friendCricleUnreadMsgRet.data.count> 0) {
                                    lightFriendCricleRedDot(true);
                                } else {
                                    lightFriendCricleRedDot(false);
                                }
                            }
                        });
                    }

                }
            }
        }).start();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (MyConfig.unRead_notification == 1) {
            lightNotificationRedDot(false);
        }
        if (MyConfig.unRead_friend_cricle == 1) {
            lightFriendCricleRedDot(false);
        }else {
            update_friend_unread_num();
        }
        verticalViewPager.setCurrentItem(0, true);
    }

    private void updateCalendarPage() {

        if (mode_calendar_week_row == 6) {

            fragment_page_app_calendar_weeks_space.setVisibility(View.GONE);
            fragment_page_app_calendar_weeks_space_2.setVisibility(View.GONE);

            viewCalendarWeekWithActivity_0.setVisibility(View.VISIBLE);
            viewCalendarWeekWithActivity_1.setVisibility(View.VISIBLE);
            viewCalendarWeekWithActivity_2.setVisibility(View.VISIBLE);
            viewCalendarWeekWithActivity_3.setVisibility(View.VISIBLE);
            viewCalendarWeekWithActivity_4.setVisibility(View.VISIBLE);
            viewCalendarWeekWithActivity_5.setVisibility(View.VISIBLE);

            viewCalendarWeekWithActivity_0.setViewCalendarWeek(currentCalendarMonth.calendarWeeks.get(0), currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            viewCalendarWeekWithActivity_1.setViewCalendarWeek(currentCalendarMonth.calendarWeeks.get(1), currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            viewCalendarWeekWithActivity_2.setViewCalendarWeek(currentCalendarMonth.calendarWeeks.get(2), currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            viewCalendarWeekWithActivity_3.setViewCalendarWeek(currentCalendarMonth.calendarWeeks.get(3), currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());

            if (currentCalendarMonth.calendarWeeks.size() > 4) {
                viewCalendarWeekWithActivity_4.setViewCalendarWeek(currentCalendarMonth.calendarWeeks.get(4), currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            } else {
                viewCalendarWeekWithActivity_4.setViewCalendarWeek(null, currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
                viewCalendarWeekWithActivity_5.setViewCalendarWeek(null, currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            }
            if (currentCalendarMonth.calendarWeeks.size() > 5) {
                viewCalendarWeekWithActivity_5.setViewCalendarWeek(currentCalendarMonth.calendarWeeks.get(5), currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            } else {
                viewCalendarWeekWithActivity_5.setViewCalendarWeek(null, currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            }

            viewCalendarTitle.setCalendarIndicator("" + currentCalendarMonth.firstDayOfCurrentMonth.getYear() +
                    getActivity().getString(R.string.time_year) + currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear() + getActivity().getString(R.string.time_month));
        } else if (mode_calendar_week_row == 1) {
            fragment_page_app_calendar_weeks_space.setVisibility(View.VISIBLE);
            fragment_page_app_calendar_weeks_space_2.setVisibility(View.VISIBLE);

            viewCalendarWeekWithActivity_0.setVisibility(View.VISIBLE);
            viewCalendarWeekWithActivity_1.setVisibility(View.GONE);
            viewCalendarWeekWithActivity_2.setVisibility(View.GONE);
            viewCalendarWeekWithActivity_3.setVisibility(View.GONE);
            viewCalendarWeekWithActivity_4.setVisibility(View.GONE);
            viewCalendarWeekWithActivity_5.setVisibility(View.GONE);

            viewCalendarWeekWithActivity_0.setViewCalendarWeek(currentCalendarWeek, currentCalendarWeek.firstDayOfCurrentMonth.getMonthOfYear());

            viewCalendarTitle.setCalendarIndicator("" + currentCalendarWeek.firstDayOfCurrentMonth.getYear() +
                    getActivity().getString(R.string.time_year) + currentCalendarWeek.firstDayOfCurrentMonth.getMonthOfYear() + getActivity().getString(R.string.time_month));
        }
    }

    private void updataCalendarCourseFlag() {

        Call<JsonGetCompanyUserListRet> repos = HttpTools.http_api_service().getCourseListByDate("2016-04-04");
        repos.enqueue(new Callback<JsonGetCompanyUserListRet>() {
            @Override
            public void onResponse(Call<JsonGetCompanyUserListRet> call, Response<JsonGetCompanyUserListRet> response) {
                // Get result Repo from response.body()

                MyLog.d("", "retrofit: code() " + response.code());
                MyLog.d("", "retrofit: message() " + response.message());

                if (response.code() == 200) {

                    MyConfig.jsonGetCompanyUserListRet = response.body();

                    MyLog.d("", "retrofit: body() 1 " + response.body().toString());
                    MyLog.d("", "retrofit: body() 2 " + response.body().code);
                    MyLog.d("", "retrofit: body() 3 " + response.body().message);

                    if (response.body().data != null) {
//                        iGetUserList.doAfterHttpRequest();
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonGetCompanyUserListRet> call, Throwable t) {
                MyLog.d("", "retrofit: onFailure " + t.getMessage());
            }
        });

    }

    class WizardPagerAdapter extends PagerAdapter {

        public Object instantiateItem(ViewGroup collection, int position) {

            switch (position) {
                case 0:
                    return fragment_page_app_calendar_weeks;
                case 1:
                    return fragment_page_app_icons;
            }
            return fragment_page_app_calendar_weeks;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == ((View) arg1);
        }

        @Override
        public float getPageWidth(int position) {
            switch (position) {
                case 0:
                    return 0.55f;
                case 1:
                    return 0.9f;
            }
            return 0.8f;
        }
    }


    public void onEventMainThread(JsonNotificationMsgFromSocket jsonNotificationMsgFromSocket) {

        MyLog.d("", "socketio in service -- type: TYPE_NOTIFICATION! eventbus...");
        /**
         * 收到的必定是未读的，必定需要点亮小红点
         */
        lightNotificationRedDot(true);

    }

    private void lightNotificationRedDot(boolean b) {

        EventBus.getDefault().post(new EventLightNotificationRedDot(b));
        MyConfig.NOTICE_UNREADED = b;
        gridAdapter_MadeInOrder_vertical = new GridAdapter_MadeInOrder_vertical(getActivity(), list, new GridAdapter_MadeInOrder_vertical.OnClickedListener() {
            @Override
            public void OnClicked(int position) {
                setDisplayPage(position);
            }
        });
        fragment_page_app_icons.setAdapter(gridAdapter_MadeInOrder_vertical);

    }

    private void lightFriendCricleRedDot(boolean b) {
        //朋友圈未读信息的显示
        EventBus.getDefault().post(new EventLightNotificationRedDot(b));
        MyConfig.FRIEND_CRICLE_UNREADED = b;
        gridAdapter_MadeInOrder_vertical = new GridAdapter_MadeInOrder_vertical(getActivity(), list, new GridAdapter_MadeInOrder_vertical.OnClickedListener() {
            @Override
            public void OnClicked(int position) {
                setDisplayPage(position);
            }
        });
        fragment_page_app_icons.setAdapter(gridAdapter_MadeInOrder_vertical);

    }


    public void onEventMainThread(EventCalendarCourseUpdate eventCalendarCourseUpdate) {

        if (mode_calendar_week_row == 6) {
            currentCalendarMonth = MyConfig.getMonthIncludeThisDay(currentCalendarMonth.originDate);
        } else {
            currentCalendarWeek = MyConfig.getWeekIncludeThisDay(currentCalendarWeek.originDate);
        }
        updateCalendarPage();

    }
    public void onEventMainThread(JsonMadeInOrderFromServiceRet orderToVertical){
        list.clear();
        list.addAll(list.size(),MyConfig.jsonMadeInOrderFromServiceRet.data.appCenter);
        gridAdapter_MadeInOrder_vertical.notifyDataSetChanged();
    }


}