package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Activitis.ActivitySchoolContact;
import co.docy.oldfriends.Activitis.ActivitySearchContactPerson;
import co.docy.oldfriends.Adapters.ListAdapterUserListWithHeader;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.EventBus.EventAddFriend;
import co.docy.oldfriends.EventBus.EventContactDownloadList;
import co.docy.oldfriends.EventBus.EventContactDownloadPlease;
import co.docy.oldfriends.EventBus.EventUserStatusUpdate;
import co.docy.oldfriends.Messages.JsonCurrentYearClassRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;


public class FragmentV4CompanyContactTongChuang extends FragmentBaseV4 {

    LinkedList<JsonCurrentYearClassRet.CurrentYearClassRet> userGroupsRets = new LinkedList<>();//要同时显示班级信息
    ListView lv_contact;
    ListAdapterUserListWithHeader listAdapterCompanyUserList;
    List<ContactUniversal> list = new ArrayList<>();
    public interface getContactNum{
        void getContactNumBysize(int num);
    }
    public getContactNum getcontactnum;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        getcontactnum = (getContactNum)activity;
    }

    private MyConfig.IF_AfterHttpRequest ifAfterHttpGetCompanyContacts = new MyConfig.IF_AfterHttpRequest() {
        @Override
        public void doAfterHttpRequest() {

//            MyConfig.filterClassFromGroupList(ifGetClassList);
            list.clear();
            MyConfig.jsonGetCompanyUserListRet_universal = ContactUniversal.convertListFromGetCompanyUserListRet(MyConfig.jsonGetCompanyUserListRet.data);
            list.addAll(list.size(),ContactUniversal.convertListFromGetCompanyUserListRet(MyConfig.jsonGetCompanyUserListRet.data));
            listAdapterCompanyUserList.notifyDataSetChanged();
            getcontactnum.getContactNumBysize(list.size());
        }
    };

    private MyConfig.IF_AfterHttpRequest ifGetClassList = new MyConfig.IF_AfterHttpRequest() {
        @Override
        public void doAfterHttpRequest() {

            Activity activity = getActivity();
            if(activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }
        }
    };
    
    private ListAdapterUserListWithHeader.OnClickedListener onClickedListener_person = new ListAdapterUserListWithHeader.OnClickedListener() {
        @Override
        public void OnClicked(final int position) {

            MyConfig.gotoUserInfo(getActivity(), list.get(position).id);

        }
    };



    private LinkedList<ContactUniversal> getCompanyUserListSortByAlpha() {
        //通讯录排序，暂时不用，防止以后使用暂时放着
        MyConfig.jsonGetCompanyUserListRet_universal = ContactUniversal.convertListFromGetCompanyUserListRet(MyConfig.jsonGetCompanyUserListRet.data);

        ContactUniversal.setNameFirstLetter(MyConfig.jsonGetCompanyUserListRet_universal);


        Collections.sort(MyConfig.jsonGetCompanyUserListRet_universal, new Comparator<ContactUniversal>() {
            @Override
            public int compare(ContactUniversal o1, ContactUniversal o2) {
                if(o1.nickName.trim().substring(0,1).equals(o2.nickName.trim().substring(0,1))
                        &&o1.nickName.trim().substring(1,2).equals(o2.nickName.trim().substring(1,2))
                        &&o1.nickName.length()>=3&&o2.nickName.length()>=3){

                    return  ContactUniversal.compareThreeName(o1,o2);

                }else if(o1.nameFristLetter.equals(o2.nameFristLetter)){
                  return  ContactUniversal.compareSecondName(o1,o2);

                }else {
                    return ContactUniversal.compare(o1, o2);
                }
            }
        });
        return MyConfig.jsonGetCompanyUserListRet_universal;

    }

    public static FragmentV4CompanyContactTongChuang newInstance(String s) {
        FragmentV4CompanyContactTongChuang f = new FragmentV4CompanyContactTongChuang();
        MyLog.d("", "read contact newInstance ");

        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString("index", s);
        f.setArguments(args);

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        MyLog.d("", "read contact onCreateView");
        View v = inflater.inflate(R.layout.fragment_contact_with_scrollbutton_tongchuang, null);
        lv_contact = (ListView) v.findViewById(android.R.id.list);
        listAdapterCompanyUserList = new ListAdapterUserListWithHeader(getActivity(),
                list,onClickedListener_person);
        View view = getActivity().getLayoutInflater().inflate(R.layout.add_contact_with_head,null);
        lv_contact.addHeaderView(view);
        lv_contact.setAdapter(listAdapterCompanyUserList);

        initwithhead(view);
        getCompanyContacts();
        return v;
    }
    public void initwithhead(View view){
        LinearLayout addcontact =(LinearLayout) view.findViewById(R.id.contact_layout_class);
        LinearLayout searchcontact =(LinearLayout) view.findViewById(R.id.activity_search_contact_linear);
        addcontact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ActivitySchoolContact.class);
                startActivity(intent);
            }
        });
        searchcontact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ActivitySearchContactPerson.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        MyLog.d("", "company change flow: USR_STATUS FragmentContact onResume" + MyConfig.usr_status);
        if (MyConfig.netConnectCheck() <= 0) {
            MyConfig.MyToast(0, getActivity(), getString(R.string.net_connect_error));
        }
    }

    private void getCompanyContacts() {

        MyLog.d("", "company change flow: USR_STATUS FragmentContact getCompanyContacts a " + MyConfig.usr_status);
        if (MyConfig.usr_status < MyConfig.USR_STATUS_HAVE_CURRENT_COMPANY) {//要先设置当前公司，然后才有公司通讯录的显示
            return;
        }
        MyLog.d("", "company change flow: USR_STATUS FragmentContact getCompanyContacts b " + MyConfig.usr_status);
        if (MyConfig.jsonGetCurrentCompanyRet == null) {
            return;
        }
        MyLog.d("", "company change flow: USR_STATUS FragmentContact getCompanyContacts c " + MyConfig.usr_status);


        MyConfig.getCurrentYearUserAddressBooks(MyConfig.jsonGetCurrentCompanyRet.data.id, ifAfterHttpGetCompanyContacts);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyLog.d("", "company change flow: USR_STATUS FragmentContact onCreate" + MyConfig.usr_status);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void onEventMainThread(EventAddFriend eventAddFriend) {
        MyLog.d("", "company change flow: onEventMainThread in FragmentSettingList.java: EventLogin");

        getCompanyContacts();
    }

    public void onEventMainThread(EventContactDownloadPlease eventContactDownloadPlease) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java");

        EventBus.getDefault().post(new EventContactDownloadList(MyConfig.jsonGetCompanyUserListRet.data, EventContactDownloadList.DOWNLOAD_BEGIN));
    }

    public void onEventMainThread(EventUserStatusUpdate eventUserStatusUpdate) {
        MyLog.d("", "company change flow: onEventMainThread in FragmentSettingList.java: EventLogin");

        getCompanyContacts();
    }

}
