package co.docy.oldfriends.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.Locale;

import co.docy.oldfriends.Activitis._ActivityRegistPersonInfo;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventHeartOneSecond;
import co.docy.oldfriends.Messages.JsonLogin;
import co.docy.oldfriends.Messages.JsonRegistRet;
import co.docy.oldfriends.Messages.JsonRegistWithPhoto;
import co.docy.oldfriends.Messages.JsonSmsSignup;
import co.docy.oldfriends.Messages.JsonSmsSignupRet;
import co.docy.oldfriends.Messages.JsonWeixinLoginRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

public class _FragmentV4AccountRegist2 extends FragmentBaseV4 implements View.OnFocusChangeListener {

    int count = 0;
    boolean waitting_server;
    JsonRegistWithPhoto jsonRegistWithPhoto;

    public TextView fragment_page_account_regist_2_phone;
    public String phone;
    EditText fragment_page_account_regist_2_code;
    EditText fragment_page_account_regist_2_name;
    EditText fragment_page_account_regist_2_pwd;
    Button fragment_page_account_regist_2_code_button;
    ImageView fragment_page_account_regist_2_next;
    ImageView icon_validation_code, icon_username, icon_password;

    public static _FragmentV4AccountRegist2 newInstance() {
        _FragmentV4AccountRegist2 f = new _FragmentV4AccountRegist2();

        return f;
    }

    public View mView;

    public void setVisibile(int visibile) {
        mView.setVisibility(visibile);
    }

    public void setY(float y) {
        mView.setY(y);
    }

    public float getY() {
        return mView.getY();
    }

    public int setCount(int tempcount){
         count  = tempcount;
        return count;
    }
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_page_account_regist_2, null);
        mView = v;

        icon_password = (ImageView) v.findViewById(R.id.password_icon);
        icon_username = (ImageView) v.findViewById(R.id.username_icon);
        icon_validation_code = (ImageView) v.findViewById(R.id.validation_icon);

        fragment_page_account_regist_2_phone = (TextView) v.findViewById(R.id.fragment_page_account_regist_2_phone);
        fragment_page_account_regist_2_code = (EditText) v.findViewById(R.id.fragment_page_account_regist_2_code);
        fragment_page_account_regist_2_name = (EditText) v.findViewById(R.id.fragment_page_account_regist_2_name);
        fragment_page_account_regist_2_pwd = (EditText) v.findViewById(R.id.fragment_page_account_regist_2_pwd);
        fragment_page_account_regist_2_code_button = (Button) v.findViewById(R.id.fragment_page_account_regist_2_code_button);
        fragment_page_account_regist_2_code_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count == 0) {
                    smsSignup();
                }
            }
        });
        fragment_page_account_regist_2_next = (ImageView) v.findViewById(R.id.fragment_page_account_regist_2_next);
        fragment_page_account_regist_2_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoRegist();
            }
        });
        fragment_page_account_regist_2_code.setOnFocusChangeListener(this);
        fragment_page_account_regist_2_name.setOnFocusChangeListener(this);
        fragment_page_account_regist_2_pwd.setOnFocusChangeListener(this);

        return v;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
//        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        // count = 120;当前短信服务商要求的发送间隔
    }


    private void smsSignup() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSmsSignup jsonSmsSignup = new JsonSmsSignup();
//                jsonSmsSignup.phone = fragment_page_account_regist_2_phone.getText().toString().trim();
                jsonSmsSignup.phone = phone;
                jsonSmsSignup.countryCode = MyConfig.app_country.phone_prefix;


                String jsonStr = gson.toJson(jsonSmsSignup, JsonSmsSignup.class);
                MyLog.d("", "httptools: send country " + jsonStr);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SMS_SIGNUP, jsonStr);
                MyLog.d("", "apitest: send msg ret " + s);

                final JsonSmsSignupRet jsonSmsSignupRet = gson.fromJson(s, JsonSmsSignupRet.class);
                if (jsonSmsSignupRet != null) {

                    if (jsonSmsSignupRet.code == MyConfig.retSuccess()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                count = 120;//当前短信服务商要求的发送间隔
                                String format = getResources().getString(R.string.please_wait);
                                String result = String.format(format, count);
                                fragment_page_account_regist_2_code_button.setText(result);
                                if (MyConfig.debug_autofill_validcode) {
                                    fragment_page_account_regist_2_code.setText(jsonSmsSignupRet.data.code);
                                }

                            }
                        });
                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

//                                MyConfig.MyToast(0, getActivity(), jsonSmsSignupRet.message);
                                MyConfig.MyToast(0, getActivity(), MyConfig.getErrorMsg(""+jsonSmsSignupRet.code));

                            }
                        });

                    }
                } else {
                    MyConfig.MyToast(0, getActivity(), getResources().getString(R.string.server_connect_fail));
                }

            }
        }).start();
    }


    public void onEventMainThread(EventHeartOneSecond eventHeartOneSecond) {

        if (count > 0) {
            count--;
        }
        if (count > 0) {
            String format = getResources().getString(R.string.please_wait);
            String result = String.format(format, count);
            fragment_page_account_regist_2_code_button.setText(result);
        } else {
            fragment_page_account_regist_2_code_button.setText(getString(R.string.prompt_valid_button));
        }

    }


    private void gotoRegist() {

        jsonRegistWithPhoto = new JsonRegistWithPhoto();
        jsonRegistWithPhoto.countryCode = MyConfig.app_country.phone_prefix;
        jsonRegistWithPhoto.name = fragment_page_account_regist_2_name.getText().toString().trim();
        fragment_page_account_regist_2_name.setText(jsonRegistWithPhoto.name);
//        jsonRegistWithPhoto.email = et_email.getText().toString().trim();
//        et_email.setText(jsonRegistWithPhoto.email);
        jsonRegistWithPhoto.phone = phone;
        fragment_page_account_regist_2_phone.setText(jsonRegistWithPhoto.phone);
        jsonRegistWithPhoto.code = fragment_page_account_regist_2_code.getText().toString().trim();
        jsonRegistWithPhoto.password = fragment_page_account_regist_2_pwd.getText().toString().trim();
        fragment_page_account_regist_2_pwd.setText(jsonRegistWithPhoto.password);
        jsonRegistWithPhoto.lang = (Locale.getDefault().getLanguage()).toUpperCase();
        MyLog.d("","jsonRegistWithPhoto.lang = Locale.getDefault().getLanguage()"+Locale.getDefault().getLanguage()+"   jsonRegistWithPhoto.countryCode ===="+jsonRegistWithPhoto.countryCode);

        jsonRegistWithPhoto.sex = true;//先假定为男，下一步更改

        /**
         * 这里客户端先自己做一个合法性检测
         */
        if (jsonRegistWithPhoto.name.length() == 0) {
            notifyRegistResualt(getResources().getString(R.string.null_name));
            return;
        }
        if (jsonRegistWithPhoto.phone.length() == 0) {
            notifyRegistResualt(getResources().getString(R.string.null_phone));
            return;
        }
//        if (jsonRegistWithPhoto.email.length() == 0) {
//            notifyRegistResualt(getResources().getString(R.string.null_email));
//            return;
//        }
        //客户端取消密码限制 20151216
//        if (jsonRegistWithPhoto.password.length() < getResources().getInteger(R.integer.mini_length_user_pwd)) {
//            notifyRegistResualt("密码不能少于6位");
//            return;
//        }
        if (jsonRegistWithPhoto.code.length() == 0) {
            notifyRegistResualt(getResources().getString(R.string.null_code));
            return;
        }


        /**
         * 新注册接口，包含可选的photo信息
         */
        new Thread(new Runnable() {
            @Override
            public void run() {

                showWaitAnimator(true);

                JsonRegistRet jsonRegistRet = HttpTools.okhttpUploadRegistWithPhoto(jsonRegistWithPhoto);


                if (jsonRegistRet != null) {

                    if (jsonRegistRet.code != MyConfig.retSuccess()) {
//                        notifyRegistResualt(jsonRegistRet.message);
                        notifyRegistResualt(MyConfig.getErrorMsg(""+jsonRegistRet.code));

                    } else {

//                        notifyRegistResualt(getResources().getString(R.string.regist_success));
                        notifyRegistResualt(MyConfig.getErrorMsg("" + jsonRegistRet.code));//成功也打印？

//                        MyConfig.prefEditor.putString(MyConfig.PREF_USER_NAME, jsonRegistWithPhoto.name);//下一步有，这里似乎多余
//                        MyConfig.prefEditor.putString(MyConfig.PREF_USER_PWD, jsonRegistWithPhoto.password);
//                        MyConfig.prefEditor.commit();

                        loginRequest();

                    }
                } else {
                    notifyRegistResualt(getResources().getString(R.string.server_not_return));
                }

                showWaitAnimator(false);

            }
        }).start();


    }


    public void loginRequest() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonLogin jsonLogin = new JsonLogin();
                jsonLogin.account = jsonRegistWithPhoto.name;
                jsonLogin.password = jsonRegistWithPhoto.password;
                jsonLogin.os = MyConfig.os_string;
                jsonLogin.uuid = MyConfig.uuid;

                Gson gson = new Gson();
                String json_login = gson.toJson(jsonLogin);

                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LOGIN, json_login);

                JsonWeixinLoginRet jsonLoginRet = gson.fromJson(s, JsonWeixinLoginRet.class);

                if (jsonLoginRet != null) {

                    if (jsonLoginRet != null) {
                        if (jsonLoginRet.code == MyConfig.retSuccess()) {

                            MyConfig.logoutClear();

                            MyConfig.usr_login_ret_json = s;
                            MyConfig.jsonWeixinLoginRet = jsonLoginRet;
                            MyConfig.usr_id = jsonLoginRet.data.id;
                            MyConfig.usr_name = jsonLoginRet.data.name;//注意name要从这里取，因为login_name_or_phone也可能是phone
                            MyConfig.usr_nickname = jsonLoginRet.data.nickName;
                            MyConfig.usr_phone = jsonLoginRet.data.phone;
//                            MyConfig.usr_pwd = jsonLoginRet.data.password;//小心，或许以后这里不会返回密码，则需要从jsonRegistWithPhoto获取
                            MyConfig.usr_pwd = jsonRegistWithPhoto.password;
                            MyConfig.usr_token = jsonLoginRet.data.authToken;//常用变量就提取出来
                            MyConfig.usr_status = MyConfig.USR_STATUS_LOGIN;
                            //    MyConfig.usr_email = jsonLoginRet.data.email;
                            MyConfig.usr_phone = jsonLoginRet.data.phone;
                            if (jsonLoginRet.data.sex) {
                                MyConfig.usr_sex = MyConfig.USER_SEX_MAN;//男
                            } else {
                                MyConfig.usr_sex = MyConfig.USER_SEX_MAN;//女
                            }
                            MyConfig.usr_avatar = jsonLoginRet.data.avatar;//实际上这一步没有avatar信息
                            MyConfig.usr_origin = jsonLoginRet.data.origin;
                            if(jsonLoginRet.data.info != null){MyConfig.usr_name_changed = jsonLoginRet.data.info.changed;}


                            MyConfig.saveUserDataToPreference(getActivity());

                            /**
                             * 在新的流程里面，注册之后不再弹出个人信息完善页面及邀请码页面
                             */
                            Intent intent_person_info = new Intent(getActivity(), _ActivityRegistPersonInfo.class);
                            startActivity(intent_person_info);

                            MyConfig.flag_open_invite_code_activity = 1;//0, no invite, 1,please start invite, 2, have started invite
                            MyLog.d("", "app start steps 0, regist set flag_open_invite_code_activity");

                            getActivity().finish();

                        } else {
                            finishWithLoginFailed(MyConfig.getErrorMsg(""+jsonLoginRet.code));

                        }
                    }

                } else {

                    finishWithLoginFailed(MyConfig.getErrorMsg(""+jsonLoginRet.code));

//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
////                            Toast.makeText(ActivityLogin.this, getString(R.string.net_msg_not_connected), Toast.LENGTH_SHORT).show();
//                            MyConfig.MyToast(0, getActivity(), getString(R.string.net_msg_not_connected));
//                        }
//                    });

                }

            }
        }).start();
    }

    private void notifyRegistResualt(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MyConfig.MyToast(0, getActivity(), message);
            }
        });

    }


    public void finishWithLoginFailed(final String message) {
        /**
         * 理论上这里不会有失败
         * 登录失败则跳转到登录页面
         */
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MyConfig.MyToast(0, getActivity(), message);
            }
        });
    }


    private void showWaitAnimator(final boolean b) {

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.fragment_page_account_regist_2_code:
                if (hasFocus) {
                  icon_validation_code.setBackgroundResource(R.drawable.login_icon_identify_focus_3x);
                } else {
                    icon_validation_code.setBackgroundResource(R.drawable.login_icon_identify_3x);
                }
                break;
            case R.id.fragment_page_account_regist_2_name:
                if (hasFocus) {
                    icon_username.setBackgroundResource(R.drawable.login_icon_user_name_focus);
                } else {
                    icon_username.setBackgroundResource(R.drawable.login_icon_user_name);
                }
                break;
            case R.id.fragment_page_account_regist_2_pwd:
                if (hasFocus) {
                    icon_password.setBackgroundResource(R.drawable.login_icon_password_focus);
                } else {
                    icon_password.setBackgroundResource(R.drawable.login_icon_password_3x);
                }
                break;
        }
    }
}
