package co.docy.oldfriends.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.R;
import fr.castorflex.android.verticalviewpager.VerticalViewPager;

public class _FragmentV4VerticalViewpagerTest extends FragmentBaseV4 {

    VerticalViewPager verticalViewPager;

    public static _FragmentV4VerticalViewpagerTest newInstance() {
        _FragmentV4VerticalViewpagerTest f = new _FragmentV4VerticalViewpagerTest();

        return f;
    }


    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_page_vertical_viewpager_test, null);

        sectionsPagerAdapter = new SectionsPagerAdapter(getActivity().getSupportFragmentManager());


        verticalViewPager = (VerticalViewPager) v.findViewById(R.id.fragment_page_app_calendar2);
        verticalViewPager.setAdapter(sectionsPagerAdapter);

        return v;
    }

    public static class DummyFragment extends Fragment {

        public static DummyFragment newInstance(int index) {
            DummyFragment f = new DummyFragment();

            // Supply index input as an argument.
            Bundle args = new Bundle();
            args.putInt("str", index);
            f.setArguments(args);

            return f;
        }


        @Override
        public View onCreateView(LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {
            TextView v = new TextView(getActivity());
            v.setText("" + getArguments().getInt("str"));
            v.setBackgroundColor(getResources().getColor(R.color.xjt_unread_color));

            return v;
        }

    }

    SectionsPagerAdapter sectionsPagerAdapter;

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            MyLog.d("", "viewpager get fragment " + position);
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.

            if (position == 0) {
                return DummyFragment.newInstance(0);
            } else if (position == 1) {
                return DummyFragment.newInstance(1);
//            } else if (position == 2) {
//                return DummyFragment.newInstance(2);
//            } else if (position == 3) {
//                return DummyFragment.newInstance(3);
            } else {
                return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    //  toolbar.setVisibility(View.VISIBLE);
                    return getString(R.string.pager_groups).toUpperCase(l);
                case 1:
                    // toolbar.setVisibility(View.GONE);
                    return getString(R.string.pager_direct).toUpperCase(l);
                case 2:
                    //  toolbar.setVisibility(View.GONE);
                    return getString(R.string.pager_notification).toUpperCase(l);
                case 3:
                    //  toolbar.setVisibility(View.GONE);
                    return getString(R.string.pager_setting).toUpperCase(l);
//                case 4:
//                    return getString(R.string.pager_4_companies).toUpperCase(l);
            }
            return null;
        }

        @Override
        public float getPageWidth(int position) {
            return 0.8f;
        }
    }


}
