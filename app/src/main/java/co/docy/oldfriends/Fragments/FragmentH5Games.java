package co.docy.oldfriends.Fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.R;

public class FragmentH5Games extends FragmentBase {

    WebView mWebView;
    ImageButton button_back, button_forward, button_home, button_stop;

    public static FragmentH5Games newInstance() {
        FragmentH5Games f = new FragmentH5Games();

        return f;
    }
     public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_h5_games, null);

         mWebView = (WebView)v.findViewById(R.id.fragment_h5game_webview);

         button_back = (ImageButton)v.findViewById(R.id.h5game_browse_back);
         button_back.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 if(mWebView.canGoBack()){
                     mWebView.goBack();
                 }
             }
         });
         button_forward = (ImageButton)v.findViewById(R.id.h5game_browse_forward);
         button_forward.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 if(mWebView.canGoForward()){
                     mWebView.goForward();
                 }
             }
         });
         button_home = (ImageButton)v.findViewById(R.id.h5game_browse_home);
         button_home.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 mWebView.loadUrl(MyConfig.h5game_url);
             }
         });
         button_stop = (ImageButton)v.findViewById(R.id.h5game_browse_stop);
         button_stop.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 mWebView.stopLoading();
             }
         });

        return v;
    }


    @Override
    public void onResume() {
        super.onResume();

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }});

        mWebView.loadUrl(MyConfig.h5game_url);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
