package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Activitis._ActivityCompanyInfo;
import co.docy.oldfriends.Adapters._ListAdapterCanJoinCompany;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.Company;
import co.docy.oldfriends.EventBus.EventCompanyJoined;
import co.docy.oldfriends.EventBus.EventCompanyListUpdateUi;
import co.docy.oldfriends.Messages.JsonCompanyCanJoinRet;
import co.docy.oldfriends.Messages.JsonJoinCompany;
import co.docy.oldfriends.Messages.JsonLeaveCompany;
import co.docy.oldfriends.Messages.JsonLeaveCompanyRet;
import co.docy.oldfriends.Messages.JsonSetCurrentCompany;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;

public class _FragmentCanJoinCompanyList extends FragmentBase implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    _ListAdapterCanJoinCompany adapter;
    List<Company> list = new LinkedList<>();
    ListView lv_companys;
    TextView fragment_canjoincompany_count;


    //   RelativeLayout fragment_canjoincompany_invite_code;

    SearchView mSearchView;
    String mCurFilter;
    Activity activity;
    private MyConfig.IOnOkClickListener iOnOkClickListener;

    public static _FragmentCanJoinCompanyList newInstance(Activity activity) {
        _FragmentCanJoinCompanyList f = new _FragmentCanJoinCompanyList();
        f.activity = activity;
        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_canjoincompany_list, null);
        MyLog.d("", "FragmentCanJoinCompanyList onCreateView");
//        activity = getActivity();
//        fragment_canjoincompany_invite_code = (RelativeLayout) v.findViewById(R.id.fragment_canjoincompany_invite_code);
//        fragment_canjoincompany_invite_code.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), ActivityInputCompanyInviteCode.class);
//                intent.putExtra("mode", 1);
//                startActivity(intent);
//            }
//        });
        fragment_canjoincompany_count = (TextView) v.findViewById(R.id.fragment_canjoincompany_count);


        lv_companys = (ListView) v.findViewById(R.id.fragment_canjoincompany_list);
        adapter = new _ListAdapterCanJoinCompany(getActivity(), list);
        adapter.enterClickListener = enterClickListener;
        adapter.jionClickListener = onJoinClickedListener;
        lv_companys.setAdapter(adapter);
        setHasOptionsMenu(true);

        return v;
    }

    private void popMenu(final int position) {

        new DialogFragment() {
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                // Use the Builder class for convenient dialog construction
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                String title;

                if (list.get(position).status == 0) {//have not joined
                    builder.setMessage(getString(R.string.prompt_if_join) + list.get(position).createCompanyRet.name + "?")
                            .setPositiveButton(R.string.prompt_confirm, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    JsonJoinCompany jsonJoinCompany = new JsonJoinCompany();
                                    jsonJoinCompany.accessToken = MyConfig.usr_token;
                                    jsonJoinCompany.id = list.get(position).createCompanyRet.id;
                                    MyLog.d("", "HttpTools: joinCompany: ");
                                    MyConfig.joinCompany(jsonJoinCompany);
                                }
                            })
                            .setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                } else {
                    builder.setMessage(getString(R.string.prompt_if_quit))
                            .setPositiveButton(R.string.prompt_confirm, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    JsonLeaveCompany jsonLeaveCompany = new JsonLeaveCompany();
                                    jsonLeaveCompany.accessToken = MyConfig.usr_token;
                                    jsonLeaveCompany.companyId = list.get(position).createCompanyRet.id;
                                    leaveCompany(jsonLeaveCompany);
                                }
                            })
                            .setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                }
                // Create the AlertDialog object and return it
                return builder.create();
            }
        }.show(getFragmentManager(), "");
    }

//    ListAdapterCanJoinCompany.OnLongClickedListener onLongClickedListener = new ListAdapterCanJoinCompany.OnLongClickedListener() {
//        @Override
//        public void OnLongClicked(int position) {
//
//        }
//    };


    public void leaveCompany(final JsonLeaveCompany jsonLeaveCompany) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonLeaveCompany, JsonLeaveCompany.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_LEAVE + jsonLeaveCompany.companyId + MyConfig.API2_LEAVE_COMPANY_2, jsonStr);

                final JsonLeaveCompanyRet jsonLeaveCompanyRet = gson.fromJson(s, JsonLeaveCompanyRet.class);
                MyLog.d("", "HttpTools: JsonLeaveRet " + s);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (jsonLeaveCompanyRet != null) {

                            if (jsonLeaveCompanyRet.code==MyConfig.retSuccess()) {

                                EventBus.getDefault().post(new EventCompanyJoined(jsonLeaveCompany.companyId, ""));

                            } else {
                                MyConfig.MyToast(0, getActivity(),
                                        jsonLeaveCompanyRet.message);

                            }

                        } else {
                            MyConfig.MyToast(0, getActivity(),
                                    getActivity().getString(R.string.prompt_return_failure));

                        }
                    }
                });


            }
        }).start();

    }


    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        uiCompanyListUpdateFromLocal();
    }

    public void onEventMainThread(EventCompanyListUpdateUi eventCompanyListUpdateUi) {

        uiCompanyListUpdateFromLocal();
    }

    public void onEventMainThread(EventCompanyJoined eventCompanyJoined) {
        if (eventCompanyJoined.companyId > 0) {
            MyConfig.setCurrentCompany(new JsonSetCurrentCompany(eventCompanyJoined.companyId));
        }
    }


    private void uiCompanyListUpdateFromLocal() {

        MyLog.d("", "httptools: FragmentCanJoinCompanyList uiCompanyListUpdateFromLocal");

        /**
         * 更新UI界面中的group list
         */

        list.clear();

        if (MyConfig.jsonCompanyCanJoinRet != null
                && MyConfig.jsonCompanyCanJoinRet.code==MyConfig.retSuccess()
                && MyConfig.jsonCompanyCanJoinRet.data != null) {

            MyLog.d("", "httptools: FragmentCanJoinCompanyList uiCompanyListUpdateFromLocal list size:" + MyConfig.jsonCompanyCanJoinRet.data.size());

            for (JsonCompanyCanJoinRet.CompanyCanJoinRet rcjr : MyConfig.jsonCompanyCanJoinRet.data) {

                if (mCurFilter == null) {
                    list.add(Company.fromCompanyCanJoin(rcjr));
                } else if (rcjr.name.contains(mCurFilter)) {
                    list.add(Company.fromCompanyCanJoin(rcjr));
                }

            }
        }
        String format = getActivity().getResources().getString(R.string.search_company_result);
        String result = String.format(format, list.size());
        fragment_canjoincompany_count.setText(result);
        adapter.notifyDataSetChanged();
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java 2");

    }


    //    ListAdapterCanJoinCompany.OnClickedListener onClickedListener = new ListAdapterCanJoinCompany.OnClickedListener() {
//        @Override
//        public void OnClicked(int position) {
//            // popMenu(position);
//            showDialog(position);
//        }
//    };
    _ListAdapterCanJoinCompany.OnClickedListener enterClickListener = new _ListAdapterCanJoinCompany.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
            Intent intent = new Intent(getActivity(), _ActivityCompanyInfo.class);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, list.get(position).createCompanyRet.id);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, list.get(position).createCompanyRet.name);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORID, list.get(position).creator);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, list.get(position).createCompanyRet.desc);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_JOINED, false);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_USERCOUNT, list.get(position).userCount);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORNICK, list.get(position).creatorNick);
            intent.putExtra(MyConfig.CONST_STRING_PARAM_GROUPCOUNT, list.get(position).groupCount);
            startActivity(intent);

        }
    };

    _ListAdapterCanJoinCompany.OnClickedListener onJoinClickedListener = new _ListAdapterCanJoinCompany.OnClickedListener() {
        @Override
        public void OnClicked(final int position) {
            MyConfig.IOnOkClickListener iOnOkClickListener = new MyConfig.IOnOkClickListener() {
                @Override
                public void OnOkClickListener() {
                    JsonJoinCompany jsonJoinCompany = new JsonJoinCompany();
                    jsonJoinCompany.accessToken = MyConfig.usr_token;
                    jsonJoinCompany.id = list.get(position).createCompanyRet.id;
                    MyLog.d("", "HttpTools: joinCompany: ");
                    MyConfig.joinCompany(jsonJoinCompany);
                    JsonSetCurrentCompany jsonSetCurrentCompany = new JsonSetCurrentCompany(list.get(position).createCompanyRet.id);
                    MyConfig.setCurrentCompany(jsonSetCurrentCompany);
                }
            };
            MyConfig.showRemindDialog("", getString(R.string.prompt_if_join) + list.get(position).createCompanyRet.name + "?", getActivity(), true, iOnOkClickListener);
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Place an action bar item for searching.
        MenuItem item = menu.add("Search");
        //item.setIcon(android.R.drawable.ic_menu_search);
        item.setIcon(R.drawable.company_views_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM
                | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        mSearchView = new MyConfig.MySearchView(getActivity());
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        item.setActionView(mSearchView);
    }


    public boolean onQueryTextChange(String newText) {
        // Called when the action bar search text has changed.  Update
        // the search filter, and restart the loader to do a new query
        // with this filter.
        String newFilter = !TextUtils.isEmpty(newText) ? newText : null;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (mCurFilter == null && newFilter == null) {
            return true;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {
            return true;
        }
        mCurFilter = newFilter;
        adapter.searchKey =newFilter;
//        getLoaderManager().restartLoader(0, null, this);

        uiCompanyListUpdateFromLocal();

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // Don't care about this.
        return true;
    }

    @Override
    public boolean onClose() {
        if (!TextUtils.isEmpty(mSearchView.getQuery())) {
            mSearchView.setQuery(null, true);
        }
        return true;
    }

}
