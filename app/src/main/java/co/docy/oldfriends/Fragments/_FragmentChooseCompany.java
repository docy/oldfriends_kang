package co.docy.oldfriends.Fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.support.v7.widget.SearchView;

import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Adapters._ListAdapterCompanies;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventHaveGetCurrentCompany;
import co.docy.oldfriends.EventBus.EventCompanyListUpdateUi;
import co.docy.oldfriends.DataClass.Company;
import co.docy.oldfriends.Messages.JsonCompanyCanJoinRet;
import co.docy.oldfriends.Messages.JsonJoinCompany;
import co.docy.oldfriends.Messages.JsonSetCurrentCompany;
import co.docy.oldfriends.Messages.JsonUserCompaniesRet;
import co.docy.oldfriends.R;

public class _FragmentChooseCompany extends FragmentBase implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    _ListAdapterCompanies adapter;
    List<Company> list = new LinkedList<>();
    ;
    ListView lv_companies;

    // The SearchView for doing filtering.
    SearchView mSearchView;

    // If non-null, this is the current filter the user has provided.
    String mCurFilter;

    public static _FragmentChooseCompany newInstance() {
        _FragmentChooseCompany f = new _FragmentChooseCompany();

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_company_list, null);

        lv_companies = (ListView) v.findViewById(R.id.fragment_company_list);

        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        list = new LinkedList<>();
        uiCompanyListUpdateFromLocal();
        MyLog.d(MyConfig.TAG_LIFECYCLE, this.getClass().getName() + " savedInstanceState: newly created, compute data ");

        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putSerializable("list", (Serializable) list);

        MyLog.d(MyConfig.TAG_LIFECYCLE, this.getClass().getName() + " savedInstanceState: putSerializable " + list.size());
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        uiCompanyListUpdateFromLocal();

    }

    _ListAdapterCompanies.OnClickedListener onClickedListener = new _ListAdapterCompanies.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
            popMenu(position);
        }
    };

    _ListAdapterCompanies.OnLongClickedListener onLongClickedListener = new _ListAdapterCompanies.OnLongClickedListener() {
        @Override
        public void OnLongClicked(int position) {
            popMenu(position);
        }
    };


    private void popMenu(final int position) {

        new DialogFragment() {
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                // Use the Builder class for convenient dialog construction
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                String title;

                if (list.get(position).status == 0) {//have not joined
                    builder.setTitle(getString(R.string.prompt_if_join))
                            .setPositiveButton(R.string.prompt_confirm, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    JsonJoinCompany jsonJoinCompany = new JsonJoinCompany();
                                    jsonJoinCompany.accessToken = MyConfig.usr_token;
                                    jsonJoinCompany.id = list.get(position).createCompanyRet.id;
                                    MyConfig.joinCompany(jsonJoinCompany);
                                }
                            })
                            .setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                } else {
                    builder.setTitle(getString(R.string.prompt_you_have_joined))
                            .setItems(R.array.company_operation, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    // of the selected item
                                    switch (which) {
                                        case 0://set as current
                                            JsonSetCurrentCompany jsonSetCurrentCompany = new JsonSetCurrentCompany(list.get(position).createCompanyRet.id);
//                                            jsonSetCurrentCompany.accessToken = MyConfig.usr_token;
//                                            jsonSetCurrentCompany.id = list.get(position).createCompanyRet.id;
                                            MyConfig.setCurrentCompany(jsonSetCurrentCompany);
                                            break;
                                        case 1://leave

                                            //todo
                                            // 还没有接口
                                            //  还有个问题，能直接从当前公司退出吗？

                                            break;
                                    }

                                }
                            })
                            .setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                }
                // Create the AlertDialog object and return it
                return builder.create();
            }
        }.show(getFragmentManager(), "");
    }




    public void onEventMainThread(EventHaveGetCurrentCompany eventHaveGetCurrentCompany) {

        MyLog.d("choosecompany", "company change flow: FragmentChooseCompany get EventHaveGetCurrentCompany ");

        /**
         * 切换当前公司应该不需要刷新公司列表
         */

        if (eventHaveGetCurrentCompany.code == MyConfig.retSuccess()) {
            uiCompanyListUpdateFromLocal();
        } else {

        }
    }

    public void onEventMainThread(EventCompanyListUpdateUi eventCompanyListUpdateUi) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentCompanyList.java: EventCompanyListUpdateUi");

        uiCompanyListUpdateFromLocal();
    }


    private void uiCompanyListUpdateFromLocal() {
        list.clear();
        if (MyConfig.jsonUserCompaniesRet != null
                && MyConfig.jsonUserCompaniesRet.code==MyConfig.retSuccess()
                && MyConfig.jsonUserCompaniesRet.data != null) {
            for (JsonUserCompaniesRet.UserCompaniesRet urr : MyConfig.jsonUserCompaniesRet.data) {

                if (mCurFilter == null) {
                    list.add(Company.fromUserCompanyRet(urr));
                } else if (urr.name.contains(mCurFilter)) {
                    list.add(Company.fromUserCompanyRet(urr));
                }

            }
        }
        if (MyConfig.debug_show_not_joined_company) {//通常不显示未加入公司
            if (MyConfig.jsonCompanyCanJoinRet != null
                    && MyConfig.jsonCompanyCanJoinRet.code==MyConfig.retSuccess()
                    && MyConfig.jsonCompanyCanJoinRet.data != null) {
                for (JsonCompanyCanJoinRet.CompanyCanJoinRet rcjr : MyConfig.jsonCompanyCanJoinRet.data) {


                    if (mCurFilter == null) {
                        list.add(Company.fromCompanyCanJoin(rcjr));
                    } else if (rcjr.name.contains(mCurFilter)) {
                        list.add(Company.fromCompanyCanJoin(rcjr));
                    }

                }
            }
        }
        adapter = new _ListAdapterCompanies(getActivity(), list, onClickedListener, onLongClickedListener);
        lv_companies.setAdapter(adapter);

        MyLog.d("", "eventbus: onEventMainThread in uiCompanyListUpdateFromLocal(), list.size is ", list.size());

        MyLog.d("choosecompany", "company change flow: FragmentChooseCompany uiCompanyListUpdateFromLocal ");
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Place an action bar item for searching.
        MenuItem item = menu.add("Search");
        item.setIcon(android.R.drawable.ic_menu_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM
                | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        mSearchView = new MyConfig.MySearchView(getActivity());
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        item.setActionView(mSearchView);
    }


    public boolean onQueryTextChange(String newText) {
        // Called when the action bar search text has changed.  Update
        // the search filter, and restart the loader to do a new query
        // with this filter.
        String newFilter = !TextUtils.isEmpty(newText) ? newText : null;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (mCurFilter == null && newFilter == null) {
            return true;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {
            return true;
        }
        mCurFilter = newFilter;
//        getLoaderManager().restartLoader(0, null, this);

        uiCompanyListUpdateFromLocal();

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // Don't care about this.
        return true;
    }

    @Override
    public boolean onClose() {
        if (!TextUtils.isEmpty(mSearchView.getQuery())) {
            mSearchView.setQuery(null, true);
        }
        return true;
    }

}
