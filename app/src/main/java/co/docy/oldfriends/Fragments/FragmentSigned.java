package co.docy.oldfriends.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonCreateSigned;
import co.docy.oldfriends.Messages.JsonCreateSignedRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/5/23.
 */
public class FragmentSigned extends FragmentBase implements LocationSource,
        AMapLocationListener{
    private AMap aMap;
    private MapView mapView;

    private LocationSource.OnLocationChangedListener mListener;
    private AMapLocationClient mlocationClient;
    private AMapLocationClientOption mLocationOption;


    String address=null;
    double longitude_address;
    double latitude_address;
    String time;
    TextView current_time,current_loc;
    ImageView success_logo;
    Button button_signed;
    public static FragmentSigned newInstance() {
        FragmentSigned f = new FragmentSigned();

        return f;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_signed ,null);
        //检查网络是否连接
        if (MyConfig.netConnectCheck() <= 0) {
            MyConfig.MyToast(0, getActivity(), getString(R.string.net_connect_error));

        }
        current_time =(TextView) v.findViewById(R.id.fragment_signed_time);
        current_loc =(TextView) v.findViewById(R.id.fragment_signed_loc);
        button_signed =(Button) v.findViewById(R.id.fragment_signed_button);
        success_logo =(ImageView) v.findViewById(R.id.fragment_signed_success);
        success_logo.setVisibility(View.GONE);
        mapView =(MapView) v.findViewById(R.id.my_location_signed);
        aMap = mapView.getMap();
        aMap.setLocationSource(this);// 设置定位监听
        aMap.getUiSettings().setMyLocationButtonEnabled(true);// 设置默认定位按钮是否显示
        aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
        // 设置定位的类型为定位模式 ，可以由定位、跟随或地图根据面向方向旋转几种
        aMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);

        button_signed.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    v.setBackgroundResource(R.drawable.sign_in_selected_3x);
                    button_signed.setText("");
                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    v.setBackgroundResource(R.drawable.sign_logo_3x);
                    button_signed.setText("立即签到");
                    //上传签到数据

                    uploadToDatalibrary();
                }
                return true;
            }
        });
        return v;
    }
    public void uploadToDatalibrary(){
        //上传签到数据到后台
        if (address!=null && address.trim().length() > 0 ) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Gson gson = new Gson();
                    JsonCreateSigned jsonCreateSigned = new JsonCreateSigned();
                    jsonCreateSigned.accessToken = MyConfig.usr_token;
                    jsonCreateSigned.location = address;
                    jsonCreateSigned.latitude = latitude_address;
                    jsonCreateSigned.longitude = longitude_address;
                    String strJsonResetMessageNotice = gson.toJson(jsonCreateSigned,JsonCreateSigned.class);
                    final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CREATE_CHECKIN , strJsonResetMessageNotice);
                    MyLog.d("上传签到数据",s);
                    JsonCreateSignedRet jsonCreateSignedRet = gson.fromJson(s,JsonCreateSignedRet.class);
                    if(jsonCreateSignedRet!=null&&jsonCreateSignedRet.code==MyConfig.retSuccess()){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                success_logo.setVisibility(View.VISIBLE);
                                AlphaAnimation aa = new AlphaAnimation(1.0f,0.1f);
                                aa.setDuration(1500);
                                success_logo.startAnimation(aa);
                                aa.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        success_logo.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });

                            }
                        });

                    }
                }
            }).start();
        }else{
            Toast.makeText(getActivity(),"没有成功定位，请重新进入",Toast.LENGTH_SHORT).show();
        }
    }





    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
        deactivate();
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLocationChanged(AMapLocation amapLocation) {
        if (mListener != null && amapLocation != null) {
            if (amapLocation != null
                    && amapLocation.getErrorCode() == 0) {

                mListener.onLocationChanged(amapLocation);// 显示系统小蓝点
                latitude_address = amapLocation.getLatitude();
                longitude_address = amapLocation.getLongitude();
                address = amapLocation.getAddress();

                SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd HH:mm");
                Date date = new Date(amapLocation.getTime());
                time = df.format(date);//定位时间
                if(address.length()>0 && time.length()>0) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            current_time.setText(time);
                            current_loc.setText(address);
                        }
                    });
                }else{

                }


            } else {
                String errText = "定位失败," + amapLocation.getErrorCode()+ ": " + amapLocation.getErrorInfo();
                Log.e("AmapErr",errText);

            }
        }
    }

    @Override
    public void activate(OnLocationChangedListener listener) {
        mListener = listener;
        if (mlocationClient == null) {
            mlocationClient = new AMapLocationClient(getActivity());
            mLocationOption = new AMapLocationClientOption();
            //设置定位监听
            mlocationClient.setLocationListener(this);
            //设置为高精度定位模式
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
            mLocationOption.setInterval(120 * 1000);
            //设置定位参数
            mlocationClient.setLocationOption(mLocationOption);
            // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
            // 注意设置合适的定位时间的间隔（最小间隔支持为2000ms），并且在合适时间调用stopLocation()方法来取消定位请求
            // 在定位结束后，在合适的生命周期调用onDestroy()方法
            // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
            mlocationClient.startLocation();
        }
    }

        @Override
        public void deactivate () {
            mListener = null;
            if (mlocationClient != null) {
                mlocationClient.stopLocation();
                mlocationClient.onDestroy();
            }
            mlocationClient = null;
        }

}
