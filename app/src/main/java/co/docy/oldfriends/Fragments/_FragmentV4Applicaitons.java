package co.docy.oldfriends.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.joda.time.LocalDate;

import co.docy.oldfriends.Activitis.ActivityDaySchedule;
import co.docy.oldfriends.Activitis.ActivityScheduleNotifications;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.CalendarDay;
import co.docy.oldfriends.DataClass.CalendarMonth;
import co.docy.oldfriends.DataClass.CalendarWeek;
import co.docy.oldfriends.Messages.JsonGetCompanyUserListRet;
import co.docy.oldfriends.Messages.JsonNotificationReceive;
import co.docy.oldfriends.Messages.JsonNotificationReceiveRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.ViewCalendarTitle;
import co.docy.oldfriends.Views.ViewCalendarWeekWithActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class _FragmentV4Applicaitons extends FragmentBaseV4 {

    int mode_calendar_week_row = 6;//这个数字表示显示几行week

    CalendarMonth currentCalendarMonth;
    CalendarWeek currentCalendarWeek;

    ViewCalendarTitle viewCalendarTitle;
    LinearLayout fragment_page_app_calendar_weeks;
    ViewCalendarWeekWithActivity viewCalendarWeekWithActivity_0;
    ViewCalendarWeekWithActivity viewCalendarWeekWithActivity_1;
    ViewCalendarWeekWithActivity viewCalendarWeekWithActivity_2;
    ViewCalendarWeekWithActivity viewCalendarWeekWithActivity_3;
    ViewCalendarWeekWithActivity viewCalendarWeekWithActivity_4;
    ViewCalendarWeekWithActivity viewCalendarWeekWithActivity_5;

    ImageView fragment_page__notify_unread;
    RelativeLayout layout_notifications;
    TextView fragment_page_app_notify_unread;

    public static _FragmentV4Applicaitons newInstance() {
        _FragmentV4Applicaitons f = new _FragmentV4Applicaitons();

        return f;
    }

    ViewCalendarWeekWithActivity.DayOnClickListener dayOnClickListener =
            new ViewCalendarWeekWithActivity.DayOnClickListener() {
                @Override
                public void dayOnClicked(CalendarDay calendarDay) {
//                MyConfig.MyToast(0, getActivity(), "you clicked day "+day.toString());

                    MyConfig.tempLocalDate = calendarDay.day;
                    Intent intent = new Intent(getActivity(), ActivityDaySchedule.class);
                    startActivity(intent);

                }
            };


    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            MyLog.d("", "fragment_page_app_calendar_weeks LayoutOnClick ");
        }
    };

    public View.OnTouchListener onTouchListener = new View.OnTouchListener() {

        float last_y;

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {

            MyLog.d("", "fragment_page_app_calendar_weeks LayoutOnTouch ");

            float new_y = motionEvent.getY();

            if (last_y != 0) {

                float delta = new_y - last_y;

                if (delta > 0) {
                    //pull down
                    MyLog.d("", "fragment_page_app_calendar_weeks pull down ");

                } else if (delta < 0) {
                    //pull up
                    MyLog.d("", "fragment_page_app_calendar_weeks pull up ");

                }

            }
            last_y = new_y;

            return true;
        }

    };


    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_page_applications, null);

        viewCalendarTitle = (ViewCalendarTitle) v.findViewById(R.id.fragment_page_app_calendar_title);

        fragment_page_app_calendar_weeks = (LinearLayout) v.findViewById(R.id.fragment_page_app_calendar_weeks);
//        fragment_page_app_calendar_weeks.setOnTouchListener(onTouchListener);

        viewCalendarWeekWithActivity_0 = (ViewCalendarWeekWithActivity) v.findViewById(R.id.fragment_page_app_week_with_activity_0);
        viewCalendarWeekWithActivity_1 = (ViewCalendarWeekWithActivity) v.findViewById(R.id.fragment_page_app_week_with_activity_1);
        viewCalendarWeekWithActivity_2 = (ViewCalendarWeekWithActivity) v.findViewById(R.id.fragment_page_app_week_with_activity_2);
        viewCalendarWeekWithActivity_3 = (ViewCalendarWeekWithActivity) v.findViewById(R.id.fragment_page_app_week_with_activity_3);
        viewCalendarWeekWithActivity_4 = (ViewCalendarWeekWithActivity) v.findViewById(R.id.fragment_page_app_week_with_activity_4);
        viewCalendarWeekWithActivity_5 = (ViewCalendarWeekWithActivity) v.findViewById(R.id.fragment_page_app_week_with_activity_5);

        viewCalendarWeekWithActivity_0.dayOnClickListener = dayOnClickListener;
        viewCalendarWeekWithActivity_1.dayOnClickListener = dayOnClickListener;
        viewCalendarWeekWithActivity_2.dayOnClickListener = dayOnClickListener;
        viewCalendarWeekWithActivity_3.dayOnClickListener = dayOnClickListener;
        viewCalendarWeekWithActivity_4.dayOnClickListener = dayOnClickListener;
        viewCalendarWeekWithActivity_5.dayOnClickListener = dayOnClickListener;

        currentCalendarMonth = MyConfig.getMonthIncludeThisDay(new LocalDate());
        currentCalendarWeek = MyConfig.getWeekIncludeThisDay(new LocalDate());

        updateCalendarPage();

        viewCalendarTitle.gotoNext = new ViewCalendarTitle.GotoNext() {
            @Override
            public void gotoNext() {
                if(mode_calendar_week_row == 6) {
                    currentCalendarMonth = MyConfig.getMonthIncludeThisDay(currentCalendarMonth.firstDayOfCurrentMonth.plusDays(32));
                }else{
                    currentCalendarWeek = MyConfig.getWeekIncludeThisDay(currentCalendarWeek.calendarDayList.getLast().day.plusDays(1));
                }
                updateCalendarPage();
            }
        };
        viewCalendarTitle.gotoPrevious = new ViewCalendarTitle.GotoPrevious() {
            @Override
            public void gotoPrevious() {
                if(mode_calendar_week_row == 6) {
                    currentCalendarMonth = MyConfig.getMonthIncludeThisDay(currentCalendarMonth.firstDayOfCurrentMonth.plusDays(-1));
                }else{
                    currentCalendarWeek = MyConfig.getWeekIncludeThisDay(currentCalendarWeek.calendarDayList.getFirst().day.plusDays(-1));
                }
                updateCalendarPage();
            }
        };
        viewCalendarTitle.gotoToday = new ViewCalendarTitle.GotoToday() {
            @Override
            public void gotoToday() {
                if(mode_calendar_week_row == 6) {
                    currentCalendarMonth = MyConfig.getMonthIncludeThisDay(new LocalDate());
                }else{
                    currentCalendarWeek = MyConfig.getWeekIncludeThisDay(new LocalDate());
                }
                updateCalendarPage();
            }
        };

        fragment_page__notify_unread =(ImageView) v.findViewById(R.id.fragment_page_app_notify_unread);
       // fragment_page_app_notify_unread = (TextView) v.findViewById(R.id.fragment_page_app_notify_unread);
        layout_notifications = (RelativeLayout) v.findViewById(R.id.fragment_page_app_notify);
        layout_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), ActivityScheduleNotifications.class);
                startActivity(intent);
                MyConfig.unRead_notification = 1;
            }
        });
        if(Notify_unread_num()>0){
            fragment_page__notify_unread.setVisibility(View.VISIBLE);
        }else{
            fragment_page__notify_unread.setVisibility(View.GONE);
        }
        if(MyConfig.unRead_notification == 1){
            fragment_page__notify_unread.setVisibility(View.GONE);
        }

        return v;
    }
    int Num = 0;
    public int Notify_unread_num(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonNotificationReceive jsonNotificationReceive = new JsonNotificationReceive();
                jsonNotificationReceive.unread = true;
                jsonNotificationReceive.accessToken = MyConfig.usr_token;

                //String strJsonResetMessageNotice = gson.toJson(jsonNotificationReceive,JsonNotificationReceive.class);
                final String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_RECEIVE_NOTIFICATION +"?"+true ,MyConfig.usr_token);
                MyLog.d("判断是否有新消息",s);

                final JsonNotificationReceiveRet jsonNotificationReceiveRet = gson.fromJson(s, JsonNotificationReceiveRet.class);


                if(jsonNotificationReceiveRet!=null&&jsonNotificationReceiveRet.code == MyConfig.retSuccess()){

                    Num =jsonNotificationReceiveRet.data.size();

                }

            }
        });
        return Num;
    }
    private void updateCalendarPage() {

        if (mode_calendar_week_row == 6) {

            viewCalendarWeekWithActivity_0.setVisibility(View.VISIBLE);
            viewCalendarWeekWithActivity_1.setVisibility(View.VISIBLE);
            viewCalendarWeekWithActivity_2.setVisibility(View.VISIBLE);
            viewCalendarWeekWithActivity_3.setVisibility(View.VISIBLE);
            viewCalendarWeekWithActivity_4.setVisibility(View.VISIBLE);
            viewCalendarWeekWithActivity_5.setVisibility(View.VISIBLE);

            viewCalendarWeekWithActivity_0.setViewCalendarWeek(currentCalendarMonth.calendarWeeks.get(0), currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            viewCalendarWeekWithActivity_1.setViewCalendarWeek(currentCalendarMonth.calendarWeeks.get(1), currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            viewCalendarWeekWithActivity_2.setViewCalendarWeek(currentCalendarMonth.calendarWeeks.get(2), currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            viewCalendarWeekWithActivity_3.setViewCalendarWeek(currentCalendarMonth.calendarWeeks.get(3), currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());

            if (currentCalendarMonth.calendarWeeks.size() > 4) {
                viewCalendarWeekWithActivity_4.setViewCalendarWeek(currentCalendarMonth.calendarWeeks.get(4), currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            } else {
                viewCalendarWeekWithActivity_4.setViewCalendarWeek(null, currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
                viewCalendarWeekWithActivity_5.setViewCalendarWeek(null, currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            }
            if (currentCalendarMonth.calendarWeeks.size() > 5) {
                viewCalendarWeekWithActivity_5.setViewCalendarWeek(currentCalendarMonth.calendarWeeks.get(5), currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            } else {
                viewCalendarWeekWithActivity_5.setViewCalendarWeek(null, currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
            }

            viewCalendarTitle.setCalendarIndicator(""+ currentCalendarMonth.firstDayOfCurrentMonth.getYear() +
                    "-"+currentCalendarMonth.firstDayOfCurrentMonth.getMonthOfYear());
        } else if (mode_calendar_week_row == 1) {

            viewCalendarWeekWithActivity_0.setVisibility(View.VISIBLE);
            viewCalendarWeekWithActivity_1.setVisibility(View.GONE);
            viewCalendarWeekWithActivity_2.setVisibility(View.GONE);
            viewCalendarWeekWithActivity_3.setVisibility(View.GONE);
            viewCalendarWeekWithActivity_4.setVisibility(View.GONE);
            viewCalendarWeekWithActivity_5.setVisibility(View.GONE);

            viewCalendarWeekWithActivity_0.setViewCalendarWeek(currentCalendarWeek, currentCalendarWeek.firstDayOfCurrentMonth.getMonthOfYear());

            viewCalendarTitle.setCalendarIndicator(""+ currentCalendarWeek.firstDayOfCurrentMonth.getYear() +
                    "-"+currentCalendarWeek.firstDayOfCurrentMonth.getMonthOfYear());
        }
    }


    private void updataCalendarCourseFlag() {

        Call<JsonGetCompanyUserListRet> repos = HttpTools.http_api_service().getCourseListByDate("2016-04-04");
        repos.enqueue(new Callback<JsonGetCompanyUserListRet>() {
            @Override
            public void onResponse(Call<JsonGetCompanyUserListRet> call, Response<JsonGetCompanyUserListRet> response) {
                // Get result Repo from response.body()

                MyLog.d("", "retrofit: code() " + response.code());
                MyLog.d("", "retrofit: message() " + response.message());

                if (response.code() == 200) {

                    MyConfig.jsonGetCompanyUserListRet = response.body();

                    MyLog.d("", "retrofit: body() 1 " + response.body().toString());
                    MyLog.d("", "retrofit: body() 2 " + response.body().code);
                    MyLog.d("", "retrofit: body() 3 " + response.body().message);

                    if (response.body().data != null) {
//                        iGetUserList.doAfterHttpRequest();
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonGetCompanyUserListRet> call, Throwable t) {
                MyLog.d("", "retrofit: onFailure " + t.getMessage());
            }
        });


    }


}
