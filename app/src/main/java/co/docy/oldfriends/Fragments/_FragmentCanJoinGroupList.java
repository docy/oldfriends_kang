package co.docy.oldfriends.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Activitis.ActivityGroupInfo;
import co.docy.oldfriends.Activitis.ActivityMainGroup;
import co.docy.oldfriends.Adapters.ListAdapterSearchGroups;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.Group;
import co.docy.oldfriends.EventBus.EventGroupListUpdateFromServer;
import co.docy.oldfriends.EventBus.EventGroupListUpdateUi;
import co.docy.oldfriends.EventBus.EventJionedGroup;
import co.docy.oldfriends.Messages.JsonGroupCanJoinRet;
import co.docy.oldfriends.Messages.JsonJoinGroup;
import co.docy.oldfriends.Messages.JsonJoinGroupRet;
import co.docy.oldfriends.Messages.JsonLeaveGroup;
import co.docy.oldfriends.Messages.JsonLeaveGroupRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;

public class _FragmentCanJoinGroupList extends FragmentBase implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    //  ListAdapterGroups adapter;
    ListAdapterSearchGroups adapter;
    LinkedList<Group> list = new LinkedList<>();
    ListView lv_groups;
    TextView fragment_canjoingroup_group_result;

    LinearLayout search_group_layout, list_null_background_layout;
    private MyConfig.IOnOkClickListener iOnOkClickListener;

    public ListAdapterSearchGroups.OnClickedListener onClickedListener;
    public ListAdapterSearchGroups.OnImageClickedListener onImageClickedListener;
    SearchView mSearchView;

    String mCurFilter;
    String name_join, desc_join;

    public static _FragmentCanJoinGroupList newInstance() {
        _FragmentCanJoinGroupList f = new _FragmentCanJoinGroupList();

//        Bundle args = new Bundle();
//        args.putString(TAG_ARGS, s);
//        f.setArguments(args);

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_canjoingroup_list, null);

        search_group_layout = (LinearLayout) v.findViewById(R.id.search_group_layout);
        list_null_background_layout = (LinearLayout) v.findViewById(R.id.list_null_background_layout);
//        fragment_canjoingroup_create_group = (LinearLayout) v.findViewById(R.id.fragment_canjoingroup_create_group);
//        fragment_canjoingroup_group_result = (TextView) v.findViewById(R.id.fragment_canjoingroup_group_result);
        lv_groups = (ListView) v.findViewById(R.id.fragment_canjoingroup_list);

//        fragment_canjoingroup_create_group.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent create_room = new Intent(getActivity(), ActivityCreateGroup.class);
//                startActivityForResult(create_room, MyConfig.REQUEST_CREATE_ROOM);
//            }
//        });
        fragment_canjoingroup_group_result = (TextView) v.findViewById(R.id.fragment_canjoingroup_group_result);
        onClickedListener = new ListAdapterSearchGroups.OnClickedListener() {
            @Override
            public void OnClicked(int position) {
                showDialog(position);
            }
        };

        onImageClickedListener = new ListAdapterSearchGroups.OnImageClickedListener() {
            @Override
            public void OnImageClicked(int position) {
                Intent intent = new Intent(getActivity(), ActivityGroupInfo.class);
                intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, list.get(position).createGroupRet.id);
                intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, list.get(position).createGroupRet.name);
                intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, list.get(position).createGroupRet.desc);
                intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_JOINED, false);
                intent.putExtra("isShowInvite", false);
                startActivity(intent);
            }
        };

        //    adapter = new ListAdapterCanJoinGroups(getActivity(), list, onClickedListener, onLongClickedListener, onImageClickedListener);
        adapter = new ListAdapterSearchGroups(getActivity(), list, MyConfig.GROUP_LIST_TYPE_SEARCH_GROUP, onImageClickedListener, onClickedListener);
        lv_groups.setAdapter(adapter);

        String format = getResources().getString(R.string.search_company_result);
        String result = String.format(format, list.size());
        fragment_canjoingroup_group_result.setText(result);
        return v;
    }

    private void showDialog(final int position) {
        if (list.get(position).status == 0) {
            iOnOkClickListener = new MyConfig.IOnOkClickListener() {
                @Override
                public void OnOkClickListener() {
                    JsonJoinGroup jsonJoinGroup = new JsonJoinGroup();
                    jsonJoinGroup.accessToken = MyConfig.usr_token;
                    jsonJoinGroup.groupId = list.get(position).createGroupRet.id;
                    name_join = list.get(position).createGroupRet.name;
                    desc_join = list.get(position).createGroupRet.desc;
                    joinGroup(jsonJoinGroup);
                }
            };
            MyConfig.showRemindDialog("", getString(R.string.prompt_if_join) + list.get(position).createGroupRet.name + "?", getActivity(), true, iOnOkClickListener);
        } else {
            iOnOkClickListener = new MyConfig.IOnOkClickListener() {
                @Override
                public void OnOkClickListener() {
                    JsonLeaveGroup jsonLeaveGroup = new JsonLeaveGroup();
                    jsonLeaveGroup.accessToken = MyConfig.usr_token;
                    jsonLeaveGroup.groupId = list.get(position).createGroupRet.id;
                    leaveGroup(jsonLeaveGroup);
                }
            };
            MyConfig.showRemindDialog("", getString(R.string.prompt_if_quit), getActivity(), true, iOnOkClickListener);
        }
    }

//    ListAdapterCanJoinGroups.OnLongClickedListener onLongClickedListener = new ListAdapterCanJoinGroups.OnLongClickedListener() {
//        @Override
//        public void OnLongClicked(int position) {
//
//        }
//    };


    public void joinGroup(final JsonJoinGroup jsonJoinGroup) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonJoinGroup, JsonJoinGroup.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_JOIN_GROUP + jsonJoinGroup.groupId + MyConfig.API2_JOIN_GROUP_2, jsonStr);

                final JsonJoinGroupRet jsonJoinGroupRet = gson.fromJson(s, JsonJoinGroupRet.class);
                MyLog.d("", "HttpTools: JsonJoinRet " + s);

                if (jsonJoinGroupRet != null && jsonJoinGroupRet.code==MyConfig.retSuccess()) {
                    ActivityGroupInfo.joined = true;
                    Intent groupintent = new Intent(getActivity(), ActivityMainGroup.class);
                    groupintent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, jsonJoinGroup.groupId);
                    groupintent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, name_join);
                    groupintent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, desc_join);
                    startActivity(groupintent);

                    EventBus.getDefault().post(new EventGroupListUpdateFromServer());
                    EventBus.getDefault().post(new EventJionedGroup(true));

                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MyConfig.MyToast(0, getActivity(),
                                    jsonJoinGroupRet.message);
                        }
                    });
                }
            }
        }).start();

    }

    public void leaveGroup(final JsonLeaveGroup jsonLeaveGroup) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonLeaveGroup, JsonLeaveGroup.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LEAVE_GROUP + jsonLeaveGroup.groupId + MyConfig.API2_LEAVE_GROUP_2, jsonStr);

                final JsonLeaveGroupRet jsonLeaveGroupRet = gson.fromJson(s, JsonLeaveGroupRet.class);
                MyLog.d("", "HttpTools: JsonLeaveRet " + s);
                if (jsonLeaveGroupRet != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            MyConfig.MyToast(0, getActivity(),
                                    jsonLeaveGroupRet.message);
                        }
                    });
                }


                EventBus.getDefault().post(new EventGroupListUpdateFromServer());
            }
        }).start();

    }

    public List<Group> getListData() {
        return null;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        if (savedInstanceState != null) {
//            //probably orientation change
//            list = (LinkedList<Group>) savedInstanceState.getSerializable("list");
//            if (list == null) {//这里有时是null，不知为何
//                list = new LinkedList<>();
//            }
//            MyLog.d(MyConfig.TAG_LIFECYCLE, this.getClass().getName() + " savedInstanceState: probably orientation change");
//        } else {
//            if (list != null) {
//                //returning from backstack, data is fine, do nothing
//                MyLog.d(MyConfig.TAG_LIFECYCLE, this.getClass().getName() + " savedInstanceState: returning from backstack, data is fine, do nothing " + list.size());
//            } else {
//                //newly created, compute data
//                list = new LinkedList<>();
//                uiRoomListUpdateFromLocal();
//                MyLog.d(MyConfig.TAG_LIFECYCLE, this.getClass().getName() + " savedInstanceState: newly created, compute data ");
//            }
//        }

//        adapter = new ListAdapterGroups(getActivity(), list, onClickedListener, onLongClickedListener);
//        lv_groups.setAdapter(adapter);

        setHasOptionsMenu(true);

    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putSerializable("list", (Serializable) list);

        MyLog.d(MyConfig.TAG_LIFECYCLE, this.getClass().getName() + " savedInstanceState: putSerializable " + list.size());
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        uiRoomListUpdateFromLocal();

    }

    public void onEventMainThread(EventGroupListUpdateUi eventGroupListUpdateUi) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java");

        uiRoomListUpdateFromLocal();
    }


    private void uiRoomListUpdateFromLocal() {

        /**
         * 更新UI界面中的group list
         */

        list.clear();
//        if (MyConfig.jsonUserGroupsRet != null
//                && MyConfig.jsonUserGroupsRet.code==MyConfig.retSuccess()
//                && MyConfig.jsonUserGroupsRet.data != null) {
//            for (JsonUserGroupsRet.UserGroupsRet urr : MyConfig.jsonUserGroupsRet.data) {
//                list.add(Group.fromUserGroupsRet(urr));
//            }
//        }

        if (MyConfig.jsonGroupCanJoinRet != null
                && MyConfig.jsonGroupCanJoinRet.code==MyConfig.retSuccess()
                && MyConfig.jsonGroupCanJoinRet.data != null) {
            for (JsonGroupCanJoinRet.GroupCanJoinRet rcjr : MyConfig.jsonGroupCanJoinRet.data) {
                if (mCurFilter == null) {
                    list.add(Group.fromGroupCanJoin(rcjr));
                } else if (rcjr.name.contains(mCurFilter)) {
                    list.add(Group.fromGroupCanJoin(rcjr));
                }
            }
        }
        adapter.notifyDataSetChanged();
        String format = getResources().getString(R.string.search_company_result);
        String result = String.format(format, list.size());
        fragment_canjoingroup_group_result.setText(result);
        if (list.size() < 1) {
            list_null_background_layout.setVisibility(View.VISIBLE);
        } else {
            list_null_background_layout.setVisibility(View.GONE);
        }
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java 2");

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Place an action bar item for searching.
        MenuItem item = menu.add("Search");
        item.setIcon(R.drawable.company_views_search);
        //   item.setIcon(getActivity().getResources().getDrawable(R.drawable.company_views_search))
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM
                | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        mSearchView = new MyConfig.MySearchView(getActivity());
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);

        item.setActionView(mSearchView);
    }


    public boolean onQueryTextChange(String newText) {
        // Called when the action bar search text has changed.  Update
        // the search filter, and restart the loader to do a new query
        // with this filter.
        String newFilter = !TextUtils.isEmpty(newText) ? newText : null;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (mCurFilter == null && newFilter == null) {

            return true;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {

            return true;
        }
        adapter.searchKey = newFilter;
        mCurFilter = newFilter;
//        getLoaderManager().restartLoader(0, null, this);

        uiRoomListUpdateFromLocal();

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // Don't care about this.
        return true;
    }

    @Override
    public boolean onClose() {
        if (!TextUtils.isEmpty(mSearchView.getQuery())) {
            mSearchView.setQuery(null, true);
        }
        return true;
    }

}
