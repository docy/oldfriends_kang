package co.docy.oldfriends.Fragments;

import android.app.Fragment;
import android.os.Bundle;

import co.docy.oldfriends.EventBus.EventHeartOneSecond;
import de.greenrobot.event.EventBus;

public class FragmentBase extends Fragment {

    boolean eventbut_registed = false;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /**
         * eventbus消息的处理可能引用view控件，所以eventbus的注册要在view控件初始化之后，所以挪动到这里
         * view控件的初始化在onCreateView中
         */
        if(!eventbut_registed) {
            EventBus.getDefault().register(this);
            eventbut_registed = true;
        }

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    public void onEventMainThread(EventHeartOneSecond eventHeartOneSecond) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        /**
         * 要在view控件被解构之前断开eventbus
         */
        if(eventbut_registed) {
            EventBus.getDefault().unregister(this);
            eventbut_registed = false;
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
