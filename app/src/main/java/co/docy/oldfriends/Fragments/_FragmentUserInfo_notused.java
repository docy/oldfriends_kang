package co.docy.oldfriends.Fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import co.docy.oldfriends.Activitis.ActivityDirectChatGroup;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventCompanyListUpdateUi;
import co.docy.oldfriends.Messages.JsonCreateDirectChatGroup;
import co.docy.oldfriends.Messages.JsonCreateDirectChatRet;
import co.docy.oldfriends.Messages.JsonGetGroupUserRet;
import co.docy.oldfriends.Messages.JsonGetUserInfoRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;

public class _FragmentUserInfo_notused extends FragmentBase {

    int user_id;
    String user_name;
    String user_nickName;
    Boolean user_changed;

    JsonGetGroupUserRet.GetGroupUserRet getGroupUserRet;
    JsonGetUserInfoRet.GetUserInfoRet getUserInfoRet;
    RadioButton userinfo_person_info_sex_man;
    RadioButton userinfo_person_info_sex_woman;
    Button userinfo_direct_chat;


    public static _FragmentUserInfo_notused newInstance(Bundle bundle) {
        _FragmentUserInfo_notused f = new _FragmentUserInfo_notused();
        f.setArguments(bundle);
        return f;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_info, null);

        Bundle bundle = getArguments();
        getUserInfoRet = bundle.getParcelable(MyConfig.CONST_PARAM_STR_CONTACT_UNIVERAL);
        getGroupUserRet = bundle.getParcelable(MyConfig.CONSTANT_PARAMETER_STRING_GETGROUPINFORET);

        if (getUserInfoRet != null) {

            user_id = getUserInfoRet.id;
            user_name = getUserInfoRet.name;
            user_nickName = getUserInfoRet.nickName;


            MyLog.d("", "FragmentUserInfo get: " + getUserInfoRet.id
                    + " " + getUserInfoRet.sex);


            ImageView userinfo_avatar = (ImageView) v.findViewById(R.id.userinfo_avatar);
            Picasso.with(getActivity()).load("" + MyConfig.getApiDomain_NoSlash_GetResources() + getUserInfoRet.avatar)
                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(userinfo_avatar);
            userinfo_avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    View dialogView = LayoutInflater.from(getActivity()).inflate(
                            R.layout.dialog_user_picture, null);
                    final Dialog setDialog = new Dialog(getActivity(), R.style.DialogStyle);
                    setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    setDialog.setCancelable(true);
                    setDialog.getWindow().setContentView(dialogView);
                    WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
                    lp.width = MyConfig.getWidthByScreenPercent(100);
                    setDialog.getWindow().setAttributes(lp);
                    ImageView imageView = (ImageView) dialogView.findViewById(R.id.user_picture);
                    Picasso.with(getActivity()).load("" + MyConfig.getApiDomain_NoSlash_GetResources() + getUserInfoRet.avatarOrigin)
                            .resize(lp.width, lp.width)
                            .into(imageView);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setDialog.cancel();
                        }
                    });
                    setDialog.setCanceledOnTouchOutside(true);
                    setDialog.show();


                }
            });

            TextView userinfo_name_text = (TextView) v.findViewById(R.id.userinfo_name_text);
            userinfo_name_text.setText(getUserInfoRet.name);
            TextView userinfo_nickname_text = (TextView) v.findViewById(R.id.userinfo_nickname_text);
            userinfo_nickname_text.setText(getUserInfoRet.nickName);

            TextView userinfo_phone_text = (TextView) v.findViewById(R.id.userinfo_phone_text);
            if (getUserInfoRet.phone != null && getUserInfoRet.phone.length() >3){
                String maskNumber = getUserInfoRet.phone.substring(0,3)+"******"+getUserInfoRet.phone.substring(getUserInfoRet.phone.length()-1,getUserInfoRet.phone.length());
                userinfo_phone_text.setText(maskNumber);
            }
            userinfo_person_info_sex_man = (RadioButton) v.findViewById(R.id.userinfo_person_info_sex_man);
            userinfo_person_info_sex_woman = (RadioButton) v.findViewById(R.id.userinfo_person_info_sex_woman);

            if (getUserInfoRet.sex) {//true = 男
                userinfo_person_info_sex_man.setChecked(true);
                userinfo_person_info_sex_man.setVisibility(View.VISIBLE);
                userinfo_person_info_sex_woman.setVisibility(View.GONE);
            } else {
                userinfo_person_info_sex_woman.setChecked(true);
                userinfo_person_info_sex_woman.setVisibility(View.VISIBLE);
                userinfo_person_info_sex_man.setVisibility(View.GONE);
            }

        } else if (getGroupUserRet != null) {

            user_id = getGroupUserRet.id;
            user_name = getGroupUserRet.name;
            user_nickName = getGroupUserRet.nickName;


            MyLog.d("", "FragmentUserInfo get: " + getGroupUserRet.id
                    + " " + getGroupUserRet.sex);


            ImageView userinfo_avatar = (ImageView) v.findViewById(R.id.userinfo_avatar);
            Picasso.with(getActivity()).load("" + MyConfig.getApiDomain_NoSlash_GetResources() + getGroupUserRet.avatar)
                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(userinfo_avatar);

            TextView userinfo_name_text = (TextView) v.findViewById(R.id.userinfo_name_text);
            userinfo_name_text.setText(getGroupUserRet.name);
            TextView userinfo_nickname_text = (TextView) v.findViewById(R.id.userinfo_nickname_text);
            userinfo_nickname_text.setText(getGroupUserRet.nickName);

            TextView userinfo_phone_text = (TextView) v.findViewById(R.id.userinfo_phone_text);
            String maskNumber = getGroupUserRet.phone.substring(0,3)+"****"+getGroupUserRet.phone.substring(getGroupUserRet.phone.length()-3,getGroupUserRet.phone.length());
            userinfo_phone_text.setText(maskNumber);

            userinfo_person_info_sex_man = (RadioButton) v.findViewById(R.id.userinfo_person_info_sex_man);
            userinfo_person_info_sex_woman = (RadioButton) v.findViewById(R.id.userinfo_person_info_sex_woman);

            if (getGroupUserRet.sex) {//true = 男
                userinfo_person_info_sex_man.setChecked(true);
                userinfo_person_info_sex_man.setVisibility(View.VISIBLE);
                userinfo_person_info_sex_woman.setVisibility(View.GONE);
            } else {
                userinfo_person_info_sex_woman.setChecked(true);
                userinfo_person_info_sex_woman.setVisibility(View.VISIBLE);
                userinfo_person_info_sex_man.setVisibility(View.GONE);
            }
        }

        userinfo_direct_chat = (Button) v.findViewById(R.id.userinfo_direct_chat);
        if (user_id == MyConfig.usr_id) {
            userinfo_direct_chat.setVisibility(View.GONE);
//            userinfo_direct_chat.setText(getString(R.string.sure));
//            userinfo_direct_chat.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    getActivity().finish();
//                }
//            });

        } else {

            userinfo_direct_chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    createDirectChatRoom();

                }
            });
        }

        return v;
    }

    private void createDirectChatRoom() {

        new AsyncTask<Integer, Void, String>() {

            @Override
            protected void onPreExecute() {
            }

            @Override
            protected String doInBackground(Integer... integers) {

                JsonCreateDirectChatGroup jsonCreateDirectChatGroup = new JsonCreateDirectChatGroup();
                jsonCreateDirectChatGroup.accessToken = MyConfig.usr_token;
                jsonCreateDirectChatGroup.targetId = integers[0];
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonCreateDirectChatGroup, JsonCreateDirectChatGroup.class);

                return new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CREATE_DIRECT_CHAT_ROOM, jsonStr);

            }

            @Override
            protected void onPostExecute(String s) {
                Gson gson = new Gson();
                JsonCreateDirectChatRet jsonCreateDirectChatRet = gson.fromJson(s, JsonCreateDirectChatRet.class);

                if (jsonCreateDirectChatRet != null && jsonCreateDirectChatRet.code==MyConfig.retSuccess()) {

//                    Toast.makeText(
//                            getActivity(),
//                            jsonCreateDirectChatRet.message,
//                            Toast.LENGTH_LONG).show();

                    MyConfig.MyToast(-1, getActivity(),
                            jsonCreateDirectChatRet.message);

                    Intent intent_main = new Intent(getActivity(), ActivityDirectChatGroup.class);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, jsonCreateDirectChatRet.data.id);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, jsonCreateDirectChatRet.data.name);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, jsonCreateDirectChatRet.data.desc);
                    startActivity(intent_main);

                    getActivity().finish();
                }

            }

        }.execute(user_id);


    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    public void onEventMainThread(EventCompanyListUpdateUi eventCompanyListUpdateUi) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentCompanyList.java: EventCompanyListUpdateUi");

    }


}
