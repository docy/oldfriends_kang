package co.docy.oldfriends.Fragments;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.LinkedList;
import java.util.ListIterator;

import co.docy.oldfriends.Activitis._ActivityCanJoinCompanyList;
import co.docy.oldfriends.Activitis._ActivityCompanyInfo;
import co.docy.oldfriends.Activitis._ActivityCompanyViewSetting;
import co.docy.oldfriends.Activitis._ActivityCreateCompany;
import co.docy.oldfriends.Activitis._ActivityInputCompanyInviteCode;
import co.docy.oldfriends.Adapters._ListAdapterCompanyView;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.Company;
import co.docy.oldfriends.EventBus.EventCompanyListUpdateUi;
import co.docy.oldfriends.EventBus.EventHaveGetCurrentCompany;
import co.docy.oldfriends.EventBus.EventHaveSetCurrentCompany;
import co.docy.oldfriends.EventBus.EventLogout;
import co.docy.oldfriends.Messages.JsonCompanyCanJoinRet;
import co.docy.oldfriends.Messages.JsonJoinCompany;
import co.docy.oldfriends.Messages.JsonSetCurrentCompany;
import co.docy.oldfriends.Messages.JsonUserCompaniesRet;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;
import info.hoang8f.android.segmented.SegmentedGroup;

public class _FragmentCompanyView extends FragmentBase implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    _ListAdapterCompanyView adapter, sortadapter;

    LinkedList<Company> list = new LinkedList<>();
    LinkedList<Company> sortlist = new LinkedList<>();

    ListView lv_companies;
    ListView company_views_sortlist;
    public static float view_y_origin;

    MenuItem item;

    RadioButton mineRadioButton, sortRadioButton;
    SegmentedGroup room_list_segment;

    SearchView mSearchView;
    PopupWindow popupWindow;

    String mCurFilter;

    public static final int TYPE_COMPANY_NORMAL_LIST = 0;

    public static final int TYPE_COMPANY_SORT_LIST = 1;

    public static _FragmentCompanyView newInstance() {
        _FragmentCompanyView f = new _FragmentCompanyView();

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_company_view, null);

        lv_companies = (ListView) v.findViewById(R.id.company_views_mine);
        company_views_sortlist = (ListView) v.findViewById(R.id.company_views_sort);

        mineRadioButton = (RadioButton) v.findViewById(R.id.company_view_mine);
        sortRadioButton = (RadioButton) v.findViewById(R.id.company_view_sort);

        adapter = new _ListAdapterCompanyView(getActivity(), list, TYPE_COMPANY_NORMAL_LIST);
        sortadapter = new _ListAdapterCompanyView(getActivity(), sortlist, TYPE_COMPANY_SORT_LIST);

        adapter.enterClickListener = onEnterClickedListener;
        lv_companies.setAdapter(adapter);

        sortadapter.jionClickListener = onJoinClickedListener;
        sortadapter.enterClickListener = onInfoClickedListener;
        company_views_sortlist.setAdapter(sortadapter);


        initPopUp();
        room_list_segment = (SegmentedGroup) v.findViewById(R.id.company_views__list_segment);
        room_list_segment.setTintColor(getResources().getColor(R.color.xjt_toolbar_blue));
        room_list_segment.getLayoutParams().width = MyConfig.screenWidth * 2 / 3;
        view_y_origin = room_list_segment.getY();
        room_list_segment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.company_view_mine) {
                    lv_companies.setVisibility(View.VISIBLE);
                    company_views_sortlist.setVisibility(View.GONE);
                } else {
                    lv_companies.setVisibility(View.GONE);
                    company_views_sortlist.setVisibility(View.VISIBLE);
                }

            }
        });
        room_list_segment.getLayoutParams().width = MyConfig.screenWidth * 2 / 3;
        mineRadioButton.setChecked(true);
        company_views_sortlist.setVisibility(View.GONE);
        company_views_sortlist.setOnScrollListener(onScrollListener);
        lv_companies.setOnScrollListener(onScrollListener);

        return v;
    }

    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        private int mLastFirstVisibleItem;
        private boolean hide = false;


        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (mLastFirstVisibleItem < firstVisibleItem) {
                //up
//                room_list_segment.setVisibility(View.INVISIBLE);
                if (!hide) {
                    ObjectAnimator objectAnimator =
                            ObjectAnimator.ofFloat(room_list_segment,
                                    "y", view_y_origin, view_y_origin - room_list_segment.getHeight()).setDuration(300);
                    objectAnimator.setInterpolator(new LinearInterpolator());
                    objectAnimator.setRepeatCount(0);
                    objectAnimator.start();
                    hide = true;
                }
            }
            if (mLastFirstVisibleItem > firstVisibleItem) {
                //down
//                room_list_segment.setVisibility(View.VISIBLE);
                if (hide) {
                    ObjectAnimator objectAnimator =
                            ObjectAnimator.ofFloat(room_list_segment,
                                    "y", view_y_origin - room_list_segment.getHeight(), view_y_origin).setDuration(300);
                    objectAnimator.setInterpolator(new LinearInterpolator());
                    objectAnimator.setRepeatCount(0);
                    objectAnimator.start();
                    hide = false;
                }
            }
            mLastFirstVisibleItem = firstVisibleItem;
        }
    };


    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        MyLog.d(MyConfig.TAG_LIFECYCLE, this.getClass().getName() + " savedInstanceState: putSerializable " + list.size());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        MyConfig.companyListUpdateFromServer();
    }


    _ListAdapterCompanyView.OnClickedListener onEnterClickedListener = new _ListAdapterCompanyView.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
            JsonSetCurrentCompany jsonSetCurrentCompany = new JsonSetCurrentCompany(list.get(position).createCompanyRet.id);
            MyConfig.setCurrentCompany(jsonSetCurrentCompany);

        }
    };
    _ListAdapterCompanyView.OnClickedListener onInfoClickedListener = new _ListAdapterCompanyView.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
            Intent intent = new Intent(getActivity(), _ActivityCompanyInfo.class);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, sortlist.get(position).createCompanyRet.id);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, sortlist.get(position).createCompanyRet.name);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORID, sortlist.get(position).creator);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, sortlist.get(position).createCompanyRet.desc);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_JOINED, false);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_USERCOUNT, sortlist.get(position).userCount);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORNICK, sortlist.get(position).creatorNick);
            intent.putExtra(MyConfig.CONST_STRING_PARAM_GROUPCOUNT, sortlist.get(position).groupCount);
            startActivity(intent);

        }
    };

    _ListAdapterCompanyView.OnClickedListener onJoinClickedListener = new _ListAdapterCompanyView.OnClickedListener() {
        @Override
        public void OnClicked(final int position) {
            MyConfig.IOnOkClickListener iOnOkClickListener = new MyConfig.IOnOkClickListener() {
                @Override
                public void OnOkClickListener() {
                    JsonJoinCompany jsonJoinCompany = new JsonJoinCompany();
                    jsonJoinCompany.accessToken = MyConfig.usr_token;
                    jsonJoinCompany.id = sortlist.get(position).createCompanyRet.id;
                    MyLog.d("", "HttpTools: joinCompany: ");
                    MyConfig.joinCompany(jsonJoinCompany);
                    JsonSetCurrentCompany jsonSetCurrentCompany = new JsonSetCurrentCompany(sortlist.get(position).createCompanyRet.id);
                    MyConfig.setCurrentCompany(jsonSetCurrentCompany);
                }
            };
            MyConfig.showRemindDialog("", getString(R.string.prompt_if_join) + sortlist.get(position).createCompanyRet.name + "?", getActivity(), true, iOnOkClickListener);
        }
    };

    public void onEventMainThread(EventHaveSetCurrentCompany eventHaveSetCurrentCompany) {
        MyLog.d("", "create or choose company: eventCompanySetCurrent.success " + eventHaveSetCurrentCompany.success);

        if (eventHaveSetCurrentCompany.success == true) {
            getActivity().finish();
        } else {
            //  MyConfig.snackbarTop(activity_company_invite_linearLayout, "Join company failed!", R.color.Red, Snackbar.LENGTH_SHORT);
            MyConfig.MyToast(1, getActivity(), "eventHaveSetCurrentCompany failed!");
        }
    }

    public
    _ListAdapterCompanyView.OnClickedListener onQuitClickedListener = new _ListAdapterCompanyView.OnClickedListener() {
        @Override
        public void OnClicked(final int position) {
            new DialogFragment() {
                @Override
                public Dialog onCreateDialog(Bundle savedInstanceState) {
                    // Use the Builder class for convenient dialog construction
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String title;

                    builder.setTitle(getString(R.string.prompt_if_quit))
                            .setPositiveButton(R.string.prompt_confirm, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
//                                    JsonJoinCompany jsonJoinCompany = new JsonJoinCompany();
//                                    jsonJoinCompany.accessToken = MyConfig.usr_token;
//                                    jsonJoinCompany.id = list.get(position).createCompanyRet.id;
//                                    MyConfig.joinCompany(jsonJoinCompany);
                                }
                            })
                            .setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                    // Create the AlertDialog object and return it
                    return builder.create();
                }
            }.show(getFragmentManager(), "");
        }
    };
    _ListAdapterCompanyView.OnClickedListener onSettingClickedListener = new _ListAdapterCompanyView.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
//            MyConfig.MyToast(0, getActivity(), "goto setting");

            // Intent intent_company = new Intent(getActivity(), ActivityCompanySetting.class);
            Intent intent_company = new Intent(getActivity(), _ActivityCompanyViewSetting.class);
            startActivity(intent_company);
            intent_company.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, list.get(position).createCompanyRet.id);
            intent_company.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORID, list.get(position).creator);
            intent_company.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORNICK, list.get(position).creatorNick);
            intent_company.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_USERCOUNT, list.get(position).userCount);
            //startActivityForResult(intent_company, MyConfig.REQUEST_CANJOIN_COMPANY);
            startActivity(intent_company);

        }
    };

    _ListAdapterCompanyView.OnClickedListener onClickedListener = new _ListAdapterCompanyView.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
            popMenu(position);
        }
    };

    _ListAdapterCompanyView.OnLongClickedListener onLongClickedListener = new _ListAdapterCompanyView.OnLongClickedListener() {
        @Override
        public void OnLongClicked(int position) {
            //  popMenu(position);
        }
    };


    private void popMenu(final int position) {

        new DialogFragment() {
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                // Use the Builder class for convenient dialog construction
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                String title;

                if (list.get(position).status == 0) {//have not joined
                    builder.setTitle(getString(R.string.prompt_if_join))
                            .setPositiveButton(R.string.prompt_confirm, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    JsonJoinCompany jsonJoinCompany = new JsonJoinCompany();
                                    jsonJoinCompany.accessToken = MyConfig.usr_token;
                                    jsonJoinCompany.id = list.get(position).createCompanyRet.id;
                                    MyConfig.joinCompany(jsonJoinCompany);
                                }
                            })
                            .setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                } else {
                    builder.setTitle(getString(R.string.prompt_you_have_joined))
                            .setItems(R.array.company_operation, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    // of the selected item
                                    switch (which) {
                                        case 0://set as current
                                            JsonSetCurrentCompany jsonSetCurrentCompany = new JsonSetCurrentCompany(list.get(position).createCompanyRet.id);
//                                            jsonSetCurrentCompany.accessToken = MyConfig.usr_token;
//                                            jsonSetCurrentCompany.id = list.get(position).createCompanyRet.id;
                                            MyConfig.setCurrentCompany(jsonSetCurrentCompany);
                                            break;
                                        case 1://leave

                                            break;
                                    }

                                }
                            })
                            .setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                }
                // Create the AlertDialog object and return it
                return builder.create();
            }
        }.show(getFragmentManager(), "");
    }

//
//    public void joinCompany(final JsonJoinCompany jsonJoinCompany) {
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Gson gson = new Gson();
//                String jsonStr = gson.toJson(jsonJoinCompany, JsonJoinCompany.class);
//
//                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_JOIN_COMPANY + jsonJoinCompany.id, jsonStr);
//
//                final JsonJoinCompanyRet jsonJoinCompanyRet = gson.fromJson(s, JsonJoinCompanyRet.class);
//                MyLog.d("", "HttpTools: JsonJoinCompanyRet " + s);
//                if (jsonJoinCompanyRet != null) {
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(
//                                    getActivity(),
//                                    jsonJoinCompanyRet.message,
//                                    Toast.LENGTH_LONG).show();
//                        }
//                    });
//                }
//
//
//                EventBus.getDefault().post(new EventCompanyListUpdate());
//            }
//        }).start();
//
//    }


    public void onEventMainThread(EventHaveGetCurrentCompany eventHaveGetCurrentCompany) {

        MyLog.d("choosecompany", "company change flow: FragmentChooseCompany get EventHaveGetCurrentCompany ");

        /**
         * 切换当前公司应该不需要刷新公司列表
         */

        if (eventHaveGetCurrentCompany.code == MyConfig.retSuccess()) {
            uiCompanyListUpdateFromLocal();
        } else {

        }
    }

    public void onEventMainThread(EventCompanyListUpdateUi eventCompanyListUpdateUi) {
        MyLog.d("", "companyListUpdateFromServer EventCompanyListUpdateUi");

        uiCompanyListUpdateFromLocal();
    }


    private void uiCompanyListUpdateFromLocal() {

        list.clear();
        sortlist.clear();

        if (MyConfig.jsonUserCompaniesRet != null
                && MyConfig.jsonUserCompaniesRet.code==MyConfig.retSuccess()
                && MyConfig.jsonUserCompaniesRet.data != null) {
            for (JsonUserCompaniesRet.UserCompaniesRet urr : MyConfig.jsonUserCompaniesRet.data) {
                if (mCurFilter == null) {
                    list.add(Company.fromUserCompanyRet(urr));
                } else if (urr.name.contains(mCurFilter)) {
                    list.add(Company.fromUserCompanyRet(urr));
                }

            }
        }

        if (MyConfig.jsonCompanyCanJoinRet != null
                && MyConfig.jsonCompanyCanJoinRet.code==MyConfig.retSuccess()
                && MyConfig.jsonCompanyCanJoinRet.data != null) {
            for (JsonCompanyCanJoinRet.CompanyCanJoinRet rcjr : MyConfig.jsonCompanyCanJoinRet_sort.data) {

                if (mCurFilter == null) {
                    sortlist.add(Company.fromCompanyCanJoin(rcjr));
                } else if (rcjr.name.contains(mCurFilter)) {
                    sortlist.add(Company.fromCompanyCanJoin(rcjr));
                }

            }

        }

//        if (false) {//不显示未加入公司
//            if (MyConfig.jsonCompanyCanJoinRet != null
//                    && MyConfig.jsonCompanyCanJoinRet.code==MyConfig.retSuccess()
//                    && MyConfig.jsonCompanyCanJoinRet.data != null) {
//                for (JsonCompanyCanJoinRet.CompanyCanJoinRet rcjr : MyConfig.jsonCompanyCanJoinRet.data) {
//
//
//                    if (mCurFilter == null) {
//                        list.add(Company.fromCompanyCanJoin(rcjr));
//                    } else if (rcjr.name.contains(mCurFilter)) {
//                        list.add(Company.fromCompanyCanJoin(rcjr));
//                    }
//
//                }
//
//            }
//        }

        /**
         * 如果有当前公司，将其放在最上面
         */
        if (MyConfig.usr_status >= MyConfig.USR_STATUS_HAVE_CURRENT_COMPANY
                && MyConfig.jsonGetCurrentCompanyRet != null
                && MyConfig.jsonGetCurrentCompanyRet.code==MyConfig.retSuccess()) {
//            if (MyConfig.jsonGetCurrentCompanyRet != null && MyConfig.jsonGetCurrentCompanyRet.code==MyConfig.retSuccess()) {
            ListIterator<Company> listIterator = list.listIterator();
            while (listIterator.hasNext()) {
                Company company = listIterator.next();
                if (company.createCompanyRet.id == MyConfig.jsonGetCurrentCompanyRet.data.id) {
                    MyLog.d("", "company sort " + company.createCompanyRet.name);
                    listIterator.remove();
                    list.add(0, company);
                    break;
                }
            }
        }
//        else{
//
//            //先看看有没有加入任何公司
//            list.size()
//            MyConfig.setCurrentCompany(new JsonSetCurrentCompany(list.getLast()));
//
//
//        }
//        MyConfig.compnayListSize = list.size();

        adapter.notifyDataSetChanged();
        sortadapter.notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_company_view, menu);
        item = menu.findItem(R.id.menu_company_view_more);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_search_company:
                Intent create_company = new Intent(getActivity(), _ActivityCanJoinCompanyList.class);
                startActivityForResult(create_company, MyConfig.REQUEST_CANJOIN_COMPANY);
                return true;
            case R.id.menu_company_view_more:
                if (popupWindow.isShowing()) {
                    popupWindow.dismiss();
                } else {
                    int xoffInPixels = MyConfig.screenWidth - 400;
                    int xoffInDip = MyConfig.px2dip(getActivity(), xoffInPixels);
                    popupWindow.showAsDropDown(getActivity().findViewById(R.id.menu_company_view_more), -xoffInDip, -20);
                    popupWindow.update();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initPopUp() {
        View popView = LayoutInflater.from(getActivity()).inflate(
                R.layout.popwindow_company_view, null);
        LinearLayout createLayout, inviteCodeLayout, setLayout, exitLayout;
        createLayout = (LinearLayout) popView.findViewById(R.id.ceate_company_layout);
        inviteCodeLayout = (LinearLayout) popView.findViewById(R.id.invite_code_layout);
        setLayout = (LinearLayout) popView.findViewById(R.id.set_layout);
        exitLayout = (LinearLayout) popView.findViewById(R.id.quit_layout);
        popupWindow = new PopupWindow(popView, LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, true);

        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        createLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent create_company = new Intent(getActivity(), _ActivityCreateCompany.class);
                startActivityForResult(create_company, MyConfig.REQUEST_CREATE_COMPANY);
                popupWindow.dismiss();
            }
        });
        inviteCodeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), _ActivityInputCompanyInviteCode.class);
                intent.putExtra("mode", 1);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        setLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_company = new Intent(getActivity(), _ActivityCompanyViewSetting.class);
//                intent_company.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, list.get(0).createCompanyRet.id);
//                intent_company.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORID, list.get(0).creator);
//                intent_company.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_CREATORNICK, list.get(0).creatorNick);
//                intent_company.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_USERCOUNT, list.get(0).userCount);
                startActivity(intent_company);
                popupWindow.dismiss();
            }
        });
        exitLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.showRemindDialog(getActivity().getResources().getString(R.string.company_view_worning),
                        getActivity().getResources().getString(R.string.company_view_worning_body), getActivity(), true, new MyConfig.IOnOkClickListener() {
                            @Override
                            public void OnOkClickListener() {
                                EventBus.getDefault().post(new EventLogout(MyConfig.LOGOUT_EVENT_TYPE_LOGOUT));
                                getActivity().finish();
                            }
                        });
                popupWindow.dismiss();
            }
        });


    }

    public boolean onQueryTextChange(String newText) {
        // Called when the action bar search text has changed.  Update
        // the search filter, and restart the loader to do a new query
        // with this filter.
        String newFilter = !TextUtils.isEmpty(newText) ? newText : null;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (mCurFilter == null && newFilter == null) {
            return true;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {
            return true;
        }
        mCurFilter = newFilter;
//        getLoaderManager().restartLoader(0, null, this);

        uiCompanyListUpdateFromLocal();

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // Don't care about this.
        return true;
    }

    @Override
    public boolean onClose() {
        if (!TextUtils.isEmpty(mSearchView.getQuery())) {
            mSearchView.setQuery(null, true);
        }
        return true;
    }

}
