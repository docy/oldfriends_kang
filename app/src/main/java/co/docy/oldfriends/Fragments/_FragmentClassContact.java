package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import co.docy.oldfriends.Adapters.ListAdapterUserList;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.EventBus.EventUserStatusUpdate;
import co.docy.oldfriends.Messages.JsonGetCompanyUserListRet;
import co.docy.oldfriends.Messages.JsonGetGroupUserRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.CustomFastScrollView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class _FragmentClassContact extends FragmentBase {

    public JsonGetGroupUserRet jsonGetGroupUserRet = new JsonGetGroupUserRet();
    public JsonGetCompanyUserListRet jsonGetCompanyUserListRet = new JsonGetCompanyUserListRet();

    ListView contact_xjt_lv;
    // private ListView listView;
    private CustomFastScrollView fastScrollView;
    FrameLayout contact_layout;
    private LinearLayout list_null_background_layout;
    ListAdapterUserList listAdapterCompanyUserList;
    float view_y_origin;
    public interface getPersonNum{
        void getPersonNumBysize(int num);
    }
    public getPersonNum getpersonnum;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        getpersonnum = (getPersonNum)activity;
    }

    private MyConfig.IF_AfterHttpRequest IFAfterHttpRequest = new MyConfig.IF_AfterHttpRequest() {
        @Override
        public void doAfterHttpRequest() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showJsonGetGroupUserListRet();
                }
            });
        }
    };

    private ListAdapterUserList.OnClickedListener onClickedListener_person = new ListAdapterUserList.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
            MyConfig.gotoUserInfo(getActivity(), jsonGetCompanyUserListRet.data.get(position).id);
        }
    };

    private void showJsonGetGroupUserListRet() {

        /**
         * 转换为公司员工列表
         */
        for(JsonGetGroupUserRet.GetGroupUserRet getGroupUserRet:jsonGetGroupUserRet.data){
            jsonGetCompanyUserListRet.data.addLast(JsonGetCompanyUserListRet.fromGroupUsers(getGroupUserRet));
        }

        listAdapterCompanyUserList = new ListAdapterUserList(getActivity(),
                ContactUniversal.convertListFromGetCompanyUserListRet(jsonGetCompanyUserListRet.data), null,
                onClickedListener_person, null);
        contact_xjt_lv.setAdapter(listAdapterCompanyUserList);
        getpersonnum.getPersonNumBysize(ContactUniversal.convertListFromGetCompanyUserListRet(jsonGetCompanyUserListRet.data).size());
        int count = jsonGetCompanyUserListRet.data.size() < 2 ? 0:jsonGetCompanyUserListRet.data.size();
        String format = getActivity().getString(R.string.contact_num);
        String result = String.format(format,count);
        contact_count.setText(result);
        if (jsonGetCompanyUserListRet.data.size() < 2) {
            list_null_background_layout.setVisibility(View.VISIBLE);
        } else {
            list_null_background_layout.setVisibility(View.GONE);
        }
        contact_xjt_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MyConfig.gotoUserInfo(getActivity(), MyConfig.jsonGetCompanyUserListRet.data.get(position).id);
            }
        });
    }

    private TextView contact_count;

    public static _FragmentClassContact newInstance(int class_id) {
        _FragmentClassContact f = new _FragmentClassContact();
        MyLog.d("", "read contact newInstance ");

        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt("class_id", class_id);
        f.setArguments(args);

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MyLog.d("", "read contact onCreateView");
        View v = inflater.inflate(R.layout.fragment_contact_with_scrollbutton, null);
        contact_xjt_lv = (ListView) v.findViewById(android.R.id.list);
        fastScrollView = (CustomFastScrollView) v.findViewById(R.id.fast_scroll_view);
        contact_count = (TextView) v.findViewById(R.id.contact_count);
        list_null_background_layout = (LinearLayout) v.findViewById(R.id.list_null_background_layout);
        getGroupContacts(getArguments().getInt("class_id", -1));
        return v;
    }



    @Override
    public void onResume() {
        super.onResume();

        MyLog.d("", "company change flow: USR_STATUS FragmentContact onResume" + MyConfig.usr_status);
        super.onResume();
        if (MyConfig.netConnectCheck() <= 0) {
            MyConfig.MyToast(0, getActivity(), getString(R.string.net_connect_error));
            return;
        }


    }



    private void getGroupContacts(int class_id) {

        if (class_id < 0){
            return;
        }

        MyLog.d("", "company change flow: USR_STATUS FragmentContact getCompanyContacts a " + MyConfig.usr_status);
        if (MyConfig.usr_status < MyConfig.USR_STATUS_HAVE_CURRENT_COMPANY) {//要先设置当前公司，然后才有公司通讯录的显示
            return;
        }
        MyLog.d("", "company change flow: USR_STATUS FragmentContact getCompanyContacts b " + MyConfig.usr_status);
        if (MyConfig.jsonGetCurrentCompanyRet == null) {
            return;
        }
        MyLog.d("", "company change flow: USR_STATUS FragmentContact getCompanyContacts c " + MyConfig.usr_status);

        if(false) {
            MyConfig.getCurrentYearUserAddressBooks(MyConfig.jsonGetCurrentCompanyRet.data.id, IFAfterHttpRequest);
        }else{
            MyLog.d("", "retrofit: we use retrofit ");

            Call<JsonGetGroupUserRet> repos = HttpTools.http_api_service().getGroupContacts("" + class_id);
            repos.enqueue(new Callback<JsonGetGroupUserRet>() {
                @Override
                public void onResponse(Call<JsonGetGroupUserRet> call, Response<JsonGetGroupUserRet> response) {
                    // Get result Repo from response.body()

                    MyLog.d("", "retrofit: code() " + response.code());
                    MyLog.d("", "retrofit: message() " + response.message());

                    if(response.code()==200){

                        jsonGetGroupUserRet = response.body();

                        MyLog.d("", "retrofit: body() 1 " + response.body().toString());
                        MyLog.d("", "retrofit: body() 2 " + response.body().code);
                        MyLog.d("", "retrofit: body() 3 " + response.body().message);

                        if(response.body().data != null) {
//                            iGetUserList.doAfterHttpRequest();
                            showJsonGetGroupUserListRet();
                        }
                    }

                }

                @Override
                public void onFailure(Call<JsonGetGroupUserRet> call, Throwable t) {
                    MyLog.d("", "retrofit: onFailure " + t.getMessage());
                }
            });
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyLog.d("", "company change flow: USR_STATUS FragmentContact onCreate" + MyConfig.usr_status);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void onEventMainThread(EventUserStatusUpdate eventUserStatusUpdate) {
        MyLog.d("", "company change flow: onEventMainThread in FragmentSettingList.java: EventLogin");

        showJsonGetGroupUserListRet();
    }




}
