package co.docy.oldfriends.Fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.support.v7.widget.SearchView;

import java.util.HashMap;
import java.util.LinkedList;

import co.docy.oldfriends.Adapters.ListAdapterSubjectList;
import co.docy.oldfriends.Adapters.ListAdapterVotePersonList;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventGroupListUpdateUi;
import co.docy.oldfriends.Messages.JsonGetTopicDetailRet;
import co.docy.oldfriends.R;


public class FragmentVoteVoterList extends FragmentBase implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {


    public HashMap<String, String> options_map;
    public HashMap<String, JsonGetTopicDetailRet.Choices> choices_map;
    LinkedList<String> options_ll = new LinkedList<>();
    LinkedList<JsonGetTopicDetailRet.Choices> choices_ll = new LinkedList<>();

    ExpandableListView fragment_person_list;
    ListAdapterVotePersonList mAdapter;
    SearchView mSearchView;
    String mCurFilter;

    public static FragmentVoteVoterList newInstance() {
        FragmentVoteVoterList f = new FragmentVoteVoterList();

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_vote_person_list, null);
        fragment_person_list = (ExpandableListView ) v.findViewById(R.id.fragment_person_list);
        return v;
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        setHasOptionsMenu(true);

        /**
         * 从vote imsg里面取出option及choices
         */

        options_map = MyConfig.imsgVoteResult.originDataFromServer.getMsgTopicRet.info.options;
        choices_map = MyConfig.imsgVoteResult.originDataFromServer.getMsgTopicRet.info.choices;

//        LinkedList<Integer> keys = new LinkedList<>();
//        for (String key : MyConfig.imsgVoteResult.originDataFromServer.getMsgTopicRet.info.options.keySet()) {
//            keys.add(Integer.valueOf(key));
//        }
//        Collections.sort(keys);

        for(int i = 0; i<options_map.size(); i++){

            options_ll.add(options_map.get(""+i));
            choices_ll.add(choices_map.get(""+i));

        }

        mAdapter = new ListAdapterVotePersonList(getActivity(), options_ll, choices_ll);
        fragment_person_list.setAdapter(mAdapter);
        int count = mAdapter.getGroupCount();
        for (int position = 1; position <= count; position++)
            fragment_person_list.expandGroup(position - 1);

    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onClose() {
        if (!TextUtils.isEmpty(mSearchView.getQuery())) {
            mSearchView.setQuery(null, true);
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void onEventMainThread(EventGroupListUpdateUi eventGroupListUpdateUi) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java");

    }

    ListAdapterSubjectList.OnLongClickedListener onLongClickedListener = new ListAdapterSubjectList.OnLongClickedListener() {
        @Override
        public void OnLongClicked(int position) {

        }
    };

    ListAdapterSubjectList.OnClickedListener onClickedListener = new ListAdapterSubjectList.OnClickedListener() {
        @Override
        public void OnClicked(int position) {

        }
    };




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Place an action bar item for searching.
        MenuItem item = menu.add("Search");
        item.setIcon(android.R.drawable.ic_menu_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM
                | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        mSearchView = new MyConfig.MySearchView(getActivity());
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        item.setActionView(mSearchView);
    }


    public boolean onQueryTextChange(String newText) {
        // Called when the action bar search text has changed.  Update
        // the search filter, and restart the loader to do a new query
        // with this filter.
        String newFilter = !TextUtils.isEmpty(newText) ? newText : null;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (mCurFilter == null && newFilter == null) {
            return true;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {
            return true;
        }
        mCurFilter = newFilter;

//        getLoaderManager().restartLoader(0, null, this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // Don't care about this.
        return true;
    }


}
