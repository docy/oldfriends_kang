package co.docy.oldfriends.Fragments;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.gson.Gson;

import co.docy.oldfriends.Activitis._ActivityAccount;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventLogin;
import co.docy.oldfriends.Messages.JsonLogin;
import co.docy.oldfriends.Messages.JsonWeixinLoginRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;

public class _FragmentV4AccountLogin extends FragmentBaseV4 {

    public EditText fragment_page_account_login_phone_name;
    EditText fragment_page_account_login_pwd;
    Button fragment_page_account_login_next;

    public static _FragmentV4AccountLogin newInstance() {
        _FragmentV4AccountLogin f = new _FragmentV4AccountLogin();

        return f;
    }

    public View mView;

    public void setVisibile(int visibile) {
        mView.setVisibility(visibile);
    }

    public void setY(float y) {
        mView.setY(y);
    }

    public float getY() {
        return mView.getY();
    }

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_page_account_login, null);
        mView = v;

        fragment_page_account_login_phone_name = (EditText) v.findViewById(R.id.fragment_page_account_login_phone_name);
        fragment_page_account_login_pwd = (EditText) v.findViewById(R.id.fragment_page_account_login_pwd);
        fragment_page_account_login_next = (Button) v.findViewById(R.id.fragment_page_account_login_next);

        fragment_page_account_login_phone_name.setText(MyConfig.usr_name);
        fragment_page_account_login_pwd.setText(MyConfig.usr_pwd);
        showNextButton();

        fragment_page_account_login_pwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                showNextButton();
            }
        });

        fragment_page_account_login_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragment_page_account_login_pwd.getText().toString().length() < 6) {
                    ((_ActivityAccount) getActivity()).showFragmentReset();
                } else {
                    loginRequest();
                }

            }
        });

        return v;
    }

    public void showNextButton() {
        if (fragment_page_account_login_pwd.getText().toString().length() < 6) {
            fragment_page_account_login_next.setText(getString(R.string.login_froget_password));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) fragment_page_account_login_next.getLayoutParams();
            int margin = (int) getResources().getDimension(R.dimen.view_marge);
            layoutParams.setMargins(margin, 0, margin, 0);
            layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            fragment_page_account_login_next.setLayoutParams(layoutParams);
            fragment_page_account_login_next.setBackgroundResource(R.color.transparent);
        } else {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) fragment_page_account_login_next.getLayoutParams();
            int margin = (int) getResources().getDimension(R.dimen.view_marge);
            layoutParams.setMargins(margin, (int) getResources().getDimension(R.dimen.login_next_button_top),
                    margin, (int) getResources().getDimension(R.dimen.login_next_button_top));
            layoutParams.width = (int) getActivity().getResources().getDimension(R.dimen.chat_room_button_size);
            layoutParams.height = (int) getActivity().getResources().getDimension(R.dimen.chat_room_button_size);
            fragment_page_account_login_next.setLayoutParams(layoutParams);
            fragment_page_account_login_next.setText("");
            fragment_page_account_login_next.setBackgroundResource(R.drawable.login_next);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    public void loginRequest() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                String login_name_or_phone = fragment_page_account_login_phone_name.getText().toString();
                String pwd = fragment_page_account_login_pwd.getText().toString();
//                MyConfig.usr_name_or_phone_temply_for_login_or_reset = login_name_or_phone;
                JsonLogin jsonLogin = new JsonLogin();
                jsonLogin.account = login_name_or_phone;
                jsonLogin.password = pwd;
                jsonLogin.os = MyConfig.os_string;
                jsonLogin.uuid = MyConfig.uuid;
                Gson gson = new Gson();
                String j = gson.toJson(jsonLogin);

                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LOGIN, j);

                final JsonWeixinLoginRet jsonLoginRet = gson.fromJson(s, JsonWeixinLoginRet.class);

                if (jsonLoginRet != null) {

                    if (jsonLoginRet.code != MyConfig.retSuccess()) {

                        /**
                         * 密码错了就清除保存的密码，并保持在这个位置
                         */
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //  et_pwd.setText("");
//                                MyConfig.MyToast(0, getActivity(), getString(R.string.error_login));
                                MyConfig.MyToast(0, getActivity(), MyConfig.getErrorMsg(""+jsonLoginRet.code));
                                MyLog.d("", "new error code: " + jsonLoginRet.code + " msg: " + MyConfig.getErrorMsg(""+jsonLoginRet.code));
                            }
                        });

                        MyConfig.usr_pwd = "";
                        MyConfig.saveUserDataToPreference(getActivity());

                    } else if (jsonLoginRet.data != null) {

                        MyConfig.logoutClear();

                        MyConfig.usr_login_ret_json = s;
                        MyConfig.jsonWeixinLoginRet = jsonLoginRet;
                        MyConfig.usr_id = jsonLoginRet.data.id;
                        MyConfig.usr_name = jsonLoginRet.data.name;//注意name要从这里取，因为login_name_or_phone也可能是phone
                        MyConfig.usr_nickname = jsonLoginRet.data.nickName;
                        MyConfig.usr_phone = jsonLoginRet.data.phone;
                        MyConfig.usr_pwd = pwd;
                        MyConfig.usr_token = jsonLoginRet.data.authToken;//常用变量就提取出来
                        MyConfig.usr_status = MyConfig.USR_STATUS_LOGIN;
                        //  MyConfig.usr_email = jsonLoginRet.data.email;
                        MyConfig.usr_phone = jsonLoginRet.data.phone;
                        if (jsonLoginRet.data.sex) {
                            MyConfig.usr_sex = MyConfig.USER_SEX_MAN;//男
                        } else {
                            MyConfig.usr_sex = MyConfig.USER_SEX_WOMAN;//女
                        }
                        MyConfig.usr_avatar = jsonLoginRet.data.avatar;
                        MyConfig.usr_origin = jsonLoginRet.data.origin;
                        if(jsonLoginRet.data.info != null){MyConfig.usr_name_changed = jsonLoginRet.data.info.changed;}


                        MyConfig.saveUserDataToPreference(getActivity());

                        finishWithSuccess();
                    }

                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            Toast.makeText(getActivity(), getString(R.string.net_msg_not_connected), Toast.LENGTH_SHORT).show();
                            MyConfig.MyToast(0, getActivity(), getString(R.string.net_msg_not_connected));
                        }
                    });
                }

            }
        }).start();
    }

    private void finishWithSuccess() {

        EventBus.getDefault().post(new EventLogin());

        getActivity().finish();
    }
}
