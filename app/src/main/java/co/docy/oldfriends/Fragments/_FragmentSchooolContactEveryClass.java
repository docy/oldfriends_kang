package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;

import co.docy.oldfriends.Adapters.ListAdapterUserList;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.Messages.JsonSchoolContactEveryClass;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/5/4.
 */
public class _FragmentSchooolContactEveryClass extends FragmentBase {

    public static _FragmentSchooolContactEveryClass newInstance() {
        _FragmentSchooolContactEveryClass f = new _FragmentSchooolContactEveryClass();
        return f;
    }
    public interface getClassPersonNum{
        public void getPersonNum(int Num);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        getclasspersonnum =(getClassPersonNum) activity;
    }

    getClassPersonNum getclasspersonnum;
    ListView every_class_listview;
    int class_id;
    ListAdapterUserList listAdapterCompanyUserList;
    int num;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_schoool_contact_every_class,null);
        every_class_listview =(ListView) v.findViewById(R.id.fragment_school_contact_every_class_listview);
        class_id = getArguments().getInt("num_id");
        getEveryClassPerson();

        return v;
    }


    private ListAdapterUserList.OnClickedListener onClickedListener_person = new ListAdapterUserList.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
            MyConfig.gotoUserInfo(getActivity(), MyConfig.jsonSchoolContactEveryClass.data.get(position).id);
        }
    };


    public void getEveryClassPerson(){
        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SCHOOL_EVERY_CLASS_CONTACT +class_id+MyConfig.API_SCHOOL_EVERY_CLASS_CONTACT_2, MyConfig.usr_token);
                MyLog.d("某个班级所有成员列表:", class_id+ s);
                MyConfig.jsonSchoolContactEveryClass = gson.fromJson(s, JsonSchoolContactEveryClass.class);
                if(MyConfig.jsonSchoolContactEveryClass.data!=null&&MyConfig.jsonSchoolContactEveryClass.code==MyConfig.retSuccess()){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listAdapterCompanyUserList = new ListAdapterUserList(getActivity(),
                                    ContactUniversal.SchoolContactEveryClass(MyConfig.jsonSchoolContactEveryClass.data), null,
                                    onClickedListener_person, null);
                            every_class_listview.setAdapter(listAdapterCompanyUserList);
                            getclasspersonnum.getPersonNum(ContactUniversal.SchoolContactEveryClass(MyConfig.jsonSchoolContactEveryClass.data).size());

                        }
                    });
                }


            }
        }).start();
    }
}
