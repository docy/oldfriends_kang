package co.docy.oldfriends.Fragments;

import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.docy.oldfriends.Adapters.ListAdapterUserList;
import co.docy.oldfriends.Adapters.ListAdapterSchoolContact;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.Messages.JsonSearchSchoolContactRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/5/3.
 */
public class FragmentSchoolContact extends FragmentBase {

    ListView schoolContact_listview;
    ListAdapterSchoolContact listAdapterSchoolContact;
    ListAdapterUserList listAdapterUserList_;
    JsonSearchSchoolContactRet jsonSearchSchoolContactRet = new JsonSearchSchoolContactRet();
    SearchView mSearchView;
    String mCurFilter;
    EditText ed;
    TextView close,search_hint;
    ImageView remove;
    LinearLayout linear;
    List<ContactUniversal> list = new ArrayList<>();

    public static FragmentSchoolContact newInstance() {
        FragmentSchoolContact f = new FragmentSchoolContact();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_school_contact, null  );
        schoolContact_listview =(ListView) v.findViewById(R.id.fragment_school_contact_listview);
        listAdapterUserList_ = new ListAdapterUserList(getActivity(),
                list , null,
                onClickedListener_person, null);
        View view = getActivity().getLayoutInflater().inflate(R.layout.add_people_to_contact,null);
        schoolContact_listview.addHeaderView(view);
        schoolContact_listview.setAdapter(listAdapterUserList_);
        initWithHead(view);
        return v;
    }
    public void initWithHead(View v){
        ed =(EditText) v.findViewById(R.id.activity_search_contact_edittext);
        close =(TextView) v.findViewById(R.id.activity_search_contact_close);
        remove =(ImageView) v.findViewById(R.id.activity_search_contact_remove);
        linear =(LinearLayout) v.findViewById(R.id.activity_search_contact_linear);
        search_hint =(TextView) v.findViewById(R.id.activity_search_contact_hint);
        search_hint.setVisibility(View.GONE);
        close.setVisibility(View.GONE);
        remove.setVisibility(View.GONE);

    }

    @Override
    public void onResume() {
        super.onResume();
        ed.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){//获得焦点
                    close.setVisibility(View.VISIBLE);
                    remove.setVisibility(View.VISIBLE);
                }else{//失去焦点
                    close.setVisibility(View.GONE);
                    remove.setVisibility(View.GONE);
                }
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed.setText("");
                ed.requestFocus();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed.setText("");
                ed.clearFocus();
                MyConfig.hide_keyboard_must_call_from_activity(getActivity());
            }
        });


        ed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s==null||s.length()==0){

                }else {
                    searchSchoolContact(s.toString());
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private ListAdapterUserList.OnClickedListener onClickedListener_person = new ListAdapterUserList.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
            MyConfig.gotoUserInfo(getActivity(), jsonSearchSchoolContactRet.data.get(position).id);
        }
    };

    //搜索的功能+
    public void searchSchoolContact(final String str){

            new Thread(new Runnable() {
                @Override
                public void run() {
                    Gson gson = new Gson();
                    HashMap<String,String> map = new HashMap();
                    map.put("search",str);
                    final String s = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2() + "follows/searchUser",map, MyConfig.usr_token);
                    MyLog.d("kanghongpu","搜索好友："+s);
                    jsonSearchSchoolContactRet = gson.fromJson(s, JsonSearchSchoolContactRet.class);
                    if(jsonSearchSchoolContactRet!=null&&jsonSearchSchoolContactRet.code==MyConfig.retSuccess()){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                list.clear();
                                search_hint.setVisibility(View.GONE);
                                list.addAll(list.size(), ContactUniversal.searchSchoolContact(jsonSearchSchoolContactRet.data));
                                        listAdapterUserList_.searchContactKey = str;
                                        if(list==null||list.size()==0){
                                            MyLog.d("kanghongpu","查看是不是被执行");
                                            search_hint.setVisibility(View.VISIBLE);
                                        }
                                listAdapterUserList_.notifyDataSetChanged();

                            }
                        });
                    }
                }
            }).start();

    }

}
