package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.LinkedList;

import co.docy.oldfriends.Activitis.ActivityNotificationMessage;
import co.docy.oldfriends.Activitis.ActivitySendNotice;
import co.docy.oldfriends.Adapters.ListAdapterScheduleNotification;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ScheduleNotification;
import co.docy.oldfriends.Messages.JsonNotificationMsgFromSocket;
import co.docy.oldfriends.Messages.JsonNotificationReceiveRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

public class FragmentScheduleNotifications extends FragmentBase implements SearchView.OnQueryTextListener, SearchView.OnCloseListener{

    ListAdapterScheduleNotification listAdapterScheduleNotification;
    LinkedList<ScheduleNotification> list = new LinkedList<ScheduleNotification>();
    ListView fragment_schedule_notifications;
    ScheduleNotification scheduleNotification  ;
    SearchView mSearchView;
    public static FragmentScheduleNotifications newInstance() {
        FragmentScheduleNotifications f = new FragmentScheduleNotifications();

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MyLog.d("", "viewpager get samplelistfragment");
        View v = inflater.inflate(R.layout.fragment_schedule_notifications, null);
        fragment_schedule_notifications = (ListView)v.findViewById(R.id.fragment_schedule_notifications);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                final String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_RECEIVE_NOTIFICATION , MyConfig.usr_token);
                final JsonNotificationReceiveRet jsonNotificationReceiveRet = gson.fromJson(s, JsonNotificationReceiveRet.class);
                MyLog.d("kanghongpu", "Http_JsonNotificationReceiveRet" + s);

                Activity activity = getActivity();
                if(activity!=null)
                    activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (jsonNotificationReceiveRet != null && jsonNotificationReceiveRet.code == MyConfig.retSuccess()) {
                            for (JsonNotificationReceiveRet.NotificationReceiveRet info : jsonNotificationReceiveRet.data) {
                                scheduleNotification = new ScheduleNotification();
                                scheduleNotification.title = info.title;
                                scheduleNotification.time = info.createdAt;
                                scheduleNotification.body = info.content;
                                scheduleNotification.author = info.sender;
                                scheduleNotification.nid = info.id;
                                scheduleNotification.type = info.readed;
                                scheduleNotification.senderAvatar = info.senderAvatar;
                                scheduleNotification.receiver = info.receiver;
                                list.add(scheduleNotification);
                            }
                        }
                        setAdapterScheduleNotification();
                    }
                });

            }

        }).start();

        setHasOptionsMenu(true);
        return v;
    }
    //添加适配器
    public void setAdapterScheduleNotification(){



        listAdapterScheduleNotification = new ListAdapterScheduleNotification(getActivity(), list, new ListAdapterScheduleNotification.OnClickedListener(){
            public void OnClicked(int position){
                ScheduleNotification scheduleNotification = list.get(position);

                MyConfig.scheduleNotification = scheduleNotification;
                Intent intent = new Intent(getActivity(), ActivityNotificationMessage.class);
                startActivity(intent);

            }
        }, null);
        fragment_schedule_notifications.setAdapter(listAdapterScheduleNotification);

    }

    public void onEventMainThread(JsonNotificationMsgFromSocket jsonNotificationMsgFromSocket) {
        MyLog.d("刷新当前页查看能不能收到新消息",jsonNotificationMsgFromSocket+"");
        /**
         * 收到的必定是未读的，必定需要点亮小红点
         */
        scheduleNotification = new ScheduleNotification();
        scheduleNotification.title = jsonNotificationMsgFromSocket.info.title;
        scheduleNotification.time = jsonNotificationMsgFromSocket.info.createdAt;
        scheduleNotification.body = jsonNotificationMsgFromSocket.info.content;
        scheduleNotification.author = jsonNotificationMsgFromSocket.info.sender;
        scheduleNotification.nid = jsonNotificationMsgFromSocket.info.id;
        scheduleNotification.type = String.valueOf(jsonNotificationMsgFromSocket.info.readed);
        scheduleNotification.senderAvatar = jsonNotificationMsgFromSocket.info.senderAvatar;
        scheduleNotification.receiver = jsonNotificationMsgFromSocket.info.receiver;
        list.add(scheduleNotification);
        listAdapterScheduleNotification.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        setAdapterScheduleNotification();
        if(!TextUtils.isEmpty(mCurFilter)) {
            uiSearchcontact(mCurFilter);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Place an action bar item for searching.
        MenuItem item = menu.add("Search");

        if(MyConfig.usr_type == MyConfig.USER_TYPE_IS_TEACHER ){
            //当身份为老师的时候显示
            MenuItem item_setnotice = menu.add("发通知");
            item_setnotice.setIcon(R.drawable.write_notice_2x).setVisible(true).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            item_setnotice.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {

                    Intent intent = new Intent(getActivity(),ActivitySendNotice.class);
                    startActivity(intent);

                    return true;
                }
            });
        }

        item.setIcon(R.drawable.company_views_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM
                | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        mSearchView = new MyConfig.MySearchView(getActivity());
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);

        item.setActionView(mSearchView);
    }

    String mCurFilter;
    @Override
    public boolean onClose() {
        if (!TextUtils.isEmpty(mSearchView.getQuery())) {
            mSearchView.setQuery(null, true);
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String newFilter = !TextUtils.isEmpty(newText) ? newText : null;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (mCurFilter == null && newFilter == null) {
            return true;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {
            return true;
        }

        mCurFilter = newFilter;
//        getLoaderManager().restartLoader(0, null, this);
        if(!TextUtils.isEmpty(mCurFilter)) {
            uiSearchcontact(mCurFilter);
        }else{
            setAdapterScheduleNotification();
        }

        return true;
    }
    LinkedList<ScheduleNotification> searchList = new LinkedList<ScheduleNotification>();
    private ListAdapterScheduleNotification.OnClickedListener  listAdapterScheduleNotification_OnClickedListener = new ListAdapterScheduleNotification.OnClickedListener(){
        public void OnClicked(int position){
            ScheduleNotification scheduleNotification = searchList.get(position);
            MyConfig.scheduleNotification = scheduleNotification;
            Intent intent = new Intent(getActivity(), ActivityNotificationMessage.class);
            startActivity(intent);

        }
    };
    private void uiSearchcontact(final String mCurFilter) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                searchList.clear();
                for (ScheduleNotification contact : list) {

                    if (mCurFilter == null) {
                        searchList.add(contact);
                    } else if (
                            (contact.author != null && contact.author.contains(mCurFilter))
                                    || (contact.title != null && contact.title.contains(mCurFilter))
                                    || (contact.time != null && contact.time.contains(mCurFilter))
                                    ||(contact.body != null && contact.body.contains(mCurFilter))
                            ) {
                        searchList.add(contact);
                    }
                }
                listAdapterScheduleNotification = new ListAdapterScheduleNotification(getActivity(),
                        searchList,listAdapterScheduleNotification_OnClickedListener, null);
                listAdapterScheduleNotification.searchContactKey = mCurFilter;

                fragment_schedule_notifications.setAdapter(listAdapterScheduleNotification);

            }
        });
    }

}
