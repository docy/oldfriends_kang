package co.docy.oldfriends.Fragments;

import android.app.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.LinkedList;

import co.docy.oldfriends.Activitis.ActivityClassEvaluate;
import co.docy.oldfriends.Activitis.ActivityFavoriteList;
import co.docy.oldfriends.Activitis.ActivityMyDetail;
import co.docy.oldfriends.Activitis.ActivitySettings;
import co.docy.oldfriends.Adapters.ListAdapter_MadeInOrder_me;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventLogout;
import co.docy.oldfriends.Messages.JsonMadeInOrderFromServiceRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import de.greenrobot.event.EventBus;

public class FragmentV4Me extends FragmentBaseV4 implements View.OnClickListener {
    private LinearLayout tab_me_portrait, tab_me_credit, tab_me_paper, tab_me_favority, tab_me_setting;
    ImageView tab_me_photo;
    TextView tab_me_name,tab_me_phone;
    Button tab_me_quit;
    private TextView tab_me_class;
    ListView listview;
    ListAdapter_MadeInOrder_me listAdapter_MadeInOrder_me;
    LinkedList<JsonMadeInOrderFromServiceRet.JsonMadeInOrder_userProfile> list =new LinkedList<>();

    public static FragmentV4Me newInstance(String s) {
        FragmentV4Me f = new FragmentV4Me();
        return f;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_me, null);

        tab_me_portrait = (LinearLayout)v.findViewById(R.id.tab_me_portrait);
        tab_me_portrait.setOnClickListener(this);
//        tab_me_paper = (LinearLayout)v.findViewById(R.id.tab_me_paper);
//        tab_me_paper.setOnClickListener(this);
        tab_me_favority = (LinearLayout)v.findViewById(R.id.tab_me_favority);
        tab_me_favority.setOnClickListener(this);
        tab_me_setting = (LinearLayout)v.findViewById(R.id.tab_me_setting);
        tab_me_setting.setOnClickListener(this);
//        tab_me_credit = (LinearLayout)v.findViewById(R.id.tab_me_credit);
//        tab_me_credit.setOnClickListener(this);
        tab_me_quit = (Button)v.findViewById(R.id.tab_me_quit);
        tab_me_quit.setOnClickListener(this);

        tab_me_name =(TextView) v.findViewById(R.id.tab_me_portrait_name);
        tab_me_class =(TextView) v.findViewById(R.id.tab_me_class);
        tab_me_phone =(TextView) v.findViewById(R.id.tab_me_portrait_phone);
        tab_me_photo =(ImageView) v.findViewById(R.id.tab_me_portrait_photo);
        listview =(ListView) v.findViewById(R.id.tab_me_favority_listview);



        if(MyConfig.jsonMadeInOrderFromServiceRet!=null
                &&MyConfig.jsonMadeInOrderFromServiceRet.data!=null
                &&MyConfig.jsonMadeInOrderFromServiceRet.data.userProfile!=null) {
            list = MyConfig.jsonMadeInOrderFromServiceRet.data.userProfile;
        }else{
//            EventBus.getDefault().post(new EventLogout(MyConfig.LOGOUT_EVENT_TYPE_LOGOUT_RESET_DATE));
        }

        listAdapter_MadeInOrder_me = new ListAdapter_MadeInOrder_me(getActivity(), list, new ListAdapter_MadeInOrder_me.OnClickedListener() {
            @Override
            public void OnClicked(int position) {

                Intent creditIntent = new Intent(getActivity(), ActivityClassEvaluate.class);
                creditIntent.putExtra(MyConfig.WEB_VIEW_URL_CATEGORY,MyConfig.WEB_VIEW_URL_CATEGORY_CREDIT);
                creditIntent.putExtra("url",list.get(position).url);
                creditIntent.putExtra("name",list.get(position).name);

                startActivity(creditIntent);

            }
        });
        listview.setAdapter(listAdapter_MadeInOrder_me);
        setListviewHeight();
        return v;
    }
    //设置listview的高度
    public void setListviewHeight(){

        ListAdapter listAdapter = listview.getAdapter();
        if (listAdapter == null) {

        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listview);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listview.getLayoutParams();
        params.height = totalHeight + 1;
        listview.setLayoutParams(params);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab_me_portrait:
                Intent portraitIntent = new Intent(getActivity(), ActivityMyDetail.class);
                startActivity(portraitIntent);
                break;
          /*  case R.id.tab_me_credit:
                Intent creditIntent = new Intent(getActivity(), ActivityClassEvaluate.class);
                creditIntent.putExtra(MyConfig.WEB_VIEW_URL_CATEGORY,MyConfig.WEB_VIEW_URL_CATEGORY_CREDIT);
                startActivity(creditIntent);
                break;
            case R.id.tab_me_paper:
                Intent paperIntent = new Intent(getActivity(), ActivityClassEvaluate.class);
                paperIntent.putExtra(MyConfig.WEB_VIEW_URL_CATEGORY,MyConfig.WEB_VIEW_URL_CATEGORY_PAPER);
                startActivity(paperIntent);
                break;*/
            case R.id.tab_me_favority:
                Intent favoriteIntent = new Intent(getActivity(), ActivityFavoriteList.class);
                startActivity(favoriteIntent);
                break;
            case R.id.tab_me_quit:
                MyConfig.IOnOkClickListener onOkClickListener = new MyConfig.IOnOkClickListener() {
                    @Override
                    public void OnOkClickListener() {
                        EventBus.getDefault().post(new EventLogout(MyConfig.LOGOUT_EVENT_TYPE_LOGOUT));
                    }
                };
                MyConfig.showRemindDialog("", getString(R.string.exit_sure), getActivity(), true, onOkClickListener);
                break;

//            case R.id.setting_language://保留这段代码，或许以后用
//                new DialogFragment() {
//
//                    @Override
//                    public void onCreate(Bundle savedInstanceState) {
//                        super.onCreate(savedInstanceState);
//                    }
//
//                    @Override
//                    public void onDestroy() {
//                        super.onDestroy();
//                    }
//
//                    @Override
//                    public Dialog onCreateDialog(Bundle savedInstanceState) {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(
//                                getActivity());
//                        builder.setTitle(getString(R.string.prompt_choose_language))
//                                .setItems(R.array.app_lang_list, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        switch (which) {
//                                            case 0://中文
//                                                MyConfig.switchAppLang(getActivity(), MyConfig.APP_LANG_CN);
//                                                break;
//                                            case 1://西文
//                                                MyConfig.switchAppLang(getActivity(), MyConfig.APP_LANG_EN);
//                                                break;
//                                        }
//
//
//                                        MyConfig.restartApp(getActivity());
//                                    }
//                                })
//                                .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//
//                                    }
//                                });
//                        return builder.create();
//                    }
//
//                }.show(getFragmentManager(), "");
//                break;
            case R.id.tab_me_setting:
                Intent intent_setting = new Intent(getActivity(),ActivitySettings.class);
                startActivity(intent_setting);
                break;
        }

    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putSerializable("list", (Serializable) list);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        /**ActivityMyDetail页面可能会更改数据，所以当从ActivityMyDetail页面返回后要更新页面*/
        initData();

//        showUserData();
    }

    /**
     * 初始化数据
     */
    public void initData() {
        tab_me_name.setText(MyConfig.usr_nickname);
        tab_me_class.setText(MyConfig.usr_current_class_name);

        tab_me_phone.setText(MyConfig.usr_phone);
        Picasso.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + MyConfig.usr_avatar)
//                .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(tab_me_photo);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyLog.d("", "onActivityResult hit fragment! request code " + requestCode + " result code " + resultCode);
        if (requestCode == MyConfig.REQUEST_GALLERY_AVATAR && resultCode == Activity.RESULT_OK) {

            Intent innerIntent = new Intent("com.android.camera.action.CROP");
            innerIntent.setDataAndType(data.getData(), MyConfig.MIME_IMAGE_STAR);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true");// 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
            if (innerIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);
            }


//            String realPath = MyConfig.getUriPath(getActivity(), fileUri);
//            File tempAttach = new File(realPath);
//
//            uploadUserAvatar(tempAttach);
        }

        if (requestCode == MyConfig.REQUEST_IMAGE_CROP && resultCode == Activity.RESULT_OK) {
            MyLog.d("", "onActivityResult hit fragment! REQUEST_IMAGE_CROP" + resultCode);

            Bitmap bitmap = data.getParcelableExtra("data");

            File tempAttach = new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg");

            // 图像保存到文件中
            FileOutputStream foutput = null;
            try {
                foutput = new FileOutputStream(tempAttach);
                if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, foutput)) {
//                    Toast.makeText(MyConfig.app,
//                            "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath(),
//                            Toast.LENGTH_LONG).show();
                    MyConfig.MyToast(-1, MyConfig.app,
                            "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath());
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            MyLog.d("", "onActivityResult hit fragment! uploadUserAvatar() file length " + tempAttach.length());
            MyConfig.uploadUserAvatar(tempAttach);

        }

    }
    public void onEventMainThread(JsonMadeInOrderFromServiceRet orderToVertical){
        list.clear();
        list.addAll(list.size(),MyConfig.jsonMadeInOrderFromServiceRet.data.userProfile);
        listAdapter_MadeInOrder_me.notifyDataSetChanged();
    }


}
