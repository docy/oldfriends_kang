package co.docy.oldfriends.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.LinkedList;

import co.docy.oldfriends.Activitis.ActivityChatRoomList;
import co.docy.oldfriends.Activitis.ActivityGroupInfo;
import co.docy.oldfriends.Activitis.ActivityMainGroup;
import co.docy.oldfriends.Activitis.ActivityNewsGroup;
import co.docy.oldfriends.Adapters.ListAdapterViewpagerTalk;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.Group;
import co.docy.oldfriends.EventBus.EventBeInvitedInGroup;
import co.docy.oldfriends.EventBus.EventGroupListUpdateFromServer;
import co.docy.oldfriends.EventBus.EventGroupListUpdateUi;
import co.docy.oldfriends.EventBus.EventJionedGroup;
import co.docy.oldfriends.EventBus.EventKick;
import co.docy.oldfriends.EventBus.EventUserStatusUpdate;
import co.docy.oldfriends.Messages.JsonDeleteGroup;
import co.docy.oldfriends.Messages.JsonGroupCanJoinRet;
import co.docy.oldfriends.Messages.JsonJoinGroup;
import co.docy.oldfriends.Messages.JsonJoinGroupRet;
import co.docy.oldfriends.Messages.JsonLeaveGroup;
import co.docy.oldfriends.Messages.JsonSetGroupToTop;
import co.docy.oldfriends.Messages.JsonSetGroupToTopRet;
import co.docy.oldfriends.Messages.JsonUserGroupsRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;

public class FragmentV4TalkList extends FragmentBaseV4 {

    final static String TAG_ARGS = "ARGS";

    ListAdapterViewpagerTalk adapter;
    LinkedList<Group> list_group = new LinkedList<>();
    ListView lv_groups;
    String name_join, desc_join;

    /**
     * 长按小组条目弹出的对话框中选择的位置
     */
    int dialogSelectPosition = -1;
    //    ListAdapterViewpagerTalk adapter_direct_chat;
    public ListAdapterViewpagerTalk.IJoinedClickListener onJoinClickedListener = new ListAdapterViewpagerTalk.IJoinedClickListener() {
        @Override
        public void join(int position) {
            showJoinDialog(position);
        }
    };
//    LinkedList<Group> list_direct_chat = new LinkedList<>();
//    ListView lv_direct_chat;
//    private LinearLayout list_null_background_layout;

//    SegmentedGroup room_list_segment;
//    RadioButton room_list_segment_group;
//    RadioButton room_list_segment_direct;

//    float view_y_origin;

    private MyConfig.IOnOkClickListener iOnOkClickListener;

    public ListAdapterViewpagerTalk.OnImageClickedListener onImageClickedListener = new ListAdapterViewpagerTalk.OnImageClickedListener() {
        @Override
        public void OnImageClicked(int position) {
            Intent intent = new Intent(getActivity(), ActivityGroupInfo.class);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, list_group.get(position).createGroupRet.id);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, list_group.get(position).createGroupRet.name);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, list_group.get(position).createGroupRet.desc);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_JOINED, false);
            intent.putExtra("isShowInvite", false);
            startActivity(intent);
        }
    };
    private String[] dialogArray;

    public static FragmentV4TalkList newInstance(String s) {
        FragmentV4TalkList f = new FragmentV4TalkList();

        Bundle args = new Bundle();
        args.putString(TAG_ARGS, s);
        f.setArguments(args);

        return f;
    }

    private void showJoinDialog(final int position) {
        if (list_group.get(position).status == 0) {
            iOnOkClickListener = new MyConfig.IOnOkClickListener() {
                @Override
                public void OnOkClickListener() {
                    JsonJoinGroup jsonJoinGroup = new JsonJoinGroup();
                    jsonJoinGroup.accessToken = MyConfig.usr_token;
                    jsonJoinGroup.groupId = list_group.get(position).createGroupRet.id;
                    name_join = list_group.get(position).createGroupRet.name;
                    desc_join = list_group.get(position).createGroupRet.desc;
                    joinGroup(jsonJoinGroup);
                }
            };
            MyConfig.showRemindDialog("", getString(R.string.prompt_if_join) + list_group.get(position).createGroupRet.name + "?", getActivity(), true, iOnOkClickListener);
        }
    }

    public void joinGroup(final JsonJoinGroup jsonJoinGroup) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonJoinGroup, JsonJoinGroup.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_JOIN_GROUP + jsonJoinGroup.groupId + MyConfig.API2_JOIN_GROUP_2, jsonStr);

                final JsonJoinGroupRet jsonJoinGroupRet = gson.fromJson(s, JsonJoinGroupRet.class);
                MyLog.d("", "HttpTools: JsonJoinRet " + s);

                if (jsonJoinGroupRet != null && jsonJoinGroupRet.code == MyConfig.retSuccess()) {
                    ActivityGroupInfo.joined = true;
                    Intent groupintent = new Intent(getActivity(), ActivityMainGroup.class);
                    groupintent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, jsonJoinGroup.groupId);
                    groupintent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, name_join);
                    groupintent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, desc_join);
                    startActivity(groupintent);
                    EventBus.getDefault().post(new EventGroupListUpdateFromServer());
                    EventBus.getDefault().post(new EventJionedGroup(true));

                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MyConfig.MyToast(0, getActivity(),
                                    jsonJoinGroupRet.message);
                        }
                    });
                }
            }
        }).start();

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_group_list, null);
//        list_null_background_layout = (LinearLayout) v.findViewById(R.id.list_null_background_layout);

        lv_groups = (ListView) v.findViewById(R.id.fragment_room_list);
        adapter = new ListAdapterViewpagerTalk(getActivity(), list_group, onClickedListener, onLongClickedListener);//取消长按操作
        adapter.onJoinClickListener = onJoinClickedListener;
        adapter.imageClickLister = onImageClickedListener;
        lv_groups.setAdapter(adapter);


//        lv_groups.setOnScrollListener(onScrollListener);

//        lv_direct_chat = (ListView) v.findViewById(R.id.fragment_direct_chat_list);
//        adapter_direct_chat = new ListAdapterViewpagerTalk(getActivity(), list_direct_chat, onClickedListener_direct_chat, onLongClickedListener_direct_chat);
//        adapter_direct_chat.onJoinClickListener = onJoinClickedListener;
//        lv_direct_chat.setAdapter(adapter_direct_chat);
//        room_list_segment = (SegmentedGroup) v.findViewById(R.id.room_list_segment);
//        room_list_segment.setTintColor(getResources().getColor(R.color.xjt_toolbar_green));
//        room_list_segment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if (checkedId == R.id.room_list_segment_group) {
//                    MyLog.d("", "FragmentGroupList: segment_group");
//                    lv_groups.setVisibility(View.VISIBLE);
//                    lv_direct_chat.setVisibility(View.GONE);
//                    list_null_background_layout.setVisibility(View.GONE);
//
//                } else {
//                    MyLog.d("", "FragmentGroupList: segment_direct_chat");
//                    lv_groups.setVisibility(View.GONE);
//                    lv_direct_chat.setVisibility(View.VISIBLE);
//                    if (list_direct_chat.size() < 1) {
//                        list_null_background_layout.setVisibility(View.VISIBLE);
//                    } else {
//                        list_null_background_layout.setVisibility(View.GONE);
//                    }
//                }
//            }
//        });
//        room_list_segment.getLayoutParams().width = MyConfig.screenWidth * 2 / 3;
//        view_y_origin = room_list_segment.getY();
//        MyLog.d("", "FragmentGroupList: oom_list_segment.getY()" + view_y_origin);
//
//        room_list_segment_group = (RadioButton) v.findViewById(R.id.room_list_segment_group);
//        room_list_segment_direct = (RadioButton) v.findViewById(R.id.room_list_segment_direct);
//
//        //初始化时显示普通群组
//        room_list_segment_group.updateToIMsg(true);
//        lv_direct_chat.setVisibility(View.GONE);
//        list_null_background_layout.setVisibility(View.GONE);
//        lv_direct_chat.setOnScrollListener(onScrollListener);


        return v;
    }
//
//    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
//        private int mLastFirstVisibleItem;
//        private boolean hide = false;
//
//
//        @Override
//        public void onScrollStateChanged(AbsListView view, int scrollState) {
//
//        }
//
//        @Override
//        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//            if (mLastFirstVisibleItem < firstVisibleItem) {
//                //up
////                room_list_segment.setVisibility(View.INVISIBLE);
//                if (!hide) {
//                    ObjectAnimator objectAnimator =
//                            ObjectAnimator.ofFloat(room_list_segment,
//                                    "y", view_y_origin, view_y_origin - room_list_segment.getHeight()).setDuration(300);
//                    objectAnimator.setInterpolator(new LinearInterpolator());
//                    objectAnimator.setRepeatCount(0);
//                    objectAnimator.start();
//                    hide = true;
//                }
//            }
//            if (mLastFirstVisibleItem > firstVisibleItem) {
//                //down
////                room_list_segment.setVisibility(View.VISIBLE);
//                if (hide) {
//                    ObjectAnimator objectAnimator =
//                            ObjectAnimator.ofFloat(room_list_segment,
//                                    "y", view_y_origin - room_list_segment.getHeight(), view_y_origin).setDuration(300);
//                    objectAnimator.setInterpolator(new LinearInterpolator());
//                    objectAnimator.setRepeatCount(0);
//                    objectAnimator.start();
//                    hide = false;
//                }
//            }
//            mLastFirstVisibleItem = firstVisibleItem;
//        }
//    };

    /**
     * 长按弹出"退出小组"和"小组置顶"对话框
     *
     * @param position
     */
    private void showDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        builder.setTitle("请选择");
        dialogSelectPosition = 0;

        dialogArray = (list_group.get(position).creator == MyConfig.usr_id
                /**新闻小组暂时先不允许删除*/
                ||list_group.get(position).createGroupRet.category==MyConfig.SERVER_ROOM_CATEGORY_NEWS
                /**聊天室不允许删除*/
                ||list_group.get(position).createGroupRet.category==MyConfig.SERVER_ROOM_CATEGORY_CHAT_ROOM
        ) ? new String[]{""} : new String[]{"", "删除小组"};/**如果是群主则不显示“删除小组”*/
        dialogArray[0] = (list_group.get(position).createGroupRet.showTop) ? "取消置顶" : "小组置顶"; /**若已经是置顶则显示“取消置顶”，否则显示“小组置顶”*/

        builder.setSingleChoiceItems(dialogArray, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogSelectPosition = which;
            }
        });
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setDialogPositiveButtonClick(position);
            }
        });
        builder.setNegativeButton("取消", null);
        AlertDialog dialog = builder.create();// 只创建，不显示，显示之前可以做其他操作。
        dialog.show();


        /*if (list_group.get(position).status == 0) {
            iOnOkClickListener = new MyConfig.IOnOkClickListener() {
                @Override
                public void OnOkClickListener() {
                    JsonJoinGroup jsonJoinGroup = new JsonJoinGroup();
                    jsonJoinGroup.accessToken = MyConfig.usr_token;
                    jsonJoinGroup.groupId = list_group.get(position).createGroupRet.id;
                    joinGroup(jsonJoinGroup);
                }
            };
            MyConfig.showRemindDialog("", getString(R.string.prompt_if_join), getActivity(), true, iOnOkClickListener);
        } else {
            if (list_group.get(position).creator == MyConfig.usr_id) {//群主
                iOnOkClickListener = new MyConfig.IOnOkClickListener() {
                    @Override
                    public void OnOkClickListener() {
                        // 进入群组设置页面
                        Intent intent = new Intent(getActivity(), ActivityGroupInfo.class);
                        intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, list_group.get(position).createGroupRet.id);
                        intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, list_group.get(position).createGroupRet.name);
                        getActivity().startActivity(intent);

                    }
                };
                MyConfig.showRemindDialog(getString(R.string.group_exit), getString(R.string.longLickquit_leader_remind), getActivity(), true, iOnOkClickListener);
            } else {// 普通组员
                iOnOkClickListener = new MyConfig.IOnOkClickListener() {
                    @Override
                    public void OnOkClickListener() {
                        JsonLeaveGroup jsonLeaveGroup = new JsonLeaveGroup();
                        jsonLeaveGroup.accessToken = MyConfig.usr_token;
                        jsonLeaveGroup.groupId = list_group.get(position).createGroupRet.id;
                        MyConfig.leaveGroup(jsonLeaveGroup);
                    }
                };
                MyConfig.showRemindDialog(getString(R.string.group_exit), getString(R.string.exit_group_remind_longclick), getActivity(), true, iOnOkClickListener);
            }

        }*/
    }

    /**
     * dialog对话框点击确定按钮的响应事件
     *
     * @param position
     */
    private void setDialogPositiveButtonClick(int position) {
        switch (dialogSelectPosition) {
            case -1:
                break;
            case 0:
                if (list_group.get(position).createGroupRet.showTop) {
                    setGroupTop(false, position);
                } else {
                    setGroupTop(true, position);
                }
                break;
            case 1:
                leaveGroup(position);
                break;
        }
    }

    /**
     * 删除小组
     *
     * @param position
     */
    private void leaveGroup(int position) {
        if (list_group.get(position).creator != MyConfig.usr_id) {//不是群主
            JsonLeaveGroup jsonLeaveGroup = new JsonLeaveGroup();
            jsonLeaveGroup.accessToken = MyConfig.usr_token;
            jsonLeaveGroup.groupId = list_group.get(position).createGroupRet.id;
            MyConfig.leaveGroup(jsonLeaveGroup);
        }
    }

    /**
     * 让小组置顶
     *
     * @param tag
     * @param position
     */
    private void setGroupTop(final boolean tag, final int position) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                JsonSetGroupToTop jsonSetGroupToTop = new JsonSetGroupToTop();
                jsonSetGroupToTop.accessToken = MyConfig.usr_token;
                jsonSetGroupToTop.groupId = list_group.get(position).createGroupRet.id;
                jsonSetGroupToTop.showTop = tag;
                String jsonStr = gson.toJson(jsonSetGroupToTop, JsonSetGroupToTop.class);
                MyLog.d("", "HttpTools: jsonSetGroupPrivateRet jsonStr:" + jsonStr);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_SETGROUPTOP + list_group.get(position).createGroupRet.id + MyConfig.API_GROUP_SETGROUPTOP_2, jsonStr);

                final JsonSetGroupToTopRet jsonSetGroupToTopRet = gson.fromJson(s, JsonSetGroupToTopRet.class);
                MyLog.d("小组置顶返回", "HttpTools: JsonSetGroupToTopRet " + s);
                if (jsonSetGroupToTopRet != null && jsonSetGroupToTopRet.code == MyConfig.retSuccess()) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            /**请求置顶成功后刷新页面*/
                            MyConfig.getCurrentCompany();
                        }
                    });
                }
            }
        }).start();
    }


    ListAdapterViewpagerTalk.OnLongClickedListener onLongClickedListener = new ListAdapterViewpagerTalk.OnLongClickedListener() {
        @Override
        public void OnLongClicked(int position) {
            //popMenu(position);
            if (list_group.get(position).createGroupRet.category == MyConfig.SERVER_ROOM_CATEGORY_DIRECT) {
                deletePrivatetalk(position);
            } else {
                showDialog(position);
            }
        }
    };

    private void deletePrivatetalk(final int position) {
        iOnOkClickListener = new MyConfig.IOnOkClickListener() {
            public void OnOkClickListener() {
                JsonDeleteGroup jsonDeleteGroup = new JsonDeleteGroup();
                jsonDeleteGroup.accessToken = MyConfig.usr_token;
                jsonDeleteGroup.groupId = list_group.get(position).createGroupRet.id;
                MyConfig.DeleteDirectTalk(jsonDeleteGroup);
            }
        };
        if (list_group.get(position).createGroupRet.category == MyConfig.SERVER_ROOM_CATEGORY_DIRECT) {
            MyConfig.showRemindDialog("", getString(R.string.group_delete_direct_chat), getActivity(), true, iOnOkClickListener);
        } else {
//            Toast.makeText(getActivity(),"不能删除小组",Toast.LENGTH_SHORT).show();
        }
    }
   /* ListAdapterViewpagerTalk.OnLongClickedListener onLongClickedListener_direct_chat = new ListAdapterViewpagerTalk.OnLongClickedListener() {
        @Override
        public void OnLongClicked(int position) {
            deleteDirectChat(position);
        }
    };*/
//
//    private void deleteDirectChat(final int position) {
//       iOnOkClickListener = new MyConfig.IOnOkClickListener() {
//            @Override
//            public void OnOkClickListener() {
//
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        JsonLeaveGroup jsonLeaveGroup = new JsonLeaveGroup();
//                        jsonLeaveGroup.accessToken = MyConfig.usr_token;
//                        jsonLeaveGroup.groupId = list_direct_chat.get(position).createGroupRet.id;
//                        Gson gson = new Gson();
//                        String jsonStr = gson.toJson(jsonLeaveGroup, JsonLeaveGroup.class);
//                        final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LEAVE_GROUP + "directchat/del/" + jsonLeaveGroup.groupId, jsonStr);
//                        final JsonLeaveGroupRet jsonLeaveGroupRet = gson.fromJson(s, JsonLeaveGroupRet.class);
//                        MyLog.d("", "HttpTools: JsonLeaveRet " + s);
//                        if (jsonLeaveGroupRet != null && jsonLeaveGroupRet.code == MyConfig.retSuccess()) {
//                            if (null != MyConfig.activityMainGroup) {
//                                MyConfig.activityMainGroup.finish();
//                            }
//                            EventBus.getDefault().post(new EventGroupListUpdateFromServer());
//                        }
//
//                    }
//                }).start();
//                //MyConfig.leaveGroup(jsonLeaveGroup);
//            }
//        };
//        MyConfig.showRemindDialog("", getString(R.string.group_delete_direct_chat), getActivity(), true, iOnOkClickListener);
//    }

//    public void joinGroup(final JsonJoinGroup jsonJoinGroup) {
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Gson gson = new Gson();
//                String jsonStr = gson.toJson(jsonJoinGroup, JsonJoinGroup.class);
//
//                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_JOIN_GROUP + jsonJoinGroup.groupId + MyConfig.API2_JOIN_GROUP_2, jsonStr);
//
//                final JsonJoinGroupRet jsonJoinGroupRet = gson.fromJson(s, JsonJoinGroupRet.class);
//                MyLog.d("", "HttpTools: JsonJoinRet " + s);
//                if (jsonJoinGroupRet != null) {
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            MyConfig.MyToast(-1, getActivity(), jsonJoinGroupRet.message);
//                        }
//                    });
//                }
//
//
//                EventBus.getDefault().post(new EventGroupListUpdateFromServer());
//            }
//        }).start();
//
//    }
//
//    public void leaveGroup(final JsonLeaveGroup jsonLeaveGroup) {
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Gson gson = new Gson();
//                String jsonStr = gson.toJson(jsonLeaveGroup, JsonLeaveGroup.class);
//
//                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LEAVE_GROUP + jsonLeaveGroup.groupId + MyConfig.API2_LEAVE_GROUP_2, jsonStr);
//
//                final JsonLeaveGroupRet jsonLeaveGroupRet = gson.fromJson(s, JsonLeaveGroupRet.class);
//                MyLog.d("", "HttpTools: JsonLeaveRet " + s);
//                if (jsonLeaveGroupRet != null) {
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
////                            Toast.makeText(
////                                    getActivity(),
////                                    jsonLeaveGroupRet.message,
////                                    Toast.LENGTH_LONG).show();
//                            MyConfig.MyToast(-1, getActivity(),
//                                    jsonLeaveGroupRet.message);
//                        }
//                    });
//                }
//
//
//                EventBus.getDefault().post(new EventGroupListUpdateFromServer());
//            }
//        }).start();
//
//    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        uiRoomListUpdateFromLocal();

    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        MyLog.d(MyConfig.TAG_LIFECYCLE, this.getClass().getName() + " savedInstanceState: putSerializable " + list_group.size());
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();

        uiRoomListUpdateFromLocal();
    }

    public void onEventMainThread(EventGroupListUpdateUi eventGroupListUpdateUi) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java");

        uiRoomListUpdateFromLocal();
    }

    public void onEventMainThread(EventUserStatusUpdate eventUserStatusUpdate) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentSettingList.java: EventLogin");

        //update personal info
        uiRoomListUpdateFromLocal();
    }

    public void onEventMainThread(EventKick e) {
        if (e.getMsgRet().userId == MyConfig.usr_id) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Gson gson = new Gson();
                    String user_rooms = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_ROOMS, MyConfig.usr_token);
                    MyLog.d("", "apitest: user rooms " + user_rooms);
                    MyConfig.jsonUserGroupsRet = gson.fromJson(user_rooms, JsonUserGroupsRet.class);
                    if (MyConfig.jsonUserGroupsRet != null && MyConfig.jsonUserGroupsRet.code == MyConfig.retSuccess()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                uiRoomListUpdateFromLocal();
                                //    MyConfig.showRemindDialog("","您已被组长移出"+ list_group.get(position).createGroupRet.name);
                            }
                        });
                    }
                }
            }).start();
        }
    }

    public void onEventMainThread(EventBeInvitedInGroup e) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String user_rooms = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_ROOMS, MyConfig.usr_token);
                MyLog.d("", "apitest: user rooms " + user_rooms);
                MyConfig.jsonUserGroupsRet = gson.fromJson(user_rooms, JsonUserGroupsRet.class);
                if (MyConfig.jsonUserGroupsRet != null && MyConfig.jsonUserGroupsRet.code == MyConfig.retSuccess()) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            uiRoomListUpdateFromLocal();
                        }
                    });
                }
            }
        }).start();
    }


    private void uiRoomListUpdateFromLocal() {

        if (MyConfig.usr_status < MyConfig.USR_STATUS_HAVE_CURRENT_COMPANY) {
            return;
        }

        /**
         * 更新UI界面中的group list
         */

        list_group.clear();
//        list_direct_chat.clear();
        if (MyConfig.jsonUserGroupsRet != null
                && MyConfig.jsonUserGroupsRet.code == MyConfig.retSuccess()
                && MyConfig.jsonUserGroupsRet.data != null) {
            for (JsonUserGroupsRet.UserGroupsRet urr : MyConfig.jsonUserGroupsRet.data) {
                if (urr.category == MyConfig.SERVER_ROOM_CATEGORY_NORMAL
                        || urr.category == MyConfig.SERVER_ROOM_CATEGORY_PRIVATE
                        || urr.category == MyConfig.SERVER_ROOM_CATEGORY_DIRECT
                        || urr.category == MyConfig.SERVER_ROOM_CATEGORY_CLASS
                        ||urr.category == MyConfig.SERVER_ROOM_CATEGORY_NEWS
                        ||urr.category == MyConfig.SERVER_ROOM_CATEGORY_CHAT_ROOM
                        ) {
                    list_group.add(Group.fromUserGroupsRet(urr));
                }
            }
//            if (list_direct_chat.size() < 1 && lv_direct_chat.isShown()) {
//                list_null_background_layout.setVisibility(View.VISIBLE);
//            } else {
//                list_null_background_layout.setVisibility(View.GONE);
//            }
        }

        /**
         * 显示未加入的小组数目
         */
//        Group null_group = new Group();
//        null_group.status = -1;
//        list_group.add(null_group);


        if (MyConfig.jsonGroupCanJoinRet != null
                && MyConfig.jsonGroupCanJoinRet.code == MyConfig.retSuccess()
                && MyConfig.jsonGroupCanJoinRet.data != null) {
            for (JsonGroupCanJoinRet.GroupCanJoinRet rcjr : MyConfig.jsonGroupCanJoinRet.data) {
                /**
                 * 20160428现在不显示未加入小组
                 */
//                list_group.add(Group.fromGroupCanJoin(rcjr));
            }
        }
//        if (MyConfig.jsonGroupCanJoinRet != null
//                && MyConfig.jsonGroupCanJoinRet.code==MyConfig.retSuccess()
//                && MyConfig.jsonGroupCanJoinRet.data != null) {
//            for (JsonGroupCanJoinRet.GroupCanJoinRet rcjr : MyConfig.jsonGroupCanJoinRet.data) {
//                list.add(Group.fromGroupCanJoin(rcjr));
//            }
//        }

//        adapter = new ListAdapterGroups(getActivity(), list, onClickedListener, onLongClickedListener);
//        lv_groups.setAdapter(adapter);
        adapter.notifyDataSetChanged();
//        adapter_direct_chat.notifyDataSetChanged();


        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java 2");

    }


    ListAdapterViewpagerTalk.OnClickedListener onClickedListener = new ListAdapterViewpagerTalk.OnClickedListener() {
        @Override
        public void OnClicked(int position) {

            Group group = list_group.get(position);

            if (group.status == 1) {

                if (group.createGroupRet.category == MyConfig.SERVER_ROOM_CATEGORY_DIRECT) {
                    /**
                     * 20160712 私聊也使用小组框架
                     */
//                    Intent intent_main = new Intent(getActivity(), ActivityDirectChatGroup.class);
                    Intent intent_main = new Intent(getActivity(), ActivityMainGroup.class);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, list_group.get(position).createGroupRet.id);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, list_group.get(position).createGroupRet.name);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, list_group.get(position).createGroupRet.desc);
                    startActivity(intent_main);

                } else if (group.createGroupRet.category == MyConfig.SERVER_ROOM_CATEGORY_NORMAL
                        || group.createGroupRet.category == MyConfig.SERVER_ROOM_CATEGORY_PRIVATE
                        || group.createGroupRet.category == MyConfig.SERVER_ROOM_CATEGORY_CLASS
                        ) {
                    Intent intent_main = new Intent(getActivity(), ActivityMainGroup.class);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, list_group.get(position).createGroupRet.id);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, list_group.get(position).createGroupRet.name);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, list_group.get(position).createGroupRet.desc);
                    startActivity(intent_main);

                } else if (group.createGroupRet.category == MyConfig.SERVER_ROOM_CATEGORY_NEWS) {
                    Intent intent_main = new Intent(getActivity(), ActivityNewsGroup.class);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, list_group.get(position).createGroupRet.id);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, list_group.get(position).createGroupRet.name);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, list_group.get(position).createGroupRet.desc);
                    startActivity(intent_main);
                } else if(group.createGroupRet.category == MyConfig.SERVER_ROOM_CATEGORY_CHAT_ROOM){
                    startActivity(new Intent(getActivity(), ActivityChatRoomList.class));
                }

            }
        }
    };
//
//    ListAdapterViewpagerTalk.OnClickedListener onClickedListener_direct_chat = new ListAdapterViewpagerTalk.OnClickedListener() {
//        @Override
//        public void OnClicked(int position) {
//
//            if (list_direct_chat.get(position).status == 1) {
//
//                Intent intent_main = new Intent(getActivity(), ActivityDirectChatGroup.class);
//                intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, list_direct_chat.get(position).createGroupRet.id);
//                intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, list_direct_chat.get(position).createGroupRet.name);
//                intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, list_direct_chat.get(position).createGroupRet.desc);
//                startActivity(intent_main);
//            }
//        }
//    };
}
