package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import co.docy.oldfriends.Activitis._ActivityAbout;
import co.docy.oldfriends.Activitis._ActivityChangePassword;
import co.docy.oldfriends.Activitis._ActivityCreateCompany;
import co.docy.oldfriends.Activitis._ActivityInputCompanyInviteCode;
import co.docy.oldfriends.Activitis._ActivityJoinedGroup;
import co.docy.oldfriends.Activitis.ActivityUpdatePersonInfo;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventHaveGetCurrentCompany;
import co.docy.oldfriends.EventBus.EventLogout;
import co.docy.oldfriends.EventBus.EventUserStatusUpdate;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import de.greenrobot.event.EventBus;

public class _FragmentCompanyViewSetting extends FragmentBase implements View.OnClickListener {
    private RelativeLayout setting_company_tel,
            setting_company_email,
            setting_company_current,
            setting_company_invite,
            setting_create_company,
            setting_language,
            setting_change_password,
            setting_person_portrait,
            setting_person_nickname,
            setting_person_group,
            setting_person_sex;
    private LinearLayout fragment_setting_about, setting_person_name;
    private RadioButton setting_person_info_sex_man, setting_person_info_sex_woman;
    private TextView setting_company_current_text,
            setting_company_tel_text,
            setting_person_name_text,
            setting_person_nickname_text,
            setting_company_email_text;
    private Button setting_person_quit;
    private ImageView setting_person_portrait_photo, setting_person_nusername_link;
    MyConfig.IOnOkClickListener onOkClickListener;


    private TextView fragment_setting_comment, fragment_setting_scort;

    public static _FragmentCompanyViewSetting newInstance(String s) {
        _FragmentCompanyViewSetting f = new _FragmentCompanyViewSetting();
        return f;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setting, null);

        setting_person_name = (LinearLayout) v.findViewById(R.id.setting_person_name);
        setting_person_nusername_link = (ImageView) v.findViewById(R.id.setting_person_nusername_link);
        setting_person_name_text = (TextView) v.findViewById(R.id.setting_person_name_text);

        setting_person_sex = (RelativeLayout) v.findViewById(R.id.setting_person_sex);
        setting_person_info_sex_man = (RadioButton) v.findViewById(R.id.setting_person_info_sex_man);
        setting_person_info_sex_woman = (RadioButton) v.findViewById(R.id.setting_person_info_sex_woman);

        setting_person_portrait = (RelativeLayout) v.findViewById(R.id.setting_person_portrait);
        setting_person_portrait_photo = (ImageView) v.findViewById(R.id.setting_person_portrait_photo);

        setting_person_nickname_text = (TextView) v.findViewById(R.id.setting_person_nickname_text);
        setting_person_nickname = (RelativeLayout) v.findViewById(R.id.setting_person_nickname);

        setting_change_password = (RelativeLayout) v.findViewById(R.id.setting_change_password);
        setting_person_group = (RelativeLayout) v.findViewById(R.id.setting_person_group);
        setting_person_quit = (Button) v.findViewById(R.id.setting_person_quit);

        //   setting_company_current = (RelativeLayout) v.findViewById(R.id.setting_company_current);
        setting_company_email_text = (TextView) v.findViewById(R.id.setting_company_email_text);
        setting_company_email = (RelativeLayout) v.findViewById(R.id.setting_company_email);

        setting_company_current_text = (TextView) v.findViewById(R.id.setting_company_current_text);
        setting_company_tel_text = (TextView) v.findViewById(R.id.setting_company_tel_text);
        setting_company_tel = (RelativeLayout) v.findViewById(R.id.setting_company_tel);

        setting_company_invite = (RelativeLayout) v.findViewById(R.id.setting_company_invite);
        setting_create_company = (RelativeLayout) v.findViewById(R.id.setting_create_company);

//        fragment_setting_comment = (TextView) v.findViewById(R.id.fragment_setting_comment);
//        fragment_setting_scort = (TextView) v.findViewById(R.id.fragment_setting_scort);
        fragment_setting_about = (LinearLayout) v.findViewById(R.id.fragment_setting_about);
        setting_language = (RelativeLayout) v.findViewById(R.id.setting_language);

        setting_person_sex.setOnClickListener(this);
        setting_person_portrait.setOnClickListener(this);
        setting_person_nickname.setOnClickListener(this);
        setting_person_quit.setOnClickListener(this);
        setting_person_group.setOnClickListener(this);
        setting_change_password.setOnClickListener(this);

        fragment_setting_about.setOnClickListener(this);
        setting_company_invite.setOnClickListener(this);
        setting_company_email.setOnClickListener(this);
        setting_company_tel.setOnClickListener(this);
        setting_create_company.setOnClickListener(this);
        setting_language.setOnClickListener(this);
//        showUserData();

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.setting_person_sex:
                updatePersonInfo("sex");
                break;
            case R.id.setting_person_nickname:
                updatePersonInfo("nickName");
                break;
            case R.id.setting_company_email:
                updatePersonInfo("email");
                break;
            case R.id.setting_company_tel:
                updatePersonInfo("phone");
                break;
            case R.id.setting_person_name:
                updatePersonInfo("username");
                break;
            case R.id.setting_person_group:
                startActivity(new Intent(getActivity(), _ActivityJoinedGroup.class));
                break;
            case R.id.setting_person_portrait:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                intent.setType(MyConfig.MIME_IMAGE_STAR);

                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(intent, MyConfig.REQUEST_GALLERY_AVATAR);
                }
                break;
            case R.id.fragment_setting_about:
                getActivity().startActivity(new Intent(getActivity(), _ActivityAbout.class));
                break;
            case R.id.setting_company_invite:
                Intent inviteintent = new Intent(getActivity(), _ActivityInputCompanyInviteCode.class);
                inviteintent.putExtra("createCompany", false);
                startActivityForResult(inviteintent, MyConfig.REQUEST_COMPANY_INVITE);
                break;
            case R.id.setting_person_quit:
                onOkClickListener = new MyConfig.IOnOkClickListener() {
                    @Override
                    public void OnOkClickListener() {
                        EventBus.getDefault().post(new EventLogout(MyConfig.LOGOUT_EVENT_TYPE_LOGOUT));
                    }
                };
                MyConfig.showRemindDialog("", getString(R.string.exit_sure), getActivity(), true, onOkClickListener);
                break;
            case R.id.setting_create_company:
                Intent create_company = new Intent(getActivity(), _ActivityCreateCompany.class);
                startActivityForResult(create_company, MyConfig.REQUEST_CREATE_COMPANY);
                break;
            case R.id.setting_change_password:
                /**
                 * 将密码的修改分开处理
                 */
                Intent create_company_intent = new Intent(getActivity(), _ActivityChangePassword.class);
                startActivityForResult(create_company_intent, MyConfig.REQUEST_CHANGE_PASSWORD);
                break;
            case R.id.setting_language:
                new DialogFragment() {

                    @Override
                    public void onCreate(Bundle savedInstanceState) {
                        super.onCreate(savedInstanceState);
                    }

                    @Override
                    public void onDestroy() {
                        super.onDestroy();
                    }

                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                getActivity());
                        builder.setTitle(getString(R.string.prompt_choose_language))
                                .setItems(R.array.app_lang_list, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case 0://中文
                                                MyConfig.switchAppLang(getActivity(), MyConfig.APP_LANG_CN);
                                                break;
                                            case 1://西文
                                                MyConfig.switchAppLang(getActivity(), MyConfig.APP_LANG_EN);
                                                break;
                                        }


                                        MyConfig.restartApp(getActivity());
                                    }
                                })
                                .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                        return builder.create();
                    }

                }.show(getFragmentManager(), "");
                break;
        }

    }

    private void updatePersonInfo(String itemName) {


        Intent intent = new Intent(getActivity(), ActivityUpdatePersonInfo.class);
        /**
         * 所有可能修改的参数
         */
        Bundle bundle = new Bundle();
        bundle.putString("nickName", MyConfig.usr_nickname);
        bundle.putString("email", MyConfig.usr_email);
        bundle.putString("phone", MyConfig.usr_phone);
        bundle.putString("username", MyConfig.usr_name);
        bundle.putInt("sex", MyConfig.usr_sex);
        bundle.putBoolean("changed", MyConfig.usr_name_changed);
        intent.putExtras(bundle);
        /**
         * bundle中包含所有三项，但是目前的UI是一次修改一项
         * 故只有这个itemName所指项被显示
         */
        intent.putExtra("itemName", itemName);
        startActivityForResult(intent, MyConfig.REQUEST_USER_UPDATE);

    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putSerializable("list", (Serializable) list);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        showUserData();
    }


    private void showUserData() {

//        if(MyConfig.usr_status < MyConfig.USR_STATUS_HAVE_CURRENT_COMPANY){ //在设置当前公司之后再显示个人数据
//            return;
//        }

        if (MyConfig.usr_avatar != null) {
            MyLog.d("", "usr_avatar: " + MyConfig.usr_avatar);
            Picasso.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + MyConfig.usr_avatar)
                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(setting_person_portrait_photo);
        }

        if (MyConfig.usr_sex == MyConfig.USER_SEX_MAN) {//true = 男
            setting_person_info_sex_man.setChecked(true);
            setting_person_info_sex_man.setVisibility(View.VISIBLE);
            setting_person_info_sex_woman.setVisibility(View.GONE);
        } else {
            setting_person_info_sex_woman.setChecked(true);
            setting_person_info_sex_woman.setVisibility(View.VISIBLE);
            setting_person_info_sex_man.setVisibility(View.GONE);
        }

        setting_person_name_text.setText(MyConfig.usr_name);
        setting_person_nickname_text.setText(MyConfig.usr_nickname);
        //创建公司后怎样成为当前公司？
        //assert这里不会异常
        if (MyConfig.jsonGetCurrentCompanyRet != null) {
            setting_company_current_text.setText(MyConfig.jsonGetCurrentCompanyRet.data.name);
        }
        setting_company_tel_text.setText(MyConfig.usr_phone);
        setting_company_email_text.setText(MyConfig.usr_email);

        if (MyConfig.usr_origin == null || !MyConfig.usr_origin.equals("weixin") || MyConfig.usr_name_changed) {
            setting_person_nusername_link.setVisibility(View.GONE);
            setting_person_name.setEnabled(false);
        } else {
            setting_person_nusername_link.setVisibility(View.VISIBLE);
            setting_person_name.setEnabled(true);
            setting_person_name.setOnClickListener(this);
        }

    }


    public void onEventMainThread(EventHaveGetCurrentCompany eventHaveGetCurrentCompany) {

        MyLog.d("choosecompany", "company change flow: FragmentSettingList get EventHaveGetCurrentCompany ");

        showUserData();
    }

    public void onEventMainThread(EventUserStatusUpdate eventUserStatusUpdate) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentSettingList.java: EventLogin");

        //update personal info
        showUserData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyLog.d("", "onActivityResult hit fragment! request code " + requestCode + " result code " + resultCode);
        if (requestCode == MyConfig.REQUEST_GALLERY_AVATAR && resultCode == Activity.RESULT_OK) {

            Intent innerIntent = new Intent("com.android.camera.action.CROP");
            innerIntent.setDataAndType(data.getData(), MyConfig.MIME_IMAGE_STAR);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true");// 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
            if (innerIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);
            }


//            String realPath = MyConfig.getUriPath(getActivity(), fileUri);
//            File tempAttach = new File(realPath);
//
//            uploadUserAvatar(tempAttach);
        }

        if (requestCode == MyConfig.REQUEST_IMAGE_CROP && resultCode == Activity.RESULT_OK) {
            MyLog.d("", "onActivityResult hit fragment! REQUEST_IMAGE_CROP" + resultCode);

            Bitmap bitmap = data.getParcelableExtra("data");

            File tempAttach = new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg");

            // 图像保存到文件中
            FileOutputStream foutput = null;
            try {
                foutput = new FileOutputStream(tempAttach);
                if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, foutput)) {
//                    Toast.makeText(MyConfig.app,
//                            "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath(),
//                            Toast.LENGTH_LONG).show();
                    MyConfig.MyToast(-1, MyConfig.app,
                            "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath());
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            MyLog.d("", "onActivityResult hit fragment! uploadUserAvatar() file length " + tempAttach.length());
            MyConfig.uploadUserAvatar(tempAttach);

        }

    }


}
