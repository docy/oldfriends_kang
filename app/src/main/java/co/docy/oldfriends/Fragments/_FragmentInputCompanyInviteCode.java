package co.docy.oldfriends.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import co.docy.oldfriends.Activitis._ActivityCreateCompany;
import co.docy.oldfriends.Activitis._ActivityInputCompanyInviteCode;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventCompanyJoined;
import co.docy.oldfriends.Messages.JsonJoinCompanyWithInviteCode;
import co.docy.oldfriends.Messages.JsonSetCurrentCompany;
import co.docy.oldfriends.R;

public class _FragmentInputCompanyInviteCode extends FragmentBase {

    int mode;

    Button invite_join, invite_create, invite_skip;

    EditText invite_edit_code;
    TextView invite_prompt;

    public static _FragmentInputCompanyInviteCode newInstance(int mode) {
        _FragmentInputCompanyInviteCode f = new _FragmentInputCompanyInviteCode();
        f.mode = mode;

        return f;
    }


//    int id_docy = 1;//调试，目前都直接加入docy公司

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_company_invite, null);

        invite_edit_code = (EditText) v.findViewById(R.id.invite_edit_code);
        invite_edit_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                if (s.toString().length() >= 6) {
//                    invite_join.setEnabled(true);
//                } else {
//                    invite_join.setEnabled(false);
//                }
            }
        });

        invite_join = (Button) v.findViewById(R.id.invite_join);

        invite_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (invite_edit_code.getText().toString().length() == MyConfig.INVITE_CODE_LENGTH) {


                    MyLog.d("", "FragmentCompanyInvite: joinCompanyWithInviteCode with code");
                    JsonJoinCompanyWithInviteCode jsonJoinCompanyWithInviteCode = new JsonJoinCompanyWithInviteCode();
                    jsonJoinCompanyWithInviteCode.accessToken = MyConfig.usr_token;
                    jsonJoinCompanyWithInviteCode.code = invite_edit_code.getText().toString();
                    MyConfig.joinCompanyWithInviteCode(jsonJoinCompanyWithInviteCode);


//                Intent intent = new Intent(getActivity(), ActivityChooseCompany.class);
//                startActivityForResult(intent, MyConfig.REQUEST_CHOOSE_COMPANY);
                } else {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                    String fromat = getActivity().getString(R.string.invite);
                    String result = String.format(fromat,MyConfig.INVITE_CODE_LENGTH);
                    MyConfig.MyToast(0, getActivity(),result);
                }

            }
        });

        invite_create = (Button) v.findViewById(R.id.invite_create);
        invite_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), _ActivityCreateCompany.class);
                startActivityForResult(intent, MyConfig.REQUEST_CHOOSE_COMPANY);
            }
        });


        invite_prompt = (TextView) v.findViewById(R.id.invite_prompt);

        /**
         * 输入邀请码页面模式mode
         *
         * 1
         *  只有输入邀请码
         * 2
         *  输入邀请码 + 创建公司
         * 3
         *  输入邀请码 + 跳过
         *
         */

        if(mode == 1){
            invite_prompt.setVisibility(View.GONE);
        }else if (mode == 2) {

//            invite_create.setVisibility(View.GONE);
//            invite_pass.setVisibility(View.GONE);
//            invite_prompt.setVisibility(View.GONE);
//            invite_join.setBackgroundColor(getResources().getColor(R.color.xjt_toolbar_green));
//            invite_join.setTextColor(Color.WHITE);
//
//            TextView invite_prompt_create = (TextView) v.findViewById(R.id.invite_prompt_create);
//            invite_prompt_create.setVisibility(View.GONE);
//            LinearLayout msg_date = (LinearLayout) v.findViewById(R.id.msg_date);
//            msg_date.setVisibility(View.GONE);

        }else if(mode == 3){


            invite_skip = (Button)v.findViewById(R.id.invite_skip);
            invite_skip.setVisibility(View.VISIBLE);
            invite_skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((_ActivityInputCompanyInviteCode)getActivity()).skipInputInviteCode();
                }
            });

        }

        return v;
    }

    public void gotoSetCurrentComapny(int companyId) {
        JsonSetCurrentCompany jsonSetCurrentCompany = new JsonSetCurrentCompany(companyId);
//        jsonSetCurrentCompany.accessToken = MyConfig.usr_token;
//        jsonSetCurrentCompany.id = companyId;
        MyConfig.setCurrentCompany(jsonSetCurrentCompany);
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putSerializable("list", (Serializable) list);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    public void onEventMainThread(EventCompanyJoined eventCompanyJoined) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentCompanyList.java: EventCompanyListUpdateUi");

        if (eventCompanyJoined.companyId > 0) {
            gotoSetCurrentComapny(eventCompanyJoined.companyId);
        } else {
//            MyConfig.snackbarTop(getView(), "Join company failed!", R.color.Red, Snackbar.LENGTH_SHORT);
            MyConfig.MyToast(0, getActivity(), eventCompanyJoined.message);
        }
    }

}
