package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import co.docy.oldfriends.Activitis._ActivityChooseCompanyOwner;
import co.docy.oldfriends.Activitis._ActivityCompanyGroupList;
import co.docy.oldfriends.Activitis._ActivityCompanyView;
import co.docy.oldfriends.Activitis._ActivityGenerateCompanyInviteCode;
import co.docy.oldfriends.Activitis._ActivityUpdateCompanyInfo;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventHaveGetCurrentCompany;
import co.docy.oldfriends.Messages.JsonDeleteCompany;
import co.docy.oldfriends.Messages.JsonDeleteCompanyRet;
import co.docy.oldfriends.Messages.JsonGetCompanyInfoRet;
import co.docy.oldfriends.Messages.JsonLeaveCompany;
import co.docy.oldfriends.Messages.JsonLeaveCompanyRet;
import co.docy.oldfriends.Messages.JsonSetCompanyLogo;
import co.docy.oldfriends.Messages.JsonSetCompanyLogoRet;
import co.docy.oldfriends.Messages.JsonUpdateCompanyInfo;
import co.docy.oldfriends.Messages.JsonUpdateCompanyInfoRet;
import co.docy.oldfriends.Messages.JsonUploadFileRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by wuxue on 15/11/25.
 */
public class _FragmentV4Option extends FragmentBaseV4 implements View.OnClickListener {


    private int creatorId;
    private int company_id;
    private ImageView company_setting_photo, company_setting_chang_picture;
    private TextView company_setting_groupleader_name, company_setting_groupmemb_count, company_setting_name, company_setting_desc, company_setting_address, company_setting_type;
    private LinearLayout company_setting_member_info, company_setting_allgroup, company_setting_inviteCode_layout, company_setting_type_layout;
    private Button company_setting_quit, company_setting_transfer, company_setting_delete;
    private MyConfig.IOnOkClickListener onOkClickListener;
    private CheckBox company_setting_inner_switch;
private RelativeLayout company_setting_name_layout, company_setting_desc_layout;
    private MyConfig.IF_AfterHttpRequest IFAfterHttpRequest = new MyConfig.IF_AfterHttpRequest() {
        @Override
        public void doAfterHttpRequest() {
//            MyConfig.jsonGetCompanyUserListRet_temp = MyConfig.jsonGetCompanyUserListRet;
            Intent newIntent = new Intent(getActivity(), _ActivityChooseCompanyOwner.class);
            newIntent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, MyConfig.jsonGetCurrentCompanyRet.data.id);
            startActivity(newIntent);
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_company_info, null);

        company_setting_inviteCode_layout = (LinearLayout) view.findViewById(R.id.company_setting_invite_code_layout);
        company_setting_inviteCode_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_invite_code = new Intent(getActivity(), _ActivityGenerateCompanyInviteCode.class);
                intent_invite_code.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, MyConfig.jsonGetCurrentCompanyRet.data.id);
                startActivity(intent_invite_code);
            }
        });
        company_setting_photo = (ImageView) view.findViewById(R.id.company_setting_photo);
        company_setting_name = (TextView) view.findViewById(R.id.company_setting_name);
        company_setting_name_layout = (RelativeLayout) view.findViewById(R.id.company_setting_name_layout);
        company_setting_desc = (TextView) view.findViewById(R.id.company_setting_desc);
        company_setting_desc_layout = (RelativeLayout) view.findViewById(R.id.company_setting_desc_layout);
        company_setting_quit = (Button) view.findViewById(R.id.company_setting_quit);
        company_setting_transfer = (Button) view.findViewById(R.id.company_setting_transfer);
        company_setting_delete = (Button) view.findViewById(R.id.company_setting_delete);
        company_setting_allgroup = (LinearLayout) view.findViewById(R.id.company_setting_allgroup);

        company_setting_member_info = (LinearLayout) view.findViewById(R.id.company_setting_member_info);
        company_setting_groupmemb_count = (TextView) view.findViewById(R.id.company_setting_groupmemb_count);
        company_setting_groupleader_name = (TextView) view.findViewById(R.id.company_setting_groupleader_name);

        company_setting_chang_picture = (ImageView) view.findViewById(R.id.company_setting_chang_picture);
        company_setting_address = (TextView) view.findViewById(R.id.company_setting_address);


        company_setting_type_layout = (LinearLayout) view.findViewById(R.id.company_setting_type_layout);
        company_setting_type = (TextView) view.findViewById(R.id.company_setting_type);
        company_setting_inner_switch = (CheckBox) view.findViewById(R.id.company_setting_inner_switch);

        //company_setting_groupleader_name.setText(MyConfig.jsonUserCompaniesRet.data.get(0).creatorNick);

        company_setting_allgroup.setOnClickListener(this);
        company_setting_quit.setOnClickListener(this);
        company_setting_transfer.setOnClickListener(this);
        company_setting_delete.setOnClickListener(this);
        company_setting_type_layout.setOnClickListener(this);
        company_setting_inner_switch.setOnClickListener(this);

        return view;
    }


    private void setUpdateClickListener() {
        company_setting_name_layout.setOnClickListener(this);
        company_setting_desc_layout.setOnClickListener(this);
        company_setting_photo.setOnClickListener(this);
    }


    private void updateCompanyInfo(String itemName) {


        Intent intent = new Intent(getActivity(), _ActivityUpdateCompanyInfo.class);
        /**
         * 目前的UI只能一次修改一项，故只有这个被显示
         * 或以后全部显示
         */
        intent.putExtra("itemName", itemName);
        startActivityForResult(intent, MyConfig.REQUEST_COMPANY_UPDATE);

    }

    public static _FragmentV4Option newInstance(String s) {
        _FragmentV4Option f = new _FragmentV4Option();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString("index", s);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshUserDataAndViews();
    }

    public void refreshUserDataAndViews() {
        if (null != MyConfig.jsonGetCurrentCompanyRet) {
            company_id = MyConfig.jsonGetCurrentCompanyRet.data.id;
            creatorId = MyConfig.jsonGetCurrentCompanyRet.data.creator;
            getCompanyInfo();
            if (MyConfig.usr_id == creatorId) {
                company_setting_quit.setVisibility(View.GONE);
                company_setting_transfer.setVisibility(View.VISIBLE);
                company_setting_delete.setVisibility(View.VISIBLE);
                company_setting_chang_picture.setVisibility(View.VISIBLE);
                company_setting_name_layout.setEnabled(true);
                company_setting_desc_layout.setEnabled(true);
                company_setting_type_layout.setEnabled(true);
                company_setting_inner_switch.setEnabled(true);
                company_setting_photo.setEnabled(true);
                company_setting_inviteCode_layout.setVisibility(View.VISIBLE);

            } else {
                company_setting_quit.setVisibility(View.VISIBLE);
                company_setting_chang_picture.setVisibility(View.GONE);
                company_setting_transfer.setVisibility(View.GONE);
                company_setting_delete.setVisibility(View.GONE);
                company_setting_name_layout.setEnabled(false);
                company_setting_desc_layout.setEnabled(false);
                company_setting_photo.setEnabled(false);
                company_setting_type_layout.setEnabled(false);
                company_setting_inner_switch.setEnabled(false);
                company_setting_inviteCode_layout.setVisibility(View.GONE);
                company_setting_quit.setText(getString(R.string.exitcompany_exit));
            }
        }
    }

    private void getCompanyInfo() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_INFO + company_id, MyConfig.usr_token);
                MyLog.d("", "OKHTTP: company " + company_id + " company info: " + s);
                MyConfig.jsonGetCompanyInfoRet = gson.fromJson(s, JsonGetCompanyInfoRet.class);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (MyConfig.jsonGetCompanyInfoRet != null && MyConfig.jsonGetCompanyInfoRet.code==MyConfig.retSuccess()) {
                            Picasso.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + MyConfig.jsonGetCompanyInfoRet.data.logoUrlOrigin)
                                    .resize(MyConfig.getWidthByScreenPercent(100), MyConfig.getPxFromDimen(getActivity(), R.dimen.companyviews_height))
                                    .centerCrop()
                                    .into(company_setting_photo);
                            company_setting_desc.setText(MyConfig.jsonGetCompanyInfoRet.data.desc);
                            company_setting_name.setText(MyConfig.jsonGetCompanyInfoRet.data.name);
                            company_setting_inner_switch.setChecked(MyConfig.jsonGetCompanyInfoRet.data.isPrivate);

                            company_setting_groupmemb_count.setText(MyConfig.jsonGetCompanyInfoRet.data.userCount+"");
                            company_setting_groupleader_name.setText(MyConfig.jsonGetCompanyInfoRet.data.creatorName);


                            //分类
                            MyLog.d("", "httptools: MyConfig.categorylist.length " + MyConfig.categorylist.length);
                            if (MyConfig.jsonGetCompanyInfoRet.data.category < 0) {
                                /**
                                 * category用作数组下标，必须大于等于0
                                 * 之前创建的公司的category为-1，必须剔除处理
                                 */
                                company_setting_type.setText("");
                            } else if (MyConfig.jsonGetCompanyInfoRet.data.category < MyConfig.categorylist.length) {
                                company_setting_type.setText(MyConfig.categorylist[MyConfig.jsonGetCompanyInfoRet.data.category]);
                            }

                            //地点
                            if (null == MyConfig.jsonGetCompanyInfoRet.data.city) {
                                company_setting_address.setText(getActivity().getString(R.string.city_beijing));
                            } else {
                                company_setting_address.setText(MyConfig.jsonGetCompanyInfoRet.data.city);
                            }


                            if (MyConfig.usr_id == MyConfig.jsonGetCompanyInfoRet.data.creator) {
                                setUpdateClickListener();
                            }
                        }
                    }
                });

            }
        }).start();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyLog.d("", "onActivityResult hit fragment! request code " + requestCode + " result code " + resultCode);
        if (requestCode == MyConfig.REQUEST_GALLERY_AVATAR && resultCode == Activity.RESULT_OK) {

            Intent innerIntent = new Intent("com.android.camera.action.CROP");
            innerIntent.setDataAndType(data.getData(), "image/*");
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true");// 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
            if (innerIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);
            }


//            String realPath = MyConfig.getUriPath(getActivity(), fileUri);
//            File tempAttach = new File(realPath);
//
//            uploadUserAvatar(tempAttach);
        }

        if (requestCode == MyConfig.REQUEST_IMAGE_CROP && resultCode == Activity.RESULT_OK) {

            Bitmap bitmap = data.getParcelableExtra("data");

            File tempAttach = new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg");

            // 图像保存到文件中
            FileOutputStream foutput = null;
            try {
                foutput = new FileOutputStream(tempAttach);
                if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, foutput)) {
                    MyConfig.MyToast(-1, MyConfig.app,
                            "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath());
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            uploadAvatar(tempAttach);

        }

    }

    public void uploadAvatar(final File f) {


        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonUploadFileRet jsonUploadFileRet = HttpTools.okhttpUploadFile(f, "image/jpeg");

                if (jsonUploadFileRet != null && jsonUploadFileRet.code==MyConfig.retSuccess()) {
                    final String path = jsonUploadFileRet.data.path;

                    JsonSetCompanyLogo jsonSetCompanyLogo = new JsonSetCompanyLogo();
                    jsonSetCompanyLogo.accessToken = MyConfig.usr_token;
                    jsonSetCompanyLogo.logo = path;

                    Gson gson = new Gson();
                    String jsonStr = gson.toJson(jsonSetCompanyLogo, JsonSetCompanyLogo.class);

                    final String ss = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_SETLOGO + company_id + MyConfig.API2_COMPANY_SETLOGO_2, jsonStr);

                    final JsonSetCompanyLogoRet jsonSetCompanyLogoRet = gson.fromJson(ss, JsonSetCompanyLogoRet.class);
                    MyLog.d("", "OKHTTP set avatar: " + ss);
                    if (jsonSetCompanyLogoRet != null && jsonSetCompanyLogoRet.code==MyConfig.retSuccess()) {


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Picasso.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + path)
                                       // .fit()
                                        .resize(MyConfig.getWidthByScreenPercent(100), MyConfig.getPxFromDimen(getActivity(), R.dimen.companyviews_height))
                                        .centerCrop()
                                        .into(company_setting_photo);
                            }
                        });

                    }

//                    Snackbar.make(getView(), jsonSetAvatarRet.message, Snackbar.LENGTH_LONG).show();
//                    MyConfig.snackbarTop(getView(), jsonSetGroupLogoRet.message, R.color.Red, Snackbar.LENGTH_SHORT);

                } else {
//                    Snackbar.make(getView(), "更换头像失败", Snackbar.LENGTH_LONG).show();
//                    MyConfig.snackbarTop(getView(), "更换头像失败", R.color.Red, Snackbar.LENGTH_SHORT);
                    //更换头像失败

                }

            }
        }).start();
    }

    /**
     * 弹出解散集体对话框
     */
    public void showRemindDialog(String title, final String remindstr, final Context mContext, final int companyId) {
        View dialogView = LayoutInflater.from(mContext).inflate(
                R.layout.dialog_exit_remind_layout, null);
        final Dialog setDialog = new Dialog(mContext, R.style.DialogStyle);
        setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialog.setCancelable(false);
        setDialog.getWindow().setContentView(dialogView);
        WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
        lp.width = MyConfig.getWidthByScreenPercent(80);
        setDialog.getWindow().setAttributes(lp);
        final Button cancleButton = (Button) dialogView.findViewById(R.id.cancle);
        Button loginButton = (Button) dialogView.findViewById(R.id.login);
        TextView remindText = (TextView) dialogView.findViewById(R.id.remind_text);
        remindText.setText(remindstr);
        TextView titletext = (TextView) dialogView.findViewById(R.id.remind_title);
        titletext.setText(title);
        final boolean istransfer = (title.length() > 0) ? true : false;
        titletext.setVisibility(istransfer ? View.VISIBLE : View.GONE);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (istransfer) {
                    JsonDeleteCompany jsonDeleteCompany = new JsonDeleteCompany();
                    jsonDeleteCompany.accessToken = MyConfig.usr_token;
                    jsonDeleteCompany.companyId = companyId;
                    DeleteCompany(jsonDeleteCompany);
                }
                setDialog.cancel();
            }

        });
        cancleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialog.cancel();
            }
        });
        setDialog.setCanceledOnTouchOutside(false);
        setDialog.show();
    }

    public void leaveCompany(final JsonLeaveCompany jsonLeaveCompany, final Context context) {
        /**
         * 以后退出公司尽量使用这个函数
         */

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonLeaveCompany, JsonLeaveCompany.class);
                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_LEAVE + jsonLeaveCompany.companyId + MyConfig.API2_LEAVE_COMPANY_2, jsonStr);
                final JsonLeaveCompanyRet jsonLeaveCompanyRet = gson.fromJson(s, JsonLeaveCompanyRet.class);
                MyLog.d("", "HttpTools: leaveCompany " + s);
                if (jsonLeaveCompanyRet != null && jsonLeaveCompanyRet.code==MyConfig.retSuccess()) {
//                    if (MyConfig.activityMainGroup != null) {
//                        MyConfig.activityMainGroup.finish();
//                    }

                    /**
                     * 如果退出的是当前公司
                     */
                    if(jsonLeaveCompany.companyId == MyConfig.jsonGetCurrentCompanyRet.data.id){

                        MyConfig.usr_status = MyConfig.USR_STATUS_LOGIN;
                        MyConfig.jsonGetCurrentCompanyRet = null;
                        MyLog.d("", "company change flow: leaveCompany companyId=" + jsonLeaveCompany.companyId);

                    }

                    Intent intent = new Intent(getActivity(), _ActivityCompanyView.class);
                    getActivity().startActivity(intent);
                } else {
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            showRemindDialog("", jsonLeaveCompanyRet.message, context, company_id);
//                        }
//                    });
                }

            }
        }).start();

    }

    public void DeleteCompany(final JsonDeleteCompany jsonDeleteComapny) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonDeleteComapny, JsonDeleteCompany.class);
                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_DELETE_COMPANY +
                        jsonDeleteComapny.companyId, jsonStr);
                final JsonDeleteCompanyRet jsonDeleteCompanyRet = gson.fromJson(s, JsonDeleteCompanyRet.class);
                MyLog.d("", "HttpTools: JsonDeleteCompanyRet " + s);
                if (jsonDeleteCompanyRet != null && jsonDeleteCompanyRet.code==MyConfig.retSuccess()) {
//                    if (MyConfig.activityMainGroup != null) {
//                        MyConfig.activityMainGroup.finish();
//                    }
                    /**
                     * 如果解散的是当前公司
                     */
                    if(jsonDeleteComapny.companyId == MyConfig.jsonGetCurrentCompanyRet.data.id){

                        MyConfig.usr_status = MyConfig.USR_STATUS_LOGIN;
                        MyConfig.jsonGetCurrentCompanyRet = null;

                        MyLog.d("", "company change flow: leaveCompany companyId=" + jsonDeleteComapny.companyId);

                    }
                    Intent intent = new Intent(getActivity(), _ActivityCompanyView.class);
                    startActivity(intent);
                }

            }
        }).start();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.company_setting_allgroup:
                Intent intent_groups = new Intent(getActivity(), _ActivityCompanyGroupList.class);
                intent_groups.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_COMPANYID, company_id);
                startActivityForResult(intent_groups, MyConfig.REQUEST_COMPANY_GROUPS);
                break;
            case R.id.company_setting_quit:
                onOkClickListener = new MyConfig.IOnOkClickListener() {
                    @Override
                    public void OnOkClickListener() {
                        JsonLeaveCompany jsonLeaveCompany = new JsonLeaveCompany();
                        jsonLeaveCompany.accessToken = MyConfig.usr_token;
                        jsonLeaveCompany.companyId = company_id;
                        leaveCompany(jsonLeaveCompany, getActivity());
                    }
                };
                MyConfig.showRemindDialog("", getActivity().getString(R.string.exit_sure_2), getActivity(), true, onOkClickListener);
                break;
            case R.id.company_setting_delete:
                showRemindDialog(getResources().getString(R.string.exit_delete_group), getResources().getString(R.string.exit_deletegroup_remind), getActivity(), company_id);
                break;
            case R.id.company_setting_transfer:
                MyConfig.getCurrentYearUserAddressBooks(company_id, IFAfterHttpRequest);
                break;
            case R.id.company_setting_name_layout:
                updateCompanyInfo(MyConfig.CONSTANT_PARAMETER_STRING_NAME);
                break;
            case R.id.company_setting_desc_layout:
                updateCompanyInfo(MyConfig.CONSTANT_PARAMETER_STRING_DESC);
                break;
            case R.id.company_setting_photo:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                intent.setType(MyConfig.MIME_IMAGE_STAR);
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(intent, MyConfig.REQUEST_GALLERY_AVATAR);
                }
                break;
            case R.id.company_setting_type_layout:

                break;
            case R.id.company_setting_inner_switch:
                if (company_setting_inner_switch.isChecked()) {
                    setCompanyInner(true);
                } else {
                    setCompanyInner(false);
                }
                break;
        }
    }

    public void setCompanyInner(final boolean b) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                final JsonUpdateCompanyInfo jsonUpdateCompanyInfo = new JsonUpdateCompanyInfo();

                jsonUpdateCompanyInfo.accessToken = MyConfig.usr_token;
                jsonUpdateCompanyInfo.isPrivate = b;

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonUpdateCompanyInfo, JsonUpdateCompanyInfo.class);
                MyLog.d("", "HttpTools: jsonStr " + jsonStr);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_COMPANY_INFO_UPDATE + company_id, jsonStr);

                final JsonUpdateCompanyInfoRet jsonUpdateCompanyInfoRet = gson.fromJson(s, JsonUpdateCompanyInfoRet.class);
                MyLog.d("", "HttpTools: " + s);
                if (jsonUpdateCompanyInfoRet != null && jsonUpdateCompanyInfoRet.code==MyConfig.retSuccess()) {

                } else {

                }

            }
        }).start();

    }

    public void onEventMainThread(EventHaveGetCurrentCompany eventHaveGetCurrentCompany) {
        refreshUserDataAndViews();
    }
}
