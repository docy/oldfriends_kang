package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.docy.oldfriends.Activitis.ActivityFriendCricleUserInfo;
import co.docy.oldfriends.Activitis.ActivityMainGroup;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.EventBus.EventAddFriend;
import co.docy.oldfriends.EventBus.EventCompanyListUpdateUi;
import co.docy.oldfriends.Messages.JsonContactAddFriends;
import co.docy.oldfriends.Messages.JsonCreateDirectChatGroup;
import co.docy.oldfriends.Messages.JsonCreateDirectChatRet;
import co.docy.oldfriends.Messages.JsonFriendCricleCommonRet;
import co.docy.oldfriends.Messages.JsonFriendCricleUserInfoRet;
import co.docy.oldfriends.Messages.JsonGetUserInfo;
import co.docy.oldfriends.Messages.JsonGetUserInfoRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import de.greenrobot.event.EventBus;

public class FragmentUserInfoTongChuang extends FragmentBase {

    int user_id;
    String user_name;
    String user_nickName;
    Boolean user_changed;
    List<JsonFriendCricleUserInfoRet.CricleOfFriendRetDataRows> list = new ArrayList<>();

//    JsonGetGroupUserRet.GetGroupUserRet getGroupUserRet;
//    JsonGetUserInfoRet.GetUserInfoRet getUserInfoRet;
    RadioButton userinfo_person_info_sex_man;
    RadioButton userinfo_person_info_sex_woman;

    private TextView user_info_portrait_name;
    private TextView user_info_portrait_company;
    private TextView user_info_contact_phone;
    private TextView user_info_contact_email;
    private TextView user_info_location;
    private TextView user_info_company_summary;
    private TextView user_info_manage_field;
    private Button user_info_direct_chat;
    private ImageView user_info_avatar;
    private TextView user_info_portrait_job,add_friend;
    private TextView user_info_class;
    private ImageView user_info_back_arrow;
    private ImageView userinfo_contact_download;
    private LinearLayout look_friend_cricle;
    ImageView cricle_image1,cricle_image2,cricle_image3,cricle_image4,cricle_view;


    public static FragmentUserInfoTongChuang newInstance(Bundle bundle) {
        FragmentUserInfoTongChuang f = new FragmentUserInfoTongChuang();
        f.setArguments(bundle);
        return f;
    }

//    int every_user_id;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_info_tongchuang, null);
//        every_user_id = getArguments().getInt("use_id");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonGetUserInfo jsonGetUserInfo = new JsonGetUserInfo();
                jsonGetUserInfo.accessToken = MyConfig.usr_token;
                jsonGetUserInfo.id = getArguments().getInt("use_id");
                MyLog.d("", "HttpTools: jsonGetUserInfo.id" + user_id );

                final String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GET_USER_INFO + jsonGetUserInfo.id, MyConfig.usr_token);

                final JsonGetUserInfoRet jsonGetUserInfoRet = gson.fromJson(s, JsonGetUserInfoRet.class);

                MyLog.d("", "HttpTools: " + s);
                if (jsonGetUserInfoRet != null && jsonGetUserInfoRet.code==MyConfig.retSuccess()) {

                    JsonGetUserInfoRet.GetUserInfoRet getUserInfoRet = jsonGetUserInfoRet.data;
                    MyConfig.tempContactUniversal_FragmentUserInfoTongChuang = ContactUniversal.fromJsonGetUserInfoRet(getUserInfoRet);
                    Activity activity = getActivity();
                    if(activity == null){
                        return;
                    }
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            initData();
                            initListener();
                        }
                    });
                }

            }

        }).start();

        initView(v);

        getFriendCricleState();



        return v;
    }



    /**初始化View*/
    private void initView(View v) {

        userinfo_contact_download = (ImageView) v.findViewById(R.id.userinfo_contact_download);
        userinfo_contact_download.setVisibility(View.GONE);
        user_info_back_arrow = (ImageView) v.findViewById(R.id.user_info_back_arrow);
        user_info_avatar = (ImageView) v.findViewById(R.id.user_info_avatar);

        user_info_portrait_name = (TextView) v.findViewById(R.id.user_info_portrait_name);
        user_info_class = (TextView) v.findViewById(R.id.user_info_class);
        user_info_portrait_company = (TextView) v.findViewById(R.id.user_info_portrait_company);
        user_info_portrait_job = (TextView) v.findViewById(R.id.user_info_portrait_job);
        user_info_contact_phone = (TextView) v.findViewById(R.id.user_info_contact_phone);
        user_info_contact_email = (TextView) v.findViewById(R.id.user_info_contact_email);
        user_info_location = (TextView) v.findViewById(R.id.user_info_location);
        user_info_company_summary = (TextView) v.findViewById(R.id.user_info_company_summary);
        user_info_manage_field = (TextView) v.findViewById(R.id.user_info_manage_field);

        user_info_direct_chat = (Button) v.findViewById(R.id.user_info_direct_chat);
        look_friend_cricle =(LinearLayout) v.findViewById(R.id.user_info_state);
        cricle_image1 =(ImageView) v.findViewById(R.id.user_info_state_image1);
        cricle_image2 =(ImageView) v.findViewById(R.id.user_info_state_image2);
        cricle_image3 =(ImageView) v.findViewById(R.id.user_info_state_image3);
        cricle_image4 =(ImageView) v.findViewById(R.id.user_info_state_image4);
        cricle_view =(ImageView) v.findViewById(R.id.user_info_state_view);
        add_friend =(TextView) v.findViewById(R.id.user_info_portrait_add_friend);

    }

    public void getFriendCricleState(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                HashMap<String,String> map = new HashMap<String, String>();
                map.put("userId",getArguments().getInt("use_id")+"");
                String s = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2()+MyConfig.API_GET_USERIF_DATA ,map,MyConfig.usr_token);
                MyLog.d("kanghongpu","个人详情中动态"+s);
                final JsonFriendCricleUserInfoRet jsonCricleOfFriendRet = gson.fromJson(s,JsonFriendCricleUserInfoRet.class);
                if(jsonCricleOfFriendRet!=null&&jsonCricleOfFriendRet.code==MyConfig.retSuccess()){
                    Activity activity = getActivity();
                    if(activity==null){
                        return;
                    }
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(jsonCricleOfFriendRet.data==null||jsonCricleOfFriendRet.data.size()==0){
                                look_friend_cricle.setVisibility(View.GONE);
                                cricle_view.setVisibility(View.GONE);
                            }else {
                               /* 个人详情中朋友圈入口，暂时屏蔽，以后还会用到，
                               look_friend_cricle.setVisibility(View.VISIBLE);
                                cricle_view.setVisibility(View.VISIBLE);*/
                                list.addAll(list.size(),jsonCricleOfFriendRet.data);
                                setImageIntoit(list);
                            }
                        }
                    });
                }
            }
        }).start();
    }
    public void setImageIntoit(List<JsonFriendCricleUserInfoRet.CricleOfFriendRetDataRows> list1){
        switch (list1.size()>4?4:list1.size()){
            case 0:
                break;
            case 1:
                Glide.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + list1.get(0).image)
                        .centerCrop()
                        .into(cricle_image1);
                break;
            case 2:
                Glide.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + list1.get(0).image)
                        .centerCrop()
                        .into(cricle_image1);
                Glide.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + list1.get(1).image)
                        .centerCrop()
                        .into(cricle_image2);
                break;
            case 3:
                Glide.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + list1.get(0).image)
                        .centerCrop()
                        .into(cricle_image1);
                Glide.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + list1.get(1).image)
                        .centerCrop()
                        .into(cricle_image2);
                Glide.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + list1.get(2).image)
                        .centerCrop()
                        .into(cricle_image3);


                break;
            case 4:
                Glide.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + list1.get(0).image)
                        .centerCrop()
                        .into(cricle_image1);
                Glide.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + list1.get(1).image)
                        .centerCrop()
                        .into(cricle_image2);
                Glide.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + list1.get(2).image)
                        .centerCrop()
                        .into(cricle_image3);
                Glide.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + list1.get(3).image)
                        .centerCrop()
                        .into(cricle_image4);
                break;

        }
    }

    /**初始化数据*/
    private void initData() {
//        Bundle bundle = getArguments();
//        getUserInfoRet = bundle.getParcelable(MyConfig.CONST_PARAM_STR_CONTACT_UNIVERAL);
//        getGroupUserRet = bundle.getParcelable(MyConfig.CONSTANT_PARAMETER_STRING_GETGROUPINFORET);

        if (MyConfig.tempContactUniversal_FragmentUserInfoTongChuang != null)  {

            user_id = MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.id;
            user_name = MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.name;
            user_nickName = MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.nickName;

            loadAvatar();

            user_info_portrait_name.setText(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.nickName);
            user_info_class.setText(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.currentClassName);
            user_info_portrait_company.setText(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.company);
            user_info_portrait_job.setText(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.title);

            user_info_contact_phone.setText(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.phone);
            user_info_contact_email.setText(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.email);

            user_info_location.setText(MyConfig.showDataByAnalyse(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.city));
            user_info_company_summary.setText(MyConfig.showDataByAnalyse(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.companyIntro));
            user_info_manage_field.setText(MyConfig.showDataByAnalyse(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.duty));

        }
    }
        /**加载头像，点击头像查看大图*/
    private void loadAvatar() {
        Picasso.with(getActivity()).load("" + MyConfig.getApiDomain_NoSlash_GetResources() + MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.avatar)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(user_info_avatar, new Callback() {
                    @Override
                    public void onSuccess() {
//                        getBitmap();

                    }

                    @Override
                    public void onError() {

                    }
                });

        user_info_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View dialogView = LayoutInflater.from(getActivity()).inflate(
                        R.layout.dialog_user_picture, null);
                final Dialog setDialog = new Dialog(getActivity(), R.style.DialogStyle);
                setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                setDialog.setCancelable(true);
                setDialog.getWindow().setContentView(dialogView);
                WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
                lp.width = MyConfig.getWidthByScreenPercent(100);
                lp.height = MyConfig.getWidthByScreenPercent(100);
                setDialog.getWindow().setAttributes(lp);
                ImageView imageView = (ImageView) dialogView.findViewById(R.id.user_picture);
                imageView.getLayoutParams().width = MyConfig.getWidthByScreenPercent(100);
                imageView.getLayoutParams().height = MyConfig.getWidthByScreenPercent(100);
                Picasso.with(getActivity()).load("" + MyConfig.getApiDomain_NoSlash_GetResources() + MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.avatarOrigin)
//                        .resize(lp.width, lp.width)
                        .fit()
                        .centerCrop()
                        .into(imageView);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setDialog.cancel();
                    }
                });
                setDialog.setCanceledOnTouchOutside(true);
                setDialog.show();
            }
        });
    }

    private void getBitmap() {
        user_info_avatar.buildDrawingCache();
        Bitmap bmap = user_info_avatar.getDrawingCache();
        MyLog.d("","tempContactUniversal_FragmentUserInfoTongChuang.bitmap"+bmap.getByteCount());
        MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.photo_bitmap = bmap;

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.photo_bytes = stream.toByteArray();
        MyLog.d("","tempContactUniversal_FragmentUserInfoTongChuang.photo_bytes"+MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.photo_bytes.length);
    }


    @Override
    public void onDestroy() {  // could be in onPause or onStop
        super.onDestroy();
    }


    /**初始化监听器，发消息的点击事件*/
    private void initListener() {
        if(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.followed){
            add_friend.setText("取消收藏");
        }else{
            add_friend.setText("收藏到通讯录");
        }
        if (user_id == MyConfig.usr_id) {
            user_info_direct_chat.setVisibility(View.GONE);
//            add_friend.setVisibility(View.GONE);

        } else {
            user_info_direct_chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(!MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.followed){

                        MyConfig.MyToast(1, getActivity(),"如果收藏到通讯录，则可以查看对方的朋友圈，并可拉对方进小组");

                    }
                    createDirectChatRoom();
                }
            });
        }

        user_info_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        userinfo_contact_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                new DialogFragment() {

                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                getActivity());
                        builder.setTitle("下载此人联系方式？")
                                .setPositiveButton(getString(R.string.prompt_confirm), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getBitmap();
                                        MyConfig.insertOneContact(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang);
                                        MyConfig.MyToast(0, getActivity(), "联系方式已下载");
                                    }
                                })
                                .setNegativeButton(getString(R.string.prompt_cancel), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                        return builder.create();
                    }
                }.show(getActivity().getFragmentManager(), "");

            }
        });

        look_friend_cricle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ActivityFriendCricleUserInfo.class);
                intent.putExtra("userId",user_id);
                intent.putExtra("userName",user_nickName);
                intent.putExtra("userAvart",MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.avatar);
                startActivity(intent);

            }
        });

        add_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //添加好友的点击事件
                if(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.followed) {
                    new AlertDialog.Builder(getActivity()).setMessage("确定取消？")
                            .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    addfriends(true);
                                }
                            })
                            .setNegativeButton("取消",null).show();

                }else{
                    addfriends(false);
                }
            }
        });

    }
    public void addfriends(final boolean tag){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                JsonContactAddFriends addFriends = new JsonContactAddFriends();
                addFriends.accessToken = MyConfig.usr_token;
                if(tag) {
                    addFriends.unfollow = tag;
                }
                String straddFriends = gson.toJson(addFriends,JsonContactAddFriends.class);
                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2()+"follows/"+user_id,straddFriends);
                MyLog.d("kanghongpu","添加好友"+s);
                JsonFriendCricleCommonRet addFriendRet = gson.fromJson(s,JsonFriendCricleCommonRet.class);
                if(addFriendRet!=null&&addFriendRet.code==MyConfig.retSuccess()){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(tag){
                                Toast.makeText(getActivity(),"删除成功",Toast.LENGTH_SHORT).show();
                                add_friend.setText("收藏到通讯录");
                                MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.followed=
                                        !MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.followed;
                            }else{
                                Toast.makeText(getActivity(),"添加成功",Toast.LENGTH_SHORT).show();
                                add_friend.setText("取消收藏");
                                MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.followed=
                                        !MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.followed;
                            }

                            EventBus.getDefault().post(new EventAddFriend());
                        }
                    });
                }
            }
        }).start();
    }

    /**创建聊天室*/
    private void createDirectChatRoom() {

        new AsyncTask<Integer, Void, String>() {

            @Override
            protected void onPreExecute() {
            }

            @Override
            protected String doInBackground(Integer... integers) {

                JsonCreateDirectChatGroup jsonCreateDirectChatGroup = new JsonCreateDirectChatGroup();
                jsonCreateDirectChatGroup.accessToken = MyConfig.usr_token;
                jsonCreateDirectChatGroup.targetId = integers[0];
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonCreateDirectChatGroup, JsonCreateDirectChatGroup.class);

                return new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CREATE_DIRECT_CHAT_ROOM, jsonStr);

            }

            @Override
            protected void onPostExecute(String s) {
                Gson gson = new Gson();
                JsonCreateDirectChatRet jsonCreateDirectChatRet = gson.fromJson(s, JsonCreateDirectChatRet.class);

                if (jsonCreateDirectChatRet != null && jsonCreateDirectChatRet.code==MyConfig.retSuccess()) {

//                    Toast.makeText(
//                            getActivity(),
//                            jsonCreateDirectChatRet.message,
//                            Toast.LENGTH_LONG).show();

                    MyConfig.MyToast(-1, getActivity(),
                            jsonCreateDirectChatRet.message);

                    /**
                     * 20160712 私聊也使用小组框架
                     */
//                    Intent intent_main = new Intent(getActivity(), ActivityDirectChatGroup.class);
                    Intent intent_main = new Intent(getActivity(), ActivityMainGroup.class);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, jsonCreateDirectChatRet.data.id);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, jsonCreateDirectChatRet.data.name);
                    intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, jsonCreateDirectChatRet.data.desc);
                    startActivity(intent_main);

                    getActivity().finish();
                }

            }

        }.execute(user_id);


    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }



    @Override
    public void onResume() {
        super.onResume();

    }


    public void onEventMainThread(EventCompanyListUpdateUi eventCompanyListUpdateUi) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentCompanyList.java: EventCompanyListUpdateUi");

    }




    /**
     * 发送邮件
     */
    private void sendEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        MyLog.d("kwwl","创建邮件对象");
        //设置文本格式
        emailIntent.setType("text/plain");
        //设置对方邮件地址
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{user_info_contact_email.getText().toString()});
        //emailIntent.setData(Uri.parse("mailto:"+user_info_contact_email.getText()));
        //设置标题内容
        // emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,getString(R.string.setting_recommend_words));
        //设置邮件文本内容
        // emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,getString(R.string.setting_recommend_words));
        startActivity(Intent.createChooser(emailIntent,"Choose Email Client"));
    }

    /**
     * 设置拨打电话的提示对话框
     */
    private void showHintDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
       // builder.setTitle("是否呼叫下方联系人:");
        View view = View.inflate(getActivity(), R.layout.dialog_hint_phone, null);
        ((TextView)view.findViewById(R.id.tv_dialog_phone)).setText(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.phone);
        ((TextView)view.findViewById(R.id.tv_dialog_name)).setText(MyConfig.tempContactUniversal_FragmentUserInfoTongChuang.nickName);

        builder.setView(view);

        builder.setPositiveButton("呼叫", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+user_info_contact_phone.getText().toString()));
                startActivity(intent);
            }
        });
        builder.setNegativeButton("取消", null);

        builder.show();

    }
}
