package co.docy.oldfriends.Fragments;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

import java.util.HashMap;
import java.util.LinkedList;

import co.docy.oldfriends.Activitis.ActivitySubGroup;
import co.docy.oldfriends.Adapters.ListAdapterSubjectList;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventFavoriteChanged;
import co.docy.oldfriends.EventBus.EventListviewScorll;
import co.docy.oldfriends.Messages.JsonGetMsgTopicCommentRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicRet;
import co.docy.oldfriends.Messages.JsonGetSubjectList;
import co.docy.oldfriends.Messages.JsonGetSubjectListRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;

public class FragmentSubjectList extends FragmentBase implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    int msgPageCount = 0;
    int groupId;
    SearchView mSearchView;
    String mCurFilter;
    PopupWindow popupWindow;

    static int Type = -1;
    final int TYPE_DEFULT = -1;
    final int TYPE_TIME_SORT = 0;
    final int TYPE_FAVORITE_SORT = 1;
    final int TYPE_COMMENT_SORT = 2;
    final int TYPE_SEARCH = 3;
    final int TYPE_VIEWS_SORT = 4;

    ListAdapterSubjectList adapter;
    LinearLayout list_null_background_layout;
    private TextView remindText;
    private ImageView remindIcon;
    public LinkedList<JsonGetSubjectListRet.GetSubjectListRet> list_subject = new LinkedList<>();
    com.handmark.pulltorefresh.library.PullToRefreshListView lv_subject;

    public static FragmentSubjectList newInstance(int groupId) {
        FragmentSubjectList f = new FragmentSubjectList();

        f.groupId = groupId;

        return f;
    }


    public View mView;
    public void setVisibile(int visibile){
        mView.setVisibility(visibile);
    }
    public void setY(float y){
        mView.setY(y);
    }
    public float getY(){
        return mView.getY();
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_subject_list, null);
        mView = v;
        list_null_background_layout = (LinearLayout) v.findViewById(R.id.list_null_background_layout);
        remindText = (TextView) v.findViewById(R.id.text);
        remindIcon = (ImageView) v.findViewById(R.id.icon);
        remindIcon.setImageDrawable(getResources().getDrawable(R.drawable.null_subject_list_background_3x));
        remindText.setText(getActivity().getString(R.string.subjectlist_reminder));
        lv_subject = (com.handmark.pulltorefresh.library.PullToRefreshListView) v.findViewById(R.id.fragment_subject_list);
        lv_subject.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
            lv_subject.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
                @Override
                public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                    moreSubjectList(Type);
                }
            });
        lv_subject.setAdapter(adapter);
        adapter = new ListAdapterSubjectList(getActivity(), list_subject, onClickedListener, onLongClickedListener, true);
        lv_subject.setAdapter(adapter);
        lv_subject.setOnScrollListener(onScrollListener);
        adapter.onCheckedChangListener = onCheckedChangListener;
        initPopUp();
        return v;
    }


    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        private int mLastFirstVisibleItem;
        private boolean hide = false;


        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (mLastFirstVisibleItem < firstVisibleItem) {
                //up
//                room_list_segment.setVisibility(View.INVISIBLE);
                if (!hide) {
                    EventBus.getDefault().post(new EventListviewScorll(1));
                    hide = true;
                }
            }
            if (mLastFirstVisibleItem > firstVisibleItem) {
                //down
//                room_list_segment.setVisibility(View.VISIBLE);
                if (hide) {
                    EventBus.getDefault().post(new EventListviewScorll(2));
                    hide = false;
                }
            }
            mLastFirstVisibleItem = firstVisibleItem;
        }
    };

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Type = TYPE_DEFULT;

    }

    @Override
    public void onResume() {
        super.onResume();

        /**
         * todo:
         * 在布局文件中添加的本fragment，所以没有预先传递groupId
         * 注意，本fragment需要嵌入到其他activity时，这里需改
         */
//        groupId = ((ActivitySubjectList) getActivity()).groupId;
        groupId = MyConfig.subjectListGroupId;
        initSubjectList(Type);
//        if (!TextUtils.isEmpty(mCurFilter)) {
//            // moreSubjectList(TYPE_SEARCH);
//            getSubjectListBystr();
//            // initSubjectList(TYPE_SEARCH);
//        } else {
//            initSubjectList(TYPE_DEFULT);
//            //moreSubjectList(TYPE_DEFULT);
//        }

    }

    public void initSubjectList(int type) {
        msgPageCount = 0;
        list_subject.clear();
        moreSubjectList(type);
    }


    public void moreSubjectList(final int type) {
        MyLog.d("查看type的值",type+"");
        msgPageCount++;
//        if (type == TYPE_SEARCH) {
//            list_subject.clear();
//            // TODO: 12/5/15 这个最好统一放在外面
//        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonGetSubjectList jsonGetSubjectList = new JsonGetSubjectList();
                jsonGetSubjectList.accessToken = MyConfig.usr_token;
                jsonGetSubjectList.groupId = groupId;
                jsonGetSubjectList.type = 0;
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, "" + jsonGetSubjectList.groupId);
                switch (type) {
                    case TYPE_DEFULT:
                        hm.put("page", "" + msgPageCount);
                       // hm.put("page", "" + TYPE_DEFULT);
                        //    moreSubjectList();
                        break;
                    case TYPE_COMMENT_SORT:
                        hm.put("page", "" + msgPageCount);
                        hm.put("order[comment]", "desc");
                        break;
                    case TYPE_FAVORITE_SORT:
                        hm.put("page", "" + msgPageCount);
                        hm.put("order[favorite]", "desc");
                        break;
                    case TYPE_VIEWS_SORT:
                        hm.put("page", "" + msgPageCount);
                        hm.put("order[views]", "desc");
                        break;
                    case TYPE_TIME_SORT:
                        hm.put("page", "" + msgPageCount);
                        hm.put("order[updatedAt]", "asc");
                        break;
//                    case TYPE_SEARCH:
//                        hm.put("keyword", "" + mCurFilter);
//                        break;

                }

                final String s = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT, hm, MyConfig.usr_token);
                MyLog.d("", "apitest: testGetSubjectList ret " + s);

                if (s != null) {
                    final JsonGetSubjectListRet jsonGetSubjectListRet = gson.fromJson(s, JsonGetSubjectListRet.class);

                    if (jsonGetSubjectListRet != null && jsonGetSubjectListRet.code == MyConfig.retSuccess()) {
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    for (JsonGetSubjectListRet.GetSubjectListRet getSubjectListRet : jsonGetSubjectListRet.data) {
                                        list_subject.addLast(getSubjectListRet);
                                    }
                                    adapter.searchKey = "";
                                    adapter.notifyDataSetChanged();
                                    if (list_subject.size() < 1) {
                                        list_null_background_layout.setVisibility(View.VISIBLE);
                                    } else {
                                        list_null_background_layout.setVisibility(View.GONE);
                                    }

                                        lv_subject.onRefreshComplete();
                                    lv_subject.setMode(PullToRefreshBase.Mode.BOTH);

                                }
                            });
                        }
                    }
                }

            }
        }).start();
    }

    public void getSubjectListBystr() {
        list_subject.clear();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonGetSubjectList jsonGetSubjectList = new JsonGetSubjectList();
                jsonGetSubjectList.accessToken = MyConfig.usr_token;
                jsonGetSubjectList.groupId = groupId;
                jsonGetSubjectList.type = 0;
                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, "" + jsonGetSubjectList.groupId);

                hm.put("keyword", "" + mCurFilter);

                final String s = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT, hm, MyConfig.usr_token);
                MyLog.d("查看投票搜索", "apitest: testGetSubjectList ret " + s);

                if (s != null) {
                    final JsonGetSubjectListRet jsonGetSubjectListRet = gson.fromJson(s, JsonGetSubjectListRet.class);

                    if (jsonGetSubjectListRet != null && jsonGetSubjectListRet.code == MyConfig.retSuccess()) {
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    for (JsonGetSubjectListRet.GetSubjectListRet getSubjectListRet : jsonGetSubjectListRet.data) {
                                        list_subject.addLast(getSubjectListRet);
                                    }

                                    adapter = new ListAdapterSubjectList(getActivity(), list_subject, onClickedListener, onLongClickedListener, true);
                                    adapter.searchKey = mCurFilter;
                                    lv_subject.setAdapter(adapter);
                                   // adapter.notifyDataSetChanged();
//                                    if (list_subject.size() < 1) {
//                                        list_null_background_layout.setVisibility(View.VISIBLE);
//                                    } else {
//                                        list_null_background_layout.setVisibility(View.GONE);
//                                    }
                                    lv_subject.onRefreshComplete();
                                    lv_subject.setMode(PullToRefreshBase.Mode.DISABLED);
                                    }
                            });
                        }
                    }
                }

            }
        }).start();
    }

    public void onEventMainThread(JsonGetMsgTopicRet.GetMsgTopicRet json_socket_topic) {
        if (json_socket_topic.groupId != groupId) {
            return;
        }
        /**
         * todo
         * 说明有新增topic，未想清楚怎么处理
         */
//        moreSubjectList();
    }

    public void onEventMainThread(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet json_socket_topicComment) {
        if (json_socket_topicComment.groupId != groupId) {
            return;
        }

        subjectListUpdateFromSocket(json_socket_topicComment);
    }

    public void subjectListUpdateFromSocket(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet json_socket_topicComment) {


        for (JsonGetSubjectListRet.GetSubjectListRet getSubjectListRet : list_subject) {

            if (json_socket_topicComment.info.topicId == getSubjectListRet.info.topicId) {

                /**
                 * 得到scoket更新，这里创建一个假的last comment
                 */

                MyConfig.subjectListUpdateLastComment(json_socket_topicComment, getSubjectListRet);

                adapter.notifyDataSetChanged();

                break;
            }

        }
    }


    ListAdapterSubjectList.OnLongClickedListener onLongClickedListener = new ListAdapterSubjectList.OnLongClickedListener() {
        @Override
        public void OnLongClicked(int position) {
        }
    };

    ListAdapterSubjectList.OnClickedListener onClickedListener = new ListAdapterSubjectList.OnClickedListener() {
        @Override
        public void OnClicked(int position) {

            JsonGetSubjectListRet.GetSubjectListRet getSubjectListRet = list_subject.get(position);

//            MyConfig.subRoomRootNode = new IMsg(//目前已经不需要这个结构了，但是需要其中的sub_list
//                    getSubjectListRet.creatorId,
//                    null,
//                    null,
//
//                    MyConfig.UTCString2Date(getSubjectListRet.updatedAt),
//
//                    MyConfig.LEVEL_MSG_1,
//                    MyConfig.mainRoomRootNode, //让其父亲为主聊天室节点，或许以后用得到
//                    MyConfig.MSG_CATEGORY_SUBJECT,
//
//                    getSubjectListRet.groupId,
//                    getSubjectListRet.id,
//                    null,
//
//                    getSubjectListRet.title,
//                    getSubjectListRet.desc
//            );

            Intent intent = new Intent(getActivity(), ActivitySubGroup.class);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_TOPICID, getSubjectListRet.id);
//            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_TITLE, getSubjectListRet.title);
//            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, getSubjectListRet.desc);
            startActivity(intent);
        }
    };
    ListAdapterSubjectList.OnCheckedChangListener onCheckedChangListener = new ListAdapterSubjectList.OnCheckedChangListener() {
        @Override
        public void checkChange(boolean isChecked, int position) {
            MyConfig.addFavorite(list_subject.get(position).id, isChecked);

        }
    };


    public void onEventMainThread(EventFavoriteChanged eventFavoriteChanged) {

        for (JsonGetSubjectListRet.GetSubjectListRet getSubjectListRet : list_subject) {

            if (getSubjectListRet.info.topicId == eventFavoriteChanged.topicId) {
                getSubjectListRet.favorited = eventFavoriteChanged.add;
                if (eventFavoriteChanged.add) {
                    getSubjectListRet.favoriteCount++;
                } else {
                    if (getSubjectListRet.favoriteCount > 0) {
                        getSubjectListRet.favoriteCount--;
                    }
                }
            }

        }

        adapter.notifyDataSetChanged();
    }

    public boolean onQueryTextChange(String newText) {

        String newFilter = !TextUtils.isEmpty(newText) ? newText : null;

        if (mCurFilter == null && newFilter == null) {

            return true;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {

            return true;
        }
        mCurFilter = newFilter;

        if (!TextUtils.isEmpty(mCurFilter)) {
           // moreSubjectList(TYPE_SEARCH);
            getSubjectListBystr();
           // initSubjectList(TYPE_SEARCH);
        } else {
            initSubjectList(TYPE_DEFULT);
           //moreSubjectList(TYPE_DEFULT);
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onClose() {
        if (!TextUtils.isEmpty(mSearchView.getQuery())) {
            mSearchView.setQuery(null, true);
        }
        return true;
    }
    private void uiSubjectListUpdateFromLocal() {

        /**
         * 更新UI界面中的group list
         */

        list_subject.clear();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                JsonGetSubjectList jsonGetSubjectList = new JsonGetSubjectList();
                jsonGetSubjectList.accessToken = MyConfig.usr_token;
                jsonGetSubjectList.groupId = groupId;
                jsonGetSubjectList.type = 0;

                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, "" + jsonGetSubjectList.groupId);
                hm.put("keyword", "" + 1);
                final String s = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT, hm, MyConfig.usr_token);
                MyLog.d("", "apitest: testGetSubjectList ret " + s);

                if (s != null) {
                    final JsonGetSubjectListRet jsonGetSubjectListRet = gson.fromJson(s, JsonGetSubjectListRet.class);

                    if (jsonGetSubjectListRet != null && jsonGetSubjectListRet.code == MyConfig.retSuccess()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                for (JsonGetSubjectListRet.GetSubjectListRet getSubjectListRet : jsonGetSubjectListRet.data) {
                                    list_subject.addLast(getSubjectListRet);
                                }
                                adapter.notifyDataSetChanged();
                                lv_subject.onRefreshComplete();
                                lv_subject.setMode(PullToRefreshBase.Mode.BOTH);
                            }
                        });
                    }
                }

            }
        }).start();
        //   adapter.notifyDataSetChanged();
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java 2");

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem item = menu.add("Search");
        item.setIcon(R.drawable.company_views_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM
                | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        mSearchView = new MyConfig.MySearchView(getActivity()) {
            @Override
            public void onActionViewCollapsed() {
                Type = TYPE_DEFULT;
                initSubjectList(Type);
                super.onActionViewCollapsed();

            }

            @Override
            public void onActionViewExpanded() {
              //  Type = TYPE_SEARCH;

                super.onActionViewExpanded();
            }
        };
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        item.setActionView(mSearchView);

        inflater.inflate(R.menu.menu_subject_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_subject_sort:
                if (popupWindow.isShowing()) {
                    popupWindow.dismiss();
                } else {
                    int xoffInPixels = MyConfig.screenWidth - 400;
                    int xoffInDip = MyConfig.px2dip(getActivity(), xoffInPixels);
                    popupWindow.showAsDropDown(getActivity().findViewById(R.id.menu_subject_sort), -xoffInDip, -20);
                    popupWindow.update();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initPopUp() {
        View popView = LayoutInflater.from(getActivity()).inflate(
                R.layout.popwindow_subject_list, null);
        TextView sortTime, sortClick, sortMember, sortHot;
        sortTime = (TextView) popView.findViewById(R.id.sort_time_text);
        sortClick = (TextView) popView.findViewById(R.id.sort_click_code);
        sortMember = (TextView) popView.findViewById(R.id.sort_member_text);
        sortHot = (TextView) popView.findViewById(R.id.sort_hot_text);
        popupWindow = new PopupWindow(popView, LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, true);

        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        sortTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initSubjectList(TYPE_TIME_SORT);
                Type = TYPE_TIME_SORT;
//                appandURL = "order[updatedAt]=desc";
//                uiSubjectListsort(appandURL);
                popupWindow.dismiss();
            }
        });
        sortClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Type = TYPE_VIEWS_SORT;
                initSubjectList(TYPE_VIEWS_SORT);
                popupWindow.dismiss();
            }
        });
        sortMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Type = TYPE_COMMENT_SORT;
                initSubjectList(TYPE_COMMENT_SORT);
                popupWindow.dismiss();
            }
        });
        sortHot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Type = TYPE_FAVORITE_SORT;
                initSubjectList(TYPE_FAVORITE_SORT);
                popupWindow.dismiss();
            }
        });


    }

    private void uiSubjectListsort(final String appandurl) {

        /**
         * 更新UI界面中的group list
         */

        list_subject.clear();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                JsonGetSubjectList jsonGetSubjectList = new JsonGetSubjectList();
                jsonGetSubjectList.accessToken = MyConfig.usr_token;
                jsonGetSubjectList.groupId = groupId;
                jsonGetSubjectList.type = 0;

                final String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT + "?" + MyConfig.CONSTANT_PARAMETER_STRING_GROUPID + "=" + jsonGetSubjectList.groupId + "&" + appandurl, MyConfig.usr_token);
                MyLog.d("", "apitest: testGetSubjectList ret " + s);
                if (s != null) {
                    final JsonGetSubjectListRet jsonGetSubjectListRet = gson.fromJson(s, JsonGetSubjectListRet.class);

                    if (jsonGetSubjectListRet != null && jsonGetSubjectListRet.code == MyConfig.retSuccess()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                for (JsonGetSubjectListRet.GetSubjectListRet getSubjectListRet : jsonGetSubjectListRet.data) {

                                    list_subject.addLast(getSubjectListRet);

                                }
                                adapter.notifyDataSetChanged();
                                lv_subject.onRefreshComplete();
                                lv_subject.setMode(PullToRefreshBase.Mode.BOTH);

                            }
                        });
                    }
                }

            }
        }).start();
        //   adapter.notifyDataSetChanged();
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java 2");

    }
}
