package co.docy.oldfriends.Fragments;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;

import co.docy.oldfriends.Activitis._ActivityAccount;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventHeartOneSecond;
import co.docy.oldfriends.EventBus.EventLogin;
import co.docy.oldfriends.EventBus.EventRegistSubmitted;
import co.docy.oldfriends.Messages.JsonLogin;
import co.docy.oldfriends.Messages.JsonResetPassword;
import co.docy.oldfriends.Messages.JsonResetPasswordRet;
import co.docy.oldfriends.Messages.JsonSmsResetPassword;
import co.docy.oldfriends.Messages.JsonSmsResetPasswordRet;
import co.docy.oldfriends.Messages.JsonWeixinLoginRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import de.greenrobot.event.EventBus;

public class _FragmentV4AccountReset extends FragmentBaseV4 implements View.OnFocusChangeListener {

    Button fragment_page_account_reset_next, fragment_page_account_reset_valid_button;
    EditText fragment_page_account_reset_phone, fragment_page_account_reset_code, fragment_page_account_reset_pwd;
    int count = 0;
    ImageView icon_phone, icon_password, icon_validation;

    public static _FragmentV4AccountReset newInstance() {
        _FragmentV4AccountReset f = new _FragmentV4AccountReset();

        return f;
    }

    public View mView;

    public void setVisibile(int visibile) {
        mView.setVisibility(visibile);
    }

    public void setY(float y) {
        mView.setY(y);
    }

    public float getY() {
        return mView.getY();
    }


    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_page_account_reset, null);
        mView = v;

        icon_password = (ImageView) v.findViewById(R.id.fragment_page_account_reset_pwd_icon);
        icon_phone = (ImageView) v.findViewById(R.id.fragment_page_account_reset_phone_icon);
        icon_validation = (ImageView) v.findViewById(R.id.fragment_page_account_reset_code_icon);

        fragment_page_account_reset_next = (Button) v.findViewById(R.id.fragment_page_account_reset_next);
        fragment_page_account_reset_valid_button = (Button) v.findViewById(R.id.fragment_page_account_reset_valid_button);
        fragment_page_account_reset_phone = (EditText) v.findViewById(R.id.fragment_page_account_reset_phone);
        fragment_page_account_reset_code = (EditText) v.findViewById(R.id.fragment_page_account_reset_code);
        fragment_page_account_reset_pwd = (EditText) v.findViewById(R.id.fragment_page_account_reset_pwd);

        fragment_page_account_reset_phone.setOnFocusChangeListener(this);
        fragment_page_account_reset_code.setOnFocusChangeListener(this);
        fragment_page_account_reset_pwd.setOnFocusChangeListener(this);

        fragment_page_account_reset_pwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (fragment_page_account_reset_pwd.getText().length() < 6) {
                    fragment_page_account_reset_next.setVisibility(View.GONE);
                } else {
                    fragment_page_account_reset_next.setVisibility(View.VISIBLE);
                }
            }
        });

        fragment_page_account_reset_valid_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smsResetPassword();
            }
        });
        fragment_page_account_reset_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitReset();
            }
        });


        return v;
    }


    private void submitReset() {

        final JsonResetPassword jsonResetPassword = new JsonResetPassword();

        jsonResetPassword.phone = fragment_page_account_reset_phone.getText().toString().trim();

        jsonResetPassword.code = fragment_page_account_reset_code.getText().toString().trim();

        jsonResetPassword.password = fragment_page_account_reset_pwd.getText().toString().trim();


        if (jsonResetPassword.phone.length() == 0) {
            notifyFailure(getResources().getString(R.string.null_phone));
            return;
        }
        if (jsonResetPassword.password.length() == 0) {
            notifyFailure(getResources().getString(R.string.passowrd_null_remind));
            return;
        }
        if (jsonResetPassword.code.length() == 0) {
            notifyFailure(getResources().getString(R.string.null_code));
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();
                String strJsonResetPassword = gson.toJson(jsonResetPassword);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_RESET_PASSWORD, strJsonResetPassword);

                JsonResetPasswordRet jsonResetPasswordRet = gson.fromJson(s, JsonResetPasswordRet.class);

                if (jsonResetPasswordRet != null) {

                    if (jsonResetPasswordRet.code == MyConfig.retSuccess()) {
                        MyConfig.usr_pwd = jsonResetPassword.password;
                        MyConfig.prefEditor.putString(MyConfig.PREF_USER_PWD, MyConfig.usr_pwd);
                        MyConfig.prefEditor.commit();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                              //  ((ActivityAccount) getActivity()).hideFragmentReset();
                                loginRequest();
                            }
                        });

                    } else {
//                        notifyFailure(jsonResetPasswordRet.message);
                        notifyFailure(MyConfig.getErrorMsg(""+jsonResetPasswordRet.code));
                    }
                }


            }
        }).start();


    }


    private void notifyFailure(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MyConfig.MyToast(0, getActivity(), message);
            }
        });

    }


    public void onEventMainThread(EventHeartOneSecond eventHeartOneSecond) {

        if (count > 0) {
            count--;
        }
        if (count > 0) {
            String format = getResources().getString(R.string.reset_password_time);
            String result = String.format(format, count);
            fragment_page_account_reset_valid_button.setText(result);
        } else {
            fragment_page_account_reset_valid_button.setText(getString(R.string.reset_password_get_identifying_code));
        }


    }

    public void onEventMainThread(EventRegistSubmitted eventRegistSubmitted) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((_ActivityAccount) getActivity()).hideFragmentReset();
            }
        });
    }


    private void smsResetPassword() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSmsResetPassword jsonSmsResetPassword = new JsonSmsResetPassword();
                jsonSmsResetPassword.phone = fragment_page_account_reset_phone.getText().toString();


                String jsonStr = gson.toJson(jsonSmsResetPassword, JsonSmsResetPassword.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SMS_RESET_PASSWORD, jsonStr);
                MyLog.d("", "apitest: send msg ret " + s);

                final JsonSmsResetPasswordRet jsonSmsResetPasswordRet = gson.fromJson(s, JsonSmsResetPasswordRet.class);
                if (jsonSmsResetPasswordRet != null) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (jsonSmsResetPasswordRet.code == MyConfig.retSuccess()) {
                                if (MyConfig.debug_autofill_validcode) {
                                    fragment_page_account_reset_code.setText(jsonSmsResetPasswordRet.data.code);
                                }

                                count = 60;
                                String format = getResources().getString(R.string.reset_password_time);
                                String result = String.format(format, count);
                                fragment_page_account_reset_valid_button.setText(result);
                            } else {
//                                MyConfig.MyToast(0, getActivity(), jsonSmsResetPasswordRet.message);
                                MyConfig.MyToast(0, getActivity(), MyConfig.getErrorMsg(""+jsonSmsResetPasswordRet.code));
                            }
                        }
                    });

                }

            }
        }).start();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.fragment_page_account_reset_phone:
                if (hasFocus) {
                    icon_phone.setBackgroundResource(R.drawable.login_icon_phone_focus_3x);
                } else {
                    icon_phone.setBackgroundResource(R.drawable.login_icon_phone_3x);
                }
                break;
            case R.id.fragment_page_account_reset_code:
                if (hasFocus) {
                    icon_validation.setBackgroundResource(R.drawable.login_icon_identify_focus_3x);
                } else {
                    icon_validation.setBackgroundResource(R.drawable.login_icon_identify_3x);

                }
                break;
            case R.id.fragment_page_account_reset_pwd:
                if (hasFocus) {
                    icon_password.setBackgroundResource(R.drawable.login_icon_password_focus);
                } else {
                    icon_password.setBackgroundResource(R.drawable.login_icon_password_3x);
                }
                break;
        }
    }

    public void loginRequest() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                String login_name_or_phone = fragment_page_account_reset_phone.getText().toString().trim();
                String pwd = fragment_page_account_reset_pwd.getText().toString().trim();
//                MyConfig.usr_name_or_phone_temply_for_login_or_reset = login_name_or_phone;
                JsonLogin jsonLogin = new JsonLogin();
                jsonLogin.account = login_name_or_phone;
                jsonLogin.password = pwd;
                jsonLogin.os = MyConfig.os_string;
                jsonLogin.uuid = MyConfig.uuid;
                Gson gson = new Gson();
                String j = gson.toJson(jsonLogin);

                String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LOGIN, j);

                final JsonWeixinLoginRet jsonLoginRet = gson.fromJson(s, JsonWeixinLoginRet.class);

                if (jsonLoginRet != null) {

                    if (jsonLoginRet.code != MyConfig.retSuccess()) {

                        /**
                         * 密码错了就清除保存的密码，并保持在这个位置
                         */
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //  et_pwd.setText("");
//                                MyConfig.MyToast(0, getActivity(), getString(R.string.error_login));
                                MyConfig.MyToast(0, getActivity(), MyConfig.getErrorMsg(""+jsonLoginRet.code));
                                MyLog.d("", "new error code: " + jsonLoginRet.code + " msg: " + MyConfig.getErrorMsg(""+jsonLoginRet.code));
                            }
                        });

                        MyConfig.usr_pwd = "";
                        MyConfig.saveUserDataToPreference(getActivity());

                    } else if (jsonLoginRet.data != null) {

                        MyConfig.logoutClear();

                        MyConfig.usr_login_ret_json = s;
                        MyConfig.jsonWeixinLoginRet = jsonLoginRet;
                        MyConfig.usr_id = jsonLoginRet.data.id;
                        MyConfig.usr_name = jsonLoginRet.data.name;//注意name要从这里取，因为login_name_or_phone也可能是phone
                        MyConfig.usr_nickname = jsonLoginRet.data.nickName;
                        MyConfig.usr_phone = jsonLoginRet.data.phone;
                        MyConfig.usr_pwd = pwd;
                        MyConfig.usr_token = jsonLoginRet.data.authToken;//常用变量就提取出来
                        MyConfig.usr_status = MyConfig.USR_STATUS_LOGIN;
                        //  MyConfig.usr_email = jsonLoginRet.data.email;
                        MyConfig.usr_phone = jsonLoginRet.data.phone;
                        if (jsonLoginRet.data.sex) {
                            MyConfig.usr_sex = MyConfig.USER_SEX_MAN;//男
                        } else {
                            MyConfig.usr_sex = MyConfig.USER_SEX_WOMAN;//女
                        }
                        MyConfig.usr_avatar = jsonLoginRet.data.avatar;
                        MyConfig.usr_origin = jsonLoginRet.data.origin;
                        if(jsonLoginRet.data.info != null){MyConfig.usr_name_changed = jsonLoginRet.data.info.changed;}


                        MyConfig.saveUserDataToPreference(getActivity());

                        finishWithSuccess();
                    }

                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            Toast.makeText(getActivity(), getString(R.string.net_msg_not_connected), Toast.LENGTH_SHORT).show();
                            MyConfig.MyToast(0, getActivity(), getString(R.string.net_msg_not_connected));
                        }
                    });
                }

            }
        }).start();
    }

    private void finishWithSuccess() {

        EventBus.getDefault().post(new EventLogin());

        getActivity().finish();
    }
}
