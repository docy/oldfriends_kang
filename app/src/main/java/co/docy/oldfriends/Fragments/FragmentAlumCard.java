package co.docy.oldfriends.Fragments;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;

public class FragmentAlumCard extends FragmentBase {

    private ImageView people_photo,code_photo;
    private TextView people_name,people_class,people_position,people_introduce;

    public static FragmentAlumCard newInstance() {
        FragmentAlumCard f = new FragmentAlumCard();

        return f;
    }
    Bitmap logo;
    Timer timer = new Timer();

    Bitmap code_bitmap;
     public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MyLog.d("", "viewpager get samplelistfragment");
        View v = inflater.inflate(R.layout.fragment_alum_card, null);

        people_name = (TextView) v.findViewById(R.id.alum_card_name);
        people_class =(TextView) v.findViewById(R.id.alum_card_class);
        people_introduce = (TextView) v.findViewById(R.id.alum_card_intro);
        people_position = (TextView) v.findViewById(R.id.alum_card_position);

        people_photo = (ImageView) v.findViewById(R.id.alum_card_photo);
        code_photo =(ImageView) v.findViewById(R.id.alum_scan_code);
        people_name.setText(MyConfig.usr_nickname);
        people_class.setText(MyConfig.usr_current_class_name);
        people_introduce.setText(MyConfig.usr_company);
        people_position.setText(MyConfig.usr_title);
        Picasso.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + MyConfig.usr_avatar)
//                .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(people_photo);
//        logo = BitmapFactory.decodeResource(getResources(),R.drawable.logo_two);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    if(MyConfig.jsonMadeInOrderFromServiceRet!=null&&MyConfig.jsonMadeInOrderFromServiceRet.data!=null) {
                        logo = Picasso.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + MyConfig.jsonMadeInOrderFromServiceRet.data.qrIcon)
                                .get();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                    code_create();
            }

        }, 1000, 600000);
           // code_create();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }
    public void code_create() /*throws WriterException*/ {

//    new Thread(new Runnable() {
//        @Override
//        public void run() {
        if(MyConfig.jsonCreateAulmCardRet!=null
                &&MyConfig.jsonCreateAulmCardRet.data!=null) {
            try {
                code_bitmap = createQRImage(MyConfig.jsonCreateAulmCardRet.data.qrData,
                        code_photo.getWidth(),
                        code_photo.getHeight(),
                        logo);
            } catch (WriterException e) {
                e.printStackTrace();
            }
        }
        //加载图片
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        code_photo.setImageBitmap(code_bitmap);

                    }
                });
      
//        }
//    }).start();
//        SimpleDateFormat timeformat = new SimpleDateFormat("yyyyMMddHHmmss");
//        subtime = (Long.parseLong(timeformat.format(new Date()))-1970)*1000;
//        StringBuffer buffer = new StringBuffer();
//        buffer.append(MyConfig.usr_name).append(MyConfig.usr_id).append(MyConfig.usr_current_class).append(subtime);

    }

    /**
     *
     *创建二维码
     */
    public  Bitmap createQRImage(String content, int widthPix, int heightPix, Bitmap logoBm)throws WriterException {
            if (content == null || "".equals(content)) {
                return null;
            }
            //配置参数
            Map<EncodeHintType, Object> hints = new HashMap<>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            //容错级别
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            // 图像数据转换，使用了矩阵转换
            BitMatrix bitMatrix = new QRCodeWriter().encode(content, BarcodeFormat.QR_CODE, widthPix, heightPix, hints);
              int width = bitMatrix.getWidth();
             int height = bitMatrix.getHeight();

        int[] pixels = new int[width * height];
            // 下面这里按照二维码的算法，逐个生成二维码的图片，
            // 两个for循环是图片横列扫描的结果
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * width + x] = 0xff000000;
                    } else {
                        pixels[y * width + x] = 0xffffffff;
                    }
                }
            }
        bitMatrix.clear();
            // 生成二维码图片的格式，使用ARGB_8888
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

            if (logoBm != null) {
                bitmap = addLogo(bitmap, logoBm);
            }
            //必须使用compress方法将bitmap保存到文件中再进行读取。直接返回的bitmap是没有任何压缩的，内存消耗巨大！
        return bitmap;
    }
    /**
     * 在二维码中间添加Logo图案
     */
    private  Bitmap addLogo(Bitmap src, Bitmap logo) {
        if (src == null) {
            return null;
        }
        if (logo == null) {
            return src;
        }
        //获取图片的宽高
        int srcWidth = src.getWidth();
        int srcHeight = src.getHeight();
        int logoWidth = logo.getWidth();
        int logoHeight = logo.getHeight();
        if (srcWidth == 0 || srcHeight == 0) {
            return null;
        }
        if (logoWidth == 0 || logoHeight == 0) {
            return src;
        }
        //logo大小为二维码整体大小的1/5
        float scaleFactor = srcWidth * 1.0f / 5 / logoWidth;
        Bitmap bitmap = Bitmap.createBitmap(srcWidth, srcHeight, Bitmap.Config.ARGB_8888);
        try {
            Canvas canvas = new Canvas(bitmap);
            canvas.drawBitmap(src, 0, 0, null);
            canvas.scale(scaleFactor, scaleFactor, srcWidth / 2, srcHeight / 2);
            canvas.drawBitmap(logo, (srcWidth - logoWidth) / 2, (srcHeight - logoHeight) / 2, null);
            canvas.save(Canvas.ALL_SAVE_FLAG);
            canvas.restore();
        } catch (Exception e) {
            bitmap = null;
            e.getStackTrace();
        }
        return bitmap;
    }




}
