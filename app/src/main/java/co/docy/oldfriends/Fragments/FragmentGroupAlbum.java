package co.docy.oldfriends.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.LinkedList;

import co.docy.oldfriends.Activitis.ActivityGallery;
import co.docy.oldfriends.Activitis.ActivityGroupUserChooserList;
import co.docy.oldfriends.Adapters.ListAdapterGroupAlbum;
import co.docy.oldfriends.Adapters.ListAdapterGroupUser;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.Messages.JsonGetGroupAlbumRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentGroupAlbum extends FragmentBase  {


    int groupId;
    String name;

    int num_in_row = 3;
    int get_more_limit = 15;

    private SwipyRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private GridLayoutManager mLayoutManager;
    private ListAdapterGroupAlbum mAdapter;
    private LinkedList<JsonGetGroupAlbumRet.GetGroupAlbumRet> list = new LinkedList<>();


    ListAdapterGroupUser.OnMoreClickedListener moreClickedListener = new ListAdapterGroupUser.OnMoreClickedListener() {
        @Override
        public void onMoreClicked() {
            MyConfig.activityGroupUserChooserListTitle = "小组成员";
            MyConfig.activityGroupUserChooserListList = ContactUniversal.convertListFromGetGroupUserRet(MyConfig.jsonGetGroupUserRet.data);
            startActivity(new Intent(getActivity(), ActivityGroupUserChooserList.class));
        }
    };



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_group_album, null);
        groupId = getArguments().getInt(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID);
        name = getArguments().getString(MyConfig.CONSTANT_PARAMETER_STRING_NAME);

        mSwipeRefreshLayout = (SwipyRefreshLayout) v.findViewById(R.id.fragment_group_album_swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
//                MyLog.d("MainActivity", "Refresh triggered at "
//                        + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
                getGroupAlbum(groupId, list.size()>0?list.getLast().createdAt.toString():null, get_more_limit);
            }
        });
        mSwipeRefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.fragment_group_album_list);

        mLayoutManager = new GridLayoutManager(getActivity(), num_in_row);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ListAdapterGroupAlbum(getActivity(), list, new ListAdapterGroupAlbum.OnThumbnailClick() {
            @Override
            public void onClick(int position) {

                MyConfig.genGroupAlbumFullSizeUrl(MyConfig.mainRoomRootNode, list, position);

                Intent intent = new Intent(getActivity(), ActivityGallery.class);
                intent.putExtra("mode", MyConfig.GALLERY_MODE_GROUP_ALBUM);
                intent.putExtra("from", getActivity().getClass().getName());
                startActivity(intent);


            }
        });

        mRecyclerView.setAdapter(mAdapter);

        getGroupAlbum(groupId, null, get_more_limit);

        return v;
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    public static FragmentGroupAlbum newInstance(int groupId, String name) {
        FragmentGroupAlbum f = new FragmentGroupAlbum();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
        args.putString(MyConfig.CONSTANT_PARAMETER_STRING_NAME, name);
        f.setArguments(args);
        return f;
    }


    private void getGroupAlbum(int id, String createdAt, int get_more_limit) {

        MyLog.d("","getGroupAlbum: id="+id+" cursor="+createdAt+" get_more_limit="+get_more_limit);

        Call<JsonGetGroupAlbumRet> repos = HttpTools.http_api_service().getGroupAlbum(id, createdAt, get_more_limit);
        repos.enqueue(new Callback<JsonGetGroupAlbumRet>() {
            @Override
            public void onResponse(Call<JsonGetGroupAlbumRet> call, Response<JsonGetGroupAlbumRet> response) {

                mSwipeRefreshLayout.setRefreshing(false);

                // Get result Repo from response.body()

                MyLog.d("", "getGroupAlbum: code() " + response.code());
                MyLog.d("", "getGroupAlbum: message() " + response.message());

                if (response.code() == 200) {

                    MyConfig.jsonGetGroupAlbumRet = response.body();

//                    MyLog.d("", "retrofit: body() 1 " + response.body().toString());
//                    MyLog.d("", "retrofit: body() 2 " + response.body().code);
//                    MyLog.d("", "retrofit: body() 3 " + response.body().message);

                    if (response.body().data != null) {


                        MyLog.d("","getGroupAlbum: "+MyConfig.jsonGetGroupAlbumRet.data.toString());


                        if(list.size() == 0){

                        }else{  //扔掉重复获取的第一张

//                            if(list.getLast().createdAt.equals(MyConfig.jsonGetGroupAlbumRet.data.getFirst().createdAt)){
                                list.removeLast();
//                            }

                        }

                        list.addAll(list.size(), MyConfig.jsonGetGroupAlbumRet.data);

                        mAdapter.notifyDataSetChanged();

                    }
                }

            }

            @Override
            public void onFailure(Call<JsonGetGroupAlbumRet> call, Throwable t) {
                MyLog.d("", "retrofit: onFailure " + t.getMessage());
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

//    private void showGroupUsers() {
//        LinkedList<JsonGetGroupUserRet.GetGroupUserRet> list_temp = new LinkedList<>();
//
////        mAdapter.moreClickedListener = moreClickedListener;
//        Resources r = getActivity().getResources();
//        int height_pix = (int) (
//                r.getDimension(R.dimen.avatar_height)
//                        + r.getDimension(R.dimen.setting_user_avatar_textview_height)
//                        + r.getDimension(R.dimen.setting_user_avatar_textview_magin_bottom)
//        );
//        mRecyclerView.getLayoutParams().height = heightRow * height_pix + (int) (r.getDimension(R.dimen.setting_bottom_margin) + r.getDimension(R.dimen.setting_top_margin));
//
//        mRecyclerView.setAdapter(mAdapter);
//    }

}
