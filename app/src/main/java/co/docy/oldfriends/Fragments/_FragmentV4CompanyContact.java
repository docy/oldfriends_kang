package co.docy.oldfriends.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.LinkedList;

import co.docy.oldfriends.Activitis._ActivityClassContact;
import co.docy.oldfriends.Adapters.ListAdapterUserList;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.EventBus.EventContactDownloadList;
import co.docy.oldfriends.EventBus.EventGroupListUpdateUi;
import co.docy.oldfriends.EventBus.EventContactDownloadPlease;
import co.docy.oldfriends.EventBus.EventUserStatusUpdate;
import co.docy.oldfriends.Messages.JsonUserGroupsRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.CustomFastScrollView;
import de.greenrobot.event.EventBus;


public class _FragmentV4CompanyContact extends FragmentBaseV4 {

    /**
     * 本fragment提供对公司某一个成员的各种操作，其功能与FragmentContactMultiChoose很不同，故不用整合
     * <p/>
     * 可能的操作
     * 点击进入私信
     * 查看个人信息
     * 设置标签、备注
     */
    LinkedList<JsonUserGroupsRet.UserGroupsRet> userGroupsRets;//要同时显示班级信息

    ListView contact_xjt_lv;
    // private ListView listView;
    private CustomFastScrollView fastScrollView;
    FrameLayout contact_layout;
    private LinearLayout list_null_background_layout;
    ListAdapterUserList listAdapterCompanyUserList;
    float view_y_origin;
    private MyConfig.IF_AfterHttpRequest IFAfterHttpRequest = new MyConfig.IF_AfterHttpRequest() {
        @Override
        public void doAfterHttpRequest() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showJsonGetCompanyUserListRet();
                }
            });
        }
    };

    private ListAdapterUserList.OnClickedListener onClickedListener_person = new ListAdapterUserList.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
            MyConfig.gotoUserInfo(getActivity(), MyConfig.jsonGetCompanyUserListRet.data.get(position).id);
        }
    };

    private ListAdapterUserList.OnClickedListener onClickedListener_class = new ListAdapterUserList.OnClickedListener() {
        @Override
        public void OnClicked(int position) {
            Intent intent = new Intent(getActivity(), _ActivityClassContact.class);
            intent.putExtra("class_id", userGroupsRets.get(position).id);
            startActivity(intent);
        }
    };


    private void showJsonGetCompanyUserListRet() {
//        userGroupsRets = MyConfig.filterClassFromGroupList();

        listAdapterCompanyUserList = new ListAdapterUserList(getActivity(),
                ContactUniversal.convertListFromGetCompanyUserListRet(MyConfig.jsonGetCompanyUserListRet.data), userGroupsRets,
                onClickedListener_person, onClickedListener_class);
        contact_xjt_lv.setAdapter(listAdapterCompanyUserList);
        int count = MyConfig.jsonGetCompanyUserListRet.data.size() < 2 ? 0:MyConfig.jsonGetCompanyUserListRet.data.size();
        String format = getActivity().getString(R.string.contact_num);
        String result = String.format(format,count);
        contact_count.setText(result);
        if (MyConfig.jsonGetCompanyUserListRet.data.size() < 2) {
            list_null_background_layout.setVisibility(View.VISIBLE);
        } else {
            list_null_background_layout.setVisibility(View.GONE);
        }

//        lv_contact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                MyConfig.gotoUserInfo(getActivity(), MyConfig.jsonGetCompanyUserListRet.data.get(position).id);
//            }
//        });
    }

    private TextView contact_count;

    public static _FragmentV4CompanyContact newInstance(String s) {
        _FragmentV4CompanyContact f = new _FragmentV4CompanyContact();
        MyLog.d("", "read contact newInstance ");

        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString("index", s);
        f.setArguments(args);

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MyLog.d("", "read contact onCreateView");
        View v = inflater.inflate(R.layout.fragment_contact_with_scrollbutton, null);
        contact_xjt_lv = (ListView) v.findViewById(android.R.id.list);
        fastScrollView = (CustomFastScrollView) v.findViewById(R.id.fast_scroll_view);
        contact_count = (TextView) v.findViewById(R.id.contact_count);
        list_null_background_layout = (LinearLayout) v.findViewById(R.id.list_null_background_layout);

        return v;
    }



    @Override
    public void onResume() {
        super.onResume();

        MyLog.d("", "company change flow: USR_STATUS FragmentContact onResume" + MyConfig.usr_status);
        super.onResume();
        if (MyConfig.netConnectCheck() <= 0) {
            MyConfig.MyToast(0, getActivity(), getString(R.string.net_connect_error));
            return;
        }

        getCompanyContacts();
    }


    private void getCompanyContacts() {
        MyLog.d("", "company change flow: USR_STATUS FragmentContact getCompanyContacts a " + MyConfig.usr_status);
        if (MyConfig.usr_status < MyConfig.USR_STATUS_HAVE_CURRENT_COMPANY) {//要先设置当前公司，然后才有公司通讯录的显示
            return;
        }
        MyLog.d("", "company change flow: USR_STATUS FragmentContact getCompanyContacts b " + MyConfig.usr_status);
        if (MyConfig.jsonGetCurrentCompanyRet == null) {
            return;
        }
        MyLog.d("", "company change flow: USR_STATUS FragmentContact getCompanyContacts c " + MyConfig.usr_status);


        MyConfig.getCurrentYearUserAddressBooks(MyConfig.jsonGetCurrentCompanyRet.data.id, IFAfterHttpRequest);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyLog.d("", "company change flow: USR_STATUS FragmentContact onCreate" + MyConfig.usr_status);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void onEventMainThread(EventUserStatusUpdate eventUserStatusUpdate) {
        MyLog.d("", "company change flow: onEventMainThread in FragmentSettingList.java: EventLogin");

        getCompanyContacts();
    }

    public void onEventMainThread(EventGroupListUpdateUi eventGroupListUpdateUi) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java");

        getCompanyContacts();
    }


    public void onEventMainThread(EventContactDownloadPlease eventContactDownloadPlease) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java");

        EventBus.getDefault().post(new EventContactDownloadList(MyConfig.jsonGetCompanyUserListRet.data, EventContactDownloadList.DOWNLOAD_BEGIN));
    }


}
