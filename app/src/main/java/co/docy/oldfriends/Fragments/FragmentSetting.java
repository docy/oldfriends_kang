package co.docy.oldfriends.Fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.docy.oldfriends.Activitis.ActivitySettingAbout;
import co.docy.oldfriends.Activitis.ActivitySettingFeedback;
import co.docy.oldfriends.Activitis.ActivitySettingMessage;
import co.docy.oldfriends.Activitis.ActivitySettingResetPasswordTongChuang;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/4/21.
 */
public class FragmentSetting extends FragmentBase implements View.OnClickListener {
    private LinearLayout linear_msg,linear_about,linear_modify,linear_feedback;
    private LinearLayout linear_version_update;
    private TextView fragment_setting_version_name;
    private ImageView fragment_setting_enable_update;
    private TextView fragment_setting_version_desc;

    public static FragmentSetting newInstance() {
        FragmentSetting f = new FragmentSetting();
        return f;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting_context,null);
        linear_msg =(LinearLayout) view.findViewById(R.id.fragment_setting_context_msg);
        linear_msg.setOnClickListener(this);
        linear_about =(LinearLayout) view.findViewById(R.id.fragment_setting_context_about);
        linear_about.setOnClickListener(this);

        linear_version_update =(LinearLayout) view.findViewById(R.id.fragment_setting_version_update);
        fragment_setting_version_name =(TextView) view.findViewById(R.id.fragment_setting_version_name);
        fragment_setting_version_desc =(TextView) view.findViewById(R.id.fragment_setting_version_desc);
        fragment_setting_enable_update =(ImageView) view.findViewById(R.id.fragment_setting_enable_update);


        linear_modify =(LinearLayout) view.findViewById(R.id.fragment_setting_context_modify);
        linear_modify.setOnClickListener(this);
        linear_feedback =(LinearLayout) view.findViewById(R.id.fragment_setting_context_feedback);
        linear_feedback.setOnClickListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
    }

    /**
     * 初始化数据，主要显示当前版本
     */
    private void initData() {

        /**这个方法请求了服务器得到最新版本号，并保存到MyConfig中，还得到了该应用程序的版本号，保存到MyConfig中*/
       MyConfig.appVersionCheck();

        fragment_setting_version_name.setText(MyConfig.appVersion_from_server_current.versionName+"");
        /**若有新版本则显示版本号，且注册点击监听事件，若没有新版本则不显示版本号，且取消点击监听事件。*/
        if(MyConfig.appVersion_from_server_current.versionCode >  MyConfig.appVersion_from_local.versionCode) {
            linear_version_update.setOnClickListener(this);
            fragment_setting_version_name.setBackgroundResource(R.drawable.button_new_version_code);
            fragment_setting_enable_update.setVisibility(View.VISIBLE);
            fragment_setting_version_desc.setText("新版更新");
        }else{
            linear_version_update.setOnClickListener(null);
            fragment_setting_version_name.setBackgroundResource(android.R.color.transparent);
            fragment_setting_enable_update.setVisibility(View.GONE);
            fragment_setting_version_desc.setText("当前为最新版本");
        }

    }

    public void onClick(View v){
        switch(v.getId()){
            case R.id.fragment_setting_context_msg:
                Intent intent_msg = new Intent(getActivity(),ActivitySettingMessage.class);
                startActivity(intent_msg);
                break;

            case R.id.fragment_setting_context_about:
                Intent intent_about = new Intent(getActivity(),ActivitySettingAbout.class);
                startActivity(intent_about);
                break;
            case R.id.fragment_setting_version_update:
                MyConfig.updateAppDownloadUrl(new MyConfig.CallBack_IfSuccess() {
                    @Override
                    public void ifSuccess() {
                        Uri uri = Uri.parse(MyConfig.appDownloadUrl);
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    }
                    @Override
                    public void ifFailed(){

                    }

                });
                break;
            case R.id.fragment_setting_context_modify:
                Intent intent_modify = new Intent (getActivity(),ActivitySettingResetPasswordTongChuang.class);
                startActivity(intent_modify);
                break;
            case R.id.fragment_setting_context_feedback:
                Intent intent_secret = new Intent(getActivity(),ActivitySettingFeedback.class);
                startActivity(intent_secret);
                break;
        }
    }
}
