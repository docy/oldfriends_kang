package co.docy.oldfriends.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.DayCourse;
import co.docy.oldfriends.R;

public class FragmentDayCourses extends FragmentBase {

    DayCourse dayCourse;


    public static FragmentDayCourses newInstance() {
        FragmentDayCourses f = new FragmentDayCourses();

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MyLog.d("", "viewpager get samplelistfragment");
        View v = inflater.inflate(R.layout.fragment_day_course, null);

        dayCourse = MyConfig.tempDayCourse;


        return v;
    }


}
