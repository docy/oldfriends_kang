package co.docy.oldfriends.Fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.HashMap;
import java.util.LinkedList;

import co.docy.oldfriends.Activitis.ActivityMainGroup;
import co.docy.oldfriends.Activitis.ActivitySubGroup;
import co.docy.oldfriends.Adapters.ListAdapterSubjectList;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventFavoriteChanged;
import co.docy.oldfriends.Messages.JsonGetMsgTopicCommentRet;
import co.docy.oldfriends.Messages.JsonGetSubjectList;
import co.docy.oldfriends.Messages.JsonGetSubjectListRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

public class _FragmentV4FavoriteList extends FragmentBaseV4 {

    int msgPageCount = 0;

    ListAdapterSubjectList adapter;
    public LinkedList<JsonGetSubjectListRet.GetSubjectListRet> list_favorite = new LinkedList<>();
    PullToRefreshListView lv_subject;
    private LinearLayout fragment_subject_layout,list_null_background_layout;

    public static _FragmentV4FavoriteList newInstance() {
        _FragmentV4FavoriteList f = new _FragmentV4FavoriteList();

        return f;
    }

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_subject_list, null);
        fragment_subject_layout = (LinearLayout) v.findViewById(R.id.fragment_subject_layout);
        list_null_background_layout= (LinearLayout) v.findViewById(R.id.list_null_background_layout);
        lv_subject = (PullToRefreshListView) v.findViewById(R.id.fragment_subject_list);
        lv_subject.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                moreFavoriteList();
            }
        });

        adapter = new ListAdapterSubjectList(getActivity(), list_favorite, onClickedListener, onLongClickedListener,false);
        adapter.onCheckedChangListener = onCheckedChangListener;
        lv_subject.setAdapter(adapter);
        return v;
    }
    ListAdapterSubjectList.OnCheckedChangListener onCheckedChangListener = new ListAdapterSubjectList.OnCheckedChangListener() {
        @Override
        public void checkChange(boolean isChecked, int position) {

        }
    };

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

//        MyLog.d(MyConfig.TAG_LIFECYCLE, this.getClass().getName() + " savedInstanceState: putSerializable " + list.size());
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        msgPageCount = 0;
        list_favorite.clear();
        moreFavoriteList();

    }

    public void moreFavoriteList() {

        msgPageCount++;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonGetSubjectList jsonGetSubjectList = new JsonGetSubjectList();
                jsonGetSubjectList.accessToken = MyConfig.usr_token;
                jsonGetSubjectList.type = 0;

                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("page", "" + msgPageCount);
                final String s = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_FAVORITY_LIST, hm, MyConfig.usr_token);
                MyLog.d("", "httptool favorite: moreFavoriteList ret " + s);

                if (s != null) {
                    final JsonGetSubjectListRet jsonGetSubjectListRet = gson.fromJson(s, JsonGetSubjectListRet.class);

                    if (jsonGetSubjectListRet != null && jsonGetSubjectListRet.code==MyConfig.retSuccess()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                for (JsonGetSubjectListRet.GetSubjectListRet getSubjectListRet : jsonGetSubjectListRet.data) {
                                    list_favorite.addLast(getSubjectListRet);
                                }
                                adapter.notifyDataSetChanged();
                                if (list_favorite.size() < 1) {
                                    list_null_background_layout.setVisibility(View.VISIBLE);
                                    //  lv_subject.getRefreshableView().setBackgroundResource(R.drawable.favourite_null_bitmap);
                                } else {
                                    //  lv_subject.getRefreshableView().setBackgroundColor(getResources().getColor(R.color.White));
                                    list_null_background_layout.setVisibility(View.GONE);
                                }
                                lv_subject.onRefreshComplete();


                            }
                        });
                    }
                }

            }
        }).start();
    }


    public void onEventMainThread(EventFavoriteChanged eventFavoriteChanged) {

        /**
         * todo
         * 关注列表有变，可能增减，这里未想清楚怎样处理
         */
        msgPageCount = 0;
        list_favorite.clear();
        moreFavoriteList();
    }

    public void onEventMainThread(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet json_socket_topicComment) {

        subjectListUpdateFromSocket(json_socket_topicComment);
    }

    public void subjectListUpdateFromSocket(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet json_socket_topicComment) {


        for (JsonGetSubjectListRet.GetSubjectListRet getSubjectListRet : list_favorite) {

            if (json_socket_topicComment.info.topicId == getSubjectListRet.info.topicId) {

                /**
                 * 得到scoket更新，这里创建一个假的last comment
                 */

                MyConfig.subjectListUpdateLastComment(json_socket_topicComment, getSubjectListRet);

                adapter.notifyDataSetChanged();

                break;
            }

        }
    }
    private void showRemindDialog(final int position) {
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_two_choice_layout,null);
        final Dialog setDialog = new Dialog(getActivity(), R.style.DialogStyle);
        setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialog.setCancelable(false);
        setDialog.getWindow().setContentView(dialogView);
        WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
        lp.width = MyConfig.getWidthByScreenPercent(80);
        setDialog.getWindow().setAttributes(lp);
        TextView groupText = (TextView) dialogView.findViewById(R.id.group_into );
        TextView cancleText = (TextView) dialogView.findViewById(R.id.cancle_favour);

        groupText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //进入主聊天室
                Intent intent_main = new Intent(getActivity(), ActivityMainGroup.class);
                intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, list_favorite.get(position).groupId);
                intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, list_favorite.get(position).title);
                intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, list_favorite.get(position).desc);
                startActivity(intent_main);
                setDialog.cancel();
            }
        });
        cancleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //取消关注
                MyConfig.addFavorite(list_favorite.get(position).info.topicId, false);
                setDialog.cancel();
            }
        });
        setDialog.setCanceledOnTouchOutside(true);
        setDialog.show();
    }

//    private void popMenu(final int position) {
//
//        new DialogFragment() {
//            @Override
//            public Dialog onCreateDialog(Bundle savedInstanceState) {
//                // Use the Builder class for convenient dialog construction
//                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                String title;
//
//                builder.setTitle("选择操作")
//                        .setItems(R.array.favorite_operation, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                switch (which) {
//                                    case 0:
//                                        //进入主聊天室
//                                        Intent intent_main = new Intent(getActivity(), ActivityMainGroup.class);
//                                        intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, list_favorite.get(position).groupId);
//                                        intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, list_favorite.get(position).title);
//                                        intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, list_favorite.get(position).desc);
//                                        startActivity(intent_main);
//                                        break;
//                                    case 1:
//                                        //取消关注
//                                        MyConfig.addFavorite(list_favorite.get(position).info.topicId, false);
//                                        break;
//                                }
//
//                            }
//                        })
//                        .setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                // User cancelled the dialog
//                            }
//                        });
//                // Create the AlertDialog object and return it
//                return builder.create();
//            }
//        }.show(getFragmentManager(), "");
//    }

    ListAdapterSubjectList.OnLongClickedListener onLongClickedListener = new ListAdapterSubjectList.OnLongClickedListener() {
        @Override
        public void OnLongClicked(int position) {
         //   popMenu(position);
            showRemindDialog(position);
        }
    };

    ListAdapterSubjectList.OnClickedListener onClickedListener = new ListAdapterSubjectList.OnClickedListener() {
        @Override
        public void OnClicked(int position) {

            if(position < list_favorite.size()) {//保险起见，但没这个必要

                JsonGetSubjectListRet.GetSubjectListRet getSubjectListRet = list_favorite.get(position);


                Intent intent = new Intent(getActivity(), ActivitySubGroup.class);
                intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, getSubjectListRet.groupId);
                intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_TOPICID, getSubjectListRet.info.topicId);
//                intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_TITLE, getSubjectListRet.title);
//                intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, getSubjectListRet.desc);
                startActivity(intent);
            }
        }
    };

}
