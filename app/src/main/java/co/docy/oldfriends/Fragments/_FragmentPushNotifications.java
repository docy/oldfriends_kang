package co.docy.oldfriends.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.LinkedList;

import co.docy.oldfriends.Adapters.ListAdapterNotification;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.EventBus.EventPushMsg;
import co.docy.oldfriends.Push.PushMsg;
import co.docy.oldfriends.R;

public class _FragmentPushNotifications extends FragmentBase {

    ListAdapterNotification listAdapterNotification;
    LinkedList<PushMsg> list = new LinkedList<>();
    ListView fragment_notification_list;

    public static _FragmentPushNotifications newInstance(String s) {
        _FragmentPushNotifications f = new _FragmentPushNotifications();

        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString("index", s);
        f.setArguments(args);

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MyLog.d("", "viewpager get samplelistfragment");
        View v = inflater.inflate(R.layout.fragment_push_notifications, null);

        list.add(new PushMsg(MyConfig.PUSH_onMessage, "a", "b", "c"));
        list.add(new PushMsg(MyConfig.PUSH_onMessage, "d", "e", "f"));


        fragment_notification_list = (ListView)v.findViewById(R.id.fragment_notification_list);

        listAdapterNotification = new ListAdapterNotification(getActivity(), list, null, null);
        fragment_notification_list.setAdapter(listAdapterNotification);

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(EventPushMsg eventPushMsg) {

        MyLog.d("","baidu push onEventMainThread "+ eventPushMsg.pushMsg.s + " " + eventPushMsg.pushMsg.type);

        PushMsg pushMsg = eventPushMsg.pushMsg;

        if(pushMsg.type == MyConfig.PUSH_onMessage){
            MyLog.d("","baidu push onEventMainThread list add pushMsg");
            list.add(pushMsg);
            listAdapterNotification.notifyDataSetChanged();
        }

    }

}
