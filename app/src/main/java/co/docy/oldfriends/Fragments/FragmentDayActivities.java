package co.docy.oldfriends.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.LinkedList;

import co.docy.oldfriends.Activitis.ActivityClassEvaluate;
import co.docy.oldfriends.Activitis.ActivityCoursewareDownLoad;
import co.docy.oldfriends.Activitis.ActivityGroupUserChooserList;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.DataClass.DayActivity;
import co.docy.oldfriends.Messages.JsonGetClassDetailRet;
import co.docy.oldfriends.Messages.JsonGetPerDayCourseRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationCircle;

public class FragmentDayActivities extends FragmentBase {


    DayActivity dayActivity;
    JsonGetPerDayCourseRet.PerDayCourseInfo perDayCourseInfo;
    private TextView day_activity_class_teacher;
    private TextView day_activity_class_class;
    private TextView day_activity_class_date;
    private TextView day_activity_class_time;
    private TextView day_activity_class_class_room;

    /**日程类型*/
    private final int CATEGORY_CLASS = 1;
    private final int CATEGORY_ACTION = 2;
    private TextView day_activity_action_organizer;
    private TextView day_activity_action_date;
    private TextView day_activity_action_time;
    private TextView day_activity_action_location;
    private TextView day_activity_action_phone;
    private LinearLayout ll_day_activity_action_customers;

    private JsonGetClassDetailRet jsonGetClassDetailRetRet;
    private TextView day_activity_class_class_introduction;
    private TextView day_activity_action_introduction;
    private LinearLayout ll_day_activity_class_download;
    private LinearLayout ll_day_activity_class_evaluate;

    public static FragmentDayActivities newInstance(String s) {

        FragmentDayActivities f = new FragmentDayActivities();

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MyLog.d("", "viewpager get samplelistfragment");
        perDayCourseInfo = MyConfig.tempPerDayCourseInfo;
        View v ;

        switch (perDayCourseInfo.type){
            case CATEGORY_CLASS:
                return inflaterClass();

            case CATEGORY_ACTION:
                return inflaterAction();
        }

        return null ;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadNetData();
    }

    /**
     * 加载课程页面
     * @return
     */
    private View inflaterClass() {
       View  v = View.inflate(getActivity(),R.layout.fragment_day_activity_class, null);
        initClassView(v);
        initClassListener();
        initClassData();
        return v;
    }




    /**
     * 加载活动页面
     * @return
     */
    private View inflaterAction() {
        View  v = View.inflate(getActivity(),R.layout.fragment_day_activity_action, null);
        initActionView(v);
        initActionData();

        return v;
    }

    /**
     * 请求网络，获取详情数据
     */
    private void loadNetData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                /**获取参与人数据*/
                String url = MyConfig.getApiDomain_HaveSlash_ApiV2() + "courses/"+perDayCourseInfo.scheduleId;
                final String s = new HttpTools().httpURLConnGet(url,MyConfig.usr_token);


                jsonGetClassDetailRetRet = gson.fromJson(s, JsonGetClassDetailRet.class);
                
                MyConfig.classDetailRetRet = jsonGetClassDetailRetRet;

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (perDayCourseInfo.type){
                            case CATEGORY_CLASS:
                                //showClassNetData(jsonGetActionCustomerRet.data.files);
                                break;

                            case CATEGORY_ACTION:
                                showActionNetData(jsonGetClassDetailRetRet.data.users);
                                break;
                        }

                    }
                });
            }
        }).start();
    }



    /**
     * 在活动页面上显示从网络上获取 的数据
     * @param usersInfos
     */
    private void showActionNetData(LinkedList<JsonGetClassDetailRet.ActionCustomerUsersInfo> usersInfos) {
        day_activity_action_phone.setText(jsonGetClassDetailRetRet.data.phone);

        if(usersInfos.size()>5) {
            for (int i = 0; i < 5; i++) {
                showCustomerIcon(usersInfos,i);
            }
            showMoreCustomerIcon();

        }else {
            for (int i = 0; i < usersInfos.size(); i++) {
                showCustomerIcon(usersInfos,i);
            }
        }
    }

    /**
     * 显示活动参与人头像
     */
    private void showCustomerIcon(final LinkedList<JsonGetClassDetailRet.ActionCustomerUsersInfo> usersInfos, final int i) {
        ImageView customerIcon = new ImageView(getActivity());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MyConfig.dp2Px(getActivity(),40), MyConfig.dp2Px(getActivity(),40));
        layoutParams.rightMargin = MyConfig.dp2Px(getActivity(),10);

        Picasso.with(getActivity()).load("" + MyConfig.getApiDomain_NoSlash_GetResources() + usersInfos.get(i).avatar)
               // .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
               // .transform(new RoundedTransformation(MyConfig.dp2Px(25), 0))
                .transform(new PicassoTransformationCircle())
                .into(customerIcon);

        customerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.gotoUserInfo(getActivity(), usersInfos.get(i).id);
            }
        });

        ll_day_activity_action_customers.addView(customerIcon, layoutParams);
    }
/**
     * 显示活动参与人头像扩展图标
     */
    private void showMoreCustomerIcon() {
        ImageView moreCustomerIcon = new ImageView(getActivity());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MyConfig.dp2Px(getActivity(),20), MyConfig.dp2Px(getActivity(),20));
        layoutParams.rightMargin = MyConfig.dp2Px(getActivity(),10);
        layoutParams.leftMargin = MyConfig.dp2Px(getActivity(),10);
        layoutParams.gravity = Gravity.CENTER_VERTICAL;

        moreCustomerIcon.setImageResource(R.drawable.day_activity_arrow_right_3x);
        moreCustomerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.activityGroupUserChooserListTitle = "活动参与人员";
                JsonGetClassDetailRet.ActionCustomerData data = jsonGetClassDetailRetRet.data;

                MyConfig.activityGroupUserChooserListList = ContactUniversal.convertListFromGetActionCustomerRet(jsonGetClassDetailRetRet.data.users);
                startActivity(new Intent(getActivity(), ActivityGroupUserChooserList.class));
            }
        });

        ll_day_activity_action_customers.addView(moreCustomerIcon, layoutParams);
    }



    /**
     * 初始化活动页面控件
     */
    private void initActionView(View view) {
        /**下面是初始化容器控件*/
        ll_day_activity_action_customers = (LinearLayout) view.findViewById(R.id.ll_day_activity_action_customers);

        /**下面是初始化子控件*/
        day_activity_action_organizer = (TextView) view.findViewById(R.id.day_activity_action_organizer);
        day_activity_action_date = (TextView) view.findViewById(R.id.day_activity_action_date);
        day_activity_action_time = (TextView) view.findViewById(R.id.day_activity_action_time);
        day_activity_action_location = (TextView) view.findViewById(R.id.day_activity_action_location);
        day_activity_action_phone = (TextView) view.findViewById(R.id.day_activity_action_phone);
        day_activity_action_introduction = (TextView) view.findViewById(R.id.day_activity_action_introduction);

    }
    /**
     * 初始化活动页面数据
     */
    private void initActionData() {
        day_activity_action_organizer.setText(perDayCourseInfo.teacher);

        /**设置日期*/
        Date beginTime = MyConfig.UTCString2Date(perDayCourseInfo.beginTime);
        day_activity_action_date.setText(MyConfig.Date2GroupInfoTimehorizon((beginTime)));

        /**设置时间*/
        String startTime = MyConfig.Date2GroupInfoTimeToday(beginTime);
        beginTime.setHours(beginTime.getHours()+perDayCourseInfo.duration);
        String endTime = MyConfig.Date2GroupInfoTimeToday(beginTime);
        day_activity_action_time.setText(startTime+"-"+endTime);

        day_activity_action_location.setText(perDayCourseInfo.classRoom);
        day_activity_action_introduction.setText(MyConfig.showDataByAnalyse(perDayCourseInfo.intro));
    }






    /**
     * 初始化课程页面控件
     */
    private void initClassView(View view) {
        /**下面是初始化容器控件*/
        ll_day_activity_class_download = (LinearLayout) view.findViewById(R.id.ll_day_activity_class_download);
        ll_day_activity_class_evaluate = (LinearLayout) view.findViewById(R.id.ll_day_activity_class_evaluate);

        /**下面是初始化子控件*/
        day_activity_class_teacher = (TextView) view.findViewById(R.id.day_activity_class_teacher);
        day_activity_class_class = (TextView) view.findViewById(R.id.day_activity_class_class);
        day_activity_class_date = (TextView) view.findViewById(R.id.day_activity_class_date);
        day_activity_class_time = (TextView) view.findViewById(R.id.day_activity_class_time);
        day_activity_class_class_room = (TextView) view.findViewById(R.id.day_activity_class_class_room);
        day_activity_class_class_introduction = (TextView) view.findViewById(R.id.day_activity_class_class_introduction);

    }

    /**
     * 注册课程控件的监听器
     */
    private void initClassListener() {

        /**课件下载*/
        ll_day_activity_class_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ActivityCoursewareDownLoad.class));
            }
        });

        /**课程评价*/
        ll_day_activity_class_evaluate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ActivityClassEvaluate.class);
                intent.putExtra(MyConfig.WEB_VIEW_URL_CATEGORY,MyConfig.WEB_VIEW_URL_CATEGORY_CLASS_EVALUATE);
                startActivity(intent);
            }
        });
    }

    /**
     * 初始化课程页面数据
     */
    private void initClassData() {

        //lookCodeAboutDate();
        day_activity_class_teacher.setText(perDayCourseInfo.teacher);
        day_activity_class_class.setText(perDayCourseInfo.group);

        /**设置日期*/
        Date beginTime = MyConfig.UTCString2Date(perDayCourseInfo.beginTime);
        day_activity_class_date.setText(MyConfig.Date2GroupInfoTimehorizon((beginTime)));

        /**设置时间*/
        String startTime = MyConfig.Date2GroupInfoTimeToday(beginTime);
        beginTime.setHours(beginTime.getHours()+perDayCourseInfo.duration);
        String endTime = MyConfig.Date2GroupInfoTimeToday(beginTime);
        day_activity_class_time.setText(startTime+"-"+endTime);

        day_activity_class_class_room.setText(perDayCourseInfo.classRoom);

        day_activity_class_class_introduction.setText(MyConfig.showDataByAnalyse(perDayCourseInfo.intro));

    }



    /**
     * 测试关于时间封装的方法，
     */
    private void lookCodeAboutDate() {
        MyLog.d("kwwl","UTCdate = "+perDayCourseInfo.beginTime);
        /**打印结果是：UTCdate = 2016-04-10T06:00:00.000Z*/

        MyLog.d("kwwl","date = "+ MyConfig.UTCString2Date(perDayCourseInfo.beginTime));
        /**打印结果是：date = Sun Apr 10 14:00:00 GMT+08:00 2016*/

        String date = MyConfig.Date2GroupInfoTime(MyConfig.UTCString2Date(perDayCourseInfo.beginTime));
        MyLog.d("kwwl","year+month+day = "+date);
        /**打印结果是： year+month+day = 16/4/10*/

        String datehorizon = MyConfig.Date2GroupInfoTimehorizon(MyConfig.UTCString2Date(perDayCourseInfo.beginTime));
        MyLog.d("kwwl","year+month+day+horizon = "+datehorizon);
        /**打印结果是： year+month+day+horizon = 2016-4-10*/

        String time = MyConfig.Date2GroupInfoTimeToday(MyConfig.UTCString2Date(perDayCourseInfo.beginTime));
        MyLog.d("kwwl","hour+minute+second = "+time);
        /**打印结果是： hour+minute+second = 02:00*/

        Date beginTime = MyConfig.UTCString2Date(perDayCourseInfo.beginTime);
        beginTime.setHours(beginTime.getHours()+perDayCourseInfo.duration);
        MyLog.d("kwwl","beginTime = "+MyConfig.Date2GroupInfoTimeToday(beginTime));
        /**打印结果是： beginTime= 05:00*/}



}
