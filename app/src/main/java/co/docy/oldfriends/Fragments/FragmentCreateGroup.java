package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import co.docy.oldfriends.Activitis.ActivityCreateGroupPeople;
import co.docy.oldfriends.Activitis.ActivityMainGroup;
import co.docy.oldfriends.Adapters.ListAdapterAddGroupMember;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.EventBus.EventCreateGroupSuccess;
import co.docy.oldfriends.Messages.JsonCreateGroup;
import co.docy.oldfriends.Messages.JsonCreateGroupRet;
import co.docy.oldfriends.Messages.JsonGroupInvitePeople;
import co.docy.oldfriends.Messages.JsonGroupInvitePeopleRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import de.greenrobot.event.EventBus;

public class FragmentCreateGroup extends FragmentBase {

    private static final int INVITE_PEOPLE = -111;
    File tempAttach;
    File randomAttach;

    RelativeLayout create_group_portrait;
    ImageView create_group_portrait_photo;
    EditText create_group_name_et;
    EditText create_group_desc_et;
    CheckBox create_group_permission_join_switch;
    CheckBox create_group_notify_mode;
    CheckBox create_group_inner_switch;
    private RecyclerView rv_add_group_member_list;
    private GridLayoutManager mLayoutManager;
    private ArrayList<ContactUniversal> contactUniversals;
    private ArrayList<ContactUniversal> selectedContactUniversals;
    private ListAdapterAddGroupMember mAdapter;

    public static FragmentCreateGroup newInstance() {
        FragmentCreateGroup f = new FragmentCreateGroup();

        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_create_group, null);

        create_group_name_et = (EditText) v.findViewById(R.id.create_group_name_et);
        create_group_desc_et = (EditText) v.findViewById(R.id.create_group_desc_et);


        create_group_portrait = (RelativeLayout) v.findViewById(R.id.create_group_portrait);
        create_group_portrait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                intent.setType(MyConfig.MIME_IMAGE_STAR);

                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(intent, MyConfig.REQUEST_GALLERY_AVATAR);
                }

            }
        });
        create_group_portrait_photo = (ImageView) v.findViewById(R.id.create_group_portrait_photo);
        /**先给一个随机的图片当做小组封面，若用户不更改就默认使用这个，若更改就使用更改的。*/
        new Thread(new Runnable() {
            @Override
            public void run() {
                randomAttach = HttpTools.okhttpGetFile(MyConfig.getApiDomain_NoSlash_GetResources() + "/directrandomlogo?type=group",
                        new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg"));

                if (randomAttach != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Picasso.with(getActivity()).load(randomAttach)
                                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                                    .into(create_group_portrait_photo);
                        }
                    });
                }
            }
        }).start();

        create_group_permission_join_switch = (CheckBox) v.findViewById(R.id.create_group_permission_join_switch);
        create_group_notify_mode = (CheckBox) v.findViewById(R.id.create_group_notify_mode);
        create_group_inner_switch = (CheckBox) v.findViewById(R.id.create_group_inner_switch);

        rv_add_group_member_list = (RecyclerView) v.findViewById(R.id.rv_add_group_member_list);

        /**在老朋友中创建小组不允许拉人*/
       // showRecyclerViewData();
        return v;
    }

    /**
     * 展示RecyclerView数据
     */
    private void showRecyclerViewData() {
        contactUniversals = new ArrayList<>();
        contactUniversals.clear();
        contactUniversals.addAll(ContactUniversal.convertListFromGetCompanyUserListRet(MyConfig.jsonGetCompanyUserListRet.data));
        MyConfig.contactUniversals = contactUniversals;
        mLayoutManager = new GridLayoutManager(getActivity(), 5);
        rv_add_group_member_list.setLayoutManager(mLayoutManager);

        selectedContactUniversals = new ArrayList<>();
        mAdapter = new ListAdapterAddGroupMember(selectedContactUniversals, onClickAddListenerInvite, onClickAddListenerKick, onClickAddListenerMore);
        rv_add_group_member_list.setAdapter(mAdapter);
    }

    /**
     * 添加成员监听器
     */
    public View.OnClickListener onClickAddListenerInvite = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getActivity(), ActivityCreateGroupPeople.class);
            intent.putExtra(MyConfig.CREATE_GROUP_PEOPLE_CATEGORY, MyConfig.CREATE_GROUP_PEOPLE_CATEGORY_INVITE);
            getActivity().startActivityForResult(intent, 31);


        }
    };
    /**
     * 减少成员监听器
     */
    public View.OnClickListener onClickAddListenerKick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getActivity(), ActivityCreateGroupPeople.class);
            intent.putExtra(MyConfig.CREATE_GROUP_PEOPLE_CATEGORY, MyConfig.CREATE_GROUP_PEOPLE_CATEGORY_KICK);
            getActivity().startActivityForResult(intent, 31);


        }
    };
    /**
     * 查看更多成员监听器
     */
    public View.OnClickListener onClickAddListenerMore = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getActivity(), ActivityCreateGroupPeople.class);
            intent.putExtra(MyConfig.CREATE_GROUP_PEOPLE_CATEGORY, MyConfig.CREATE_GROUP_PEOPLE_CATEGORY_MORE);
            getActivity().startActivityForResult(intent, 31);


        }
    };


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);
    }

    /**
     * 从添加成员页面返回
     */
    public void forResult() {
        selectedContactUniversals.clear();
        for (ContactUniversal contactUniversal : contactUniversals) {
            if (contactUniversal.selected) {
                selectedContactUniversals.add(contactUniversal);
            }

        }
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == MyConfig.REQUEST_GALLERY_AVATAR && resultCode == Activity.RESULT_OK) {

            Intent innerIntent = new Intent("com.android.camera.action.CROP");
            innerIntent.setDataAndType(data.getData(), MyConfig.MIME_IMAGE_STAR);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true");// 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
            if (innerIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);

            }

        }

        if (requestCode == MyConfig.REQUEST_IMAGE_CROP && resultCode == Activity.RESULT_OK) {
            MyLog.d("", "onActivityResult hit fragment! REQUEST_IMAGE_CROP" + resultCode);

            Bitmap bitmap = data.getParcelableExtra("data");

            tempAttach = new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg");

            // 图像保存到文件中
            FileOutputStream foutput = null;
            try {
                foutput = new FileOutputStream(tempAttach);
                if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, foutput)) {

//                    Toast.makeText(MyConfig.app,
//                            "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath(),
//                            Toast.LENGTH_LONG).show();
                    MyConfig.MyToast(-1, MyConfig.app,
                            "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath());

                    Picasso.with(getActivity()).load(tempAttach)
                            .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                            .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                            .into(create_group_portrait_photo);

                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }


    }


    public void submitWithPhoto() {
        /**
         * 新注册接口，包含可选的photo信息
         */
        final JsonCreateGroup jsonCreateGroup = new JsonCreateGroup();

        jsonCreateGroup.name = create_group_name_et.getText().toString().trim();
        create_group_name_et.setText(jsonCreateGroup.name);
        jsonCreateGroup.desc = create_group_desc_et.getText().toString().trim();
        create_group_desc_et.setText(jsonCreateGroup.desc);


        if (create_group_permission_join_switch.isChecked()) {
            jsonCreateGroup.joinType = 1;
        } else {
            jsonCreateGroup.joinType = 0;
        }

        jsonCreateGroup.broadcast = create_group_notify_mode.isChecked();
        if (create_group_inner_switch.isChecked()) {
            jsonCreateGroup.category = MyConfig.SERVER_ROOM_CATEGORY_PRIVATE;
        } else {
            jsonCreateGroup.category = MyConfig.SERVER_ROOM_CATEGORY_NORMAL;
        }
        MyLog.d("", "groupinfo: create, private is " + jsonCreateGroup.category);

        if (create_group_name_et.length() < 2 || create_group_name_et.length() > 20) {
            notifyFailure(getActivity().getString(R.string.create_group_name_length));
            return;
        }

        if (tempAttach != null) {
            jsonCreateGroup.logo = tempAttach;
        } else if (randomAttach != null) {
            jsonCreateGroup.logo = randomAttach;
        }
        int i = 0;
        for (ContactUniversal contactUniversal : contactUniversals) {
            if (contactUniversal.selected) {
                jsonCreateGroup.userIds[i] = contactUniversal.id;
                i++;
            }

        }


        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonCreateGroupRet jsonCreateGroupRet = HttpTools.okhttpCreateGroupWithPhoto(jsonCreateGroup);

                if (jsonCreateGroupRet != null) {

                    if (jsonCreateGroupRet.code != MyConfig.retSuccess()) {
                        notifyFailure(jsonCreateGroupRet.message);
                    } else {
                        addGroupPeople(jsonCreateGroupRet);

                    }
                } else {
                    notifyFailure(getActivity().getString(R.string.create_group_fail));
                }


            }
        }).start();
    }

    /**
     * 添加被选中的人为组员
     */
    private void addGroupPeople(final JsonCreateGroupRet jsonCreateGroupRet) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                final JsonGroupInvitePeople jsonGroupInvitePeople = new JsonGroupInvitePeople();
                jsonGroupInvitePeople.accessToken = MyConfig.usr_token;

                for (ContactUniversal contactUniversal : contactUniversals) {
                    if (contactUniversal.selected) {

                        jsonGroupInvitePeople.toAdd.add(contactUniversal.id);
                    }

                }

                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonGroupInvitePeople, JsonGroupInvitePeople.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_USERS_INVITE + jsonCreateGroupRet.data.id + MyConfig.API2_GROUP_USERS_INVITE_2, jsonStr);

                final JsonGroupInvitePeopleRet jsonGroupInvitePeopleRet = gson.fromJson(s, JsonGroupInvitePeopleRet.class);
                MyLog.d("", "HttpTools: " + s);
                if (jsonGroupInvitePeopleRet != null && jsonGroupInvitePeopleRet.code == MyConfig.retSuccess()) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            notifySuccess(jsonCreateGroupRet);
                        }
                    });
                }

            }
        }).start();
    }

    private void notifyFailure(final String s) {
        MyLog.d("", "CreateGroup failed " + s);

//        Snackbar.make(this.getView(), s, Snackbar.LENGTH_LONG).show();
//        MyConfig.snackbarTop(getView(), s, R.color.Red, Snackbar.LENGTH_SHORT);
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MyConfig.MyToast(0, getActivity(), s);
                }
            });
        }
    }

    private void notifySuccess(JsonCreateGroupRet jsonCreateGroupRet) {
        MyLog.d("", "CreateGroup code");
//        Snackbar.make(this.getView(), "Success!", Snackbar.LENGTH_LONG).show();
        Intent intent_main = new Intent(getActivity(), ActivityMainGroup.class);
        intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, jsonCreateGroupRet.data.id);
        intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, jsonCreateGroupRet.data.name);
        intent_main.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, jsonCreateGroupRet.data.desc);
        startActivity(intent_main);
        getActivity().finish();
        EventBus.getDefault().post(new EventCreateGroupSuccess());
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_group_create, menu);

        super.onCreateOptionsMenu(menu, inflater);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                Toast.makeText(this, "home key is pressed, input is discarded", Toast.LENGTH_LONG).show();

//                backWithoutResult();
                return true;
            case R.id.create_group_submit:
//                submit();
                submitWithPhoto();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}


