package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.LinkedList;

import co.docy.oldfriends.Activitis.ActivityChooseGroupOwner;
import co.docy.oldfriends.Activitis.ActivityDeleteUserList;
import co.docy.oldfriends.Activitis.ActivityGroupInfo;
import co.docy.oldfriends.Activitis.ActivityGroupInfoNotifySetting;
import co.docy.oldfriends.Activitis.ActivityGroupInvitePeople;
import co.docy.oldfriends.Activitis.ActivityGroupUserChooserList;
import co.docy.oldfriends.Activitis.ActivityMainGroup;
import co.docy.oldfriends.Activitis.ActivityUpdateGroupInfo;
import co.docy.oldfriends.Adapters.ListAdapterGroupUser;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.EventBus.EventCompanyListUpdateUi;
import co.docy.oldfriends.EventBus.EventGroupListUpdateFromServer;
import co.docy.oldfriends.EventBus.EventHeartOneSecond;
import co.docy.oldfriends.EventBus.EventJionedGroup;
import co.docy.oldfriends.Messages.JsonDeleteGroup;
import co.docy.oldfriends.Messages.JsonGetGroupInfoRet;
import co.docy.oldfriends.Messages.JsonGetGroupUserRet;
import co.docy.oldfriends.Messages.JsonJoinGroup;
import co.docy.oldfriends.Messages.JsonJoinGroupRet;
import co.docy.oldfriends.Messages.JsonLeaveGroup;
import co.docy.oldfriends.Messages.JsonLeaveGroupRet;
import co.docy.oldfriends.Messages.JsonSetGroupLive;
import co.docy.oldfriends.Messages.JsonSetGroupLiveRet;
import co.docy.oldfriends.Messages.JsonSetGroupLogo;
import co.docy.oldfriends.Messages.JsonSetGroupLogoRet;
import co.docy.oldfriends.Messages.JsonSetGroupPrivate;
import co.docy.oldfriends.Messages.JsonSetGroupPrivateRet;
import co.docy.oldfriends.Messages.JsonSetGroupToTop;
import co.docy.oldfriends.Messages.JsonSetGroupToTopRet;
import co.docy.oldfriends.Messages.JsonUploadFileRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import de.greenrobot.event.EventBus;

public class FragmentGroupInfo extends FragmentBase implements View.OnClickListener {

    MyConfig.IOnOkClickListener onOkClickListener;

    int groupId;
    String name;

    int num_in_row = 5;
    public static int memberCount;
    private TextView groupinfo_intru_text, groupinfo_user_text, groupinfo_name_text;
    private Button groupinfo_quit, groupinfo_transfer, groupinfo_delete;
    private ImageView groupinfo_portrait_photo;
    private RelativeLayout groupinfo_portrait, groupinfo_name_layout, groupinfo_intru_layout, groupinfo_user;
    private CheckBox groupinfo_inner_switch, groupinfo_setgrouptop_switch, groupinfo_setlive_switch;
    private LinearLayout groupinfo_msg, group_info_infoview;
    private RecyclerView mRecyclerView;
    private GridLayoutManager mLayoutManager;
    private ListAdapterGroupUser mAdapter;
    private int heightRow;
    private LinkedList<JsonGetGroupUserRet.GetGroupUserRet> list = new LinkedList<>();
    //  CheckBox groupinfo_permission_switch;
    //  CheckBox groupinfo_notify_switch;
    ListAdapterGroupUser.OnInviteClickedListener inviteClickedListener = new ListAdapterGroupUser.OnInviteClickedListener() {
        @Override
        public void onInviteClicked() {
            MyLog.d("", "inviteClickedListener");
            Intent intent = new Intent(getActivity(), ActivityGroupInvitePeople.class);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
            intent.putExtra("mode", MyConfig.MODE_CHOOSE_MULTI);
            MyConfig.byPassContactUniversalList_temp = ContactUniversal.convertListFromGetGroupUserRet(MyConfig.jsonGetGroupUserRet.data);
            startActivityForResult(intent, MyConfig.REQUEST_CHOOSE_CONTACT);
        }
    };
    ListAdapterGroupUser.OnDeleteClickedListener deleteClickedListener = new ListAdapterGroupUser.OnDeleteClickedListener() {
        @Override
        public void onDeleteClicked() {
            Intent intent = new Intent(getActivity(), ActivityDeleteUserList.class);
            intent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
            intent.putExtra("mode", MyConfig.MODE_CHOOSE_MULTI_DELETE);
            //     MyConfig.byPassContactUniversalList_temp = ContactUniversal.convertListFromGetGroupUserRet(MyConfig.jsonGetGroupUserRet.data);
            startActivity(intent);
        }
    };

    ListAdapterGroupUser.OnMoreClickedListener moreClickedListener = new ListAdapterGroupUser.OnMoreClickedListener() {
        @Override
        public void onMoreClicked() {
            MyConfig.activityGroupUserChooserListTitle = "小组成员";
            MyConfig.activityGroupUserChooserListList = ContactUniversal.convertListFromGetGroupUserRet(MyConfig.jsonGetGroupUserRet.data);
            startActivity(new Intent(getActivity(), ActivityGroupUserChooserList.class));
        }
    };

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_groupinfo, null);
        groupId = getArguments().getInt(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID);
        name = getArguments().getString(MyConfig.CONSTANT_PARAMETER_STRING_NAME);

        groupinfo_name_text = (TextView) v.findViewById(R.id.groupinfo_name_text);
        //  groupinfo_name_text.setText(name);
        groupinfo_user_text = (TextView) v.findViewById(R.id.groupinfo_user_text);
        groupinfo_intru_text = (TextView) v.findViewById(R.id.groupinfo_intru_text);
        groupinfo_portrait_photo = (ImageView) v.findViewById(R.id.groupinfo_portrait_photo);
        groupinfo_portrait = (RelativeLayout) v.findViewById(R.id.groupinfo_portrait);
        groupinfo_msg = (LinearLayout) v.findViewById(R.id.groupinfo_msg);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.groupinfo_user_list);
        groupinfo_inner_switch = (CheckBox) v.findViewById(R.id.groupinfo_inner_switch);
        groupinfo_setgrouptop_switch =(CheckBox) v.findViewById(R.id.groupinfo_setGroupTop_switch);
        groupinfo_setlive_switch =(CheckBox) v.findViewById(R.id.groupinfo_setLive_switch);
        groupinfo_delete = (Button) v.findViewById(R.id.groupinfo_delete);
        groupinfo_quit = (Button) v.findViewById(R.id.groupinfo_quit);
        groupinfo_transfer = (Button) v.findViewById(R.id.groupinfo_transfer);
        groupinfo_name_layout = (RelativeLayout) v.findViewById(R.id.groupinfo_name);
        groupinfo_intru_layout = (RelativeLayout) v.findViewById(R.id.groupinfo_intru);
        groupinfo_user = (RelativeLayout) v.findViewById(R.id.groupinfo_user);

        mLayoutManager = new GridLayoutManager(getActivity(), num_in_row);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (!ActivityGroupInfo.joined) {
            mAdapter = new ListAdapterGroupUser(getActivity(), list, true, false, false);
        } else {
            mAdapter = new ListAdapterGroupUser(getActivity(), list, true, false, true);
        }

        mRecyclerView.setAdapter(mAdapter);

        group_info_infoview = (LinearLayout) v.findViewById(R.id.group_info_infoview);
        group_info_infoview.setVisibility(getActivity().getIntent().getBooleanExtra("isShowInvite", true) ? View.VISIBLE : View.GONE);

        groupinfo_name_layout.setOnClickListener(this);
        groupinfo_intru_layout.setOnClickListener(this);
        groupinfo_portrait.setOnClickListener(this);
        groupinfo_inner_switch.setOnClickListener(this);
        groupinfo_setgrouptop_switch.setOnClickListener(this);
        groupinfo_setlive_switch.setOnClickListener(this);
        groupinfo_msg.setOnClickListener(this);
        groupinfo_quit.setOnClickListener(this);
        groupinfo_transfer.setOnClickListener(this);
        groupinfo_delete.setOnClickListener(this);

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateGroupInfo();
    }

//    public void onEventMainThread(EventHeartOneSecond eventHeartOneSecond) {
//        MyLog.d("", "ListAdapterContactOfCompanyMultiChoose EventHeartOneSecond ");
//
//        if (eventHeartOneSecond.heart_second_count % 2 == 0) {
//            getGroupUsers();
//        }
//
//    }

    public void onEventMainThread(EventCompanyListUpdateUi eventCompanyListUpdateUi) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentCompanyList.java: EventCompanyListUpdateUi");

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyLog.d("", "onActivityResult hit fragment! request code " + requestCode + " result code " + resultCode);
        if (requestCode == MyConfig.REQUEST_GALLERY_AVATAR && resultCode == Activity.RESULT_OK) {

            Intent innerIntent = new Intent("com.android.camera.action.CROP");
            innerIntent.setDataAndType(data.getData(), MyConfig.MIME_IMAGE_STAR);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_CROP, "true");// 才能出剪辑的小方框，不然没有剪辑功能，只能选取图片
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTX, 1); // 放大缩小比例的X
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_ASPECTY, 1);// 放大缩小比例的X   这里的比例为：   1:1
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTX, 320);  //这个是限制输出图片大小
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_OUTPUTY, 320);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_RETURN_DATA, true);
            innerIntent.putExtra(MyConfig.CONSTANT_ATTRIBUTE_STRING_SCALE, true);
            if (innerIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(innerIntent, MyConfig.REQUEST_IMAGE_CROP);
            }


//            String realPath = MyConfig.getUriPath(getActivity(), fileUri);
//            File tempAttach = new File(realPath);
//
//            uploadUserAvatar(tempAttach);
        }

        if (requestCode == MyConfig.REQUEST_IMAGE_CROP && resultCode == Activity.RESULT_OK) {

            Bitmap bitmap = data.getParcelableExtra("data");

            File tempAttach = new File(MyConfig.appCacheDirPath, MyConfig.formClientCheckId(0) + ".jpg");

            // 图像保存到文件中
            FileOutputStream foutput = null;
            try {
                foutput = new FileOutputStream(tempAttach);
                if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, foutput)) {
//                    Toast.makeText(MyConfig.app,
//                            "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath(),
//                            Toast.LENGTH_LONG).show();
                    MyConfig.MyToast(-1, MyConfig.app,
                            "已生成缓存文件，等待上传！文件位置：" + tempAttach.getAbsolutePath());
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            uploadAvatar(tempAttach);

        }

    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putSerializable("list", (Serializable) list);

    }

    public static FragmentGroupInfo newInstance(int groupId, String name) {
        FragmentGroupInfo f = new FragmentGroupInfo();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
        args.putString(MyConfig.CONSTANT_PARAMETER_STRING_NAME, name);
        f.setArguments(args);
        return f;
    }

    public void setInner(final boolean category) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSetGroupPrivate jsonSetGroupPrivate = new JsonSetGroupPrivate();

                jsonSetGroupPrivate.setPrivate = category;
                jsonSetGroupPrivate.accessToken = MyConfig.usr_token;

                String jsonStr = gson.toJson(jsonSetGroupPrivate, JsonSetGroupPrivate.class);
                MyLog.d("", "HttpTools: jsonSetGroupPrivateRet jsonStr:" + jsonStr);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SET_GROUP_PRIVATE + groupId + MyConfig.API2_SET_GROUP_PRIVATE_2, jsonStr);

                final JsonSetGroupPrivateRet jsonSetGroupPrivateRet = gson.fromJson(s, JsonSetGroupPrivateRet.class);
                MyLog.d("", "HttpTools: jsonSetGroupPrivateRet " + s);
                if (jsonSetGroupPrivateRet != null && jsonSetGroupPrivateRet.code == MyConfig.retSuccess()) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });

                }

            }
        }).start();

    }

    public void leaveGroup(final JsonLeaveGroup jsonLeaveGroup) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonLeaveGroup, JsonLeaveGroup.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_LEAVE_GROUP + jsonLeaveGroup.groupId + MyConfig.API2_LEAVE_GROUP_2, jsonStr);

                final JsonLeaveGroupRet jsonLeaveGroupRet = gson.fromJson(s, JsonLeaveGroupRet.class);
                MyLog.d("", "HttpTools: JsonLeaveRet " + s);
                if (jsonLeaveGroupRet != null && jsonLeaveGroupRet.code == MyConfig.retSuccess()) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            Toast.makeText(
//                                    getActivity(),
//                                    jsonLeaveGroupRet.message,
//                                    Toast.LENGTH_LONG).show();
                            MyConfig.MyToast(-1, getActivity(),
                                    jsonLeaveGroupRet.message);
                        }
                    });

//                  EventBus.getDefault().post(new EventQuitGroup());
                    if (null != MyConfig.activityMainGroup) {
                        MyConfig.activityMainGroup.finish();
                    }

                    getActivity().finish();


                }

            }
        }).start();

    }

    public void updateGroupInfo() {
        getGroupInfo();

    }

    private void getGroupInfo() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_INFO + groupId, MyConfig.usr_token);
                MyLog.d("", "OKHTTP: room " + groupId + " groupinfo: " + s);
                MyConfig.jsonGetGroupInfoRet = gson.fromJson(s, JsonGetGroupInfoRet.class);
                Activity activity = getActivity();
                if(activity == null){return;}
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (MyConfig.jsonGetGroupInfoRet != null && MyConfig.jsonGetGroupInfoRet.code == MyConfig.retSuccess()) {
                            Picasso.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + MyConfig.jsonGetGroupInfoRet.data.logo)
                                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                                    .into(groupinfo_portrait_photo);
                            groupinfo_intru_text.setText(MyConfig.jsonGetGroupInfoRet.data.desc);
                            groupinfo_name_text.setText(MyConfig.jsonGetGroupInfoRet.data.name);
                            group_info_infoview.setVisibility(getActivity().getIntent().getBooleanExtra("isShowInvite", true) ? View.VISIBLE : View.GONE);
//                            if(jsonGetGroupInfoRet.data.joinType == 0){
//                                groupinfo_permission_switch.updateToIMsg(false);
//                            }else{
//                                groupinfo_permission_switch.updateToIMsg(true);
//                            }
                            //                        groupinfo_notify_switch.updateToIMsg(jsonGetGroupInfoRet.data.broadcast);

                            if (MyConfig.jsonGetGroupInfoRet.data.category == MyConfig.SERVER_ROOM_CATEGORY_PRIVATE) {
                                groupinfo_inner_switch.setChecked(true);
                            } else {
                                groupinfo_inner_switch.setChecked(false);
                            }

                            if(MyConfig.jsonGetGroupInfoRet.data.showTop){
                                groupinfo_setgrouptop_switch.setChecked(true);
                            }else{
                                groupinfo_setgrouptop_switch.setChecked(false);
                            }
                            if(MyConfig.jsonGetGroupInfoRet.data.status == 1){
                                groupinfo_setlive_switch.setChecked(true);
                            }else{
                                groupinfo_setlive_switch.setChecked(false);
                            }

                            if(MyConfig.usr_type == MyConfig.USER_TYPE_IS_TEACHER && MyConfig.usr_id == MyConfig.jsonGetGroupInfoRet.data.creator){
                                groupinfo_setlive_switch.setClickable(true);
                            }else{
                                groupinfo_setlive_switch.setClickable(false);
                                groupinfo_setlive_switch.setTextColor(getActivity().getResources().getColor(R.color.yuanzhuo_universal_split_line));
                            }

                            getGroupUsers();

                        }
                    }
                });

            }
        }).start();
    }

    private void getGroupUsers() {
        MyLog.d("", "OKHTTP: getGroupUsers() 3");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_USERS + groupId + MyConfig.API2_GROUP_USERS_2, MyConfig.usr_token);
                MyLog.d("", "OKHTTP: room " + groupId + " users: " + s);
                MyConfig.jsonGetGroupUserRet = gson.fromJson(s, JsonGetGroupUserRet.class);
                Activity activity = getActivity();
                if (activity != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (MyConfig.jsonGetGroupUserRet != null && MyConfig.jsonGetGroupUserRet.code == MyConfig.retSuccess()) {
                                //更新msg列表
                                list = MyConfig.jsonGetGroupUserRet.data;

                                showGroupUsers();

                            }

                        }
                    });
                }

            }
        }).start();
    }

    private void showGroupUsers() {
        LinkedList<JsonGetGroupUserRet.GetGroupUserRet> list_temp = new LinkedList<>();

        MyLog.d("", "OKHTTP: list.size " + list.size());
        groupinfo_user_text.setText("" + list.size());
        memberCount = list.size();
        if (list.size() > 0 && MyConfig.usr_id == list.getFirst().id) {  //创建人才能操作如下选项
            groupinfo_inner_switch.setClickable(true);
            groupinfo_portrait.setClickable(true);
            groupinfo_quit.setVisibility(View.GONE);
            groupinfo_name_layout.setEnabled(true);
            groupinfo_intru_layout.setEnabled(true);

            groupinfo_delete.setVisibility(View.VISIBLE);
            groupinfo_transfer.setVisibility(View.VISIBLE);
            //  groupinfo_quit.setVisibility(View.VISIBLE);
            if (list.size() <= 13) {
                list_temp = list;
                heightRow = 1 + (list_temp.size() + 1) / num_in_row;
                //   showMore = false;
            } else {
                list_temp.clear();
                for (int i = 0; i < 14; i++) {
                    list_temp.add(list.get(i));
                }
                heightRow = 3;

            }
            if (!ActivityGroupInfo.joined) {
                mAdapter = new ListAdapterGroupUser(getActivity(), list_temp, true, true,false);
            } else {
                mAdapter = new ListAdapterGroupUser(getActivity(), list_temp, true, true,true);
            }
        } else {
            groupinfo_inner_switch.setClickable(false);
            groupinfo_inner_switch.setTextColor(getActivity().getResources().getColor(R.color.yuanzhuo_universal_split_line));
            groupinfo_portrait.setClickable(false);
            groupinfo_name_layout.setEnabled(false);
            groupinfo_intru_layout.setEnabled(false);
            //   groupinfo_quit.setEnabled(true);

            groupinfo_delete.setVisibility(View.GONE);
            groupinfo_transfer.setVisibility(View.GONE);
            groupinfo_quit.setVisibility(View.VISIBLE);
            if (MyConfig.jsonGetGroupInfoRet.data.category == MyConfig.SERVER_ROOM_CATEGORY_PRIVATE ||
                    !(getActivity().getIntent().getBooleanExtra("isShowInvite", true))) {
                //   mAdapter = new ListAdapterGroupUser(getActivity(), list, false);
                if (list.size() <= 15) {
                    list_temp = list;
                    heightRow = 1 + (list_temp.size() - 1) / num_in_row;
                    //      showMore = false ;
                } else {
                    list_temp.clear();
                    for (int i = 0; i < 15; i++) {
                        list_temp.add(list.get(i));
                    }
                    heightRow = 3;

                }
                if (!ActivityGroupInfo.joined) {
                    mAdapter = new ListAdapterGroupUser(getActivity(), list_temp, false, false, false);
                } else {
                    mAdapter = new ListAdapterGroupUser(getActivity(), list_temp, false, false, true);
                }
            } else {
                // mAdapter = new ListAdapterGroupUser(getActivity(), list, true);
                if (list.size() <= 14) {
                    list_temp = list;
                    heightRow = 1 + (list_temp.size()) / num_in_row;

                } else {
                    list_temp.clear();
                    for (int i = 0; i < 15; i++) {
                        list_temp.add(list.get(i));
                    }
                    heightRow = 3;

                }
                if (!ActivityGroupInfo.joined) {
                    mAdapter = new ListAdapterGroupUser(getActivity(), list_temp, true, false, false);
                } else {
                    mAdapter = new ListAdapterGroupUser(getActivity(), list_temp, true, false, true);
                }
            }
            if (!ActivityGroupInfo.joined) {
                groupinfo_quit.setText(getString(R.string.join_group));
            } else {
                groupinfo_quit.setText(getString(R.string.exit));
            }
        }
        mAdapter.inviteClickedListener = inviteClickedListener;
        mAdapter.deleteClickedListener = deleteClickedListener;
        mAdapter.moreClickedListener = moreClickedListener;
        Resources r = getActivity().getResources();
        int height_pix = (int) (
                r.getDimension(R.dimen.avatar_height)
                        + r.getDimension(R.dimen.setting_user_avatar_textview_height)
                        + r.getDimension(R.dimen.setting_user_avatar_textview_magin_bottom)
        );
        mRecyclerView.getLayoutParams().height = heightRow * height_pix + (int) (r.getDimension(R.dimen.setting_bottom_margin) + r.getDimension(R.dimen.setting_top_margin));

        mRecyclerView.setAdapter(mAdapter);
    }

    public void uploadAvatar(final File f) {


        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonUploadFileRet jsonUploadFileRet = HttpTools.okhttpUploadFile(f, "image/jpeg");

                if (jsonUploadFileRet != null && jsonUploadFileRet.code == MyConfig.retSuccess()) {
                    final String path = jsonUploadFileRet.data.path;

                    JsonSetGroupLogo jsonSetGroupLogo = new JsonSetGroupLogo();
                    jsonSetGroupLogo.accessToken = MyConfig.usr_token;
                    jsonSetGroupLogo.logo = path;

                    Gson gson = new Gson();
                    String jsonStr = gson.toJson(jsonSetGroupLogo, JsonSetGroupLogo.class);

                    final String ss = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_SETLOGO + groupId + MyConfig.API2_GROUP_SETLOGO_2, jsonStr);

                    final JsonSetGroupLogoRet jsonSetGroupLogoRet = gson.fromJson(ss, JsonSetGroupLogoRet.class);
                    MyLog.d("", "OKHTTP set avatar: " + ss);
                    if (jsonSetGroupLogoRet != null && jsonSetGroupLogoRet.code == MyConfig.retSuccess()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Picasso.with(getActivity()).load(MyConfig.getApiDomain_NoSlash_GetResources() + path)
                                        .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                                        .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                                        .into(groupinfo_portrait_photo);
                                MyConfig.MyToast(0, getActivity(), jsonSetGroupLogoRet.message);
                            }
                        });

                    }

//                    Snackbar.make(getView(), jsonSetAvatarRet.message, Snackbar.LENGTH_LONG).show();
//                    MyConfig.snackbarTop(getView(), jsonSetGroupLogoRet.message, R.color.Red, Snackbar.LENGTH_SHORT);

                } else {
//                    Snackbar.make(getView(), "更换头像失败", Snackbar.LENGTH_LONG).show();
                    //           MyConfig.snackbarTop(getView(), "更换头像失败", R.color.Red, Snackbar.LENGTH_SHORT);
                    //更换头像失败get
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MyConfig.MyToast(0, getActivity(), getActivity().getString(R.string.groupinfo_avatar));
                        }
                    });
                }

            }
        }).start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.groupinfo_portrait:
                final Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                intent.setType(MyConfig.MIME_IMAGE_STAR);
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(intent, MyConfig.REQUEST_GALLERY_AVATAR);
                }
                break;
            case R.id.groupinfo_msg:
                Intent msgintent = new Intent(getActivity(), ActivityGroupInfoNotifySetting.class);
                msgintent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
                startActivity(msgintent);
                break;
            case R.id.groupinfo_name:
                Intent nameintent = new Intent(getActivity(), ActivityUpdateGroupInfo.class);
                nameintent.putExtra("type", MyConfig.CONSTANT_PARAMETER_STRING_NAME);
                startActivity(nameintent);
                break;
            case R.id.groupinfo_intru:
                Intent intruintent = new Intent(getActivity(), ActivityUpdateGroupInfo.class);
                intruintent.putExtra("type", MyConfig.CONSTANT_PARAMETER_STRING_DESC);
                startActivity(intruintent);
                break;
            case R.id.groupinfo_quit:
                if (ActivityGroupInfo.joined) {
                    onOkClickListener = new MyConfig.IOnOkClickListener() {
                        @Override
                        public void OnOkClickListener() {
                            JsonLeaveGroup jsonLeaveGroup = new JsonLeaveGroup();
                            jsonLeaveGroup.accessToken = MyConfig.usr_token;
                            jsonLeaveGroup.groupId = groupId;
                            leaveGroup(jsonLeaveGroup);
                        }
                    };
                    MyConfig.showRemindDialog("", getActivity().getString(R.string.exit_sure_2), getActivity(), true, onOkClickListener);

                } else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Gson gson = new Gson();
                            JsonJoinGroup jsonJoinGroup = new JsonJoinGroup();
                            jsonJoinGroup.accessToken = MyConfig.usr_token;
                            jsonJoinGroup.groupId = groupId;
                            String jsonStr = gson.toJson(jsonJoinGroup, JsonJoinGroup.class);
                            final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_JOIN_GROUP + groupId + MyConfig.API2_JOIN_GROUP_2, jsonStr);
                            final JsonJoinGroupRet jsonJoinGroupRet = gson.fromJson(s, JsonJoinGroupRet.class);
                            MyLog.d("", "HttpTools: JsonJoinRet " + jsonJoinGroupRet);
                            if (jsonJoinGroupRet != null && jsonJoinGroupRet.code == MyConfig.retSuccess()) {
                                EventBus.getDefault().post(new EventGroupListUpdateFromServer());
                                EventBus.getDefault().post(new EventJionedGroup(true));
                                ActivityGroupInfo.joined = true;
                                Intent groupintent = new Intent(getActivity(), ActivityMainGroup.class);
                                groupintent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
                                groupintent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, name);
                                groupintent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, groupinfo_intru_text.getText());
                                startActivity(groupintent);
                            } else {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), getActivity().getString(R.string.groupinfo_join_failed), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }
                    }).start();
                }

                break;
            case R.id.groupinfo_delete:
                showRemindDialog(getResources().getString(R.string.exit_delete_group),
                        getResources().getString(R.string.exit_deletegroup_remind),
                        getActivity(), groupId);
                break;
            case R.id.groupinfo_transfer:
                Intent newIntent = new Intent(getActivity(), ActivityChooseGroupOwner.class);
                newIntent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
                newIntent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_NAME, (groupinfo_name_text.getText()) == null ? " " : groupinfo_name_text.getText().toString());
                newIntent.putExtra(MyConfig.CONSTANT_PARAMETER_STRING_DESC, (groupinfo_intru_text.getText()) == null ? " " : groupinfo_intru_text.getText().toString());
                startActivity(newIntent);
                break;
            case R.id.groupinfo_inner_switch:
                if (groupinfo_inner_switch.isChecked()) {
                    setInner(true);
                } else {
                    setInner(false);
                }
                break;
            case R.id.groupinfo_setGroupTop_switch:
                if(groupinfo_setgrouptop_switch.isChecked()){
                    setGroupTop(true);
                }else{
                    setGroupTop(false);
                }
                break;
            case R.id.groupinfo_setLive_switch:
                if(groupinfo_setlive_switch.isChecked()){
                    MyLog.d("", "HttpTools: jsonSetGroupLive jsonStr: true ------ ");
                    setGroupLive(true);
                }else{
                    MyLog.d("", "HttpTools: jsonSetGroupLive jsonStr: false --------");
                    setGroupLive(false);
                }
                break;
        }

    }

    private void setGroupLive(final boolean tag){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                JsonSetGroupLive jsonSetGroupLive = new JsonSetGroupLive();
                jsonSetGroupLive.accessToken = MyConfig.usr_token;
                jsonSetGroupLive.groupId = groupId;
                jsonSetGroupLive.broad = tag;
                String jsonStr = gson.toJson(jsonSetGroupLive, JsonSetGroupLive.class);
                MyLog.d("", "HttpTools: jsonSetGroupLive jsonStr:" + jsonStr);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_SET_LIVE + groupId + MyConfig.API_GROUP_SET_LIVE_2, jsonStr);

                final JsonSetGroupLiveRet jsonSetGroupLiveRet = gson.fromJson(s, JsonSetGroupLiveRet.class);
                MyLog.d("小组设置直播", "HttpTools: jsonSetGroupLive " + s);
                if(jsonSetGroupLiveRet!=null&&jsonSetGroupLiveRet.code == MyConfig.retSuccess()){

                }
            }
        }).start();
    }

    private void setGroupTop(final boolean tag){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                JsonSetGroupToTop jsonSetGroupToTop = new JsonSetGroupToTop();
                jsonSetGroupToTop.accessToken = MyConfig.usr_token;
                jsonSetGroupToTop.groupId = groupId;
                jsonSetGroupToTop.showTop = tag;
                String jsonStr = gson.toJson(jsonSetGroupToTop, JsonSetGroupToTop.class);
                MyLog.d("", "HttpTools: jsonSetGroupPrivateRet jsonStr:" + jsonStr);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_SETGROUPTOP + groupId + MyConfig.API_GROUP_SETGROUPTOP_2, jsonStr);

                final JsonSetGroupToTopRet jsonSetGroupToTopRet = gson.fromJson(s, JsonSetGroupToTopRet.class);
                MyLog.d("小组置顶返回", "HttpTools: JsonSetGroupToTopRet " + s);
                if(jsonSetGroupToTopRet!=null&&jsonSetGroupToTopRet.code == MyConfig.retSuccess()){

                }
            }
        }).start();
    }

    public void showRemindDialog(String title, final String remindstr, final Context mContext, final int groupId) {
        View dialogView = LayoutInflater.from(mContext).inflate(
                R.layout.dialog_exit_remind_layout, null);

        final Dialog setDialog = new Dialog(mContext, R.style.DialogStyle);

        setDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setDialog.setCancelable(false);
        setDialog.getWindow().setContentView(dialogView);

        WindowManager.LayoutParams lp = setDialog.getWindow().getAttributes();
        lp.width = MyConfig.getWidthByScreenPercent(80);
        setDialog.getWindow().setAttributes(lp);

        final Button cancleButton = (Button) dialogView.findViewById(R.id.cancle);
        Button loginButton = (Button) dialogView.findViewById(R.id.login);

        TextView remindText = (TextView) dialogView.findViewById(R.id.remind_text);
        remindText.setText(remindstr);

        TextView titletext = (TextView) dialogView.findViewById(R.id.remind_title);
        titletext.setText(title);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                JsonDeleteGroup jsonDeleteGroup = new JsonDeleteGroup();
                jsonDeleteGroup.accessToken = MyConfig.usr_token;
                jsonDeleteGroup.groupId = groupId;
                MyConfig.DeleteGroup(jsonDeleteGroup);

                getActivity().finish();

//                Intent intent = new Intent(mContext, ActivityViewPager.class);
//                mContext.startActivity(intent);

                setDialog.cancel();
            }

        });

        cancleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialog.cancel();
            }
        });
        setDialog.setCanceledOnTouchOutside(false);

        setDialog.show();
    }

    public void onEventMainThread(EventJionedGroup eventJionedGroup) {
        getActivity().finish();
    }

}
