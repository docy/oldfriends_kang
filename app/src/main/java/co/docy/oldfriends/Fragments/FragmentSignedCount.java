package co.docy.oldfriends.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import co.docy.oldfriends.Adapters.ListAdapterGetSignedMessage;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonGetSignedByDateRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/5/24.
 */
public class FragmentSignedCount extends FragmentBase {

    public static FragmentSignedCount newInstance() {
        FragmentSignedCount f = new FragmentSignedCount();
        return f;
    }
    ListAdapterGetSignedMessage listAdapterGetSignedMessage;
    TextView time_show;
    LinearLayout choose_time;
    ListView signed_listview;
    String current_time;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_signed_count,null);
        time_show =(TextView) v.findViewById(R.id.fragment_signed_count_time);
        choose_time =(LinearLayout) v.findViewById(R.id.fragment_signed_count_choosetime);
        signed_listview =(ListView) v.findViewById(R.id.fragment_signed_count_listview);
        Date date=new Date();
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
        current_time = format.format(date);
        time_show.setText(getWeek(current_time));
        choose_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateTimePicKDialog();
            }
        });
        getSignedByDate(current_time);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void dateTimePicKDialog() {
        //弹出选择时间的框
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_signed_count_choosetime, null);
        final DatePicker  datePicker = (DatePicker) view.findViewById(R.id.choose_time_datePicker);
        AlertDialog ad = new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String year = datePicker.getYear()+"";
                        String month ;
                        if(datePicker.getMonth()< 9){
                            month = "0"+ (datePicker.getMonth()+1);
                        }else{
                            month = ""+ (datePicker.getMonth()+1);
                        }
                        String day;
                        if(datePicker.getDayOfMonth()<= 9){
                            day = "0"+ datePicker.getDayOfMonth();
                        }else{
                            day = ""+ datePicker.getDayOfMonth();
                        }
                        StringBuffer buffer = new StringBuffer();
                        buffer.append(year).append("-").append(month).append("-").append(day);
                        getSignedByDate(buffer.toString());
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                }).show();

    }

    public void getSignedByDate(final String str){
        //按时间获取当前的签到数据
        new Thread(new Runnable() {
            @Override
            public void run() {
                    Gson gson = new Gson();
                    HashMap<String,String> map = new HashMap();
                    map.put("date",str);
                    final String s = new HttpTools().httpURLConnGetWithKeyValue(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CREATE_CHECKIN,map, MyConfig.usr_token);
                    MyLog.d("获取每日的签到数据",s);
                    final JsonGetSignedByDateRet jsonGetSignedByDateRet = gson.fromJson(s,JsonGetSignedByDateRet.class);
                    if(jsonGetSignedByDateRet !=null&& jsonGetSignedByDateRet.code == MyConfig.retSuccess()){
                        Activity activity = getActivity();
                        if(activity==null){
                            return;
                        }
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                listAdapterGetSignedMessage = new ListAdapterGetSignedMessage(getActivity(),jsonGetSignedByDateRet.data.rows);
                                if(jsonGetSignedByDateRet.data.rows.size()>0){
                                    signed_listview.setAdapter(listAdapterGetSignedMessage);
                                }else{
                                    signed_listview.setAdapter(listAdapterGetSignedMessage);
                                    Toast.makeText(getActivity(),"该日没有签到",Toast.LENGTH_SHORT).show();
                                }
                                time_show.setText(getWeek(str));
                            }
                        });
                    }
                }

        }).start();
    }


    private String getWeek(String strDate) {
        //转化时间的格式，计算星期几
        String Week = "星期";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
           c.setTime(format.parse(strDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        switch(c.get(Calendar.DAY_OF_WEEK)){
            case 1:
                Week += "日";
                break;
            case 2:
                Week += "一";
                break;
            case 3:
                Week += "二";
                break;
            case 4:
                Week += "三";
                break;
            case 5:
                Week += "四";
                break;
            case 6:
                Week += "五";
                break;
            case 7:
                Week += "六";
                break;
            default:
                break;
        }
        return Week+" "+strDate.substring(5,strDate.length());
    }


}
