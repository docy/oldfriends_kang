package co.docy.oldfriends.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonGetGroupInfoNotifySettingRet;
import co.docy.oldfriends.Messages.JsonGetGroupUserRet;
import co.docy.oldfriends.Messages.JsonSetGroupInfoNotifySetting;
import co.docy.oldfriends.Messages.JsonSetGroupInfoNotifySettingRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;

public class FragmentGroupInfoNotifySetting extends FragmentBase {

    int groupId;

    LinearLayout groupinfo_notifysetting_0;
    ImageView groupinfo_notifysetting_0_link;
    LinearLayout groupinfo_notifysetting_1;
    ImageView groupinfo_notifysetting_1_link;
    LinearLayout groupinfo_notifysetting_2;
    ImageView groupinfo_notifysetting_2_link;


    JsonGetGroupUserRet jsonGetGroupUserRet;
    LinkedList<JsonGetGroupUserRet.GetGroupUserRet> list = new LinkedList<>();

    public static FragmentGroupInfoNotifySetting newInstance(int groupId) {
        FragmentGroupInfoNotifySetting f = new FragmentGroupInfoNotifySetting();

        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, groupId);
        MyLog.d("", "jsonGetGroupInfoNotifySettingRet groupId " + groupId);
        f.setArguments(args);

        return f;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_groupinfo_notify_setting, null);

        groupId = getArguments().getInt(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID);
        MyLog.d("","jsonGetGroupInfoNotifySettingRet 2 groupId "+groupId);

        groupinfo_notifysetting_0 = (LinearLayout)v.findViewById(R.id.groupinfo_notifysetting_0);
        groupinfo_notifysetting_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateNotify(0);


            }
        });
        groupinfo_notifysetting_0_link = (ImageView)v.findViewById(R.id.groupinfo_notifysetting_0_link);

        groupinfo_notifysetting_1 = (LinearLayout)v.findViewById(R.id.groupinfo_notifysetting_1);
        groupinfo_notifysetting_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateNotify(1);
            }
        });
        groupinfo_notifysetting_1_link = (ImageView)v.findViewById(R.id.groupinfo_notifysetting_1_link);

        groupinfo_notifysetting_2 = (LinearLayout)v.findViewById(R.id.groupinfo_notifysetting_2);
        groupinfo_notifysetting_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateNotify(2);
            }
        });
        groupinfo_notifysetting_2_link = (ImageView)v.findViewById(R.id.groupinfo_notifysetting_2_link);

        return v;
    }

    public void updateNotify(int level) {

        updateUi(level);

        JsonSetGroupInfoNotifySetting jsonSetGroupInfoNotifySetting = new JsonSetGroupInfoNotifySetting();
        jsonSetGroupInfoNotifySetting.accessToken = MyConfig.usr_token;
        jsonSetGroupInfoNotifySetting.groupId = groupId;
        jsonSetGroupInfoNotifySetting.level = level;
        setNotifySetting(jsonSetGroupInfoNotifySetting);
    }

    public void updateUi(int level) {
        switch (level){
            case 0:
                groupinfo_notifysetting_0_link.setVisibility(View.VISIBLE);
                groupinfo_notifysetting_1_link.setVisibility(View.INVISIBLE);
                groupinfo_notifysetting_2_link.setVisibility(View.INVISIBLE);
                break;
            case 1:
                groupinfo_notifysetting_0_link.setVisibility(View.INVISIBLE);
                groupinfo_notifysetting_1_link.setVisibility(View.VISIBLE);
                groupinfo_notifysetting_2_link.setVisibility(View.INVISIBLE);
                break;
            case 2:
                groupinfo_notifysetting_0_link.setVisibility(View.INVISIBLE);
                groupinfo_notifysetting_1_link.setVisibility(View.INVISIBLE);
                groupinfo_notifysetting_2_link.setVisibility(View.VISIBLE);
                break;
        }
    }


    public void setNotifySetting(final JsonSetGroupInfoNotifySetting jsonSetGroupInfoNotifySetting) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                String jsonStr = gson.toJson(jsonSetGroupInfoNotifySetting, JsonSetGroupInfoNotifySetting.class);

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_INFO + jsonSetGroupInfoNotifySetting.groupId + MyConfig.API2_GROUP_INFO_NOTIFY_SETTING_2, jsonStr);

                final JsonSetGroupInfoNotifySettingRet jsonSetGroupInfoNotifySettingRet = gson.fromJson(s, JsonSetGroupInfoNotifySettingRet.class);
                MyLog.d("", "HttpTools: JsonLeaveRet " + s);
                if (jsonSetGroupInfoNotifySettingRet != null && jsonSetGroupInfoNotifySettingRet.code==MyConfig.retSuccess()) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            Toast.makeText(
//                                    getActivity(),
//                                    jsonSetGroupInfoNotifySettingRet.message,
//                                    Toast.LENGTH_LONG).show();
                            MyConfig.MyToast(-1, getActivity(),
                                    jsonSetGroupInfoNotifySettingRet.message);
                        }
                    });

                }

            }
        }).start();

    }


    @Override
    public void onResume() {
        super.onResume();
        getNotifySetting();

    }

    private void getNotifySetting(){

        new Thread(new Runnable() {
            @Override
            public void run() {

                Gson gson = new Gson();

                MyLog.d("","jsonGetGroupInfoNotifySettingRet 3 groupId "+groupId);

                String s = new HttpTools().httpURLConnGet(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GROUP_INFO + groupId + MyConfig.API2_GROUP_INFO_NOTIFY_SETTING_2, MyConfig.usr_token);
                MyLog.d("", "HttpTools: jsonGetGroupInfoNotifySettingRet " + groupId + " users: " + s);

                final JsonGetGroupInfoNotifySettingRet jsonGetGroupInfoNotifySettingRet = gson.fromJson(s, JsonGetGroupInfoNotifySettingRet.class);


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(jsonGetGroupInfoNotifySettingRet != null && jsonGetGroupInfoNotifySettingRet.code==MyConfig.retSuccess()) {
                            updateUi(jsonGetGroupInfoNotifySettingRet.data.level);
                        }
                    }
                });

            }
        }).start();

    }




}
