package co.docy.oldfriends.Fragments;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.support.v7.widget.SearchView;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Adapters.ListAdapterDevicePhoneNumber;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.DevicePhoneContact;
import co.docy.oldfriends.EventBus.EventGroupListUpdateUi;
import co.docy.oldfriends.R;

public class _FragmentPhoneContact extends FragmentBase implements SearchView.OnQueryTextListener, SearchView.OnCloseListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * 本fragment提供手机通讯录
     *
     *  目前提供单选，通过clickListener返回
     *
     *  以后考虑类似小集体通讯录，提供三种选择模式，单选、多选、单选并立即返回
     *
     */


    List<DevicePhoneContact> list = new LinkedList<>();
    List<DevicePhoneContact> templist = new LinkedList<>();

    ListView fragment_device_contact_list;

    // This is the Adapter being used to display the list's data.
    ListAdapterDevicePhoneNumber mAdapter;

    // The SearchView for doing filtering.
    SearchView mSearchView;

    // If non-null, this is the current filter the user has provided.
    String mCurFilter;

    public static _FragmentPhoneContact newInstance(OnClickedListener clickListener) {
        _FragmentPhoneContact f = new _FragmentPhoneContact();
        f.clickListener = clickListener;

        return f;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_device_contact_list, null);

        fragment_device_contact_list = (ListView) v.findViewById(R.id.fragment_device_contact_list);
        return v;
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
// We have a menu item to show in action bar.
        setHasOptionsMenu(true);


        // Create an empty adapter we will use to display the loaded data.
        mAdapter = new ListAdapterDevicePhoneNumber(getActivity(), list, null, null);

        fragment_device_contact_list.setAdapter(mAdapter);


        // Prepare the loader.  Either re-connect with an existing one,
        // or start a new one.
        getLoaderManager().initLoader(0, null, this);
    }


    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onClose() {
        if (!TextUtils.isEmpty(mSearchView.getQuery())) {
            mSearchView.setQuery(null, true);
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void onEventMainThread(EventGroupListUpdateUi eventGroupListUpdateUi) {
        MyLog.d("", "eventbus: onEventMainThread in FragmentGroupList.java");

    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Place an action bar item for searching.
        MenuItem item = menu.add("Search");
        item.setIcon(android.R.drawable.ic_menu_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM
                | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        mSearchView = new MyConfig.MySearchView(getActivity());
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        item.setActionView(mSearchView);
    }


    public boolean onQueryTextChange(String newText) {
        // Called when the action bar search text has changed.  Update
        // the search filter, and restart the loader to do a new query
        // with this filter.
        String newFilter = !TextUtils.isEmpty(newText) ? newText : null;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (mCurFilter == null && newFilter == null) {
            return true;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {
            return true;
        }
        mCurFilter = newFilter;
        getLoaderManager().restartLoader(0, null, this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // Don't care about this.
        return true;
    }


    // These are the Contacts rows that we will retrieve.
    static final String[] CONTACTS_SUMMARY_PROJECTION = new String[]{
            ContactsContract.Contacts._ID,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.Contacts.CONTACT_STATUS,
            ContactsContract.Contacts.CONTACT_PRESENCE,
            ContactsContract.Contacts.PHOTO_ID,
            ContactsContract.Contacts.LOOKUP_KEY,
    };


    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // This is called when a new Loader needs to be created.  This
        // sample only has one Loader, so we don't care about the ID.
        // First, pick the base URI to use depending on whether we are
        // currently filtering.
        Uri baseUri;
        if (mCurFilter != null) {
            baseUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_FILTER_URI,
                    Uri.encode(mCurFilter));
        } else {
            baseUri = ContactsContract.Contacts.CONTENT_URI;
        }

        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        String select = "((" + ContactsContract.Contacts.DISPLAY_NAME + " NOTNULL) AND ("
                + ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1) AND ("
                + ContactsContract.Contacts.DISPLAY_NAME + " != '' ))";
        return new CursorLoader(getActivity(), baseUri,
                CONTACTS_SUMMARY_PROJECTION, select, null,
                ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
    }

    public InputStream openDisplayPhoto(long contactId) {
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri displayPhotoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.DISPLAY_PHOTO);
        try {
            AssetFileDescriptor fd =
                    getActivity().getContentResolver().openAssetFileDescriptor(displayPhotoUri, "r");
            return fd.createInputStream();
        } catch (IOException e) {
            return null;
        }
    }

    public void onLoadFinished(Loader<Cursor> loader, final Cursor cursor) {

        if (cursor == null) {
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    templist = new LinkedList<DevicePhoneContact>();

                    while (cursor.moveToNext()) {

                        DevicePhoneContact devicePhoneContact = new DevicePhoneContact();

                        String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        devicePhoneContact.contactId = contactId;
                        devicePhoneContact.photo_bitmap = BitmapFactory.decodeStream(openDisplayPhoto(Long.parseLong(devicePhoneContact.contactId)));
                        String userName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        devicePhoneContact.name = userName;
                        String photo_id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_ID));
                        devicePhoneContact.photo_id = photo_id;

                        if (devicePhoneContact.photo_bitmap != null) {
//                            MyLog.d("", "contact avatar " + photo_id + " " + phoneContactNamePhone.photo_bitmap.getWidth() + " " + phoneContactNamePhone.photo_bitmap.getHeight());
                        }
                        /**
                         * 一个人可能有多个电话，不过我们只取第一个
                         */
                        Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                        if (phones != null) {
                            while (phones.moveToNext()) {

                                String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                                MyLog.d("", "phone number " + number);
                                int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                                switch (type) {
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                        break;
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                        break;
                                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                        break;
                                }
                                devicePhoneContact.phone = number;
                                break;//我们只取第一个电话号码
                            }
                            phones.close();
                        }

                        templist.add(devicePhoneContact);
                    }
                    cursor.close();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            list = templist;

                            mAdapter = new ListAdapterDevicePhoneNumber(getActivity(), list, new ListAdapterDevicePhoneNumber.OnClickedListener() {
                                @Override
                                public void OnClicked(int position) {
//                                    MyLog.d("", "invite friend: device contact list clicked 1");
                                    if (clickListener != null) {
                                        clickListener.OnClicked(list.get(position));
//                                        MyLog.d("", "invite friend: device contact list clicked 2");
                                    }
                                }
                            }, null);
                            fragment_device_contact_list.setAdapter(mAdapter);

                        }
                    });
                } catch (Exception e) {
                    MyLog.d("", "onLoadFinished Exception: " + e.toString());
                }

            }
        }).start();


//        mAdapter.notifyDataSetChanged();

    }

    private void getContact(DevicePhoneContact devicePhoneContact) {


    }

    public void onLoaderReset(Loader<Cursor> loader) {
        // This is called when the last Cursor provided to onLoadFinished()
        // above is about to be closed.  We need to make sure we are no
        // longer using it.
        mAdapter.notifyDataSetChanged();
    }


    public interface OnClickedListener {
        public void OnClicked(DevicePhoneContact devicePhoneContact);
    }
    public OnClickedListener clickListener;

}
