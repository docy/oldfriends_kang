package co.docy.oldfriends.Fragments;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import co.docy.oldfriends.Activitis._ActivityAccount;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.Country;
import co.docy.oldfriends.Messages.JsonSmsSignup;
import co.docy.oldfriends.Messages.JsonSmsSignupRet;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views._DialogCountryPicker;

public class _FragmentV4AccountRegist1 extends FragmentBaseV4 {

    public EditText fragment_page_account_regist_1_phone;
    ImageView fragment_page_account_regist_1_next, fragment_page_account_regist_1_country_logo;
    public TextView fragment_page_account_regist_1_country_prefix;
     ProgressBar fragment_page_account_regist_1_loading;

    public static _FragmentV4AccountRegist1 newInstance() {
        _FragmentV4AccountRegist1 f = new _FragmentV4AccountRegist1();

        return f;
    }

    public View mView;
    public void setVisibile(int visibile){
        mView.setVisibility(visibile);
    }
    public void setY(float y){
        mView.setY(y);
    }
    public float getY(){
        return mView.getY();
    }


    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_page_account_regist_1, null);
        mView = v;

        fragment_page_account_regist_1_loading = (ProgressBar) v.findViewById(R.id.fragment_page_account_regist_1_loading);
        fragment_page_account_regist_1_phone = (EditText) v.findViewById(R.id.fragment_page_account_regist_1_phone);
        fragment_page_account_regist_1_next = (ImageView) v.findViewById(R.id.fragment_page_account_regist_1_next);

//        Country country = MyConfig.getCountryByCode(MyConfig.app_country_code, MyConfig.supportedCountriesList);
        fragment_page_account_regist_1_country_logo = (ImageView)v.findViewById(R.id.fragment_page_account_regist_1_country_logo);
        Picasso.with(getActivity()).load(MyConfig.app_country.flag).into(fragment_page_account_regist_1_country_logo);
        fragment_page_account_regist_1_country_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new _DialogCountryPicker(getActivity(), MyConfig.screenWidth / 2, MyConfig.screenHeight / 2, new _DialogCountryPicker.CountryChoose() {
                    @Override
                    public void choosed(Country country) {
                        MyConfig.app_country = country;
                        Picasso.with(getActivity()).load(country.flag).into(fragment_page_account_regist_1_country_logo);
                        fragment_page_account_regist_1_country_prefix.setText(MyConfig.app_country.phone_prefix);
                    }
                }).show();
            }
        });
        fragment_page_account_regist_1_country_prefix = (TextView)v.findViewById(R.id.fragment_page_account_regist_1_country_prefix);
        fragment_page_account_regist_1_country_prefix.setText(MyConfig.app_country.phone_prefix);

        fragment_page_account_regist_1_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                Pattern pattern = Pattern.compile("[0-9]{10}");
                Pattern pattern;
                if(MyConfig.app_country.code.equals("CN")){
                    pattern = Pattern.compile("1[0-9]{10}");//中国手机11位
                } else{
                    pattern = Pattern.compile("[0-9]{10}");//北美手机10位
                }
                Matcher matcher = pattern.matcher(fragment_page_account_regist_1_phone.getText().toString());
                if (matcher.matches()) {
                    fragment_page_account_regist_1_next.setVisibility(View.VISIBLE);
                } else {
                    fragment_page_account_regist_1_next.setVisibility(View.GONE);
                }

            }
        });
        fragment_page_account_regist_1_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment_page_account_regist_1_loading.setVisibility(View.VISIBLE);
                smsSignup();
              //  ((ActivityAccount)getActivity()).showFragmentRegist2();
            }
        });


        return v;
    }


    private void smsSignup() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();

                JsonSmsSignup jsonSmsSignup = new JsonSmsSignup();
                jsonSmsSignup.phone = fragment_page_account_regist_1_phone.getText().toString().trim();
                jsonSmsSignup.countryCode = MyConfig.app_country.phone_prefix;


                final String jsonStr = gson.toJson(jsonSmsSignup, JsonSmsSignup.class);
                MyLog.d("", "httptools: send country " + jsonStr);
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        MyConfig.MyToast(0, getActivity(), jsonStr);
//                    }
//                });

                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SMS_SIGNUP, jsonStr);
                MyLog.d("", "apitest: send msg ret " + s);

                final JsonSmsSignupRet jsonSmsSignupRet = gson.fromJson(s, JsonSmsSignupRet.class);
                if (jsonSmsSignupRet != null) {

                    if (jsonSmsSignupRet.code == MyConfig.retSuccess()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MyConfig.hide_keyboard_must_call_from_activity(getActivity());
                                ((_ActivityAccount) getActivity()).showFragmentRegist2();
                                fragment_page_account_regist_1_loading.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MyConfig.MyToast(0, getActivity(), MyConfig.getErrorMsg("" + jsonSmsSignupRet.code));
                                fragment_page_account_regist_1_loading.setVisibility(View.GONE);
                            }
                        });

                    }
                } else {
                    MyConfig.MyToast(0, getActivity(), getResources().getString(R.string.server_connect_fail));
                }

            }
        }).start();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register
        //EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();



    }



}
