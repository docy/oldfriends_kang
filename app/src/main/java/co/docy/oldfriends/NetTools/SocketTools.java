package co.docy.oldfriends.NetTools;

import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import java.net.URISyntaxException;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;

/**
 * Created by youhy on 5/29/15.
 */
public class SocketTools {

    private static Socket mSocket = null;

    protected SocketTools() {

    }

//    public static void


    public static void info(){
        MyLog.d("","socketio switch: toString "+mSocket.toString());
        MyLog.d("","socketio switch: connected "+mSocket.connected());
    }

    public static void init(){
        try {
            mSocket = IO.socket(MyConfig.getRtmUrl());
            MyLog.d("","socketio switch: "+mSocket.toString());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
    // Lazy Initialization (If required then only)
    public static synchronized SocketTools getInstance() {
        if(mSocket == null) {
            try {
                mSocket = IO.socket(MyConfig.getRtmUrl());
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }
        return new SocketTools();
    }

    public static void authenticate() {
        String sb = new StringBuilder("{\"token\":\"" + MyConfig.usr_token + "\"}").toString();
        MyLog.d("", "socketio in service: ========= authenticate ack back 1 " + sb);

        JSONObject arrays[] = new JSONObject[1];
        try {
            arrays[0] = new JSONObject(sb);
        } catch (Exception e) {

        }

        mSocket.emit("authenticate", arrays, new Ack() {
            public void call(Object... args) {

                MyLog.d("", "socketio in service: ========= authenticate ack back 2 " + args.toString());


                MyConfig.rtm_status = 2;//已经连接并且认证通过

            }
        });

        MyLog.d("", "socketio in service: ========== authenticate ack back 3 ");

        MyConfig.rtm_status = 2;//todo 实际上没有获取到ack，这里假定验证成功

    }

    public static void disconnect(){
        mSocket.disconnect();
    }

    public static void connect(){
        mSocket.connect();
    }

    public static boolean hasListener(){
        return mSocket.hasListeners(MyConfig.rtm_tag);
    }

    public static void addListener(){
        mSocket.off();
        mSocket.on(MyConfig.rtm_tag, MyConfig.rtm_listener);
        MyLog.d("", "socketio switch: connect() @"+mSocket+" #" + mSocket.listeners(MyConfig.rtm_tag).toString());
    }

    public static boolean connected(){
//        MyLog.d("", "socketio in service: return connected() as " + mSocket.connected());
        return mSocket.connected();
    }
}
