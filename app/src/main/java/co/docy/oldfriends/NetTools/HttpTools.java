package co.docy.oldfriends.NetTools;

/**
 * Created by youhy on 5/29/15.
 */


import com.google.gson.Gson;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.IMsg;
import co.docy.oldfriends.Messages.JsonCreateCompany;
import co.docy.oldfriends.Messages.JsonCreateCompanyRet;
import co.docy.oldfriends.Messages.JsonCreateGroup;
import co.docy.oldfriends.Messages.JsonCreateGroupRet;
import co.docy.oldfriends.Messages.JsonFriendCricleCommonRet;
import co.docy.oldfriends.Messages.JsonRegistRet;
import co.docy.oldfriends.Messages.JsonRegistWithPhoto;
import co.docy.oldfriends.Messages.JsonRegistWithPhotoTongChuang;
import co.docy.oldfriends.Messages.JsonFriendCricleSendNewContentRet;
import co.docy.oldfriends.Messages.JsonSendFileRet;
import co.docy.oldfriends.Messages.JsonSendImageRet;
import co.docy.oldfriends.Messages.JsonSendMapRet;
import co.docy.oldfriends.Messages.JsonSendSoundCommentRet;
import co.docy.oldfriends.Messages.JsonSendSoundMsgRet;
import co.docy.oldfriends.Messages.JsonSendVideoRet;
import co.docy.oldfriends.Messages.JsonUploadFileRet;
import co.docy.oldfriends.Messages.JsonUploadImageRet;
import co.docy.oldfriends.Messages.Json_DirectRoom_SendTopicImageRet;
import co.docy.oldfriends.Messages.Json_SubRoom_SendCommentImageRet;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpTools {

    /**
     * todo
     * post的时候token也放在header里面
     */
    public HttpTools() {

    }


    /**
     * ==== following is retrofit support =======
     */

//    public static okhttp3.OkHttpClient okHttpClient = new okhttp3.OkHttpClient.Builder().addNetworkInterceptor(new okhttp3.Interceptor() {
//        @Override
//        public okhttp3.Response intercept(Chain chain) throws IOException {
//            okhttp3.Request request = chain.request();
//            MyLog.d("", "retrofit: intercept() " + request.headers().toString());
//            request = request.newBuilder()
//                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
//                    .build();
//
//            okhttp3.Response response = chain.proceed(request);
//            MyLog.d("", "retrofit: intercept() ret code " + response.toString());
//
//            return response;
//        }
//    }).build();
//
//    public static Retrofit retrofit = new Retrofit.Builder()
//            .client(okHttpClient)
//            .baseUrl(MyConfig.getApiDomain_HaveSlash_ApiV2())
//            .addConverterFactory(GsonConverterFactory.create())
//            .build();


    public static Retrofit getRetrofit(){

        okhttp3.Interceptor interceptor = new okhttp3.Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                okhttp3.Request request = chain.request();
                MyLog.d("", "retrofit: intercept() header 1:" + request.headers().toString());
                request = request.newBuilder()
                        .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                        .build();
                MyLog.d("", "retrofit: intercept() header 2:" + request.headers().toString());

                okhttp3.Response response = chain.proceed(request);
                MyLog.d("", "retrofit: intercept() ret code " + response.toString());

                return response;
            }
        };

        okhttp3.OkHttpClient okHttpClient = new okhttp3.OkHttpClient.Builder().addNetworkInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(MyConfig.getApiDomain_HaveSlash_ApiV2())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        return retrofit;

    }

    public static MyConfig.IF_HTTP_API http_api_service(){
        return getRetrofit().create(MyConfig.IF_HTTP_API.class);
    }


    /**
     * ==== following method use okhttp =======
     */


    public static File okhttpGetFile(String url, File toFile) {

        File ret;

        try {
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .build();
            Response response = new OkHttpClient().newCall(request).execute();

            InputStream is = response.body().byteStream();
            BufferedInputStream input = new BufferedInputStream(is, 1024);
            OutputStream output = new FileOutputStream(toFile);

            byte[] data = new byte[1024];

            int count;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();

            ret = toFile;
        } catch (IOException e) {
            ret = null;
        }

        if (ret == null) {
            MyConfig.notifyNetworkError();
        }

        return ret;
    }

    public static JsonFriendCricleCommonRet okhttpUploadFriendHeadImage(File file) {
        JsonFriendCricleCommonRet jsonSendCricleOfFriendRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;

        try {
            MultipartBuilder multipartBuilder = new MultipartBuilder()
                    .type(MultipartBuilder.FORM);

            multipartBuilder.addPart(
                    Headers.of("Content-Disposition", "form-data; name=\"image\";filename=\"" + file.getName() + "\""),
                    RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), file));

            RequestBody requestBody = multipartBuilder.build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CHANGE_FRIEND_LIKE + MyConfig.API_GET_DATA_OF_FRIEND_WITH_HEAD)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                ret = response.body().string();
                MyLog.d("", "OKHTTP image response code: " + ret);

                Gson gson = new Gson();
                jsonSendCricleOfFriendRet = gson.fromJson(ret, JsonFriendCricleCommonRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP image: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonSendCricleOfFriendRet;
    }


    public static JsonFriendCricleSendNewContentRet okhttpUploadFriendImage(String os,String title, File file) {
        JsonFriendCricleSendNewContentRet jsonSendCricleOfFriendRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;

        try {
            MultipartBuilder multipartBuilder = new MultipartBuilder()
                .type(MultipartBuilder.FORM);
            if(os!=null) {
                multipartBuilder.addFormDataPart(MyConfig.CONST_PARAM_STR_OS, os);
            }
            if (title!=null) {
                multipartBuilder.addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_TITLE, "" + title);
            }
            multipartBuilder.addPart(
                    Headers.of("Content-Disposition", "form-data; name=\"file\";filename=\"" + file.getName() + "\""),
                    RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), file));

            RequestBody requestBody = multipartBuilder.build();

            MyLog.d("kanghongpu",file.getAbsolutePath()+title);

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_GET_DATA_OF_FRIEND)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                ret = response.body().string();
                MyLog.d("", "OKHTTP image response code: " + ret);

                Gson gson = new Gson();
                jsonSendCricleOfFriendRet = gson.fromJson(ret, JsonFriendCricleSendNewContentRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP image: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonSendCricleOfFriendRet;
    }

    public static JsonCreateCompanyRet okhttpCreateCompanyWithPhoto(JsonCreateCompany jsonCreateCompany) {

        MyLog.d("", "OKHTTP okhttpCreateGroupWithPhoto");

        JsonCreateCompanyRet jsonCreateCompanyRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;

        try {
            MultipartBuilder multipartBuilder = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_NAME, "" + jsonCreateCompany.name)
                    .addFormDataPart("phone", "" + jsonCreateCompany.phone)
                    .addFormDataPart("category", "" + jsonCreateCompany.category)
                    .addFormDataPart("city", "" + jsonCreateCompany.city)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_DESC, "" + jsonCreateCompany.desc)
                    .addFormDataPart("isPrivate", "" + jsonCreateCompany.isPrivate);

            if (jsonCreateCompany.logoUrl != null) {
                multipartBuilder.addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"logoUrl\";filename=\"" + jsonCreateCompany.logoUrl.getName() + "\""),
                        RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), jsonCreateCompany.logoUrl));
                MyLog.d("", "create company avatar: logoUrl " + jsonCreateCompany.logoUrl.getAbsolutePath());

            }

            RequestBody requestBody = multipartBuilder.build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CREATE_COMPANY)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                ret = response.body().string();
                MyLog.d("", "OKHTTP okhttpCreateGroupWithPhoto response code: " + ret);

                Gson gson = new Gson();
                jsonCreateCompanyRet = gson.fromJson(ret, JsonCreateCompanyRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP okhttpCreateGroupWithPhoto: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonCreateCompanyRet;
    }

    public static JsonCreateGroupRet okhttpCreateGroupWithPhoto(JsonCreateGroup jsonCreateGroup) {

        MyLog.d("", "OKHTTP okhttpCreateGroupWithPhoto");

        JsonCreateGroupRet jsonCreateGroupRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;

        try {
            MultipartBuilder multipartBuilder = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_NAME, "" + jsonCreateGroup.name)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_DESC, "" + jsonCreateGroup.desc)
                    .addFormDataPart("category", "" + jsonCreateGroup.category)
                    .addFormDataPart("broadcast", "" + jsonCreateGroup.broadcast)
                    .addFormDataPart("joinType", "" + jsonCreateGroup.joinType);

            if (jsonCreateGroup.logo != null) {
                multipartBuilder.addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"logo\";filename=\"" + jsonCreateGroup.logo.getName() + "\""),
                        RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), jsonCreateGroup.logo));

            }

            RequestBody requestBody = multipartBuilder.build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_CREATE_ROOM)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                ret = response.body().string();
                MyLog.d("", "OKHTTP okhttpCreateGroupWithPhoto response code: " + ret);

                Gson gson = new Gson();
                jsonCreateGroupRet = gson.fromJson(ret, JsonCreateGroupRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP okhttpCreateGroupWithPhoto: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonCreateGroupRet;
    }


    public static JsonRegistRet okhttpUploadRegistWithPhotoTongChuang(JsonRegistWithPhotoTongChuang jsonRegistWithPhoto) {

        MyLog.d("", "OKHTTP okhttpUploadRegistWithPhoto");

        JsonRegistRet jsonRegistRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;

        try {
            MultipartBuilder multipartBuilder = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
//                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_NAME, "" + jsonRegistWithPhoto.name)
//                    .addFormDataPart("email", "" + jsonRegistWithPhoto.email)
                    .addFormDataPart("password", "" + jsonRegistWithPhoto.password)
                    .addFormDataPart("phone", "" + jsonRegistWithPhoto.phone)
                    .addFormDataPart("code", "" + jsonRegistWithPhoto.code)
                    .addFormDataPart("countryCode", "" + jsonRegistWithPhoto.countryCode)
                    .addFormDataPart("sex", "" + jsonRegistWithPhoto.sex)
                    .addFormDataPart("lang", "" + jsonRegistWithPhoto.lang);


            MyLog.d("", "person sex regist " + jsonRegistWithPhoto.sex);
            MyLog.d("", "countryCode " + jsonRegistWithPhoto.countryCode);

            if (jsonRegistWithPhoto.avatar != null) {
                multipartBuilder.addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"avatar\";filename=\"" + jsonRegistWithPhoto.avatar.getName() + "\""),
                        RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), jsonRegistWithPhoto.avatar));

            }

            RequestBody requestBody = multipartBuilder.build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_REGIST)
//                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                ret = response.body().string();
                MyLog.d("", "OKHTTP okhttpUploadRegistWithPhoto response code: " + ret);

                Gson gson = new Gson();
                jsonRegistRet = gson.fromJson(ret, JsonRegistRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP okhttpUploadRegistWithPhoto: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonRegistRet;
    }

    public static JsonRegistRet okhttpUploadRegistWithPhoto(JsonRegistWithPhoto jsonRegistWithPhoto) {

        MyLog.d("", "OKHTTP okhttpUploadRegistWithPhoto");

        JsonRegistRet jsonRegistRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;

        try {
            MultipartBuilder multipartBuilder = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_NAME, "" + jsonRegistWithPhoto.name)
//                    .addFormDataPart("email", "" + jsonRegistWithPhoto.email)
                    .addFormDataPart("password", "" + jsonRegistWithPhoto.password)
                    .addFormDataPart("phone", "" + jsonRegistWithPhoto.phone)
                    .addFormDataPart("code", "" + jsonRegistWithPhoto.code)
                    .addFormDataPart("countryCode", "" + jsonRegistWithPhoto.countryCode)
                    .addFormDataPart("sex", "" + jsonRegistWithPhoto.sex)
                    .addFormDataPart("lang", "" + jsonRegistWithPhoto.lang);


            MyLog.d("", "person sex regist " + jsonRegistWithPhoto.sex);
            MyLog.d("", "countryCode " + jsonRegistWithPhoto.countryCode);

            if (jsonRegistWithPhoto.avatar != null) {
                multipartBuilder.addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"avatar\";filename=\"" + jsonRegistWithPhoto.avatar.getName() + "\""),
                        RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), jsonRegistWithPhoto.avatar));

            }

            RequestBody requestBody = multipartBuilder.build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_REGIST)
//                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                ret = response.body().string();
                MyLog.d("", "OKHTTP okhttpUploadRegistWithPhoto response code: " + ret);

                Gson gson = new Gson();
                jsonRegistRet = gson.fromJson(ret, JsonRegistRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP okhttpUploadRegistWithPhoto: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonRegistRet;
    }


    public static JsonSendVideoRet okhttpUploadSubjectVideo(IMsg iMsg, String mimeString) {

//        MyLog.d("", "httptools video iMsg.title: " + iMsg.title);

        JsonSendVideoRet jsonSendVideoRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;
        File f = iMsg.attached;

        try {

            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, "" + iMsg.groupId)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_TITLE, "" + iMsg.title)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_DESC, "" + iMsg.body)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_CLIENTID, "" + iMsg.clientId)
                    .addFormDataPart(MyConfig.CONST_PARAM_STR_OS, MyConfig.os_string)
                    .addPart(
                            Headers.of("Content-Disposition", "form-data; name=\"video\";filename=\"" + f.getName() + "\""),
                            RequestBody.create(MediaType.parse(MyConfig.MIME_FILE_STAR), f))
                    .build();

//        MyLog.d("", "httptools video iMsg.title 1: " + iMsg.title);

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_VIDEO)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

//        MyLog.d("", "httptools video iMsg.title 2: " + iMsg.title);

            Response response = client.newCall(request).execute();

//            MyLog.d("", "httptools video share in process: response tostring: " + response.toString());

            if (response.isSuccessful()) {

                ret = response.body().string();
//                MyLog.d("", "httptools video share in process: response code: " + ret + " headers:"+request.headers().toString());

                Gson gson = new Gson();
                jsonSendVideoRet = gson.fromJson(ret, JsonSendVideoRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "share in process: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonSendVideoRet;
    }


    public static JsonSendFileRet okhttpUploadSubjectFile(IMsg iMsg, String mimeString) {

//        MyLog.d("", "share in process: iMsg.body: " + iMsg.body);

        JsonSendFileRet jsonSendFileRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;
        File f = iMsg.attached;

        try {

            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, "" + iMsg.groupId)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_TITLE, "" + iMsg.title)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_DESC, "" + iMsg.body)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_CLIENTID, "" + iMsg.clientId)
                    .addPart(
                            Headers.of("Content-Disposition", "form-data; name=\"file\";filename=\"" + f.getName() + "\""),
                            RequestBody.create(MediaType.parse(MyConfig.MIME_FILE_STAR), f))
                    .build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_FILE)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                ret = response.body().string();
//                MyLog.d("", "share in process: response code: " + ret + " headers:"+request.headers().toString());

                Gson gson = new Gson();
                jsonSendFileRet = gson.fromJson(ret, JsonSendFileRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "share in process: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonSendFileRet;
    }

    public static JsonSendImageRet okhttpUploadSubjectImage(IMsg iMsg, String mimeString) {

//        MyLog.d("", "OKHTTP image : " + iMsg.body);

        JsonSendImageRet jsonSendImageRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;
        File f = iMsg.attached;

        try {

            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, "" + iMsg.groupId)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_TITLE, "" + iMsg.title)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_DESC, "" + iMsg.body)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_NEWS, "" + iMsg.news)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_CLIENTID, "" + iMsg.clientId)
                    .addPart(
                            Headers.of("Content-Disposition", "form-data; name=\"image\";filename=\"" + f.getName() + "\""),
                            RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), f))
                    .build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_IMAGE)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                ret = response.body().string();
//                MyLog.d("", "OKHTTP image response code: " + ret);

                Gson gson = new Gson();
                jsonSendImageRet = gson.fromJson(ret, JsonSendImageRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP image: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonSendImageRet;
    }


    public static JsonSendSoundMsgRet okhttpUploadSoundMsg(IMsg iMsg, String mimeString) {

//        MyLog.d("", "OKHTTP sound : " + iMsg.attached.getAbsolutePath());

        JsonSendSoundMsgRet jsonSendSoundMsgRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;
        File f = iMsg.attached;
//        MyLog.d("", "OKHTTP sound f.length: " + f.length());

        try {

            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, "" + iMsg.groupId)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_CLIENTID, "" + iMsg.clientId)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_DURATION, "" + iMsg.duration)
                    .addPart(
                            Headers.of("Content-Disposition", "form-data; name=\"audio\";filename=\"" + f.getName() + "\""),
                            RequestBody.create(MediaType.parse(MyConfig.MIME_AUDIO_STAR), f))
                    .build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_UPLOAD_SOUND)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

//            MyLog.d("", "OKHTTP sound 1 ");
            Response response = client.newCall(request).execute();
//            MyLog.d("", "OKHTTP sound 2 ");

            if (response.isSuccessful()) {

                ret = response.body().string();
//                MyLog.d("", "OKHTTP sound response code: " + ret);

                Gson gson = new Gson();
                jsonSendSoundMsgRet = gson.fromJson(ret, JsonSendSoundMsgRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP sound: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonSendSoundMsgRet;
    }

    public static JsonSendSoundCommentRet okhttpUploadSoundComment(IMsg iMsg, String mimeString) {

//        MyLog.d("", "OKHTTP sound : " + iMsg.attached.getAbsolutePath());

        JsonSendSoundCommentRet jsonSendSoundCommentRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;
        File f = iMsg.attached;
//        MyLog.d("", "OKHTTP sound f.length: " + f.length());

        try {

            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, "" + iMsg.groupId)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_CLIENTID, "" + iMsg.clientId)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_DURATION, "" + iMsg.duration)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_AUDIO, "" + iMsg.audioPath)
                    .addPart(
                            Headers.of("Content-Disposition", "form-data; name=\"audio\";filename=\"" + f.getName() + "\""),
                            RequestBody.create(MediaType.parse(MyConfig.MIME_AUDIO_STAR), f))
                    .build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SEND_TOPIC_COMMENT + iMsg.topicId + MyConfig.API2_SEND_TOPIC_COMMENT_2)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

//            MyLog.d("", "OKHTTP sound 1 ");
            Response response = client.newCall(request).execute();
//            MyLog.d("", "OKHTTP sound 2 ");

            if (response.isSuccessful()) {

                ret = response.body().string();
//                MyLog.d("", "OKHTTP sound response code: " + ret);

                Gson gson = new Gson();
                jsonSendSoundCommentRet = gson.fromJson(ret, JsonSendSoundCommentRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP sound: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonSendSoundCommentRet;
    }

    public static Json_SubRoom_SendCommentImageRet okhttpUploadCommentImage(IMsg iMsg, String mimeString) {

//        MyLog.d("", "OKHTTP image : " + iMsg.body);

        Json_SubRoom_SendCommentImageRet json_subRoom_sendCommentImageRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;

//        MyLog.d("", "OKHTTP image 1");
        try {
            MultipartBuilder multipartBuilder = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
//                    .addFormDataPart("message", "" + iMsg.body)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_CLIENTID, "" + iMsg.clientId);
//            MyLog.d("", "OKHTTP image 2");

            File image = iMsg.attached;
            multipartBuilder.addPart(
                    Headers.of("Content-Disposition", "form-data; name=\"image\";filename=\"" + image.getName() + "\""),
                    RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), image));
//            MyLog.d("", "OKHTTP image 3");

            RequestBody requestBody = multipartBuilder.build();
//            MyLog.d("", "OKHTTP image 4");


//            RequestBody requestBody = new MultipartBuilder()
//                    .type(MultipartBuilder.FORM)
//                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, "" + iMsg.groupId)
//                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_CLIENTID, "" + iMsg.clientId)
//                    .addPart(
//                            Headers.of("Content-Disposition", "form-data; name=\"image\";filename=\"" + f.getName() + "\""),
//                            RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), f))
//                    .build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SEND_TOPIC_COMMENT + iMsg.topicId + MyConfig.API2_SEND_TOPIC_COMMENT_2)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();
//            MyLog.d("", "OKHTTP image 5");

            Response response = client.newCall(request).execute();
//            MyLog.d("", "OKHTTP image 6");

            if (response.isSuccessful()) {

                ret = response.body().string();
//                MyLog.d("", "OKHTTP image response code: " + ret);

                Gson gson = new Gson();
                json_subRoom_sendCommentImageRet = gson.fromJson(ret, Json_SubRoom_SendCommentImageRet.class);
            }
//            MyLog.d("", "OKHTTP image 7");

        } catch (IOException e) {
            MyLog.d("", "OKHTTP image: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return json_subRoom_sendCommentImageRet;
    }


    public static Json_DirectRoom_SendTopicImageRet okhttpUploadSubjectImage_DirectRoom(IMsg iMsg, String mimeString) {

        MyLog.d("", "OKHTTP image : " + iMsg.body);

        Json_DirectRoom_SendTopicImageRet json_directRoom_sendTopicImageRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;

        try {
            MultipartBuilder multipartBuilder = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, "" + iMsg.groupId)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_CLIENTID, "" + iMsg.clientId);


            if (iMsg.type == MyConfig.TYPE_TOPIC_IMAGE_DIRECTROOM) {
                File image = iMsg.attached;
                multipartBuilder.addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"image\";filename=\"" + image.getName() + "\""),
                        RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), image));

            } else if (iMsg.type == MyConfig.TYPE_TOPIC_MAP_DIRECTROOM) {
                File screenShot = new File(iMsg.mapLocation.screenShotPath);
                multipartBuilder
                        .addFormDataPart("longitude", "" + iMsg.mapLocation.longitude)
                        .addFormDataPart("latitude", "" + iMsg.mapLocation.latitude)
                        .addPart(
                                Headers.of("Content-Disposition", "form-data; name=\"image\";filename=\"" + screenShot.getName() + "\""),
                                RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), screenShot));

            }

            RequestBody requestBody = multipartBuilder.build();


//            RequestBody requestBody = new MultipartBuilder()
//                    .type(MultipartBuilder.FORM)
//                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, "" + iMsg.groupId)
//                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_CLIENTID, "" + iMsg.clientId)
//                    .addPart(
//                            Headers.of("Content-Disposition", "form-data; name=\"image\";filename=\"" + f.getName() + "\""),
//                            RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), f))
//                    .build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SEND_IMAGE_DIRECTROOM)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                ret = response.body().string();
                MyLog.d("", "OKHTTP image response code: " + ret);

                Gson gson = new Gson();
                json_directRoom_sendTopicImageRet = gson.fromJson(ret, Json_DirectRoom_SendTopicImageRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP image: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return json_directRoom_sendTopicImageRet;
    }

    public static JsonSendMapRet okhttpUploadSubjectMap(IMsg iMsg) {

        MyLog.d("", "OKHTTP map : " + iMsg.body);

        JsonSendMapRet jsonSendMapRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;

        try {
            MyLog.d("", "OKHTTP map : iMsg.mapLocation" + iMsg.mapLocation);


            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_GROUPID, "" + iMsg.groupId)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_TITLE, "" + iMsg.title)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_DESC, "" + iMsg.body)
                    .addFormDataPart(MyConfig.CONSTANT_PARAMETER_STRING_CLIENTID, "" + iMsg.clientId)
                    .addFormDataPart("longitude", "" + iMsg.mapLocation.longitude)
                    .addFormDataPart("latitude", "" + iMsg.mapLocation.latitude)
                    .addPart(
                            Headers.of("Content-Disposition", "form-data; name=\"image\";filename=\"" + iMsg.mapLocation.screenShotPath + "\""),
                            RequestBody.create(MediaType.parse(MyConfig.MIME_IMAGE_STAR), new File(iMsg.mapLocation.screenShotPath)))
                    .build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_SUBJECT_MAP)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                ret = response.body().string();
                MyLog.d("", "OKHTTP map response code: " + ret);

                Gson gson = new Gson();
                jsonSendMapRet = gson.fromJson(ret, JsonSendMapRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP map: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonSendMapRet;
    }


    public static JsonUploadImageRet okhttpUploadImage(File f, String mimeString) {
        JsonUploadImageRet jsonUploadImageRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;

        try {

            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)

                    .addPart(
                            Headers.of("Content-Disposition", "form-data; name=\"image\";filename=\"" + f.getName() + "\""),
                            RequestBody.create(MediaType.parse(mimeString), f))
                    .build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_UPLOAD_IMAGE)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                ret = response.body().string();
                MyLog.d("", "OKHTTP response code: " + ret);

                Gson gson = new Gson();
                jsonUploadImageRet = gson.fromJson(ret, JsonUploadImageRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonUploadImageRet;
    }


    public static JsonUploadFileRet okhttpUploadFile(File f, String mimeString) {
        JsonUploadFileRet jsonUploadFileRet = null;
        OkHttpClient client = new OkHttpClient();
        String ret = null;

        try {

            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)

                    .addPart(
                            Headers.of("Content-Disposition", "form-data; name=\"file\";filename=\"" + f.getName() + "\""),
                            RequestBody.create(MediaType.parse(mimeString), f))
                    .build();

            Request request = new Request.Builder()
                    .url(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_UPLOAD_FILE)
                    .addHeader("Authorization", "Bearer " + MyConfig.usr_token)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {

                ret = response.body().string();
                MyLog.d("", "OKHTTP response code: " + ret);

                Gson gson = new Gson();
                jsonUploadFileRet = gson.fromJson(ret, JsonUploadFileRet.class);
            }

        } catch (IOException e) {
            MyLog.d("", "OKHTTP: exception " + e.toString());
        }

        HttpRetStringCheck(ret);

        return jsonUploadFileRet;
    }


    /**
     * ==== following method use HttpURLConnection =======
     */


    /**
     * http get url
     */
    public String httpURLConnGet(String url, String token) {
        String ret = getRetString(doGetActionAndReturnInputStream(url, token));

        HttpRetStringCheck(ret);

        return ret;
    }

    public String httpURLConnGetWithKeyValue(String url, HashMap<String, String> map, String token) {
        String ret = getRetString(doGetActionAndReturnInputStream(url + "?" + getPostDataString(map), token));

        HttpRetStringCheck(ret);

        return ret;
    }

    public InputStream doGetActionAndReturnInputStream(String url_address, String token) {
        InputStream is = null;

        try {
            URL url = new URL(url_address);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
//            conn.setRequestProperty("accessToken", token);
//            conn.addRequestProperty("accessToken", token);
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.connect();

            int response = conn.getResponseCode();
            MyLog.d("", "HttpTools response: " + response);
            if (response != HttpURLConnection.HTTP_OK) {
                return null;
            }

            is = conn.getInputStream();
            return is;
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return null;
        }
    }


    /**
     * http delete json
     * <p/>
     * 目前token放在json字符串里面，或许应该统一放在http header里面
     */
    public String httpURLConnDeleteJson(String url, String json) {
        return getRetString(usingUrlConnDeleteJson(url, json));
    }

    public InputStream usingUrlConnDeleteJson(String requestURL, String s) {
        return doDeleteActionAndReturnInputStream(requestURL, s, "application/json;charset=utf-8");
    }

    private InputStream doDeleteActionAndReturnInputStream(String requestURL, String body, String bodyType) {

        /**
         * TODO:
         *  合并这三者
         *      doDeleteActionAndReturnInputStream
         *      doPostActionAndReturnInputStream
         *      doGetActionAndReturnInputStream
         */

        URL url;
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setReadTimeout(15000);
//            conn.setConnectTimeout(15000);
            conn.setRequestMethod("DELETE");
//            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", bodyType);


//            OutputStream os = conn.getOutputStream();
//            BufferedWriter writer = new BufferedWriter(
//                    new OutputStreamWriter(os, "UTF-8"));
//            writer.write(body);
//            writer.flush();
//            writer.close();
//            os.close();

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                MyLog.d("", "HttpTools: response code doDeleteActionAndReturnInputStream ok " + responseCode);
                return conn.getInputStream();
            } else {
                MyLog.d("", "HttpTools: response code doDeleteActionAndReturnInputStream" + responseCode);
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * http post json
     * <p/>
     * 目前token放在json字符串里面，或许应该统一放在http header里面
     */
    public String httpURLConnPostJson(String url, String json) {
        String ret = getRetString(usingUrlConnPostJson(url, json));

        HttpRetStringCheck(ret);

        return ret;
    }

    public static void HttpRetStringCheck(String ret) {
        if (ret == null) {
            MyConfig.notifyNetworkError();
        }else{
            MyConfig.HttpRetCodeCheck(ret);
        }
    }

    public InputStream usingUrlConnPostJson(String requestURL, String s) {
        return doPostActionAndReturnInputStream(requestURL, s, "application/json;charset=utf-8");
    }

    /**
     * http post key-value
     */
    public String httpURLConnPostKeyValue(String url, HashMap<String, String> map) {
        String ret = getRetString(usingUrlConnPostKeyValue(url, map));

        if (ret == null) {
            MyConfig.notifyNetworkError();
        }

        return ret;
    }

    public InputStream usingUrlConnPostKeyValue(String requestURL, HashMap<String, String> postDataParams) {
        return doPostActionAndReturnInputStream(requestURL, getPostDataString(postDataParams), "text/plain");
    }


    /**
     * HTTP stream process
     */
    private InputStream doPostActionAndReturnInputStream(String requestURL, String body, String bodyType) {
        URL url;
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", bodyType);
//            conn.setFixedLengthStreamingMode(body.length());


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(body);

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                return conn.getInputStream();
            } else {
                MyLog.d("", "HttpTools: response code " + responseCode);
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getPostDataString(HashMap<String, String> params) {//throws UnsupportedEncodingException {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        try {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
            return result.toString();
        } catch (UnsupportedEncodingException e) {
            return null;
        }

    }

    private String getRetString(InputStream is) {

        String buf;

        if (is == null) {
            return null;
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"));
            StringBuilder sb = new StringBuilder();
            String line = "";
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
//                MyLog.d("", "HttpTools line: " + line);
            }
            is.close();
            buf = sb.toString();
//            MyLog.d("", "HttpTools getRetString: " + buf);
            MyLog.d("", "HttpTools getRetString: " + buf);

            return buf;

        } catch (Exception e) {
            MyLog.d("", "HttpTools Exception: " + e.toString());
            return null;
        }
    }

}
