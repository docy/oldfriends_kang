package co.docy.oldfriends.DataClass;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonCompanyCanJoinRet;
import co.docy.oldfriends.Messages.JsonCreateCompanyRet;
import co.docy.oldfriends.Messages.JsonUserCompaniesRet;

/**
 * Created by youhy on 6/17/15.
 */
public class Company implements java.io.Serializable {

    /**
     * info from server
     */
//    JsonCompanyCanJoinRet.CompanyCanJoinRet companyCanJoinRet;
    public JsonCreateCompanyRet.CreateCompanyRet createCompanyRet = new JsonCreateCompanyRet.CreateCompanyRet();


    /**
     * addition info in JsonUserCompaniesRet
     */
    public String defaultGroup;
    public String feedbackGroup;
    public String broadcastGroup;
    public int creator;
    public String creatorNick;
    public String creatorName;
    public int userCount;
    public int unread;
    public int topicCount;

    public int groupCount;


    /**
     * info locally
     */

    /**
     * users
     */


    /**
     * room setting
     */
    public int status = -1;

    /**
     * joined already
     */
    public static Company fromUserCompanyRet(JsonUserCompaniesRet.UserCompaniesRet userCompaniesRet) {
        Company r = new Company();

        r.createCompanyRet.id = userCompaniesRet.id;
        r.createCompanyRet.name = userCompaniesRet.name;
        r.createCompanyRet.desc = userCompaniesRet.desc;
        r.createCompanyRet.domain = userCompaniesRet.domain;
        r.createCompanyRet.city = userCompaniesRet.city;
        r.createCompanyRet.fullName = userCompaniesRet.fullName;
        r.createCompanyRet.address = userCompaniesRet.address;
        r.createCompanyRet.narrate = userCompaniesRet.narrate;
        r.createCompanyRet.funding = userCompaniesRet.funding;
        r.createCompanyRet.webSite = userCompaniesRet.webSite;
        r.createCompanyRet.logoUrl = userCompaniesRet.logoUrl;
        r.createCompanyRet.logoUrlOrigin = userCompaniesRet.logoUrlOrigin;
        r.createCompanyRet.url = userCompaniesRet.url;
        r.createCompanyRet.updatedAt = userCompaniesRet.updatedAt;
        r.createCompanyRet.createdAt = userCompaniesRet.createdAt;
        r.creator = userCompaniesRet.creator;
        r.creatorName = userCompaniesRet.creatorName;
        r.creatorNick = userCompaniesRet.creatorNick;
        r.creatorName = userCompaniesRet.creatorName;
        r.userCount = userCompaniesRet.userCount;
        r.unread = userCompaniesRet.unread;

        r.topicCount = userCompaniesRet.topicCount;


        r.status = 1;
        return r;
    }

    /**
     * can join, but not yet
     */
    public static Company fromCompanyCanJoin(JsonCompanyCanJoinRet.CompanyCanJoinRet companyCanJoinRet) {
        Company r = new Company();

        r.createCompanyRet.id = companyCanJoinRet.id;
        r.createCompanyRet.name = companyCanJoinRet.name;
        r.createCompanyRet.desc = companyCanJoinRet.desc;
        r.createCompanyRet.domain = companyCanJoinRet.domain;
        r.createCompanyRet.city = companyCanJoinRet.city;
        r.createCompanyRet.fullName = companyCanJoinRet.fullName;
        r.createCompanyRet.address = companyCanJoinRet.address;
        r.createCompanyRet.narrate = companyCanJoinRet.narrate;
        r.createCompanyRet.funding = companyCanJoinRet.funding;
        r.createCompanyRet.webSite = companyCanJoinRet.webSite;
        r.createCompanyRet.logoUrl = companyCanJoinRet.logoUrl;
        r.createCompanyRet.url = companyCanJoinRet.url;
        r.createCompanyRet.updatedAt = companyCanJoinRet.updatedAt;
        r.createCompanyRet.createdAt = companyCanJoinRet.createdAt;
        r.userCount = companyCanJoinRet.userCount;
        r.creatorNick = companyCanJoinRet.creatorNick;
        r.createCompanyRet.logoUrlOrigin = companyCanJoinRet.logoUrlOrigin;
        r.topicCount = companyCanJoinRet.topicCount;
        r.groupCount = companyCanJoinRet.groupCount ;
        r.status = 0;
        return r;
    }

    /**
     * I create, so joined already by system
     */
    public static Company fromCreateCompanyRet(JsonCreateCompanyRet.CreateCompanyRet createCompanyRet) {
        Company r = new Company();

        r.createCompanyRet = createCompanyRet;
        r.creator = MyConfig.usr_id;

        r.status = 1;
        return r;
    }

}
