package co.docy.oldfriends.DataClass;

import co.docy.oldfriends.Configure.MyConfig;

/**
 * Created by fighting on 2016/6/21.
 * 由于报名的时间在本地显示的样式与上传到服务器上的样式不同，所以定义一个类，有两个字段，一个表示本地显示，一个表示上传到服务器
 */
public class ActivityTime {
    private String localTime;
    private String serverTime;

    public void setTime(int year, int month, int day,  int hour, int minute){
        this.localTime = MyConfig.getLocalDateStringWithWeek(year, month, day,hour, minute);
        this.serverTime = MyConfig.getServerDateString(year, month, day,hour, minute);
    }

    public String getLocalTime(){
        return localTime;
    }
    public String getServerTime(){
        return serverTime;
    }
}
