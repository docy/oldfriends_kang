package co.docy.oldfriends.DataClass;

/**
 * Created by youhy on 8/11/15.
 */
public class DayActivity {

    public String name;
    public String intro;
    public int duration;
    public int teacherId;
    public int type;
    public int credit;
    public String room;
    public String beginTime;
    public String createdAt;

    public DayActivity(String name){
        this.name = name;
    }

}
