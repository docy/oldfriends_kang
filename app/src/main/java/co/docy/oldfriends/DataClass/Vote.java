package co.docy.oldfriends.DataClass;

import java.util.HashMap;
import java.util.HashSet;

import co.docy.oldfriends.Messages.JsonGetTopicDetailRet;

/**
 * 本地显示用vote信息
 */
public class Vote {
    public int topicId;
    public String title;
    public String desc;
    public String clientId;

    //vote
    public boolean single;
    public boolean anonymous;
    public HashMap<String, String> options = new HashMap<>();
    public HashSet<Integer> checked = new HashSet<>();
    public HashMap<String, JsonGetTopicDetailRet.Choices> choices = new HashMap<>();
    public boolean voted;
    public boolean closed;
    public int resouceId;
    public int voterNum;
    public int imageId;

    /**
     * 处理后的vote信息
     *  重点是，将上面的map转换为list，这是因为显示的时候是有次序的，而map是无序的
     */

}
