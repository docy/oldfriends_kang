package co.docy.oldfriends.DataClass;

import java.io.File;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import co.docy.oldfriends.Adapters.ListAdapterMsg;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonGetEventRet;
import co.docy.oldfriends.Messages.JsonGetMsgRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicCommentRet;
import co.docy.oldfriends.Messages.JsonGetMsgTopicRet;
import co.docy.oldfriends.Messages.JsonSoundMsg;
import co.docy.oldfriends.Messages.Json_DirectRoom_GetTopicImageRet;
import co.docy.oldfriends.R;

/**
 *  msg level 0: root subject
 *  msg level 1: all reply to root subject, all new subject
 *  msg level 2: all reply to new subject
 *
 *  main room show all msg of level 0/1/2
 *  a sub room show a new subject and all its reply
 */
public class IMsg {
    public int msg_from = MyConfig.MSG_FROM_SERVER;
    /**
     * todo
     *  有三种场景
     *  1、本地构建数据并上传
     *  2、服务器数据
     *  3、服务器数据转换为本地可显示的数据
     *  看看能否把这三者区分开
     */

    public int user_id;
    public String user_name;
    public String avatar;

    public Date date;
    public String time_ampm;
    public String time_date;
    public String time_date_show;
    public String time_subroom;
    public boolean first_in_a_day;

    public boolean favorited;//是否被收藏或关注

    public int groupId;
    public int topicId;//发送topic的时候没有，由服务器添加，但是发送topic comment的时候有
    public String clientId;
    public IMsg imsParent;//以后可能不用了

    public int level;
    public int category;
    public String creatorName;//如果是comment，这里是subject的发起者的昵称
    public String creatorAvatar;//如果是comment，这里是subject的发起者的头像

    public String title;
    public String body;

    public LinkedList<IMsg> ll_sub_msg = new LinkedList<IMsg>();
    public LinkedList<IMsg> ll_sub_msg_with_image = new LinkedList<IMsg>();
    public ListAdapterMsg la_msg;
//    public ListAdapterMsg_DirectRoom la_msg_directroom;
    public LinkedList<String> ll_sub_image_url = new LinkedList<String>();

    /**
     * attached photo
     */
    public File attached;
    public String attachedMime;

    /**
     * attached vote
     */
    public Vote vote = new Vote();

    /**
     * attached vote
     */
    public MActivity mActivity = new MActivity();

    /**
     * attached map
     */
    public MapLocation mapLocation = new MapLocation();


    /**
     * 从origin data中抽取的数据
     */
    public String id;
    public int type;
    public int subType;

    //for image, vote, map
    public String thumbNail;
    public String fullSize;
    public int fullSize_h;
    public int fullSize_w;
    public int thumbNail_h;
    public int thumbNail_w;
    //for file
    public String fileName;
    public String filePath_inServer;
    public String filePath_inLocal;
    public int fileDownloadProgress;
    public int fileSize;
    public String hashCode;
    //for link
    public String link;
    //for sound
    public boolean readed;
    public int audioId;
    public String audioPath;
    public int audioPlaying;
    public int duration;

    //for ios share
    public String news;
    public String os;

    public String server_createAt;//聊天室下拉刷新时计算page需要这个createAt
    public String server_tag_id;

    public OriginDataFromServer originDataFromServer = new OriginDataFromServer();


    /**条件@某人的字段，可以同时@很多人，故使用集合*/
    public ArrayList<Integer> targets;


    public class OriginDataFromServer { //存放原始数据，常用数据提取到上面IMsg层

        // ========= in main room ============

        /**
         * 第一类，来自http接口
         *  msg
         *  subject
         *      文本
         *      图片
         *  comment
         */
        public JsonSoundMsg jsonSoundMsg;
        public JsonGetEventRet.GetEventRet getEventRet;
        public JsonGetMsgRet.GetMsgRet getMsgRet;
        public JsonGetMsgTopicRet.GetMsgTopicRet getMsgTopicRet;
        public JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet getMsgTopicCommentRet;


        /**
         * 第二类，来自socket
         *  msg
         *  subject
         *      文本
         *      图片
         *  comment
         */
//        public Json_Socket_Msg json_socket_msg;
//        public Json_Socket_Topic json_socket_topic;
//        public Json_Socket_TopicComment json_socket_topicComment;

        // ========= in sub room ============

        /**
         * 第一类，来自http接口
         *  subject
         *      文本
         *      图片
         *  comment
         */
//        public JsonGetMsgTopicRet.GetMsgTopicRet getMsgTopicRet_subroom;
//        public Json_SubRoom_GetTopicCommentRet.GetTopicCommentRet getTopicCommentRet_subroom;

        /**
         * 第二类，来自socket
         *  comment
         */

//        public Json_Socket_TopicComment json_socket_topicComment_subroom;

        // ========= in direct room ============
        public Json_DirectRoom_GetTopicImageRet.DirectRoom_GetTopicImageRet getImageRet;

    }


    public void setLocalSendTag(){
        msg_from = MyConfig.MSG_FROM_LOCAL;
    }

    public void attachFileBeforeSend2Server(File f, String mime){
        /**
         * if a image is attached, put it into father's gallery
         */
        attached = f;
        attachedMime = mime;
//        if(attachedMime.equals(MyConfig.MIME_IMAGE_STAR)){
//            imsParent.ll_sub_image_url.add(f.getAbsolutePath());
//            this.ll_sub_image_url.add(f.getAbsolutePath());
//        }
    }

    public IMsg(
            int user_id,
            String user_name,
            String avatar,

            Date date,

            int level,
            IMsg imsParent,
            int category,

            int groupId,
            int topicId,
            String clientId,

            String title,
            String body
    ) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.avatar = avatar;

        this.date = date;
        Format formatter_ampm;
        if(MyConfig.app != null) {//todo
//            formatter_ampm = new SimpleDateFormat(MyConfig.app.getString(R.string.time_am_hh_mm));
            formatter_ampm = new SimpleDateFormat(MyConfig.app.getString(R.string.time_HH_mm));
        }else{
            formatter_ampm = new SimpleDateFormat("a h:mm");
        }
        this.time_ampm = formatter_ampm.format(this.date);
        Format formatter_date;
        if(MyConfig.app != null) {//todo
            formatter_date = new SimpleDateFormat(MyConfig.app.getString(R.string.time_yyyy_mm_dd_week));
        }else{
            formatter_date = new SimpleDateFormat("yyyy年M月d日 E");
        }
        this.time_date = formatter_date.format(this.date);
//		this.time_ampm = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(date);
        Format formatter_subroom;
        if(MyConfig.app != null) {
            formatter_subroom = new SimpleDateFormat(MyConfig.app.getString(R.string.time_mm_dd_hh_mm));
        }else{
            formatter_subroom = new SimpleDateFormat("M月d日 h:mm");
        }
        this.time_subroom = formatter_subroom.format(this.date);

        this.level = level;
        this.category = category;
        this.imsParent = imsParent;

        this.groupId = groupId;
        this.topicId = topicId;
        this.clientId = clientId;

//        /**
//         * 如果是subject msg，则把自己也放入子列表
//         * 这是便于在这个subject的子聊天室里面也显示这个subject自己
//         */
//        if(type == MyConfig.MSG_CATEGORY_SUBJECT){
//            ll_sub_msg.add(this);
//        }

        this.title = title;
        this.body = body;
    }

}
