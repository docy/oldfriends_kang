package co.docy.oldfriends.DataClass;

/**
 * Created by youhy on 8/11/15.
 */
public class ScheduleNotification {

    public String author;
    public String title;
    public String time;
    public int nid;
    public String type;
    public String body;
    public String senderAvatar;
    public String receiver;

    public ScheduleNotification(){}

    public ScheduleNotification(
            String type,
            int nid,
            String author,
            String time,
            String title,
            String body

    ){
        this.title = title;
        this.author = author;
        this.time = time;
        this.type = type;
        this.body = body;
    }

}
