package co.docy.oldfriends.DataClass;

import java.util.LinkedList;

import co.docy.oldfriends.Messages.JsonGetTopicDetailRet;

/**
 * Created by fighting on 2016/6/3.
 */
public class MActivity {
    public int groupId;
    public String title;
    public String desc;
    public String clientId;

    public String closeTime;/**报名截止时间*/
    public String startTime;/**活动开始时间*/
    public String endTime;/**活动结束时间*/
    public String joinedNum;
    public String location;
    public float cost;
    public int limit;
    public int resouceId;

    public double longitude;/**经度*/
    public double latitude;/**纬度*/

    public int status;/**0 未加入, 1 加入 2 排队*/

    public boolean isPublic;
    public boolean closed;
    public LinkedList<JsonGetTopicDetailRet.JoinedUsers> joinedUsers;

}
