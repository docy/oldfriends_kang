package co.docy.oldfriends.DataClass;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonCreateGroupRet;
import co.docy.oldfriends.Messages.JsonGroupCanJoinRet;
import co.docy.oldfriends.Messages.JsonUserGroupsRet;

/**
 * Created by youhy on 6/17/15.
 */
public class Subject implements java.io.Serializable{

    /**
     * info from server
     */
    public JsonCreateGroupRet.CreateGroupRet createGroupRet = new JsonCreateGroupRet.CreateGroupRet();


    /**
     * info locally
     */
    public IMsg imsgRoot;
    public String update_time;

    /**
     * users
     */


    /**
     * room setting
     */
    public int status = -1;

    /**
     * joined already
     */
    public static Subject fromUserGroupsRet(JsonUserGroupsRet.UserGroupsRet userGroupsRet){
        Subject r = new Subject();

        r.createGroupRet.id = userGroupsRet.id;
        r.createGroupRet.name = userGroupsRet.name;
        r.createGroupRet.desc = userGroupsRet.desc;
        r.createGroupRet.category = userGroupsRet.category;
        r.createGroupRet.logo = userGroupsRet.logo;
        r.createGroupRet.joinType = userGroupsRet.joinType;
        r.createGroupRet.deleted = userGroupsRet.deleted;
        r.createGroupRet.updatedAt = userGroupsRet.updatedAt;
        r.createGroupRet.createdAt = userGroupsRet.createdAt;

        r.update_time = MyConfig.Date2GroupInfoTime(MyConfig.UTCString2Date(userGroupsRet.updatedAt));

        r.status = 1;
        return r;
    }

    /**
     * can join, but not yet
     */
    public static Subject fromGroupCanJoin(JsonGroupCanJoinRet.GroupCanJoinRet groupCanJoinRet){
        Subject r = new Subject();

        r.createGroupRet.id = groupCanJoinRet.id;
        r.createGroupRet.name = groupCanJoinRet.name;
        r.createGroupRet.desc = groupCanJoinRet.desc;
        r.createGroupRet.category = groupCanJoinRet.category;
        r.createGroupRet.logo = groupCanJoinRet.logo;
        r.createGroupRet.joinType = groupCanJoinRet.joinType;
        r.createGroupRet.deleted = groupCanJoinRet.deleted;
        r.createGroupRet.updatedAt = groupCanJoinRet.updatedAt;
        r.createGroupRet.createdAt = groupCanJoinRet.createdAt;

        r.update_time = MyConfig.Date2GroupInfoTime(MyConfig.UTCString2Date(groupCanJoinRet.updatedAt));

        r.status = 0;
        return r;
    }

    /**
     * I create, so joined already by system
     */
    public static Subject fromCreateGroupRet(JsonCreateGroupRet.CreateGroupRet createGroupRet){
        Subject r = new Subject();

        r.createGroupRet = createGroupRet;

        r.update_time = MyConfig.Date2GroupInfoTime(MyConfig.UTCString2Date(createGroupRet.updatedAt));

        r.status = 1;
        return r;
    }

}
