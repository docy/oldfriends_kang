package co.docy.oldfriends.DataClass;

import android.graphics.Bitmap;

/**
 * Created by youhy on 8/11/15.
 */
public class DevicePhoneContact {

    public String contactId;
    public String name;
    public String phone;
    public String photo_id;
    public Bitmap photo_bitmap;

}
