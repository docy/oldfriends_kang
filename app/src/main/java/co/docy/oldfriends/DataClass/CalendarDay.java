package co.docy.oldfriends.DataClass;

import co.docy.oldfriends.Configure.MyConfig;

/**
 * Created by youhy on 5/13/15.
 */
public class CalendarDay {

    public org.joda.time.LocalDate day;
    public boolean have_activity;

    public CalendarDay(org.joda.time.LocalDate day, boolean have_activity) {
        this.day = day;
        this.have_activity = have_activity;
    }

    public CalendarDay(org.joda.time.LocalDate day) {
        this.day = day;

        if (
                day != null
                        && MyConfig.jsonGetCoursesBySlotRet != null
                        && MyConfig.jsonGetCoursesBySlotRet.data != null
                        && MyConfig.jsonGetCoursesBySlotRet.data.size() > 0
                ) {

            Object object = MyConfig.jsonGetCoursesBySlotRet.data.get(day.toString());
            if (object != null) {

                /**
                 * 类型转换之前做null检测，否则报空指针错
                 */

                this.have_activity = (boolean) object;
                return;
            }

        }


        this.have_activity = false;

    }

}
