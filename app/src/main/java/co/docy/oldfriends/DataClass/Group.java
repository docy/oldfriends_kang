package co.docy.oldfriends.DataClass;

import java.util.Date;
import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonCompanyGroupListRet;
import co.docy.oldfriends.Messages.JsonCreateGroupRet;
import co.docy.oldfriends.Messages.JsonGroupCanJoinRet;
import co.docy.oldfriends.Messages.JsonLastMsgUniversal;
import co.docy.oldfriends.Messages.JsonUserGroupsRet;
import co.docy.oldfriends.R;

/**
 * Created by youhy on 6/17/15.
 */
public class Group implements java.io.Serializable {

    /**
     * info from server
     */
    public JsonCreateGroupRet.CreateGroupRet createGroupRet = new JsonCreateGroupRet.CreateGroupRet();
    public LinkedList<JsonLastMsgUniversal> messages;
    public int unreadCount = 0;
    public int userCount;
    public int creator;
    public int  topicCount;
    public int activity;
    public int live_status;//直播状态

    /**
     * info locally
     */
    public IMsg imsgRoot;

    /**
     * for UserGroupsRet, it's the last msg time
     * for other, it's the group create or update time
     */
    public String update_time;
    public String update_msg;

    /**
     * users
     */


    /**
     * room setting
     */
    public int status = -1;

    /**
     * joined already
     */
    public static Group fromUserGroupsRet(JsonUserGroupsRet.UserGroupsRet userGroupsRet) {
        Group r = new Group();

        r.messages = userGroupsRet.messages;

        r.createGroupRet.id = userGroupsRet.id;
        r.createGroupRet.name = userGroupsRet.name;
        r.createGroupRet.desc = userGroupsRet.desc;
        r.createGroupRet.category = userGroupsRet.category;
        r.createGroupRet.logo = userGroupsRet.logo;
        r.createGroupRet.joinType = userGroupsRet.joinType;
        r.createGroupRet.deleted = userGroupsRet.deleted;
        r.createGroupRet.updatedAt = userGroupsRet.updatedAt;
        r.createGroupRet.createdAt = userGroupsRet.createdAt;
        r.createGroupRet.showTop = userGroupsRet.showTop;
//        r.update_time = MyConfig.Date2GroupInfoTime(MyConfig.UTCString2Date(userGroupsRet.updatedAt));

        r.unreadCount = userGroupsRet.unreadCount;
        r.creator = userGroupsRet.creator;
        r.userCount = userGroupsRet.userCount;
        r.live_status = userGroupsRet.status;


        if(userGroupsRet.messages != null && userGroupsRet.messages.size() > 0) {
//        MyLog.d("","userGroupsRet.messages.getLast().updatedAt: "+userGroupsRet.messages.getLast().updatedAt);
            Date current = MyConfig.UTCString2Date(userGroupsRet.messages.getLast().updatedAt);

            /**这段代码抽取到了Config中，为了在子话题列表中复用*/
            r.update_time = MyConfig.dateFromatTodayOrYestodayOrDate(current);


            int type = userGroupsRet.messages.getLast().type;
            int subType = userGroupsRet.messages.getLast().subType;
            String name = userGroupsRet.messages.getLast().sender;
            StringBuilder sb = new StringBuilder();
            if (r.createGroupRet.category != MyConfig.SERVER_ROOM_CATEGORY_DIRECT &&type != MyConfig.TYPE_NEWS) {
                sb.append(name + ":");
            }
            if (type == MyConfig.TYPE_MSG) {
                sb.append(userGroupsRet.messages.getLast().info.message);
            }else if(type == MyConfig.TYPE_NEWS){
                sb.append(userGroupsRet.messages.getLast().info.message);
            }
            else if (type == MyConfig.TYPE_TOPIC) {
                sb.append(userGroupsRet.messages.getLast().info.title);
            } else if (type == MyConfig.TYPE_TOPIC_COMMANT) {

                if (subType == MyConfig.SUBTYPE_IMAGE_COMMENT) {
                    sb.append(MyConfig.app.getString(R.string.menu_plus_gallery));
                } else if (subType == MyConfig.SUBTYPE_TEXT_COMMENT) {
                    sb.append(userGroupsRet.messages.getLast().info.message);
                } else if (subType == MyConfig.SUBTYPE_SOUND_COMMENT) {
                    sb.append(MyConfig.app.getString(R.string.group_category_sound));
                } else {
                    sb.append(MyConfig.app.getString(R.string.group_category_unknown) + subType);
                }

            } else if (type == MyConfig.TYPE_TOPIC_IMAGE_DIRECTROOM) {
                sb.append(MyConfig.app.getString(R.string.menu_plus_gallery));
            } else if (type == MyConfig.TYPE_TOPIC_MAP_DIRECTROOM) {
                sb.append(MyConfig.app.getString(R.string.menu_plus_map));
            } else if (type == MyConfig.TYPE_CREATE) {
                sb.append(MyConfig.app.getString(R.string.maingroup_create));
            } else if (type == MyConfig.TYPE_JOIN) {
                sb.append(MyConfig.app.getString(R.string.maingroup_join));
            } else if (type == MyConfig.TYPE_LEAVE) {
                sb.append(MyConfig.app.getString(R.string.maingroup_leave));
            } else if (type == MyConfig.TYPE_SOUND) {
                sb.append(MyConfig.app.getString(R.string.group_category_sound));

            }
            r.update_msg = sb.toString();
        }else{
            r.update_msg = "";
        }
        r.status = 1;
        return r;
    }



    /**
     * can join, but not yet
     * 现在已经不显示GroupCanJoin
     */
    public static Group fromGroupCanJoin(JsonGroupCanJoinRet.GroupCanJoinRet groupCanJoinRet) {
        Group r = new Group();

        r.createGroupRet.id = groupCanJoinRet.id;
        r.createGroupRet.name = groupCanJoinRet.name;
        r.createGroupRet.desc = groupCanJoinRet.desc;
        r.createGroupRet.category = groupCanJoinRet.category;
        r.createGroupRet.logo = groupCanJoinRet.logo;
        r.createGroupRet.joinType = groupCanJoinRet.joinType;
        r.createGroupRet.deleted = groupCanJoinRet.deleted;
        r.createGroupRet.updatedAt = groupCanJoinRet.updatedAt;
        r.createGroupRet.createdAt = groupCanJoinRet.createdAt;
        r.userCount = groupCanJoinRet.userCount;
        r.topicCount = groupCanJoinRet.topicCount;
        r.activity = groupCanJoinRet.activity;
        r.update_time = MyConfig.Date2GroupInfoTime(MyConfig.UTCString2Date(groupCanJoinRet.updatedAt));

        r.status = 0;
        return r;
    }

    /**
     * I create, so joined already by system
     */
    public static Group fromCreateGroupRet(JsonCreateGroupRet.CreateGroupRet createGroupRet) {
        Group r = new Group();

        r.createGroupRet = createGroupRet;

        r.update_time = MyConfig.Date2GroupInfoTime(MyConfig.UTCString2Date(createGroupRet.updatedAt));

        r.status = 1;
        return r;
    }


    public static Group fromCompanyGroupListRet(JsonCompanyGroupListRet.CompanyGroupListRet companyGroupListRet) {
        Group group = new Group();
        group.createGroupRet.id = companyGroupListRet.id;
        group.userCount = companyGroupListRet.userCount;
        group.creator = companyGroupListRet.creator;
        group.createGroupRet.name = companyGroupListRet.name;
        group.createGroupRet.category = companyGroupListRet.category;
        group.createGroupRet.logo = companyGroupListRet.logo;
        group.createGroupRet.joinType = companyGroupListRet.joinType;
        group.createGroupRet.deleted = companyGroupListRet.deleted;
        group.createGroupRet.updatedAt = companyGroupListRet.updatedAt;
        group.createGroupRet.createdAt = companyGroupListRet.createdAt;
        group.createGroupRet.desc = companyGroupListRet.desc;
        group.status = 0;
        return group;
    }


}
