package co.docy.oldfriends.DataClass;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonFriendCricleRet;
import co.docy.oldfriends.Messages.JsonFriendCricleDetailRet;

/**
 * Created by khp on 2016/7/27.
 */
public class FriendCricleMsg implements Parcelable {
    public int id;
    public int userId;
    public int type;
    public String image;
    public String fullPath;
    public String title;
    public String createdAt;
    public String updatedAt;
    public boolean liked;
    public ArrayList<FriendCricleMsg> likeMsg = new ArrayList<>();
    public ArrayList<FriendCricleMsg> commentMsg = new ArrayList<>();

    //用户信息
    public String user_nickName;
    public String user_avatar;

    //视频的路径
    public String videoPath;
    public String videoPath_local;
    public int videoProgress;
    public String hashCode;
    public int videosize;
    public String os;

    //点赞的信息
    public String like_nickName;
    public int like_id;
    public String like_avatar;

    //评论的信息
    public String comment;
    public String comment_nickName;
    public int comment_id;
    public String comment_avatar;

    public FriendCricleMsg(){}
    protected FriendCricleMsg(Parcel in) {
        id = in.readInt();
        userId = in.readInt();
        type = in.readInt();
        image = in.readString();
        title = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        liked = in.readByte() != 0;
        user_nickName = in.readString();
        user_avatar = in.readString();
        videoPath = in.readString();
        like_nickName = in.readString();
        like_id = in.readInt();
        like_avatar = in.readString();
        comment = in.readString();
        comment_nickName = in.readString();
        comment_id = in.readInt();
        comment_avatar = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(userId);
        dest.writeInt(type);
        dest.writeString(image);
        dest.writeString(title);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeByte((byte) (liked ? 1 : 0));
        dest.writeString(user_nickName);
        dest.writeString(user_avatar);
        dest.writeString(videoPath);
        dest.writeString(like_nickName);
        dest.writeInt(like_id);
        dest.writeString(like_avatar);
        dest.writeString(comment);
        dest.writeString(comment_nickName);
        dest.writeInt(comment_id);
        dest.writeString(comment_avatar);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FriendCricleMsg> CREATOR = new Creator<FriendCricleMsg>() {
        @Override
        public FriendCricleMsg createFromParcel(Parcel in) {
            return new FriendCricleMsg(in);
        }

        @Override
        public FriendCricleMsg[] newArray(int size) {
            return new FriendCricleMsg[size];
        }
    };

    public static LinkedList<FriendCricleMsg> chooseData(LinkedList<JsonFriendCricleRet.CricleOfFriendRetDataRows> list){
        LinkedList<FriendCricleMsg> link = new LinkedList<>();
        for(JsonFriendCricleRet.CricleOfFriendRetDataRows jc:list){
            link.add(getchoosedata(jc));
        }
        return  link;
    }
    public static FriendCricleMsg getchoosedata(JsonFriendCricleRet.CricleOfFriendRetDataRows jc) {
        FriendCricleMsg fmsg = new FriendCricleMsg();
        fmsg.liked = jc.liked;
        fmsg.userId = jc.userId ;
        fmsg.image = jc.image;
        fmsg.createdAt = jc.createdAt;
        fmsg.id = jc.id;
        fmsg.title = jc.title;
        fmsg.updatedAt = jc.updatedAt ;
        fmsg.type =jc.type;

        if(jc.info!=null) {
            fmsg.fullPath = jc.info.fullPath;
            fmsg.videoPath = jc.info.videoPath;
            fmsg.videosize = jc.info.size;
            fmsg.os = jc.info.os;
            MyConfig.setLocalFileAnd(fmsg);
        }
        if(jc.User!=null) {
            fmsg.user_avatar = jc.User.avatar;
            fmsg.user_nickName = jc.User.nickName;
        }

        for(JsonFriendCricleRet.CricleOfFriendRetComment jcdata:jc.comments){
            fmsg.commentMsg.add(getcommentdata(jcdata));
        }
        for(JsonFriendCricleRet.CricleOfFriendRetLikes jclike:jc.likes){
            fmsg.likeMsg.add(getlikedata(jclike));
        }


        return fmsg;
    }
    public static FriendCricleMsg getcommentdata(JsonFriendCricleRet.CricleOfFriendRetComment jc){
        FriendCricleMsg fm = new FriendCricleMsg();
        fm.comment_avatar = jc.avatar;
        fm.comment_id = jc.id;
        fm.comment = jc.comment;
        fm.comment_nickName = jc.nickName;
        return fm;
    }
    public static FriendCricleMsg getlikedata(JsonFriendCricleRet.CricleOfFriendRetLikes jc){
        FriendCricleMsg fc = new FriendCricleMsg();
        fc.like_avatar = jc.avatar;
        fc.like_id = jc.id;
        fc.like_nickName = jc.nickName;
        return fc;
    }


    public static FriendCricleMsg choosedataDetail(JsonFriendCricleDetailRet.CricleOfFriendRetDataRows jc) {
        FriendCricleMsg fmsg = new FriendCricleMsg();
        fmsg.liked = jc.liked;
        fmsg.userId = jc.userId ;
        fmsg.image = jc.image;
        fmsg.createdAt = jc.createdAt;
        fmsg.id = jc.id;
        fmsg.title = jc.title;
        fmsg.updatedAt = jc.updatedAt ;
        fmsg.type =jc.type;

        if(jc.info!=null) {
            fmsg.fullPath = jc.info.fullPath;
            fmsg.videoPath = jc.info.videoPath;
            fmsg.videosize = jc.info.size;
            fmsg.os = jc.info.os;
            MyConfig.setLocalFileAnd(fmsg);
        }
        if(jc.User!=null) {
            fmsg.user_avatar = jc.User.avatar;
            fmsg.user_nickName = jc.User.nickName;
        }

        for(JsonFriendCricleDetailRet.CricleOfFriendRetComment jcdata:jc.comments){
            fmsg.commentMsg.add(commentdataDetail(jcdata));
        }
        for(JsonFriendCricleDetailRet.CricleOfFriendRetLikes jclike:jc.likes){
            fmsg.likeMsg.add(likedataDetail(jclike));
        }


        return fmsg;
    }
    public static FriendCricleMsg commentdataDetail(JsonFriendCricleDetailRet.CricleOfFriendRetComment jc){
        FriendCricleMsg fm = new FriendCricleMsg();
        fm.comment_avatar = jc.avatar;
        fm.comment_id = jc.id;
        fm.comment = jc.comment;
        fm.comment_nickName = jc.nickName;
        return fm;
    }
    public static FriendCricleMsg likedataDetail(JsonFriendCricleDetailRet.CricleOfFriendRetLikes jc){
        FriendCricleMsg fc = new FriendCricleMsg();
        fc.like_avatar = jc.avatar;
        fc.like_id = jc.id;
        fc.like_nickName = jc.nickName;
        return fc;
    }


}
