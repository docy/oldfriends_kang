package co.docy.oldfriends.DataClass;

/**
 * Created by youhy on 8/11/15.
 */
public class DayCourse {

    public String name;
    public String intro;
    public int duration;
    public int teacherId;
    public int type;
    public int credit;
    public String room;
    public String beginTime;
    public String createdAt;

    public DayCourse(String name){
        this.name = name;
    }

}
