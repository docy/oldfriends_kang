package co.docy.oldfriends.DataClass;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Messages.JsonGetClassDetailRet;
import co.docy.oldfriends.Messages.JsonGetCompanyUserListRet;
import co.docy.oldfriends.Messages.JsonGetGroupUserRet;
import co.docy.oldfriends.Messages.JsonGetUserInfoRet;
import co.docy.oldfriends.Messages.JsonSchoolContactEveryClass;
import co.docy.oldfriends.Messages.JsonSearchSchoolContactRet;

/**
 * Created by youhy on 8/11/15.
 */
public class ContactUniversal implements Parcelable {

    /**
     * 个人信息可能来自群组成员、公司成员，但不包含自己
     * 这里的字段应该覆盖所有情况
     * 以后所有adapter都使用这个数据结构
     */

    public int id;
    public String name;
    public String nickName;
    public String phone;
    public String avatar;
    public String avatarOrigin;
    public String email;
    public int status;
    public boolean sex;
    public String nameFristLetter;

    public int userType;
    public String currentCompany;
    public boolean enableNoti;
    public String company;
    public String title;
    public String companyIntro;
    public String duty;
    public String city;
    public String currentClassName;
    public String nameSecondLetter;
    public String nameThreeLetter;
    public boolean followed;

    //for multi-chooser
    public boolean selected;

    //for device phone number
    public String contactId;
    public String photo_id;
    public Bitmap photo_bitmap;
    public byte[] photo_bytes;

    public static void setNameFirstLetter(List<ContactUniversal> list) {

        PinyingCharacterParser pinyingCharacterParser = new PinyingCharacterParser();
        String pinyinThree;
        for (ContactUniversal cUserInfoDto:list) {
            if (cUserInfoDto != null && cUserInfoDto.nickName != null) {
                String pinyin = pinyingCharacterParser.getSelling(cUserInfoDto.nickName.substring(0,1));
                if(cUserInfoDto.nickName.trim().length()>=2)
                    cUserInfoDto.nameSecondLetter = pinyingCharacterParser.getSelling(cUserInfoDto.nickName.substring(1,2)).toUpperCase();
                if(cUserInfoDto.nickName.trim().length()>=3){
                    cUserInfoDto.nameThreeLetter   = pinyingCharacterParser.getSelling(cUserInfoDto.nickName.substring(2,3)).toUpperCase();
                }

                String sortString = pinyin.toUpperCase();
                cUserInfoDto.nameFristLetter = sortString;

            }
        }
    }

    public static int compare(ContactUniversal o1, ContactUniversal o2) {

        if((o1 != null && o1.nameFristLetter != null)
            && (o2 != null && o2.nameFristLetter != null)) {
            return o1.nameFristLetter.compareTo(o2.nameFristLetter);
        }else{
            return 0;
        }

    }
    public static int compareSecondName(ContactUniversal o1, ContactUniversal o2) {
        if((o1 != null && o1.nameSecondLetter != null)
                && (o2 != null && o2.nameSecondLetter != null)) {
            return o1.nameSecondLetter.compareTo(o2.nameSecondLetter);
        }else{
            return 0;
        }


    }
    public static int compareThreeName(ContactUniversal o1, ContactUniversal o2) {
        if((o1 != null && o1.nameThreeLetter != null)
                && (o2 != null && o2.nameThreeLetter != null)) {
            return o1.nameThreeLetter.compareTo(o2.nameThreeLetter);
        }else{
            return 0;
        }

    }


    /**
     * 手工转。小心服务器字段变化
     */

    public static ContactUniversal fromPhoneContactNamePhone(DevicePhoneContact devicePhoneContact){

        ContactUniversal contactUniversal = new ContactUniversal();

        contactUniversal.name = devicePhoneContact.name;
        contactUniversal.phone = devicePhoneContact.phone;
        contactUniversal.contactId = devicePhoneContact.contactId;
        contactUniversal.photo_id = devicePhoneContact.photo_id;
        contactUniversal.photo_bitmap = devicePhoneContact.photo_bitmap;

        return contactUniversal;

    }


    public static ContactUniversal fromJsonGetUserInfoRet(JsonGetUserInfoRet.GetUserInfoRet getUserInfoRet){

        ContactUniversal contactUniversal = new ContactUniversal();

        contactUniversal.id = getUserInfoRet.id;
        contactUniversal.name = getUserInfoRet.name;
        contactUniversal.nickName = getUserInfoRet.nickName;
        contactUniversal.phone = getUserInfoRet.phone;
        contactUniversal.avatar = getUserInfoRet.avatar;
        contactUniversal.avatarOrigin = getUserInfoRet.avatarOrigin;
        contactUniversal.email = getUserInfoRet.email;
        contactUniversal.status = getUserInfoRet.status;
        contactUniversal.sex = getUserInfoRet.sex;
//        contactUniversal.userType = getUserInfoRet.userType;
//        contactUniversal.currentCompany = getUserInfoRet.currentCompany;
//        contactUniversal.enableNoti = getUserInfoRet.enableNoti;
        contactUniversal.company = getUserInfoRet.company;
        contactUniversal.title = getUserInfoRet.title;
       contactUniversal.companyIntro = getUserInfoRet.companyIntro;
       contactUniversal.duty = getUserInfoRet.duty;
       contactUniversal.currentClassName = getUserInfoRet.currentClassName;
        contactUniversal.city = getUserInfoRet.city;
        contactUniversal.followed = getUserInfoRet.followed;

        contactUniversal.selected = false;

        return contactUniversal;

    }

    public static ContactUniversal fromJsonGetGroupUserRet(JsonGetGroupUserRet.GetGroupUserRet getGroupUserRet){

        ContactUniversal contactUniversal = new ContactUniversal();
//        contactUniversal.nameFristLetter = new PinyingCharacterParser().getFirstSpell(getGroupUserRet.name).trim().substring(0,1);
        contactUniversal.id = getGroupUserRet.id;
        contactUniversal.name = getGroupUserRet.name;
        contactUniversal.nickName = getGroupUserRet.nickName;
        contactUniversal.phone = getGroupUserRet.phone;
        contactUniversal.avatar = getGroupUserRet.avatar;
        contactUniversal.avatarOrigin = getGroupUserRet.avatarOrigin;
        contactUniversal.email = getGroupUserRet.email;
        contactUniversal.status = getGroupUserRet.status;
        contactUniversal.sex = getGroupUserRet.sex;
        contactUniversal.userType = getGroupUserRet.userType;
        contactUniversal.currentCompany = getGroupUserRet.currentCompany;
        contactUniversal.enableNoti = getGroupUserRet.enableNoti;
        contactUniversal.company = getGroupUserRet.company;
        contactUniversal.title = getGroupUserRet.title;
        contactUniversal.companyIntro = getGroupUserRet.companyIntro;
        contactUniversal.duty = getGroupUserRet.duty;
        contactUniversal.city = getGroupUserRet.city;

        contactUniversal.selected = false;

        return contactUniversal;

    }
    public static ContactUniversal fromJsonGetCompanyUserListRet(JsonGetCompanyUserListRet.GetCompanyUserListRet getCompanyUserListRet){
        ContactUniversal contactUniversal = new ContactUniversal();
//        contactUniversal.nameFristLetter = new PinyingCharacterParser().getFirstSpell(getCompanyUserListRet.name).trim().substring(0,1);
        contactUniversal.id = getCompanyUserListRet.id;
        contactUniversal.name = getCompanyUserListRet.name;
        contactUniversal.nickName = getCompanyUserListRet.nickName;
        contactUniversal.phone = getCompanyUserListRet.phone;
        contactUniversal.avatar = getCompanyUserListRet.avatar;
        contactUniversal.avatarOrigin = getCompanyUserListRet.avatarOrigin;
        contactUniversal.email = getCompanyUserListRet.email;
        contactUniversal.status = getCompanyUserListRet.status;
        contactUniversal.sex = getCompanyUserListRet.sex;
        contactUniversal.userType = getCompanyUserListRet.userType;
        contactUniversal.currentCompany = getCompanyUserListRet.currentCompany;
        contactUniversal.enableNoti = getCompanyUserListRet.enableNoti;
        contactUniversal.company = getCompanyUserListRet.company;
        contactUniversal.title = getCompanyUserListRet.title;
        contactUniversal.companyIntro = getCompanyUserListRet.companyIntro;
        contactUniversal.duty = getCompanyUserListRet.duty;
        contactUniversal.city = getCompanyUserListRet.city;

        return contactUniversal;

    }
    public static ContactUniversal fromJsonGetSchoolContact(JsonSearchSchoolContactRet.SearchSchoolContact getCompanyUserListRet){
        ContactUniversal contactUniversal = new ContactUniversal();
        contactUniversal.id = getCompanyUserListRet.id;
        contactUniversal.name = getCompanyUserListRet.name;
        contactUniversal.nickName = getCompanyUserListRet.nickName;
        contactUniversal.phone = getCompanyUserListRet.phone;
        contactUniversal.avatar = getCompanyUserListRet.avatar;
        contactUniversal.avatarOrigin = getCompanyUserListRet.avatarOrigin;
        contactUniversal.email = getCompanyUserListRet.email;
        contactUniversal.status = getCompanyUserListRet.status;
        contactUniversal.sex = getCompanyUserListRet.sex;
        contactUniversal.userType = getCompanyUserListRet.userType;
        contactUniversal.currentCompany = getCompanyUserListRet.currentCompany;
        contactUniversal.enableNoti = getCompanyUserListRet.enableNoti;
        contactUniversal.company = getCompanyUserListRet.company;
        contactUniversal.title = getCompanyUserListRet.title;
        contactUniversal.companyIntro = getCompanyUserListRet.companyIntro;
        contactUniversal.duty = getCompanyUserListRet.duty;
        contactUniversal.city = getCompanyUserListRet.city;

        return contactUniversal;

    }
    public static LinkedList<ContactUniversal> searchSchoolContact(LinkedList<JsonSearchSchoolContactRet.SearchSchoolContact> data){

        LinkedList<ContactUniversal> contactUniversalList = new LinkedList<>();

        for(JsonSearchSchoolContactRet.SearchSchoolContact getGroupUserRet:data){

            contactUniversalList.add(fromJsonGetSchoolContact(getGroupUserRet));

        }
        return contactUniversalList;
    }


    public static LinkedList<ContactUniversal> convertListFromGetGroupUserRet(LinkedList<JsonGetGroupUserRet.GetGroupUserRet> data){

        LinkedList<ContactUniversal> contactUniversalList = new LinkedList<>();

        for(JsonGetGroupUserRet.GetGroupUserRet getGroupUserRet:data){

            contactUniversalList.add(fromJsonGetGroupUserRet(getGroupUserRet));

        }
        return contactUniversalList;
    }
    //校友录中每个班级的人员
    public static ContactUniversal fromJsonGetSchoolContactEveryClass(JsonSchoolContactEveryClass.SchoolContactEveryClass getCompanyUserListRet){
        ContactUniversal contactUniversal = new ContactUniversal();
        contactUniversal.id = getCompanyUserListRet.id;
        contactUniversal.name = getCompanyUserListRet.name;
        contactUniversal.nickName = getCompanyUserListRet.nickName;
        contactUniversal.phone = getCompanyUserListRet.phone;
        contactUniversal.avatar = getCompanyUserListRet.avatar;
        contactUniversal.avatarOrigin = getCompanyUserListRet.avatarOrigin;
        contactUniversal.email = getCompanyUserListRet.email;
        contactUniversal.status = getCompanyUserListRet.status;
        contactUniversal.sex = getCompanyUserListRet.sex;
        contactUniversal.userType = getCompanyUserListRet.userType;
        contactUniversal.currentCompany = getCompanyUserListRet.currentCompany;
        contactUniversal.enableNoti = getCompanyUserListRet.enableNoti;
        contactUniversal.company = getCompanyUserListRet.company;
        contactUniversal.title = getCompanyUserListRet.title;
        contactUniversal.companyIntro = getCompanyUserListRet.companyIntro;
        contactUniversal.duty = getCompanyUserListRet.duty;
        contactUniversal.city = getCompanyUserListRet.city;

        return contactUniversal;

    }
    //校友录中某个班级的人员
    public static LinkedList<ContactUniversal> SchoolContactEveryClass(LinkedList<JsonSchoolContactEveryClass.SchoolContactEveryClass> data){

        LinkedList<ContactUniversal> contactUniversalList = new LinkedList<>();

        for(JsonSchoolContactEveryClass.SchoolContactEveryClass getGroupUserRet:data){

            contactUniversalList.add(fromJsonGetSchoolContactEveryClass(getGroupUserRet));

        }
        return contactUniversalList;
    }

    /**
     * 活动参与人员信息
     * @param data
     * @return
     */
    public static LinkedList<ContactUniversal> convertListFromGetActionCustomerRet(LinkedList<JsonGetClassDetailRet.ActionCustomerUsersInfo> data){

        LinkedList<ContactUniversal> contactUniversalList = new LinkedList<>();

        for(JsonGetClassDetailRet.ActionCustomerUsersInfo getActionCustomerRet:data){

            contactUniversalList.add(fromJsonGetActionCustomerRet(getActionCustomerRet));

        }
        return contactUniversalList;
    }

    private static ContactUniversal fromJsonGetActionCustomerRet(JsonGetClassDetailRet.ActionCustomerUsersInfo getActionCustomerRet) {

        ContactUniversal contactUniversal = new ContactUniversal();

        contactUniversal.id = getActionCustomerRet.id;
        contactUniversal.name = getActionCustomerRet.name;
        contactUniversal.nickName = getActionCustomerRet.nickName;
        contactUniversal.avatar = getActionCustomerRet.avatar;
        contactUniversal.avatarOrigin = getActionCustomerRet.avatarOrigin;




        contactUniversal.selected = false;

        return contactUniversal;
    }


    public static LinkedList<ContactUniversal> convertListFromGetCompanyUserListRet(LinkedList<JsonGetCompanyUserListRet.GetCompanyUserListRet> data){

        LinkedList<ContactUniversal> contactUniversalList = new LinkedList<>();

        for(JsonGetCompanyUserListRet.GetCompanyUserListRet getCompanyUserListRet:data){

            contactUniversalList.add(fromJsonGetCompanyUserListRet(getCompanyUserListRet));

        }
        return contactUniversalList;
    }

    public ContactUniversal(){

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        //小心写入次序
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(nickName);
        dest.writeString(phone);
        dest.writeString(avatar);
        dest.writeString(avatarOrigin);
        dest.writeString(email);
        dest.writeInt(status);
        dest.writeString("" + sex);
        dest.writeInt(userType);
        dest.writeString(currentCompany);
        dest.writeString("" + enableNoti);
        dest.writeString(company);
        dest.writeString(title);
        dest.writeString(companyIntro);
        dest.writeString(duty);
        dest.writeString(city);
    }

    private ContactUniversal(Parcel in) {
        id = in.readInt();
        name = in.readString();
        nickName = in.readString();
        phone = in.readString();
        avatar = in.readString();
        avatarOrigin = in.readString();
        email = in.readString();
        status = in.readInt();
        sex = Boolean.parseBoolean(in.readString());
        userType = in.readInt();
        currentCompany = in.readString();
        enableNoti = Boolean.parseBoolean(in.readString());
        company = in.readString();
        title = in.readString();
        companyIntro = in.readString();
        duty = in.readString();
        city = in.readString();
    }


    public static final Creator<ContactUniversal> CREATOR
            = new Creator<ContactUniversal>() {
        public ContactUniversal createFromParcel(Parcel in) {
            return new ContactUniversal(in);
        }

        public ContactUniversal[] newArray(int size) {
            return new ContactUniversal[size];
        }
    };
}
