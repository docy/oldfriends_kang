package co.docy.oldfriends.DataClass;

/**
 * Created by youhy on 8/11/15.
 */
public class Country {

    /**
     * 比较好的国家选择器
     *  https://github.com/roomorama/AndroidCountryPicker
     *  https://github.com/ruben-h/Android-ListDialogExample
     *  https://github.com/uglymittens/country-master
     */

    public int flag;
    public String name;
    public String code;
    public String phone_prefix;

    public Country(String code, String name, String phone_prefix){
        this.code = code;
        this.name = name;
        this.phone_prefix = phone_prefix;
    }


    public Country(){
    }

}
