package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by fighting on 2016/4/22.
 */
public class JsonGetPerDayCourse {

    public int code;
    public LinkedList<PerDayCourseInfo> data;

    public class PerDayCourseInfo{

        public String id;
        public String name;
        public String intro;
        public String logo;
        public String duration;
        public String teacherId;
        public String type;
        public String credit;
        public String companyId;
        public String createdAt;
        public String updatedAt;
        public String teacher;
        public String classRoom;
        public String group;
        public String beginTime;
        public String scheduleId;
    }
}
