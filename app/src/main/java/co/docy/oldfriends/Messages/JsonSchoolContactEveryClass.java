package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by khp on 2016/5/4.
 */
public class JsonSchoolContactEveryClass {

    public int code;

    public LinkedList<SchoolContactEveryClass> data = new LinkedList<>();

    public String message;

    public static class SchoolContactEveryClass{
        public int id;
        public String name;
        public String nickName;
        public String phone;
        public String avatar;
        public String avatarOrigin;
        public String email;
        public int status;
        public boolean sex;
        public int userType;

        //20160421
        public String currentCompany;
        public boolean enableNoti;
        public String company;
        public String title;
        public String companyIntro;
        public String duty;
        public String city;

    }
}
