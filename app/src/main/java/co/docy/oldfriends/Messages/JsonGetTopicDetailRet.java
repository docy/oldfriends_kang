package co.docy.oldfriends.Messages;


import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetTopicDetailRet {

    public int code;

    public String message;

    public GetTopicDetailRet data;

    public class GetTopicDetailRet {
        public String id;
        public int type;
        public int subType;
        public int groupId;
        public int userId;
        public Info info;
        public boolean deleted;
        public String createdAt;
        public String updatedAt;
        public String sender;
        public String avatar;
        public boolean favorited;
        public LinkedList<JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet> comments = new LinkedList();
    }

    public class Info{
        public int topicId;
        public String title;
        public String desc;
        public String clientId;

        //vote
        public boolean single;
        public boolean anonymous;
        public HashMap<String, String> options = new HashMap<>();
        public HashMap<String, JsonGetTopicDetailRet.Choices> choices = new HashMap<>();
        public boolean voted;
        public boolean closed;
        public int resouceId;
        public int voterNum;

        //activity
        public boolean isPublic;
        public String closeTime;
        public String startTime;
        public String endTime;
        public String joinedNum;
        public String location;
        public float fee;

        public int status;/**0 未加入, 1 加入 2 排队*/
        public int limit;
        public LinkedList<JoinedUsers> joinedUsers;


        //map
        public double latitude;
        public double longitude;

        //image
        public String thumbNail;
        public String fullSize;
        public int fullSize_h;
        public int fullSize_w;
        public int thumbNail_h;
        public int thumbNail_w;

        //file
        public String fileName;
        public String filePath;
        public int fileSize;
        public String hashCode;

        //link
        public String link;
        public String news;
        public String os;
    }

    /**已经报名的人员信息*/
    public class JoinedUsers {
        public int id;
        public String name;
        public String nickName;
        public String avatar;
        public String avatarOrigin;
    }

    public class Choices{
        public int votesCount;
        public LinkedList<VotedPersons> votes = new LinkedList<>();
    }
    public class VotedPersons{
        public int id;
        public String nickName;
        public String avatar;
    }

}
