package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGenerateCompanyInviteCodeRet {

    public int code;

    public GenerateCompanyInviteCodeRet data;

    public String message;

    public static class GenerateCompanyInviteCodeRet {
        public int companyId;
        public String code;
        public String target;
        public boolean valid;
    }

}
