package co.docy.oldfriends.Messages;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetCompanyUserListRet {

    public int code;

    public LinkedList<GetCompanyUserListRet> data = new LinkedList<>();

    public String message;

    public static GetCompanyUserListRet fromGroupUsers(JsonGetGroupUserRet.GetGroupUserRet getGroupUserRet){

        GetCompanyUserListRet getCompanyUserListRet = new GetCompanyUserListRet();

        getCompanyUserListRet.id = getGroupUserRet.id;
        getCompanyUserListRet.name = getGroupUserRet.name;
        getCompanyUserListRet.nickName = getGroupUserRet.nickName;
        getCompanyUserListRet.phone = getGroupUserRet.phone;
        getCompanyUserListRet.avatar = getGroupUserRet.avatar;
        getCompanyUserListRet.avatarOrigin = getGroupUserRet.avatarOrigin;
        getCompanyUserListRet.email = getGroupUserRet.email;
        getCompanyUserListRet.status = getGroupUserRet.status;
        getCompanyUserListRet.sex = getGroupUserRet.sex;
        getCompanyUserListRet.userType = getGroupUserRet.userType;

        getCompanyUserListRet.currentCompany = getGroupUserRet.currentCompany;
        getCompanyUserListRet.enableNoti = getGroupUserRet.enableNoti;
        getCompanyUserListRet.company = getGroupUserRet.company;
        getCompanyUserListRet.title = getGroupUserRet.title;
        getCompanyUserListRet.companyIntro = getGroupUserRet.companyIntro;
        getCompanyUserListRet.duty = getGroupUserRet.duty;
        getCompanyUserListRet.city = getGroupUserRet.city;

        return getCompanyUserListRet;
    }

    public static class GetCompanyUserListRet implements Parcelable {

        public GetCompanyUserListRet(){
        }

        public int id;
        public String name;
        public String nickName;
        public String phone;
        public String avatar;
        public String avatarOrigin;
        public String email;
        public int status;
        public boolean sex;
        public int userType;

        //20160421
        public String currentCompany;
        public boolean enableNoti;
        public String company;
        public String title;
        public String companyIntro;
        public String duty;
        public String city;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            //小心写入次序
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeString(nickName);
            dest.writeString(phone);
            dest.writeString(avatar);
            dest.writeString(avatarOrigin);
            dest.writeString(email);
            dest.writeInt(status);
            dest.writeString("" + sex);
            dest.writeInt(userType);


            dest.writeString(currentCompany);
            dest.writeString("" + enableNoti);
            dest.writeString(company);
            dest.writeString(title);
            dest.writeString(companyIntro);
            dest.writeString(duty);
            dest.writeString(city);
        }



        public static final Creator<GetCompanyUserListRet> CREATOR
                = new Creator<GetCompanyUserListRet>() {
            public GetCompanyUserListRet createFromParcel(Parcel in) {
                return new GetCompanyUserListRet(in);
            }

            public GetCompanyUserListRet[] newArray(int size) {
                return new GetCompanyUserListRet[size];
            }
        };

        private GetCompanyUserListRet(Parcel in) {
            id = in.readInt();
            name = in.readString();
            nickName = in.readString();
            phone = in.readString();
            avatar = in.readString();
            avatarOrigin = in.readString();
            email = in.readString();
            status = in.readInt();
            sex = Boolean.parseBoolean(in.readString());
            userType = in.readInt();


            currentCompany = in.readString();
            enableNoti = Boolean.parseBoolean(in.readString());
            company = in.readString();
            title = in.readString();
            companyIntro = in.readString();
            duty = in.readString();
            city = in.readString();
        }


    }



}
