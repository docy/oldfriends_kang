package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonUpdateGroupInfo {

    public String accessToken;
    public String name;
    public String desc;
    public Boolean broadcast;
    public Number joinType;

}
