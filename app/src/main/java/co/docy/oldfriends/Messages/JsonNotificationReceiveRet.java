package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by khp on 2016/4/23.
 */
public class JsonNotificationReceiveRet {

    public int code;

    public LinkedList<NotificationReceiveRet> data;

    public String message;

    public static class NotificationReceiveRet {
        public String companyId;
        public String content;
        public String createdAt;
        public int id;
        public String readed;
        public String sender;
        public String senderAvatar;
        public String receiver;
        public String senderId;
        public String title;
        public String updatedAt;


    }

}
