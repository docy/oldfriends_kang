package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by khp on 2016/6/6.
 */
public class JsonMadeInOrderFromServiceRet {
   public int code;
   public String message;
   public JsonMadeInOrder_Data data;
   public static class JsonMadeInOrder_Data{
      public int id;
      public int companyId;
      public String qrIcon;//二维码的图片
      public LinkedList<JsonMadeInOrder_strat> startPics;//闪屏页的图片
      public LinkedList<JsonMadeInOrder_appCenter> appCenter;
      public LinkedList<JsonMadeInOrder_userProfile> userProfile;
   }

   public static class JsonMadeInOrder_strat{
      public String url;
   }

   public static class JsonMadeInOrder_appCenter{
      public String url;
      public String icon;
      public String name;
      public boolean clickable;
   }

   public static class JsonMadeInOrder_userProfile{
      public String url;
      public String icon;
      public String name;
      public int type;
   }

   public String createdAt;
   public String updatedAt;
}
