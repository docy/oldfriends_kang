package co.docy.oldfriends.Messages;


import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonCompanyGroupListRet {

    public int code;

    public LinkedList<CompanyGroupListRet> data;

    public String message;

    public static class CompanyGroupListRet {
        public int id;
        public String name;
        public String desc;
        public int category;
        public String logo;
        public String joinType;
        public boolean deleted;
        public String updatedAt;
        public String createdAt;
        public int userCount;
        public int creator;
    }

}
