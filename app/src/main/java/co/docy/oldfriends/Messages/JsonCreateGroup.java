package co.docy.oldfriends.Messages;

import java.io.File;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonCreateGroup {

    public String accessToken;
    public String name;
    public String desc;
    public File logo;
    public int category;
    public boolean broadcast;
    public int joinType;

   // public LinkedList<Integer> userIds = new LinkedList<>();
    public Integer[] userIds = new Integer[100];

}
