package co.docy.oldfriends.Messages;

/**
 * Created by khp on 2016/8/1.
 */
public class JsonGetNewCommentRet {
    public int code;
    public NewComment data;
    public static class NewComment{
        public int count;
        public String avatar;
    }
}
