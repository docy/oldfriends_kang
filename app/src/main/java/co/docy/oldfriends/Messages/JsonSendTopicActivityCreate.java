package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonSendTopicActivityCreate {

    public String accessToken;
    public int groupId;
    public String clientId;
    public String title;
    public String desc;

    public String closeTime;
    public String startTime;
    public String endTime;
    public String location;
    public float fee;
    public int limit;

    public double longitude;
    public double latitude;


    public boolean isPublic;


}
