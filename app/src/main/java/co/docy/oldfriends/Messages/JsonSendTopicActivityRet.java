package co.docy.oldfriends.Messages;

/**
 * Created by fighting on 2016/6/13.
 */
public class JsonSendTopicActivityRet {
    public int code;

    public String message;

    public Data data;

    public class Data{
        public int id;
        public int userId;
        public int activityId;
        public int status;
        public int num;
        public String updatedAt;
        public String createdAt;

    }
}
