package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by khp on 2016/5/3.
 */
public class JsonSchoolContactRet {
    public int code;

    public LinkedList<JsonSchoolContactMessage> data;

    public String message;

    public static class JsonSchoolContactMessage {
        public int id;
        public String name;
        public String desc;
        public int category;
        public String logo;
        public String joinType;
        public boolean deleted;
        public String updatedAt;
        public String createdAt;
        public String year;
        public int userCount;


    }

}
