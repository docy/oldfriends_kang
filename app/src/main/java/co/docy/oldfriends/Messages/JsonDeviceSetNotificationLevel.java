package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonDeviceSetNotificationLevel {

    public String accessToken;
    public String os;
    public String pushToken;
    public String uuid;
    public int level;

}
