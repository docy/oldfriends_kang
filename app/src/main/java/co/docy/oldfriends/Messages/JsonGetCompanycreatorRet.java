package co.docy.oldfriends.Messages;

/**
 * Created by wuxue on 22/10/15.
 */
public class JsonGetCompanycreatorRet {

    public int code;

    public GetCompanycreatorRet data;

    public String message;

    public static class GetCompanycreatorRet {
        public int id;
        public String name;
        public String desc;
        public String domain;
        public String city;
        public String fullName;
        public String address;
        public String narrate;
        public String funding;
        public String webSite;
        public String logoUrl;
        public String logoUrlOrigin;
        public String url;
        public String updatedAt;
        public String createdAt;
        public int creator;
    }

}
