package co.docy.oldfriends.Messages;

import java.util.HashMap;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonSendTopicVoteCreate {

    public String accessToken;
    public int groupId;
    public String title;
    public boolean single;
    public boolean anonymous;
//    public String options;
//    public ArrayList<String> options = new ArrayList<String>();
    public HashMap<String, String> options = new HashMap<>();

    public String clientId;

    public int imageId;

}
