package co.docy.oldfriends.Messages;

/**
 * Created by fighting on 2016/5/4.
 */
public class JsonSubmitFeedback {
    public String accessToken;
    public String title;
    public String content;
}
