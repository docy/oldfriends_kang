package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonSendSoundCommentRet {

    public int code;

    public SendMsgRet data;

    public String message;


    public class SendMsgRet {
        public String id;
        public boolean deleted;
        public int type;
        public int groupId;
        public int userId;
        public Info info;
        public String updatedAt;
        public String createdAt;
        public String subType;
    }

    public class Info {
        public int topicId;
        public String clientId;
        public String title;
        public int topicType;
        public String creatorName;
        public String creatorAvatar;

    }

}
