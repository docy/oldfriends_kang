package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class __not_used__Json_Socket_Msg {

    public String id;
    public boolean deleted;
    public int type;
    public int subType;
    public int groupId;
    public int userId;
    public String sender;
    public String avatar;
    public String updatedAt;
    public String createdAt;
    public Info info = new Info();

    public class Info {
        public String message;
        public String clientId;
    }

}
