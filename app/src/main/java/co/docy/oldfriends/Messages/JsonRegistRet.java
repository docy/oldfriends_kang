package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonRegistRet {

    public int code;

    public RegistRet data;

    public String message;

    public class RegistRet {
        int id;
        String name;
        String nickName;
        String email;
        String avatar;
        String avatarOrigin;
        String password;
        String createAt;
        String updateAt;
        String phone;
        String sex;
        String birth;
        String status;
        public String authToken;
        String tokenExpire;
        public String lang;
    }

}
