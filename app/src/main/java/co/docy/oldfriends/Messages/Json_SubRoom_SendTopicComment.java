package co.docy.oldfriends.Messages;

import java.util.ArrayList;

/**
 * Created by youhy on 6/1/15.
 */
public class Json_SubRoom_SendTopicComment {

    public String accessToken;
    public String message;
    public int topicId;
    public String clientId;
    public ArrayList<Integer> targets;

}
