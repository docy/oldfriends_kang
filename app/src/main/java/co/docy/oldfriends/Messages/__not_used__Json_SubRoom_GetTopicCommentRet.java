package co.docy.oldfriends.Messages;


import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class __not_used__Json_SubRoom_GetTopicCommentRet {

    public int code;

    public LinkedList<GetTopicCommentRet> data;

    public String message;

    public class GetTopicCommentRet {
        public String id;
        public int type;
        public int subType;
        public int groupId;
        public int userId;
        public Info info;
        public boolean deleted;
        public String updatedAt;
        public String createdAt;
        public String sender;
        public String avatar;
    }

    public class Info{
        public int topicId;
        public String message;
        public String clientId;
    }

}
