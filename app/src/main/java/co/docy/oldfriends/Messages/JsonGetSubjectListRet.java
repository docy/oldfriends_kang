package co.docy.oldfriends.Messages;


import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetSubjectListRet {

    public int code;


    public LinkedList<GetSubjectListRet> data;

    public String message;

    public class GetSubjectListRet {
        public int id;
        public String title;
        public String desc;
        public int type;
        public int creatorId;
        public int resourceId;
        public int groupId;
        public String updatedAt;
        public String createdAt;
        public int views;
        public int favoriteCount;
        public int commentCount;
        public boolean favorited;

        public Info info;

        public LinkedList<LastComment> comments = new LinkedList<>();
    }

    public class Info{
        public int topicId;
        public String title;
        public String desc;
        public String clientId;
        public String thumbNail;
        public String fullSize;
        public int fullSize_h;
        public int fullSize_w;
        public int thumbNail_h;
        public int thumbNail_w;
    }

    public static class LastComment{
        public String id;
        public String nickName;
        public String createdAt;
        public int type;
        public String creatorName;
        public String creatorAvatar;
        public int imageId;

        public String message;

        public String thumbNail;
        public String fullSize;
        public int fullSize_h;
        public int fullSize_w;
        public int thumbNail_h;
        public int thumbNail_w;

    }
}
