package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by khp on 2016/8/3.
 */
public class JsonFriendCricleDetailRet {
    public int code;
    public CricleOfFriendRetDataRows data;

    public static class CricleOfFriendRetDataRows{
        public int id;
        public int userId;
        public int type;
        public String image;
        public String title;
        public infoData info;
        public String createdAt;
        public String updatedAt;
        public boolean liked;
        public Userself User;
        public LinkedList<CricleOfFriendRetLikes> likes;
        public LinkedList<CricleOfFriendRetComment> comments;

    }
    public static class infoData{
        public String fullPath;
        public String videoPath;
        public int size;
        public String os;
    }
    public static class Userself{
        public String nickName;
        public String avatar;
    }

    public static class CricleOfFriendRetLikes{
        public String nickName;
        public int id;
        public String avatar;
    }

    public static class CricleOfFriendRetComment{
        public String comment;
        public String nickName;
        public int id;
        public String avatar;
    }
}
