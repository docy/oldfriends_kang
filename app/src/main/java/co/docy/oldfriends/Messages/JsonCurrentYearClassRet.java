package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by khp on 2016/5/18.
 */
public class JsonCurrentYearClassRet {
    public int code;
    public String message;
    public LinkedList<CurrentYearClassRet> data;

    public static class CurrentYearClassRet{
        public int id;
        public String name;
        public int companyId;
        public String year;
        public String createdAt;
        public String updatedAt;
        public int userCount;
    }



}
