package co.docy.oldfriends.Messages;

/**
 * Created by khp on 2016/8/2.
 */
public class JsonFriendCricleSocketRet {
    public int id;
    public String sender;
    public String nickName;
    public int subType;
    public int userId;
    public int type;
    public String avatar;
    public static class commentInfo{
        public String comment;
        public int momentId;
    }

}
