package co.docy.oldfriends.Messages;

/**
 * Created by fighting on 2016/5/4.
 */
public class JsonGetFeedbackRet {

    public int code;

    FeedbackData data ;

    public class FeedbackData{
        public int id;
        public int userId;

        public String title;
        public String content;
        public String updatedAt;
        public String createdAt;
    }



}
