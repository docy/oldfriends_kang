package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonSendMapRet {

    public int code;

    public SendMsgRet data;

    public String message;

    public class SendMsgRet {
        public String id;
        public boolean deleted;
        public int type;
        public String subType;
        public int groupId;
        public int userId;
        public Info info;
        public String updatedAt;
        public String createdAt;
    }

    public class Info {
        public String title;
        public int topicId;
        public String clientId;
        public String desc;

        public String fullSize;
        public double latitude;
        public double longitude;
        public String thumbNail;
        public int fullSize_h;
        public int fullSize_w;
        public int thumbNail_h;
        public int thumbNail_w;

    }

}
