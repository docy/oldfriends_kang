package co.docy.oldfriends.Messages;

/**
 * Created by khp on 2016/4/29.
 */
public class JsonCreateAulmCardRet {
    public int code;

    public CreateAulmCardFromFu data;

    public String message;

    public static class CreateAulmCardFromFu{
        public String qrData;
    }
}
