package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by khp on 2016/8/8.
 */
public class JsonFriendCricleUserInfoRet {
    public int code;
    public LinkedList<CricleOfFriendRetDataRows> data;


    public static class CricleOfFriendRetDataRows{
        public int id;
        public int userId;
        public int type;
        public String image;
        public String title;
        public infoData info;
        public String createdAt;
        public String updatedAt;

    }
    public static class infoData{
        public String videoPath;
    }
}
