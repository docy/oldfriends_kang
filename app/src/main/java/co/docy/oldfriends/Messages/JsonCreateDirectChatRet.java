package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonCreateDirectChatRet {

    public int code;

    public CreateDirectChatRet data;

    public String message;

    public class CreateDirectChatRet {
        public int id;
        public String name;
        public String desc;
        public String category;
        public String logo;
        public String joinType;
        public boolean deleted;
        public String createAt;
        public String updateAt;
    }

}
