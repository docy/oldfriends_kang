package co.docy.oldfriends.Messages;


/**
 * Created by youhy on 6/1/15.
 */
public class __not_used__JsonGetMsgTopicImageDetailRet {

    public int code;

    public GetMsgTopicRet data;

    public String message;

    public class GetMsgTopicRet {
        public String id;
        public String title;
        public String desc;
        public int type;
        public int creatorId;
        public int resourceId;
        public int groupId;
        public String updatedAt;
        public String createdAt;
        public Info info;
    }

    public class Info{
        public int id;
        public String name;
        public String type;
        public String path;
        public int size;
        public String thumb_64;
        public String thumb_80;
        public String thumb_360;
        public String createdAt;
        public String updatedAt;
    }

}
