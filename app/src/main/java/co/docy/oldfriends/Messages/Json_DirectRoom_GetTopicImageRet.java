package co.docy.oldfriends.Messages;


import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class Json_DirectRoom_GetTopicImageRet {

    public int code;

    public LinkedList<DirectRoom_GetTopicImageRet> data;

    public String message;

    public static class DirectRoom_GetTopicImageRet {
        public String id;
        public int type;
        public int subType;
        public int groupId;
        public int userId;
        public Info info = new Info();
        public boolean deleted;
        public String updatedAt;
        public String createdAt;
        public String sender;
        public String avatar;
        public String nickName;
    }

    public static class Info{
        public String clientId;
        public int imageId;

        //map
        public double latitude;
        public double longitude;

        //image
        public String thumbNail;
        public String fullSize;
        public int fullSize_h;
        public int fullSize_w;
        public int thumbNail_h;
        public int thumbNail_w;
    }

}
