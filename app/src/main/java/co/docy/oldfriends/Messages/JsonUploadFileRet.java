package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonUploadFileRet {

    public int code;

    public UploadFileRet data;

    public String message;

    public static class UploadFileRet {
        public String fieldname;
        public String riginalname;
        public String name;
        public String encoding;
        public String mimetype;
        public String path;
        public String extension;
        public int size;
        public String truncated;
        public String buffer;
    }

}
