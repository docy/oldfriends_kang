package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by khp on 2016/5/24.
 */
public class JsonGetSignedByDateRet {
    public int code;
    public String message;
    public CheckSignedTime data;

    public static class CheckSignedTime{

       public  int count;
       public  LinkedList<CheckSignedTimeList> rows = new LinkedList<>();


    }
    public static class CheckSignedTimeList{
        public int id;
        public int userId;
        public String location;
        public int companyId;
        public  String createdAt;
        public  String updatedAt;

    }


}
