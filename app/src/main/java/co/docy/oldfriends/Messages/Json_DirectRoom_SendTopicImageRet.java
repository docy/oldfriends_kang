package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class Json_DirectRoom_SendTopicImageRet {

    public int code;

    public SendTopicMsgRet data;

    public String message;

    public class SendTopicMsgRet {
        public String id;
        public boolean deleted;
        public int type;
        public String subType;
        public int groupId;
        public int userId;
        public Info info;
        public String updatedAt;
        public String createdAt;
    }

    public class Info {
        public String clientId;

        public int imageId;
        public String fullSize;
        public String thumbNail;
        public String fullSize_h;
        public String fullSize_w;
        public String thumbNail_h;
        public String thumbNail_w;
    }

}
