package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by fighting on 2016/4/25.
 */
public class JsonGetClassDetailRet {
    public int code;

    public ActionCustomerData  data;

    public class ActionCustomerData{
        public int id;

        public String name;
        public String phone;

        public LinkedList<ActionCustomerUsersInfo> users;
        public LinkedList<ClassDetailFilesInfo> files;
    }

    public class ActionCustomerUsersInfo{
        public int id;

        public String name;
        public String nickName;
        public String avatar;
        public String avatarOrigin;

    }
    public class ClassDetailFilesInfo {
        public int id;
        public String name;
        public String  type;
        public String path;
        public String extension;

        public int size;
        public String hashCode;
        public String createdAt;
        public String updatedAt;
        public int creatorId;
        public String creator;

        /**服务器路径和本地路径*/
        public String filePath_inServer ;
        public String filePath_inLocal ;

    }

}
