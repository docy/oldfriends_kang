package co.docy.oldfriends.Messages;


import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetMsgTopicCommentRet {

    public int code;

    public LinkedList<GetMsgTopicCommentRet> data;

    public String message;

    public static class GetMsgTopicCommentRet {
        public String id;
        public int type;
        public int subType;
        public int groupId;
        public int userId;
        public Info info;
        public boolean deleted;
        public String updatedAt;
        public String createdAt;
        public String sender;
        public String nickName;
        public String avatar;
    }

    public class Info {
        public int topicId;
        public String message;
        public String title;
        public String clientId;
        public String creatorName;//如果是comment，这里是subject的发起者的昵称
        public String creatorAvatar;//如果是comment，这里是subject的发起者的头像

        //link
        public String link;
        public String content;

        //image
        public String thumbNail;
        public String fullSize;
        public int fullSize_h;
        public int fullSize_w;
        public int thumbNail_h;
        public int thumbNail_w;

        //sound
        public int audioId;
        public int duration;
        public String audioPath;
        public boolean readed;
        public String news;
    }

}
