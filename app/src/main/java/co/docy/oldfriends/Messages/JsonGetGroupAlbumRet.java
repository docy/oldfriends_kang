package co.docy.oldfriends.Messages;


import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetGroupAlbumRet {

    public int code;

    public LinkedList<GetGroupAlbumRet> data;

    public String message;

    public static class GetGroupAlbumRet {
        public String fullSize;
        public String thumbNail;
        public String createdAt;

        @Override
        public String toString() {
            return ""+fullSize+" "+thumbNail+" "+createdAt;
        }
    }


}
