package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by fighting on 2016/4/22.
 */
public class JsonGetPerDayCourseRet {

    public int code;
    public LinkedList<PerDayCourseInfo> data;

    public class PerDayCourseInfo{

        public int id;
        public String name;
        public String intro;
        public String logo;
        public int duration;
        public int teacherId;
        public int type;
        public int credit;
        public String createdAt;
        public String updatedAt;
        public String teacher;
        public String classRoom;
        public String group;
        public String beginTime;
        public int scheduleId;
    }
}
