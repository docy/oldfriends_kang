package co.docy.oldfriends.Messages;


import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonUserCompaniesRet {

    public int code;

    public LinkedList<UserCompaniesRet> data;

    public String message;

    public static class UserCompaniesRet {
        public int id;
        public String name;
        public String desc;
        public String domain;
        public String city;
        public String phone;
        public String fullName;
        public String address;
        public String narrate;
        public String funding;
        public String webSite;
        public String logoUrl;
        public String logoUrlOrigin;
        public String url;
        public String defaultGroup;
        public String updatedAt;
        public String createdAt;
        public String feedbackGroup;
        public int creator;
        public String broadcastGroup;
        public String creatorNick;
        public String creatorName;
        public int userCount;
        public int unread;
        public int topicCount;
    }

}
