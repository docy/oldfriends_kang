package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by khp on 2016/6/7.
 */
public class JsonChooseGroupToSendNotice {
    public int code;
    public String message;
    public LinkedList<ChooseGroupToSendNotice> data;

    public static class ChooseGroupToSendNotice{
        public int id;
        public String name;
        public String desc;
        public int category;
        public String logo;
        public String createdAt;
        public String updatedAt;
    }
}
