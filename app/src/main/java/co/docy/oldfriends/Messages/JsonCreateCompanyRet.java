package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonCreateCompanyRet {

    public int code;

    public CreateCompanyRet data;

    public String message;

    public static class CreateCompanyRet {
        public int id;
        public String name;
        public String desc;
        public String domain;
        public String city;
        public String fullName;
        public String address;
        public String narrate;
        public String funding;
        public String webSite;
        public String logoUrl;
        public String logoUrlOrigin;
        public String url;
        public String updatedAt;
        public String createdAt;
    }

}
