package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonWeixinLoginRet {

    public int code;

    public WeixinLoginRet data;

    public String message;

    public class WeixinLoginRet {
        public int id;
        public String name;
        public String nickName;
        public String password;
        public String avatar;
        public String avatarOrigin;
        public String origin;
        public Info info;
        public String updateAt;
        public String createAt;
        public String phone;
        public String email;
        public boolean sex;
        public String birth;
        public String status;
        public String authToken;
        public String tokenExpire;
        public String currentCompany;

        public String lang;
        public String countryCode;
        public int userType;
        public String company;
        public String title;
        public boolean enableNoti;
        public String duty;
        public String companyIntro;
        public String city;
        public int currentClass;
        public String currentClassName;

    }

    public class Info{
        public String openid;
        public boolean changed;
    }

}
