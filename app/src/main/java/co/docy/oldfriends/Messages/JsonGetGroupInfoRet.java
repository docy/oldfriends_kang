package co.docy.oldfriends.Messages;


/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetGroupInfoRet {

    public int code;

    public GetGroupInfoRet data;

    public String message;

    public class GetGroupInfoRet {
        public int id;
        public String name;
        public String desc;
        public int category;//用此字段判断私密
        public String logo;
        public String logoOrigin;
        public int joinType;
        public boolean broadcast;
        public boolean deleted;
        public String createdAt;
        public String updatedAt;
        public int creator;
        public boolean showTop;
        public int status;
    }



}
