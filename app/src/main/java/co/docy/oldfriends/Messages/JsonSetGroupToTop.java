package co.docy.oldfriends.Messages;

/**
 * Created by khp on 2016/5/19.
 */
public class JsonSetGroupToTop {
    public String accessToken;
    public int groupId;
    public boolean showTop;
}
