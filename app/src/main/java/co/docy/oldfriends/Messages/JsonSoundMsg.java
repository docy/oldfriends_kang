package co.docy.oldfriends.Messages;


/**
 * Created by youhy on 6/1/15.
 */
public class JsonSoundMsg {

    public String id;
    public int type;
    public int subType;
    public int groupId;
    public int userId;
    public Info info;
    public boolean deleted;
    public String updatedAt;
    public String createdAt;
    public String nickName;
    public String sender;
    public String avatar;

    public class Info{
        public int audioId;
        public String audioPath;
        public String clientId;
        public int duration;
        public boolean readed;
    }

}
