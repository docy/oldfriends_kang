package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonUploadImageRet {

    public int code;

    public UploadImageRet data;

    public String message;

    public static class UploadImageRet {
        public int id;
        public String name;
        public String type;
        public String path;
        public String thumbNail;
        public int thumbNail_w;
        public int thumbNail_h;
        public String fullSize;
        public int fullSize_w;
        public int fullSize_h;
        public int size;
        public String updatedAt;
        public String createdAt;
    }

}
