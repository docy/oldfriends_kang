package co.docy.oldfriends.Messages;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetUserInfoRet {

    public int code;

    public GetUserInfoRet data;

    public String message;

    public static class GetUserInfoRet implements Parcelable {
        public int id;
        public String name;
        public String nickName;
        public String phone;
        public String email;
        public String avatar;
        public String avatarOrigin;
        public boolean followed;


        public String company;
        public String title;
        public String city;

        /**添加公司简介和分管领域,以及班级*/
        public String companyIntro;
        public String duty;
        public String currentClassName;

        public int status;
        public boolean sex;
        public Info info;
        public class Info {
            public boolean changed;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            //小心写入次序
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeString(nickName);
            dest.writeString(phone);
            dest.writeString(avatar);
            dest.writeString(avatarOrigin);
            dest.writeString(email);
            dest.writeInt(status);
            dest.writeString("" + sex);


            dest.writeString(company);
            dest.writeString(title);
            dest.writeString(city);

            dest.writeString(companyIntro);
            dest.writeString(duty);
            dest.writeString(currentClassName);
        }



        public static final Creator<GetUserInfoRet> CREATOR
                = new Creator<GetUserInfoRet>() {
            public GetUserInfoRet createFromParcel(Parcel in) {
                return new GetUserInfoRet(in);
            }

            public GetUserInfoRet[] newArray(int size) {
                return new GetUserInfoRet[size];
            }
        };

        private GetUserInfoRet(Parcel in) {
            id = in.readInt();
            name = in.readString();
            nickName = in.readString();
            phone = in.readString();
            avatar = in.readString();
            avatarOrigin = in.readString();
            email = in.readString();
            status = in.readInt();
            sex = Boolean.parseBoolean(in.readString());

            company = in.readString();
            title = in.readString();
            city = in.readString();

            companyIntro = in.readString();
            duty = in.readString();
            currentClassName = in.readString();
        }

    }

}
