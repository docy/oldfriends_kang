package co.docy.oldfriends.Messages;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonLastMsgUniversal {

    /**
     * 注意，这是群组最后一条信息，格式是服务器端定的，不要自行修改
     */

    public String id;
    public boolean deleted;
    public int type;
    public int groupId;
    public int userId;
    public String sender;
    public String avatar;
    public Info info = new Info();
    public String updatedAt;
    public String createdAt;
    public int subType;

    public class Info {
        public int topicId;
        public String title;
        public String desc;
        public String message;
        public String clientId;
    }

    public static JsonLastMsgUniversal fromJsonSocketMsg(JsonGetMsgRet.GetMsgRet json_socket_msg){
        /**
         * 不是所有的东西都需要，看看grouplist页面展示时需要更新哪些信息
         */
        JsonLastMsgUniversal jsonLastMsgUniversal = new JsonLastMsgUniversal();
        jsonLastMsgUniversal.type = json_socket_msg.type;
        jsonLastMsgUniversal.subType = json_socket_msg.subType;
        jsonLastMsgUniversal.sender = json_socket_msg.sender;
        jsonLastMsgUniversal.updatedAt = json_socket_msg.updatedAt;
        jsonLastMsgUniversal.info.message = json_socket_msg.info.message;
        return jsonLastMsgUniversal;
    }


    public static JsonLastMsgUniversal fromJsonSocketTopic(JsonGetMsgTopicRet.GetMsgTopicRet json_socket_topic){
        JsonLastMsgUniversal jsonLastMsgUniversal = new JsonLastMsgUniversal();
        jsonLastMsgUniversal.type = json_socket_topic.type;
        jsonLastMsgUniversal.subType = json_socket_topic.subType;
        jsonLastMsgUniversal.sender = json_socket_topic.sender;
        jsonLastMsgUniversal.updatedAt = json_socket_topic.updatedAt;
        jsonLastMsgUniversal.info.title = json_socket_topic.info.title;
        return jsonLastMsgUniversal;
    }


    public static JsonLastMsgUniversal fromJsonSocketTopicComment(JsonGetMsgTopicCommentRet.GetMsgTopicCommentRet json_socket_topicComment){
        JsonLastMsgUniversal jsonLastMsgUniversal = new JsonLastMsgUniversal();
        jsonLastMsgUniversal.type = json_socket_topicComment.type;
        jsonLastMsgUniversal.subType = json_socket_topicComment.subType;
        jsonLastMsgUniversal.sender = json_socket_topicComment.sender;
        jsonLastMsgUniversal.updatedAt = json_socket_topicComment.updatedAt;
        if(json_socket_topicComment.subType == MyConfig.SUBTYPE_IMAGE){
            MyLog.d("","last update msg -- image comment");
            jsonLastMsgUniversal.info.message = "图片";
        }else {
            MyLog.d("","last update msg -- text comment");
            jsonLastMsgUniversal.info.message = json_socket_topicComment.info.message;
        }
        return jsonLastMsgUniversal;
    }


    public static JsonLastMsgUniversal fromJsonSocketDirectroomImage(Json_DirectRoom_GetTopicImageRet.DirectRoom_GetTopicImageRet json_socket_directroom_image){
        JsonLastMsgUniversal jsonLastMsgUniversal = new JsonLastMsgUniversal();
        jsonLastMsgUniversal.type = json_socket_directroom_image.type;
        jsonLastMsgUniversal.subType = json_socket_directroom_image.subType;
        jsonLastMsgUniversal.sender = json_socket_directroom_image.sender;
        jsonLastMsgUniversal.updatedAt = json_socket_directroom_image.updatedAt;
        if(json_socket_directroom_image.type == MyConfig.TYPE_TOPIC_IMAGE_DIRECTROOM){
            MyLog.d("","last update msg -- image directroom");
            jsonLastMsgUniversal.info.message = "图片";
        }else if(json_socket_directroom_image.type == MyConfig.TYPE_TOPIC_MAP_DIRECTROOM){
            MyLog.d("","last update msg -- map directroom");
            jsonLastMsgUniversal.info.message = "地图";
        }
        return jsonLastMsgUniversal;
    }


}
