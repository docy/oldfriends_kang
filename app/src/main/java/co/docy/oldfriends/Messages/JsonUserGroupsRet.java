package co.docy.oldfriends.Messages;


import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonUserGroupsRet {

    public int code;

    public LinkedList<UserGroupsRet> data;

    public String message;

    public static class UserGroupsRet {
        public int id;
        public String name;
        public String desc;
        public int category;
        public String logo;
        public String joinType;
        public boolean deleted;
        public String updatedAt;
        public String createdAt;
        public boolean showTop;
        public int status;

        public int unreadCount;

        public LinkedList<JsonLastMsgUniversal> messages;

        public int userCount;
        public int creator;

    }

}
