package co.docy.oldfriends.Messages;


import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonCompanyCanJoinRet {

    public int code;

    public LinkedList<CompanyCanJoinRet> data;

    public String message;

    public static class CompanyCanJoinRet {
        public int id;
        public String name;
        public String desc;
        public String domain;
        public String city;
        public String fullName;
        public String address;
        public String narrate;
        public String funding;
        public String webSite;
        public String logoUrl;
        public String logoUrlOrigin;
        public String url;
        public String updatedAt;
        public String createdAt;
        public int creator;
        public String creatorNick;
        public int userCount;
        public int topicCount;
        public int groupCount;
    }

}
