package co.docy.oldfriends.Messages;

import java.io.File;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonCreateCompany {

    public String accessToken;
    public String name;
    public String desc;
    public Boolean isPrivate;
    public String phone;
    public String domain;
    public String city;
    public String fullName;
    public int category;
    public String adress;
    public String narrate;
    public String funding;
    public String webSite;
    public File logoUrl;
    public String url;


}
