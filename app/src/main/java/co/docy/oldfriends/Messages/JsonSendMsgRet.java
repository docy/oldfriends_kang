package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonSendMsgRet {

    public int code;

    public SendMsgRet data;

    public String message;

    public class SendMsgRet {
        public String id;
        boolean deleted;
        int type;
        int groupId;
        int userId;
        public Info info;
        String updatedAt;
        String createdAt;
        String subType;
    }

    class Info {
        public String message;
        public String clientId;
    }

}
