package co.docy.oldfriends.Messages;

import java.io.File;

/**
 * Created by youhy on 6/1/15.
 */
public class Json_DirectRoom_SendTopicImage {

    public String accessToken;
    public File image;
    public int groupId;
    public String clientId;

    public double latitude = -1;
    public double longitude = -1;

}
