package co.docy.oldfriends.Messages;

import java.util.ArrayList;

/**
 * Created by fighting on 2016/8/17.
 */
public class JsonRegistLabelGetRet {
    public int code;
    public RegistLabelData data;

    public class RegistLabelData{
        public ArrayList<RegistLabel> all;
        public ArrayList<RegistLabel> mine;
    }

    public class RegistLabel{
        public int id;
        public String name;
    }
}
