package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by fighting on 2016/4/25.
 */
public class JsonGetActionCustomerRet {
    public int code;

    public ActionCustomerData  data;

    public class ActionCustomerData{
        public int id;

        public String name;
        public String phone;

        public LinkedList<ActionCustomerUsersInfo> users;
    }

    public class ActionCustomerUsersInfo{
        public int id;

        public String name;
        public String nickName;
        public String avatar;
        public String avatarOrigin;

    }

}
