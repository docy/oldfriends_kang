package co.docy.oldfriends.Messages;

import java.util.ArrayList;

/**
 * Created by fighting on 2016/8/2.
 */
public class JsonGetChatRoomListRet {
    public int code;

    public ArrayList<ChatRoomListData>  data;

    public class ChatRoomListData{
        public int id;
        public String name;
        public String desc;
        public int category;
        public String logo;
        public String logoOrigin;
        public int joinType;
        public boolean broadcast;
        public boolean deleted;
        public String createdAt;
        public String updatedAt;
        public int status;
        public int userCount;
    }
}
