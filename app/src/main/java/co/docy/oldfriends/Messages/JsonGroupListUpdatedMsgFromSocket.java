package co.docy.oldfriends.Messages;


/**
 * Created by youhy on 6/1/15.
 */
public class JsonGroupListUpdatedMsgFromSocket {

    /**
     * socket接收到的格式和http请求的不同，所以这里重新定义
     */

    public String id;
    public int type;
    public int subType;
    public int groupId;
    public int userId;
    public Info info;
    public String updatedAt;
    public String createdAt;
    public String nickName;
    public String sender;
    public String avatar;

    public class Info{
        public int id;
        public String title;
        public String content;
        public String updatedAt;
        public String createdAt;
        public boolean readed;
        public String sender;
        public String senderAvatar;
        public String receiver;
    }

}
