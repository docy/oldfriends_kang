package co.docy.oldfriends.Messages;

import java.util.Map;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetMyPrivilege {

    public int code;

    public Map<String, Boolean> data;

    public String message;

}
