package co.docy.oldfriends.Messages;


/**
 * Created by khp on 2016/6/8.
 */
public class JsonCreatNoticeMessage {

    public String accessToken;
    public String title;
    public String content;
    public int[] userIds;
    public Integer[] groupIds = new Integer[100];

}
