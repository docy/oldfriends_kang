package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetVersionsRet {

    public int code;

    public GetVersionsRet data;

    public String message;

    public static class GetVersionsRet {
        public String api;
        public String ios;
        public AppVersion android;
    }

    public class AppVersion {
        public int versionCode;
        public String versionName;
    }

}
