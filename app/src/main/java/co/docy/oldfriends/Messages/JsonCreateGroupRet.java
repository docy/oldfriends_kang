package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonCreateGroupRet {

    public int code;

    public CreateGroupRet data;

    public String message;

    public static class CreateGroupRet {
        public boolean deleted;
        public int id;
        public String name;
        public String desc;
        public String updatedAt;
        public String createdAt;
        public int category;
        public String logo;
        public String joinType;
        public boolean showTop;

    }

}
