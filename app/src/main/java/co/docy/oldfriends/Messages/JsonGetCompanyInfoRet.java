package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetCompanyInfoRet {

    public int code;

    public GetCompanyInfoRet data;

    public String message;

    public static class GetCompanyInfoRet {
        public int id;
        public String name;
        public String desc;
        public String domain;
        public String phone;
        public String city;
        public boolean isPrivate;
        public int category;
        public String fullName;
        public String address;
        public String narrate;
        public String funding;
        public String webSite;
        public String logoUrl;
        public String logoUrlOrigin;
        public String url;
        public String updatedAt;
        public String createdAt;
        public int creator;
        public String creatorName;
        public int userCount;
        public int groupCount;


    }

}
