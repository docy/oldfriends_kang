package co.docy.oldfriends.Messages;


import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetEventRet {

    public int code;

    public LinkedList<GetEventRet> data;

    public String message;

    public static class GetEventRet {
        public String id;
        public int type;
        public int subType;
        public int groupId;
        public int userId;
        public Info info;
        public boolean deleted;
        public String updatedAt;
        public String createdAt;
        public String sender;
        public String nickName;
        public String avatar;

        public class Info{
            public String message;
            public String clientId;
        }
    }


}
