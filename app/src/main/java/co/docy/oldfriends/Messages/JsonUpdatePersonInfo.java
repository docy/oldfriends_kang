package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonUpdatePersonInfo {

    public String accessToken;
    public String nickName;
    public String phone;
    public String email;
    public String password;
    public String oldpassword;
    public boolean sex;

    public String name;
    public String user_class;
    public String city;
    public String company;
    public String duty;

    public String code;
    public String title;
    public String companyIntro;
}
