package co.docy.oldfriends.Messages;

/**
 * Created by khp on 2016/8/1.
 */
public class JsonFriendCricleUnreadMsgRet {
    public int code;
    public FriendCricleUnreadMsgRet data;
    public static class FriendCricleUnreadMsgRet{
        public int count;
    }
}
