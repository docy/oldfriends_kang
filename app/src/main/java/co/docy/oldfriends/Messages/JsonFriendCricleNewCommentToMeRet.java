package co.docy.oldfriends.Messages;

import java.util.ArrayList;

/**
 * Created by khp on 2016/8/2.
 */
public class JsonFriendCricleNewCommentToMeRet {
    public int code;
    public ArrayList<FriendCricleNewCommentToMeRet> data;
    public static class FriendCricleNewCommentToMeRet{
        public int id;
        public int momentId;
        public int type;
        public String createdAt;
        public MomentContext Moment;
        public CommentInfo Info;
    }
    public static class MomentContext{
        public int id;
        public int userId;
        public String image;
        public String title;

    }
    public static class CommentInfo{
        public int id ;
        public int userId;
        public String comment;
        public CommentUser User;
    }
    public static class CommentUser{
        public String nickName;
        public String avatar;
    }
}
