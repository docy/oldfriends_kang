package co.docy.oldfriends.Messages;

import java.util.HashMap;

/**
 * Created by youhy on 6/1/15.
 */
public class __not_used__Json_Socket_Topic {
    /**
     * todo
     *  设法与http返回的topic结构统一
     */

    public String id;
    public int type;
    public int subType;
    public int groupId;
    public int userId;
    public Info info = new Info();
    public boolean deleted;
    public String sender;
    public String avatar;
    public String updatedAt;
    public String createdAt;
    public boolean favorited;

    public class Info {
        public int topicId;
        public String title;
        public String desc;
        public String clientId;

        //vote
        public boolean single;
        public boolean anonymous;
        public HashMap<String, String> options = new HashMap<>();

        //map
        public double latitude;
        public double longitude;

        //image
        public String thumbNail;
        public String fullSize;
        public int fullSize_h;
        public int fullSize_w;
        public int thumbNail_h;
        public int thumbNail_w;
    }

}
