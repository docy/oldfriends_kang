package co.docy.oldfriends.Messages;

/**
 * Created by khp on 2016/7/29.
 */
public class JsonFriendCricleWithHeadRet {
    public int code;
    public JsonFriendCricleWithHead data;

    public static class JsonFriendCricleWithHead{
        public String profilePicture;
    }
}
