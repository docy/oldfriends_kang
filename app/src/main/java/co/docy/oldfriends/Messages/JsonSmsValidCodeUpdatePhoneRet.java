package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonSmsValidCodeUpdatePhoneRet {

    public int code;

    public SmsSignupRet data;

    public String message;

    public class SmsSignupRet {
        public boolean expired;
        public int id;
        public String phone;
        public String code;
        public String messageId;
        public int captchaType;
        public String createAt;
        public String updateAt;
    }

//    @Override
//    public String toString() {
//        return s;
//    }

}
