package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonJoinCompanyWithInviteCodeRet {

    public int code;

    public JoinCompanyWithInviteCodeRet data;

    public String message;

    public class JoinCompanyWithInviteCodeRet {
        public int companyId;
    }

}
