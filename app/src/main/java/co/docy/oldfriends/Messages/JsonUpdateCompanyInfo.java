package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonUpdateCompanyInfo {

    public String accessToken;
    public String name;
    public String desc;
    public String domain;
    public String city;
    public String phone;
    public int categroy;
    public boolean isPrivate;
    public String fullName;
    public String address;
    public String narrate;
    public String funding;
    public String webSite;
    public String url;
    public String logo;

    public static JsonUpdateCompanyInfo fromJsonGetCompanyInfoRet(JsonGetCompanyInfoRet.GetCompanyInfoRet getCompanyInfoRet){
        JsonUpdateCompanyInfo jsonUpdateCompanyInfo = new JsonUpdateCompanyInfo();

        jsonUpdateCompanyInfo.name = getCompanyInfoRet.name;
        jsonUpdateCompanyInfo.desc = getCompanyInfoRet.desc;
        jsonUpdateCompanyInfo.domain = getCompanyInfoRet.domain;
        jsonUpdateCompanyInfo.city = getCompanyInfoRet.city;
        jsonUpdateCompanyInfo.phone = getCompanyInfoRet.phone;
        jsonUpdateCompanyInfo.fullName = getCompanyInfoRet.fullName;
        jsonUpdateCompanyInfo.address = getCompanyInfoRet.address;
        jsonUpdateCompanyInfo.narrate = getCompanyInfoRet.narrate;
        jsonUpdateCompanyInfo.funding = getCompanyInfoRet.funding;
        jsonUpdateCompanyInfo.webSite = getCompanyInfoRet.webSite;
        jsonUpdateCompanyInfo.url = getCompanyInfoRet.url;

        return jsonUpdateCompanyInfo;

    }

}
