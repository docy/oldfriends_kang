package co.docy.oldfriends.Messages;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGenerateInviteCodeRet {

    public int code;

    public GenerateInviteCodeRet data;

    public String message;

    public static class GenerateInviteCodeRet {
        public int companyId;
        public String code;
        public String target;
        public boolean valid;
    }

}
