package co.docy.oldfriends.Messages;

/**
 * Created by khp on 2016/4/23.
 */
public class JsonNotificationReceive {

    public boolean unread;
    public int page;
    public int limit;
    public String accessToken;

}
