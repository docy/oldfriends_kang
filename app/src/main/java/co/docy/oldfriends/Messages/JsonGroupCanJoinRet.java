package co.docy.oldfriends.Messages;


import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGroupCanJoinRet {

    public int code;

    public LinkedList<GroupCanJoinRet> data;

    public String message;

    public static class GroupCanJoinRet {
        public int id;
        public String name;
        public String desc;
        public int category;
        public String logo;
        public String joinType;
        public boolean deleted;
        public String updatedAt;
        public String createdAt;
        public int userCount;
        public int  topicCount;
        public int activity;

    }

}
