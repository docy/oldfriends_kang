package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by khp on 2016/7/27.
 */
public class JsonFriendCricleSendNewContentRet {
    public int code;
    public  SendCricleOfFriendRetDataRows data;
    public String message;


    public static class SendCricleOfFriendRetDataRows{
        public int id;
        public int userId;
        public int type;
        public String image;
        public String title;
        public SendinfoData info;
        public String createdAt;
        public String updatedAt;
        public boolean liked;
        public LinkedList<SendCricleOfFriendRetLikes> likes;
        public LinkedList<SendCricleOfFriendRetComment> comments;

    }
    public static class SendinfoData{
        public String fullPath;
        public String videoPath;
        public int size;
        public String os;
    }

    public static class SendCricleOfFriendRetLikes{
        public String nickName;
        public int id;
        public String avatar;
    }

    public static class SendCricleOfFriendRetComment{
        public String comment;
        public String nickName;
        public int id;
        public String avatar;
    }
}
