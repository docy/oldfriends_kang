package co.docy.oldfriends.Messages;

import co.docy.oldfriends.Configure.MyConfig;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonSetCurrentCompany {

    public String accessToken;
    public int id;

    public JsonSetCurrentCompany(int companyId){
        this.id = companyId;
        this.accessToken = MyConfig.usr_token;
    }

}
