package co.docy.oldfriends.Messages;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetGroupUserRet {

    public int code;

    public LinkedList<GetGroupUserRet> data = new LinkedList<>();

    public String message;

    public static class GetGroupUserRet implements Parcelable {
        public int id;
        public String name;
        public String nickName;
        public String phone;
        public String email;
        public int status;
        public boolean sex;
        public String avatar;
        public String avatarOrigin;
        public int userType;
        public int notification;

        //20160421
        public String currentCompany;
        public boolean enableNoti;
        public String company;
        public String title;
        public String companyIntro;
        public String duty;
        public String city;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            //小心写入次序
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeString(nickName);
            dest.writeString(phone);
            dest.writeString(email);
            dest.writeInt(status);
            dest.writeString("" + sex);
            dest.writeString(avatar);
            dest.writeString(avatarOrigin);
            dest.writeInt(userType);
            dest.writeInt(notification);

            dest.writeString(currentCompany);
            dest.writeString("" + enableNoti);
            dest.writeString(company);
            dest.writeString(title);
            dest.writeString(companyIntro);
            dest.writeString(duty);
            dest.writeString(city);
        }



        public static final Parcelable.Creator<GetGroupUserRet> CREATOR
                = new Parcelable.Creator<GetGroupUserRet>() {
            public GetGroupUserRet createFromParcel(Parcel in) {
                return new GetGroupUserRet(in);
            }

            public GetGroupUserRet[] newArray(int size) {
                return new GetGroupUserRet[size];
            }
        };

        private GetGroupUserRet(Parcel in) {
            id = in.readInt();
            name = in.readString();
            nickName = in.readString();
            phone = in.readString();
            email = in.readString();
            status = in.readInt();
            sex = Boolean.parseBoolean(in.readString());
            avatar = in.readString();
            avatarOrigin = in.readString();
            userType = in.readInt();
            notification = in.readInt();

            currentCompany = in.readString();
            enableNoti = Boolean.parseBoolean(in.readString());
            company = in.readString();
            title = in.readString();
            companyIntro = in.readString();
            duty = in.readString();
            city = in.readString();
        }


    }



}
