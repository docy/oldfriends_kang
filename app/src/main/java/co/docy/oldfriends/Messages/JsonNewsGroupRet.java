package co.docy.oldfriends.Messages;

import java.util.LinkedList;

/**
 * Created by khp on 2016/7/8.
 */
public class JsonNewsGroupRet {
    public int code;
    public LinkedList<NewsGroupRet> data;

    public static class NewsGroupRet{
        public String id;
        public int type;
        public int subType;
        public int groupId;
        public int userId;
        public NewsGroupInfo info;
        public boolean deleted;
        public String createdAt;
        public String updatedAt;
        public String sender;
        public String nickName;
        public String avatar;
    }

    public static class NewsGroupInfo{
        public LinkedList<NewsGroupInfo_news> news;
    }
    public static class NewsGroupInfo_news{
        public String link;
        public String[] image;
        public String title;
    }
}
