package co.docy.oldfriends.Messages;


import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by youhy on 6/1/15.
 */
public class JsonGetMsgTopicRet {

    public int code;

    public LinkedList<GetMsgTopicRet> data;

    public String message;

    public static class GetMsgTopicRet {
        public String id;
        public int type;
        public int subType;
        public int groupId;
        public int userId;
        public Info info = new Info();
        public boolean deleted;
        public String updatedAt;
        public String createdAt;
        public String sender;
        public String avatar;
        public boolean favorited;
    }

    public static class Info{
        public int topicId;
        public String title;
        public String desc;
        public String clientId;

        //vote
        public boolean single;
        public boolean anonymous;
        public HashMap<String, String> options = new HashMap<>();
        public HashMap<String, JsonGetTopicDetailRet.Choices> choices = new HashMap<>();
        public boolean voted;
        public boolean closed;
        public int resouceId;
        public int voterNum;


        //activity
        public boolean isPublic;
        public String closeTime;
        public String startTime;
        public String endTime;
        public String joinedNum;
        public String location;
        public float fee;

        public int status;/**0 未加入, 1 加入 2 排队*/
        public int limit;
        public LinkedList<JsonGetTopicDetailRet.JoinedUsers> joinedUsers;
        //map
        public double latitude;
        public double longitude;

        //image
        public String thumbNail;
        public String fullSize;
        public int fullSize_h;
        public int fullSize_w;
        public int thumbNail_h;
        public int thumbNail_w;

        //file
        public String fileName;
        public String filePath;
        public int fileSize;
        public String hashCode;

        //link
        public String link;

        /**
         * 在ios分享进来的文本主题及图片主题中有news字段，并且应该为字符串『news』
         */
        public String news;

        /**
         * ios录制的视频需要使用landscape模式播放
         */
        public String os;
    }

}
