/*
 Copyright (c) 2013 Roman Truba

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial
 portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package co.docy.oldfriends.TouchGallery.GalleryWidget;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.TouchGallery.TouchView.UrlTouchImageView;


/**
 Class wraps URLs to adapter, then it instantiates {@link ru.truba.touchgallery.TouchView.UrlTouchImageView} objects to paging up through them.
 */
public class UrlPagerAdapter extends BasePagerAdapter {

    public interface OnItemLongClickListener {
        void onItemLongClicked(View view, int position,  String link);
    }
    protected OnItemLongClickListener mOnItemLongClickListener;
    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
        mOnItemLongClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClicked(View view);
    }
    protected OnItemClickListener mOnItemClickListener;
    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    List<String> mResources;
    public UrlPagerAdapter(Context context, List<String> resources)
	{
		super(context, resources);
        mResources = resources;
	}

    @Override
    public void setPrimaryItem(ViewGroup container, final int position, Object object) {
        super.setPrimaryItem(container, position, object);

        if(object == null){
            return;
        }

        ((GalleryViewPager)container).mCurrentView = ((UrlTouchImageView)object).getImageView();

        ((GalleryViewPager)container).mCurrentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyLog.d("", "gallery save: setOnClickListener");
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClicked(v);
                }
            }
        });
        ((GalleryViewPager)container).mCurrentView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                MyLog.d("","gallery save: setOnLongClickListener");
                if(mOnItemLongClickListener != null){
                    mOnItemLongClickListener.onItemLongClicked(v, position, mResources.get(position));
                }
                return true;
            }
        });


    }

    @Override
    public Object instantiateItem(ViewGroup collection, final int position){
        final UrlTouchImageView iv = new UrlTouchImageView(mContext);
        iv.setUrl(mResources.get(position));
        iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        collection.addView(iv, 0);
        return iv;
    }
}
