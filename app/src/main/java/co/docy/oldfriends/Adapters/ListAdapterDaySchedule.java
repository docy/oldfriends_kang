package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonGetPerDayCourseRet;
import co.docy.oldfriends.R;


public class ListAdapterDaySchedule extends BaseAdapter {


    public interface OnLongClickedListener {
        void OnLongClicked(int position);
    }

    public OnLongClickedListener longClickListener;

    public interface OnClickedListener {
        void OnClicked(int position);
    }

    public OnClickedListener clickListener;

    private static class ViewHolder {
        RelativeLayout day_schedule_layout;
        TextView day_schedule_time;
        TextView day_schedule_place;
        TextView day_schedule_teacher;
        TextView day_schedule_name;
        View day_schedule_divider;
    }

    Context context;
    final List<JsonGetPerDayCourseRet.PerDayCourseInfo> list;
    private static LayoutInflater inflater = null;



    public ListAdapterDaySchedule(Context context,
                                  List<JsonGetPerDayCourseRet.PerDayCourseInfo> objects, OnClickedListener o) {
        this.context = context;
        this.list = objects;
        this.clickListener = o;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public JsonGetPerDayCourseRet.PerDayCourseInfo getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        JsonGetPerDayCourseRet.PerDayCourseInfo perDayCourseInfo = getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_day_schedule, null);
            viewHolder.day_schedule_layout = (RelativeLayout) convertView.findViewById(R.id.day_schedule_layout);

            viewHolder.day_schedule_time = (TextView) convertView.findViewById(R.id.day_schedule_time);
            viewHolder.day_schedule_place = (TextView) convertView.findViewById(R.id.day_schedule_place);
            viewHolder.day_schedule_name = (TextView) convertView.findViewById(R.id.day_schedule_name);
            viewHolder.day_schedule_teacher = (TextView) convertView.findViewById(R.id.day_schedule_teacher);
            viewHolder.day_schedule_divider =  convertView.findViewById(R.id.day_schedule_divider);


            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        /**最后一个条目下面不显示分割线*/
        if(position == getCount()-1){
            viewHolder.day_schedule_divider.setVisibility(View.GONE);
        }else{
            viewHolder.day_schedule_divider.setVisibility(View.VISIBLE);
        }

        viewHolder.day_schedule_place.setText(perDayCourseInfo.classRoom);
        viewHolder.day_schedule_name.setText(perDayCourseInfo.name);
        viewHolder.day_schedule_teacher.setText(perDayCourseInfo.teacher);

        /**设置时间*/
        Date beginTime = MyConfig.UTCString2Date(perDayCourseInfo.beginTime);
        String startTime = MyConfig.Date2GroupInfoTimeToday(beginTime);
        beginTime.setHours(beginTime.getHours()+perDayCourseInfo.duration);
        String endTime = MyConfig.Date2GroupInfoTimeToday(beginTime);
        viewHolder.day_schedule_time.setText(startTime+"-"+endTime);

        viewHolder.day_schedule_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(clickListener!=null) {
                    clickListener.OnClicked(position);
                }
            }
        });

        return convertView;

    }


}
