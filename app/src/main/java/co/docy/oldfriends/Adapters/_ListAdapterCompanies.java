package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.Company;
import co.docy.oldfriends.R;


public class _ListAdapterCompanies extends BaseAdapter {


    public interface OnLongClickedListener {
        public void OnLongClicked(int position);
    }

    public OnLongClickedListener longClickListener;

    public interface OnClickedListener {
        public void OnClicked(int position);
    }

    public OnClickedListener clickListener;

    private static class ViewHolder {
        ImageView row_icon;
        TextView row_id;
        TextView row_name;
        TextView row_description;
        TextView row_joined;
        ImageView row_current;
        RelativeLayout row_layout;
    }

    Context context;
    final List<Company> listItems;
    private static LayoutInflater inflater = null;


    @Override
    public Company getItem(int position) {
        return listItems.get(position);
    }

    public _ListAdapterCompanies(Context context,
                                 List<Company> objects, OnClickedListener o, OnLongClickedListener oo) {
        this.context = context;
        this.listItems = objects;
        this.clickListener = o;
        this.longClickListener = oo;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listItems.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Company company = getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_company, parent,false);
            viewHolder.row_icon = (ImageView) convertView.findViewById(R.id.company_icon);
            viewHolder.row_id = (TextView) convertView.findViewById(R.id.company_id);
            viewHolder.row_name = (TextView) convertView.findViewById(R.id.company_name);
            viewHolder.row_description = (TextView) convertView.findViewById(R.id.company_description);
            viewHolder.row_joined = (TextView) convertView.findViewById(R.id.company_joined);
            viewHolder.row_current = (ImageView) convertView.findViewById(R.id.company_current);
            viewHolder.row_layout = (RelativeLayout) convertView.findViewById(R.id.company_layout);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.row_icon.setImageResource(R.drawable.c_ic_launcher);
        viewHolder.row_id.setText("" + company.createCompanyRet.id);
        viewHolder.row_name.setText(company.createCompanyRet.name);


        if (company.status == 1) {

            viewHolder.row_current.setVisibility(View.VISIBLE);
            if (MyConfig.jsonGetCurrentCompanyRet != null && MyConfig.jsonGetCurrentCompanyRet.code==MyConfig.retSuccess()
                    && MyConfig.jsonGetCurrentCompanyRet.data.id == company.createCompanyRet.id) {

                MyLog.d("choosecompany", "company change flow: ListAdapterCompanies current company id " + MyConfig.jsonGetCurrentCompanyRet.data.id);

                viewHolder.row_current.setImageResource(R.drawable.switch_selected_3x);
            } else {
                viewHolder.row_current.setImageResource(R.drawable.switch_normal_3x);
            }
            viewHolder.row_current.setTag(R.id.adapter_company_list_position, position);
            viewHolder.row_current.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        clickListener.OnClicked(
                                ((Integer) (v.getTag(R.id.adapter_company_list_position))).intValue()
                        );
                    }
                }
            });

            viewHolder.row_joined.setVisibility(View.GONE);
        } else {
            viewHolder.row_current.setVisibility(View.GONE);
            viewHolder.row_joined.setText("not joined");
            viewHolder.row_joined.setVisibility(View.VISIBLE);
        }

        viewHolder.row_description.setText(company.createCompanyRet.desc);

//        setOnClick(viewHolder.row_icon, company.createCompanyRet.id);
//        setOnClick(viewHolder.row_id, company.createCompanyRet.id);
//        setOnClick(viewHolder.row_name, company.createCompanyRet.id);
//        setOnClick(viewHolder.row_description, company.createCompanyRet.id);

        viewHolder.row_layout.setTag(R.id.adapter_company_list_position, position);
        viewHolder.row_layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (longClickListener != null) {
                    longClickListener.OnLongClicked(
                            ((Integer) (v.getTag(R.id.adapter_company_list_position))).intValue()
                    );
                }
                return false;
            }
        });

        return convertView;

    }

    private void setOnClick(View v, int position) {
        v.setTag(R.id.adapter_company_list_position, position);
        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (longClickListener != null) {
                    longClickListener.OnLongClicked(
                            ((Integer) (v.getTag(R.id.adapter_company_list_position))).intValue()
                    );
                }
                return false;
            }
        });
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.OnClicked(
                            ((Integer) (v.getTag(R.id.adapter_company_list_position))).intValue()
                    );
                }
            }
        });
    }

}
