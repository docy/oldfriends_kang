package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonGetTopicDetailRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;


public class ListAdapterVotePersonList extends BaseExpandableListAdapter
{
    Context context;
    LayoutInflater inflater;

    LinkedList<String> options;
    LinkedList<JsonGetTopicDetailRet.Choices> choices;

    public  ListAdapterVotePersonList(Context c, LinkedList<String> options, LinkedList<JsonGetTopicDetailRet.Choices> choices)
    {
        this.context = c;
        this.options = options;
        this.choices = choices;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public  Object getChild(int  groupPosition, int  childPosition)
    {
        return  choices.get(groupPosition).votes.get(childPosition);
    }

    public  long  getChildId(int  groupPosition, int  childPosition)
    {
        return  childPosition;
    }

    public  int  getChildrenCount(int  groupPosition)
    {
        return  choices.get(groupPosition).votes.size();
    }

    public  View getChildView(int  groupPosition, int  childPosition,
                              boolean  isLastChild, View convertView, ViewGroup parent)
    {
        View v = inflater.inflate(R.layout.row_vote_view_voter, null);
        TextView person_name = (TextView)v.findViewById(R.id.voter_name);
        person_name.setText(choices.get(groupPosition).votes.get(childPosition).nickName);
        ImageView person_avatar = (ImageView)v.findViewById(R.id.voter_avatar);
        int usr_id = choices.get(groupPosition).votes.get(childPosition).id;
        person_avatar.setTag(usr_id);
        person_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.gotoUserInfo(context, (Integer)(v.getTag()));
            }
        });
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + choices.get(groupPosition).votes.get(childPosition).avatar)
//                .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                .fit().centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(person_avatar);
        return  v;
    }
    // group method stub
    public  Object getGroup(int  groupPosition)
    {
        return  options.get(groupPosition);
    }

    public  int  getGroupCount()
    {
        return  options.size();
    }

    public  long  getGroupId(int  groupPosition)
    {
        return  groupPosition;
    }

    public  View getGroupView(int  groupPosition, boolean  isExpanded,
                              View convertView, ViewGroup parent)
    {
        View v = inflater.inflate(R.layout.row_vote_view_voter_header, null);
        TextView row_vote_result_option = (TextView)v.findViewById(R.id.row_vote_result_option);
        row_vote_result_option.setText(options.get(groupPosition));
        return  v;
    }

    public  boolean  hasStableIds()
    {
        return  false ;
    }

    public  boolean  isChildSelectable(int  groupPosition, int  childPosition)
    {
        return  true ;
    }
}
