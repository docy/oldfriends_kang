package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.DataClass.Group;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import co.docy.oldfriends.emoji.EmojiUtils;


public class ListAdapterViewpagerTalk extends BaseAdapter {


    public interface OnLongClickedListener {
        public void OnLongClicked(int position);
    }

    public OnLongClickedListener longClickListener;

    public interface OnClickedListener {
        public void OnClicked(int id);
    }

    public interface OnImageClickedListener {
        public void OnImageClicked(int id);
    }

    public OnImageClickedListener imageClickLister;


    public OnClickedListener clickListener;

    public interface IJoinedClickListener {
        public void join(int position);
    }
    public IJoinedClickListener onJoinClickListener;

    private static class ViewHolder {
        //已加入小组
//        ImageView row_group_segment_button_space;
        ImageView row_icon;
        ImageView row_unread_spot, lockImage, row_live;
        TextView row_id;
        TextView row_unread_count;
        TextView row_name;
        TextView row_description;
        TextView row_joined;
        TextView row_update;
        RelativeLayout row_layout;
        View split_line;
        RelativeLayout room_layout;

        //未加入小组
        ImageView group_icon;
        TextView group_name, group_member_num, group_subject_num, group_description;
        Button group_join;
        RatingBar group_hot_starts;
        RelativeLayout search_group_layout;

        //未加入小组个数
        TextView group_count;
    }

    Context context;
    final List<Group> listItems;
    private static LayoutInflater inflater = null;


    @Override
    public Group getItem(int position) {
        return listItems.get(position);
    }

    public ListAdapterViewpagerTalk(Context context,
                                    LinkedList<Group> objects, OnClickedListener o, ListAdapterViewpagerTalk.OnLongClickedListener oo) {
        this.context = context;
        this.listItems = objects;
        this.clickListener = o;
        this.longClickListener = oo;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listItems.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Group group = getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_viewpager_talk, null);

            viewHolder.room_layout = (RelativeLayout) convertView.findViewById(R.id.room_layout);
            viewHolder.search_group_layout = (RelativeLayout) convertView.findViewById(R.id.search_group_layout);
            viewHolder.group_count = (TextView) convertView.findViewById(R.id.group_result);
            /**
             * listview 的item布局里是相对布局，加载时会报inflater错误，用以下方式加载则不会，具体原因未知，待探索
             *据说因为inflater.inflate在API几之前是2个参数，但是只能处理简单的布局，
             * 复杂的布局处理不了，后来增加到3个参数可以处理比较复杂的问题
             */

//            convertView = inflater.inflate(R.layout.row_viewpage_talk, parent,false);


            viewHolder.group_icon = (ImageView) convertView.findViewById(R.id.group_icon);
            viewHolder.group_name = (TextView) convertView.findViewById(R.id.group_name);
            viewHolder.group_member_num = (TextView) convertView.findViewById(R.id.group_member_num);
            viewHolder.group_subject_num = (TextView) convertView.findViewById(R.id.subject_num);
            viewHolder.group_description = (TextView) convertView.findViewById(R.id.group_description);
            viewHolder.group_hot_starts = (RatingBar) convertView.findViewById(R.id.group_hot_starts);
            viewHolder.group_join = (Button) convertView.findViewById(R.id.group_join);
            viewHolder.search_group_layout = (RelativeLayout) convertView.findViewById(R.id.search_group_layout);


//            viewHolder.row_group_segment_button_space = (ImageView) convertView.findViewById(R.id.row_group_segment_button_space);
            viewHolder.row_icon = (ImageView) convertView.findViewById(R.id.room_icon);
            viewHolder.row_live = (ImageView) convertView.findViewById(R.id.row_live);
            viewHolder.row_unread_spot = (ImageView) convertView.findViewById(R.id.room_unread_spot);
            viewHolder.row_unread_count = (TextView) convertView.findViewById(R.id.room_unread_count);
            viewHolder.row_id = (TextView) convertView.findViewById(R.id.row_id);
            viewHolder.row_name = (TextView) convertView.findViewById(R.id.row_name);
            viewHolder.row_description = (TextView) convertView.findViewById(R.id.row_description);
            viewHolder.row_joined = (TextView) convertView.findViewById(R.id.row_joined);
            viewHolder.row_update = (TextView) convertView.findViewById(R.id.row_update);
            viewHolder.row_layout = (RelativeLayout) convertView.findViewById(R.id.room_layout);
            viewHolder.split_line = convertView.findViewById(R.id.group_split_line);
            viewHolder.lockImage = (ImageView) convertView.findViewById(R.id.room_lock);


            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
//        if (position == 0) {
//            viewHolder.row_group_segment_button_space.setVisibility(View.VISIBLE);
//        } else {
//            viewHolder.row_group_segment_button_space.setVisibility(View.GONE);
//        }

        switch (group.status) {
            case -1:
                //结果数
                viewHolder.group_count.setVisibility(View.VISIBLE);
                viewHolder.search_group_layout.setVisibility(View.GONE);
                viewHolder.room_layout.setVisibility(View.GONE);
                viewHolder.group_count.setText(String.format(context.getString(R.string.unjoined_group_count), (listItems.size() - position - 1)));
                break;
            case 0:
                //未加入
                viewHolder.group_count.setVisibility(View.GONE);
                viewHolder.search_group_layout.setVisibility(View.VISIBLE);
                viewHolder.room_layout.setVisibility(View.GONE);

                viewHolder.group_icon.setImageResource(R.drawable.c_ic_launcher);
                Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + group.createGroupRet.logo)
//                        .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                        .fit()
                        .centerCrop()
                        .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                        .into(viewHolder.group_icon);

                TextPaint tp = viewHolder.group_name.getPaint();
                tp.setFakeBoldText(true);
                viewHolder.group_description.setText(group.createGroupRet.desc);
                viewHolder.group_name.setText(group.createGroupRet.name);
                viewHolder.group_join.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onJoinClickListener.join(position);
                    }
                });
                viewHolder.search_group_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        imageClickLister.OnImageClicked(position);
                    }
                });
                viewHolder.group_member_num.setText(String.format(context.getResources().getString(R.string.peope_num), group.userCount));
                String format = context.getResources().getString(R.string.subject_num);
                String Result = String.format(format, group.topicCount);
                viewHolder.group_subject_num.setText(Result);
                viewHolder.group_hot_starts.setProgress(group.activity);

                break;
            case 1:
                //已加入
                viewHolder.group_count.setVisibility(View.GONE);
                viewHolder.search_group_layout.setVisibility(View.GONE);
                viewHolder.room_layout.setVisibility(View.VISIBLE);

                viewHolder.row_layout.setBackgroundResource(R.color.White);
//        viewHolder.row_icon.setImageResource(R.drawable.logo2);
                Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + group.createGroupRet.logo)
//                        .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                        .fit()
                        .centerCrop()
                        .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                        .into(viewHolder.row_icon);


//        viewHolder.row_id.setText("" + group.createGroupRet.id);
                viewHolder.row_id.setText("" + group.unreadCount);
                if (group.unreadCount == 0) {
                    viewHolder.row_unread_spot.setVisibility(View.INVISIBLE);
                    viewHolder.row_unread_count.setVisibility(View.INVISIBLE);
                    viewHolder.row_id.setTextColor(MyConfig.app.getResources().getColor(R.color.Black));
                } else if (group.unreadCount < 100) {
                    viewHolder.row_unread_spot.setVisibility(View.INVISIBLE);
                    viewHolder.row_unread_count.setVisibility(View.VISIBLE);
                    viewHolder.row_unread_count.setText("" + group.unreadCount);
                    viewHolder.row_id.setTextColor(MyConfig.app.getResources().getColor(R.color.Red));
                } else {
                    viewHolder.row_unread_spot.setVisibility(View.VISIBLE);
                    viewHolder.row_unread_count.setVisibility(View.INVISIBLE);
                    viewHolder.row_id.setTextColor(MyConfig.app.getResources().getColor(R.color.Red));

                }
                if (MyConfig.SERVER_ROOM_CATEGORY_PRIVATE == group.createGroupRet.category) {
                    viewHolder.lockImage.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.lockImage.setVisibility(View.GONE);
                }
                MyConfig.unReadCount_temp = group.unreadCount;
                viewHolder.row_name.setText(group.createGroupRet.name);
                TextPaint textPaint = viewHolder.row_name.getPaint();
                textPaint.setFakeBoldText(true);

                viewHolder.row_description.setText(EmojiUtils.showEmoji(context,group.update_msg));
                viewHolder.row_update.setText(group.update_time);

                if(group.createGroupRet.category == MyConfig.SERVER_ROOM_CATEGORY_CHAT_ROOM){
                    viewHolder.row_description.setVisibility(View.GONE);
                    viewHolder.row_update.setVisibility(View.GONE);
                    viewHolder.row_unread_spot.setVisibility(View.INVISIBLE);
                    viewHolder.row_unread_count.setVisibility(View.INVISIBLE);
                }else{
                    viewHolder.row_description.setVisibility(View.VISIBLE);
                    viewHolder.row_update.setVisibility(View.VISIBLE);

                }

                if(group.createGroupRet.showTop || group.live_status == 1) {
                    viewHolder.room_layout.setBackgroundColor(context.getResources().getColor(R.color.xjt_input_stroke));
                }else{
                    viewHolder.room_layout.setBackgroundColor(context.getResources().getColor(R.color.White));
                }

                if(group.live_status == 1) {
                    viewHolder.row_live.setVisibility(View.VISIBLE);
                }else{
                    viewHolder.row_live.setVisibility(View.GONE);
                }

                setOnClick(viewHolder.row_layout, position);

                break;
        }

        return convertView;

    }

    private void setOnClick(View v, final int position) {
        v.setTag(R.id.adapter_group_list_position, position);
        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (longClickListener != null) {
                    longClickListener.OnLongClicked(position);
                }
                return true;
            }
        });
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.OnClicked(
                            ((Integer) (v.getTag(R.id.adapter_group_list_position))).intValue()
                    );
                }
            }
        });
    }

}
