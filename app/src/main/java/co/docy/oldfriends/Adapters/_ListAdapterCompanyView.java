package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.DataClass.Company;
import co.docy.oldfriends.Fragments._FragmentCompanyView;
import co.docy.oldfriends.R;


public class _ListAdapterCompanyView extends ArrayAdapter {


    public interface OnLongClickedListener {
        public void OnLongClicked(int position);
    }

    public OnLongClickedListener longClickListener;

    public interface OnClickedListener {
        public void OnClicked(int position);
    }

    public OnClickedListener enterClickListener;
    public OnClickedListener jionClickListener;
    public int type;

    private static class ViewHolder {
        LinearLayout mCompanyInfoLayout,list_layout;
        TextView mCompanyNameText, mCompanyDescText, mMessageCountText, mMemberCountText, mAddressTtext;
        ImageView mCompany_icon, mCompany_view_head, company_people_num_spand;
        Button row_join;

    }

    Context context;
    final List<Company> listItems;
    private static LayoutInflater inflater = null;


    @Override
    public Company getItem(int position) {
        return listItems.get(position);
    }

    public _ListAdapterCompanyView(Context context, List<Company> objects, int type) {
        super(context, R.layout.row_company_view);
        this.context = context;
        this.listItems = objects;
        this.type = type;
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listItems.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Company company = getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_company_view, parent, false);
            viewHolder.list_layout = (LinearLayout) convertView.findViewById(R.id.list_layout);
            viewHolder.mCompanyInfoLayout = (LinearLayout) convertView.findViewById(R.id.company_info_layout);
            viewHolder.mCompanyNameText = (TextView) convertView.findViewById(R.id.company_name);
            viewHolder.mCompanyDescText = (TextView) convertView.findViewById(R.id.company_desc);
            viewHolder.mAddressTtext = (TextView) convertView.findViewById(R.id.company_address);
            viewHolder.mCompany_icon = (ImageView) convertView.findViewById(R.id.company_icon);
            viewHolder.mMemberCountText = (TextView) convertView.findViewById(R.id.company_people_num);
            viewHolder.mMessageCountText = (TextView) convertView.findViewById(R.id.company_new_message);
            viewHolder.mCompany_view_head = (ImageView) convertView.findViewById(R.id.company_view_head);
            viewHolder.company_people_num_spand = (ImageView) convertView.findViewById(R.id.company_people_num_spand);
            viewHolder.row_join = (Button) convertView.findViewById(R.id.row_join);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String memFormat = context.getString(R.string.peope_num);
        String memberCount = String.format(memFormat,listItems.get(position).userCount);

        String addressFormat = context.getString(R.string.location);
        String address = String.format(addressFormat,listItems.get(position).createCompanyRet.city == null ?context.getResources().getString(R.string.beijing):listItems.get(position).createCompanyRet.city);

        String messageFormat =context.getString(R.string.new_message);
        String messageCount = String.format(messageFormat, (listItems.get(position).unread > 99 ? "99+" : listItems.get(position).unread));

        String subjectFormat = context.getString(R.string.subject_num);
        String subjectCount = String.format(subjectFormat,(listItems.get(position).unread > 99 ? "99+" : listItems.get(position).topicCount));


        if (type == _FragmentCompanyView.TYPE_COMPANY_SORT_LIST) {
            viewHolder.mAddressTtext.setMaxWidth(9);
            MyConfig.addForeColorSpan(memberCount, context.getResources().getColor(R.color.xjt_toolbar_blue), memberCount.indexOf(" "), memberCount.length(), viewHolder.mMessageCountText);
            MyConfig.addForeColorSpan(address, context.getResources().getColor(R.color.SkyBlue), address.indexOf(" "), address.length(), viewHolder.mMemberCountText);
            MyConfig.addForeColorSpan(subjectCount, context.getResources().getColor(R.color.xjt_vote_result_2), subjectCount.indexOf(" "), subjectCount.length(), viewHolder.mAddressTtext);
//            viewHolder.mAddressTtext.setVisibility(View.INVISIBLE);
//            viewHolder.company_people_num_spand.setVisibility(View.INVISIBLE);
            viewHolder.row_join.setVisibility(View.VISIBLE);
            viewHolder.row_join.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    jionClickListener.OnClicked(position);
                }
            });
            viewHolder.list_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    enterClickListener.OnClicked(position);
                }
            });
        } else {
            viewHolder.mAddressTtext.setMaxWidth(6);
            MyConfig.addForeColorSpan(memberCount, context.getResources().getColor(R.color.xjt_toolbar_blue), memberCount.indexOf(" "), memberCount.length(), viewHolder.mMemberCountText);
            MyConfig.addForeColorSpan(address, context.getResources().getColor(R.color.SkyBlue), address.indexOf(" "), address.length(), viewHolder.mAddressTtext);
            MyConfig.addForeColorSpan(messageCount, listItems.get(position).unread < 1 ? context.getResources().getColor(R.color.xjt_setting_text) : Color.RED, messageCount.indexOf(" "), messageCount.length(), viewHolder.mMessageCountText);
            viewHolder.mAddressTtext.setVisibility(View.VISIBLE);
            viewHolder.row_join.setVisibility(View.GONE);
//            viewHolder.mCompanyInfoLayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    enterClickListener.OnClicked(position);
//                }
//            });
            viewHolder.list_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    enterClickListener.OnClicked(position);
                }
            });
        }

        if (position == 0) {
            viewHolder.mCompany_view_head.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mCompany_view_head.setVisibility(View.GONE);
        }


        viewHolder.mCompanyNameText.setText(company.createCompanyRet.name);

        viewHolder.mCompanyDescText.setText(company.createCompanyRet.desc);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + listItems.get(position).createCompanyRet.logoUrlOrigin)
                .resize(MyConfig.getWidthByScreenPercent(100), MyConfig.getPxFromDimen(context, R.dimen.companyviews_height_cardview))
                .centerCrop()
                .into(viewHolder.mCompany_icon);


        viewHolder.mCompany_icon.setTag(R.id.adapter_company_list_position, position);
        return convertView;

    }

}
