package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonGetGroupUserRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;


public class ListAdapterGroupUser extends Adapter<ListAdapterGroupUser.ViewHolder> {

    List<JsonGetGroupUserRet.GetGroupUserRet> listItems;
    Context context;
    boolean showInvite, showDelete, isJioned;
    int ResultCount;

    public ListAdapterGroupUser(Context context, List<JsonGetGroupUserRet.GetGroupUserRet> li, boolean showInvite,boolean showDelete,boolean isJioned) {
        this.listItems = li;
        this.context = context;
        this.showInvite = showInvite;
        this.showDelete = showDelete;
        this.isJioned = isJioned;

    }

    public interface OnInviteClickedListener {
        public void onInviteClicked();
    }

    public OnInviteClickedListener inviteClickedListener;

    public interface OnDeleteClickedListener {
        public void onDeleteClicked();
    }
    public OnDeleteClickedListener deleteClickedListener;

    public OnMoreClickedListener moreClickedListener;
    public interface OnMoreClickedListener {
        public void onMoreClicked();
    }




    public static class ViewHolder extends RecyclerView.ViewHolder {

        public FrameLayout frameLayout;
        public ImageView user_icon;
        public ImageView user_mask;
        public TextView user_name;
        public ImageView host_image;

        public ViewHolder(View itemView) {
            super(itemView);
            this.user_name = (TextView) itemView.findViewById(R.id.user_name);
            this.user_icon = (ImageView) itemView.findViewById(R.id.user_icon);
            this.user_mask = (ImageView) itemView.findViewById(R.id.user_mask);
            this.frameLayout = (FrameLayout) itemView.findViewById(R.id.file_frame);
            this.host_image = (ImageView) itemView.findViewById(R.id.adapter_groupuser_host);
        }
    }


    @Override
    public int getItemCount() {
//        MyLog.d("", "OKHTTP: listItems.size() " + listItems.size());
//        MyLog.d("", "OKHTTP: listItems.size() count" + listItems.size());

        if (showDelete) {

            if (MyConfig.FALSE_TAG && showInvite) {//屏蔽拉人需求，这里不需要执行，有需求时在打开。
                if (listItems.size() + 2 > 15) {
                    ResultCount = 12;
                    return 15;
                } else {
                    ResultCount = 12;
                    return listItems.size() + 2;
                }

            } else {
                if (listItems.size() + 1 > 15) {
                    ResultCount = 13;
                    return 15;
                } else {
                    ResultCount = 13;
                    return listItems.size() + 1;
                }
            }

        } else {
            ResultCount = 14;
            return listItems.size();
        }

    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MyLog.d("", "OKHTTP: listItems.size() " + listItems.size() + " position " + position);

        if (MyConfig.FALSE_TAG && showInvite) {//老朋友没有通讯录，暂时屏蔽小组拉人
            if (position == 0) {
                holder.user_icon.setImageResource(R.drawable.invitation_3x);
                holder.frameLayout.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (inviteClickedListener != null) {
                            inviteClickedListener.onInviteClicked();
                        }
                    }

                });
                holder.user_mask.setAlpha((float) 0);
                holder.user_name.setText("");

                /**
                 * 小心，由于recyclerview的重用机制，上次使用的long click痕迹没有自动消除
                 * 如果这里不填入一个无操作的listener，则以前的listener还在，还会进入以前的long click处理流程
                 */
                holder.frameLayout.setOnLongClickListener(new View.OnLongClickListener() {

                    @Override
                    public boolean onLongClick(View v) {
                        return false;
                    }
                });

                return;
            }
            position--;
            //   count ++;
        }

        if (showDelete) {
            if (position == 0) {
                holder.user_icon.setImageResource(R.drawable.group_kick_people_3x);
                holder.frameLayout.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (deleteClickedListener != null) {
                            deleteClickedListener.onDeleteClicked();
                        }
                    }

                });
                holder.user_mask.setAlpha((float) 0);
                holder.user_name.setText("");

                holder.frameLayout.setVisibility(View.VISIBLE);
                /**
                 * 小心，由于recyclerview的重用机制，上次使用的long click痕迹没有自动消除
                 * 如果这里不填入一个无操作的listener，则以前的listener还在，还会进入以前的long click处理流程
                 */

                holder.frameLayout.setOnLongClickListener(new View.OnLongClickListener() {

                    @Override
                    public boolean onLongClick(View v) {
                        return false;
                    }
                });

                return;
            }
            position--;
        }

        if (position == ResultCount) {
            holder.user_icon.setImageResource(R.drawable.group_info_more);
            holder.frameLayout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (moreClickedListener != null) {
                        moreClickedListener.onMoreClicked();
                    }
                }

            });
            holder.user_mask.setAlpha((float) 0);
            holder.user_name.setText("");
            holder.frameLayout.setVisibility(View.VISIBLE);
            holder.frameLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            });
            return;
        }


        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + listItems.get(position).avatar)
//                .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.user_icon);
        MyLog.d("", "OKHTTP: adaptor avatar " + MyConfig.getApiDomain_NoSlash_GetResources() + listItems.get(position).avatar);
        holder.user_mask.setAlpha((float) 0);
        holder.frameLayout.setTag(Integer.valueOf(position));
        if (isJioned){
            holder.frameLayout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int position = (Integer) (v.getTag());
                    MyConfig.gotoUserInfo(context, listItems.get(position).id);
                    /*JsonGetGroupUserRet.GetGroupUserRet getGroupUserRet = listItems.get(position);
                    MyConfig.tempContactUniversal_FragmentUserInfoTongChuang = ContactUniversal.fromJsonGetGroupUserRet(getGroupUserRet);
                    Intent intent = new Intent(context, ActivityUserInfo.class);
                    context.startActivity(intent);*/

                }
            });
        }
        holder.user_name.setText(listItems.get(position).nickName);
        if (position == 0 ) {
//            holder.user_name.setTextColor(context.getResources().getColor(R.color.Red));
            holder.host_image.setVisibility(View.VISIBLE);
        } else {
            //          holder.user_name.setTextColor(context.getResources().getColor(R.color.Black));
            holder.host_image.setVisibility(View.GONE);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = ViewGroup.inflate(parent.getContext(),
                R.layout.gridview_pic, null);
        ViewHolder holder = new ViewHolder(view);


        MyLog.d("", "OKHTTP: onCreateViewHolder() ");

        return holder;
    }

    // public static interface OnItemClickListener {
    // public void onItemClick(View item);
    // }
    //
    // OnItemClickListener onItemClickListener;
    //
    // public void setOnItemClickListener(OnItemClickListener listener){
    // onItemClickListener = listener;
    // }
}