package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.docy.oldfriends.Activitis.ActivityClassEvaluate;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonNewsGroupRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;

/**
 * Created by khp on 2016/7/7.
 */
public class ListAdapterNewsGroup extends RecyclerView.Adapter<ListAdapterNewsGroup.ViewHolder>{
    Context context;
    List<JsonNewsGroupRet.NewsGroupRet> list;
    OnClick_item onClick_item;
    public interface OnClick_item {
        void onclick(int postion);
    }
    public ListAdapterNewsGroup(Context context,List<JsonNewsGroupRet.NewsGroupRet> list,OnClick_item o){
        this.context = context;
        this.list = list;
        onClick_item = o;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView first_relative_image,second_relative_image,first_image,second_image,three_image,four_image;
        TextView relative_text,first_text,second_text,three_text,four_text,time;
        TextView first_view,second_view,three_view,four_view;
        RelativeLayout relativeLayout;
        LinearLayout first_linear,second_linear,three_linear,four_linear,time_layout;

        public ViewHolder(View itemView) {
            super(itemView);
            time =(TextView) itemView.findViewById(R.id.listview_news_group_time);
            time_layout =(LinearLayout) itemView.findViewById(R.id.listview_news_group_date);

            first_relative_image =(ImageView) itemView.findViewById(R.id.listview_news_group_first_relative_image);
            second_relative_image = (ImageView) itemView.findViewById(R.id.listview_news_group_second_relative_image);
            relative_text =(TextView) itemView.findViewById(R.id.listview_news_group_relative_text);
            relativeLayout =(RelativeLayout) itemView.findViewById(R.id.listview_news_group_relative);

            first_image =(ImageView) itemView.findViewById(R.id.listview_news_group_first_image);
            first_text =(TextView) itemView.findViewById(R.id.listview_news_group_first_text);
            first_linear =(LinearLayout) itemView.findViewById(R.id.listview_news_group_first_linear);

            second_image = (ImageView) itemView.findViewById(R.id.listview_news_group_second_image);
            second_text =(TextView) itemView.findViewById(R.id.listview_news_group_second_text);
            second_linear =(LinearLayout) itemView.findViewById(R.id.listview_news_group_second_linear);

            three_image = (ImageView) itemView.findViewById(R.id.listview_news_group_three_image);
            three_text =(TextView) itemView.findViewById(R.id.listview_news_group_three_text);
            three_linear =(LinearLayout) itemView.findViewById(R.id.listview_news_group_three_linear);

            four_image = (ImageView) itemView.findViewById(R.id.listview_news_group_four_image);
            four_text =(TextView) itemView.findViewById(R.id.listview_news_group_four_text);
            four_linear =(LinearLayout) itemView.findViewById(R.id.listview_news_group_four_linear);

            first_view =(TextView) itemView.findViewById(R.id.listview_news_group_first_view);
            second_view =(TextView) itemView.findViewById(R.id.listview_news_group_two_view);
            three_view =(TextView) itemView.findViewById(R.id.listview_news_group_three_view);
            four_view =(TextView) itemView.findViewById(R.id.listview_news_group_four_view);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = ViewGroup.inflate(parent.getContext(), R.layout.listview_news_group,null);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        List<JsonNewsGroupRet.NewsGroupInfo_news> newsList = new ArrayList<>();

        if(list!=null
                &&list.size()>0
                &&list.get(position).info!=null
                &&list.get(position).info.news!=null
                &&list.get(position).info.news.size()>0) {
            newsList = list.get(position).info.news;
        }
        holder.relativeLayout.setVisibility(View.GONE);
        holder.first_linear.setVisibility(View.GONE);
        holder.second_linear.setVisibility(View.GONE);
        holder.three_linear.setVisibility(View.GONE);
        holder.four_linear.setVisibility(View.GONE);
        holder.time_layout.setVisibility(View.GONE);
        holder.first_view.setVisibility(View.GONE);
        holder.second_view.setVisibility(View.GONE);
        holder.three_view.setVisibility(View.GONE);
        holder.four_view.setVisibility(View.GONE);
        switch (newsList.size()){
            case 2:
                holder.time_layout.setVisibility(View.VISIBLE);
                holder.time.setText(time_format(list.get(position).createdAt));
                twoNews(newsList,holder);
                break;
            case 3:
                holder.time_layout.setVisibility(View.VISIBLE);
                holder.time.setText(time_format(list.get(position).createdAt));
                threeNews(newsList,holder);
                break;
            case 4:
                holder.time_layout.setVisibility(View.VISIBLE);
                holder.time.setText(time_format(list.get(position).createdAt));
                fourNews(newsList,holder);
                break;
            case 5:
                holder.time_layout.setVisibility(View.VISIBLE);
                holder.time.setText(time_format(list.get(position).createdAt));
                fiveNews(newsList,holder);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void twoNews(final List<JsonNewsGroupRet.NewsGroupInfo_news> two_list,ViewHolder holder){
        //有两条新闻时
        holder.relativeLayout.setVisibility(View.VISIBLE);
        holder.first_linear.setVisibility(View.VISIBLE);
        holder.first_view.setVisibility(View.VISIBLE);
        holder.relative_text.setText(two_list.get(0).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + two_list.get(0).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.first_relative_image);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + two_list.get(0).image[1])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.second_relative_image);

        holder.first_text.setText(two_list.get(1).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + two_list.get(1).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.first_image);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(two_list,0);
            }
        });
        holder.first_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(two_list,1);
            }
        });
    }

    public void threeNews(final List<JsonNewsGroupRet.NewsGroupInfo_news> three_list,ViewHolder holder){
        //有三条新闻
        holder.relativeLayout.setVisibility(View.VISIBLE);
        holder.first_linear.setVisibility(View.VISIBLE);
        holder.second_linear.setVisibility(View.VISIBLE);
        holder.first_view.setVisibility(View.VISIBLE);
        holder.second_view.setVisibility(View.VISIBLE);
        holder.relative_text.setText(three_list.get(0).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + three_list.get(0).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.first_relative_image);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + three_list.get(0).image[1])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.second_relative_image);

        holder.first_text.setText(three_list.get(1).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + three_list.get(1).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.first_image);

        holder.second_text.setText(three_list.get(2).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + three_list.get(2).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.second_image);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(three_list,0);
            }
        });
        holder.first_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(three_list,1);
            }
        });
        holder.second_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(three_list,2);
            }
        });

    }

    public void fourNews(final List<JsonNewsGroupRet.NewsGroupInfo_news> four_list,ViewHolder holder){
        //当有四条新闻
        holder.relativeLayout.setVisibility(View.VISIBLE);
        holder.first_linear.setVisibility(View.VISIBLE);
        holder.second_linear.setVisibility(View.VISIBLE);
        holder.three_linear.setVisibility(View.VISIBLE);
        holder.first_view.setVisibility(View.VISIBLE);
        holder.second_view.setVisibility(View.VISIBLE);
        holder.three_view.setVisibility(View.VISIBLE);
        holder.relative_text.setText(four_list.get(0).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + four_list.get(0).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.first_relative_image);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + four_list.get(0).image[1])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.second_relative_image);

        holder.first_text.setText(four_list.get(1).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + four_list.get(1).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.first_image);

        holder.second_text.setText(four_list.get(2).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + four_list.get(2).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.second_image);
        holder.three_text.setText(four_list.get(3).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + four_list.get(3).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.three_image);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(four_list,0);
            }
        });
        holder.first_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(four_list,1);
            }
        });
        holder.second_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(four_list,2);
            }
        });
        holder.three_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(four_list,3);
            }
        });
    }
    public void fiveNews(final List<JsonNewsGroupRet.NewsGroupInfo_news> five_list,ViewHolder holder){
        //有五条新闻时
        holder.relativeLayout.setVisibility(View.VISIBLE);
        holder.first_linear.setVisibility(View.VISIBLE);
        holder.second_linear.setVisibility(View.VISIBLE);
        holder.three_linear.setVisibility(View.VISIBLE);
        holder.four_linear.setVisibility(View.VISIBLE);
        holder.first_view.setVisibility(View.VISIBLE);
        holder.second_view.setVisibility(View.VISIBLE);
        holder.three_view.setVisibility(View.VISIBLE);
        holder.four_view.setVisibility(View.VISIBLE);
        holder.relative_text.setText(five_list.get(0).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + five_list.get(0).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.first_relative_image);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + five_list.get(0).image[1])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.second_relative_image);

        holder.first_text.setText(five_list.get(1).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + five_list.get(1).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.first_image);

        holder.second_text.setText(five_list.get(2).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + five_list.get(2).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.second_image);
        holder.three_text.setText(five_list.get(3).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + five_list.get(3).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.three_image);
        holder.four_text.setText(five_list.get(4).title);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + five_list.get(4).image[0])
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.four_image);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(five_list,0);
            }
        });
        holder.first_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(five_list,1);
            }
        });
        holder.second_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(five_list,2);
            }
        });
        holder.three_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(five_list,3);
            }
        });
        holder.four_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickToActivity(five_list,4);
            }
        });
    }

    public void onClickToActivity(List<JsonNewsGroupRet.NewsGroupInfo_news> list_data,int num){
        //点击后跳转页面
        Intent creditIntent = new Intent(context, ActivityClassEvaluate.class);
        creditIntent.putExtra(MyConfig.WEB_VIEW_URL_CATEGORY,MyConfig.WEB_VIEW_URL_CATEGORY_CREDIT);
        creditIntent.putExtra("url",list_data.get(num).link);
        creditIntent.putExtra("name",list_data.get(num).title);
        context.startActivity(creditIntent);
    }

    public String time_format(String str){
        //时间格式
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        String timeStr = formatter.format(new Date());
        if(timeStr.equals(MyConfig.Date2GroupInfoTimehorizon(MyConfig.UTCString2Date(str)))){
           return MyConfig.Date2GroupInfoTimeToday(MyConfig.UTCString2Date(str));
        }else{
            Format format = new SimpleDateFormat("M月d日 HH:mm");
            String time_Str = format.format(MyConfig.UTCString2Date(str));
            return time_Str;
        }
    }

}
