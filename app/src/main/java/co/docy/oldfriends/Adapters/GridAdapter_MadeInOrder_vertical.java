package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonMadeInOrderFromServiceRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;

/**
 * Created by khp on 2016/6/3.
 */
public class GridAdapter_MadeInOrder_vertical extends RecyclerView.Adapter<GridAdapter_MadeInOrder_vertical.ViewHolder> {
    Context context;
    List<JsonMadeInOrderFromServiceRet.JsonMadeInOrder_appCenter> list;
    public interface OnClickedListener {
        public void OnClicked(int position);
    }
    public OnClickedListener onClickedListener_item;
    private static LayoutInflater inflater = null;

    public GridAdapter_MadeInOrder_vertical(Context context,List<JsonMadeInOrderFromServiceRet.JsonMadeInOrder_appCenter> list,OnClickedListener onClickedListener_item){
        this.context = context;
        this.list = list;
        this.onClickedListener_item = onClickedListener_item;
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = ViewGroup.inflate(parent.getContext(),
                R.layout.made_in_order_gridadapter, null);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,final int position) {
        holder.order_app_text.setText(list.get(position).name);
        Picasso.with(context).load( MyConfig.getApiDomain_NoSlash_GetResources() + list.get(position).icon)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.order_app_img);

        if(MyConfig.NOTICE_UNREADED){
            if(list.get(position).name.equals("通知")) {
                holder.notice_unreaded.setVisibility(View.VISIBLE);
            }
        }
        if(MyConfig.FRIEND_CRICLE_UNREADED){
            if(list.get(position).name.equals("好友圈")) {
                holder.notice_unreaded.setVisibility(View.VISIBLE);
            }
        }

        holder.order_me_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onClickedListener_item != null){
                    onClickedListener_item.OnClicked(position);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView order_app_img, notice_unreaded;
        TextView order_app_text;
        RelativeLayout order_me_layout;
        public ViewHolder(View itemView){
            super(itemView);
            this.order_app_img =(ImageView) itemView.findViewById(R.id.made_in_order_grid_icon);
            this.order_app_text =(TextView) itemView.findViewById(R.id.made_in_order_grid_icon_text);
            this.order_me_layout =(RelativeLayout) itemView.findViewById(R.id.made_in_order_grid_icon_layout);
            this.notice_unreaded =(ImageView) itemView.findViewById(R.id.made_in_order_grid_icon_unread);
        }
    }
    /*@Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.made_in_order_gridadapter,null);
            viewHolder.order_app_img =(ImageView) convertView.findViewById(R.id.made_in_order_grid_icon);
            viewHolder.order_app_text =(TextView) convertView.findViewById(R.id.made_in_order_grid_icon_text);
            viewHolder.order_me_layout =(RelativeLayout) convertView.findViewById(R.id.made_in_order_grid_icon_layout);
            viewHolder.notice_unreaded =(ImageView) convertView.findViewById(R.id.made_in_order_grid_icon_unread);
            convertView.setTag(viewHolder);
        }else {
            viewHolder =(ViewHolder) convertView.getTag();
        }
        viewHolder.order_app_text.setText(list.get(position).name);
        Picasso.with(context).load( MyConfig.getApiDomain_NoSlash_GetResources() + list.get(position).icon)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(viewHolder.order_app_img);

        if(MyConfig.NOTICE_UNREADED){
            if(list.get(position).name.equals("通知")) {
                viewHolder.notice_unreaded.setVisibility(View.VISIBLE);
            }
        }

        viewHolder.order_me_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onClickedListener_item != null){
                    onClickedListener_item.OnClicked(position);
                }
            }
        });
        return convertView;
    }
    class ViewHolder{
        ImageView order_app_img,notice_unreaded;
        TextView order_app_text;
        RelativeLayout order_me_layout;
    }*/
}
