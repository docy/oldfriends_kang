package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ScheduleNotification;
import co.docy.oldfriends.Messages.JsonSetMessageReaded;
import co.docy.oldfriends.NetTools.HttpTools;
import co.docy.oldfriends.R;


public class ListAdapterScheduleNotification extends BaseAdapter {


    public interface OnLongClickedListener {
        public void OnLongClicked(int position);
    }

    public OnLongClickedListener longClickListener;

    public interface OnClickedListener {
        public void OnClicked(int position);
    }

    public OnClickedListener clickListener;

    private static class ViewHolder {
        ImageView row_sch_notification_cricle;
        LinearLayout row_sch_notification_linearlayout;
        TextView row_sch_notification_author,
                row_sch_notification_title,
                row_sch_notification_time,
                row_sch_notification_body;

    }

    Context context;
    final List<ScheduleNotification> list;
    private LayoutInflater inflater = null;
    public String searchContactKey = "";



    @Override
    public ScheduleNotification getItem(int position) {
        return list.get(position);
    }

    public ListAdapterScheduleNotification(Context context,
                                           List<ScheduleNotification> objects, OnClickedListener o, OnLongClickedListener oo) {
        this.context = context;
        this.list = objects;
        this.clickListener = o;
        this.longClickListener = oo;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder= null;
        if(list.get(position).type.trim().equals("false")){
       // if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_schedule_notification, null);
            viewHolder.row_sch_notification_linearlayout =(LinearLayout) convertView.findViewById(R.id.row_sch_notification_linearlayout);
            viewHolder.row_sch_notification_author = (TextView) convertView.findViewById(R.id.row_sch_notification_author);
            viewHolder.row_sch_notification_body =(TextView) convertView.findViewById(R.id.row_sch_notification_body);
            viewHolder.row_sch_notification_time =(TextView) convertView.findViewById(R.id.row_sch_notification_time);
            viewHolder.row_sch_notification_title =(TextView) convertView.findViewById(R.id.row_sch_notification_title);
            viewHolder.row_sch_notification_cricle =(ImageView) convertView.findViewById(R.id.row_sch_notification_icon);

           convertView.setTag(viewHolder);
        } else {
           // viewHolder = (ViewHolder) convertView.getTag();
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_schedule_notification_change, null);
            viewHolder.row_sch_notification_linearlayout =(LinearLayout) convertView.findViewById(R.id.row_sch_notification_change_linearlayout);
            viewHolder.row_sch_notification_author = (TextView) convertView.findViewById(R.id.row_sch_notification_change_author);
            viewHolder.row_sch_notification_body =(TextView) convertView.findViewById(R.id.row_sch_notification_change_body);
            viewHolder.row_sch_notification_time =(TextView) convertView.findViewById(R.id.row_sch_notification_change_time);
            viewHolder.row_sch_notification_title =(TextView) convertView.findViewById(R.id.row_sch_notification_change_title);
        }

        viewHolder.row_sch_notification_title.setText(list.get(position).title);
        viewHolder.row_sch_notification_author.setText(list.get(position).author);
        viewHolder.row_sch_notification_time.setText(getListTime(list.get(position).time));
        viewHolder.row_sch_notification_body.setText(list.get(position).body);
        //搜索的字体加颜色
        if (!TextUtils.isEmpty(searchContactKey)){
            if(list.get(position).title.contains(searchContactKey)) {
                MyConfig.addForeColorSpan(list.get(position).title, context.getResources().getColor(R.color.Red),
                        list.get(position).title.indexOf(searchContactKey), list.get(position).title.indexOf(searchContactKey) + searchContactKey.length(), viewHolder.row_sch_notification_title);
            }else if(list.get(position).author.contains(searchContactKey)){
                MyConfig.addForeColorSpan(list.get(position).author, context.getResources().getColor(R.color.Red),
                        list.get(position).author.indexOf(searchContactKey), list.get(position).author.indexOf(searchContactKey) + searchContactKey.length(), viewHolder.row_sch_notification_author);
            }else if(getListTime(list.get(position).time).contains(searchContactKey)){
                MyConfig.addForeColorSpan(getListTime(list.get(position).time), context.getResources().getColor(R.color.Red),
                        getListTime(list.get(position).time).indexOf(searchContactKey), getListTime(list.get(position).time).indexOf(searchContactKey) + searchContactKey.length(), viewHolder.row_sch_notification_time);
            }else if(list.get(position).body.contains(searchContactKey)){
                MyConfig.addForeColorSpan(list.get(position).body, context.getResources().getColor(R.color.Red),
                        list.get(position).body.indexOf(searchContactKey), list.get(position).body.indexOf(searchContactKey) + searchContactKey.length(), viewHolder.row_sch_notification_body);
            }
        }else {
            viewHolder.row_sch_notification_title.setText(list.get(position).title);
            viewHolder.row_sch_notification_author.setText(list.get(position).author);
            viewHolder.row_sch_notification_time.setText(getListTime(list.get(position).time));
            viewHolder.row_sch_notification_body.setText(list.get(position).body);
        }
        viewHolder.row_sch_notification_linearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickListener!=null) {
                    clickListener.OnClicked(position);
                    if (list.get(position).type.equals("false")) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Gson gson = new Gson();
                                JsonSetMessageReaded jsonSetMessageReaded = new JsonSetMessageReaded();
                                jsonSetMessageReaded.accessToken = MyConfig.usr_token;
                                String strJsonResetMessageNotice = gson.toJson(jsonSetMessageReaded, JsonSetMessageReaded.class);
                                final String s = new HttpTools().httpURLConnPostJson(MyConfig.getApiDomain_HaveSlash_ApiV2() + MyConfig.API_USER_SETMASSAGEREADED +"/"+list.get(position).nid , strJsonResetMessageNotice);
                                MyLog.d("kanghongpu",s);

                            }
                        }).start();

                    }
                    list.get(position).type = "true";
                }
            }
        });


        return convertView;

    }
    public String getListTime(String str){
        String day =MyConfig.Date2GroupInfoTime(MyConfig.UTCString2Date(str));
        SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd");
        String today = sdf.format(new Date());
        if(day.equals(today)){
            return MyConfig.Date2GroupInfoTimeToday(MyConfig.UTCString2Date(str));
        }else{
            return day;
        }
    }

}
