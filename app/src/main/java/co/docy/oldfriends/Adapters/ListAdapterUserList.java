package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.Messages.JsonUserGroupsRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.CustomFastScrollView;
import co.docy.oldfriends.Views.Function;
import co.docy.oldfriends.Views.PicassoTransformationRounded;


public class ListAdapterUserList extends BaseAdapter implements CustomFastScrollView.SectionIndexer {

//    public interface OnLongClickedListener {
//        public void OnLongClicked(int position);
//    }
//    public OnLongClickedListener longClickListener;

    public interface OnClickedListener {
        public void OnClicked(int position);
    }
    public OnClickedListener onClickedListener_person;
    public OnClickedListener onClickedListener_class;

    private int TextViewResourseId;
    private CustomFastScrollView.SectionIndexer sectionIndexer;


    private static class ViewHolder {

        RelativeLayout contact_layout_class;
        ImageView contact_layout_class_avatar;
        TextView contact_layout_class_name;

       // TextView contact_sort_title;

        LinearLayout contact_layout_person;
        ImageView contact_avatar;
        TextView contact_desc;
        TextView contact_nickname;
        TextView contact_phone_number;
    }

    Context context;
    final List<ContactUniversal> list;
    LinkedList<JsonUserGroupsRet.UserGroupsRet> classes;
    private LayoutInflater inflater = null;
    public String searchContactKey = "";

    @Override
    public ContactUniversal getItem(int position) {
        return list.get(position);
    }

    public ListAdapterUserList(Context context,
                               List<ContactUniversal> objects, LinkedList<JsonUserGroupsRet.UserGroupsRet> classes, OnClickedListener onClickedListener_person, OnClickedListener onClickedListener_class) {
        this.context = context;
        if(objects != null) {
            this.list = objects;
        }else{
            this.list = new LinkedList<>();
        }
        if(classes != null) {
            this.classes = classes;
        }else{
            this.classes = new LinkedList<>();
        }
        this.onClickedListener_person = onClickedListener_person;
        this.onClickedListener_class = onClickedListener_class;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size() +classes.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position_in_all, View convertView, ViewGroup parent) {


        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_contact, null);

            viewHolder.contact_layout_class = (RelativeLayout) convertView.findViewById(R.id.contact_layout_class);
            viewHolder.contact_layout_person = (LinearLayout) convertView.findViewById(R.id.contact_layout_person);
            viewHolder.contact_layout_class_avatar = (ImageView) convertView.findViewById(R.id.contact_layout_class_avatar);
            viewHolder.contact_layout_class_name = (TextView) convertView.findViewById(R.id.contact_layout_class_name);

            viewHolder.contact_avatar = (ImageView) convertView.findViewById(R.id.contact_avatar);
            viewHolder.contact_desc = (TextView) convertView.findViewById(R.id.contact_desc);
            viewHolder.contact_nickname = (TextView) convertView.findViewById(R.id.contact_nickname);
            viewHolder.contact_phone_number = (TextView) convertView.findViewById(R.id.contact_phone_number);
            //viewHolder.contact_sort_title = (TextView) convertView.findViewById(R.id.contact_sort_title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.contact_layout_class.setVisibility(View.GONE);
       // viewHolder.contact_sort_title.setVisibility(View.GONE);
        viewHolder.contact_layout_person.setVisibility(View.GONE);

        if(position_in_all < classes.size()){
            /**
             * 这里显示班级
             */
            final int position = position_in_all;
            viewHolder.contact_layout_class.setVisibility(View.VISIBLE);

            viewHolder.contact_layout_class.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onClickedListener_class != null){
                        onClickedListener_class.OnClicked(position);
                    }
                }
            });

            Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + classes.get(position).logo)
                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(viewHolder.contact_layout_class_avatar);

            viewHolder.contact_layout_class_name.setText(classes.get(position).name);

        }else {

            /**
             * 这里显示个人
             */
            final int position = position_in_all - classes.size();
         //   viewHolder.contact_sort_title.setVisibility(View.VISIBLE);
            viewHolder.contact_layout_person.setVisibility(View.VISIBLE);

            viewHolder.contact_layout_person.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onClickedListener_person != null){
                        onClickedListener_person.OnClicked(position);
                    }
                }
            });

//        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + list.get(position).avatar).into(viewHolder.contact_avatar);

            Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + list.get(position).avatar)
//                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .fit()
                    .centerCrop()
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(viewHolder.contact_avatar);

            viewHolder.contact_avatar.setTag(R.id.adapter_company_list_position, position);
            MyLog.d("", "HttpTools: jsonGetUserInfo.id position:" + position + " id:" + list.get(position).id);
            viewHolder.contact_desc.setText(list.get(position).company);
            viewHolder.contact_nickname.setText(list.get(position).nickName);
//            viewHolder.contact_phone_number.setText(list.get(position).phone);
           // if (list.get(position).phone != null && list.get(position).phone.length() > 3) {
//                String maskNumber = list.get(position).phone.substring(0, 3) + "******" + list.get(position).phone.substring(list.get(position).phone.length() - 1, list.get(position).phone.length());
//                viewHolder.contact_phone_number.setText(maskNumber);
              //  viewHolder.contact_phone_number.setText(list.get(position).phone);
          //  }

           /* if (!TextUtils.isEmpty(searchContactKey)){
                if(list.get(position).nickName!=null&&list.get(position).nickName.contains(searchContactKey)) {
                    MyConfig.addForeColorSpan(list.get(position).nickName, context.getResources().getColor(R.color.Red), list.get(position).nickName.indexOf(searchContactKey), list.get(position).nickName.indexOf(searchContactKey) + searchContactKey.length(), viewHolder.contact_nickname);
                }else if(list.get(position).phone.contains(searchContactKey)){
                    MyConfig.addForeColorSpan(list.get(position).phone, context.getResources().getColor(R.color.Red),
                            list.get(position).phone.indexOf(searchContactKey), list.get(position).phone.indexOf(searchContactKey) + searchContactKey.length(), viewHolder.contact_phone_number);
                }else if(list.get(position).company.contains(searchContactKey)){
                    MyConfig.addForeColorSpan(list.get(position).company, context.getResources().getColor(R.color.Red),
                            list.get(position).company.indexOf(searchContactKey), list.get(position).company.indexOf(searchContactKey) + searchContactKey.length(), viewHolder.contact_desc);
                }

            }else {
                viewHolder.contact_nickname.setText(list.get(position).nickName);
//                viewHolder.contact_desc.setText(list.get(position).company);
                viewHolder.contact_phone_number.setText(list.get(position).phone);
            }*/
//            if (position == 0) {
//                viewHolder.contact_sort_title.setVisibility(View.VISIBLE);
//                viewHolder.contact_sort_title.setText("" + list.get(position).name.subSequence(0, 1).charAt(0));
//            } else if (list.get(position).name.subSequence(0, 1).charAt(0) == list.get(position - 1).name.subSequence(0, 1).charAt(0)) {
//                viewHolder.contact_sort_title.setVisibility(View.GONE);
//
//            } else {
//                viewHolder.contact_sort_title.setVisibility(View.VISIBLE);
//                viewHolder.contact_sort_title.setText("" + list.get(position).name.subSequence(0, 1).charAt(0));
//            }
        }

        return convertView;

    }

    @Override
    public int getPositionForSection(int section) {
        return getSectionIndexer().getPositionForSection(section);
    }

    @Override
    public int getSectionForPosition(int position) {
        return getSectionIndexer().getSectionForPosition(position);
    }

    @Override
    public Object[] getSections() {
        return getSectionIndexer().getSections();
    }

    private CustomFastScrollView.SectionIndexer getSectionIndexer() {
        if (sectionIndexer == null) {
            sectionIndexer = createSectionIndexer(list);
        }
        return sectionIndexer;
    }

    private CustomFastScrollView.SectionIndexer createSectionIndexer(List<ContactUniversal> contactlist) {


        return createSectionIndexer(contactlist, new Function<ContactUniversal, String>() {

            @Override
            public String apply(ContactUniversal input) {
                return input.nickName;
            }
        });
    }


    /**
     * Create a SectionIndexer given an arbitrary function mapping countries to their section name.
     * @param countries
     * @param sectionFunction
     * @return
     */
    private CustomFastScrollView.SectionIndexer createSectionIndexer(
            List<ContactUniversal> countries, Function<ContactUniversal, String> sectionFunction) {

        List<String> sections = new ArrayList<String>();
        final List<Integer> sectionsToPositions = new ArrayList<Integer>();
        final List<Integer> positionsToSections = new ArrayList<Integer>();


        // assume the countries are properly sorted
        for (int i = 0; i < countries.size(); i++) {
            ContactUniversal country = countries.get(i);
            String section = sectionFunction.apply(country);
            if (sections.isEmpty() || !sections.get(sections.size() - 1).equals(section)) {
                // add a new section
                sections.add(section);
                // map section to position
                sectionsToPositions.add(i);
            }

            // map position to section
            positionsToSections.add(sections.size() - 1);
        }

        final String[] sectionsArray = sections.toArray(new String[sections.size()]);

        return new CustomFastScrollView.SectionIndexer() {

            @Override
            public Object[] getSections() {
                return sectionsArray;
            }

            @Override
            public int getSectionForPosition(int position) {
                return positionsToSections.get(position);
            }

            @Override
            public int getPositionForSection(int section) {
                return sectionsToPositions.get(section);
            }
        };
    }

    public void refreshSections() {
        sectionIndexer = null;
        getSectionIndexer();
    }

}



