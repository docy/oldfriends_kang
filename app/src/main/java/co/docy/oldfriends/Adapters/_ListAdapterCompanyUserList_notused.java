package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonGetCompanyUserListRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;


public class _ListAdapterCompanyUserList_notused extends BaseAdapter  implements StickyListHeadersAdapter {


    public interface OnLongClickedListener {
        public void OnLongClicked(int position);
    }

    public OnLongClickedListener longClickListener;

    public interface OnClickedListener {
        public void OnClicked(int position);
    }

    public OnClickedListener clickListener;

    private static class ViewHolder {
        ImageView contact_avatar;
        TextView contact_name;
        TextView contact_nickname;
        TextView contact_phone_number;
    }

    private static class HeaderViewHolder {
        ImageView contact_segment_button_space;
        TextView text;
    }



    Context context;
    final List<JsonGetCompanyUserListRet.GetCompanyUserListRet> list;
    private static LayoutInflater inflater = null;


    @Override
    public JsonGetCompanyUserListRet.GetCompanyUserListRet getItem(int position) {
        return list.get(position);
    }

    public _ListAdapterCompanyUserList_notused(Context context,
                                               List<JsonGetCompanyUserListRet.GetCompanyUserListRet> objects, OnClickedListener o, OnLongClickedListener oo) {
        this.context = context;
        this.list = objects;
        this.clickListener = o;
        this.longClickListener = oo;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        JsonGetCompanyUserListRet.GetCompanyUserListRet user = getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_contact, null);

            viewHolder.contact_avatar = (ImageView) convertView.findViewById(R.id.contact_avatar);
            viewHolder.contact_name = (TextView) convertView.findViewById(R.id.contact_desc);
            viewHolder.contact_nickname = (TextView) convertView.findViewById(R.id.contact_nickname);
            viewHolder.contact_phone_number = (TextView) convertView.findViewById(R.id.contact_phone_number);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


//        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + list.get(position).avatar).into(viewHolder.contact_avatar);

        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + list.get(position).avatar)
                .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(viewHolder.contact_avatar);

        viewHolder.contact_avatar.setTag(R.id.adapter_company_list_position, position);
        MyLog.d("", "HttpTools: jsonGetUserInfo.id position:" + position + " id:" + list.get(position).id);
        viewHolder.contact_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.gotoUserInfo(context, list.get(((Integer) (v.getTag(R.id.adapter_company_list_position))).intValue()).id);
            }
        });
        viewHolder.contact_name.setText(context.getString(R.string.user_name)+list.get(position).name);
        viewHolder.contact_nickname.setText(list.get(position).nickName);
        String maskNumber = list.get(position).phone.substring(0,3)+"******"+list.get(position).phone.substring(list.get(position).phone.length()-1,list.get(position).phone.length());
        viewHolder.contact_phone_number.setText(maskNumber);

        return convertView;

    }


    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.row_contact_header, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.text_header);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
        String headerText = "" + list.get(position).name.subSequence(0, 1).charAt(0);
        holder.text.setText(headerText.toUpperCase());
        holder.contact_segment_button_space = (ImageView) convertView.findViewById(R.id.contact_segment_button_space);

//        if(position == 0) {
//            holder.contact_segment_button_space.setVisibility(View.VISIBLE);
//        }else{
            holder.contact_segment_button_space.setVisibility(View.GONE);
//        }

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        //return the first character of the country as ID because this is what headers are based upon
        return list.get(position).name.subSequence(0, 1).charAt(0);
    }


    private void setOnClick(View v, int position) {
        v.setTag(R.id.adapter_company_list_position, position);
        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (longClickListener != null) {
                    longClickListener.OnLongClicked(
                            ((Integer) (v.getTag(R.id.adapter_company_list_position))).intValue()
                    );
                }
                return false;
            }
        });
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.OnClicked(
                            ((Integer) (v.getTag(R.id.adapter_company_list_position))).intValue()
                    );
                }else{

                }
            }
        });
    }

}
