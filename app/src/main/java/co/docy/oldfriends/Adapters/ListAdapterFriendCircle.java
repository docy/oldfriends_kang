package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Activitis.ActivityGallery;
import co.docy.oldfriends.Activitis.ActivityVideoPlayer;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.FriendCricleMsg;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;

/**
 * Created by khp on 2016/7/22.
 */
public class ListAdapterFriendCircle extends BaseAdapter {

    List<FriendCricleMsg> list;
    LinkedList<FriendCricleMsg> likes = new LinkedList<>();
    LinkedList<FriendCricleMsg> comments = new LinkedList<>();
    Context context;
    private static LayoutInflater inflater = null;
    public ListAdapterFriendCircle(Context context, List<FriendCricleMsg> list, ChangeFriendLikes o){
        this.context = context;
        this.list = list;
        this.changeFriendLikes = o;
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    public interface ChangeFriendLikes{
        //所有点击公用一个接口，different为区分点击的类型，0：点赞，1：删除朋友圈,2：评论
        public void changeFriendLikesListen(int postion,boolean tag,int different);
    }
    public ChangeFriendLikes changeFriendLikes;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final FriendCricleMsg friendsMsg = list.get(position);

        //点赞的集合
        likes.clear();
        if(list!=null&&list.get(position).likeMsg!=null) {
            likes.addAll(likes.size(), list.get(position).likeMsg);

        }
        //评论的集合
        comments.clear();
        if(list!=null&&list.get(position).commentMsg!=null) {
            comments.addAll(comments.size(), list.get(position).commentMsg);
        }
         final ViewHolder holder ;
        if(convertView==null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adapter_cricle_of_friends,null);
            holder.comment_linear = (LinearLayout) convertView.findViewById(R.id.adapter_cricle_of_friends_comment_linear);
            holder.fav_linear = (LinearLayout) convertView.findViewById(R.id.adapter_cricle_of_friends_fav_linear);
            holder.message_time = (TextView) convertView.findViewById(R.id.adapter_cricle_of_friends_time);
            holder.message_del = (TextView) convertView.findViewById(R.id.adapter_cricle_of_friends_del);
            holder.fav_num = (CheckBox) convertView.findViewById(R.id.adapter_cricle_of_friends_fav);
            holder.fav_name =(TextView) convertView.findViewById(R.id.adapter_cricle_of_friends_fav_name);
            holder.comment_num = (TextView) convertView.findViewById(R.id.adapter_cricle_of_friends_comment);
            holder.comment_name =(LinearLayout) convertView.findViewById(R.id.adapter_cricle_of_friends_comment_name);
            holder.peopel_photo = (ImageView) convertView.findViewById(R.id.adapter_cricle_of_friends_people_photo);
            holder.people_name = (TextView) convertView.findViewById(R.id.adapter_cricle_of_friends_people_name);

            holder.photo = (ImageView) convertView.findViewById(R.id.adapter_cricle_of_friends_photo);
            holder.title = (TextView) convertView.findViewById(R.id.adapter_cricle_of_friends_title);
            holder.image_play =(ImageView) convertView.findViewById(R.id.adapter_cricle_of_friends_photo_start);
            holder.image_relative =(RelativeLayout) convertView.findViewById(R.id.adapter_cricle_of_friends_photo_relative);
            holder.all_layout =(RelativeLayout) convertView.findViewById(R.id.adapter_cricle_of_friends_relativelatout);
            convertView.setTag(holder);
        }else{
            holder =(ViewHolder) convertView.getTag();
        }
        holder.title.setVisibility(View.GONE);
        holder.photo.setVisibility(View.GONE);
        holder.message_del.setVisibility(View.GONE);
        holder.fav_linear.setVisibility(View.GONE);
        holder.comment_linear.setVisibility(View.GONE);
        holder.image_play.setVisibility(View.GONE);
        holder.image_relative.setVisibility(View.GONE);

        holder.people_name.setText(friendsMsg.user_nickName);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + friendsMsg.user_avatar)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.peopel_photo);
        holder.peopel_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.gotoUserInfo(context,friendsMsg.userId);
            }
        });
        holder.message_time.setText(MyConfig.getTime(friendsMsg.createdAt));
        if(friendsMsg.userId == MyConfig.usr_id){
            holder.message_del.setVisibility(View.VISIBLE);
        }
        if(friendsMsg.title!=null&&friendsMsg.title.length()>0) {
            holder.title.setVisibility(View.VISIBLE);
            holder.title.setText(friendsMsg.title);
        }
        //根据数据的格式来判断显示  图片/视频/文字
        if(friendsMsg.image!=null) {
            holder.image_relative.setVisibility(View.VISIBLE);
            if(friendsMsg.videoPath!=null) {
                holder.image_play.setVisibility(View.VISIBLE);
            }
            holder.photo.setVisibility(View.VISIBLE);
            Glide.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + friendsMsg.image)
                    .centerCrop()
                    .into(holder.photo);
        }/*else {
            holder.image_relative.setVisibility(View.VISIBLE);
            holder.photo.setVisibility(View.VISIBLE);
            Glide.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + friendsMsg.image)
                    .centerCrop()
                    .into(holder.photo);
        }*/
        holder.photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(friendsMsg.videoPath!=null){

                    friendsMsg.videoProgress = MyConfig.getFileDownloadProgress(friendsMsg.videoPath_local, friendsMsg.videosize);

                    if(friendsMsg.videoProgress == 100){
                        MyLog.d("kanghongpu",friendsMsg.videoPath_local+111);
                        Intent intent = new Intent(context, ActivityVideoPlayer.class);
                        intent.putExtra("mode", 1);
                        intent.putExtra("path", friendsMsg.videoPath_local);
                        intent.putExtra("recorder_os",friendsMsg.os);
                        context.startActivity(intent);
                    }else {
                        MyLog.d("kanghongpu",friendsMsg.videoPath_local+222);
                        Intent intent = new Intent(context, ActivityVideoPlayer.class);
                        intent.putExtra("mode",0);
                        intent.putExtra("path", MyConfig.getApiDomain_NoSlash_GetResources() + friendsMsg.videoPath);
                        intent.putExtra("recorder_os",friendsMsg.os);
                        context.startActivity(intent);
                    }

                }else {

                    Intent intent = new Intent(context, ActivityGallery.class);
                    intent.putExtra("type", 10);
                    if(friendsMsg.fullPath!=null) {
                        intent.putExtra("url", MyConfig.getApiDomain_NoSlash_GetResources() + friendsMsg.fullPath);
                    }else{
                        intent.putExtra("url", MyConfig.getApiDomain_NoSlash_GetResources() + friendsMsg.image);
                    }
                    context.startActivity(intent);
                }
            }
        });
        //显示点赞的信息
        if(likes == null||likes.size()==0){
            holder.fav_num.setText("");
            holder.fav_num.setChecked(false);
        }
        if(likes!=null&&likes.size()>0){
            holder.fav_num.setChecked(friendsMsg.liked);
            holder.fav_num.setText(likes.size()+"");
            holder.fav_linear.setVisibility(View.VISIBLE);
            StringBuilder builder = new StringBuilder();
            for(int i=0;i<likes.size();i++){
                builder.append(likes.get(i).like_nickName+"\r");
            }
            holder.fav_name.setText(builder.toString());
        }
        //显示评论的信息
        holder.comment_name.removeAllViews();
        if(comments == null||comments.size()==0){
            holder.comment_num.setText("");
        }
        if (comments!=null&&comments.size()>0){
            holder.comment_num.setText(comments.size()+"");
            holder.comment_linear.setVisibility(View.VISIBLE);
            Collections.reverse(comments);
            int lenght=comments.size()>5?5:comments.size();
            for(int i=0;i<lenght;i++) {
                TextView text = new TextView(context);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(0, 10, 0, 0);
                MyConfig.addForeColorSpan(comments.get(i).comment_nickName+":"+comments.get(i).comment,
                        context.getResources().getColor(R.color.xjt_subject),
                        (comments.get(i).comment_nickName+" : "+comments.get(i).comment).indexOf(comments.get(i).comment_nickName),
                        (comments.get(i).comment_nickName+" : "+comments.get(i).comment).indexOf(comments.get(i).comment_nickName) + (comments.get(i).comment_nickName).length(),
                        text);
//                text.setText(EmojiUtils.showEmoji(context,comments.get(i).comment_nickName+":"+comments.get(i).comment));
                text.setLayoutParams(params);
                holder.comment_name.addView(text);
            }

        }
        //点击点赞按钮
        holder.fav_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(friendsMsg.liked){
                    changeFriendLikes.changeFriendLikesListen(position,true,0);
                }else {
                    changeFriendLikes.changeFriendLikesListen(position,false,0);

                }

            }
        });
        //点击评论的按钮
        holder.comment_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFriendLikes.changeFriendLikesListen(position,false,2);
            }
        });

        //删除自己发的朋友圈
        holder.message_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFriendLikes.changeFriendLikesListen(position,false,1);
            }
        });
        holder.all_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFriendLikes.changeFriendLikesListen(position,false,3);
            }
        });


        return convertView;
    }

    public static class ViewHolder {
        ImageView peopel_photo,photo,image_play;
        TextView people_name,title,message_time,message_del,comment_num,fav_name;
        LinearLayout comment_linear,fav_linear,comment_name;
        CheckBox fav_num;
        RelativeLayout image_relative,all_layout;

    }




}
