package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonChooseGroupToSendNotice;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;


public class ListAdapterChooseGroupToSendNotice extends BaseAdapter   {

    public interface OnClickedListener {
        public void OnClicked(int position);
    }
    public OnClickedListener clickListener;

    public interface OnChooseChangedListener {
        public void OnClicked(int position, boolean isChecked);
    }
    public OnChooseChangedListener chooseChangedListener;

    private static class ViewHolder {
        LinearLayout choose_group_layout;
        ImageView choose_group_img;
        TextView choose_group_name;
        ToggleButton choose_group_choose;
    }


    Context context;
    List<JsonChooseGroupToSendNotice.ChooseGroupToSendNotice> list;

    private static LayoutInflater inflater = null;


    @Override
    public JsonChooseGroupToSendNotice.ChooseGroupToSendNotice getItem(int position) {
        return list.get(position);
    }

    public ListAdapterChooseGroupToSendNotice(Context context,
                                              List<JsonChooseGroupToSendNotice.ChooseGroupToSendNotice> objects, OnClickedListener o,OnChooseChangedListener ooo) {
        this.context = context;
        this.list = objects;
        this.clickListener = o;
        this.chooseChangedListener = ooo;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        MyLog.d("","ListAdapterContactOfCompanyMultiChoose getCount "+list.size());
        return list.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        MyLog.d("","ListAdapterContactOfCompanyMultiChoose position "+position);


        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.choose_group_to_send_notice, null);
            viewHolder.choose_group_layout = (LinearLayout)convertView.findViewById(R.id.choose_group_send_notice_layout);
            viewHolder.choose_group_img = (ImageView) convertView.findViewById(R.id.choose_group_send_notice_icon);
            viewHolder.choose_group_name = (TextView) convertView.findViewById(R.id.choose_group_send_notice_name);
            viewHolder.choose_group_choose = (ToggleButton) convertView.findViewById(R.id.choose_group_send_notice_choose);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() +list.get(position).logo)
//                .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(viewHolder.choose_group_img);
        viewHolder.choose_group_name.setText(list.get(position).name);
        viewHolder.choose_group_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(clickListener != null){
                    clickListener.OnClicked(position);
                }
            }
        });
        viewHolder.choose_group_choose.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(chooseChangedListener != null) {
                    chooseChangedListener.OnClicked(position, isChecked);
                }
            }
        });

        return convertView;

    }





}
