package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import co.docy.oldfriends.Push.PushMsg;
import co.docy.oldfriends.R;


public class ListAdapterNotification extends BaseAdapter {


    public interface OnLongClickedListener {
        public void OnLongClicked(int position);
    }

    public OnLongClickedListener longClickListener;

    public interface OnClickedListener {
        public void OnClicked(int position);
    }

    public OnClickedListener clickListener;

    private static class ViewHolder {
        TextView row_notification_text;
    }

    Context context;
    final List<PushMsg> list;
    private static LayoutInflater inflater = null;


    @Override
    public PushMsg getItem(int position) {
        return list.get(position);
    }

    public ListAdapterNotification(Context context,
                                   List<PushMsg> objects, OnClickedListener o, OnLongClickedListener oo) {
        this.context = context;
        this.list = objects;
        this.clickListener = o;
        this.longClickListener = oo;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        PushMsg PushMsg = getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_notification, null);
            viewHolder.row_notification_text = (TextView) convertView.findViewById(R.id.row_notification_text);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.row_notification_text.setText(list.get(position).s);


        return convertView;

    }


}
