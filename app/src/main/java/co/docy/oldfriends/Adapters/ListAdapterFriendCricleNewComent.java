package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonFriendCricleNewCommentToMeRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import co.docy.oldfriends.emoji.EmojiUtils;

/**
 * Created by khp on 2016/8/1.
 */
public class ListAdapterFriendCricleNewComent extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater = null;
    List<JsonFriendCricleNewCommentToMeRet.FriendCricleNewCommentToMeRet> list = new ArrayList<>();
    public interface OnclickItem{
        void onclickitem(int postion);
    }
    public OnclickItem onclickItem;
    public ListAdapterFriendCricleNewComent(Context context,List<JsonFriendCricleNewCommentToMeRet.FriendCricleNewCommentToMeRet> list,OnclickItem o){
        this.context = context;
        this.list = list;
        this.onclickItem = o;
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.friend_cricle_new_comment,null);
            holder.comment_linear =(LinearLayout) convertView.findViewById(R.id.new_comment_linearlayout);
            holder.comment_like =(ImageView) convertView.findViewById(R.id.new_comment_people_like);
            holder.comment_photo =(ImageView) convertView.findViewById(R.id.new_comment_people_photo);
            holder.context_image =(ImageView) convertView.findViewById(R.id.new_comment_old_image);
            holder.comment_name =(TextView) convertView.findViewById(R.id.new_comment_people_name);
            holder.comment_txt =(TextView) convertView.findViewById(R.id.new_comment_people_comment);
            holder.comment_time =(TextView) convertView.findViewById(R.id.new_comment_people_comment_time);
            holder.context_txt =(TextView) convertView.findViewById(R.id.new_comment_old_context);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.comment_like.setVisibility(View.GONE);
        holder.comment_txt.setVisibility(View.GONE);
        holder.context_txt.setVisibility(View.GONE);
        holder.context_image.setVisibility(View.GONE);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + list.get(position).Info.User.avatar)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.comment_photo);
        holder.comment_name.setText(list.get(position).Info.User.nickName);
        holder.comment_time.setText(MyConfig.getTime(list.get(position).createdAt));

        if(list.get(position).type==3){
            holder.comment_txt.setVisibility(View.VISIBLE);
            holder.comment_txt.setText(EmojiUtils.showEmoji(context,list.get(position).Info.comment));
        }else if(list.get(position).type==2){
            holder.comment_like.setVisibility(View.VISIBLE);
            holder.comment_like.setImageResource(R.drawable.like_normal_3x);
        }
        if(list.get(position).Moment.title!=null&&list.get(position).Moment.image!=null){
            holder.context_image.setVisibility(View.VISIBLE);
            Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + list.get(position).Moment.image)
                    .fit()
                    .centerCrop()
                    .into(holder.context_image);
        }else if(list.get(position).Moment.title!=null&&list.get(position).Moment.image==null){

            holder.context_txt.setVisibility(View.VISIBLE);
            holder.context_txt.setText(list.get(position).Moment.title);

        }else if(list.get(position).Moment.image!=null){
            holder.context_image.setVisibility(View.VISIBLE);
            Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + list.get(position).Moment.image)
                    .fit()
                    .centerCrop()
                    .into(holder.context_image);
        }
        holder.comment_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclickItem.onclickitem(position);
            }
        });

        return convertView;
    }

    public static class ViewHolder{
        ImageView comment_photo,comment_like,context_image;
        TextView comment_name,comment_txt,comment_time,context_txt;
        LinearLayout comment_linear;
    }
}
