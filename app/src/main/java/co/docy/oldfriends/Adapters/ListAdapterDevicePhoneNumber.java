package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.util.List;

import co.docy.oldfriends.DataClass.DevicePhoneContact;
import co.docy.oldfriends.R;


public class ListAdapterDevicePhoneNumber extends BaseAdapter {


    public interface OnLongClickedListener {
        public void OnLongClicked(int position);
    }
    public OnLongClickedListener longClickListener;

    public interface OnClickedListener {
        public void OnClicked(int position);
    }
    public OnClickedListener clickListener;

    private static class ViewHolder {
        RelativeLayout contact_layout;
        ImageView contact_avatar;
        TextView contact_name;
        TextView contact_phone_number;
    }

    Context context;
    final List<DevicePhoneContact> list;
    private static LayoutInflater inflater = null;


    @Override
    public DevicePhoneContact getItem(int position) {
        return list.get(position);
    }

    public ListAdapterDevicePhoneNumber(Context context,
                                        List<DevicePhoneContact> objects, OnClickedListener o, OnLongClickedListener oo) {
        this.context = context;
        this.list = objects;
        this.clickListener = o;
        this.longClickListener = oo;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        DevicePhoneContact devicePhoneContact = getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_phone_contact, null);
            viewHolder.contact_layout = (RelativeLayout)convertView.findViewById(R.id.contact_layout_person);
            viewHolder.contact_avatar = (ImageView) convertView.findViewById(R.id.contact_avatar);
            viewHolder.contact_name = (TextView) convertView.findViewById(R.id.contact_desc);
            viewHolder.contact_phone_number = (TextView) convertView.findViewById(R.id.contact_phone_number);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.contact_avatar.setImageBitmap(devicePhoneContact.photo_bitmap);
        viewHolder.contact_name.setText(devicePhoneContact.name);
        viewHolder.contact_phone_number.setText(devicePhoneContact.phone);

        viewHolder.contact_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickListener != null){
                    clickListener.OnClicked(position);
                }
            }
        });

        return convertView;

    }


}
