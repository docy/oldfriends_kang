package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonGetSignedByDateRet;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/5/24.
 */
public class ListAdapterGetSignedMessage extends BaseAdapter {

    LinkedList<JsonGetSignedByDateRet.CheckSignedTimeList> list;
    Context context;
    private static LayoutInflater inflater = null;

    public ListAdapterGetSignedMessage(Context context,LinkedList<JsonGetSignedByDateRet.CheckSignedTimeList> list){
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView==null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.show_signed_message, null);
            viewHolder.time =(TextView) convertView.findViewById(R.id.show_signed_message_time);
            viewHolder.location =(TextView) convertView.findViewById(R.id.show_signed_message_loc);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.time.setText(MyConfig.Date2StringDateAndTime(MyConfig.UTCString2Date(list.get(position).createdAt)));
        viewHolder.location.setText(list.get(position).location);
        return convertView;
    }
    private static class ViewHolder {

        TextView time,location;

    }

}
