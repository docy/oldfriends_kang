package co.docy.oldfriends.Adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;


public class ListAdapterAddGroupMember extends RecyclerView.Adapter<ListAdapterAddGroupMember.ViewHolder> {


    private ArrayList<ContactUniversal> mSelectedContactUniversals;
    private View.OnClickListener mOnClickInvite;
    private View.OnClickListener mOnClickrKick;
    private View.OnClickListener mOnClickMore;


    public ListAdapterAddGroupMember(ArrayList<ContactUniversal> selectedContactUniversals, View.OnClickListener onClickInvite, View.OnClickListener onClickrKick,
                                     View.OnClickListener onClickMore) {
        mSelectedContactUniversals = selectedContactUniversals;
        mOnClickInvite = onClickInvite;
        mOnClickrKick = onClickrKick;
        mOnClickMore = onClickMore;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public ImageView mImageView;
        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.tv_add_member_name);
            mImageView = (ImageView) v.findViewById(R.id.iv_add_member_icon);
        }
    }

    @Override
    public int getItemCount() {
        if(mSelectedContactUniversals.size()==0){
            return 1;
        }else if (mSelectedContactUniversals.size()<13){
            return mSelectedContactUniversals.size()+2;
        }else{
            return 15;
        }


    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_add_group_member, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.mImageView.setImageResource(R.drawable.invitation_3x);
        /**添加按钮*/
        if(position == 0){
            holder.mTextView.setVisibility(View.INVISIBLE);
            holder.mImageView.setOnClickListener(mOnClickInvite);
            /**减少按钮*/
        }else if(position == 1){
            holder.mTextView.setVisibility(View.INVISIBLE);
            holder.mImageView.setOnClickListener(mOnClickrKick);
            holder.mImageView.setImageResource(R.drawable.group_kick_people_3x);
            if(mSelectedContactUniversals.size() == 0){
                holder.mImageView.setVisibility(View.INVISIBLE);
                holder.mImageView.setOnClickListener(null);
            }
        }else if(position == 14){
            holder.mTextView.setVisibility(View.INVISIBLE);
            holder.mImageView.setImageResource(R.drawable.group_info_more);
            holder.mImageView.setOnClickListener(mOnClickMore);
        }else {
            final ContactUniversal contactUniversal = mSelectedContactUniversals.get(position-2);
            holder.mTextView.setVisibility(View.VISIBLE);
            holder.mTextView.setText(contactUniversal.nickName);
            Picasso.with(holder.mImageView.getContext()).load(MyConfig.getApiDomain_NoSlash_GetResources() + contactUniversal.avatar)
//                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .fit()
                    .centerCrop()
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(holder.mImageView);

            holder.mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyConfig.gotoUserInfo(v.getContext(), contactUniversal.id);
                }
            });

        }

    }

}