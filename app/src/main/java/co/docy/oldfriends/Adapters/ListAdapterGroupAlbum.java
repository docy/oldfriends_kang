package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonGetGroupAlbumRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import co.docy.oldfriends.Views.ViewGridSquareForRecyclerview;


public class ListAdapterGroupAlbum extends Adapter<ListAdapterGroupAlbum.ViewHolder> {


    public interface OnThumbnailClick{
        void onClick(int position);
    }
    public OnThumbnailClick onThumbnailClick;

    List<JsonGetGroupAlbumRet.GetGroupAlbumRet> listItems;
    Context context;

    public ListAdapterGroupAlbum(Context context, List<JsonGetGroupAlbumRet.GetGroupAlbumRet> li, OnThumbnailClick onThumbnailClick) {
        this.listItems = li;
        this.context = context;
        this.onThumbnailClick = onThumbnailClick;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewGridSquareForRecyclerview group_album_image_layout;
        public ImageView group_album_image;


        public ViewHolder(View itemView) {
            super(itemView);
            this.group_album_image = (ImageView) itemView.findViewById(R.id.group_album_image);
            this.group_album_image_layout = (ViewGridSquareForRecyclerview) itemView.findViewById(R.id.group_album_image_layout);
        }
    }


    @Override
    public int getItemCount() {

        return listItems.size();

    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        MyLog.d("", "getGroupAlbum: adapter position " + position + " createAt " + listItems.get(position).createdAt);


        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + listItems.get(position).thumbNail)
//        Picasso.with(context).load(R.drawable.audio_unread_dot_3x)
//                .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(holder.group_album_image);

        if(onThumbnailClick != null){

            holder.group_album_image_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onThumbnailClick.onClick(position);
                }
            });

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = ViewGroup.inflate(parent.getContext(),
                R.layout.gridview_album, null);
        ViewHolder holder = new ViewHolder(view);


//        MyLog.d("", "OKHTTP: onCreateViewHolder() ");

        return holder;
    }

}