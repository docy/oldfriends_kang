package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonGetTopicDetailRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;

/**
 * Created by fighting on 2016/6/13.
 */
public class ListAdapterActivityPersonList extends BaseAdapter{
    LinkedList<JsonGetTopicDetailRet.JoinedUsers> joinedUsers;
    Context mContext;

    public ListAdapterActivityPersonList(Context context,LinkedList<JsonGetTopicDetailRet.JoinedUsers> list) {
        joinedUsers = list;
        mContext = context;
    }

    @Override
    public int getCount() {
        return joinedUsers.size();
    }

    @Override
    public JsonGetTopicDetailRet.JoinedUsers getItem(int position) {
        return joinedUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        JsonGetTopicDetailRet.JoinedUsers usersList = this.joinedUsers.get(position);
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView =View.inflate(mContext,R.layout.row_contact_multi_choose, null);
            viewHolder.contact_avatar = (ImageView) convertView.findViewById(R.id.contact_avatar);
            viewHolder.contact_name = (TextView) convertView.findViewById(R.id.contact_desc);
            viewHolder.contact_phone = (TextView) convertView.findViewById(R.id.contact_phone);
            viewHolder.contact_choose = (ToggleButton) convertView.findViewById(R.id.contact_choose);
            viewHolder.contact_phone.setVisibility(View.GONE);
            viewHolder.contact_choose.setVisibility(View.GONE);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(mContext).load(MyConfig.getApiDomain_NoSlash_GetResources() + usersList.avatar)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(viewHolder.contact_avatar);

        viewHolder.contact_name.setText(usersList.nickName);
        viewHolder.contact_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.gotoUserInfo(mContext, joinedUsers.get(position).id);
            }
        });
        return convertView;
    }
    private static class ViewHolder {
        ImageView contact_avatar;
        TextView contact_name;
        TextView contact_phone;
        ToggleButton contact_choose;
    }

}
