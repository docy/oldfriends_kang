package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.CustomFastScrollView;
import co.docy.oldfriends.Views.Function;
import co.docy.oldfriends.Views.PicassoTransformationRounded;


public class ListAdapterUserListWithHeader extends BaseAdapter implements CustomFastScrollView.SectionIndexer {

//    public interface OnLongClickedListener {
//        public void OnLongClicked(int position);
//    }
//    public OnLongClickedListener longClickListener;

    public interface OnClickedListener {
        public void OnClicked(int position);
    }
    public OnClickedListener onClickedListener_person;
    public OnClickedListener onClickedListener_class;

    private int TextViewResourseId;
    private CustomFastScrollView.SectionIndexer sectionIndexer;


    private static class ViewHolder {

        TextView contact_sort_title;

        LinearLayout contact_layout_person;
        ImageView contact_avatar;
        TextView contact_desc;
        TextView contact_nickname;
        TextView contact_phone_number;
    }

    Context context;
    final List<ContactUniversal> list;

    private static LayoutInflater inflater = null;

    @Override
    public ContactUniversal getItem(int position) {
        return list.get(position);
    }

    public ListAdapterUserListWithHeader(Context context,
                                         List<ContactUniversal> objects,OnClickedListener onClickedListener_person) {
        this.context = context;
        if(objects != null) {
            this.list = objects;
        }else{
            this.list = new LinkedList<>();
        }

        this.onClickedListener_person = onClickedListener_person;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_contact_tongchuang, null);

            viewHolder.contact_layout_person = (LinearLayout) convertView.findViewById(R.id.contact_layout_person_tongchuang);

            viewHolder.contact_avatar = (ImageView) convertView.findViewById(R.id.contact_avatar);
            viewHolder.contact_desc = (TextView) convertView.findViewById(R.id.contact_desc);
            viewHolder.contact_nickname = (TextView) convertView.findViewById(R.id.contact_nickname);
            viewHolder.contact_phone_number = (TextView) convertView.findViewById(R.id.contact_phone_number);
//            viewHolder.contact_sort_title = (TextView) convertView.findViewById(R.id.contact_sort_title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

//        viewHolder.contact_sort_title.setVisibility(View.GONE);
        viewHolder.contact_layout_person.setVisibility(View.GONE);


//            viewHolder.contact_sort_title.setVisibility(View.VISIBLE);
            viewHolder.contact_layout_person.setVisibility(View.VISIBLE);

            viewHolder.contact_layout_person.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onClickedListener_person != null) {
                        onClickedListener_person.OnClicked(position);
                    }
                }
            });

 //       Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + list.get(position).avatar).into(viewHolder.contact_avatar);
            if (list.get(position) != null) {
               /* // 根据position获取分类的首字母的Char ascii值
                int section = getSectionForPosition(position);
                // 如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
                if (position == getPositionForSection(section)) {
                    viewHolder.contact_sort_title.setText(list.get(position).nameFristLetter.substring(0,1));
                } else {
                    viewHolder.contact_sort_title.setVisibility(View.GONE);
                }*/
                Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + list.get(position).avatar)
//                        .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                        .fit()
                        .centerCrop()
                        .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                        .into(viewHolder.contact_avatar);

                viewHolder.contact_avatar.setTag(R.id.adapter_company_list_position, position);
                MyLog.d("", "HttpTools: jsonGetUserInfo.id position:" + position + " id:" + list.get(position).id);
//                viewHolder.contact_desc.setText(list.get(position).company);
                viewHolder.contact_nickname.setText(list.get(position).nickName);
                viewHolder.contact_phone_number.setText(list.get(position).phone);

           }

        return convertView;

    }

    /**
     * 根据ListView的当前位置获取分类的首字母的Char ascii值
     */
    public int getSectionForPosition(int position) {
        return list.get(position).nameFristLetter.substring(0,1).charAt(0);
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getCount(); i++) {
            char firstChar = list.get(i).nameFristLetter.substring(0,1).charAt(0);
            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public Object[] getSections() {
        return null;
    }

//    @Override
//    public Object[] getSections() {
//        return getSectionIndexer().getSections();
//    }

    private CustomFastScrollView.SectionIndexer getSectionIndexer() {
        if (sectionIndexer == null) {
            sectionIndexer = createSectionIndexer(list);
        }
        return sectionIndexer;
    }

    private CustomFastScrollView.SectionIndexer createSectionIndexer(List<ContactUniversal> contactlist) {


        return createSectionIndexer(contactlist, new Function<ContactUniversal, String>() {

            @Override
            public String apply(ContactUniversal input) {
                return input.nickName;
            }
        });
    }


    /**
     * Create a SectionIndexer given an arbitrary function mapping countries to their section name.
     * @param countries
     * @param sectionFunction
     * @return
     */
    private CustomFastScrollView.SectionIndexer createSectionIndexer(
            List<ContactUniversal> countries, Function<ContactUniversal, String> sectionFunction) {

        List<String> sections = new ArrayList<String>();
        final List<Integer> sectionsToPositions = new ArrayList<Integer>();
        final List<Integer> positionsToSections = new ArrayList<Integer>();


        // assume the countries are properly sorted
        for (int i = 0; i < countries.size(); i++) {
            ContactUniversal country = countries.get(i);
            String section = sectionFunction.apply(country);
            if (sections.isEmpty() || !sections.get(sections.size() - 1).equals(section)) {
                // add a new section
                sections.add(section);
                // map section to position
                sectionsToPositions.add(i);
            }

            // map position to section
            positionsToSections.add(sections.size() - 1);
        }

        final String[] sectionsArray = sections.toArray(new String[sections.size()]);

        return new CustomFastScrollView.SectionIndexer() {

            @Override
            public Object[] getSections() {
                return sectionsArray;
            }

            @Override
            public int getSectionForPosition(int position) {
                return positionsToSections.get(position);
            }

            @Override
            public int getPositionForSection(int section) {
                return sectionsToPositions.get(section);
            }
        };
    }

    public void refreshSections() {
        sectionIndexer = null;
        getSectionIndexer();
    }

}



