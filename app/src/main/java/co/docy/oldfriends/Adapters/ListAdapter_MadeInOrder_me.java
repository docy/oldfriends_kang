package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonMadeInOrderFromServiceRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;

/**
 * Created by khp on 2016/6/3.
 */
public class ListAdapter_MadeInOrder_me extends BaseAdapter {
    Context context;
    List<JsonMadeInOrderFromServiceRet.JsonMadeInOrder_userProfile> list;
    public interface OnClickedListener {
        public void OnClicked(int position);
    }
    public OnClickedListener onClickedListener_item;
    private static LayoutInflater inflater = null;

    public ListAdapter_MadeInOrder_me(Context context,List<JsonMadeInOrderFromServiceRet.JsonMadeInOrder_userProfile> list,OnClickedListener onClickedListener_item){
        this.context = context;
        this.list = list;
        this.onClickedListener_item = onClickedListener_item;
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.made_in_order_me_page,null);
            viewHolder.order_me_img =(ImageView) convertView.findViewById(R.id.made_in_order_me_photo);
            viewHolder.order_me_text =(TextView) convertView.findViewById(R.id.made_in_order_me_txt);
            viewHolder.order_me_layout =(LinearLayout) convertView.findViewById(R.id.made_in_order_me_layout);
            convertView.setTag(viewHolder);
        }else {
            viewHolder =(ViewHolder) convertView.getTag();
        }
        viewHolder.order_me_text.setText(list.get(position).name);
        Picasso.with(context).load( MyConfig.getApiDomain_NoSlash_GetResources() + list.get(position).icon)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(viewHolder.order_me_img);
        viewHolder.order_me_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onClickedListener_item != null){
                    onClickedListener_item.OnClicked(position);
                }
            }
        });
        return convertView;
    }
    class ViewHolder{
        ImageView order_me_img;
        TextView order_me_text;
        LinearLayout order_me_layout;
    }
}
