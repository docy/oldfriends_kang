package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonSchoolContactRet;
import co.docy.oldfriends.R;

/**
 * Created by khp on 2016/5/3.
 */
public class ListAdapterSchoolContact extends BaseAdapter {

    public interface OnClickedListener {
        public void OnClicked(int position);
    }

    public OnClickedListener clickListener;
    Context context;
    final List<JsonSchoolContactRet.JsonSchoolContactMessage> list;
    private static LayoutInflater inflater = null;

    @Override
    public JsonSchoolContactRet.JsonSchoolContactMessage getItem(int position) {
        return list.get(position);
    }

    public ListAdapterSchoolContact(Context context,
                                    List<JsonSchoolContactRet.JsonSchoolContactMessage> objects, OnClickedListener o) {
        this.context = context;
        this.list = objects;
        this.clickListener = o;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return list.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }
    private static class ViewHolder {
        TextView classname,classphoto,classpeoplenum;
        LinearLayout classlinear;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.fragment_school_contact_listadapter,null);
            viewHolder.classlinear =(LinearLayout) convertView.findViewById(R.id.fragment_school_contact_linearlayout);
            viewHolder.classname =(TextView) convertView.findViewById(R.id.fragment_school_contact_classname);
            viewHolder.classphoto =(TextView) convertView.findViewById(R.id.fragment_school_contact_classphoto);
            viewHolder.classpeoplenum =(TextView) convertView.findViewById(R.id.fragment_school_contact_classnum);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.classname.setText(list.get(position).name);
        viewHolder.classphoto.setText(list.get(position).year);
        viewHolder.classpeoplenum.setText(list.get(position).userCount+"人");
//        if(list.get(position).name.length()>5) {
//            StringBuffer buffer = new StringBuffer();
//            buffer.append(list.get(position).name.substring(8, 11)).append("\n").append(list.get(position).name.substring(12,13));
//            viewHolder.classphoto.setText(list.get(position).year);
//        }else{
//
//        }
          //  int random = (int) (Math.random() * 10);
            GradientDrawable gd = new GradientDrawable();//创建drawable
             gd.setColor(context.getResources().getColor(MyConfig.getSchoolContactPeopleResultColor(position,list)));
            gd.setCornerRadius(10);
            viewHolder.classphoto.setBackgroundDrawable(gd);



        viewHolder.classlinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickListener!=null){
                    clickListener.OnClicked(position);
                }
            }
        });


        return convertView;
    }
}