package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.text.format.Formatter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.Date;
import java.util.LinkedList;


import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonGetClassDetailRet;
import co.docy.oldfriends.R;

/**
 * Created by fighting on 2016/4/28.
 */
public class ListAdapterCoursewareDownload extends BaseAdapter {


    private Context mContext;
    private LinkedList<JsonGetClassDetailRet.ClassDetailFilesInfo> mFilesList;

    public ListAdapterCoursewareDownload(Context context, LinkedList<JsonGetClassDetailRet.ClassDetailFilesInfo> filesList) {

        mContext = context;
        mFilesList = filesList;
    }

    @Override
    public int getCount() {
        return mFilesList.size();
    }

    @Override
    public JsonGetClassDetailRet.ClassDetailFilesInfo getItem(int position) {
        return mFilesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null){
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.adapter_courseware_download,null);

            holder.ivIcon = (ImageView)convertView.findViewById(R.id.iv_courseware_download_icon);
            holder.tvName = (TextView)convertView.findViewById(R.id.iv_courseware_download_name);
            holder.tvDate = (TextView)convertView.findViewById(R.id.iv_courseware_download_date);
            holder.tvCreator = (TextView)convertView.findViewById(R.id.iv_courseware_download_creator);
            holder.tvSize = (TextView)convertView.findViewById(R.id.iv_courseware_download_size);

            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        JsonGetClassDetailRet.ClassDetailFilesInfo filesInfo = getItem(position);


        holder.tvName.setText(filesInfo.name);

        /**设置日期*/
        Date beginTime = MyConfig.UTCString2Date(filesInfo.createdAt);
        String startTime = MyConfig.Date2GroupInfoTimeToday(beginTime);
        holder.tvDate.setText(MyConfig.Date2GroupInfoTimehorizon((beginTime))+"  "+startTime);

        holder.tvCreator.setText(filesInfo.creator);
        holder.tvSize.setText(Formatter.formatFileSize(mContext,filesInfo.size));
        return convertView;
    }

    private static class ViewHolder{
        ImageView ivIcon;

        TextView tvName;
        TextView tvDate;
        TextView tvCreator;
        TextView tvSize;
    }
}
