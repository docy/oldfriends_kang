package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.DataClass.FriendCricleMsg;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.GlideTransformRounded;

/**
 * Created by khp on 2016/7/29.
 */
public class ListAdapterFriendCricleUserInfo extends BaseAdapter {
    List<FriendCricleMsg> list;
    Context context;
    private static LayoutInflater inflater = null;

    public OnClickitem onClickitem;
    public interface OnClickitem{
        void onclickitem(int postion);
    }
    public ListAdapterFriendCricleUserInfo(Context context, List<FriendCricleMsg> list, OnClickitem onClickitem){
        this.list = list;
        this.context = context;
        this.onClickitem = onClickitem;
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        FriendCricleMsg msg = list.get(position);
        ViewHolder holder;
        if(convertView==null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.cricle_friend_userinfo_adapter, null);
            holder.msg_context =(TextView) convertView.findViewById(R.id.cricle_friend_userinfo_context);
            holder.msg_time = (TextView) convertView.findViewById(R.id.cricle_friend_userinfo_time);
            holder.msg_image =(ImageView) convertView.findViewById(R.id.cricle_friend_userinfo_image);
            holder.linear =(LinearLayout) convertView.findViewById(R.id.cricle_friend_userinfo_linear);
            convertView.setTag(holder);
        }else{
            holder =(ViewHolder) convertView.getTag();
        }
        holder.msg_context.setVisibility(View.GONE);
//        holder.msg_time.setVisibility(View.GONE);
        holder.msg_image.setVisibility(View.GONE);
        if(msg.title!=null&&msg.title.length()>0){
            holder.msg_context.setVisibility(View.VISIBLE);
            holder.msg_context.setText(msg.title);
        }
        if(msg.image!=null&&msg.image.length()>0){
            holder.msg_image.setVisibility(View.VISIBLE);
            Glide.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + msg.image)
                    .centerCrop()
                    .transform(new GlideTransformRounded(context,5))
                    .into(holder.msg_image);
        }
        holder.msg_time.setText(getTime(msg.createdAt));
        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onClickitem!=null)
                onClickitem.onclickitem(position);
            }
        });
        return convertView;
    }

    public static class ViewHolder{
        TextView msg_time,msg_context;
        ImageView msg_image;
        LinearLayout linear;
    }

    public  String getTime(String str){
        Date date = Calendar.getInstance().getTime();
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        date.setTime(date.getTime() - 24 * 60 * 60 * 1000);//昨天
        String yestoday = formatter.format(date);

        String timeStr = formatter.format(new Date());
        String timeMsg = MyConfig.Date2GroupInfoTimehorizon(MyConfig.UTCString2Date(str));
        if(timeStr.equals(timeMsg)){
            return "今天";
        }else if(yestoday.equals(timeMsg)){
            return "昨天";
        } else{
            Format format = new SimpleDateFormat("M月d");
            String time_Str = format.format(MyConfig.UTCString2Date(str));
            return time_Str;
        }
    }


}
