package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.DataClass.Group;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;


public class ListAdapterGroups extends BaseAdapter {


    public interface OnClickedListener {
        public void OnClicked(int position);
    }

    public OnClickedListener clickListener;

    public interface OnImageClickedListener {
        public void OnImageClicked(int id);
    }

    public OnImageClickedListener imageClickLister;

    public interface OnLongClickedListener {
        public void OnLongClicked(int id);
    }

    public OnLongClickedListener longClickListener;


    private static class ViewHolder {
        ImageView group_icon, group_lock;
        TextView group_name, group_people_num, group_search_name, group_search_description;
        Button group_search_join;
    }

    private Context context;
    private int listType;
    LinkedList<Group> listItems;
    private static LayoutInflater inflater = null;

    public ListAdapterGroups(Context context, LinkedList<Group> objects, int listType,
                             OnImageClickedListener imageClickLister, OnClickedListener clickListener) {
        this.context = context;
        this.listItems = objects;
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listType = listType;
        this.imageClickLister = imageClickLister;
        this.clickListener = clickListener;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        Group group = listItems.get(position);

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_group_list, parent, false);
            viewHolder.group_icon = (ImageView) convertView.findViewById(R.id.group_icon);
            viewHolder.group_name = (TextView) convertView.findViewById(R.id.group_name);
            viewHolder.group_people_num = (TextView) convertView.findViewById(R.id.group_people_num);
            viewHolder.group_lock = (ImageView) convertView.findViewById(R.id.room_lock);
            viewHolder.group_search_name = (TextView) convertView.findViewById(R.id.group_search_name);
            viewHolder.group_search_join = (Button) convertView.findViewById(R.id.group_search_join);
            viewHolder.group_search_description = (TextView) convertView.findViewById(R.id.group_search_description);
            switch (listType) {
                case MyConfig.GROUP_LIST_TYPE_GROUP_SIMPLE_INFO:
                    viewHolder.group_search_name.setVisibility(View.GONE);
                    viewHolder.group_search_join.setVisibility(View.GONE);
                    viewHolder.group_search_description.setVisibility(View.GONE);

                    viewHolder.group_name.setVisibility(View.VISIBLE);
                    viewHolder.group_people_num.setVisibility(View.VISIBLE);

                    break;
                case MyConfig.GROUP_LIST_TYPE_SEARCH_GROUP:
                    viewHolder.group_search_name.setVisibility(View.VISIBLE);
                    viewHolder.group_search_join.setVisibility(View.VISIBLE);
                    viewHolder.group_search_description.setVisibility(View.VISIBLE);

                    viewHolder.group_name.setVisibility(View.GONE);
                    viewHolder.group_people_num.setVisibility(View.GONE);

                    break;
            }
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.group_icon.setImageResource(R.drawable.c_ic_launcher);
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + group.createGroupRet.logo)
                .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(viewHolder.group_icon);

        if (listType == MyConfig.GROUP_LIST_TYPE_SEARCH_GROUP) {

            TextPaint tp = viewHolder.group_name.getPaint();
            tp.setFakeBoldText(true);
            viewHolder.group_search_description.setText(group.createGroupRet.desc);
            viewHolder.group_search_name.setText(group.createGroupRet.name);
            viewHolder.group_search_join.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.OnClicked(position);
                }
            });
            //  setOnClick(viewHolder.group_search_join, position);

            viewHolder.group_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageClickLister.OnImageClicked(position);
                }
            });


        } else if (listType == MyConfig.GROUP_LIST_TYPE_GROUP_SIMPLE_INFO) {

            viewHolder.group_name.setText(group.createGroupRet.name);
            String format = context.getString(R.string.people);
            String result = String.format(format, group.userCount);
            viewHolder.group_people_num.setText(result);
        }

        if (MyConfig.SERVER_ROOM_CATEGORY_PRIVATE == group.createGroupRet.category) {
            viewHolder.group_lock.setVisibility(View.VISIBLE);
        } else {
            viewHolder.group_lock.setVisibility(View.GONE);
        }

        return convertView;

    }
/*
* 备用
* **/

//    private void setOnClick(View v, int position) {
//        v.setTag(R.id.adapter_group_list_position, position);
//        v.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                if (longClickListener != null) {
//                    longClickListener.OnLongClicked(
//                            ((Integer) (v.getTag(R.id.adapter_group_list_position))).intValue()
//                    );
//                }
//                return false;
//            }
//        });
//        v.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (clickListener != null) {
//                    clickListener.OnClicked(
//                            ((Integer) (v.getTag(R.id.adapter_group_list_position))).intValue()
//                    );
//                }
//            }
//        });
//    }
}
