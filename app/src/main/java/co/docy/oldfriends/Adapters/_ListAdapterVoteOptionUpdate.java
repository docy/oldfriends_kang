package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import co.docy.oldfriends.R;


public class _ListAdapterVoteOptionUpdate extends BaseAdapter {

    Context context;
    final List<String> listItems;
    private static LayoutInflater inflater = null;

    private static class ViewHolder {
        TextView update_vote_option_num;
        TextView update_vote_option_tv;
        ImageView update_vote_option_tb;
    }


    @Override
    public String getItem(int position) {
        return listItems.get(position);
    }

    public _ListAdapterVoteOptionUpdate(Context context,
                                        List<String> objects) {
        this.context = context;
        this.listItems = objects;
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listItems.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        String opt = (String) getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_vote_option_update, null);
            viewHolder.update_vote_option_num = (TextView) convertView.findViewById(R.id.update_vote_option_num);
            viewHolder.update_vote_option_tv = (TextView) convertView.findViewById(R.id.update_vote_option_tv);
            viewHolder.update_vote_option_tb = (ImageView) convertView.findViewById(R.id.update_vote_option_tb);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.update_vote_option_num.setText(""+position);
        viewHolder.update_vote_option_tv.setText(""+opt);


        return convertView;

    }

}
