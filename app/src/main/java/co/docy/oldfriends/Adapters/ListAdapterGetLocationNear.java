package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amap.api.services.core.PoiItem;


import java.util.ArrayList;

import co.docy.oldfriends.R;

public class ListAdapterGetLocationNear extends BaseAdapter {
	private Context context;
	private ArrayList<PoiItem> arrayList;
	int numFromActivity;
	public interface layoutClick{
		void layoutClickLister(int i);
	}
	public layoutClick layoutclick;

	public ListAdapterGetLocationNear(Context context,int num, ArrayList<PoiItem> arrayList, layoutClick layoutclick) {
		this.context = context;
		this.numFromActivity = num;
		this.arrayList = arrayList;
		this.layoutclick =layoutclick;
	}

	@Override
	public int getCount() {

		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {

		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
	 	ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = View.inflate(context, R.layout.listadapter_get_location_near, null);
			holder.item_name = (TextView) convertView
					.findViewById(R.id.listadapter_get_location_near_name);
			holder.item_distance = (TextView) convertView
					.findViewById(R.id.listadapter_get_location_near_distance);
			holder.item_phone = (TextView) convertView
					.findViewById(R.id.listadapter_get_location_near_phone);
			holder.layout =(LinearLayout) convertView.findViewById(R.id.listadapter_get_location_near_layout);
		 	holder.item_img =(ImageView) convertView.findViewById(R.id.listadapter_get_location_near_icon);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.item_img.setVisibility(View.GONE);
		holder.item_distance.setVisibility(View.GONE);
		holder.item_name.setText(String.valueOf(arrayList.get(position)));
		holder.item_distance.setText(String.valueOf(arrayList.get(position)
				.getDistance())+"米");
		holder.item_phone.setText(arrayList.get(position).getCityName()+arrayList.get(position).getAdName()+arrayList.get(position).getSnippet());
		if(numFromActivity==0){
			holder.item_distance.setVisibility(View.VISIBLE);
		}

		holder.layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				layoutclick.layoutClickLister(position);

			}
		});
		setOnclick(holder.layout,holder.item_img,position);
		return convertView;
	}
	public void setOnclick(View v,final View v1,final int position){
		v.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				layoutclick.layoutClickLister(position);
				if(numFromActivity==1) {
//					v1.setVisibility(View.VISIBLE);
				}
			}
		});
	}

	static class ViewHolder {
		TextView item_name;         //名称
		TextView item_distance;     //距离
		TextView item_phone;        //电话
		TextView item_location;//经纬度
		ImageView item_img;
		LinearLayout layout;

	}

}
