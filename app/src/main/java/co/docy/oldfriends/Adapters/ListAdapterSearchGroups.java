package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.DataClass.Group;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;


public class ListAdapterSearchGroups extends BaseAdapter {


    public interface OnClickedListener {
        public void OnClicked(int position);
    }

    public OnClickedListener clickListener;

    public interface OnImageClickedListener {
        public void OnImageClicked(int id);
    }

    public OnImageClickedListener imageClickLister;

    public interface OnLongClickedListener {
        public void OnLongClicked(int id);
    }

    public OnLongClickedListener longClickListener;


    private class ViewHolder {
        ImageView group_icon;
        TextView group_name, group_member_num, group_subject_num, group_description;
        Button group_join;
        RatingBar group_hot_starts;
        RelativeLayout search_group_layout;
    }

    private Context context;
    private int listType;
    LinkedList<Group> listItems;
    public String searchKey = "";
    private LayoutInflater inflater = null;

    public ListAdapterSearchGroups(Context context, LinkedList<Group> objects, int listType,
                                   OnImageClickedListener imageClickLister, OnClickedListener clickListener) {
        this.context = context;
        this.listItems = objects;
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listType = listType;
        this.imageClickLister = imageClickLister;
        this.clickListener = clickListener;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        Group group = listItems.get(position);

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_search_group, parent, false);
            viewHolder.group_icon = (ImageView) convertView.findViewById(R.id.group_icon);
            viewHolder.group_name = (TextView) convertView.findViewById(R.id.group_name);
//            viewHolder.group_mag_time = (TextView) convertView.findViewById(R.id.group_msg_time);
            viewHolder.group_member_num = (TextView) convertView.findViewById(R.id.group_member_num);
            viewHolder.group_subject_num = (TextView) convertView.findViewById(R.id.subject_num);
            viewHolder.group_description = (TextView) convertView.findViewById(R.id.group_description);
            viewHolder.group_hot_starts = (RatingBar) convertView.findViewById(R.id.group_hot_starts);
            viewHolder.group_join = (Button) convertView.findViewById(R.id.group_join);
            viewHolder.search_group_layout = (RelativeLayout) convertView.findViewById(R.id.search_group_layout);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.group_icon.setImageResource(R.drawable.c_ic_launcher);
        viewHolder.group_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageClickLister.OnImageClicked(position);
            }
        });
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + group.createGroupRet.logo)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(viewHolder.group_icon);


//        viewHolder.group_mag_time.setText(group.update_time);
        viewHolder.group_description.setText(group.update_msg);
        viewHolder.group_name.setText(group.createGroupRet.name);
        viewHolder.group_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.OnClicked(position);
            }
        });
        viewHolder.search_group_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageClickLister.OnImageClicked(position);
//                clickListener.OnClicked(position);
            }
        });
        if (!TextUtils.isEmpty(searchKey)){
            if(group.createGroupRet.name.contains(searchKey)) {
                MyConfig.addForeColorSpan(group.createGroupRet.name, context.getResources().getColor(R.color.Red), group.createGroupRet.name.indexOf(searchKey), group.createGroupRet.name.indexOf(searchKey) + searchKey.length(), viewHolder.group_name);
            }
        }else {
            viewHolder.group_name.setText(group.createGroupRet.name);
        }
        viewHolder.group_member_num.setText(String.format(context.getResources().getString(R.string.peope_num),group.userCount) );
        String format = context.getResources().getString(R.string.subject_num);
        String Result = String.format(format,group.topicCount);
        viewHolder.group_subject_num.setText(Result);
        viewHolder.group_hot_starts.setProgress(group.activity);
        TextPaint tp = viewHolder.group_name.getPaint();
        tp.setFakeBoldText(false);
        return convertView;

    }
/*
* 备用
* **/

//    private void setOnClick(View v, int position) {
//        v.setTag(R.id.adapter_group_list_position, position);
//        v.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                if (longClickListener != null) {
//                    longClickListener.OnLongClicked(
//                            ((Integer) (v.getTag(R.id.adapter_group_list_position))).intValue()
//                    );
//                }
//                return false;
//            }
//        });
//        v.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (clickListener != null) {
//                    clickListener.OnClicked(
//                            ((Integer) (v.getTag(R.id.adapter_group_list_position))).intValue()
//                    );
//                }
//            }
//        });
//    }
}
