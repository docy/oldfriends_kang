package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Messages.JsonGetChatRoomListRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;

/**
 * Created by fighting on 2016/8/1.
 */
public class ListAdapterChatRoom extends BaseAdapter {

    private Context mContext;
    private ArrayList<JsonGetChatRoomListRet.ChatRoomListData> listDatas;
    public String mTextFilter;


    public ListAdapterChatRoom(Context context, ArrayList<JsonGetChatRoomListRet.ChatRoomListData> data){
        mContext = context;
        listDatas = data;
    }

    @Override
    public int getCount() {
        return listDatas.size();
    }

    @Override
    public JsonGetChatRoomListRet.ChatRoomListData getItem(int position) {
        return listDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(mContext,R.layout.row_chat_room, null);
            viewHolder.chat_room_icon = (ImageView) convertView.findViewById(R.id.chat_room_icon);
            viewHolder.chat_room_name = (TextView) convertView.findViewById(R.id.chat_room_name);
            viewHolder.chat_room_msg_time = (TextView) convertView.findViewById(R.id.chat_room_msg_time);
            viewHolder.chat_room_online_num = (TextView) convertView.findViewById(R.id.chat_room_online_num);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        JsonGetChatRoomListRet.ChatRoomListData chatRoomData = getItem(position);
        Picasso.with(mContext).load(MyConfig.getApiDomain_NoSlash_GetResources()+chatRoomData.logo)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(viewHolder.chat_room_icon);

        if (TextUtils.isEmpty(mTextFilter)){
            viewHolder.chat_room_name.setText(chatRoomData.name);
        }else {
            if(chatRoomData.name.contains(mTextFilter)) {
                MyConfig.addForeColorSpan(chatRoomData.name, mContext.getResources().getColor(R.color.Red), chatRoomData.name.indexOf(mTextFilter), chatRoomData.name.indexOf(mTextFilter) + mTextFilter.length(), viewHolder.chat_room_name);
            }
        }
        viewHolder.chat_room_online_num.setText("在线成员:"+chatRoomData.userCount);

        return convertView;
    }

    private static class ViewHolder {
        ImageView chat_room_icon;
        TextView chat_room_name,chat_room_msg_time,chat_room_online_num;
    }
}
