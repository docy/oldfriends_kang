package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Activitis.ActivityLocationView;
import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.MapLocation;
import co.docy.oldfriends.DataClass.IMsg;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.LinearLayoutCaptureLongClick;
import co.docy.oldfriends.Views.PicassoTransformationRounded;
import co.docy.oldfriends.Views.ViewActivityInfoMainroom;
import co.docy.oldfriends.Views.ViewActivityInfoSubroom;
import co.docy.oldfriends.Views.ViewVoteInfoMainroom;
import co.docy.oldfriends.Views.ViewVoteResultInfo;
import co.docy.oldfriends.Views.ViewVoteResultOption;
import co.docy.oldfriends.Views.ViewVoteUpdateOption;
import co.docy.oldfriends.emoji.EmojiUtils;


public class ListAdapterMsg extends BaseAdapter {

    /**报名子话题在子聊天室中显示的view对象，*/
    public ViewActivityInfoSubroom viewActivityInfoSubroom;

    public interface OnCaptureLongClickedListener {
        boolean onLongClicked(int position, IMsg iMsg, MotionEvent event);
    }
    public OnCaptureLongClickedListener captureLongClickedListener;


    public interface OnLongClickedListener {
        public void onLongClicked(IMsg iMsg);
    }
    public OnLongClickedListener longClickedListener;

    public interface OnMapClickedListener {
        public void onMapClicked(IMsg iMsg);
    }
    public OnMapClickedListener mapClickedListener;

    public interface OnFileClickedListener {
        public void onFileClicked(Context context, IMsg imsNew);
    }
    public OnFileClickedListener fileClickedListener;

    public interface OnImageClickedListener {
        public void onImageClicked(IMsg imsg);
    }
    public OnImageClickedListener imageClickedListener;

    public interface OnSoundClickedListener {
        public void onSoundClicked(IMsg imsg);
    }

    public OnSoundClickedListener soundClickedListener;

    public interface OnLinkClickedListener {
        public void onLinkClicked(IMsg imsNew);
    }
    public OnLinkClickedListener linkClickedListener;

    public interface OnDownloadClickedListener {
        public void onDownloadClicked(IMsg imsNew);
    }
    public OnDownloadClickedListener downloadClickedListener;

    public interface OnForwardClickedListener {
        public void onForwardClicked(IMsg imsNew);
    }
    public OnForwardClickedListener forwardClickedListener;

    public interface OnShareClickedListener {
        public void onShareClicked(IMsg imsNew);
    }
    public OnShareClickedListener shareClickedListener;

    public interface OnFavoriteClickedListener {
        public void onFavoriteClicked(int topicId);
    }
    public OnFavoriteClickedListener favoriteClickedListener;

    public interface OnVoteSubmitClickedListener {
        public void onVoteSubmitClicked(int resourceId, LinkedList<Integer> options);
    }
    public OnVoteSubmitClickedListener voteSubmitClickedListener;

    public interface OnVotePersonListClickedListener {
        public void onVotePersonListClicked(IMsg iMsg);
    }

    public OnVotePersonListClickedListener votePersonListClickedListener;

    public interface OnVoteFinishClickedListener {
        public void onVoteFinishClicked(IMsg iMsg);
    }

    public OnVoteFinishClickedListener voteFinishClickedListener;

    public interface OnVoteOptionClickedListener {
        public void onVoteOptionClicked(IMsg iMsg, int num, boolean checked);
    }

    MyConfig.IOnOkClickListener onOkClickListener;

    AnimationDrawable animationDrawable;

    public OnVoteOptionClickedListener voteOptionClickedListener = new OnVoteOptionClickedListener() {
        @Override
        public void onVoteOptionClicked(IMsg iMsg, int num, boolean checked) {
            if (iMsg.vote.single) {   //单选
                if (checked) {    //有一个被选择了，取消其他选择

                    iMsg.vote.checked.clear();
                    iMsg.vote.checked.add(num);

                } else {  //有一个被取消了

                }

                for (ViewVoteUpdateOption viewVoteUpdateOption : viewVoteUpdateOptionsList) {
                        viewVoteUpdateOption.showChecked();
                }

            } else {  //多选
                if (checked) {    //有一个被选择了

                } else {  //有一个被取消选择了

                }
            }
        }
    };
    LinkedList<ViewVoteUpdateOption> viewVoteUpdateOptionsList;

    /**提交报名的接口*/
    public interface OnActivitySubmitClickedListener {
        public void onActivitySubmitClicked(int resourceId,int num);
    }
    public OnActivitySubmitClickedListener activitySubmitClickedListener;

    /**取消报名的接口*/
    public interface OnActivityLeaveClickedListener {
        public void onActivityLeaveClicked(int resourceId);
    }
    public OnActivityLeaveClickedListener activityLeaveClickedListener;

 /**取消报名的接口*/
    public interface OnActivityCloseClickedListener {
        public void onActivityCloseClicked(int resourceId);
    }
    public OnActivityCloseClickedListener activityCloseClickedListener;

 /**报名时选择人数*/
    public interface OnActivityInputPeopleNumClickedListener {
        public void onActivityInputPeopleNumClicked();
    }
    public OnActivityInputPeopleNumClickedListener activityInputPeopleNumClickedListener;


    private static class ViewHolder {

        LinearLayoutCaptureLongClick msg_row;

        TextView msg_info;
        LinearLayout msg_date;
        TextView msg_date_text;
        LinearLayout msg_main;
        ImageView msg_avatar;
        TextView msg_user;
        TextView msg_time;
        LinearLayout msg_main_list_detail;

        TextView msg_title_reply;
        TextView msg_title;
        TextView msg_url;
        TextView msg_url_today_top;
        TextView msg_body;

        ImageView msg_arrows;
        ImageView msg_attached_image;
        ImageView msg_attached_image_video_play;
        ImageView msg_activity_result;
        TextView msg_attached_file_info;
        LinearLayout msg_attached_vote_mainroom;
        LinearLayout msg_extend_vote_subroom;
        Button msg_extend_vote_send;
        //        TextView msg_text_vote_title;
//        ListView msg_vote_opt_list;
        LinearLayout msg_main_list;

        LinearLayout msg_extend_share;
        ImageButton msg_share;
        ImageButton msg_collect;
        ImageButton msg_delete;
        TextView msg_extend_comment_divider;
        RelativeLayout msg_attached_sound_layout, msg_attached_sound;
        ImageView play_sound_anim, play_sound_unread, msg_attached_sound_pop_shape;
        TextView play_sound_time;

        //for share in, for compatibility with ios
        LinearLayout msg_mainroom_share_in;
        ImageView msg_mainroom_share_in_avatar;
        TextView msg_mainroom_share_in_title;
        TextView msg_mainroom_share_in_link,msg_space;

        AVLoadingIndicatorView avloadingIndicatorView;
    }


    Context context;
    int room_level;
    final List<IMsg> listItems;
    private static LayoutInflater inflater = null;


    @Override
    public IMsg getItem(int position) {
        return listItems.get(position);
    }

    public ListAdapterMsg(Context context, int room_level,
                          List<IMsg> objects, OnLinkClickedListener onLinkClickedListener, OnImageClickedListener onImageClickedListener) {
        this.context = context;
        this.room_level = room_level;
        this.listItems = objects;
        this.linkClickedListener = onLinkClickedListener;
        this.imageClickedListener = onImageClickedListener;
        // MyLog.d(MyConfig.LIFECYCLE,"MyArrayAdapter.MyArrayAdapter()");
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listItems.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        final IMsg imsg = getItem(position);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(linkClickedListener != null) {
                    linkClickedListener.onLinkClicked(imsg);//goto a sub room of this subject
                }
            }
        };


        final ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_msg, null);


            viewHolder.msg_row = (LinearLayoutCaptureLongClick)convertView.findViewById(R.id.msg_row);

            viewHolder.msg_info = (TextView) convertView.findViewById(R.id.msg_info);
            viewHolder.msg_date = (LinearLayout) convertView.findViewById(R.id.msg_date);
            viewHolder.msg_date_text = (TextView) convertView.findViewById(R.id.msg_date_text);

            viewHolder.msg_main = (LinearLayout) convertView.findViewById(R.id.msg_main);
            viewHolder.msg_avatar = (ImageView) convertView.findViewById(R.id.msg_avatar);

            viewHolder.msg_main_list = (LinearLayout) convertView.findViewById(R.id.msg_main_list);
            viewHolder.msg_user = (TextView) convertView.findViewById(R.id.msg_user);
            viewHolder.msg_time = (TextView) convertView.findViewById(R.id.msg_time);
            viewHolder.msg_activity_result = (ImageView) convertView.findViewById(R.id.msg_activity_result);

            viewHolder.msg_main_list_detail = (LinearLayout) convertView.findViewById(R.id.msg_main_list_detail);
            viewHolder.msg_title_reply = (TextView) convertView.findViewById(R.id.msg_title_reply);
            viewHolder.msg_title = (TextView) convertView.findViewById(R.id.msg_title);
            viewHolder.msg_url = (TextView) convertView.findViewById(R.id.msg_url);
            viewHolder.msg_url_today_top = (TextView) convertView.findViewById(R.id.msg_url_today_top);
            viewHolder.msg_body = (TextView) convertView.findViewById(R.id.msg_body);
            viewHolder.msg_arrows = (ImageView) convertView.findViewById(R.id.msg_arrows);

            viewHolder.msg_attached_sound = (RelativeLayout) convertView.findViewById(R.id.msg_attached_sound);
            viewHolder.msg_attached_sound_layout = (RelativeLayout) convertView.findViewById(R.id.msg_attached_sound_layout);
            viewHolder.play_sound_anim = (ImageView) convertView.findViewById(R.id.play_sound_anim);
            viewHolder.play_sound_unread = (ImageView) convertView.findViewById(R.id.play_sound_unread);
            viewHolder.play_sound_time = (TextView) convertView.findViewById(R.id.play_sound_time);
            viewHolder.avloadingIndicatorView = (AVLoadingIndicatorView) convertView.findViewById(R.id.wait_to_play_avloadingIndicatorView);
            viewHolder.msg_attached_sound_pop_shape = (ImageView) convertView.findViewById(R.id.msg_attached_sound_pop_shape);

            viewHolder.msg_attached_image = (ImageView) convertView.findViewById(R.id.msg_attached_image);
            viewHolder.msg_attached_image_video_play = (ImageView) convertView.findViewById(R.id.msg_attached_image_video_play);
            viewHolder.msg_attached_file_info = (TextView) convertView.findViewById(R.id.msg_attached_file_info);
            viewHolder.msg_attached_vote_mainroom = (LinearLayout) convertView.findViewById(R.id.msg_attached_vote_mainroom);
            viewHolder.msg_extend_vote_subroom = (LinearLayout) convertView.findViewById(R.id.msg_extend_vote_subroom);
            viewHolder.msg_extend_vote_send = (Button) convertView.findViewById(R.id.msg_extend_vote_send);

            viewHolder.msg_extend_share = (LinearLayout) convertView.findViewById(R.id.msg_extend_share);
            viewHolder.msg_share = (ImageButton) convertView.findViewById(R.id.msg_share);
            viewHolder.msg_collect = (ImageButton) convertView.findViewById(R.id.msg_collect);
            viewHolder.msg_delete = (ImageButton) convertView.findViewById(R.id.msg_delete);
            viewHolder.msg_extend_comment_divider = (TextView) convertView.findViewById(R.id.msg_extend_comment_divider);


            viewHolder.msg_mainroom_share_in = (LinearLayout)convertView.findViewById(R.id.msg_mainroom_share_in);
            viewHolder.msg_mainroom_share_in_avatar = (ImageView)convertView.findViewById(R.id.msg_mainroom_share_in_avatar);
            viewHolder.msg_mainroom_share_in_title = (TextView)convertView.findViewById(R.id.msg_mainroom_share_in_title);
            viewHolder.msg_mainroom_share_in_link = (TextView)convertView.findViewById(R.id.msg_mainroom_share_in_link);

            viewHolder.msg_space =(TextView) convertView.findViewById(R.id.msg_space);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.msg_row.mTouchListener = new LinearLayoutCaptureLongClick.OnOverLayerTouchListener() {
            @Override
            public boolean onOverLayerTouch(MotionEvent event) {

                if(captureLongClickedListener != null){
                    return captureLongClickedListener.onLongClicked(position, imsg, event);
                }
                return false;
            }
        };

        viewHolder.msg_space.setVisibility(View.VISIBLE);
        // ------------- date divider ------------------
        viewHolder.msg_date.setVisibility(View.GONE);
        if (imsg.first_in_a_day ) { //仅仅在主聊天室显示时间分割线
            /**现在更改为在主聊天室和私人聊天室中都显示时间分割线*/
            if(this.room_level == MyConfig.LEVEL_ROOM_MAIN || this.room_level == MyConfig.LEVEL_ROOM_PRIVATE){
                viewHolder.msg_date.setVisibility(View.VISIBLE);
                viewHolder.msg_date_text.setText(imsg.time_date_show);
            }
        }

        // ------------- debug info ---------------------- 这个功能负面影响很大，暂时屏蔽
        if (MyConfig.debug_in_msg) {
            viewHolder.msg_info.setVisibility(View.VISIBLE);

            //debug
            StringBuilder sb = new StringBuilder();
//            sb.append("clientId " + imsg.clientId);
//            sb.append(", server_tag_id " + imsg.server_tag_id);
//            if(imsg.filePath_inServer != null){
//                sb.append(", fileType " + MyConfig.guessMimeByUrl(MyConfig.getHttpURLConnectionRedirected(MyConfig.getApiDomain_NoSlash_GetResources() + imsg.filePath_inServer).url_redirected));
//            }
            sb.append("" + imsg.type + " " + imsg.subType +" "+imsg.filePath_inLocal);
            viewHolder.msg_info.setText(sb.toString());
        } else {
            viewHolder.msg_info.setVisibility(View.GONE);
        }

        // ------------- head avatar ----------------------
//        String url = MyConfig.getHttpURLConnectionRedirected(MyConfig.getApiDomain_NoSlash_GetResources() + imsg.avatar).url_redirected;
        String url = MyConfig.getApiDomain_NoSlash_GetResources() + imsg.avatar;
        MyLog.d("", "url redirect header : " + url.toString());

        viewHolder.msg_avatar.setVisibility(View.VISIBLE);
//        viewHolder.msg_avatar.getLayoutParams().height = context.getResources().getDimensionPixelSize(R.dimen.avatar_height);
//        viewHolder.msg_avatar.getLayoutParams().height = MyConfig.getPxFromDimen(context, R.dimen.avatar_height);

        if(
                (imsg.type == MyConfig.TYPE_CREATE)
                || (imsg.type == MyConfig.TYPE_LEAVE)
                || (imsg.type == MyConfig.TYPE_JOIN)
                ){

            Picasso.with(context).load(R.drawable.horn_3x)
//                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .fit()
                    .centerCrop()
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(viewHolder.msg_avatar);


            viewHolder.msg_avatar.setOnClickListener(null);
        }else {
            Picasso.with(context).load(url)
//                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .fit()
                    .centerCrop()
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(viewHolder.msg_avatar);
            viewHolder.msg_avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                gotoUserInfo(imsg.user_id);
                    MyConfig.gotoUserInfo(context, imsg.user_id);
                }
            });

            if (this.room_level == MyConfig.LEVEL_ROOM_PRIVATE) {/**私聊天室头像禁止点击*/
                viewHolder.msg_avatar.setOnClickListener(null);
            }
        }

        // ------------- user name ---------------
        viewHolder.msg_user.setVisibility(View.VISIBLE);
        viewHolder.msg_user.setText(imsg.user_name);
        TextPaint tp_name = viewHolder.msg_user.getPaint();
        tp_name.setFakeBoldText(true);

        // ------------- time_ampm -------------
        if (this.room_level == MyConfig.LEVEL_ROOM_MAIN || this.room_level == MyConfig.LEVEL_ROOM_PRIVATE) {
            viewHolder.msg_time.setText(imsg.time_ampm);
        } else if (this.room_level == MyConfig.LEVEL_ROOM_SUB) {
            viewHolder.msg_time.setText(imsg.time_subroom);
        }

        /**
         *  ------------- local send -------------
         * 如果是先发送到本地的
         */
        if (imsg.msg_from == MyConfig.MSG_FROM_LOCAL && imsg.body != null) {
			viewHolder.msg_activity_result.setVisibility(View.GONE);
            viewHolder.msg_title_reply.setVisibility(View.GONE);
            viewHolder.msg_title.setVisibility(View.GONE);
            viewHolder.msg_url.setVisibility(View.GONE);
            viewHolder.msg_arrows.setVisibility(View.GONE);
//            if (imsg.category == MyConfig.MSG_CATEGORY_SUBJECT) {
//                viewHolder.msg_title.setVisibility(View.VISIBLE);
//                viewHolder.msg_title.setText(imsg.title);
//            } else if (imsg.category == MyConfig.MSG_CATEGORY_NORMAL) {
//            } else if (imsg.category == MyConfig.MSG_CATEGORY_SUBJECT_COMMENT) {
//                viewHolder.msg_title_reply.setVisibility(View.GONE);
////                viewHolder.msg_title_reply.setText(context.getResources().getString(R.string.msg_re) + imsg.creatorName + context.getResources().getString(R.string.msg_re_2));
//                viewHolder.msg_title.setVisibility(View.VISIBLE);
//                viewHolder.msg_title.setText(imsg.title);
//            }

            viewHolder.msg_body.setVisibility(View.VISIBLE);



            viewHolder.msg_body.setText(EmojiUtils.showEmoji(context, imsg.body));
            viewHolder.msg_body.setTextColor(context.getResources().getColor(R.color.xjt_app_text_inactive));
//            viewHolder.msg_body.setBackgroundColor(context.getResources().getColor(R.color.Red));
//            viewHolder.msg_body.setText(context.getString(R.string.sending));
            viewHolder.msg_extend_share.setVisibility(View.GONE);
            // clear attached view first
            viewHolder.msg_attached_sound.setVisibility(View.GONE);
            viewHolder.msg_attached_image.setVisibility(View.GONE);
            viewHolder.msg_attached_image_video_play.setVisibility(View.GONE);
            viewHolder.msg_mainroom_share_in.setVisibility(View.GONE);
            viewHolder.msg_attached_file_info.setVisibility(View.GONE);
            viewHolder.msg_attached_vote_mainroom.setVisibility(View.GONE);
            viewHolder.msg_extend_vote_subroom.setVisibility(View.GONE);
            viewHolder.msg_extend_vote_send.setVisibility(View.GONE);//这个在msg_vote区域之外，所以单独设置一下
            viewHolder.msg_url_today_top.setVisibility(View.GONE);//这个在msg_vote区域之外，所以单独设置一下

            //clear click
//            viewHolder.msg_main_list.setClickable(false);
//            viewHolder.msg_title_reply.setClickable(false);
//            viewHolder.msg_title.setClickable(false);
            viewHolder.msg_body.setClickable(false);


            return convertView;
        }

        /**
         * -------------- for share in, have to be compatible with ios --------------------
         */

        if (this.room_level == MyConfig.LEVEL_ROOM_MAIN && MyConfig.isShareInNews(imsg)) {


            viewHolder.msg_title_reply.setVisibility(View.GONE);
            viewHolder.msg_title.setVisibility(View.GONE);
            viewHolder.msg_url.setVisibility(View.GONE);
            viewHolder.msg_arrows.setVisibility(View.GONE);
            viewHolder.msg_body.setVisibility(View.GONE);
            viewHolder.msg_extend_share.setVisibility(View.GONE);
            // clear attached view first
            viewHolder.msg_attached_sound.setVisibility(View.GONE);
            viewHolder.msg_attached_image.setVisibility(View.GONE);
            viewHolder.msg_attached_image_video_play.setVisibility(View.GONE);
            viewHolder.msg_mainroom_share_in.setVisibility(View.VISIBLE);
            viewHolder.msg_attached_file_info.setVisibility(View.GONE);
            viewHolder.msg_attached_vote_mainroom.setVisibility(View.GONE);
            viewHolder.msg_extend_vote_subroom.setVisibility(View.GONE);
            viewHolder.msg_extend_vote_send.setVisibility(View.GONE);//这个在msg_vote区域之外，所以单独设置一下

            MyLog.d("", "share in: thumbNail " + imsg.thumbNail + " fullSize " + imsg.fullSize);
            if(TextUtils.isEmpty(imsg.thumbNail)){
                Picasso.with(context).load(R.drawable.today_top1_default_icon_3x)
                        .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                        .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                        .into(viewHolder.msg_mainroom_share_in_avatar);

            }else{
                Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + imsg.thumbNail)
                        .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                        .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                        .into(viewHolder.msg_mainroom_share_in_avatar);
            }

//            Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + imsg.thumbNail).into(viewHolder.msg_mainroom_share_in_avatar);

            MyConfig.seperateShareInBodyLink(context, imsg.title, viewHolder.msg_mainroom_share_in_title, viewHolder.msg_mainroom_share_in_link);
            viewHolder.msg_mainroom_share_in.setOnClickListener(onClickListener);
            return convertView;

        }

        /**
         *  ------------- title & body -------------
         */
        //clear all first
        viewHolder.msg_title_reply.setVisibility(View.GONE);
        viewHolder.msg_title.setVisibility(View.GONE);
        viewHolder.msg_activity_result.setVisibility(View.GONE);
        viewHolder.msg_url.setVisibility(View.GONE);
        viewHolder.msg_body.setVisibility(View.GONE);
        viewHolder.msg_arrows.setVisibility(View.GONE);
        viewHolder.msg_main_list.setClickable(false);   //据说在setOnClickListener中会自动打开这个开关
        viewHolder.msg_url_today_top.setVisibility(View.GONE);
        //always show body
        showIfHaveMsgBody(imsg, viewHolder);

        if (imsg.category == MyConfig.MSG_CATEGORY_SUBJECT) {

            if (this.room_level == MyConfig.LEVEL_ROOM_MAIN) {

                viewHolder.msg_title.setText(context.getResources().getString(R.string.msg_title) + imsg.title + context.getResources().getString(R.string.msg_title));
                viewHolder.msg_title.setTextColor(context.getResources().getColor(R.color.xjt_subject));
                TextPaint tp = viewHolder.msg_title.getPaint();//设置中文粗体
                tp.setFakeBoldText(true);
                viewHolder.msg_title.setVisibility(View.VISIBLE);

                //如果vote系列
                if (imsg.subType == MyConfig.SUBTYPE_VOTE) {

                } else if (imsg.subType == MyConfig.SUBTYPE_VOTE_CLOSE) {
                    //分离vote close信息做特殊处理
                    viewHolder.msg_title.setText(context.getResources().getString(R.string.msg_title) +
                                    imsg.originDataFromServer.getMsgTopicRet.sender +
                                    context.getString(R.string.have_closed_vote) +
                                    imsg.title +
                                    context.getResources().getString(R.string.msg_title));


                }else if (imsg.subType == MyConfig.SUBTYPE_ACTIVITY_CLOSE) {
                    //分离vote close信息做特殊处理
                    viewHolder.msg_title.setText(context.getResources().getString(R.string.msg_title) +
                                    imsg.originDataFromServer.getMsgTopicRet.sender +
                                    context.getString(R.string.have_closed_activity) +
                                    imsg.title +
                                    context.getResources().getString(R.string.msg_title));


                } else if (imsg.subType == MyConfig.SUBTYPE_LINK) {
                    viewHolder.msg_url.setText(imsg.link);
                    viewHolder.msg_url.setVisibility(View.VISIBLE);

//                    showIfHaveMsgBody(imsg, viewHolder);
                }else {//vote以外的其他
//                    showIfHaveMsgBody(imsg, viewHolder);
                }

                viewHolder.msg_arrows.setVisibility(View.VISIBLE);
                viewHolder.msg_arrows.setOnClickListener(onClickListener);

                viewHolder.msg_main_list.setOnClickListener(onClickListener);
//                viewHolder.msg_title_reply.setOnClickListener(onClickListener);
//                viewHolder.msg_title.setOnClickListener(onClickListener);
//                viewHolder.msg_body.setOnClickListener(onClickListener);


            } else if (this.room_level == MyConfig.LEVEL_ROOM_SUB) {

                viewHolder.msg_title.setText(imsg.title);
                viewHolder.msg_title.setTextColor(context.getResources().getColor(R.color.xjt_name_desc));
                viewHolder.msg_title.setVisibility(View.VISIBLE);

                if(imsg.subType == MyConfig.SUBTYPE_LINK){
                    viewHolder.msg_url.setText(imsg.link);
                    viewHolder.msg_url.setVisibility(View.VISIBLE);

//                    showIfHaveMsgBody(imsg, viewHolder);
                }

//                showIfHaveMsgBody(imsg, viewHolder);
               if("news".equals(imsg.news)){
                //if(imsg.title.startsWith("今日头条")){
                    MyLog.d("kwwl","属于今日头条"+imsg.news);
                    viewHolder.msg_url_today_top.setVisibility(View.VISIBLE);
                    MyConfig.seperateShareInBodyLink(context, imsg.title, viewHolder.msg_title, viewHolder.msg_url_today_top);
                }else{
                   viewHolder.msg_url_today_top.setVisibility(View.GONE);
               }
            }

        } else if (imsg.category == MyConfig.MSG_CATEGORY_EVENT) {

//            showIfHaveMsgBody(imsg, viewHolder);

        } else if (imsg.category == MyConfig.MSG_CATEGORY_NORMAL) {

//            showIfHaveMsgBody(imsg, viewHolder);

        } else if (imsg.category == MyConfig.MSG_CATEGORY_SUBJECT_COMMENT) {

            if (this.room_level == MyConfig.LEVEL_ROOM_MAIN) {
                viewHolder.msg_title_reply.setText(context.getResources().getString(R.string.msg_re) + imsg.creatorName + context.getResources().getString(R.string.msg_re_2));
                viewHolder.msg_title_reply.setVisibility(View.VISIBLE);
                if("news".equals(imsg.news)){
                    int indexOf = imsg.title.indexOf("http");
                    if(indexOf != -1){
                        imsg.title = imsg.title.substring(0,indexOf);
                        if(imsg.title.length() > 30){
                            imsg.title = imsg.title.substring(0,30)+"...";
                        }
                    }

                }

                String s;
                if(imsg.title.length() > 30){
                    s = imsg.title.substring(0, 30)+"...";
                }else{
                    s = imsg.title;
                }
                viewHolder.msg_title.setText(context.getResources().getString(R.string.msg_title) + s + context.getResources().getString(R.string.msg_title));

//                viewHolder.msg_title.setText(context.getResources().getString(R.string.msg_title) + imsg.title + context.getResources().getString(R.string.msg_title));
                viewHolder.msg_title.setTextColor(context.getResources().getColor(R.color.xjt_subject));
                TextPaint tp = viewHolder.msg_title.getPaint();
                tp.setFakeBoldText(true);
                viewHolder.msg_title.setVisibility(View.VISIBLE);

//                showIfHaveMsgBody(imsg, viewHolder);

                viewHolder.msg_arrows.setVisibility(View.VISIBLE);
                viewHolder.msg_arrows.setOnClickListener(onClickListener);

                viewHolder.msg_main_list.setOnClickListener(onClickListener);
//                viewHolder.msg_title_reply.setOnClickListener(onClickListener);
//                viewHolder.msg_title.setOnClickListener(onClickListener);
//                viewHolder.msg_body.setOnClickListener(onClickListener);

            } else if (this.room_level == MyConfig.LEVEL_ROOM_SUB) {

//                showIfHaveMsgBody(imsg, viewHolder);

            }
        } else if (imsg.category == MyConfig.MSG_CATEGORY_PRIVATE) {


        } else {

        }

        if (longClickedListener != null) {

            viewHolder.msg_main_list.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    longClickedListener.onLongClicked(imsg);
                    return false;
                }
            });

        }


        // -------------  photo & file & location & sound -------------

        // clear attached view first
        viewHolder.msg_attached_sound.setVisibility(View.GONE);
        viewHolder.msg_attached_image.setVisibility(View.GONE);
        viewHolder.msg_attached_image_video_play.setVisibility(View.GONE);
        viewHolder.msg_mainroom_share_in.setVisibility(View.GONE);
        viewHolder.msg_attached_file_info.setVisibility(View.GONE);
        viewHolder.msg_attached_vote_mainroom.setVisibility(View.GONE);
        viewHolder.msg_extend_vote_subroom.setVisibility(View.GONE);
        viewHolder.msg_extend_vote_send.setVisibility(View.GONE);//这个在msg_vote区域之外，所以单独设置一下
        //  viewHolder.msg_extend_vote_send.getLayoutParams().width = getVoteWidth();

        if (imsg.type == MyConfig.TYPE_SOUND) {//这里是普通消息类

           // viewHolder.msg_attached_sound_layout.getLayoutParams().width = (int) context.getResources().getDimension(R.dimen.talk_pop_min_width) + (imsg.duration * 800) / 60;
           int width = (int) context.getResources().getDimension(R.dimen.talk_pop_min_width) + MyConfig.dp2Px(context,(imsg.duration * 180) / 60) ;
            viewHolder.msg_attached_sound_layout.getLayoutParams().width = width;
            /**在测试中发现存在61秒的可能，所以做这个判断*/
            imsg.duration = imsg.duration > 60 ? 60:imsg.duration;
            viewHolder.play_sound_time.setText("" + imsg.duration + "“");
            viewHolder.play_sound_unread.setVisibility(imsg.readed ? View.INVISIBLE : View.VISIBLE);
            if (imsg.user_id == MyConfig.usr_id) {
                viewHolder.msg_attached_sound_layout.setBackgroundResource(R.drawable.talk_pop_own_shape_3x);
                viewHolder.msg_attached_sound_pop_shape.setImageResource(R.drawable.talk_pop_own_3x);
            } else {
                viewHolder.msg_attached_sound_layout.setBackgroundResource(R.drawable.talk_pop_other_shape_3x);
                viewHolder.msg_attached_sound_pop_shape.setImageResource(R.drawable.talk_pop_other_3x);
            }

            switch (imsg.audioPlaying) {
                case MyConfig.MEDIAPLAYER_NOT_INITIAL:
                    viewHolder.play_sound_anim.setBackgroundResource(R.drawable.play_void_high_3x);
                    viewHolder.avloadingIndicatorView.setVisibility(View.GONE);
                    viewHolder.play_sound_anim.setVisibility(View.VISIBLE);
                    break;
                case MyConfig.MEDIAPLAYER_IS_PREPARING:
                    viewHolder.avloadingIndicatorView.setVisibility(View.VISIBLE);
                    viewHolder.play_sound_anim.setVisibility(View.GONE);
                    break;
                case MyConfig.MEDIAPLAYER_IS_PLAYING:
                    viewHolder.avloadingIndicatorView.setVisibility(View.GONE);
                    viewHolder.play_sound_anim.setVisibility(View.VISIBLE);
                    viewHolder.play_sound_anim.setBackgroundResource(R.drawable.play_void_anim);
                    animationDrawable = (AnimationDrawable) viewHolder.play_sound_anim.getBackground();
                    animationDrawable.start();
                    break;
                case MyConfig.MEDIAPLAYER_IS_READY:
                    viewHolder.play_sound_anim.setBackgroundResource(R.drawable.play_void_high_3x);
                    viewHolder.avloadingIndicatorView.setVisibility(View.GONE);
                    viewHolder.play_sound_anim.setVisibility(View.VISIBLE);
                    break;
                default:
                    viewHolder.play_sound_anim.setBackgroundResource(R.drawable.play_void_high_3x);
            }

            viewHolder.msg_attached_sound_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (soundClickedListener != null) {
                        soundClickedListener.onSoundClicked(imsg);//show father gallery
                    }
                }
            });

            viewHolder.msg_attached_sound.setVisibility(View.VISIBLE);

        } else if (imsg.type == MyConfig.TYPE_TOPIC_COMMANT) {//这里是评论类

            if (imsg.subType == MyConfig.SUBTYPE_IMAGE) {

                Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + imsg.thumbNail).into(viewHolder.msg_attached_image);

                viewHolder.msg_attached_image.setVisibility(View.VISIBLE);
                viewHolder.msg_attached_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (imageClickedListener != null) {
                            imageClickedListener.onImageClicked(imsg);//show father gallery
                        }
                    }
                });


            }else if (imsg.subType == MyConfig.SUBTYPE_SOUND_COMMENT){
                viewHolder.msg_attached_sound_layout.getLayoutParams().width = (int) context.getResources().getDimension(R.dimen.talk_pop_min_width) + (imsg.originDataFromServer.getMsgTopicCommentRet.info.duration* 800) / 60;
                viewHolder.play_sound_time.setText("" + imsg.originDataFromServer.getMsgTopicCommentRet.info.duration + "“");
                viewHolder.play_sound_unread.setVisibility(imsg.readed?View.INVISIBLE:View.VISIBLE);
                if (imsg.user_id == MyConfig.usr_id) {
                    viewHolder.msg_attached_sound_layout.setBackgroundResource(R.drawable.talk_pop_own_shape_3x);
                    viewHolder.msg_attached_sound_pop_shape.setImageResource(R.drawable.talk_pop_own_3x);
                } else {
                    viewHolder.msg_attached_sound_layout.setBackgroundResource(R.drawable.talk_pop_other_shape_3x);
                    viewHolder.msg_attached_sound_pop_shape.setImageResource(R.drawable.talk_pop_other_3x);
                }

                switch (imsg.audioPlaying) {
                    case MyConfig.MEDIAPLAYER_NOT_INITIAL:
                        viewHolder.play_sound_anim.setBackgroundResource(R.drawable.play_void_high_3x);
                        viewHolder.avloadingIndicatorView.setVisibility(View.GONE);
                        viewHolder.play_sound_anim.setVisibility(View.VISIBLE);
                        break;
                    case MyConfig.MEDIAPLAYER_IS_PREPARING:
                        viewHolder.avloadingIndicatorView.setVisibility(View.VISIBLE);
                        viewHolder.play_sound_anim.setVisibility(View.GONE);
                        break;
                    case MyConfig.MEDIAPLAYER_IS_PLAYING:
                        viewHolder.avloadingIndicatorView.setVisibility(View.GONE);
                        viewHolder.play_sound_anim.setVisibility(View.VISIBLE);
                        viewHolder.play_sound_anim.setBackgroundResource(R.drawable.play_void_anim);
                        animationDrawable = (AnimationDrawable) viewHolder.play_sound_anim.getBackground();
                        animationDrawable.start();
                        break;
                    case MyConfig.MEDIAPLAYER_IS_READY:
                        viewHolder.play_sound_anim.setBackgroundResource(R.drawable.play_void_high_3x);
                        viewHolder.avloadingIndicatorView.setVisibility(View.GONE);
                        viewHolder.play_sound_anim.setVisibility(View.VISIBLE);
                        break;
                    default:
                        viewHolder.play_sound_anim.setBackgroundResource(R.drawable.play_void_high_3x);
                }

                viewHolder.msg_attached_sound_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (soundClickedListener != null) {
                            soundClickedListener.onSoundClicked(imsg);//show father gallery
                        }
                    }
                });

                viewHolder.msg_attached_sound.setVisibility(View.VISIBLE);

            }

        } else if (imsg.type == MyConfig.TYPE_TOPIC) {//这里是主题类

            MyLog.d("", "attached view: sub type " + imsg.subType);

            if (imsg.subType == MyConfig.SUBTYPE_MAP) {
                MyLog.d("", "attached view: 1 path " + imsg.mapLocation.screenShotPath);
                showMap(imsg, viewHolder);


            }else if(imsg.subType == MyConfig.SUBTYPE_FILE) {

                MyLog.d("","file upload 111");

                /**
                 * 先判断常见扩展名
                 */
                String ext = MyConfig.getExtFromPathOrName(imsg.fileName);
                if(ext == null){
                    Picasso.with(context).load(R.drawable.ext_file).into(viewHolder.msg_attached_image);
                }else if(ext.startsWith("pdf")){
                    Picasso.with(context).load(R.drawable.ext_pdf).into(viewHolder.msg_attached_image);
                }else if(ext.startsWith("doc")){
                    Picasso.with(context).load(R.drawable.ext_doc).into(viewHolder.msg_attached_image);
                }else if(ext.startsWith("ppt")){
                    Picasso.with(context).load(R.drawable.ext_ppt).into(viewHolder.msg_attached_image);
                }else if(ext.startsWith("txt")){
                    Picasso.with(context).load(R.drawable.ext_txt).into(viewHolder.msg_attached_image);
                }else if(ext.startsWith("zip")){
                    Picasso.with(context).load(R.drawable.ext_zip).into(viewHolder.msg_attached_image);
                }else {
                    /**
                     * 再根据mime
                     */
                    String mime = MyConfig.guessMimeByFilename(imsg.fileName);
                    if(mime == null){
                        Picasso.with(context).load(R.drawable.ext_file).into(viewHolder.msg_attached_image);
                    } else if (mime.startsWith(MyConfig.MIME_AUDIO_PREFIX)) {
                        Picasso.with(context).load(R.drawable.ext_audio).into(viewHolder.msg_attached_image);
                    } else if (mime.startsWith(MyConfig.MIME_VIDEO_PREFIX)) {
                        Picasso.with(context).load(R.drawable.ext_video).into(viewHolder.msg_attached_image);
                    } else if (mime.startsWith(MyConfig.MIME_IMAGE_PREFIX)) {
                        Picasso.with(context).load(R.drawable.ext_picture).into(viewHolder.msg_attached_image);
                    } else {
                        Picasso.with(context).load(R.drawable.ext_file).into(viewHolder.msg_attached_image);
                    }
                }

                //点击监听器fileClickedListener
                viewHolder.msg_attached_image.setVisibility(View.VISIBLE);
                viewHolder.msg_attached_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (fileClickedListener != null) {
                            fileClickedListener.onFileClicked(context, imsg);
                        }
                    }
                });

                StringBuilder sb_file_info = new StringBuilder();
                sb_file_info.append(imsg.fileName + "   " + MyConfig.byteConvert(imsg.fileSize));
                if (imsg.fileDownloadProgress == 0) {
                    sb_file_info.append(context.getString(R.string.not_load));
                }else if(imsg.fileDownloadProgress == 100) {

                        sb_file_info.append(context.getString(R.string.have_load));
                }else{
                        sb_file_info.append(context.getString(R.string.loading) + imsg.fileDownloadProgress);
                }
                viewHolder.msg_attached_file_info.setText(sb_file_info.toString());
                viewHolder.msg_attached_file_info.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (downloadClickedListener != null) {
                            downloadClickedListener.onDownloadClicked(imsg);
                        }
                    }
                });
                viewHolder.msg_attached_file_info.setVisibility(View.VISIBLE);

            }else if (imsg.subType == MyConfig.SUBTYPE_IMAGE) {
                MyLog.d("", "attached view: 2 path " + imsg.thumbNail);

                showImage(imsg, viewHolder);

            }else if (imsg.subType == MyConfig.SUBTYPE_VIDEO) {
                MyLog.d("", "attached view: video path " + imsg.thumbNail);

                showImage(imsg, viewHolder);

            } else if (imsg.subType == MyConfig.SUBTYPE_VOTE) {

                /**
                 * vote
                 */
                if (imsg.vote != null) {

                    /**
                     *
                     * 有三种场景
                     *  1、在主聊天室里面
                     *  2、在子聊天室里面，但是本用户还没投过票
                     *  3、在子聊天室里面，本用户已经投过票
                     *
                     */

                    if (this.room_level == MyConfig.LEVEL_ROOM_MAIN) {  //主聊天室
                        viewHolder.msg_attached_vote_mainroom.removeAllViews();

                        showVoteInMainRoom(imsg, viewHolder.msg_attached_vote_mainroom);

                        viewHolder.msg_attached_vote_mainroom.setVisibility(View.VISIBLE);

                    } else if (this.room_level == MyConfig.LEVEL_ROOM_SUB) { //子聊天室
                        viewHolder.msg_extend_vote_subroom.removeAllViews();

                        if (imsg.vote.voted) {    //当前用户已经投过了
                            showVoteInfo(imsg, viewHolder.msg_extend_vote_subroom);
                            showVoteResult(imsg, viewHolder.msg_extend_vote_subroom);


                            MyLog.d("", "vote debug: voted");
                            //发起者可以结束投票
                            if (!imsg.vote.closed && imsg.originDataFromServer.getMsgTopicRet.userId == MyConfig.usr_id) {
                                MyLog.d("", "vote debug: not close and creator. if closed? " + imsg.vote.closed);
                                setFinishButton(imsg, viewHolder);
                            }

                        } else {  //当前用户未投过
                            MyLog.d("", "vote debug: not voted");
                            if (imsg.vote.closed) {
                                showVoteInfo(imsg, viewHolder.msg_extend_vote_subroom);
                                MyLog.d("", "vote debug: closed");
                                showVoteResult(imsg, viewHolder.msg_extend_vote_subroom);
                            } else {
                                MyLog.d("", "vote debug: note closed");
                                showVoteInfo(imsg, viewHolder.msg_extend_vote_subroom);
                                showVoteOptions(imsg, viewHolder.msg_extend_vote_subroom);
                                setSubmitButton(imsg, viewHolder);
                            }

                        }

                        if (MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {
                            if (imsg.vote.closed) {
                                setFinishedFlag(imsg, viewHolder);
                            }
                        }

                        viewHolder.msg_extend_vote_subroom.setVisibility(View.VISIBLE);
                    }

                }

                if (imsg.thumbNail != null) {
                    MyLog.d("", "httptools find image in vote");
                    Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + imsg.thumbNail).into(viewHolder.msg_attached_image);
                    viewHolder.msg_attached_image.setVisibility(View.VISIBLE);
                    viewHolder.msg_attached_image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (imageClickedListener != null) {
                                imageClickedListener.onImageClicked(imsg);//show father gallery
                            }
                        }
                    });
                }
            }else if (imsg.subType == MyConfig.SUBTYPE_ACTIVITY){


                viewHolder.msg_body.setVisibility(View.GONE);

                if (this.room_level == MyConfig.LEVEL_ROOM_MAIN) {  //主聊天室
                    viewHolder.msg_attached_vote_mainroom.removeAllViews();/**首先去除所有的子控件，然后根据需要往里面添加*/

                    showActivityInMainRoom(imsg, viewHolder.msg_attached_vote_mainroom);

                    viewHolder.msg_attached_vote_mainroom.setVisibility(View.VISIBLE);
                    viewHolder.msg_title.setVisibility(View.GONE);

                }else if (this.room_level == MyConfig.LEVEL_ROOM_SUB) { //子聊天室
                    viewHolder.msg_title.setVisibility(View.GONE);
                    viewHolder.msg_avatar.setVisibility(View.GONE);
                    viewHolder.msg_time.setVisibility(View.GONE);
                    viewHolder.msg_user.setVisibility(View.GONE);

                    viewHolder.msg_extend_vote_subroom.removeAllViews();

                    showActivityInSubRoom(imsg, viewHolder.msg_extend_vote_subroom);

                    setActivitySubmmitButton(imsg, viewHolder);

                }

            }
        } else if(imsg.type == MyConfig.TYPE_TOPIC_IMAGE_DIRECTROOM){//私信专有图片类
            showImage(imsg, viewHolder);

        } else if(imsg.type == MyConfig.TYPE_TOPIC_MAP_DIRECTROOM){//私信专有地图类
            showMap(imsg, viewHolder);

        }


        // -------------  subroom share -------------

        if ((imsg.category == MyConfig.MSG_CATEGORY_SUBJECT) && (this.room_level == 2)) {

            /**
             * 子话题页面对subject特殊显示
             */
            viewHolder.msg_extend_comment_divider.setVisibility(View.VISIBLE);
            viewHolder.msg_extend_share.setVisibility(View.VISIBLE);

            if (listItems.get(position).favorited) {
                viewHolder.msg_collect.setImageResource(R.drawable.collection_selected_3x);
                //           Picasso.with(context).load(R.drawable.focus_selected_3x).fit().into(viewHolder.msg_collect);
            } else {
                viewHolder.msg_collect.setImageResource(R.drawable.collection_3x);
                //       Picasso.with(context).load(R.drawable.focus_normal_3x).fit().into(viewHolder.msg_collect);
            }

            viewHolder.msg_collect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    favoriteClickedListener.onFavoriteClicked(imsg.topicId);
                    MyConfig.addFavorite(imsg.topicId, !imsg.favorited);
                    if(imsg.favorited){
                        Toast.makeText(context,"取消收藏",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(context,"收藏成功",Toast.LENGTH_SHORT).show();
                    }


                }
            });

            viewHolder.msg_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (imsg.user_id  == MyConfig.usr_id){
                        onOkClickListener = new MyConfig.IOnOkClickListener() {
                            @Override
                            public void OnOkClickListener() {
                                MyConfig.delTopic(imsg.topicId);
                            }
                        };
                        MyConfig.showRemindDialog("", context.getString(R.string.sure_delete), context, true, onOkClickListener);
                    }
                }
            });

            viewHolder.msg_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (imsg.type == MyConfig.TYPE_TOPIC && imsg.subType == MyConfig.SUBTYPE_ACTIVITY){
//                        MyToast.makeText(context,"报名子话题暂不支持转发",Toast.LENGTH_SHORT).show();
                        MyConfig.MyToast(0, context, "报名子话题暂不支持转发");
                        return;
                    }

                    if(shareClickedListener != null){
                        //暂停对外分享
                        //shareClickedListener.onShareClicked(imsg);
                    }

                    if(forwardClickedListener != null){
                        forwardClickedListener.onForwardClicked(imsg);
                    }
                }
            });


        } else {
            viewHolder.msg_extend_comment_divider.setVisibility(View.GONE);
            viewHolder.msg_extend_share.setVisibility(View.GONE);

        }

        /**
         * 对评论所占空间进行压缩
         */
        if (this.room_level == MyConfig.LEVEL_ROOM_MAIN
                && position > 0
                && imsg.category == MyConfig.MSG_CATEGORY_SUBJECT_COMMENT) {

            IMsg last_imsg = listItems.get(position - 1);
            if (last_imsg.topicId == imsg.topicId) {
                /**
                 * 如果上一条msg和本
                 */

                viewHolder.msg_avatar.setVisibility(View.INVISIBLE);
                viewHolder.msg_title.setVisibility(View.GONE);
                viewHolder.msg_title_reply.setVisibility(View.GONE);
                viewHolder.msg_space.setVisibility(View.GONE);

            }

        }

        return convertView;

    }

    public void showMap(final IMsg imsg, ViewHolder viewHolder) {
        //方法一，自行调节大小
//                MyConfig.imageViewShowWithLimit(viewHolder.msg_attached_image,
//                        new File(imsg.mapLocation.screenShotPath), MyConfig.screenWidth / 2);

        //方法二，用Picasso直接显示
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + imsg.mapLocation.screenShotPath).into(viewHolder.msg_attached_image);

        //方法三，用Picasso调节尺寸后显示

        viewHolder.msg_attached_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    if (mapClickedListener != null) {
//                        mapClickedListener.onMapClicked(imsg);//show father gallery
//                    }

                MyConfig.tempMapLocation = new MapLocation();
                //MyConfig.tempMapLocation.address = imsg.mapLocation.address;
                //暂时的，我们在body里面存放了地址信息。但很遗憾，私信里面没有这个信息
                //2016-5-27 地图地址信息在title中，因此改成title
                MyConfig.tempMapLocation.address = imsg.title;
                MyConfig.tempMapLocation.longitude = imsg.mapLocation.longitude;
                MyConfig.tempMapLocation.latitude = imsg.mapLocation.latitude;
                MyConfig.tempMapLocation.screenShotPath = imsg.mapLocation.screenShotPath;

                Intent intent = new Intent(context, ActivityLocationView.class);
                context.startActivity(intent);

            }
        });


        viewHolder.msg_attached_image.setVisibility(View.VISIBLE);
    }

    public void showImage(final IMsg imsg, ViewHolder viewHolder) {
        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + imsg.thumbNail).into(viewHolder.msg_attached_image);
        MyLog.d("","httptools video adapter image path:"+MyConfig.getApiDomain_NoSlash_GetResources() + imsg.thumbNail);

        //此处要保留，不要删
        if (MyConfig.FALSE_TAG_TO_BYPASS_BUT_KEEP_FOR_FUTURE_USE) {
            /**
             * 此处要保留，不要删
             *
             *  以下transform部分代码自行设置图片尺寸，为以后屏幕适配做基础
             *  没有transform代码的，则是直接按原图显示，在小屏幕上会过大
             */

            Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + imsg.thumbNail).transform(new Transformation() {
                @Override
                public Bitmap transform(Bitmap source) {

                    int src_width = source.getWidth();
                    int src_height = source.getHeight();
                    int new_width = MyConfig.maxSizeInRoomImage;
                    int new_height = MyConfig.maxSizeInRoomImage;

                    if (src_width == src_height) {

                    } else if (src_width > src_height) {
                        new_height = MyConfig.maxSizeInRoomImage * src_height / src_width;
                    } else {
                        new_width = MyConfig.maxSizeInRoomImage * src_width / src_height;
                    }
                    Bitmap result = Bitmap.createScaledBitmap(source, new_width, new_height, false);
                    if (result != source) {
                        source.recycle();
                    }
                    return result;

                }

                @Override
                public String key() {
                    return "fit_list";
                }
            }).into(viewHolder.msg_attached_image);

        }

        viewHolder.msg_attached_image.setVisibility(View.VISIBLE);
        viewHolder.msg_attached_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageClickedListener != null) {
                    imageClickedListener.onImageClicked(imsg);//show father gallery
                }
            }
        });

        if(imsg.type == MyConfig.TYPE_TOPIC && imsg.subType == MyConfig.SUBTYPE_VIDEO) {
            viewHolder.msg_attached_image_video_play.setVisibility(View.VISIBLE);
        }
//        else {
//            viewHolder.msg_attached_image_video_play.setVisibility(View.GONE);
//        }


    }

    public void showIfHaveMsgBody(IMsg imsg, ViewHolder viewHolder) {
        if (imsg.body != null && imsg.body.length() > 0) {
//            viewHolder.msg_body.setText(imsg.body);
            viewHolder.msg_body.setVisibility(View.VISIBLE);
            viewHolder.msg_body.setTextColor(context.getResources().getColor(R.color.xjt_name_desc));

            if(imsg.news != null && imsg.news.equals("news")) {
                /**
                 * 只有share in的news文本里面才有长url，才需要缩短
                 */
                MyConfig.shrinkShareInBodyLink(context, imsg.body, viewHolder.msg_body);
            }else{
                viewHolder.msg_body.setText(EmojiUtils.showEmoji(context, imsg.body));
            }

        }
    }



    public void setFinishedFlag(final IMsg imsg, ViewHolder viewHolder) {
//        viewHolder.msg_extend_vote_send.setTag(viewHolder.msg_vote);
        viewHolder.msg_extend_vote_send.setText(context.getString(R.string.close_vote));
        viewHolder.msg_extend_vote_send.setTextColor(context.getResources().getColor(R.color.Black));
        viewHolder.msg_extend_vote_send.setBackgroundResource(R.color.xjt_background);
        viewHolder.msg_extend_vote_send.setVisibility(View.VISIBLE);
    }

    public void setFinishButton(final IMsg imsg, ViewHolder viewHolder) {
//        viewHolder.msg_extend_vote_send.setTag(viewHolder.msg_vote);
        viewHolder.msg_extend_vote_send.setText(context.getString(R.string.close_vote));
        viewHolder.msg_extend_vote_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (voteFinishClickedListener != null) {
                    MyLog.d("", "HttpTools: vote debug setFinishButton ");
                    voteFinishClickedListener.onVoteFinishClicked(imsg);
                }

            }
        });
        viewHolder.msg_extend_vote_send.setVisibility(View.VISIBLE);
    }

    public void setSubmitButton(final IMsg imsg, ViewHolder viewHolder) {
//        viewHolder.msg_extend_vote_send.setTag(viewHolder.msg_vote);
        viewHolder.msg_extend_vote_send.setText(context.getString(R.string.start_vote));
        viewHolder.msg_extend_vote_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinkedList<Integer> options = new LinkedList();

                Iterator<Integer> it = imsg.vote.checked.iterator();
                while (it.hasNext()) {
                    options.add(it.next());
                }


//                for (ViewVoteUpdateOption viewVoteUpdateOption : viewVoteUpdateOptionsList) {
//                    if (viewVoteUpdateOption.isChecked()) {
//                        options.add(viewVoteUpdateOption.getNum());
//                    }
//                }

//                LinearLayout msg_vote = (LinearLayout) v.getTag();
//                int count = msg_vote.getChildCount();
//                for (int i = 1; i < count; i++) { //ViewVoteUpdateOption从索引1开始，索引0是ViewVoteResultSum
//                    ViewVoteUpdateOption vvou = (ViewVoteUpdateOption) msg_vote.getChildAt(i);
//                    if (vvou.isChecked()) {
//                        options.add(vvou.getNum());
//                    }
//                }

                if (voteSubmitClickedListener != null) {
                    MyLog.d("", "voteSubmitClickedListener in listadaptermsg");
                    voteSubmitClickedListener.onVoteSubmitClicked(imsg.vote.resouceId, options);
                }

            }
        });
        viewHolder.msg_extend_vote_send.setVisibility(View.VISIBLE);
    }

    public void showVoteOptions(IMsg imsg, LinearLayout linearLayout) {

        viewVoteUpdateOptionsList = new LinkedList<>();
        for (String key : imsg.vote.options.keySet()) {
            String s = imsg.vote.options.get(key);
            ViewVoteUpdateOption viewVoteUpdateOption = new ViewVoteUpdateOption(context, imsg, s, Integer.valueOf(key));
            viewVoteUpdateOption.voteOptionClickedListener = voteOptionClickedListener;
//            viewVoteUpdateOption.setOption(s);
//            viewVoteUpdateOption.setNum(Integer.valueOf(key));
//            viewVoteUpdateOption.updateToIMsg(imsg.vote.checked.contains(Integer.valueOf(key)));
            viewVoteUpdateOptionsList.add(viewVoteUpdateOption);
        }

        Collections.sort(viewVoteUpdateOptionsList, new Comparator<ViewVoteUpdateOption>() {
            @Override
            public int compare(ViewVoteUpdateOption o1, ViewVoteUpdateOption o2) {
                return o1.num - o2.num;
            }
        });

        for (ViewVoteUpdateOption viewVoteUpdateOption : viewVoteUpdateOptionsList) {
            linearLayout.addView(viewVoteUpdateOption);
        }

    }

    public void showVoteInMainRoom(final IMsg imsg, LinearLayout linearLayout) {

        ViewVoteInfoMainroom viewVoteInfoMainroom = new ViewVoteInfoMainroom(context, imsg);

        //  viewVoteInfoMainroom.vote_in_main_layout.getLayoutParams().width = getVoteWidth();
        viewVoteInfoMainroom.vote_in_main_layout.setOnClickListener(new View.OnClickListener() {
            //点击投票图片进入子话题
            @Override
            public void onClick(View v) {

                linkClickedListener.onLinkClicked(imsg);//goto a sub room of this subject
            }
        });

        linearLayout.addView(viewVoteInfoMainroom);
    }

    /**
     * 报名在主聊天室中的显示样式
     * @param imsg
     * @param linearLayout
     */
    public void showActivityInMainRoom(final IMsg imsg, LinearLayout linearLayout) {

        ViewActivityInfoMainroom viewActivityInfoMainroom = new ViewActivityInfoMainroom(context, imsg);

        //  viewVoteInfoMainroom.vote_in_main_layout.getLayoutParams().width = getVoteWidth();
        viewActivityInfoMainroom.activity_in_main_layout.setOnClickListener(new View.OnClickListener() {
            //点击投票图片进入子话题
            @Override
            public void onClick(View v) {

                linkClickedListener.onLinkClicked(imsg);//goto a sub room of this subject
            }
        });

        linearLayout.addView(viewActivityInfoMainroom);
    }
    /**
     * 报名在子聊天室中的显示样式
     * @param imsg
     * @param linearLayout
     */
    public void showActivityInSubRoom( IMsg imsg, LinearLayout linearLayout) {

        viewActivityInfoSubroom = new ViewActivityInfoSubroom(context, imsg);


        linearLayout.addView(viewActivityInfoSubroom);
        linearLayout.setVisibility(View.VISIBLE);
        /**在报名时选择“报名人数”的点击响应事件*/
        viewActivityInfoSubroom.ll_activity_people_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(activityInputPeopleNumClickedListener != null){
                    activityInputPeopleNumClickedListener.onActivityInputPeopleNumClicked();
                }

            }
        });
    }

    /**
     * 设置报名页面的button
     * @param imsg
     * @param viewHolder
     */
    private void setActivitySubmmitButton(final IMsg imsg, ViewHolder viewHolder) {

        if(imsg.mActivity.closed){/**报名是否关闭*/
            viewHolder.msg_extend_vote_send.setVisibility(View.GONE);
        }else{
            if(imsg.originDataFromServer.getMsgTopicRet.userId == MyConfig.usr_id){/**如果是自己创建的*/
                viewHolder.msg_extend_vote_send.setText("关闭报名");
                viewHolder.msg_extend_vote_send.setVisibility(View.VISIBLE);

                viewHolder.msg_extend_vote_send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /**关闭报名*/

                        if(activityCloseClickedListener != null){
                            activityCloseClickedListener.onActivityCloseClicked(imsg.mActivity.resouceId);
                        }
                    }
                });

            }else{

                switch (imsg.mActivity.status){
                    case 0://未报名
                        viewHolder.msg_extend_vote_send.setText("报名");
                        viewHolder.msg_extend_vote_send.setVisibility(View.VISIBLE);
                        viewHolder.msg_extend_vote_send.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                /**报名*/
                                String s = viewActivityInfoSubroom.activity_people_num.getText().toString().trim();
                                int num = Integer.parseInt(s);
                                if(activitySubmitClickedListener != null){
                                    activitySubmitClickedListener.onActivitySubmitClicked(imsg.mActivity.resouceId,num);
                                }
                            }
                        });
                    break;
                    case 1://已经报名
                         viewHolder.msg_extend_vote_send.setText("取消报名");
                         viewHolder.msg_extend_vote_send.setVisibility(View.VISIBLE);

                         viewHolder.msg_extend_vote_send.setOnClickListener(new View.OnClickListener() {
                             @Override
                             public void onClick(View v) {
                                 /**取消报名*/
                                 if(activityLeaveClickedListener != null){
                                     activityLeaveClickedListener.onActivityLeaveClicked(imsg.mActivity.resouceId);
                                 }
                             }
                         });
                    break;
                    case 2://排队
                        viewHolder.msg_extend_vote_send.setText("取消排队");
                        viewHolder.msg_extend_vote_send.setVisibility(View.VISIBLE);
                        viewHolder.msg_extend_vote_send.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                /**取消报名*/
                                if(activityLeaveClickedListener != null){
                                    activityLeaveClickedListener.onActivityLeaveClicked(imsg.mActivity.resouceId);
                                }
                            }
                        });
                    break;

                }
            }
        }


    }

    public void showVoteInfo(final IMsg imsg, LinearLayout linearLayout) {
        //显示总的投票结果
        ViewVoteResultInfo viewVoteResultInfo;

        viewVoteResultInfo = new ViewVoteResultInfo(context, imsg);

        //  viewVoteResultInfo.update_vote_sum_layout.getLayoutParams().width = getVoteWidth();
        if (!imsg.vote.anonymous && imsg.vote.voterNum > 0) {
            viewVoteResultInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (votePersonListClickedListener != null) {
                        votePersonListClickedListener.onVotePersonListClicked(imsg);
                    }
                }
            });
        } else {
            viewVoteResultInfo.setClickable(false);
        }
        linearLayout.addView(viewVoteResultInfo);
    }

    public void showVoteResult(final IMsg imsg, LinearLayout linearLayout) {
        //显示每条option的投票结果
        LinkedList<ViewVoteResultOption> linkedList = new LinkedList<>();
        for (String key : imsg.vote.options.keySet()) {
            MyLog.d("", "httptool vote result option in adaper: " + imsg.vote.options.toString());

            ViewVoteResultOption viewVoteResultOption = new ViewVoteResultOption(context, Integer.valueOf(key), imsg.vote.options.get(key), imsg.vote.choices.get(key).votesCount, imsg.vote.voterNum);
            //  viewVoteResultOption.update_vote_option_result_layout.getLayoutParams().width = getVoteWidth();

            linkedList.add(viewVoteResultOption);
        }

        Collections.sort(linkedList, new Comparator<ViewVoteResultOption>() {
            @Override
            public int compare(ViewVoteResultOption o1, ViewVoteResultOption o2) {
                return o1.num - o2.num;
            }
        });

        for (ViewVoteResultOption viewVoteResultOption : linkedList) {
            linearLayout.addView(viewVoteResultOption);
        }

    }

    public int getVoteWidth() {
        return MyConfig.getWidthByScreenPercent(66);
    }


}
