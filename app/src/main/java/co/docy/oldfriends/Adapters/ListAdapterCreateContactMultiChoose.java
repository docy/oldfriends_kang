package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;


public class ListAdapterCreateContactMultiChoose extends BaseAdapter   {

    private static class ViewHolder {
        LinearLayout contact_layout;
        ImageView contact_avatar;
        TextView contact_name;
        TextView contact_phone;
        ToggleButton contact_choose;
    }

    Context context;
    List<ContactUniversal> list;
    private String category;
    private static LayoutInflater inflater = null;

    @Override
    public ContactUniversal getItem(int position) {
        return list.get(position);
    }

    public ListAdapterCreateContactMultiChoose(Context context,List<ContactUniversal> objects,String category) {
        this.context = context;
        this.list = objects;
        this.category = category;
        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        MyLog.d("","ListAdapterContactOfCompanyMultiChoose getCount "+list.size());
        return list.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        MyLog.d("","ListAdapterContactOfCompanyMultiChoose position "+position);
        final ContactUniversal contactUniversal = list.get(position);
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_contact_multi_choose, null);
            viewHolder.contact_layout = (LinearLayout)convertView.findViewById(R.id.contact_layout_person);
            viewHolder.contact_avatar = (ImageView) convertView.findViewById(R.id.contact_avatar);
            viewHolder.contact_name = (TextView) convertView.findViewById(R.id.contact_desc);
            viewHolder.contact_phone = (TextView) convertView.findViewById(R.id.contact_phone);
            viewHolder.contact_choose = (ToggleButton) convertView.findViewById(R.id.contact_choose);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + contactUniversal.avatar)
//                .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(viewHolder.contact_avatar);

        viewHolder.contact_avatar.setTag(R.id.adapter_company_list_position, position);
        viewHolder.contact_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.gotoUserInfo(context, list.get(((Integer) (v.getTag(R.id.adapter_company_list_position))).intValue()).id);
            }
        });
       // viewHolder.contact_name.setText(contactUniversal.name);
        viewHolder.contact_name.setText(contactUniversal.nickName);
        viewHolder.contact_phone.setText(contactUniversal.phone);
        final ToggleButton contact_choose = viewHolder.contact_choose;

        setToggleButtonChecked(contactUniversal, contact_choose);
        viewHolder.contact_choose.setClickable(false);
        viewHolder.contact_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactUniversal.selected = !contactUniversal.selected ;
                setToggleButtonChecked(contactUniversal, contact_choose);
            }
        });

        if(category.equals(MyConfig.CREATE_GROUP_PEOPLE_CATEGORY_MORE)){
            viewHolder.contact_choose.setVisibility(View.GONE);
            viewHolder.contact_layout.setOnClickListener(null);
        }else {
            viewHolder.contact_choose.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    /**
     * 设置ButtonChecked显示状态，若是删除成员则是相反显示，若是其他就正常显示
     * @param contactUniversal
     * @param contact_choose
     */
    private void setToggleButtonChecked(ContactUniversal contactUniversal, ToggleButton contact_choose) {
        if(category.equals(MyConfig.CREATE_GROUP_PEOPLE_CATEGORY_KICK)){
            contact_choose.setChecked(!contactUniversal.selected);
        }else {
            contact_choose.setChecked(contactUniversal.selected);
        }
    }
}
