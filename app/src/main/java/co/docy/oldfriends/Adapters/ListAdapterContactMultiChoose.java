package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.DataClass.ContactUniversal;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;


public class ListAdapterContactMultiChoose extends BaseAdapter   {


    public interface OnLongClickedListener {
        public void OnLongClicked(int position);
    }
    public OnLongClickedListener longClickListener;

    public interface OnClickedListener {
        public void OnClicked(int position);
    }
    public OnClickedListener clickListener;

    public interface OnChooseChangedListener {
        public void OnClicked(int position, boolean isChecked);
    }
    public OnChooseChangedListener chooseChangedListener;

    private static class ViewHolder {
        LinearLayout contact_layout;
        ImageView contact_avatar;
        TextView contact_name;
        TextView contact_phone;
        ToggleButton contact_choose;
    }


    Context context;
    int mode;
    List<ContactUniversal> list;

    private static LayoutInflater inflater = null;


    @Override
    public ContactUniversal getItem(int position) {
        return list.get(position);
    }

    public ListAdapterContactMultiChoose(Context context,
                                         List<ContactUniversal> objects, int mode, OnClickedListener o, OnLongClickedListener oo, OnChooseChangedListener ooo) {
        this.context = context;
        this.list = objects;
        this.mode = mode;
        this.clickListener = o;
        this.longClickListener = oo;
        this.chooseChangedListener = ooo;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        MyLog.d("","ListAdapterContactOfCompanyMultiChoose getCount "+list.size());
        return list.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        MyLog.d("","ListAdapterContactOfCompanyMultiChoose position "+position);

        final ContactUniversal contactUniversal = getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_contact_multi_choose, null);
            viewHolder.contact_layout = (LinearLayout)convertView.findViewById(R.id.contact_layout_person);
            viewHolder.contact_avatar = (ImageView) convertView.findViewById(R.id.contact_avatar);
            viewHolder.contact_name = (TextView) convertView.findViewById(R.id.contact_desc);
            viewHolder.contact_phone = (TextView) convertView.findViewById(R.id.contact_phone);
            viewHolder.contact_choose = (ToggleButton) convertView.findViewById(R.id.contact_choose);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + contactUniversal.avatar)
//                .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                .fit()
                .centerCrop()
                .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                .into(viewHolder.contact_avatar);

        viewHolder.contact_avatar.setTag(R.id.adapter_company_list_position, position);
        MyLog.d("", "HttpTools: jsonGetUserInfo.id position:" + position + " id:" + contactUniversal.id);
        viewHolder.contact_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConfig.gotoUserInfo(context, list.get(((Integer) (v.getTag(R.id.adapter_company_list_position))).intValue()).id);
            }
        });
       // viewHolder.contact_name.setText(contactUniversal.name);
        viewHolder.contact_name.setText(contactUniversal.nickName);
        viewHolder.contact_phone.setText(contactUniversal.phone);

        viewHolder.contact_choose.setChecked(contactUniversal.selected);
        viewHolder.contact_choose.setClickable(false);
        if(mode == MyConfig.MODE_CHOOSE_MULTI) {
            viewHolder.contact_choose.setVisibility(View.VISIBLE);
        }else if(mode == MyConfig.MODE_CHOOSE_SINGLE){
            viewHolder.contact_choose.setVisibility(View.VISIBLE);
        }else if(mode == MyConfig.MODE_CHOOSE_SINGLE_WITHOUT_SUBMIT){
            viewHolder.contact_choose.setVisibility(View.GONE);
        }

        viewHolder.contact_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickListener != null){
                    clickListener.OnClicked(position);
                }
            }
        });

//        viewHolder.contact_choose.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(chooseChangedListener != null){
//                    chooseChangedListener.OnClicked(position, isChecked);
//                    MyLog.d("", "ListAdapterContactOfCompanyMultiChoose changed position " + position+" "+isChecked);
//                }
//            }
//        });


        return convertView;

    }



    private void setOnClick(View v, int position) {
        v.setTag(R.id.adapter_company_list_position, position);
        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (longClickListener != null) {
                    longClickListener.OnLongClicked(
                            ((Integer) (v.getTag(R.id.adapter_company_list_position))).intValue()
                    );
                }
                return false;
            }
        });
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.OnClicked(
                            ((Integer) (v.getTag(R.id.adapter_company_list_position))).intValue()
                    );
                }else{

                }
            }
        });
    }

}
