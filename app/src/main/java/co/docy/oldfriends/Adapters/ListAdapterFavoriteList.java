package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.List;

import co.docy.oldfriends.Configure.MyConfig;
import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.Messages.JsonGetSubjectListRet;
import co.docy.oldfriends.R;
import co.docy.oldfriends.Views.PicassoTransformationRounded;


public class ListAdapterFavoriteList extends BaseAdapter {

    boolean isCheckBoxEnable = true;

    public interface OnLongClickedListener {
        public void OnLongClicked(int id);
    }

    public OnLongClickedListener longClickListener;

    public interface OnClickedListener {
        public void OnClicked(int id);
    }

    public OnClickedListener clickListener;

    public interface OnCheckedChangListener {
        public void checkChange(boolean isChecked, int position);
    }

    public OnCheckedChangListener onCheckedChangListener;

    private static class ViewHolder {
        ImageView row_icon;
        TextView row_name;
        TextView row_description;
        TextView row_update;
        RelativeLayout row_layout;
        TextView subject_view, subject_comment;
        CheckBox subject_favourite;
        View split_line, split_line_buttom;
    }

    Context context;
    final List<JsonGetSubjectListRet.GetSubjectListRet> listItems;
    private static LayoutInflater inflater = null;

    public static String searchKey = "";

    @Override
    public JsonGetSubjectListRet.GetSubjectListRet getItem(int position) {
        return listItems.get(position);
    }

    public ListAdapterFavoriteList(Context context,
                                  LinkedList<JsonGetSubjectListRet.GetSubjectListRet> objects, OnClickedListener o, ListAdapterFavoriteList.OnLongClickedListener oo, boolean isCheckBoxEnable) {
        this.context = context;
        this.listItems = objects;
        this.clickListener = o;
        this.longClickListener = oo;
        this.isCheckBoxEnable = isCheckBoxEnable;

        this.inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listItems.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        JsonGetSubjectListRet.GetSubjectListRet subject = getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_subject, parent, false);
            viewHolder.row_icon = (ImageView) convertView.findViewById(R.id.subject_icon);
            viewHolder.row_name = (TextView) convertView.findViewById(R.id.subject_name);
            viewHolder.split_line = convertView.findViewById(R.id.split_line);
            viewHolder.split_line_buttom = convertView.findViewById(R.id.split_line_button);
            viewHolder.row_description = (TextView) convertView.findViewById(R.id.subject_description);
            viewHolder.row_update = (TextView) convertView.findViewById(R.id.subject_update);
            viewHolder.row_layout = (RelativeLayout) convertView.findViewById(R.id.subject_layout);
            viewHolder.subject_comment = (TextView) convertView.findViewById(R.id.subject_comment);
            viewHolder.subject_favourite = (CheckBox) convertView.findViewById(R.id.subject_favorite);
            viewHolder.subject_view = (TextView) convertView.findViewById(R.id.subject_views);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.row_layout.setBackgroundResource(R.color.White);

        if (subject.type == MyConfig.SUBTYPE_FILE) {
            Picasso.with(context).load(R.drawable.list_file)
                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(viewHolder.row_icon);
        } else if (subject.type == MyConfig.SUBTYPE_IMAGE) {
            Picasso.with(context).load(MyConfig.getApiDomain_NoSlash_GetResources() + subject.info.thumbNail)
                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(viewHolder.row_icon);
        } else if (subject.type == MyConfig.SUBTYPE_TEXT) {
            Picasso.with(context).load(R.drawable.list_topic)
                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(viewHolder.row_icon);
        } else if (subject.type == MyConfig.SUBTYPE_MAP) {
            Picasso.with(context).load(R.drawable.list_map)
                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(viewHolder.row_icon);
        } else if (subject.type == MyConfig.SUBTYPE_VOTE) {
            Picasso.with(context).load(R.drawable.list_vote)
                    .resize(MyConfig.AVATAR_RESIZE, MyConfig.AVATAR_RESIZE)
                    .transform(new PicassoTransformationRounded(MyConfig.AVATAR_RADIUS, 0))
                    .into(viewHolder.row_icon);
        }
        if (!TextUtils.isEmpty(searchKey)) {
            MyConfig.addForeColorSpan(subject.title, context.getResources().getColor(R.color.xjt_vote_result_2), subject.title.indexOf(searchKey), subject.title.indexOf(searchKey) + searchKey.length(), viewHolder.row_name);
        } else {
            viewHolder.row_name.setText(subject.title);
        }

        if (isCheckBoxEnable) {
            viewHolder.subject_favourite.setOnCheckedChangeListener(null);//如果这里不清空，上一轮设置的OnCheckedChangeListener还在
            viewHolder.subject_favourite.setChecked(subject.favorited);//如果上一轮设置的OnCheckedChangeListener还在，这个设置操作会触发一次动作
            viewHolder.subject_favourite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    onCheckedChangListener.checkChange(isChecked, position);
                }
            });
        } else {
            viewHolder.subject_favourite.setChecked(true);
            viewHolder.subject_favourite.setEnabled(false);
        }
        MyLog.d("", "subject.comments.size" + subject.comments.size());
        if (subject.comments.size() > 0) {
            JsonGetSubjectListRet.LastComment lastComment = subject.comments.getLast();
            StringBuilder sb = new StringBuilder();

            sb.append(lastComment.nickName + ":");

            if (lastComment.type == MyConfig.SUBTYPE_TEXT_COMMENT) {
                sb.append(lastComment.message);
            } else if (lastComment.type == MyConfig.SUBTYPE_IMAGE_COMMENT) {
                sb.append(context.getString(R.string.menu_plus_gallery));
            }
            viewHolder.row_description.setText(sb.toString());
        } else {
            viewHolder.row_description.setText("暂无回复。。。");
        }
        // viewHolder.row_update.setText(MyConfig.Date2GroupInfoTime(MyConfig.UTCString2Date((subject.updatedAt== null)?subject.createdAt:subject.updatedAt)));
        /**row_update是更新日期**/
        if (subject.commentCount == 0 || subject.comments.size() == 0) {
            viewHolder.row_update.setText(MyConfig.Date2GroupInfoTime(MyConfig.UTCString2Date(subject.createdAt)));
        } else {
            viewHolder.row_update.setText(MyConfig.Date2GroupInfoTime(MyConfig.UTCString2Date(subject.comments.getLast().createdAt)));
        }



        String viewString = String.format(context.getString(R.string.click), subject.views);
        viewHolder.subject_view.setText(viewString);
        String commentCountString = String.format(context.getString(R.string.reply),subject.commentCount);
        viewHolder.subject_comment.setText(commentCountString);
        String favoriteCountString = String.format(context.getString(R.string.focus),subject.favoriteCount);
        viewHolder.subject_favourite.setText(favoriteCountString);

        setOnClick(viewHolder.row_layout, position);

        return convertView;

    }

    private int getTypeIcon(int type) {
        return R.drawable.c_ic_launcher;
    }

    private void setOnClick(View v, int position) {
        v.setTag(R.id.adapter_subject_list_position, position);
        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (longClickListener != null) {
                    longClickListener.OnLongClicked(
                            ((Integer) (v.getTag(R.id.adapter_subject_list_position))).intValue()
                    );
                }
                return false;
            }
        });
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.OnClicked(
                            ((Integer) (v.getTag(R.id.adapter_subject_list_position))).intValue()
                    );
                }
            }
        });
    }

}
