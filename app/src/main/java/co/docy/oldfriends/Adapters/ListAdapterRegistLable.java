package co.docy.oldfriends.Adapters;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import co.docy.oldfriends.Configure.MyLog;
import co.docy.oldfriends.R;

/**
 * Created by fighting on 2016/8/15.
 */
public class ListAdapterRegistLable extends RecyclerView.Adapter<ListAdapterRegistLable.ViewHolder> {
    private Context mContext;
    private ArrayList<String> mSelectedLabelList;
    private ArrayList<String> mUnselectedLabelList;

    /**条目类型，标题时为0*/
    private int VIEW_TYPET_TITLE = 0;
    /**条目类型，正常时为1*/
    private int VIEW_TYPET_NORMAL = 1;

    /**已经选择的标签集合的长度，包括虚线框的个数，根据这个变量显示虚线框的个数。*/
    public int selectedLabelListLength;

    public ListAdapterRegistLable(Context context, ArrayList<String> selectedLabelList,ArrayList<String> unselectedLabelList,
                                  MyClickListener clickListener,DeleteClickListener deleteClickListener) {
        mContext = context;
        mSelectedLabelList = selectedLabelList;
        mUnselectedLabelList = unselectedLabelList;
        mClickListener = clickListener;
        mDeleteClickListener = deleteClickListener;
        updateSelectedLabelListLength();
        MyLog.d("kwwl","selectedLabelListLength ======================="+selectedLabelListLength);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0
                || position == selectedLabelListLength+1
                || position > getItemCount()-3 ) {
            return VIEW_TYPET_TITLE;
        } else {
            return VIEW_TYPET_NORMAL;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPET_TITLE) {
            View view = View.inflate(mContext, R.layout.row_regist_label_title, null);
            return new ViewHolder(view, viewType);
        } else {
            View view = View.inflate(mContext, R.layout.row_regist_label, null);
            return new ViewHolder(view, viewType);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(holder.mViewType == VIEW_TYPET_TITLE){
            if(position == 0) {
                holder.mTextView.setVisibility(View.VISIBLE);
                holder.mTextView.setText("已选标签("+mSelectedLabelList.size()+")");
            }else if(position > getItemCount()-3){/**底部空白使用的是title的View，目的是使用它占据空间，但不可见*/
                holder.mTextView.setVisibility(View.INVISIBLE);
            }else {
                holder.mTextView.setVisibility(View.VISIBLE);
                holder.mTextView.setText("可选标签");
            }
        }else {
            if(position < mSelectedLabelList.size()+1) {
                holder.mImageView.setVisibility(View.VISIBLE);
                holder.mTextView.setText(mSelectedLabelList.get(position - 1));
                holder.mTextView.setBackgroundResource(R.drawable.shape_regist_label_bg);
            }else if(position < selectedLabelListLength+1) {
                holder.mImageView.setVisibility(View.GONE);
                holder.mTextView.setText("");
                holder.mTextView.setBackgroundResource(R.drawable.regist_label_selected_dashline_3x);
            }else{
                holder.mImageView.setVisibility(View.GONE);
                holder.mTextView.setText(mUnselectedLabelList.get(position - selectedLabelListLength-2));
                holder.mTextView.setBackgroundResource(R.drawable.shape_regist_label_bg);
            }

        }
    }

    @Override
    public int getItemCount() {
        return selectedLabelListLength + mUnselectedLabelList.size()+4;/**加4的原因是：2个标题，2个底部空白。*/
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        final RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager){
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
            final GridLayoutManager.SpanSizeLookup spanSizeLookup = gridLayoutManager.getSpanSizeLookup();
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup(){
                @Override
                public int getSpanSize(int position){/**这个方法的意思是一个条目占据几个格，所以当为标题时要返回4个，表示占据四个格*/
                    int viewType = getItemViewType(position);
                    if (viewType == VIEW_TYPET_TITLE){
                        return gridLayoutManager.getSpanCount();//等于4
                    }else {
                        return spanSizeLookup.getSpanSize(position);//等于1
                    }
                }
            });
            gridLayoutManager.setSpanCount(gridLayoutManager.getSpanCount());
        }
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public int mViewType;
        public TextView mTextView;
        public ImageView mImageView;

        public ViewHolder(View v, int viewType) {/**注意此时一定要添加viewType参数*/
            super(v);
            mViewType = viewType;
            if (viewType == VIEW_TYPET_TITLE) {
                mTextView = (TextView) v.findViewById(R.id.register_lable_title);
            }else {
                mTextView = (TextView) v.findViewById(R.id.register_lable_name);
                mImageView = (ImageView) v.findViewById(R.id.register_lable_delete);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mClickListener != null && mViewType == VIEW_TYPET_NORMAL)
                            mClickListener.onClick(getAdapterPosition(),selectedLabelListLength);
                    }
                });

                mImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mDeleteClickListener != null && mViewType == VIEW_TYPET_NORMAL)
                            mDeleteClickListener.onClick(getAdapterPosition());
                    }
                });

            }
        }
    }

    /**
     * 更新已经选择的标签的集合的长度，每次调用adapter.notifyDataSetChanged();都要调用这个方法。
     */
    public void updateSelectedLabelListLength(){
        if(mSelectedLabelList.size()%4 == 0){
            selectedLabelListLength = mSelectedLabelList.size() + 4 ;
        }else {
            selectedLabelListLength = (mSelectedLabelList.size()/4 + 1) * 4;
        }
    }


    /**条目点击监听器接口*/
    public interface MyClickListener {
        /**当点击删除时被调用*/
        public void onClick(int position,int length);
    }

    /**删除已选择的标签监听器接口，此次是在适配器的构造方法中传递过来*/
    public MyClickListener mClickListener;

    /**删除已选择的标签监听器接口*/
    public interface DeleteClickListener {
        /**当点击条目时被调用*/
        public void onClick(int position);
    }

    /**条目点击监听器对象，此次是在适配器的构造方法中传递过来*/
    public DeleteClickListener mDeleteClickListener;
}
